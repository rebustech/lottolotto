<?php

/**
 * Provides the engine that is used for lodging insurance through Loteria Castillo
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */

namespace LL\TicketEngines;

class LoteriaCastillo extends \BaseTicketEngine {

    var $aBallNames=array(
        \Lottery::EUROPEANLOTTERY=>array('number','number','number','number','number','star','star'),
        \Lottery::EUROJACKPOT=>array('number','number','number','number','number','star','star'),
    );


    function _getBackendConfigurationFields() {

        $aConfigurationFields = new \ConfigurationEngine();

        $aConfigurationFields->addField(new \ConfigurationString('sEndPoint', \lang::get('API End Point')));
        $aConfigurationFields->addField(new \ConfigurationString('sTicketType', \lang::get('Ticket Type ID')));
        $aConfigurationFields->addField(new \ConfigurationString('sPreSharedKey', \lang::get('Preshared Key')));

        return $aConfigurationFields;
    }

    /**
     * Calculates the cost based on the prize payout for each of the prize tiers
     * that this instance of the engine has been set to cover
     * @param type $oTicket
     */
    function getCost($oTicket) {
        return 1;
    }

    /**
     * Submits all tickets for this draw via either email or FTP
     * Lotto Gopher have an API planned for the future
     * @param draw $oDraw
     */
    function beforeDraw(\draw $oDraw) {
        //Start a ticket buying session
        $oTicketSession = \TicketPurchasingSession::startSession($this->id, $oDraw);
        $this->oTicketSession=$oTicketSession;

        $sAuditLogMsg = "Loteria Castillo - starting buying session, id {$oTicketSession->id}";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_PURCHASE_SESSION_START', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        //// ORIGINAL CODE FROM CASTILLO ///
        $count = 0;

        /**
         * Convert those into a CSV file to send to PIMS
         * Could have done this with a configurable field list, but best to hardcode
         * as it's going to be handling quite a lot of data
         */
        $aOutData = array();
        $bFoundTickets = true;

        $count = 0;
        $sAuditLogMsg = "Loteria Castillo - Looking for tickets";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_SEARCHING_TICKETS', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        // Stop once all tickets have been found
        $aTickets = $oTicketSession->getTickets(5000);


        if (sizeof($aTickets) == 0) {
            $sAuditLogMsg = "No tickets found for Loteria Castillo";
            \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_NO_TICKETS_FOUND', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

            $bFoundTickets = false;
        }
        $this->config=stripslashes($this->config);

        $sAuditLogMsg = $this->config;
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_GETTING_CONFIG', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        $this->oConfigData = json_decode($this->config);
        $sAuditLogMsg = print_r($this->oConfigData, 1);
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_GOT_CONFIG', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        if ($this->oConfigData->sEndPoint) {
            $iTotalTickets=0;
            $iFailedTickets=0;
            $iSuccessTickets=0;

            foreach ($aTickets as $oTicket) {
                $aOutData=array();
                $count=0;

                $aOutData[$count]['numbers'] = $oTicket['numbers'];
                $aOutData[$count]['lotteryid'] = $this->_getCastilloLotteryIdAndPriceFromSystemId($oTicket['fk_lottery_id']);
                $aOutData[$count]['drawdate'] = $this->_formatCastilloLotteryDate($oTicket['fk_lotterydate']);

                // Create XML ready for sending over
                $sXMLString = $this->buildXML($aOutData);

                $oTicketSession->writeRequestToEngine($sXMLString);

                // Send the XML via CURL and get response
                $sResponse = $this->curl_call($sXMLString, $oTicketSession);

                // Get data into an array
                $aResponseData = $this->_convertXMLToArray($sResponse);

                // Decode it
                $aContent = $this->decodeMessageFromCURL($aResponseData);


                // At this point, we should have a status
                if ($aContent['status'] == 'OK') {
                    $iSuccessTickets++;
                } else {
                    $iFailedTickets++;
                    // Raise provblem with ticket buying
                    $sAuditLogMsg = "A problem has occured with buying tickets please contact us. Respose is " . $aContent['status'];
                    \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_PROBLEM_BUYING_TICKETS', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
                    // Take this ticket out of the buying session so it doesn't get marked as done
                    $aData=array('fk_ticket_purchasing_session_id'=>null);
                    \DAL::Update('booking_items',$aData,'bookingitem_id = '.$oTicket['bookingitem_id']);
                }
            }

            $this->last_run_date=date('Y-m-d H:i:s');
            if($iSuccessTickets>0){
                $this->successful_tickets+=$iSuccessTickets;
                $this->successful_runs++;
                $this->last_successful_run_date=date('Y-m-d H:i:s');
                $this->last_result='OK';
                $sAuditLogMsg = 'Loteria Castillo - '.$iSuccessTickets.' / '.$iTotalTickets.' tickets bought';
                \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_BOUGHT', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
                $oTicketSession->endSession();
            }

            if($iFailedTickets>0){
                $oTicketSession->abandonSession();

                $this->failed_tickets+=$iFailedTickets;
                $this->failed_runs++;
                $this->last_failure_run_date=date('Y-m-d H:i:s');
                $this->last_result='FAIL';
                // If we're getting any failures reduce the priority of this system
                $this->priority--;

                // Raise provblem with ticket buying
                $sAuditLogMsg = 'Loteria Castillo -  '.$iFailedTickets.' / '.$iTotalTickets.' failed to buy tickets '.$strErr;
                \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_PROBLEM_BUYING_TICKETS', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
            }

            //If we havn't had a successful run in 40 minutes suspend the engine
            if(time()-strtotime($this->last_successful_run_date) > (40*60) && $this->last_successful_run_date!='0000-00-00 00:00:00'){
                $this->is_suspended=1;
            }

            $this->save();

            $oTicketSession->endSession();

        } else {
            $this->failed_runs++;
            $this->last_failure_run_date=date('Y-m-d H:i:s');
            $this->last_result='CONFIG ERROR';
            \AuditLog::LogItem('No endpoint configured ' . $this->config, 'CASTILLO_PROBLEM_NO_ENDPOINT', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
            $oTicketSession->abandonSession();
        }

    }

    /**
     * Once we have prepared a bunch of tickets for sending they are marked as pending
     */
    function markPending() {
        $sAuditLogMsg = "Loteria Castillo - tickets marked as pending - session id " . $this->id;
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_MARKED_PENDING', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        \DAL::Update('booking_items', array('purchased', 1), 'fk_ticket_purchasing_session_id=' . $this->id);
    }

    /**
     * Once the tickets have been transmitted to the provider we mark them as sent
     */
    function markSent() {
        $sAuditLogMsg = "Loteria Castillo - tickets marked as sent - session id " . $this->id;
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_MARKED_SENT', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        \DAL::Update('booking_items', array('purchased', 2), 'fk_ticket_purchasing_session_id=' . $this->id);
    }

    /**
     * Once we have confirmed that all the tickets in the session were successfully purchased
     * use this to confirm all the open tickets as bought.
     * Don't forget to also call endSession as confirmSession doesn't do this for you
     */
    function confirmSession() {
        $sAuditLogMsg = "Loteria Castillo - tickets marked as confirmed - session id " . $this->id;
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_MARKED_CONFIRMED', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        \DAL::Update('booking_items', array('purchased', 3), 'fk_ticket_purchasing_session_id=' . $this->id);
    }

    /**
     * Encrypts a string via 3DES using the supplied key
     * (from original documentation)
     *
     * @param type $clear String to be encrypted
     * @param type $key Key to use when encrypting
     * @return encrypted string
     */
    function _encryptString($clear, $key) {
        $key = pack("H" . strlen($key), $key);

        if ($key && $clear) {
            $td = mcrypt_module_open(MCRYPT_3DES, '', 'cbc', '');
            $bs = mcrypt_enc_get_block_size($td);
            if ((strlen($clear) % $bs) > 0) {
                $fill = str_repeat(chr(0), 8 - (strlen($clear) % $bs));
            } else {
                $fill = str_repeat(chr(0), 8);
            }
            $clear .= $fill;
            $padding = str_repeat(chr(8), 8);
            $iv = str_repeat(chr(0), mcrypt_enc_get_iv_size($td));
            mcrypt_generic_init($td, $key, $iv);
            $encrypted_data = mcrypt_generic($td, $clear . $padding);
            $cifrado = $encrypted_data;
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            return($cifrado);
        }
    }

    /**
     * Decrypts a string via 3DES using the supplied key
     * (from original documentation)
     *
     * @param string $cyphered Encrypted string
     * @param string $key Key to use when decrypting
     * @return decrypted string
     */
    function _decryptString($cyphered, $key) {
        $key = pack("H" . strlen($key), $key);

        if ($key && $cyphered) {
            $td = mcrypt_module_open(MCRYPT_3DES, '', 'cbc', '');
            $iv = str_repeat(chr(0), mcrypt_enc_get_iv_size($td));
            mcrypt_generic_init($td, $key, $iv);
            $clear_data = mdecrypt_generic($td, $cyphered);
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            $clear_data = str_replace(str_repeat(chr(8), 8), '', $clear_data);
            $clear_data = str_replace(chr(0), '', $clear_data);
            return($clear_data);
        }
    }

    /**
     * Build XML structure containing all tickets in this session
     *
     * @param array $aAllData all ticket data
     * @return array
     */
    function buildXML($aAllData) {

        $iNumBets = count($aAllData);

        $sAuditLogMsg = "Loteria Castillo - building ticket XML for $iNumBets bets";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_BUILDING_XML', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        //echo ("{$iNumBets} in bet array<br/>" );

        $i = 0;
        $sXML = "";
        foreach ($aAllData as $aData) {


            if ($i == 0) {

                $aCastilloData = $aData['lotteryid'];

                $iDate = $aData['drawdate'];

                $sAuditLogMsg = "Loteria Castillo - setting draw date {$iDate} and lottery ID {$aCastilloData['id']}";
                \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_SET_DATE_ID', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

                $iID = time();

                $sXML .= '<?xml version="1.0" encoding="UTF-8"?>';


                $sXML .= '<ticket type="' . $aCastilloData['id'] .
                        '" date="' . $iDate .
                        '" bets="' . $iNumBets .
                        '" price="' . sprintf('%0.2f', $aCastilloData['price'] * $iNumBets) . '">';

                $sXML .= '<id>' . $iID . '</id>';
            }
            $aNumbers = explode("|", $aData['numbers']);

            $sAuditLogMsg = "Loteria Castillo - numbers are {$aData['numbers']}";
            \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_NUMBERS', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

            $sXML .= '<combination>';
            foreach ($aNumbers as $iNumSeq=>$iNumber) {
                $sXML .= '<'.$this->aBallNames[$this->fk_lottery_id][$iNumSeq].'>' . sprintf('%1d', $iNumber) . '</'.$this->aBallNames[$this->fk_lottery_id][$iNumSeq].'>';
            }
            $sXML .= '</combination>';

            $i++;
        }

        $sXML .= '</ticket>';

        //echo("<br/>Ticket XML is: <br/>");
        //echo("<tt>" . htmlentities($sXML)  . "</tt><br/>");
        //echo("Now building rest of XML<br/>");
        // Build the remaining bit of XML
        $sAllXML = $this->_buildContainerXML($sXML);

        $sAuditLogMsg = $sXML;
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_XML_BUILT', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        // Return all XML built
        return $sAllXML;
    }

    /**
     * Build the remaining XML structure
     * (encodes contents and signature)
     *
     * @param string $sTicketXML XML to be encoded
     * @return string
     */
    function _buildContainerXML($sTicketXML) {
        $sAuditLogMsg = "Loteria Castillo - building container  XML";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_BUILDING_CONTAINER_XML', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

// Random key required for encryption
        $aKey = $this->_returnKey();

        $sAuditLogMsg = "Loteria Castillo - chose key {$aKey['keyO']} - {$aKey['keyH']}";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_KEY', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        // Unique ID based on microtime
        $aMicrtime = explode(" ", microtime());
        $iID = substr(date("YmdHis") . substr($aMicrtime[0], 2), 0, 20);

        // Encrypt the supplied XML using the random key
        $sContentString = base64_encode($this->_encryptString($sTicketXML, $aKey['keyH']));

        // Generate the signed signature from the encrypted xml
        $sSignatureString = $this->_generateSignature($sContentString);

        // Complete building the XML
        $sXML = '<?xml version="1.0" encoding="UTF-8"?>';
        $sXML .= '<message>';
        $sXML .= '<operation id="' . $iID . '" type="1" key="' . $aKey['keyO'] . '">';
        $sXML .= '<content>';
        $sXML .= $sContentString;
        $sXML .= '</content>';
        $sXML .= '</operation>';
        $sXML .= '<signature>';
        $sXML .= base64_encode($sSignatureString);
        $sXML .= '</signature>';
        $sXML .= '</message>';

        $sAuditLogMsg = $sXML;
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_CONTAINER_XML_BUILT', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        return $sXML;
    }

    /**
     * Decode received XML message from array
     *
     * @param array $aConvertedXML
     */
    function decodeMessageFromCURL($aConvertedXML) {
        $sAuditLogMsg = "Loteria Castillo - decoding message from CURL";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_DECODING_CURL_MESSSAGE', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        $sMessage = $aConvertedXML['operation']['content'];

        // Retrieve the correct hexadecimal key
        $aKey = $this->_returnKey($aConvertedXML['operation']['@attributes']['key']);

        $sAuditLogMsg = "Loteria Castillo - return key {$aKey['keyO']} - {$aKey['keyH']}";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_RETURN_KEY', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);


        $sDecrypted = $this->_decryptString(base64_decode($sMessage), $aKey['keyH']);

        // Decrypted message will also be XML, so we need to further convert
        $aDecryptedMessage = $this->_convertXMLToArray($sDecrypted);

        //print_d($sDecryptedMessage);

        // Write response to session
        $this->oTicketSession->writeResponseFromEngineToSession($sDecrypted);


        $sAuditLogMsg = "Loteria Castillo - returning decoded message";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_RETURN_DECODED_CURL_MESSSAGE', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        return ($aDecryptedMessage);
    }

    /**
     * Generates a signature based on supplied data and preshared key
     *
     * @param string $sData data to use in generating signature
     * @return string
     */
    function _generateSignature($sData) {

        $sAuditLogMsg = "Loteria Castillo - generating signature";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_TICKETS_GEN_SIGNATURE', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        return sha1(base64_decode($sData) . $this->oConfigData->sPreSharedKey);
    }

    /**
     * Performs a call via CURL
     *
     * @param string $aData data to be sent
     * @param object $oTicketSession ticket buying session
     * @return string
     */
    function curl_call($sData, $oTicketSession) {

        $sAuditLogMsg = "Loteria Castillo - starting CURL call";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_CURL_CALL_START', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->oConfigData->sEndPoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sData);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            print_d(curl_error($ch));

            $sAuditLogMsg = "CURL call errors " . print_r(curl_error($ch), 1);
            \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_CURL_CALL_ERRORS', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);
        }
        curl_close($ch);

        $sAuditLogMsg = "Loteria Castillo - CURL call ended";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_CURL_CALL_ENDED', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        return $response;
    }

    /**
     * Return the correct castillo lottery id and lottery price
     * from the supplied 'system' id
     *
     * @param integer $iLotteryId the supplied lottery id
     * @return integer
     */
    function _getCastilloLotteryIdAndPriceFromSystemId($iLotteryId) {
        $aCastilloData = array();

        // These should really be changed to constants!
        switch ($iLotteryId) {

            case 2:     // EuroMillions - single day
                $aCastilloData['id'] = 6;
                $aCastilloData['price'] = "2.00";
                break;

            case 7:     // El Gordo de La Primitiva
                $aCastilloData['id'] = 3;
                $aCastilloData['price'] = "1.00";
                break;

            case 16:    // La Primitiva
                $aCastilloData['id'] = 1;
                $aCastilloData['price'] = "1.00";
                break;

            case 17:    // Bonolotto
                $aCastilloData['id'] = 4;
                $aCastilloData['price'] = "1.00";
                break;

            default:
                $aCastilloData['id'] = 0;
                $aCastilloData['price'] = "0.00";
        }

        $sAuditLogMsg = "Loteria Castillo - Castillo price and lottery data for ML id {$iLotteryId} is {$aCastilloData['price']} | {$aCastilloData['id']}";
        \AuditLog::LogItem($sAuditLogMsg, 'CASTILLO_LOTTERY_PRICE_DATA', 'ticket_purchase_sessions',$this->$this->oTicketSession->id);

        return $aCastilloData;
    }

    /**
     * Formats a draw date from the database in yymmdd format, for sending
     * over in the XML
     *
     * @param date $sDate the draw date from the database
     * @return null
     */
    function _formatCastilloLotteryDate($sDate) {

        return date("ymd", strtotime($sDate));
    }

    /**
     * Returns the correct hexadecimal key from the supplied integer
     * If no integer supplied, then generate one automatically
     * before returning
     *
     * @return integer
     */
    function _returnKey($iKey = false) {
        $aReturn = "";

        if ($iKey === false) {
            $iKey = rand(0, 9);
        }

        switch ($iKey) {
            case 0:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000000";
                break;

            case 1:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000001";
                break;

            case 2:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000002";
                break;

            case 3:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000003";
                break;

            case 4:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000004";
                break;

            case 5:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000005";
                break;

            case 6:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000006";
                break;

            case 7:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000007";
                break;

            case 8:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000008";
                break;

            case 9:
                $aReturn['keyH'] = "000000000000000000000000000000000000000000000009";
                break;

            default :
                $aReturn['keyH'] = "";
        }
        $aReturn['keyO'] = $iKey;
        return $aReturn;
    }

    /**
     * Converts an XML string to an array
     *
     * @param string $sXMLString XML to be converted
     * @return array
     */
    function _convertXMLToArray($sXMLString) {
        $xml = simplexml_load_string($sXMLString);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        return $array;
    }

}
