<?php

namespace LL\TicketEngines;

class LoteriaCastilloInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\LoteriaCastilloInstaller', 'installTicketModule', 'Install Loteria Castillo module');
        return $aJobs;
    }

    static function installTicketModule(){
        self::installModule('Loteria Castillo',1);
        self::installHandler('\\LL\\TicketEngines\\LoteriaCastillo', 'Loteria Castillo', 'TicketEngine',1);
    }
}
