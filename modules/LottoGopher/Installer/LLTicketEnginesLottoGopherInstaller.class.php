<?php

namespace LL\TicketEngines;

class LottoGopherInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\LottoGopherInstaller', 'installTicketModule', 'Install Lotto Gopher module');
        return $aJobs;
    }

    static function installTicketModule(){
        self::installModule('LottoGopher',1);
        self::installHandler('\\LottoGopher', 'Lotto Gopher', 'TicketEngine',1);
    }
}
