<?php

class German_scraperApi extends scraper{
    var $aURLS=array();
    var $sBaseURL='http://www.europeanlotteryguild.com/lottery_results/german_lotto_results/draw_history?results_date=';

    function __construct() {
        set_time_limit(0);
        $this->oLottery=new GermanLotto();
    }

    function scrape(){
        $this->scrapeDates();
        $this->scrapeResultPages();
    }

    function scrapeResultPages(){
        foreach($this->aURLS as $sURL){
            $sURL='http://www.europeanlotteryguild.com'.$sURL;
            $sData=$this->getData($sURL);

            //Date comes from the URL
            $aURL=explode('?date=',$sURL);
            $sDate=$aURL[1];

            //Find the draw to update
            $iDrawId=$this->locateDraw($this->oLottery->id, $sDate);

            //$oResult=new \LL\Results\LotteryResult($iDrawId);

            //$this->getBalls($sData,$oResult);

            //$oResult->save();

            //die();
        }

    }

    function getBalls($sData,$oResult){
        $oDoc=new DOMDocument();
        $oDoc->loadHTML($sData);

        $sXPath='//div[@class="info-box results-info"]/div/dl[1]/dd';

        $aBalls=$this->runXPath($oDoc, $sXPath);
        foreach($aBalls as $oBall){
            $oResult->oDrawBalls->addBall($oBall->nodeValue);
        }
    }

    function getJackpot($sData){

    }

    function getResults($sData){

    }

    function scrapeDates(){
        error_reporting(E_ERROR);
        for($iYear=2010;$iYear<=2014;$iYear++){
            for($iMonth=1;$iMonth<=12;$iMonth++){

                if($iMonth>5 && $iYear==2014) continue;

                $sURL=$this->sBaseURL.$iYear.'-'.$iMonth.'-01';
                $sData=$this->getData($sURL);


                $oDoc=new DOMDocument();
                $oDoc->loadHTML($sData);

                $sXPath='//div[@id="results-listing"]/a';

                $aLinks=$this->runXPath($oDoc, $sXPath);
                foreach($aLinks as $oLink){
                    $this->aURLS[]=$oLink->getAttribute('href');
                }
            }
        }
    }

}
