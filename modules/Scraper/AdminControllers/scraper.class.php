<?php

class scraper extends \LL\AjaxController{
    var $oLottery=null;

    function getDraw(){
        $sURL='http://www.thelotter.com/lottery-results/usa-powerball';

        $sData=$this->getData($sURL);

        $oDoc=new DOMDocument();
        $oDoc->loadHTML($sData);

        /**
         * Get the balls
         */
        $sXPath='//div[@class="results_balls"]//td//div[@class="ballTexticon"]';
        $aBallElements=$this->runXPath($oDoc, $sXPath);
        foreach($aBallElements as $oBallElement){
            $aBalls[]=$oBallElement->nodeValue;
        }
        print_r($aBalls);
        /**
         * Get the prize breakdown
         */

        $aSplitForPrize=explode('data : ',$sData);
        $aSplitForPrize=explode(',rowNum',$aSplitForPrize[1]);

        $sJson=$aSplitForPrize[0];
        $sJson=str_replace('"',"{QUOTE}",$sJson);
        $sJson=str_replace("'",'"',$sJson);
        $sJson=str_replace("{QUOTE}","'",$sJson);
        $sValidJson = preg_replace("/(\n[\t ]*)([^\t ]+):/", "$1\"$2\":", $sJson);
        $sValidJson=str_replace('""','"',$sValidJson);
        $sValidJson=str_replace("\",\r\n{",',{',$sValidJson);
        $sValidJson=str_replace('"{','{',$sValidJson);

        $oResults=json_decode($sValidJson);
/*
        foreach($oResults as $oResult){
            $resultsRow=array(
                'prize_per_winner'=>$oResult->LocalWinningAmount,
                'total_winners'=>$oResult->
            )
        }
*/

        /**
         * Get the date
         */

        $sXPath='//td[@id="tdMainLeftSite"]//select';
        $aDateElements=$this->runXPath($oDoc, $sXPath);

        foreach($aDateElements as $oDateElement){
            $aDateParts=explode(' | ',$oDateElement->nodeValue);
            $sDate=date('Y-m-d',strtotime($aDateParts[1]));
        }

        echo $sDate;

        $iLoteryId=4; //Powerball
        //Find out our draw number
    }

    function getData($sURL){
        $sFileName=base64_encode($sURL);
        $sCachePath='../system/modules/Scraper/Cache/';
        if(!file_exists($sCachePath.$sFileName)){
            $sData=file_get_contents($sURL);
            file_put_contents($sCachePath.$sFileName, $sData);
            return $sData;
        }
        return file_get_contents($sCachePath.$sFileName);
    }

    function runXPath($oDoc,$sXPath){
        $xpath = new DOMXpath($oDoc);
        $elements = $xpath->query($sXPath);
        return $elements;
    }

    function getDrawPages(){

    }

    function locateDraw($iLotteryID,$dDate){
        $sSQL='SELECT id FROM lottery_draws WHERE fk_lottery_id='.$iLotteryID.' AND date(fk_lottery_date)=\''.$dDate.'\'';
        $iLotteryDrawID=\DAL::executeGetOne($sSQL);
        if($iLotteryDrawID==0){
            //Create a draw
            $aData=array('fk_lottery_id'=>$iLotteryID,'fk_lottery_date'=>$dDate.' '.$this->oLottery->sLottoDrawTime);
            $iLotteryDrawID=\DAL::Insert('lottery_draws', $aData,true);
        }
        return $iLotteryDrawID;
    }
}