<?php

/**
 * Payment gateway for PayFrex
 *
 * To configure for other hosts adapt this accordingly:
 *
 * Config::$config->payfrex->returnURL='http://local.maxlotto.com/system/controls/paymentGateway.php?g=payfrex';
 * Config::$config->payfrex->sErrorURL='http://local.maxlotto.com/errorFunds.php';
 * Config::$config->payfrex->iPayFrexMerchantID='XXXX';
 * Config::$config->payfrex->sPayFrexPasswd='YYYY';
 *
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class Payfrex extends PaymentGateways
{

    //protected $sReturnURL = "http://payfrex:payfrex@dev.maxlotto.com/system/controls/paymentGateway.php?g=payfrex";
    //protected $sErrorURL = "http://payfrex:payfrexe@dev.maxlotto.com/errorFunds.php";

    //protected $sReturnURL = "http://dev.maxlotto.com/system/controls/paymentGateway.php?g=payfrex";
    //protected $sErrorURL = "http://dev.maxlotto.com/errorFunds.php";

    protected $sReturnURL;  // Set in constructor as dependent on host
    protected $sErrorURL;  // Set in constructor as dependent on host

    protected $iPayFrexMerchantID = "1008";
    protected $sPayFrexPasswd = "C1tYR04DCh35t3R";

    /**
     * @var paymentType - used to toggle between in and out (withdrawals)
     */
    private $paymentType = '';

    public function __construct() {

        $this->sReturnURL = "http://" . $_SERVER['HTTP_HOST'] . "/system/controls/paymentGateway.php?g=payfrex";
        $this->sErrorURL = "http://" . $_SERVER['HTTP_HOST'] . "/errorFunds.php";

        //protected $sReturnURL = "https://www.lovelotto.com/system/controls/paymentGateway.php?g=payfrex";
        if(isset(\Config::$config->payfrex->returnURL)) $this->sReturnURL = \Config::$config->payfrex->returnURL;
        if(isset(\Config::$config->payfrex->sErrorURL)) $this->sErrorURL = \Config::$config->payfrex->sErrorURL;
        if(isset(\Config::$config->payfrex->iPayFrexMerchantID)) $this->iPayFrexMerchantID = \Config::$config->payfrex->iPayFrexMerchantID;
        if(isset(\Config::$config->payfrex->sPayFrexPasswd)) $this->sPayFrexPasswd = \Config::$config->payfrex->sPayFrexPasswd;

        parent::__construct(PaymentGateways::PAYFREX);
    }

    public function setPaymentType($type) {
        $this->paymentType = $type;
    }

    public function getCharges() {
        return ($this->fAmount * 0.00) + 0.00;
    }

    public function ProcessPayment(&$oPage, &$oMember) {
        try {
            $sRedirectTo = $this->prepareTransaction($oMember);

            if ($sRedirectTo !== false) {
                $oPage->saveSessionObjects();
                return $sRedirectTo;
            } else {
                // Raise a message in the audit log to say Payfrex has died.
                $sAuditLogMsg = "Payfrex - redirect to is false";
                AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_REDIRECT_TO_FALSE', 'transactions', 0);

                $this->RedirectToPage($this->sErrorURL);
            }
        } catch (Exception $e) {

            // Raise a message in the audit log to say Payfrex has died.
            $sAuditLogMsg = "Payfrex - no redirect URL found";
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_NO_REDIRECT_URL', 'transactions', 0);

            // Show message to customer
            $errMesg = $e->getMessage();

            /**
             * @todo, Move into its own view
             */
            echo '<!-- Fonts -->
        	<link href="http://fonts.googleapis.com/css?family=Oswald:400,300%7CShare+Tech+Mono%7CPaytone+One%7CSatisfy%7COpen+Sans:400italic,600italic,700italic,800italic,400,600,700,800" rel="stylesheet" type="text/css">

            <!-- Modernizer -->
        	<script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>

        	<!-- Font Awesome -->
        	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
            <script src="//cdn.jsdelivr.net/jquery/1.11.1/jquery.min.js"></script>

            <!-- Page styles -->
            <link rel="stylesheet" href="'.$base.'static/foundation/css/app.css" />';

            die('<body style="background: none"><p style="font-size: 0.8rem; color: #C80000; margin-bottom: 0;"><i class="fa fa-exclamation-triangle"></i> '.VCMS::get('Payment.ErrorMessage[Message]', array('Message' => $errMesg)).'</p></body>');

        }

    }


    public function prepareTransaction(&$oMember) {

        // Unique store key to be passed as part of the call to Payfrex on the status URL
        $sStoreKey = LL\PaymentProcessing\Status::generateStoreKey();

        $aMemberDetails = $oMember->getMemberDetails();

        $aParams = array();

        function parse_number($number, $dec_point=null) {
            if(empty($dec_point)) {
                $locale = localeconv();
                $dec_point = $locale['decimal_point'];
            }
            return floatval(str_replace($dec_point, '.', preg_replace('/[^\d'.preg_quote($dec_point).']/', '', $number)));
        }


        // Country ID
        $iCountryId = Config::$config->iLanguageId;

        // Language for payment screen
        // We'll defoult to English here
        switch ($iCountryId)
        {
            case 3:
                $sCountry = "de";
            break;

            default:
                $sCountry = "en";

        }


        //$this->fAmount = parse_number($this->fAmount);
        // Run $this->fAmount through the reverse number formatter
        $this->fAmount = reverseNumberFormat($this->fAmount);

        $aParams['amount'] = abs(reverseNumberFormat($this->fAmount + $this->getCharges()));
        $aParams['currency'] = "EUR";
        $aParams['country'] = $oMember->getMemberCountryCode();
        $aParams['chCountry'] = $oMember->getMemberCountryCode();
        $aParams['customerId'] = $oMember->getMemberID();
        $aParams['merchantId'] = $this->iPayFrexMerchantID;
        $aParams['merchantTransactionId'] = $this->iTransactionID;
        $aParams['language'] = $sCountry;
        $aParams['topLogo'] = "";

        $sAuditLogMsg = "Payfrex Amount is {$aParams['amount']}";
        AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_AMOUNT_VALUE', 'transactions', 0);

        /**
         * If session[checkout] is set, we want to proceed to the 'review order' screen again
         * once we have some funds.
         */
        //if(empty($_SESSION['checkout'])) {

        // Set up redirects depending on type of transaction
        // Both succesful and error transactions go to the same URL
        // We also pass the transaction ID as part of the URL,
        // this is so we can capture any messages if there are any errors
        // although first we add random characters to a base 64 encoding of it
        $sTransactionIDReturn = 'kw' . base64_encode($this->iTransactionID) . "np";

        if($this->paymentType == 'withdrawal') {
            $aParams['successURL'] = "http://" . $_SERVER['HTTP_HOST'] . '/withdrawalreceipt.php?t=' . $sTransactionIDReturn;
            $aParams['errorURL'] = "http://" . $_SERVER['HTTP_HOST'] . '/withdraw.php?t=' . $sTransactionIDReturn;
            //$aParams['errorURL'] = $this->sErrorURL . '?t=' . $this->iTransactionID;
        }
        else
        {
            $aParams['successURL'] = "http://" . $_SERVER['HTTP_HOST'] . '/' . Navigation::getPageURL($this->iConfirmPageID) . '?t=' . $sTransactionIDReturn;
            //$aParams['errorURL'] = "http://" . $_SERVER['SERVER_NAME'] . '/' . Navigation::getPageURL($this->iConfirmPageID) . '?t=' . $sTransactionIDReturn;
            $aParams['errorURL'] = "http://" . $_SERVER['HTTP_HOST'] . '/addFunds?t=' . $sTransactionIDReturn;
            //$aParams['errorURL'] = $this->sErrorURL . '?t=' . $this->iTransactionID;
        }
        //} else {
        //        $aParams['successURL'] = 'http://'.$_SERVER['HTTP_HOST'].'/checkout.php';
        //}

        // Return URL with added status key
        $aParams['statusURL'] = $this->sReturnURL . '&sk=' . $sStoreKey;

        $aParams['cancelURL'] = "http://" . $_SERVER['HTTP_HOST'] . '/' . Navigation::getPageURL($this->iCancelPageID);

        $aParams['paymentSolution'] = $this->paymentType;

        // returnTo parameter is used to help with error messages
        if($this->paymentType == 'withdrawal') {
            $aParams['operationType'] = 'credit';
            $aParams['returnTo'] = 'withdraw';
        }
        else
        {
            $aParams['returnTo'] = 'addfunds';
        }
        $aParams['test'] = $this->bTestGateway;

        $aParams['customerEmail'] = $oMember->getUsername();
        $aParams['firstname'] = $aMemberDetails['firstname'];
        $aParams['lastname'] = $aMemberDetails['lastname'];
        $aParams['addressLine1'] = $aMemberDetails['address1'];
        $aParams['addressLine2'] = $aMemberDetails['address2'];
        $aParams['city'] = $aMemberDetails['city'];
        $aParams['postcode'] = $aMemberDetails['zip'];
        $aParams['telephone'] = $aMemberDetails['contact'];

        $aParams['merchantTransactionRef'] = $this->sTransactionReference;
        $aParams['merchantCustomerId'] = $oMember->getMemberID();

        // Build custom parameter string
        $sMerchantParamsStr = 'merchantTransactionRef:' . $this->sTransactionReference . ';' .
                              'merchantCustomerId:' . $oMember->getMemberID() . ';' .
                              'returnTo:' . $aParams['returnTo'];

        $aParams['merchantParams'] = $sMerchantParamsStr;

        // Used in Moneybookers module - needed here?
        $aParams['detail1_description'] = WEBSITETITLE . " Member Funds";
        $aParams['detail1_text'] = $oMember->getFullName();
        $aParams['detail2_description'] = "Member Account Topup";
        $aParams['detail2_text'] = "EUR " . number_format($this->fAmount, 2, '.', ',');
        $aParams['detail3_description'] = "Processing Fees";
        $aParams['detail3_text'] = "EUR " . number_format($this->getCharges(), 2, '.', ',');


        $sMD5Passwd = hash('md5', $this->sPayFrexPasswd);
        $sParamString = $this->prepareParameters($aParams);
        $sSHA256ParamsIntegrityCheck = hash('sha256', $sParamString);

        $sEncryptedValue = $this->encrypt($sParamString, $sMD5Passwd);
        //URL ENCODED IS NEEDED BEFORE POSTING
        $sEncoded = urlencode($sEncryptedValue);

        $sVarsToPayFrex = 'merchantId=' . $this->iPayFrexMerchantID .
                          '&encrypted=' . $sEncoded .
                          '&integrityCheck=' . $sSHA256ParamsIntegrityCheck;

/*
        print_d($aParams);

        echo ("MD5 passwd = {$sMD5Passwd} (MD5 of {$this->sPayFrexPasswd})<br>");
        echo ("Param string is {$sParamString} <br>");
        echo ("Integrity check is {$sSHA256ParamsIntegrityCheck}<br>");
		echo ("Encrypted value is {$sEncryptedValue}<br>");
        echo ("Encoded of above is value is {$sEncoded}<br>");
        echo ("Vars to Payfrex is {$sVarsToPayFrex}");

*/
        $sReturn= $this->curl_call($sVarsToPayFrex);

/*
        echo("<br><br> Return from PayFrex is $sReturn");
        die();

*/
        if ($sReturn === "error") {
            throw new Exception("Unable to connect to Payfrex to get redirect URL");
        }

        if ($sReturn === false) {
            throw new Exception("Problem connecting to Payfrex to get redirect URL");
        }

        return $sReturn;

    }

    private function prepareParameters($aParams)
    {
        // Create a single query string from the key/val pairs of the supplied array

        $sParamString = "";

        foreach ($aParams as $key=>$val)
        {
            $sParamString .= $key . "=" . $val . "&";
        }


        // Remove the extra ampersand at the end of the string

        return substr($sParamString, 0, -1);

    }

    public function ConfirmPayment($sDataFromPayFrex) {
        $sAuditLogMsg = "About to Parse Payfrex XML";
        AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_BEFORE_PARSE_XML', 'transactions', 0);

        // Parse the Payfrex XML returned
        $aParsedXML = $this->parsePayFrexXML($sDataFromPayFrex);

        // Get the type of transaction (credit/debit)
        $sTransactionType = $this->getTransactionTypeFromPayfrexXML($aParsedXML);

        if ($sTransactionType == 'CREDIT')
        {
            // This is a credit, so handle accordingly
            $this->confirmAsCredit($aParsedXML, $sDataFromPayFrex);

        }
        else
        {
            // Moved out into seperate function
            $this->confirmAsDebit($aParsedXML);
        }
    }


    /**
     * Confirms transaction as debit (deposit)
     * @param type $aParsedXML parsed XML
     */
    protected function confirmAsDebit($aParsedXML)
    {

        //print_d($aParsedXML);
        $sAuditLogMsg = "This is a DEBIT (i.e. deposit) transaction";
        AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_IS_DEBIT', 'transactions', 0);


        $aTransaction = $this->searchTransaction($aParsedXML['operations']['operation']['merchantTransactionId'],
                                                $aParsedXML['fromPayfrex']['merchantcustomerid'],
                                                $aParsedXML['fromPayfrex']['merchanttransactionref']);
        //die();

        if (is_array($aTransaction)) {

           $sAuditLogMsg = "Found transaction |" . $aParsedXML['fromPayfrex']['merchantcustomerid'] .
                                               "|" . $aParsedXML['operations']['operation']['merchantTransactionId'] . "|" .
                                               $aParsedXML['fromPayfrex']['merchanttransactionref'];
           AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_FOUND', 'transactions', 0);


           // Need to check the success of the actual operation, not the request!!!
           if ($aParsedXML['operations']['operation']["status"] == "SUCCESS") {

               // Change the payment gateway ID depending on what method was used
               $sPaymentSolution = strtolower($aParsedXML['operations']['operation']['paymentSolution']);
               switch ($sPaymentSolution)
               {
                   case "neteller" :
                       $this->iPaymentGatewayID = self::NETELLER;
                   break;

                   case "ukash" :
                       $this->iPaymentGatewayID = self::UKASH;
                   break;

                   default :
                       $this->iPaymentGatewayID = self::PAYFREX;
                   break;
               }

               $sAuditLogMsg = "Transaction successful -  ID " . $aTransaction['transaction_id'] . " - Gateway " . $aParsedXML['operations']['operation']['paymentSolution'];
               AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_SUCCESS', 'transactions', 0);

               $this->iTransactionID = $aTransaction['transaction_id'];
               $this->fAmount = $aTransaction['amount'];
               $this->sTransactionReference = $aTransaction['bookingreference'];

               // CHange gateway reference here to whatever comes back from Payfrex in the Paysol field
               $this->sGatewayReference = 'PS' . $aParsedXML['operations']['operation']['paySolTransactionId'];

               if ($aTransaction['confirmed'] == 0) {
                   // Should be OK to confirm the transaction now
                   $this->confirmTransaction();

                   // Don't need to update the balance here - handled in confirmTransaction above
                   //$this->updateMemberBalance($aTransaction['member_id']);

                   $this->sendAddFundsReceipt($aTransaction['member_id']);
               } elseif ($aTransaction['status'] == 0) {
                   $this->confirmTransaction(false, 2);

                   // Redirect to error page and pass page accessed from and transaction ID
                   $this->RedirectToPage($this->sErrorURL . '?p=' . $aParsedXML['fromPayfrex']['returnTo'] . '&t=' . $aParsedXML['operations']['operation']['merchantTransactionId']);
               }
           } else {

               $sAuditLogMsg = "Transaction NOT successful. Cust ID: " . $aParsedXML['fromPayfrex']['merchantcustomerid'] .
                                                   "| Trans ID: " . $aParsedXML['operations']['operation']['merchantTransactionId'] .
                                                   "| Trans Ref: " .  $aParsedXML['fromPayfrex']['merchanttransactionref'];
               AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_FAIL', 'transactions', 0);

               // Redirect to error page and pass page accessed from
               $this->RedirectToPage($this->sErrorURL . '?p=' . $aParsedXML['fromPayfrex']['returnTo'] . '&t=' . $aParsedXML['operations']['operation']['merchantTransactionId']);

               /*
               $oEmail = new Mailer();
               $oEmail->shortHeader();
               $oEmail->setSubject("Moneybookers Payment Failed");
               $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
               $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
               $oEmail->sendEmail("<h2>PayFrex Payment Failed.</h2><pre>" . print_r($aParsedXML, true) . "</pre>");
               */
           }
        }
        else
        {

           $sAuditLogMsg = "Transaction NOT FOUND. Cust ID: " . $aParsedXML['fromPayfrex']['merchantcustomerid'] .
                                   "| Trans ID: " . $aParsedXML['operations']['operation']['merchantTransactionId'] .
                                   "| Trans Ref: " .  $aParsedXML['fromPayfrex']['merchanttransactionref'];
           AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_NOT_FOUND', 'transactions', 0);


           /*
           $oEmail = new Mailer();
           $oEmail->shortHeader();
           $oEmail->setSubject("Moneybookers Payment Failed");
           $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
           $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
           $oEmail->sendEmail("<h2>PayFrex Payment Failed.</h2><pre>" . print_r($aParsedXML, true) . "</pre>");
           */

           // Redirect to error page and pass page accessed from and transaction id
           $this->RedirectToPage($this->sErrorURL . '?p=' . $aParsedXML['fromPayfrex']['returnTo'] . '&t=' . $aParsedXML['operations']['operation']['merchantTransactionId']);

           die ("Problem accessing transaction in database.");
        }

    }

    /**
     * Processes a credit (withdrawal)
     *
     * @param array $aParsedXML array of parsed XML from payfrex
     * @param array $sXMLString xml string from Payfrex
     */
    protected function confirmAsCredit($aParsedXML, $sXMLString)
    {
        // Check whether the transactions have been split
        $aTransactionsFromPayfrex = $this->extractTransactionInfo($aParsedXML);

        $sAuditLogMsg = "This is a CREDIT (i.e. withdrawal) transaction";
        AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_IS_CREDIT_WITHDRAWAL', 'transactions', 0);


        // Search for the original transaction
        if (!array_key_exists('fromPayfrex', $aParsedXML))
        {

            // Dealing only with an update of a split transaction, where we don't have customer or transaction ref
           // so just search on transaction id and then get all linked transactions
           //$aTransaction = $this->searchTransaction($aTransactionsFromPayfrex['master']['merchantTransactionId'], null,null);

            $sAuditLogMsg = "Confirming split transactions";
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_CONFIRMING_SPLIT_TRANSACTIONS', 'transactions', 0);

            $this->confirmSplitTransactions($aTransactionsFromPayfrex);


        }
        else
        {
            // Payfrex is returning the original transaction and customer ID - initial withdrawal transaction
            // so search for it in the database
            $aTransaction = $this->searchTransaction($aTransactionsFromPayfrex['master']['merchantTransactionId'], $aParsedXML['fromPayfrex']['merchantcustomerid'], $aParsedXML['fromPayfrex']['merchanttransactionref']);

            // Found the original transaction
            if (is_array($aTransaction)) {

                $sAuditLogMsg = "Found original transaction | " . $aTransactionsFromPayfrex['master']['merchantTransactionId'];
                AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_ORIGINAL_TRANSACTION_FOUND', 'transactions', 0);

                // Pending or success are valid statuses here
                if ($aParsedXML["status"] == "PENDING" || $aParsedXML["status"] == "SUCCESS") {

                    $sAuditLogMsg = "Transaction successful -  ID " . $aTransaction['transaction_id'];
                    AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_SUCCESS', 'transactions', 0);

                    // Transaction will always have been split at this stage
                    $sAuditLogMsg = "Transaction has been split into " . count($aTransactionsFromPayfrex['new']) . " seperate transactions";
                    AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_SPLIT', 'transactions', 0);

                    // Original transaction details
                    $this->iTransactionID = $aTransaction['transaction_id'];
                    $this->fAmount = $aTransactionsFromPayfrex['master']['amount'];
                    $this->sTransactionReference = $aTransaction['bookingreference'];

                    // Change the payment gateway ID depending on what method was used
                    // (at this point, it should just be withdrawal)
                    $sPaymentSolution = strtolower($aTransactionsFromPayfrex['master']['paymentSolution']);

                    // Set gateway ID as generic withdrawal
                    $this->iPaymentGatewayID = self::WITHDRAWAL;

                    // CHange gateway reference here to whatever comes back from Payfrex in the payfrex transaction field
                    // PW = Payment withdrawal
                    $this->sGatewayReference = 'PW' . $aTransactionsFromPayfrex['master']['payFrexTransactionId'];

                    if ($aTransaction['confirmed'] == 0) {
                        $sAuditLogMsg = "Confirming original transaction";
                        AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_CONFIRM_ORIGINAL_TRANSACTION', 'transactions', 0);

                        // Confirm the original transaction
                        $this->confirmTransaction();

                        // Add the new transactions
                        foreach ($aTransactionsFromPayfrex['new'] as $aNewTransaction)
                        {
                            $iPaymentGatewayID = '';
                            // Change the payment gateway ID depending on what method was used
                            $sPaymentSolution = strtolower($aNewTransaction['paymentSolution']);
                            switch ($sPaymentSolution)
                            {
                                case "neteller" :
                                    $iPaymentGatewayID = self::NETELLER_WITHDRAWAL;
                                break;

                                case "ukash" :
                                    $iPaymentGatewayID = self::UKASH_WITHDRAWAL;
                                break;

                                default :
                                    $iPaymentGatewayID = self::CC_WITHDRAWAL;
                                break;
                            }

                            // This split transaction has its own reference, however we still need to refer back to the original transaction which this is a part of
                            // so we make a combination value of the original transaction id, and append the new transaction id to the end.
                            $sGatewayRef = 'PWS' . $aNewTransaction['originalPayFrexTransactionId'] . "-" . $aNewTransaction['payFrexTransactionId'];

                            // Add the transaction, initially unconfirmed
                            $iNewTransactionID = $this->addSplitTransaction($aTransaction['member_id'],
                                                                            $aNewTransaction['amount'],
                                                                            $sGatewayRef,
                                                                            $iPaymentGatewayID,
                                                                            $sXMLString);

                            $sAuditLogMsg = "Adding new split transaction {$iNewTransactionID} (Payfrex ID {$aNewTransaction['payFrexTransactionId']}, original {$aNewTransaction['originalPayFrexTransactionId']} , amount {$aNewTransaction['amount']}";
                            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_UPDATE_ORIG_TRANSACTION', 'transactions', 0);
                        }

                        // Update the member balance now that all the split transactions are done
                        $this->updateMemberBalance($aTransaction['member_id']);

                        // TODO: Withdrawal receipt!


                    } elseif ($aTransaction['status'] == 0) {
                        $this->confirmTransaction(false, 2);

                        // Redirect to error page and pass page accessed from and transaction id
                        $this->RedirectToPage($this->sErrorURL . '?p=' . $aParsedXML['fromPayfrex']['returnTo'] . '&t=' . $aTransactionsFromPayfrex['master']['merchantTransactionId']);
                    }

                } else {

                    // Transaction has not been successful

                    $sAuditLogMsg = "Transaction NOT successful. Trans ID: " . $aParsedXML['operations']['operation']['merchantTransactionId']
                                    . "| Cust ID: " . $aParsedXML['fromPayfrex']['merchantcustomerid'] .
                                    "| Trans Ref: " . $aParsedXML['fromPayfrex']['merchanttransactionref'];
                    AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_FAIL', 'transactions', 0);

                   // Redirect to error page, need to pass message too
                   $this->RedirectToPage($this->sErrorURL . '?p=' . $aParsedXML['fromPayfrex']['returnTo'] . '&t=' . $aTransactionsFromPayfrex['master']['merchantTransactionId']);


                    /*
                    $oEmail = new Mailer();
                    $oEmail->shortHeader();
                    $oEmail->setSubject("Moneybookers Payment Failed");
                    $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
                    $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
                    $oEmail->sendEmail("<h2>PayFrex Payment Failed.</h2><pre>" . print_r($aParsedXML, true) . "</pre>");
                     *
                     */
                }
            } else {
                // Transaction not found in the database
                $sAuditLogMsg = "Transaction NOT FOUND. Trans ID: " . $aTransactionsFromPayfrex['master']['merchantTransactionId'];
                        //. "| Cust ID: " . $aParsedXML['fromPayfrex']['merchantcustomerid'] .
                        //"| Trans Ref: " . $aParsedXML['fromPayfrex']['merchanttransactionref'];
                AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_NOT_FOUND', 'transactions', 0);


                /*
                $oEmail = new Mailer();
                $oEmail->shortHeader();
                $oEmail->setSubject("Moneybookers Payment Failed");
                $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->sendEmail("<h2>PayFrex Payment Failed.</h2><pre>" . print_r($aParsedXML, true) . "</pre>");
                */

                // Redirect to error page and pass page accessed from
                $this->RedirectToPage($this->sErrorURL . '?p=' . $aParsedXML['fromPayfrex']['returnTo'] . '&t=' . $aTransactionsFromPayfrex['master']['merchantTransactionId']);


                die("Problem accessing transaction in database.");
            }
        }
    }



    /**
     * Confirm all split transactions from the original transactions
     * @param type $aTransactionsFromPayfrex
     */
    protected function confirmSplitTransactions($aTransactionsFromPayfrex)
    {

        // Find original transaction
        $iOriginalTransactionID = $aTransactionsFromPayfrex['all'][0]['merchantTransactionId'];
        $aOriginalTransaction = $this->searchTransaction($iOriginalTransactionID, null,null);

        // Original transaction reference will be a string which begins "PW"
        // and then contains the original payfrex transaction number
        // so we need to get that
        $iOrigTransRef = substr($aOriginalTransaction['gatewayreference'], 2);

        // Now get all matching split transactions that contain that reference
        // will be in the format PWS{$originaltransaction}
        // so can use a LIKE command here
        $sSQL = "SELECT t.amount as amount,
                        t.fk_status_id as status_id,
                        t.confirmed as confirmed,
                        t.fk_member_id as member_id,
                        t.transaction_id as transaction_id,
                        t.bookingreference as bookingreference,
                        t.gatewayreference as gatewayreference
                FROM transactions t
                WHERE t.gatewayreference LIKE 'PWS{$iOrigTransRef}%'";

        $aSplitTransactions = DAL::executeQuery($sSQL);

        $sAuditLogMsg = $sSQL;
        AuditLog::LogItem("WHERE t.gatewayreference LIKE 'PWS{$iOrigTransRef}%'", 'PAYFREX_SPLIT_TRANSACTIONS_SQL', 'transactions', 0);

        // Do we have all the split transactins?
        if ($aSplitTransactions)
        {

            $sAuditLogMsg = "Found split transactions from orig transaction | " . $iOriginalTransactionID;
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_SPLIT_TRANSACTIONS_FOUND', 'transactions', 0);

            // Go through the transactions returned from the database
            foreach ($aSplitTransactions as $aSplitTransaction)
            {

                // Using a do/while loop here
                $bFound = false;
                do
                {
                    // Go through the transactinos from payfrex
                    foreach ($aTransactionsFromPayfrex['all'] as $aPayfrexTransaction)
                    {
                        // Cnstruct the gateway reference string
                        $sTransRef = "PWS".$iOrigTransRef."-".$aPayfrexTransaction['payFrexTransactionId'];

                        // We've found the correct transaction, so go no further
                        if ($aSplitTransaction['gatewayreference'] == $sTransRef) {
                            $bFound = true;

                            // Transaction details for this particular transaction
                            $this->iTransactionID        = $aSplitTransaction['transaction_id'];
                            $this->fAmount               = $aPayfrexTransaction['amount'];
                            $this->sTransactionReference = $aSplitTransaction['bookingreference'];
                            $this->sGatewayReference     = $aSplitTransaction['gatewayreference'];

                            // Set the payment gateway ID depending on what method was used
                            $sPaymentSolution = strtolower($aPayfrexTransaction['paymentSolution']);

                            switch ($sPaymentSolution)
                            {
                                case "neteller" :
                                    $this->iPaymentGatewayID = self::NETELLER_WITHDRAWAL;
                                break;

                                case "ukash" :
                                    $this->iPaymentGatewayID = self::UKASH_WITHDRAWAL;
                                break;

                                default :
                                    $this->iPaymentGatewayID = self::CC_WITHDRAWAL;
                                break;
                            }


                        }
                    }
                } while ($bFound === false);

                if ($aSplitTransaction['confirmed'] == 0) {

                    // Record as confirmed but ensure it doesn't add to the user's balance
                    $this->confirmTransaction(true, 6);

                    $sAuditLogMsg = "Confirmed split transaction {$aSplitTransaction['transaction_id']} (Payfrex ID {$aPayfrexTransaction['payFrexTransactionId']} with status 6";
                    AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_CONFIRM_SPLIT_TRANSACTION', 'transactions', 0);


                } elseif ($aSplitTransaction['status'] == 0) {
                    $this->confirmTransaction(false, 2);
                    $sAuditLogMsg = "Problem confirming split transaction {$aSplitTransaction['transaction_id']} (Payfrex ID {$aPayfrexTransaction['payFrexTransactionId']} with status 6";
                    AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_PROBLEM_CONFIRM_SPLIT_TRANSACTION', 'transactions', 0);
                }


            }
        }
        else
        {
            $sAuditLogMsg = "Problem finding split transactions for orig transaction {$iOriginalTransactionID}";
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_PROBLEM_FIND_SPLIT_TRANSACTIONS', 'transactions', 0);
        }
    }


    protected function searchTransaction($iTransactionID, $iMemberID, $sTransactionReference) {

        //echo ("Member ID $iMemberID, Transaction ID $iTransactionID, Transaction Ref $sTransactionReference");
        $sSQL = "SELECT t.amount as amount,
                        t.fk_status_id as status_id,
                        t.confirmed as confirmed,
                        t.fk_member_id as member_id,
                        t.transaction_id as transaction_id,
                        t.bookingreference as bookingreference,
                        t.gatewayreference as gatewayreference
                FROM transactions t
                WHERE t.transaction_id = {$iTransactionID} ";

        // Currently, returning XML from a withdrawal confirmation in Payfrex
        // does not pass back our previously submitted member id or transaction reference
        // so we need to ignore these if a withdrawal
        if ($iMemberID !== null && $sTransactionReference !== null)
        {
            $sSQL .= "AND t.fk_member_id = {$iMemberID}
                     AND t.bookingreference = '{$sTransactionReference}'";
        }
        //echo ($sSQL);


        if ($iTransactionID) {

            $sAuditLogMsg = "Transaction ID: {$iTransactionID}, Member:{$iMemberID} Transaction Ref: {$sTransactionReference}";
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_TRANSACTION_SQL_SEARCH', 'transactions', 0);
            return DAL::executeGetRow($sSQL);
        }
        return NULL;
    }



    /**
     * Determines what type of transaction this is, from Payfrex XML
     * Credit = Withdrawal, Debit = Payment
     *
     * @param array $aXML array of xml from Payfrex
     * @return string
     */
    protected function getTransactionTypeFromPayfrexXML($aXML)
    {
        $sTransactionType = '';

        $iNumOps = $aXML['@attributes']['operation-size'];

        if ($iNumOps > 1)
        {
            // Return first option here
            $sTransactionType = $aXML['operations']['operation'][0]['operationType'];

        }
        else
        {
            $sTransactionType = $aXML['operations']['operation']['operationType'];

        }

        return $sTransactionType;
    }


    /**
     * Extracts the transaction information from the Payfrex XML
     * Only used ias part of the widthdrawal process
     * (whether confirmation or not)
     *
     * @param array $aXML array of XML from PayFrex
     * @return array
     */
    protected function extractTransactionInfo($aXML)
    {
        $aTransactions = array();

        $iNumOps = $aXML['@attributes']['operation-size'];

        // Check if we have optional transaction information at this stage
        // If we do, then this is a deposit or an initial withdrawal transaction
        if (array_key_exists('fromPayfrex', $aXML))
        {
            // Do we have multiple transactions?
            if ($iNumOps > 1)
            {
                // Take the first transaction as the original
                $aTransactions['master'] = $aXML['operations']['operation'][0];
                // Put the remaining transactions onto the array
                $aTransactions['new'] = array_slice($aXML['operations']['operation'], 1);
            }
            else
            {

                // Only one transaction, so set it as the original
                // This will be the case if a withdrawal transaction is being confirmed
                $aTransactions['master'] = $aXML['operations']['operation'];

                // No new transactions, so set as null
                $aTransactions['new'] = null;
            }
        }
        else
        {   // No optional transaction information
            // so this is a confirmation of previous

            // Do we have multiple transactions?
            if ($iNumOps > 1)
            {
                // Copy array directly
                $aTransactions['all'] = $aXML['operations']['operation'];
            }
            else
            {
                // Only one transaction, so assign it to the 0th element of the array
                $aTransactions['all'][0] = $aXML['operations']['operation'];
            }
        }
        return $aTransactions;
    }

    protected function RedirectToPage($sURL) {
        header("Location: " . $sURL);
    }

    protected function curl_call($sPostFields) {

        $ch = curl_init( $this->sURL );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $sPostFields);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec( $ch );

        if (curl_errno($ch)) {
            //print_d(curl_error($ch));

            $sCurlError = curl_error($ch);
            $sAuditLogMsg = "Payfrex CURL error $sCurlError";
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_CURL_ERROR', 'transactions', 0);

            return false;
        }
        curl_close($ch);
        return $response;

    }

    /**
     * Parses the XML returned by Payfrex,
     * stores in the database and returns as array
     * @param type $sXML
     * @return type
     */
    protected function parsePayFrexXML($sXML)
    {

        $oXML = simplexml_load_string($sXML);

        //print_d($oXML);

        $sJSON = json_encode($oXML);
        //echo("hhh");
        //echo($sJSON);

        $aParsedXML = json_decode($sJSON,TRUE);

        // Get number of operations in the XML,
        // in order to extract correct transaction ID information

        $iNumOps = $aParsedXML['@attributes']['operation-size'];

        // Need to get the optional params here
        $aOptionalParams = $aParsedXML['optionalTransactionParams']['entry'];

        if (is_array($aOptionalParams))
        {

            $sAuditLogMsg = "Payfrex optional params found in XML";
            AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_OPTIONAL_PARAMS_FOUND', 'transactions', 0);

            foreach ($aOptionalParams as $aParams)
            {
                $sKey = $aParams['key'];
                $aParsedXML['fromPayfrex'][$sKey] = $aParams['value'];
            }
        }

        // How many operations?
        if ($iNumOps > 1)
        {
            // Transaction ID is in first array element
            $iTransactionID = $aParsedXML['operations']['operation'][0]['merchantTransactionId'];
        }
        else
        {
            $iTransactionID = $aParsedXML['operations']['operation']['merchantTransactionId'];
        }

        //print_d($aParsedXML);
        //
        // Store in database
        $sTablename = "transactions";
        $aData = array('api_response' => $sXML);
        $sParams = "transaction_id = " . $iTransactionID;


        $sAuditLogMsg = "Payfrex updating transaction id {$iTransactionID} with returned XML";
        AuditLog::LogItem($sAuditLogMsg, 'PAYFREX_UPDATE_XML_IN_TRANSACTION', 'transactions', 0);

        DAL::Update($sTablename, $aData, $sParams);

        return $aParsedXML;
    }


    // SECURITY FUNCTIONALITY

    private function encrypt($input, $key) {
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);

        $input = $this->pkcs5_pad($input, $size);

        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
        $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $data = base64_encode($data);
        return $data;
    }


    private function pkcs5_pad ($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    private function decrypt($sStr, $sKey) {
        $decrypted= mcrypt_decrypt(
                                    MCRYPT_RIJNDAEL_128,
                                    $sKey,
                                    base64_decode($sStr),
                                    MCRYPT_MODE_ECB
        );
        $dec_s = strlen($decrypted);
        $padding = ord($decrypted[$dec_s-1]);
        $decrypted = substr($decrypted, 0, -$padding);
        return $decrypted;
    }


    /**
     * Adds a split transaction into the transaction table
     *
     * @param int $iMemberID member id
     * @param float $fAmount transaction amount
     * @param string $sGatewayRef  gateway reference
     * @param integer $iCurrencyID  currency id
     *
     * @return integer
     */
    private function addSplitTransaction($iMemberID, $fAmount, $sGatewayRef, $iPaymentGatewayID, $sXMLString, $iCurrencyID = 1) {

        // Need to negate the transaction amount
        $fAmount = 0 - $fAmount;

        // Add transaction, set as pending and not confirmed.
        // this will be confirmed later.
        $sTablename = "transactions";
        $aData = array(
            'confirmed' => 0,
            'fk_member_id' => $iMemberID,
            'fk_gateway_id' => $iPaymentGatewayID,
            'fk_currency_id' => $iCurrencyID,
            'bookingreference' => TRANSACTIONREFERENCE . time(),
            'amount' => $fAmount,
            'transaction_date' => date("Y-m-d H:i:s"),
            'fk_status_id' => 2,
            'gatewayreference' => $sGatewayRef,
            'api_response' => $sXMLString
        );

        $this->iTransactionID = DAL::Insert($sTablename, $aData, true);

        return $this->iTransactionID;
    }
}
