<?php
/**
 * Installs PayFrex
 * @package LoveLotto
 * @subpackage PayFrex
 */

namespace LL\PayFrex;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        if(self::getModuleVersion('PayFrex')<1){
        }
        $aJobs[]=new \LL\Installer\Task('LL\PayFrex\Installer', 'installDatabase', 'Install PayFrex Database updates');

        return $aJobs;
    }

    /**
     * Installs all the tables we need for the new CMS
     */
    static function installDatabase(\LL\Installer\Task $oTask){
        self::installAPIResponseLogging();

    }

    /**
     * Modification to transactions table
     */
    static function installAPIResponseLogging(){
        $oTTable=new \LL\Installer\Table('transactions');

        $oTTable->addField(new \LL\Installer\Field('api_response', 'blob'));

        $oTTable->compile();
    }


}