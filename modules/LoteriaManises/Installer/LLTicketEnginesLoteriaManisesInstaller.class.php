<?php

namespace LL\TicketEngines;

class LoteriaManisesInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\LoteriaManisesInstaller', 'installTicketModule', 'Install Loteria Manises module');
        return $aJobs;
    }

    static function installTicketModule(){
        self::installModule('LoteriaManises',1);
        self::installHandler('\\LL\\TicketEngines\\LoteriaManises', 'Loteria Manises', 'TicketEngine',1);
    }
}
