<?php
/**
 * Provides the engine that is used for lodging insurance through PIMS SCA
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */

namespace LL\TicketEngines;

class LoteriaManises extends \BaseTicketEngine{

    function _getBackendConfigurationFields(){

        $aConfigurationFields=new \ConfigurationEngine();

        $aConfigurationFields->addField(new \ConfigurationString('sEndPoint',\lang::get('API End Point')));
        $aConfigurationFields->addField(new \ConfigurationString('sTicketType',\lang::get('Ticket Type ID')));
        $aConfigurationFields->addField(new \ConfigurationString('sPreSharedKey',\lang::get('Preshared Key')));

        return $aConfigurationFields;
    }

    /**
     * Calculates the cost based on the prize payout for each of the prize tiers
     * that this instance of the engine has been set to cover
     * @param type $oTicket
     */
    function getCost($oTicket) {
        return 1;
    }

    /**
     * Submits all tickets for this draw via either email or FTP
     * Lotto Gopher have an API planned for the future
     * @param draw $oDraw
     */
    function beforeDraw(draw $oDraw) {
        //Start a ticket buying session
        $oTicketSession=\TicketPurchasingSession::startSession($this->id, $oDraw->id);

        /**
         * Convert those into a CSV file to send to PIMS
         * Could have done this with a configurable field list, but best to hardcode
         * as it's going to be handling quite a lot of data
         */
        $aOutFile=array();
        $bFoundTickets=true;
        while($bFoundTickets){
            $aTickets=$oTicketSession->getTickets(100);
            if(sizeof($aTickets)==0) $bFoundTickets=false;
            foreach($aTickets as $oTicket){
                $aOutFile[]=$oTicket['email'].',';
            }
        }
        $sOutFile=implode("\r\n",$aOutFile);
        //Get the MD5
        $sMD5=md5($outfile);

        if($this->config->sEndPoint){
            //FTP the files across by FTP
            file_put_contents('ftp://'.$this->config->sEndPoint,$sOutFile);
        }
        if($this->config->sEMailAddress){
            //Send by email
        }
    }
}