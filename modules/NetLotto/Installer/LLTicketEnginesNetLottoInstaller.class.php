<?php

namespace LL\TicketEngines;

class NetLottoInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\NetLottoInstaller', 'installTicketModule', 'Install NetLotto module');
        return $aJobs;
    }

    static function installTicketModule(){
        self::installModule('NetLotto',1);
        self::installHandler('\\LL\\TicketEngines\\NetLotto', 'NetLotto', 'TicketEngine',1);
    }
}
