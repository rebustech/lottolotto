<?php
/**
 * Provides the engine that is used for lodging insurance through PIMS SCA
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */

namespace LL\TicketEngines;

class NetLotto extends \BaseTicketEngine{

    function _getBackendConfigurationFields(){

        $aConfigurationFields=new \ConfigurationEngine();

        // Base end point - actual full endpoint changes depending on functionality required
        $aConfigurationFields->addField(new \ConfigurationString('sEndPoint',\lang::get('API End Point')));

        $aConfigurationFields->addField(new \ConfigurationString('sPreSharedAPIKey',\lang::get('Preshared API Key')));

        $aConfigurationFields->addField(new \ConfigurationString('sAccountID',\lang::get('Account ID')));
        $aConfigurationFields->addField(new \ConfigurationString('sPassword',\lang::get('Password')));

        $aConfigurationFields->addField(new \ConfigurationString('sID',\lang::get('NetLotto sID')));
        $aConfigurationFields->addField(new \ConfigurationString('sSupp',\lang::get('NetLotto sSupp')));
        $aConfigurationFields->addField(new \ConfigurationString('sCategory',\lang::get('NetLotto sCategory')));


        return $aConfigurationFields;
    }

    /**
     * Calculates the cost based on the prize payout for each of the prize tiers
     * that this instance of the engine has been set to cover
     * @param type $oTicket
     */
    function getCost($oTicket) {
        return 1;
    }

    function loadConfig(){
        $this->oConfigData = json_decode($this->config);

        if($this->oConfigData->iMinToSend<10) $this->oConfigData->iMinToSend=10;
        if($this->oConfigData->iMaxToSend=='') $this->oConfigData->iMaxToSend=50;

        return $this->oConfigData;
    }

    /**
     * Submits all tickets for this draw via either email or FTP
     * @param draw $oDraw
     */
    function beforeDraw($oDraw) {
        $this->loadConfig();

        $sAuditLogMsg = "NetLotto - starting buying session, id {$oTicketSession->id}";
        \AuditLog::LogItem($sAuditLogMsg, 'NETLOTTO_PURCHASE_SESSION_START', 'TicketPurchasingSession',$this->id);

        //Start a ticket buying session
        $oTicketSession=\TicketPurchasingSession::startSession($this->id, $oDraw);
        $this->oTicketSession=$oTicketSession;

        $this->config=stripslashes($this->config);
        $sAuditLogMsg = $this->config;
        \AuditLog::LogItem($sAuditLogMsg, 'NETLOTTO_GETTING_CONFIG', 'TicketPurchasingSession',$this->id);
        $sAuditLogMsg = print_r($this->oConfigData, 1);
        \AuditLog::LogItem($sAuditLogMsg, 'NETLOTTO_GOT_CONFIG', 'TicketPurchasingSession',$this->id);
        
        
        //// ORIGINAL CODE FROM CASTILLO ///
        $count = 0;

        // Stop once all tickets have been found
        $aTickets=$oTicketSession->getTickets($this->oConfigData->iMaxToSend);

        if(sizeof($aTickets)<$this->oConfigData->iMinToSend) {
            $sAuditLogMsg = "Netlotto - Not enough tickets found (".sizeof($aTickets).'found, minimum to send is '.$this->oConfigData->iMinToSend.')';
            \AuditLog::LogItem($sAuditLogMsg, 'NETLOTTO_NO_TICKETS_FOUND', 'ticket_engines',$this->id);
            $this->last_result='BELOW THRESH';
            $this->save();
            $bFoundTickets=false;
            $oTicketSession->abandonSession();
            return;
        }

        foreach($aTickets as $oTicket){
            $count++;
            if ($count == 1)
            {
                //$aOutData['lotteryinfo']=$this->_getNetLottoLotteryInfoFromSystemId($oTicket['fk_lottery_id']);
                
                $aOutData['lotteryinfo']['id'] = $this->oConfigData->sID;
                $aOutData['lotteryinfo']['supp'] = $this->oConfigData->sSupp;
                $aOutData['lotteryinfo']['category'] = $this->oConfigData->sCategory;

                //$aOutData['drawdate']=$this->_formatCastilloLotteryDate($oTicket['fk_lotterydate']);
            }
            $aOutData['numbers'][$count]=$oTicket['numbers'];
        }

        print_r($aOutData);
        
        // Create JSON ready for sending over
        $sJSONString = $this->buildTicketsJSON($aOutData);


        if ($this->oConfigData->sEndPoint) {
            $oTicketSession->writeRequestToEngine($sJSONString);
            // Send the JSON via CURL and get response
            $sResponse = $this->_postTickets($sJSONString);

            // Response here should be a numeric value representing the  order ID
            // if successful, else it returns the error

            $oTicketSession->writeResponseFromEngineToSession($sResponse);

            print_r($sResponse);
            
            if (is_numeric($sResponse))
            {
                \AuditLog::LogItem('NetLotto Order Number : '.$response, 'NETLOTTO_PURCHASE_SUCCESS', 'TicketPurchasingSession',$this->id);
                // Successful order
                $oTicketSession->endSession();

                $this->successful_tickets+=$count;
                $this->successful_runs++;
                $this->last_successful_run_date=date('Y-m-d H:i:s');
                $this->last_result='OK';
                $this->save();
            }
            else
            {
                $oTicketSession->abandonSession();
                // Unsuccessful order
                // Raise provblem with ticket buying
                $oTicketSession->status="A problem has occured with buying tickets " . $aResponse['message'];
                $oTicketSession->success=false;
                $oTicketSession->tickets_failed=$count;

                $this->failed_tickets+=$count;
                $this->failed_runs++;
                $this->last_failure_run_date=date('Y-m-d H:i:s');
                $this->last_result=$response;

                // If we're getting any failures reduce the priority of this system
                $this->priority--;

                $this->save();
                
                \AuditLog::LogItem('NetLotto Error : '.$response, 'NETLOTTO_PURCHASE_ERROR', 'TicketPurchasingSession',$this->id);

            }
        }else{
            \AuditLog::LogItem('No endpoint configured ' . $this->config, 'NETLOTTO_PROBLEM_NO_ENDPOINT', 'TicketPurchasingSession',$this->id);
            $oTicketSession->abandonSession();
        }


    }

    /**
     * Builds JSON string containing tickets to purchase
     *
     * @param array $aOutData data to use when building the json
     * @return string
     */
    function buildTicketsJSON($aOutData) {

        // Next draw information
        // as I think we can only buy tickets for the next draw
        $aDrawInfoFromNetLotto = $this->_getNextDrawInfoFromAPI();

        $iLotteryId = $aOutData['lotteryinfo']['id'];

        $iDrawNo = $aDrawInfoFromNetLotto[$iLotteryId]['NextDrawNo'];
        $iCategory = $aOutData['lotteryinfo']['category'];

        $aToJSON = array();

        // AffiliateUserUniqueIDs needs to be unique for every call
        // (which is NOT clear in the documentation!)
        // so add the current timestamp here
        $aToJSON['AffiliateUserUniqueID'] = 'ML_' . date('YmdHis');

        $aToJSON['OrderEntries'][0]['DrawType'] = $iLotteryId;
        $aToJSON['OrderEntries'][0]['DrawNo'] = $iDrawNo;
        $aToJSON['OrderEntries'][0]['GameCategory'] = $iCategory;

        foreach ($aOutData['numbers'] as $sNumbers)
        {

            $aNumbers = explode('|', $sNumbers);

            $count = 0;
            $max = count($aNumbers);
            $aOutNumbers = array();
            foreach ($aNumbers as $iNumber)
            {

                // Check if at the last number in the array
                // and set as supplementary if needed
                if ($count == $max && $iLotteryId = $aOutData['lotteryinfo']['supp'] === true)
                {
                    $aTemp = array("Number"=>(int) sprintf('%d', $iNumber), "IsSupp"=>(bool) true);
                }
                else
                {
                    $aTemp = array("Number"=>(int) sprintf('%d', $iNumber), "IsSupp"=>(bool) false);
                }
                $aOutNumbers[] = $aTemp;
            }

            $aToJSON['OrderEntries'][0]['ListOfGameNumbers'][] = $aOutNumbers;
        }

        $sToJSON = json_encode($aToJSON);

        return $sToJSON;
    }



    /**
     * Returns concatenated string of email account ID and password
     * @return string
     */
    function _returnEncodedAccountDetails()
    {
         return base64_encode(urldecode($this->oConfigData->sAccountID.":".$this->oConfigData->sPassword));
    }


   /**
     * Return the correct netlotto lottery id
     * from the supplied 'system' id
     *
     * @param integer $iLotteryId the supplied lottery id
     * @return integer
     */
    function _getNetLottoLotteryInfoFromSystemId($iLotteryId)
    {
      
        
        $aNetLottoData = array();

        $aNetLottoData['id'] = $this->oConfig->sID;
        $aNetLottoData['supp'] = $this->oConfig->sSupp;
        $aNetLottoData['category'] = $this->oConfig->sCategory;

        return $aNetLottoData;
    }


    /**
     * Performs a call via CURL
     *
     * @param string $aData data to be sent
     * @return string
     */
    function _curl_call($aData) {

        //echo ($this->config->sEndPoint . $aData['url'] . "<br>");

        $ch = curl_init( $this->oConfigData->sEndPoint . $aData['url'] );

        $encode = $this->_returnEncodedAccountDetails();

        //echo ($encode . "<br>");

        $apikey = $this->oConfigData->sPreSharedAPIKey;
        //echo ($apikey . "<br>");
        if ($aData['method'] == 'POST')
        {
            //echo("Method is POST<br>");
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $aData['postdata']);
        }

        if ($aData['method'] == 'GET')
        {
           //echo("Method is GET<br>");
           curl_setopt( $ch, CURLOPT_HTTPGET, 1);
        }

        $options = array(CURLOPT_HTTPHEADER => array('Content-type: application/json',
                                                     'NetLottoAuth: '.$encode ,
                                                     'NetLottoApiKey: '.$apikey),
                         CURLOPT_SSL_VERIFYPEER => false,
                         CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt_array( $ch, $options );

        $sAuditLogMsg = print_r($aData,1);
        \AuditLog::LogItem($sAuditLogMsg, 'NETLOTTO_PURCHASE_SEND_TICKETS', 'TicketPurchasingSession',$this->id);


        $response = curl_exec( $ch );

        if (curl_errno($ch)) {
            $this->oTicketSession->abandonSession();
            //Execute the Error handling module to display errors.
            $sAuditLogMsg = print_r(curl_error($ch),1);
            \AuditLog::LogItem($sAuditLogMsg, 'NETLOTTO_PURCHASE_FAILURE', 'TicketPurchasingSession',$this->id);
        }
        curl_close($ch);
        return $response;

    }

    /**
     * Get next draw information for all lotteries
     * Converts returned information to array if present,
     * else returns false
     *
     * @return array if response, else false
     */
    function _postTickets($sJSON)
    {
        $aData['url'] = 'Order/PostOrder';
        $aData['method'] = 'POST';
        $aData['postdata'] = $sJSON;

        $oResponse = $this->_curl_call($aData);

        if ($oResponse)
        {
            $aResponse = $this->_json_to_array($oResponse);

            $aResponse = $this->_refactorArray($aResponse);

            return $aResponse;
        }
        else
        {
            return false;
        }
    }


    /**
     * Get next draw information for all lotteries
     * Converts returned information to array if present,
     * else returns false
     *
     * @return array if response, else false
     */
    function _getNextDrawInfoFromAPI()
    {
        $this->loadConfig();

        $aData['url'] = 'Draw/GetDraws';
        $aData['method'] = 'GET';

        $oResponse = $this->_curl_call($aData);

        print_r($oResponse);
        
        if ($oResponse)
        {
            $aResponse = $this->_json_to_array($oResponse);

            $aResponse = $this->_refactorArray($aResponse);

            return $aResponse;
        }
        else
        {
            return false;
        }
    }


    /**
     * Converts JSON to associative array
     *
     * @param onject $oJSON JSON onject to convert
     * @return array
     */
    function _json_to_array($oJSON)
    {

        return json_decode($oJSON, TRUE);

    }


    /**
     * API call to get a list of all categories
     *
     * @return array if found, else false
     */
    function _getDrawCategoriesFromAPI()
    {
        $this->loadConfig();
        $aData['url'] = 'Draw/GetCategories';
        $aData['method'] = 'GET';

        $oResponse = $this->_curl_call($aData);

        if ($oResponse)
        {

            $aResponse = $this->_json_to_array($oResponse);

            return $aResponse;
        }
        else
        {
            echo("No response");
            return false;
        }
    }

    /**
     * API call to get latest results for all lotteries
     *
     * @return array if found, else false
     */
    function _getLatestResultsFromAPI()
    {
        $aData['url'] = 'Draw/GetLatestResults';
        $aData['method'] = 'GET';

        $oResponse = $this->_curl_call($aData);

        if ($oResponse)
        {

            $aResponse = $this->_json_to_array($oResponse);

            return $aResponse;
        }
        else
        {
            echo("No response");
            return false;
        }
    }


    /**
     * Refactor array
     *
     * @param array $aToRefactor
     * @return array
     */
    function _refactorArray($aToRefactor) {

        $aNewArray = array();

        foreach ($aToRefactor as $array)
        {
            $aTemp = array();
            foreach ($array as $key=>$val)
            {


                if ($key=="DrawType")
                {
                    $id = $val;
                }
                else
                {
                    $aTemp[$key] = $val;
                }

            }
            $aNewArray[$id] = $aTemp;
        }

        return $aNewArray;
    }


    /**
     * Once we have prepared a bunch of tickets for sending they are marked as pending
     */
    function markPending(){
        DAL::Update('booking_items', array('purchased',1), 'fk_ticket_purchasing_session_id='.$this->id);
    }

    /**
     * Once the tickets have been transmitted to the provider we mark them as sent
     */
    function markSent(){
        DAL::Update('booking_items', array('purchased',2), 'fk_ticket_purchasing_session_id='.$this->id);
    }

    /**
     * Once we have confirmed that all the tickets in the session were successfully purchased
     * use this to confirm all the open tickets as bought.
     * Don't forget to also call endSession as confirmSession doesn't do this for you
     */
    function confirmSession(){
        DAL::Update('booking_items', array('purchased',3), 'fk_ticket_purchasing_session_id='.$this->id);
    }
}