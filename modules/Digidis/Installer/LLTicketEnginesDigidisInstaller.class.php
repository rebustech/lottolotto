<?php

namespace LL\TicketEngines;

class DigidisInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\DigidisInstaller', 'installTicketModule', 'Install Digidis module');
        return $aJobs;
    }

    static function installTicketModule(){
        self::installModule('Digidis',1);
        self::installHandler('\\LL\\TicketEngines\\Digidis', 'Digidis', 'TicketEngine',1);
    }
}
