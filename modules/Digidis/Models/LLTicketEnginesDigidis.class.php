<?php
/**
 * Provides the engine that is used for lodging insurance through Digidis
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Nathan Pace
 */

namespace LL\TicketEngines;

class Digidis extends \BaseTicketEngine{

    protected $oSOAP = NULL;

    function _getBackendConfigurationFields(){

        $aConfigurationFields=new \ConfigurationEngine();

        $aConfigurationFields->addField(new \ConfigurationString('sEndPoint',\lang::get('API End Point')));
        $aConfigurationFields->addField(new \ConfigurationString('iPartner',\lang::get('Partner ID')));
        $aConfigurationFields->addField(new \ConfigurationString('iPartnerTicketID',\lang::get('Partner ticket ID')));
        $aConfigurationFields->addField(new \ConfigurationString('sSecretPhrase',\lang::get('Secret Phrase')));
        $aConfigurationFields->addField(new \ConfigurationString('sSecureCodeKey',\lang::get('Secure Code Key')));
        $aConfigurationFields->addField(new \ConfigurationString('sSecureCodeIV',\lang::get('Secure Code IV')));
        $aConfigurationFields->addField(new \ConfigurationString('dCost',\lang::get('Cost per ticket')));
        $aConfigurationFields->addField(new \ConfigurationString('iMinToSend', \lang::get('Min number of tickets to send')));
        $aConfigurationFields->addField(new \ConfigurationString('iMaxToSend', \lang::get('Max number of tickets to send')));

        return $aConfigurationFields;
    }

    function loadConfig(){
        $this->oConfigData = json_decode($this->config);

        if($this->oConfigData->iMinToSend<1) $this->oConfigData->iMinToSend=1;
        if($this->oConfigData->iMaxToSend=='') $this->oConfigData->iMaxToSend=200;

        return $this->oConfigData;
    }

    /**
     * Calculates the cost based on the prize payout for each of the prize tiers
     * that this instance of the engine has been set to cover
     * @param type $oTicket
     */
    function getCost($oTicket) {
        return 1;

        // these lines won't currently be reached
        $oConfigData=$this->loadConfig();
        return $oConfigData->dCost;
    }

    /**
     * Submits all tickets for this draw via SOAP call
     * @param draw $oDraw
     */
    function beforeDraw($oDraw){
        $this->loadConfig();

        // Start a ticket buying session
        $oTicketSession=\TicketPurchasingSession::startSession($this->id, $oDraw);

        $sAuditLogMsg = "Digidis - starting buying session, id {$oTicketSession->id}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_PURCHASE_SESSION_START', 'ticket_engines',$this->id);

        $bFoundTickets=true;

        $aOutData = array();
        $count = 0;

        $sAuditLogMsg = "Digidis - Looking for tickets";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_SEARCHING_TICKETS', 'ticket_engines',$this->id);

        $sAuditLogMsg = $this->config;
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_GETTING_CONFIG', 'ticket_engines',$this->id);

        $sAuditLogMsg = print_r($this->oConfigData, 1);
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_GOT_CONFIG', 'ticket_engines',$this->id);

        $aTickets=$oTicketSession->getTickets($this->oConfigData->iMaxToSend);
        if(sizeof($aTickets)==0) {
            $sAuditLogMsg = "Digidis - No tickets found";
            \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_NO_TICKETS_FOUND', 'ticket_engines',$this->id);
            $this->last_result='UP TO DATE';
            $this->save();
            $bFoundTickets=false;
            return false;
        }
        if(sizeof($aTickets)<$this->oConfigData->iMinToSend) {
            $sAuditLogMsg = "Digidis - Not enough tickets found (".sizeof($aTickets).'found, minimum to send is '.$this->oConfigData->iMinToSend.')';
            \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_NO_TICKETS_FOUND', 'ticket_engines',$this->id);
            $this->last_result='BELOW THRESH';
            $this->save();
            $bFoundTickets=false;
        }

        // Get all tickets in this session into an array
        // Some formatting of certain data is needed, handled by fucntions
        foreach($aTickets as $oTicket){

            $sPartnerTicket = $oTicket['bookingitem_id'];
            $aGameInfo = $this->_getDigidisGameInfo($oTicket['fk_lottery_id']);

            $aOutData[$count]['idPartner']=$this->oConfigData->iPartner;
            $aOutData[$count]['idGameType']=$this->oConfigData->iPartnerTicketID;
            $aOutData[$count]['isCommunity']=0;
            $aOutData[$count]['drawDate']=$this->_formatDigidisLotteryDate($oTicket['fk_lotterydate']);
            $aOutData[$count]['idPartnerTicket']=$sPartnerTicket;
            $aOutData[$count]['receivedBets'][]=$this->formatTicketNumbers($oTicket['numbers'], $sPartnerTicket, $aGameInfo['bonuses'] );
            $aOutData[$count]['secureCode']= $this->_generateSecureCode(array($this->oConfigData->iPartnerTicketID,$sPartnerTicket));
            $count++;
        }

        $sAuditLogMsg = 'Retrieved '.$count.' tickets for purchasing';

        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_TICKETS_FOUND', 'ticket_engines',$this->id);

        // End point should be in the database
        if($this->oConfigData->sEndPoint!=''){

            $sAuditLogMsg = "Sending tickets to Digidis endpoint";
            \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_SENDING_TICKETS', 'ticket_engines',$this->id);


            // Open SOAP connection
            $this->openSOAPConnection();

            // Get next draw information
            //$aResponse = $this->getNextDrawInfo($aOutData);

            $oTicketSession->writeRequestToEngine(json_encode($aOutData));

            // Submit tickets to be bought and get response
            $aResponse = $this->buyTickets($aOutData, $oTicketSession);

            // At this point, we should have a status
            if ($aResponse['status'] == 'OK')
            {
                $sAuditLogMsg = "Digidis -  $count tickets bought";
                \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_TICKETS_BOUGHT', 'ticket_engines',$this->id);

                $oTicketSession->tickets_purchased=$count;

                $this->successful_tickets+=$count;
                $this->successful_runs++;
                $this->last_successful_run_date=date('Y-m-d H:i:s');
                $this->last_result='OK';
                $this->save();
                $oTicketSession->endSession();

                $sAuditLogMsg = "Digidis -  tickets successfully sent";
                \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_TICKETS_SUCCESS', 'ticket_engines',$this->id);


            }
            else
            {
                $oTicketSession->abandonSession();

                // Raise provblem with ticket buying
                $oTicketSession->status="A problem has occured with buying tickets " . $aResponse['message'];
                $oTicketSession->success=false;
                $oTicketSession->tickets_failed=$count;

                $this->failed_tickets+=$count;
                $this->failed_runs++;
                $this->last_failure_run_date=date('Y-m-d H:i:s');
                $this->last_result=$aResponse['message'];

                // If we're getting any failures reduce the priority of this system
                $this->priority--;

                switch($aResponse['objReturn']->response){
                    case 'LIMIT_REACHED_NOT_IMPORTED':
                        $this->is_suspended=1;
                        break;
                    case 'ERROR_DRAWDATE_IN_THE_PAST':
                        //Tickets not bought
                        break;
                    case 'WARNING_IDPARTNERTICKET_ALREADY_IMPORTED':
                        break;
                    case 'ERROR_IDPARTNERTBET_ALREADY_USED':
                        break;
                    case 'ERROR_DRAWDATE_NOT_FOUND':
                        break;
                    case 'ERROR_DRAWDATE_NOT_ENOUGH':
                        break;
                    case 'ERROR_DRAWDATE_NOT_ENOUGH':
                        break;
                    case 'ERROR_DRAWDATE_NOT_ENOUGH':
                        break;
                    case 'ERROR_INVALID_DATA':
                        break;
                    default:
                        $this->is_suspended=1;
                        break;
                }

                $sAuditLogMsg = "Problem with buying tickets " . $aResponse['message'];
                \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_PROBLEM_BUYING_TICKETS', 'ticket_engines',$this->id);

            }

            $this->save();

            $oTicketSession->endSession();


        } else {
            $this->failed_runs++;
            $this->last_failure_run_date=date('Y-m-d H:i:s');
            $this->last_result='CONFIG ERROR';
            \AuditLog::LogItem('No endpoint configured ' . $this->config, 'DIGIDIS_PROBLEM_NO_ENDPOINT', 'ticket_engines',$this->id);
            $oTicketSession->abandonSession();
        }

        $sAuditLogMsg = "Digidis - finished buying session, id {$oTicketSession->id}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_PURCHASE_SESSION_END', 'ticket_engines',$this->id);

        return $oTicketSession;
    }


    /**
     * Once we have prepared a bunch of tickets for sending they are marked as pending
     */
    function markPending(){
        $sAuditLogMsg = "Digidis - tickets marked as pending - session id " .$this->id;
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_TICKETS_MARKED_PENDING', 'booking_items', 0);
        DAL::Update('booking_items', array('purchased',1), 'fk_ticket_purchasing_session_id='.$this->id);
    }

    /**
     * Once the tickets have been transmitted to the provider we mark them as sent
     */
    function markSent(){
        $sAuditLogMsg = "Digidis - tickets marked as sent - session id ".$this->id;
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_TICKETS_MARKED_SENT', 'booking_items', 0);
        DAL::Update('booking_items', array('purchased',2), 'fk_ticket_purchasing_session_id='.$this->id);
    }

    /**
     * Once we have confirmed that all the tickets in the session were successfully purchased
     * use this to confirm all the open tickets as bought.
     * Don't forget to also call endSession as confirmSession doesn't do this for you
     */
    function confirmSession(){
        $sAuditLogMsg = "Digidis - tickets marked as confirmed - session id ".$this->id;
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_TICKETS_MARKED_CONFIRMED', 'booking_items', 0);

        DAL::Update('booking_items', array('purchased',3), 'fk_ticket_purchasing_session_id='.$this->id);
    }



    /**
     * Open a SOAP connection
     */
    protected function openSOAPConnection() {
        // https://dgptestservices.serviapuestas.es:8443/axis2/services/TEST_DGPServices?wsdl
        $sAuditLogMsg = "Digidis - Opening SOAP connection";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_OPEN_SOAP_CONNECT', 'ticket_engines',$this->id);
        $this->oSOAP = new \SoapClient($this->oConfigData->sEndPoint);
        \AuditLog::LogItem('Digidis - SOAP connection opened', 'DIGIDIS_OPEN_SOAP_CONNECT', 'ticket_engines',$this->id);
    }



    /**
     * Wrapper function to get next draw information
     *
     * @param array $aData array of data referring to lottery
     * @return array
     */
    protected function getNextDrawInfo($aData) {

        $sAuditLogMsg = "Digidis - Getting next draw information";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_GET_NEXT_DRAW_INFO', 'ticket_engines',$this->id);

        $oResponse = $this->oSOAP->__soapCall('obtainNextDrawInfo', $aData);

        // Pass response to be formatted and return as array
        return $this->_formatResponse($oResponse, "nextDraw");
    }

    /**
     * Wrapper function to purchase tickets
     *
     * @param array $aData array of data referring to lottery
     * @param object $oTicketSession ticket buying session
     * @return array
     */
    protected function buyTickets($aData, $oTicketSession) {

        print_r($aData);

       $sAuditLogMsg = "Digidis - buying tickets";
       \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_BUY_TICKETS', 'ticket_engines',$this->id);

       $oResponse = $this->oSOAP->__soapCall('importTicket', $aData);
       // Pass response to be formatted and return as array
       $aResponse = $this->_formatResponse($oResponse, "buyTickets");

       // Write response to session
       $oTicketSession->writeResponseFromEngineToSession(serialize($aResponse));

       // Return response
       return $aResponse;
    }

    /**
     * Return Digidis-specific information for each lottery
     * based on the lottery ID from the system
     *
     * Id and number of bonus numbers required
     *
     * @param integer $iLotteryId lottery id from system
     * @return integer
     */
    function _getDigidisGameInfo($iLotteryId)
    {
        $aDigidisInfo = array();

        // These should really be changed to constants!
        switch ($iLotteryId)
        {
            case 2:  // EuroMillions
                $aDigidisInfo['id'] = 9;
                $aDigidisInfo['bonuses'] = 2;
            break;
            case 11:  // Eurojackpot
                $aDigidisInfo['id']  = 31;
                $aDigidisInfo['bonuses'] = 1;
            break;
            case 13:  // Eurojackpot
                $aDigidisInfo['id']  = 30;
                $aDigidisInfo['bonuses'] = 2;
            break;

            default:
                $aDigidisInfo['id']  = 6;
                $aDigidisInfo['bonuses'] = 0;
        }


        return $aDigidisInfo;
    }

    /**
     * Formats a draw date from the database in ISO 8601 format
     * for sending over as part of the SOAP call
     *
     * @param date $sDate the draw date from the database
     * @return null
     */
    function _formatDigidisLotteryDate($sDate)
    {

        $sAuditLogMsg = "Digidis -  lottery id data for ML id {$iLotteryId} is {$aDigidisInfo['id']}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_LOTTERY_ID_DATA', 'booking_items', 0);

        $sFormatDate = date("c", strtotime($sDate));

        $sAuditLogMsg = "Digidis - formatting date as {$sFormatDate}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_FORMATTING_DATE', 'ticket_engines',$this->id);

       return $sFormatDate;
    }

    /**
     * Generates partner ticket id - a unique value to identify each ticket
     * ID is a timestamp in format YmdHis
     *
     * @return date
     */
    function _generatePartnerTicket()
    {

        $sPartnerTicket = date ("YmdHis");

        $sAuditLogMsg = "Digidis - generating partner ticket as {$sPartnerTicket}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_FORMATTING_DATE', 'ticket_engines',$this->id);

        return $sPartnerTicket;
    }

    /**
     * Generates secure code hash to be passed over in SOAP call
     *
     * @param array $aData array of data to be added to the encryption
     * @return string
     */
    function _generateSecureCode($aData){

        $sAuditLogMsg = "Digidis - generating secure code";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_GEN_SECURE_CODE', 'ticket_engines',$this->id);

        // Any data supplied has to be converted into a string
        $sData = implode("", $aData);

        // Current GMT date in milliseconds since UNIX epoch
        $iGMTDateTimeMS = round(microtime(true) * 1000);


        // Need to add secret phrase and partner ID to the string to be encrypted
        $sToEncrypt = $this->oConfigData->iPartner .
                      $this->oConfigData->sSecretPhrase .
                      $sData .
                      $iGMTDateTimeMS;


	$sSecureCodeKey = "\x13\x5a\x42\x7c\x19\x3b\x3b\x68\x4d\x28\x21\x35\x4b\x32\x2a\x4f";
	$sSecureCodeIV = "\x46\x2c\x43\x1b\x1a\x1b\x50\x3c";

	$sEncoded = openssl_encrypt($sToEncrypt , "bf-cbc" , $sSecureCodeKey ,false, $sSecureCodeIV);
	$dec2 = openssl_decrypt($sEncoded, "bf-cbc", $this->oConfigData->sSecureCodeKey, false, $this->oConfigData->sSecureCodeIV);

        /**
         * Sanity check - if we decode the securecode back does it match?
         */
        if($dec2!=$sToEncrypt){
            die('Mismatched');
        }

        // Run the encryption and return encrypted string
        //$sEncoded = openssl_encrypt($sToEncrypt , "bf-cbc" , $this->oConfigData->sSecureCodeKey ,false, $this->oConfigData->sSecureCodeIV);

        $sAuditLogMsg = "Digidis - generated secure code";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_GEN_SECURE_CODE_END', 'ticket_engines',$this->id);

        return $sEncoded;


    }

    /**
     * Takes string of lottery numbers and generates arrays of main/bonus numbers
     * before concatenating into string
     *
     * @param string $sNumbers number string
     * @param integer $iTicketId ticket ID
     * @param integer $iBonusNumbers number of bonus numbers
     *
     * @return string
     */
    function formatTicketNumbers($sNumbers, $iTicketId, $iBonusNumbers)
    {

        $sAuditLogMsg = "Digidis - formatting numbers {$sNumbers}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_FORMAT_NUMBERS', 'ticket_engines',$this->id);

        // Final string is made up of the ticket id, main numbers and bonus numbers
        // Individual numbers seperated by commas, component parts by semicolons

        $aNumbers = explode("|", $sNumbers);

        $aNumbersSeperate = array();

        $sFinalString = $iTicketId . ";"; // First part of string

        // Work out the bonus numbers from the number string
        if ($iBonusNumbers > 0)
        {
            $iBonusNumPos = 0 - (int) $iBonusNumbers;

            $aNumbersSeperate['main']  = array_slice($aNumbers, 0, $iBonusNumPos);
            $aNumbersSeperate['bonus'] = array_slice($aNumbers, $iBonusNumPos);

        }
        else
        {
            $aNumbersSeperate['main']  = $aNumbers;
            $aNumbersSeperate['bonus'] = null;
        }

        // Second part of string - main numbers
        $sFinalString .= implode(",", $aNumbersSeperate['main']);

        $sFinalString .= ";";

        // Add bonus numbers if we have them
        if ($aNumbersSeperate['bonus'] !== null)
        {
            $sFinalString .= implode(",", $aNumbersSeperate['bonus']);
        }

        $sAuditLogMsg = "Digidis - formatted numbers {$sFinalString}";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_FORMAT_NUMBERS', 'ticket_engines',$this->id);

        return $sFinalString;
    }


    /**
     * Format response from SOAP call into a more friendly format
     *
     * @param object $oResponse response
     * @return array
     */
    function _formatResponse($oResponse)
    {
        $sAuditLogMsg = "Digidis -  formatting SOAP response";
        \AuditLog::LogItem($sAuditLogMsg, 'DIGIDIS_FORMATTING_SOAP_RESPONSE', 'booking_items', 0);

        $aResponseInfo = array();

        // 'response' variable will contain a string with underscores
        // and first element before first underscore will have a status of
        // either OK, KO, ERROR or WARNING

        // Seperate the string into an array
        $aResponseString = explode("_", $oResponse->return->response);

        // Get the first element and use that as the status
        $aResponseInfo['status'] = array_shift($aResponseString);

        // Rest of the string as a message
        $aResponseInfo['message'] = implode(" ", $aResponseString);

        // Pass the rest of the object back in a new element
        $aResponseInfo['objReturn'] = $oResponse->return;

        return ($aResponseInfo);
    }
}
