<?php

namespace LL\TicketEngines;

class PIMSInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\PIMSInstaller', 'installTicketModule', 'Install PIMS module');
        return $aJobs;
    }

    static function installTicketModule(){
        self::installModule('PIMS Insurance',1);
        self::installHandler('\\PIMSInsuredTicketEngine', 'Insurance', 'TicketEngine',1);
    }
}
