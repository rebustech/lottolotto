<?php
/**
 * Provides the engine that is used for lodging insurance through PIMS SCA
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */
class PIMSInsuredTicketEngine extends BaseTicketEngine{


    function _getBackendConfigurationFields(){

        $aConfigurationFields=new ConfigurationEngine();

        $aConfigurationFields->addField(new ConfigurationString('sEndPoint',lang::get('FTP End Point')));
        $aConfigurationFields->addField(new ConfigurationString('sFTPUserName',lang::get('FTP Username')));
        $aConfigurationFields->addField(new ConfigurationString('sFTPPassword',lang::get('FTP Password')));

        return $aConfigurationFields;
    }
    /**
     * Calculates the cost based on the prize payout for each of the prize tiers
     * that this instance of the engine has been set to cover
     * @param type $oTicket
     */
    function getCost($iJackpot) {
        return 0.6;
        /**
         * Instantiate the game engine to get the odds and exposure for this line
         */
        $oGameEngine=GameEngine::getById($oTicket->fk_game_engine_id);
        $oGameEngine->populateBuyerOptions(unserialize($oTicket->game_options));
        $fMaximumExposure=$oGameEngine->getMaximumExposure();
        $fOdds=$oGameEngine->getOdds();

        /**
         * We're not worried about the draw here, just the exposure
         */
        return ((1/$fOdds)*$fMaximumExposure)*1.7;
    }
    /**
     * Submits all tickets for this draw to to the PIMS SCA API
     * @param draw $oDraw
     */
    function beforeDraw(\draw $oDraw) {
        $this->config=stripslashes($this->config);
        $this->oConfigData=json_decode($this->config);

        /**
         * Check if there is already a bluefin
         * We also only want to send it every 60 mins except in the lead up to the draw, then every 10
         */
        //if(strtotime($this->last_run_date)==0) $this->last_run_date=date('Y-m-d H:i:s');

        $timeSinceLastStart=((time()-strtotime($this->last_run_date))*60);
        $timeToDraw=(strtotime($oDraw->drawdate)-time())*60;
        \AuditLog::LogItem('Time since last lockup : '.$timeSinceLastStart.' Time to draw : '.$timeToDraw, 'BLUEFIN_START_SESSION', 'PIMSInsuredTicketEngine', $this->id);
        if($timeToDraw<60){
            if($timeSinceLastStart<10) return;
        }else{
            if($timeSinceLastStart<60) return;
        }

        if($this->oConfigData->sEndPoint!=''){
            //Start a ticket buying session
            $oTicketSession=TicketPurchasingSession::startSession($this->id,$oDraw);

            \AuditLog::LogItem('Starting Insurance Lockup', 'BLUEFIN_START_SESSION', 'PIMSInsuredTicketEngine', $this->id);

            $this->last_run_date=date('Y-m-d H:i:s');


            /**
             * Convert those into a CSV file to send to PIMS
             * Could have done this with a configurable field list, but best to hardcode
             * as it's going to be handling quite a lot of data
             */
            $aOutFile=array();
            $bFoundTickets=true;
            $aTickets1=$oTicketSession->getTickets(5000);

            //For insurance we have to send the whole file every time

            $sSQL="SELECT b.fk_member_id,bi.bookingitem_id,bi.fk_lottery_id,bi.fk_lotterydate,bi.numbers
                    FROM booking_items bi
                    INNER JOIN bookings b ON bi.fk_booking_id=b.booking_id
                    WHERE fk_lotterydate='".$oDraw->drawdate."' AND fk_lottery_id=".$oDraw->fk_lottery_id;

            $aTickets=\DAL::executeQuery($sSQL);

            //if(sizeof($aTickets)==0) $bFoundTickets=false;
            foreach($aTickets as $oTicket){
                $aOutFile[]=$oTicket['member_id'].','.$oTicket['fk_booking_id'].','.$oTicket['fk_lottery_date'].','.$oTicket['numbers'].','.$oTicket['fk_lottery_id'].',';
            }
            $sOutFile=implode("\r\n",$aOutFile);
            //Get the MD5
            $sMD5=md5($sOutFile);

            \AuditLog::LogItem('MD5:'.$sMD5, 'BLUEFIN_SENDING_FILES', 'transactions', 0);
            //FTP the files across to PIMS
            $this->_send_file($oDraw->fk_lottery_id.'-'.$oDraw->fk_lotterydate.'.csv', $sOutFile);
            $this->_send_file($oDraw->fk_lottery_id.'-'.$oDraw->fk_lotterydate.'-hash.txt', $sMD5);

            $this->successful_tickets+=sizeof($aTickets1);
            $this->successful_runs++;
            $this->last_successful_run_date=date('Y-m-d H:i:s');
            $this->last_result='OK';

            $oTicketSession->endSession();
        }else{
            $this->failed_runs++;
            $this->last_failure_run_date=date('Y-m-d H:i:s');
            $this->last_result='CONFIG ERROR';
            $this->priority--;
            \AuditLog::LogItem('Config missing or incorrect', 'BLUEFIN_CONFIG_MISSING', 'PIMSInsuredTicketEngine', $this->id);
        }
        $this->save();
    }

    function _send_file($sFilename,$sData){
        \AuditLog::LogItem($sFilename.' '.strlen($sData).' bytes', 'BLUEFIN_UPLOAD_FILE', 'PIMSInsuredTicketEngine', $this->id);
        file_put_contents('ftp://'.$this->oConfigData->sEndPoint,$sOutFile);
    }

    /**
     * Creates a pims ticket engine in the list of ticket engine handlers
     */
    static function install(){
        LLInstaller::installHandler('PimsInsuredTicketEngine','PIMS Insurance', 'TicketEngine', __FILE__);
    }
}