<?php

/**
 * FAQpage smarty object
 * Used to handle the FAQ system from the database into smarty templates
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
class FAQPage extends Page {

    protected $iFAQCategoryID = NULL;
    protected $oFAQ = NULL;

    public function __construct($iPageID, $sTemplate, $bSSL = false) {
        parent::__construct($iPageID, $sTemplate, $bSSL);
        $this->assignFAQCategoryID();
    }

    public function displayPage() {
        if (!$this->isCached()) {
            $this->assignFAQCategories();
            $this->assignFAQs();
        }
        parent::displayPage();
    }

    protected function assignFAQCategories() {
        $this->smarty->assign("faqcategories", $this->oFAQ->getFAQCategories());
    }

    protected function assignFAQs() {
        $aFAQs = $this->oFAQ->getFAQs($this->iFAQCategoryID);
        if (empty($aFAQs)) {
            $this->iFAQCategoryID = NULL;
            $aFAQs = $this->oFAQ->getFAQs($this->iFAQCategoryID);
        }
        $this->smarty->assign("faqs", $aFAQs);
        unset($aFAQs);
    }

    protected function assignFAQCategoryID() {
        $this->oFAQ = new FAQs($this->iLangID);
        $iFAQCategoryID = (int) $_GET['fcid'];
        $this->iFAQCategoryID = $iFAQCategoryID;
    }

    protected function setFAQCategoryID($iFAQCategoryID) {
        $this->iFAQCategoryID = $iFAQCategoryID;
    }

}
