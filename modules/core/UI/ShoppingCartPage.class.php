<?php

/**
 * Smarty object that handles the shopping cart page
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
class ShoppingCartPage extends Page {

    public function __construct($iPageID, $sTemplate) {
        parent::__construct($iPageID, $sTemplate, true);
    }

    public function displayPage() {
        parent::displayPage();
    }

}