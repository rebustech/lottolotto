<?php

/**
 * CheckoutPage smarty object
 * Handles the checkout system for smarty templates
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
class CheckoutPage extends MemberAreaPage {

    public function __construct($iPageID, $sTemplate, $bSSL = true, $bShowLoginPage = false) {
        parent::__construct($iPageID, $sTemplate, $bSSL, $bShowLoginPage);
    }

    public function performActions($sTransactionIDStr = null) {
        if($_GET['force']==1){
            $_POST['action']='purchase';
        }
        if ($_POST['action']) {
            switch ($_POST['action']) {
                case 'remove': $this->deleteFromCart();
                    break;
                case 'checkout':
                    $this->checkBalance();
                    break;
                case 'purchase':
                    sleep(2);
                    LL\PaymentProcessing\Status::setStep(2);
                    if($this->checkBalance()) {

                        sleep(1);
                        LL\PaymentProcessing\Status::setStep(2);
                        # Successfully saved?
                        $this->oCart->commitPurchase($this->oMember, $this->oCurrency);

                        $_SESSION['last_order']=$this->oCart;
                        # Remove checkout stage
                        unset($_SESSION['checkout']);
                        unset($_SESSION['is_first_order']);
                        # Save the new cart object
                        $this->saveSessionObjects();
                        
                        sleep(1);
                        LL\PaymentProcessing\Status::setStep(3);

                        //header('Location: /myaccount.php?action=bets&m=1'); //Ticket #201 - Go to the lottery orders page until we get messaging sorted out
                        
                        // Redirect to the transaction page with transaction ID,
                        // so that a confirmation message will be shown
                        header("Location: /transactions.php?t=$sTransactionIDStr");
                        
                        //header("Location: /myaccount");
                        die("<script>document.location='/transactions.php?t=$sTransactionIDStr';</script>");
                    }else{
                        
                        LL\PaymentProcessing\Status::setStep(2,'PaymentModal.TopUpRequired');
                        LL\PaymentProcessing\Status::setStep(3,'PaymentModal.GoingToPaymentPage');
                        sleep(2);
                        LL\PaymentProcessing\Status::setStep(2);
                        sleep(1);
                        LL\PaymentProcessing\Status::setStep(3);
                        
                        // Pass the supplied transaction ID if there is one
                        header("Location: /addFunds?t=$sTransactionIDStr");
                        exit;
                    }
                    break;
                default:
                    break;
            }
        }
        $this->saveSessionObjects();
    }

    protected function deleteFromCart() {
        if ($_POST['lotteryid'] && $_POST['number'] && $_POST['drawdate']) {
            $this->oCart->removeFromCart($_POST['lotteryid'], $_POST['drawdate'], $_POST['number']);
        }
    }

    public function checkBalance() {
        $bValid = true;
        $iMemberBalance = round($this->oMember->getMemberBalance(true), 2);
        if (($iMemberBalance - $this->oCart->getCartTotal()) < 0) {
            $this->oError->addNotice("You currently have insufficient funds to purchase your tickets. You will need to  <a href=\"http://" . WEBSITEURL . "addFunds.php\">add funds</a> to your account in order to place your order.");
            $this->smarty->assign("checkouterror", true);
            $bValid = false;
        }
        return $bValid;
    }

    public function assignReceipt() {

        $aReceiptDetails = $this->oCart->getReceiptDetails();

		# Valid booking reference?
		if(!empty($aReceiptDetails['bookingreference'])) {

			$this->smarty->assign("referencenumber", $aReceiptDetails['bookingreference']);
			$this->smarty->assign("amount", $aReceiptDetails['total']);
			$this->smarty->assign("gatewayname", $aReceiptDetails['gateway']);
			$aMember = $this->oMember->getMemberDetails(1);

			$aGoogleEcommerce = array();
			$aGoogleEcommerce['bookingreference'] = $aReceiptDetails['bookingreference'];
			$aGoogleEcommerce['store'] = WEBSITETITLE;
			$aGoogleEcommerce['total'] = $aReceiptDetails['total'];
			$aGoogleEcommerce['city'] = $aMember['city'];
			$aGoogleEcommerce['country'] = $aMember['country'];
			$aGoogleEcommerce['items'] = $aReceiptDetails['items'];
			$this->smarty->assign("googleecommerce", $aGoogleEcommerce);

		}

		$aReceiptDetails['bookingID'] = 10206;

		if(!empty($aReceiptDetails['bookingID'])) {

			# Get ticket
			$latestTicket = array();
			$tickets = $this->oMember->getBookingItems($this->iLangID, $iTotalTickets, $aReceiptDetails['bookingID'], 0, 1);

			# Grab the first one
			if(!empty($tickets)) {
				$latestTicket = $tickets[0];
				$latestTicket['multiLine'] = true;
			}

			# Store in view
			$this->smarty->assign("latestTicket", $latestTicket);

		}

		unset($aReceiptDetails);
		unset($aGoogleEcommerce);

    }

    public function displayPage() {
        parent::displayPage();
    }
    
}

?>