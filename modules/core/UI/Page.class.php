<?php

/**
 * Smarty object used for generic pages from the CMS
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
class Page extends SmartyObject {

    var $oMember = NULL;
    var $oCart = NULL;
    var $oPage = NULL;
    var $oCurrency = NULL;
    var $oNavigation = NULL;
    var $oError = NULL;
    var $iLangID = NULL;
    var $sCharSet = 'UTF-8';
    var $sDBCharSet = 'utf8';
    var $hasLoadedGames = false;
    var $requiresFullGameDetails = false;

    // Constants for default country, language and currency
    const DEFAULT_COUNTRY_ID = 223;                 // UNITED KINGDOM
    const DEFAULT_HOMESITE = 'dev.maxlotto.com';    // DEFAULT MAXLOTTO SITE
    const DEFAULT_LANG_ID = 1;                      // ENGLISH
    const DEFAULT_CURRENCY_ID = 1;                  // EURO
    
    public function __construct($iPageID, $sTemplate, $bSSL = false) {
        
        $this->assignSessionObjects();
        $this->assignLanguage();
        $this->assignCurrency();
        $this->oPage = new PageDetails($iPageID, $this->iLangID);
        $this->oNavigation = new Navigation($this->iLangID);
        $this->getPageDetails();
        $this->oError = new Error();
        $this->assignAffiliate();
        $this->checkMemberLocationGeoIP();

        //$this->checkMemberLocation();
        
        parent::__construct($sTemplate, $this->oPage->getCacheTime(), $this->createCacheName(), $bSSL);
    }

    public function displayPage() {
        $this->assignSmartyPageVars();
        $this->assignActiveLotteryDetails();
        parent::displayPage();
        $this->saveSessionObjects();
    }

    public function isCached() {
        return parent::isCached();
    }

    public function assign($varname, $data) {
        $this->smarty->assign($varname, $data);
    }

    private function assignSessionObjects() {
        $oSC = isset($_SESSION['cart']) ? unserialize($_SESSION['cart']) : null;
        if (is_object($oSC) && is_a($oSC, "Cart")) {
            $this->oCart = $oSC;
        } else {
            $this->oCart = new Cart();
        }
        unset($oSC);
        $oMember = isset($_SESSION['memberobject']) ? unserialize($_SESSION['memberobject']) : null;
        if (is_object($oMember) && is_a($oMember, "Member")) {
            $this->oMember = $oMember;
        } else {
            $this->oMember = new Member();
        }
        unset($oMember);


        $oCurrency = isset($_SESSION['currency']) ? unserialize($_SESSION['currency']) :null;
        if (is_object($oCurrency) && is_a($oCurrency, "Currency")) {

            $this->oCurrency = $oCurrency;
        } else {
            $this->oCurrency = new Currency();
        }
        unset($oCurrency);

    }

    public function saveSessionObjects() {
        @session_start();
        $_SESSION['cart'] = serialize($this->oCart);
        $_SESSION['memberobject'] = serialize($this->oMember);
        $_SESSION['currency'] = serialize($this->oCurrency);
    }

    private function createCacheName() {
        $sCacheName = urlencode($_SERVER['SCRIPT_FILENAME']);
        $sCacheName .= "L" . $this->iLangID . "P" . $this->oPage->getPageID() . "C" . $this->oCurrency->getSelectedCurrencyID() . "|G";
        foreach ($_GET as $key => $value) {
            $sCacheName .= $key . $value . "|";
        }
        $sCacheName .= "|P";
        foreach ($_POST as $key => $value) {
            $sCacheName .= $key . $value . "|";
        }
        return $sCacheName;
    }

    protected function getPageDetails() {
        $this->oPage->getPageDetails();
        if (!$this->oPage->checkPageActive()) {
            header('location: /404.html');
            exit(0);
        }
    }

    protected function assignUserDetails() {
        if ($this->oMember->validateMember()) {
            $this->smarty->assign("memberdetails", $this->oMember->getMemberDetails());
            $this->smarty->assign("loggedin", true);
        } else {
            $this->smarty->assign("loggedin", false);
        }
        $this->smarty->assign("cart", $this->oCart);
        $this->smarty->assign("membercountry", $_SESSION['countryid']);
    }

    protected function assignCurrency() {
        
        if (!array_key_exists('cid', $_GET))
        {
            // Use currency set in config file
            if (isset(\Config::$config->iCurrencyId))
            {
                $_SESSION['CurrencyID'] = \Config::$config->iCurrencyId;
            }
            else
            {
                // No currency in config, default to class constant
                $_SESSION['CurrencyID'] = self::DEFAULT_CURRENCY_ID;
            }
        }
        else
        {
            if (is_numeric($_GET['cid'])) {
                $_SESSION['CurrencyID'] = (int) $_GET['cid'];
            }
        }
        
        $this->oCurrency->switchCurrency((array_key_exists('CurrencyID', $_SESSION)) ? $_SESSION['CurrencyID'] : NULL);
    }

    protected function assignLanguage() {
        $sLangCode=(isset(\Config::$config->language))?\Config::$config->language:'en-gb';

        $aLangDetails = Language::getLanguageDetailsByCode($sLangCode);
        if ($aLangDetails['language_id']) {
            $bRedirect = false;
            $_SESSION['LanguageID'] = $aLangDetails['language_id'];
        }

        if (empty($aLangDetails)) {
            $aLangDetails = Language::getLanguageDetails((array_key_exists('LanguageID', $_SESSION)) ? $_SESSION['LanguageID'] : NULL);
        }

        $this->iLangID = $aLangDetails['language_id'];
        $this->sDBCharSet = $aLangDetails['dbcharset'];
        $this->sCharSet = $aLangDetails['charset'];
        $this->bDefaultLang = $aLangDetails['default'];
        $this->sLangCode = $aLangDetails['code'];

        \DAL::setCharSet($this->sDBCharSet);

        /*
        if ($bRedirect) {
            $this->saveSessionObjects();
            header("Status: 301 Moved Permanently", false, 301);
            parse_str($_SERVER['QUERY_STRING'], $aQueryString);
            unset($aQueryString['lid']);
            unset($aQueryString['lang']);
            $sQuery = str_replace("/index.php", "/", $_SERVER['SCRIPT_NAME']);
            if ($aQueryString)
                $sQuery .= "?" . http_build_query($aQueryString);
            header("Location: /" . $this->sLangCode . $sQuery);
            exit;
        }*/
    }

    public function assignFAQQuestions($iFAQCategoryID = NULL) {
        if (!$this->isCached()) {
            $oFAQ = new FAQs($this->iLangID);
            $this->smarty->assign("faqs", $oFAQ->getFAQs($iFAQCategoryID, NULL));
            if ($iFAQCategoryID) {
                $this->smarty->assign("faqcategory", $oFAQ->getFAQCategories($iFAQCategoryID));
            }
        }
    }

    protected function assignSmartyPageVars() {

        # Get the draws from the cache
        $aOffers = LLCache::get('LR-LotteryResults-getWinners');

        # Not in cache, query it
        if(empty($aOffers)) {

            $aOffers = LotteryResults::getWinners();
            foreach($aOffers as $key => $aOffer) {
                if(!empty($aOffer['play_page_id'])) {
                    $aOffers[$key]['url'] = Navigation::getPageURL($aOffer['play_page_id']);
                }
            }
            shuffle($aOffers);
            $aOffers = array_slice($aOffers, 0, 25);

            # Whack into LLCache
            LLCache::addObject('LR-LotteryResults-getWinners', $aOffers, 900);

        }

        # Store in Smarty
        $this->smarty->assign("offers", $aOffers);

        if(!$this->isCached()) {

            # Load the footer variables
            require_once(WEBSITEPATH . "system/includes/footer.php");

            # Get the footer links (oNavigation, getPagesByIDs is cached)
            $this->smarty->assign("footerlinks", $this->oNavigation->getPagesByIDs($aFooterPages));

            # Get each of the footer sections
            foreach ($aFooterSectionPages as $key => $aFooterSectionPage) {
                $aFooterSectionPages[$key] = $this->oNavigation->getPagesByIDs($aFooterSectionPage);
            }

            $this->smarty->assign("footersectionlinks", $aFooterSectionPages);

            # Cache the currency data too
            $this->smarty->assign("currency", $this->oCurrency->getSelectedCurrency());
            $this->smarty->assign("currencies", Currency::getCurrencies());

            $this->smarty->assign("menutitle", $this->oPage->getMenuTitle());
            $this->smarty->assign("pagetitle", $this->oPage->getPageTitle());
            $this->smarty->assign("browsertitle", $this->oPage->getBrowserTitle());
            $this->smarty->assign("pageid", $this->oPage->getPageID());
            $this->smarty->assign("sectionid", $this->oPage->getSectionID());
            $this->smarty->assign("sectiontitle", $this->oNavigation->getSectionTitle($this->oPage->getSectionID()));

            # Cache the language data
            $this->smarty->assign("languages", Language::getLanguages());
            $this->smarty->assign("language", Language::getLanguageDetails($this->iLangID));

            $this->smarty->assign("languageid", $this->iLangID);
            $this->smarty->assign("content", $this->oPage->getPageContent());
            $this->smarty->assign("keywords", $this->oPage->getSEOKeywords());
            $this->smarty->assign("description", $this->oPage->getSEODescription());
            $this->smarty->assign("breadcrumbs", $this->oNavigation->getBreadcrumbs($this->oPage->getLeftNode(), $this->oPage->getRightNode()));
            $this->smarty->assign("sections", $this->oNavigation->getSections());
            //$this->smarty->assign ("navigationmenu",$this->oNavigation->getNavigationMenu(NULL, true));
            $this->smarty->assign("sidenavigation", $this->oNavigation->getNavigationMenu($this->oPage->getSectionID(), true));
            $this->smarty->assign("charset", $this->sCharSet);
            $this->smarty->assign("pagedetails", $this->oPage->aPageDetails);
        }
        $this->smarty->assign("nationallottery", Lottery::NATIONALLOTTERY);
        $this->smarty->assign("euromillions", Lottery::EUROPEANLOTTERY);
        $this->smarty->assign("errors", $this->oError->getErrorList());
        $this->smarty->assign("notices", $this->oError->getNoticeList());
        $this->oError->clearErrors();
        $this->oError->clearNotices();
        $this->smarty->assign("currencyid", $this->oCurrency->getSelectedCurrencyID());
        $this->smarty->assign("xrate", $this->oCurrency->getSelectedCurrencyXRate());
        $this->smarty->assign("currencysymbol", $this->oCurrency->getSelectedCurrencySymbol());
        $this->assignUserDetails();
    }

    protected function assignAffiliate() {
        if(isset($_GET['aid'])) {
            Affiliates::setAffiliate($_GET['aid'], (isset($_GET['cid']) ? $_GET['cid'] : 0), (isset($_GET['bid']) ? $_GET['bid'] : 0));
        }
        if(isset($_GET['tk'])) {
            Affiliates::setAffiliate((string) urldecode($_GET['tk']), null, null, true);
        }
    }

    // Function deprecated in facour of new one below
    protected function checkMemberLocation() {
        $fMemberIP = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        if ($fMemberIP != $_SESSION['member_ip'] || !$this->oMember->CountryBlockSet()) {
            $_SESSION['countryid'] = $this->oMember->getCountryFromIP($fMemberIP, (isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : ''));
            $_SESSION['member_ip'] = $fMemberIP;
        }
    }

    /**
     * checkMemberLocationGeoIP()
     * 
     * Use the GeoIP library to get the country the customer is looking at the site from
     * 
     * Calls a private function
     * 
     * @return nothing
     */
    protected function checkMemberLocationGeoIP() {

        // Get the IP address - either forwarded from, one passed on the query string
        // or default to the remote host
        
        // NEED TO REMOVE GET OPTION BEFORE GO LIVE AS POTENTIAL SECURITY HOLE!
        // ONLY USED FOR TESTING AT THE MOMENT
        $fMemberIP = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : 
                     (isset($_GET['ip'])?$_GET['ip'] : $_SERVER['REMOTE_ADDR']);
        
//        $sss = print_r($_SESSION, true);
//        echo ("<!--- Sess 1" . $sss . " --->");
        
//        echo ("<!--- " . $fMemberIP . " --->");
        
        // Check on the following criteria:
 
        // 1) geoip_check flag has not been set
        if (!isset($_SESSION['geoip_check']))
        {
            $this->_checkMemberLocationGeoIP($fMemberIP);
        }
        
        // 2) session ip address and current IP address have changed
        if (isset($_SESSION['member_ip']))
        {
            if ($fMemberIP != $_SESSION['member_ip'])
            {           
                $this->_checkMemberLocationGeoIP($fMemberIP);
            }
        }
        
        // 3) session homesite (host) and current host have changed
        if (isset($_SESSION['homesite']))
        {
            if ($_SERVER['HTTP_HOST'] != $_SESSION['homesite'])
            {           
                $this->_checkMemberLocationGeoIP($fMemberIP);
            }
        }        
    }
    
    /**
     * _checkMemberLocationGeoIP()
     * 
     * Private function which does the IP checking
     * @param $sIP string the ip address
     * @return nothing
     */
    private function _checkMemberLocationGeoIP($sIP)
    {
        
        // Pass in IP address above to GeoIP class
        $oGeoIP = new GeoIP($sIP);

        // Only do the next set of checks if not localhost
        if (!$oGeoIP->isLocalhost())
        {
            // Get member country info - or false if country blocked/inactive
            $aMemberCountryInfo = $oGeoIP->geoIPMemberCountryInfo();

            if ($aMemberCountryInfo['blocked'] != 0 || $aMemberCountryInfo['isactive'] == 0) {
        
                /*if (strtolower($aMemberCountryInfo['code'])  == 'gb')
                {
                    $sHost = 'nath.' . self::DEFAULT_HOMESITE;
                }
                else
                {
                    $sHost = strtolower($aMemberCountryInfo['code']) . '.nath.' . self::DEFAULT_HOMESITE;
                }
        
                $this->_doRedirect("http://" . $sHost . "/blocked.php");*/
                $this->_doRedirect("blocked.php");
                               
            }
            else
            {

                //$_SESSION['countryid'] = $this->oMember->getCountryFromIP($fMemberIP, (isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : ''));
                $_SESSION['countryid'] = $aMemberCountryInfo['country_id'];
                $_SESSION['member_ip'] = $sIP;
                $_SESSION['iso2country'] = $aMemberCountryInfo['code'];

                 // New session variable to determine whether the customer has already been redirected
                if (!isset($_SESSION['geoip_check']))
                {
                    $_SESSION['geoip_check'] = 1;
                }

                // Redirect to ip country's page
                $this->redirectToCountryPage($aMemberCountryInfo);
            }
        }
        else
        {
            // Localhost, don't do any checking - original code
            $_SESSION['countryid'] = $this->oMember->getCountryFromIP($sIP, (isset($_SERVER['HTTP_CF_IPCOUNTRY']) ? $_SERVER['HTTP_CF_IPCOUNTRY'] : ''));
            $_SESSION['member_ip'] = $sIP;

            // New session variable to indicate if on a localhost setup
            if (!isset($_SESSION['is_localhost']))
            {
                $_SESSION['is_localhost'] = 1;
            }
        }        
    }
    
    /**
     * Redirect to the correct homesite for this country
     * Mainly used at initial "blocking" check
     * 
     * @param integer $iCountryID countryry id
     */
    protected function redirectToCountryPage($aMemberCountryInfo)
    {
        
        // Get a mapping for this country id, if exists
        //$aMapData = GeoIP::getCountryHomepageCurrencyMapping($aMemberCountryInfo['country_id']);
        
        // Write session data
        $this->writeMappingSessionInfo($aMemberCountryInfo); 
        
        
        // Redirect if we have mapping data
        //if ($aMapData != false)
        //{
            $this->_doRedirect();
        //}
    }
    
    /**
     * Write mapping data information to the session
     * 
     * @param array $aMapData array of mapping data if present, else false
     */
    protected function writeMappingSessionInfo($aMapData)
    {
        // Original code used database table
      /*
        if ($aMapData !== false)
        {
            // Row found, set session data from row
            
            // If country code is GB, just redirect to the main site
            if (strtolower($aMapData['iso_code'])  == 'gb')
            {
                $_SESSION['homesite'] = 'nath.' . self::DEFAULT_HOMESITE;
            }
            else
            {
                $_SESSION['homesite'] = strtolower($aMapData['iso_code']) . '.nath.' . self::DEFAULT_HOMESITE;
            }
            
            $_SESSION['lang_id'] = $aMapData['language_id'];
            $_SESSION['currency_id'] = $aMapData['currency_id'];
            
        }
        else 
        {
            // No row found, set session data from constants
            $_SESSION['homesite'] = $_SERVER['HTTP_HOST'];
            $_SESSION['lang_id'] = self::DEFAULT_LANG_ID;
            $_SESSION['currency_id'] = self::DEFAULT_CURRENCY_ID;
        }
 */
        // Language and currency will be defined in config file for each site
        
        // If country code is GB, just redirect to the main site
        if (strtolower($aMapData['code'])  == 'gb')
        {
            //$_SESSION['homesite'] = 'nath.' . self::DEFAULT_HOMESITE;
            $_SESSION['homesite'] = self::DEFAULT_HOMESITE;
        }
        else
        {
            //$_SESSION['homesite'] = strtolower($aMapData['code']) . '.nath.' . self::DEFAULT_HOMESITE;
            $_SESSION['homesite'] = strtolower($aMapData['code']) . self::DEFAULT_HOMESITE;
        }

        // Order of values is: supplied in $aMapData, config file, class constants
        $_SESSION['lang_id'] = (isset($aMapData['language'])?$aMapData['language'] : (isset(\Config::$config->iLanguageId)?\Config::$config->iLanguageId  : self::DEFAULT_LANG_ID));
        $_SESSION['currency_id'] = (isset($aMapData['currency'])?$aMapData['currency'] : (isset(\Config::$config->iCurrencyId)?\Config::$config->iCurrrencyId : self::DEFAULT_CURRENCY_ID));
                   
        // Save session objects
        $this->saveSessionObjects();
    }
    
    /**
     * Perform redirect 
     * 
     * @param string $sRedirect address to redirect to
     */
    protected function _doRedirect($sRedirect = null)
    {
        // COnstruct redirect string if one has not been passed in
        if (is_null($sRedirect))
        {
            // Build redirect string and redirect
            $sRedirect = "http://" . $_SESSION['homesite'] . $_SERVER['REQUEST_URI'];

        }

        header("Location: " . $sRedirect);
        exit;
        
    }
 
    
    public function assignCaptcha() {
        $this->smarty->assign("captcha", reCAPTCHA::recaptcha_get_html(CAPTCHAPUBLIC, $this->bSSL));
    }

    protected function getActiveLotteries() {
        $this->smarty->assign("activelotteries", Lottery::getEnabledLotteries());
    }

    public function assignActiveLotteryDetails() {

        if($this->hasLoadedGames) {
            return;
        }

        $includeGames = array(
            3, 4, 5, 6, 7
        );

        # We only want 3 - 7 (the main five games)
        if(!$this->requiresFullGameDetails) {
            $whereStatement = " AND g.id IN('".implode($includeGames, "', '")."')";
            $key = 'primaryGameList';
        } else {
            $whereStatement = '';
            $key = 'allGameList';
        }

        $gameLottos = LLCache::get('GE-'.$key);

        # Not in cache, query it
        if(empty($gameLottos)) {

            /**
             *
            ALTER TABLE  `game_engines` ADD  `css_id` VARCHAR( 30 ) NULL AFTER  `css_file` ,
            ADD  `css_logo` VARCHAR( 30 ) NULL AFTER  `css_id` ,
            ADD  `css_logo_small` VARCHAR( 30 ) NULL AFTER  `css_logo` ;
             * WHERE active = 1
             */
            $sSql = 'SELECT g.*, gc.handler FROM game_engines g
            LEFT JOIN game_engine_components gc ON (gc.fk_game_engine_id = g.id)
            WHERE active = 1
            '.$whereStatement.'
            GROUP BY g.id
            ORDER BY name ASC';
            $gameEngineChildren = DAL::executeQuery($sSql);

            if(!empty($gameEngineChildren)) {
                foreach($gameEngineChildren as $aChild){
                    $gameLottos[] = $aChild;
                }
            }

            # Whack into LLCache
            LLCache::add('GE-'.$key, $gameLottos, false, 900);

        }

        # Push to Smarty
        $this->smarty->assign("games", $gameLottos);

        # Get the draws from the cache
        $lotteryDrawPrizes = LLCache::get('GE-lotteryDraws');

        # Not in cache, query it
        if(empty($lotteryDrawPrizes)) {

            # Get the most-coming draw
            $sSql = "SELECT ld.*, cc.*,DATE_SUB(ld.drawdate, INTERVAL (ld.cutoff + (60 * (lc.timezone))) MINUTE) as cutoff FROM r_lottery_dates ld
            INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
            INNER JOIN currencies cc ON cc.currency_id = lc.fk_currency_id
            WHERE ld.drawdate > NOW()
            GROUP BY ld.fk_lottery_id
            ORDER BY ld.drawdate ASC";
            $lotteryDrawPrizesRaw = DAL::executeQuery($sSql);

            $lotteryDrawPrizes = array();
            if(!empty($lotteryDrawPrizesRaw)) {
                foreach($lotteryDrawPrizesRaw as $draw) {
                    $lotteryDrawPrizes[$draw['fk_lottery_id']] = $draw;
                }
            }

            # Whack into LLCache
            LLCache::add('GE-lotteryDraws', $lotteryDrawPrizes, false, 900);

        }

        # Store the prizes
        $this->smarty->assign("lotteryDrawPrizes", $lotteryDrawPrizes);

        # Get the draws from the cache
        $lotteryResults = LLCache::get('GE-lotteryResults');

        # Not in cache, query it
        if(empty($lotteryResults)) {

            # Get the most-coming draw
            $sSQL = "SELECT
                        ld.numbers,
                        ld.fk_lotterydate AS lotterydate,
                        ld.jackpot,
                        ld.draw,
                        ld.id,
                        lc.lottery_id,
                        ld.winnings
                    FROM lottery_draws ld
                    INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
                    INNER JOIN (SELECT fk_lottery_id,MAX(fk_lotterydate) AS fk_lotterydate FROM lottery_draws WHERE winnings != 'b:0;'
                        AND winnings IS NOT NULL
                        AND fk_lotterydate <= NOW() GROUP BY fk_lottery_id) AS pp ON pp.fk_lottery_id=ld.fk_lottery_id AND pp.fk_lotterydate=ld.fk_lotterydate
                    GROUP BY lottery_id
                    ORDER BY ld.fk_lotterydate DESC";
            $lotteryResultsRaw = DAL::executeQuery($sSQL);

            $lotteryResults = array();
            foreach($lotteryResultsRaw as $resultDraw) {
                $lotteryResults[$resultDraw['lottery_id']] = $resultDraw;
                $lotteryResults[$resultDraw['lottery_id']]['numbers'] = explode("|", $resultDraw['numbers']);
                $lotteryResults[$resultDraw['lottery_id']]['winnings'] = unserialize($resultDraw['winnings']);
            }

            # Whack into LLCache
            LLCache::add('GE-lotteryResults', $lotteryResults, false, 900);

        }

        # Store the prizes
        $this->smarty->assign("lotteryResults", $lotteryResults);

        /*
        $aLotteries = Lottery::getEnabledLotteries();
        foreach($aLotteries as $aLottery) {
            $this->assignLotteryDetails($aLottery['lottery_id']);
        }
        $this->sortLotteries();
        */

        $this->hasLoadedGames = true;

    }

}
