<?php
/**
 * Abstract class for smartyobjects.
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
abstract class SmartyObject {

    protected $smarty;
    protected $sTemplate;
    protected $sCacheName;
    protected $bDefaultLang = true;
    protected $sLangCode = NULL;

    protected function __construct($aTemplates, $iCache, $sCacheName = "", $bSSL = false) {

        if (file_exists("__config.php")) {
            require("__config.php");
        } elseif (file_exists("../__config.php")) {
            require("../__config.php");
        }
        if (substr($_SERVER['SERVER_NAME'], strlen($_SERVER['SERVER_NAME']) - strlen(".com")) != ".com") {
            $bSSL = false;
        }

        $this->sCacheName = $sCacheName;
        $this->bSSL = $bSSL;
        /*
          if ($bSSL ){
          $protocol="https";
          if ( !isset($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS']) != 'on' ) {
          header ('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
          exit();
          }
          }
          else {
          $protocol="http";
          if ( array_key_exists('HTTPS',$_SERVER) && ( isset($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS']) == 'on' ) ) {
          header ('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
          exit();
          }
          }
         *
         */

        $this->setupSmarty();
        if (!is_array($aTemplates))
            $this->sTemplate = $aTemplates;
        else
            $this->assignTemplates($aTemplates);

        $this->assignCache($iCache);
    }

    protected function displayPage() {
        $this->smarty->display($this->sTemplate, $this->sCacheName);
    }

    protected function isCached() {
        return $this->smarty->is_cached($this->sTemplate, $this->sCacheName);
    }

    protected function setupSmarty() {

        if (file_exists(SMARTY_CLASS)) {
            require(SMARTY_CLASS);
        }

	    # Keeping as $smarty for now
	    $this->smarty = new LLView();

		/*
        $this->smarty->compile_dir = Config::$config->sSmartyCompileDir;
        $this->smarty->cache_dir = Config::$config->sSmartyCacheDir;
        $this->smarty->template_dir = TEMPLATEPATH; // Setting Template Directory
        //SETUP PLUGIN {$dynamic}
        $this->smarty->register_block("dynamic", array("SmartyObject", "smarty_block_dynamic"), false);
        $this->smarty->register_modifier("doexchange", array("SmartyObject", "smarty_modifier_doexchange"));
        $this->smarty->register_function("translate", array("SmartyObject", "smarty_function_translate"));
        $this->smarty->register_modifier("addurlparam", array("SmartyObject", "smarty_modifier_addurlparam"));
		*/

        $this->smarty->assign("sitename", WEBSITETITLE);
        $sLangFolder = $this->sLangCode . "/";
        $this->smarty->assign("isssl", $this->bSSL);
        if ($this->bSSL) {
            $this->smarty->assign("root", "/");
            $this->smarty->assign("base", "/");
            $this->smarty->assign("mediabase", '/skins/'.Config::$config->sWebsiteName.'/assets/media/');
            $this->smarty->assign("scriptbase", '/skins/'.Config::$config->sWebsiteName.'/assets/scripts/');
            $this->smarty->assign("stylesheetbase", '/skins/'.Config::$config->sWebsiteName.'/assets/styles/');
            $this->smarty->assign("commonbase", "/");
            $this->smarty->assign("systembase", "/system/");

            // DO LOGIC FOR canonical URL - EXCULSIVE SO MUST REMEMBER TO ADD - TODO
            parse_str($_SERVER['QUERY_STRING'], $aQueryString);
            $sNewQuery = str_replace("/index.php", "/", $_SERVER['SCRIPT_NAME']);
            $aNewQuery = array();
            if (!empty($aQueryString['draw']) && strpos($sNewQuery, "php") && strpos($sNewQuery, "results")) {
                $aNewQuery['draw'] = $aQueryString['draw'];
            }
            if (!empty($aNewQuery))
                $sNewQuery .= "?" . http_build_query($aNewQuery);
            $this->smarty->assign("canonicalurl", "//" . WEBSITEURL . $this->sLangCode . $sNewQuery);
        }
        else {

            $this->smarty->assign("root", "/");
            $this->smarty->assign("base", "/" );
            $this->smarty->assign("mediabase", '/skins/'.Config::$config->sWebsiteName.'/assets/media/');
            $this->smarty->assign("scriptbase", '/skins/'.Config::$config->sWebsiteName.'/assets/scripts/');
            $this->smarty->assign("stylesheetbase", '/skins/'.Config::$config->sWebsiteName.'/assets/styles/');
            $this->smarty->assign("commonbase", "/");
            $this->smarty->assign("systembase", "/system/");

            // DO LOGIC FOR canonical URL - EXCULSIVE SO MUST REMEMBER TO ADD - TODO
            parse_str($_SERVER['QUERY_STRING'], $aQueryString);
            $sNewQuery = str_replace("/index.php", "/", $_SERVER['SCRIPT_NAME']);
            $aNewQuery = array();
            if (!empty($aQueryString['draw']) && strpos($sNewQuery, "php") && strpos($sNewQuery, "results")) {
                $aNewQuery['draw'] = $aQueryString['draw'];
            }
            if (!empty($aNewQuery))
                $sNewQuery .= "?" . http_build_query($aNewQuery);
            $this->smarty->assign("canonicalurl", "http://" . WEBSITEURL . $this->sLangCode . $sNewQuery);
        }
    }

    //SMARTY FUNCTIONS
    protected function assignCache($iCache) {
        if (!is_null($iCache)) {
            //Disable Smarty Caching from here
            $this->smarty->caching = SMARTYCACHE;
            $this->smarty->cache_lifetime = $iCache * 60;
        }
    }

    protected function assignTemplates($aTemplates) {
        foreach ($aTemplates as $key => $value) {
            if ($key == "main")
                $this->sTemplate = $value;
            else
                $this->smarty->assign($key, $value);
        }
    }

    public static function smarty_block_dynamic($param, $content, &$smarty) {
        return $content;
    }

    public function smarty_modifier_doexchange($value, $xrate) {
        return Currency::convertFromBase($value, $xrate);
    }

    public static function smarty_function_translate($params, &$smarty) {
        $sReturn = $params['default'];

        if (!empty($params['id']) && !empty($params['langid'])) {
            $iMiscTranslationID = $params['id'];
            $iLangID = $params['langid'];
            $sSQL = "SELECT mtl.title
					FROM misctranslations_cmn mtc
					INNER JOIN misctranslations_lang mtl ON mtl.fk_translation_id = mtc.translation_id
					INNER JOIN languages l ON l.language_id = mtl.fk_language_id
					WHERE mtc.translation_id = {$iMiscTranslationID}
					AND
						(
							l.language_id = {$iLangID}
							OR
							l.default = 1
						)
					AND mtc.is_active = 1
					AND mtl.is_active = 1
					AND l.is_active = 1
					ORDER BY l.default ASC";
            $sResponse = DAL::executeGetOne($sSQL);
            if ($sResponse) {
                $sReturn = $sResponse;
            }
        }
        if ($params['assign']) {
            $smarty->assign($params['assign'], $sReturn);
        } else {
            return $sReturn;
        }
    }

    public static function smarty_modifier_addurlparam($url, $param, $value = NULL, $replace = false) {
        if ($value != NULL) {
            // we were passed the parameter and value as
            // separate plug-in parameters, so just apply
            // them to the URL.
            $url = self::addURLParameter($url, $param, $value, $replace);
        } elseif (is_array($param)) {
            // we were passed an assoc. array containing
            // parameter names and parameter values, so
            // apply them all to the URL.
            foreach ($param as $paramName => $paramValue) {
                $url = self::addURLParameter($url, $paramName, $paramValue, $replace);
            }
        } else {
            // was passed a string containing at least one parameter
            // so parse out those passed and apply them separately
            // to the URL.
            $numParams = preg_match_all('/([^=?&]+?)=([^&]*)/', $param, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $url = self::addURLParameter($url, $match[1], $match[2], $replace);
            }
        }
        return $url;
    }

    protected static function addURLParameter($url, $paramName, $paramValue, $bReplace = false) {

        $newurl = str_replace("&" . $paramName . "=" . $paramValue, '', $url);
        $newurl = str_replace($paramName . "=" . $paramValue, '', $newurl);

        if ($newurl != $url && $bReplace == false) {
            return $newurl;
        }
        if ($bReplace == true) {
            if (preg_match('/[?&](' . $paramName . ')=[^&]*/', $newurl)) {
                // parameter is already defined in the URL, so
                // replace the parameter value, rather than
                // append it to the end.
                $newurl = preg_replace('/([?&]' . $paramName . ')=[^&]*/', '$1=' . $paramValue, $newurl);
            } elseif (!empty($paramValue)) {
                // can simply append to the end of the URL, once
                // we know whether this is the only parameter in
                // there or not.
                $newurl .= strpos($newurl, '?') ? '&' : '?';
                $newurl .= $paramName . '=' . $paramValue;
            }
        } else {
            if (!empty($paramValue)) {
                // can simply append to the end of the URL, once
                // we know whether this is the only parameter in
                // there or not.
                $newurl .= strpos($newurl, '?') ? '&' : '?';
                $newurl .= $paramName . '=' . $paramValue;
            }
        }
        return $newurl;
    }

}
