<?php

define('HELP_SECTION', 141);
define('ABOUT_SECTION', 5);

/**
 * Not sure about this one at the moment, appears to just be used for generic pages
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */

class TextPage extends Page
{
	public function __construct($iPageID, $sTemplate, $bSSL = false) {
		parent::__construct($iPageID, $sTemplate, $bSSL);
	}

	public function displayPage(){
		parent::displayPage();
	}

        public function assignContent($sContent){
            $this->smarty->assign("content2", $sContent);
        }

	public function assignSiteMap()
	{
        if ( !$this->isCached() )
		{
			$aSiteMap = $this->oNavigation->getNavigationMenu(NULL, false, 0, true);
			$this->smarty->assign("sitemap", $aSiteMap);
			unset($aSiteMap);
		}
	}

	public function assignSideNavigation()
	{
		{

			if($this->oPage->aPageDetails['section_id'] == HELP_SECTION || $this->oPage->aPageDetails['section_id'] == ABOUT_SECTION) {

				$aSiteMap = $this->oNavigation->getNavigationMenu(HELP_SECTION, false, 2, true);
				$this->smarty->assign("sidenav", $aSiteMap);

				$aSiteMap = $this->oNavigation->getNavigationMenu(ABOUT_SECTION, false, 2, true);
				$this->smarty->assign("sidenavExtra", $aSiteMap);

			} else {

				$aSiteMap = $this->oNavigation->getNavigationMenu($this->oPage->aPageDetails['section_id'], false, 2, true);
				$this->smarty->assign("sidenav", $aSiteMap);

			}
			unset($aSiteMap);
		}
	}

	public function geoWarnPage()
	{
		$this->smarty->assign("memberareapage", 1 );
		if ( $_SESSION['legalagreement'] == true )
		{
			header("location: /" . $this->sLangCode . "/index.php");
			exit;
		}
		elseif ( $_POST['a'] == 'geowarn' )
		{
			if ( $_POST['accept'] )
			{
				$_SESSION['legalagreement'] = true;
				$sRedirectURL = "/index.php";
				if ( $_SESSION['goingtopage'] )
				{
					$sPageURL = Navigation::getPageURL($_SESSION['goingtopage']);
					if ( $sPageURL )
					{
						$sRedirectURL = "/" . $sPageURL;
						unset($sPageURL);
					}
				}
				header("location: /" . $this->sLangCode . "{$sRedirectURL}");
				exit;

			}
			else
			{
				$this->oMember->Logout();
				$this->saveSessionObjects();
				header("location: /" . $this->sLangCode . "/index.php");
				exit;
			}
		}
	}

	public function sendInlineContactForm() {

		if(isset($_POST['contactUsForm'])) {

			$_POST['firstName'] = trim($_POST['firstName']);
			$_POST['lastName'] = trim($_POST['lastName']);
			$_POST['email'] = trim($_POST['email']);

			# Valid message, else just show the form + errors
			if(!empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['email'])) {

				$oEmail = new Mailer();
				$oEmail->setSubject(VCMS::get('Contact.Email.Subject'));
				$oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
				$oEmail->IsHTML(true);
				$oEmail->addTo(VCMS::get('Contact.Email.SendTo'), WEBSITETITLE);

				# Send the message
				$oEmail->sendEmail(VCMS::get('Contact.Email.Body', array(
							'FirstName' => $_POST['firstName'],
							'LastName' => $_POST['lastName'],
							'Email' => $_POST['email'],
							'Phone' => $_POST['phone'],
							'Country' => $_POST['country'],
							'Message' => $_POST['text']
						)
					)
				);

				# Redirect
				header('Location: /about/contact-thank-you');

			}

		}

	}

	public function processActions()
	{
		if ( $_POST['action'] )
		{
			switch ( $_POST['action'] )
			{
				case 'registerAffiliate' :
						$aAffiliateDetails = $_POST;
						if ( Affiliates::addAffiliate($aAffiliateDetails, $this->oError) )
						{
							header("location: /" . $this->sLangCode . "/about/affiliates-receipt.php");
							exit;
						}
						else
						{
							$this->smarty->assign("registerform", $aAffiliateDetails);
						}
					break;
			}
		}
	}
}
?>
