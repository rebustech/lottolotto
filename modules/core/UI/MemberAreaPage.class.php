<?php

/**
 * Smarty object used to handle the displaying of member details
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
class MemberAreaPage extends Page {

    protected $iPage = 1;
    protected $iStart = 0;
    protected $iLimit = 20;

    public function __construct($iPageID, $sTemplate, $bSSL = true, $bShowLoginPage = false) {
        parent::__construct($iPageID, $sTemplate, $bSSL);

        if (!$bShowLoginPage || $this->oMember->validateMember()) {
            $this->validateLoggedIn();
            $this->assignMemberFullDetails();
        }
        if ($this->oMember->isCountryBlocked() && $bShowLoginPage !== 2) {
            if (!$_SESSION['legalagreement']) {
                $_SESSION['goingtopage'] = $this->oPage->getPageID();
                $this->saveSessionObjects();
                header("location: /geowarn.php");
                exit;
            }
        }
    }

    public function displayPage() {
        $this->smarty->assign("memberareapage", 1);
        parent::displayPage();
    }

    public function assignTransactionsList() {
        $iPage = (int) $_GET['page'];
        if ($iPage) {
            $this->iStart = ($this->iLimit * ($iPage - 1));
            $this->iPage = $iPage;
        }
        $this->smarty->assign("membertransactions", $this->oMember->getTransactions($iTotalTransactions, $this->iStart, $this->iLimit));
        if ($iTotalTransactions > $this->iLimit) {
            $this->smarty->assign("typetitle", "Transactions");
            $this->smarty->assign("page", ($this->iPage));
            $this->smarty->assign("total", $iTotalTransactions);
            $this->smarty->assign("start", ($this->iStart + 1));
            if (( $this->iStart + $this->iLimit ) > $iTotalTransactions) {
                $this->smarty->assign("end", $iTotalTransactions);
            } else {
                $this->smarty->assign("end", $this->iStart + $this->iLimit);
            }
            $this->smarty->assign("noofpages", ceil($iTotalTransactions / $this->iLimit));
        }
    }

	public function getLatestTicket() {

		$iTotalTickets = 0;

		# Get a single ticket
		if(!empty($_GET['ticketId'])) {

			$latestTicket = array();
			$tickets = $this->oMember->getBookingItems($this->iLangID, $iTotalTickets, $_GET['ticketId'], 0, 1, 'ticket');

			# Grab the first one
			if(!empty($tickets)) {
				$latestTicket = $tickets[0];
			}

		} else {

			$latestTicket = array();
			$tickets = $this->oMember->getBookingOrderItems($this->iLangID, $iTotalTickets, 0, 1, 'all');

			# Grab the first one
			if(!empty($tickets)) {
				$latestTicket = $tickets[0];
			}

		}

		# Store in view
		$this->smarty->assign("latestTicket", $latestTicket);

	}

    public function assignTicketsList() {

        # Attempt to get the reference
        if(!empty($_GET['orderItem'])) {

            # Query for a valid booking
            $booking = $this->oMember->getBookingByOrderItem($this->iLangID, $_GET['orderItem']);

            # We found it, now get the booking_items
            if(!empty($booking)) {

                # Whack into Smarty
                $this->smarty->assign("booking", $booking);

                # Change template
                $this->sTemplate = 'account-booking';

                # Get everything
                $this->assignBookingItemList($booking['fk_booking_id']);
                return;
            }
        }

        # Attempt to get the reference
        if(!empty($_GET['bookingId'])) {

            $iTotalTickets = 0;

            # Query for a valid booking
            $booking = $this->oMember->getBookingItems($this->iLangID, $iTotalTickets, $_GET['bookingId'], 0, 1);

            # We found it, now get the booking_items
            if(!empty($booking)) {

                # Whack into Smarty
                $this->smarty->assign("booking", $booking);

                # Don't cache these pages
                $this->smarty->assign("caching", false);
                $this->smarty->assign("cache", false);

                # Change template
                $this->sTemplate = 'account-booking';

                # Get everything
                $this->assignBookingItemList($_GET['bookingId']);
                return;
            }
        }

		# Get all booking_items
		$this->assignBookingOrderItemList();

		/*
        $iPage = (int) $_GET['page'];
        if($iPage) {
            $this->iStart = ($this->iLimit * ($iPage - 1));
            $this->iPage = $iPage;
        }
        $this->smarty->assign("memberBets", $this->oMember->getTickets($this->iLangID, $iTotalTickets, $this->iStart, $this->iLimit));
        if ($iTotalTickets > $this->iLimit) {
            $this->smarty->assign("typetitle", "Tickets");
            $this->smarty->assign("page", ($this->iPage));
            $this->smarty->assign("total", $iTotalTickets);
            $this->smarty->assign("start", ($this->iStart + 1));
            if (( $this->iStart + $this->iLimit ) > $iTotalTickets) {
                $this->smarty->assign("end", $iTotalTickets);
            } else {
                $this->smarty->assign("end", $this->iStart + $this->iLimit);
            }
            $this->smarty->assign("noofpages", ceil($iTotalTickets / $this->iLimit));
        }
		*/

    }

	 public function assignBookingOrderItemList() {
		$iTotalTickets = 0;
        $iPage = (int) $_GET['page'];
        if ($iPage) {
            $this->iStart = ($this->iLimit * ($iPage - 1));
            $this->iPage = $iPage;
        }

        # Query the table
        $status=(isset($_GET['status']) ? filter_input(INPUT_GET, 'status', FILTER_SANITIZE_STRING) : 'all');
        $month=(isset($_GET['month']) ? intval(filter_input(INPUT_GET, 'month', FILTER_SANITIZE_NUMBER_INT)) : date('m'));

        $bookingItems = $this->oMember->getBookingItems($this->iLangID, $iTotalTickets, $bookingID, $this->iStart, $this->iLimit, $status,$month);
        # Get all booking_items for the bookingId
        $this->smarty->assign("bookingItems", $bookingItems);

        # Get the last 6 months of months
        $iCurrentMonth=date('m');
        for($iMonth=0;$iMonth<6;$iMonth++){
            $aNewMonth=explode(' ',strftime('%m %B %Y',strtotime(' - '.$iMonth.' months')),2);
            $oMonthInfo=new stdClass();
            $oMonthInfo->num=$aNewMonth[0];
            $oMonthInfo->name=$aNewMonth[1];
            $oMonthInfo->selected=($aNewMonth[0]==$month);
            $aMonths[$aNewMonth[0]]=$oMonthInfo;
        }
        $this->smarty->assign("monthList", $aMonths);


        $this->smarty->assign("bookingItems", $this->oMember->getBookingOrderItems($this->iLangID, $iTotalTickets, $this->iStart, $this->iLimit, $status,$month));
        if ($iTotalTickets > $this->iLimit) {
            $this->smarty->assign("typetitle", "Tickets");
            $this->smarty->assign("page", ($this->iPage));
            $this->smarty->assign("total", $iTotalTickets);
            $this->smarty->assign("start", ($this->iStart + 1));
            if (( $this->iStart + $this->iLimit ) > $iTotalTickets) {
                $this->smarty->assign("end", $iTotalTickets);
            } else {
                $this->smarty->assign("end", $this->iStart + $this->iLimit);
            }
            $this->smarty->assign("noofpages", ceil($iTotalTickets / $this->iLimit));
        }

		# Assign the list of status
		$this->assignStatusList();

    }

	public function assignBookingItemList($bookingID) {

		$iTotalTickets = 0;
        $iPage = (int) $_GET['page'];
        if ($iPage) {
            $this->iStart = ($this->iLimit * ($iPage - 1));
            $this->iPage = $iPage;
        }

        # Query the table
        $status=(isset($_GET['status']) ? filter_input(INPUT_GET, 'status', FILTER_SANITIZE_STRING) : 'all');
        $month=(isset($_GET['month']) ? filter_input(INPUT_GET, 'month', FILTER_SANITIZE_NUMBER_INT) : date('m'));

        $bookingItems = $this->oMember->getBookingItems($this->iLangID, $iTotalTickets, $bookingID, $this->iStart, $this->iLimit, $status,$month);
        # Get all booking_items for the bookingId
        $this->smarty->assign("bookingItems", $bookingItems);

        # Get the last 6 months of months
        $iCurrentMonth=date('m');
        for($iMonth=0;$iMonth<6;$iMonth++){
            $aNewMonth=explode(' ',strftime('%m %B %Y',strtotime(' - '.$iMonth.' months')),2);
            $oMonthInfo->num=$aNewMonth[0];
            $oMonthInfo->name=$aNewMonth[1];
            $oMonthInfo->selected=($aNewMonth[0]==$month);
            $aMonths[$aNewMonth[0]]=$oMonthInfo;
        }
        $this->smarty->assign("monthList", $aMonths);

        # Need some pagination
        if($iTotalTickets > $this->iLimit) {
            $this->smarty->assign("typetitle", "Tickets");
            $this->smarty->assign("page", ($this->iPage));
            $this->smarty->assign("total", $iTotalTickets);
            $this->smarty->assign("start", ($this->iStart + 1));
            if (( $this->iStart + $this->iLimit ) > $iTotalTickets) {
                $this->smarty->assign("end", $iTotalTickets);
            } else {
                $this->smarty->assign("end", $this->iStart + $this->iLimit);
            }
            $this->smarty->assign("noofpages", ceil($iTotalTickets / $this->iLimit));
        }

		# Assign the list of status
		$this->assignStatusList();

    }

	public function assignStatusList() {

		$statusList = LLCache::get('statusList');

		# Not in cache, query it
		if(empty($statusList)) {

			$statusList = DAL::executeQuery("SELECT * FROM status");

			# Whack into LLCache
			LLCache::add('statusList', $statusList, false, 900);

		}

		 $this->smarty->assign("statusList", $statusList);

	}

    public function showLoginPod() {
        if (($this->oMember->validateMember()) && (empty($_POST['ajax']))) {
            if(empty($_GET['modal'])) {
                header("location: /myaccount.php");
                exit;
            } else {
                echo json_encode(array('loggedIn' => true, 'redirect' => '/myaccount'));
                exit;
            }

        }
    }

    protected function validateLoggedIn() {
        if (!$this->oMember->validateMember()) {
            $this->oMember->setRedirectPage($this->oPage->getPageID());
            $this->saveSessionObjects();
            header("location: /?loginModal=1"); //Was login.php
            exit;
        }
    }

    public function updateDetails() {

		# No registering session
		if(empty($_SESSION['registeringUserId'])) {
			$this->register(true);
		}

		# No user
        if (!$this->oMember->getMemberID()) {
            $this->oMember->iMemberID = $_SESSION['registeringUserId'];
        }

        # Define defaults
        $aMemberDetails = $_POST;
        $bValid = true;

        # Clear current error queue
        $this->oError->clearMessages();

		/**
		 * @requires ALTER TABLE  `members` ADD  `title` VARCHAR( 10 ) NULL ;
		 */
        if(!isset($aMemberDetails['ajax'])) {
    		if(!$aMemberDetails['title']) {
    			$bValid = false;
    			$this->oError->addError(VCMS::get('Error.Req.Title'));
    		}
        }

		# Run validation
		if (!$aMemberDetails['firstname']) {
			$bValid = false;
			$this->oError->addError(VCMS::get('Error.Req.FirstName'));
		}

		if (!$aMemberDetails['lastname']) {
			$bValid = false;
			$this->oError->addError(VCMS::get('Error.Req.Surname'));
		}

		/**
		 * @requires ALTER TABLE  `members` ADD  `dob` DATE NULL ;
		 */
		if(!$aMemberDetails['dob']) {
			$bValid = false;
			$this->oError->addError(VCMS::get('Error.Req.DOB'));
		} else {

			# Check their age
			if(class_exists('DateTime')) {
				$tz  = new DateTimeZone('Europe/London');
				$ageDate = DateTime::createFromFormat('Y-m-d', $aMemberDetails['dob'], $tz);
                if(is_object($ageDate)){
                    $age = $ageDate->diff(new DateTime('now', $tz))->y;
                } else {
                    $age=0;
                }
			} else {
				$birthDate = explode('-', $aMemberDetails['dob']);
				$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md") ? ((date("Y") - $birthDate[0]) - 1) : (date("Y") - $birthDate[0]));
			}

			# Less than 18?
			if($age < 18) {
				$bValid = false;
				$this->oError->addError(VCMS::get('Error.Underage'));
			}

		}

		/**
		 * ALTER TABLE `members` ADD `county` VARCHAR(250) NOT NULL AFTER `zip`;
		 *
		if (!$aMemberDetails['country']) {
			$bValid = false;
			$this->oError->addError("Country is Required.");
		}
		*/

        if(!isset($aMemberDetails['ajax']) && !isset($_POST['ajax'])) {

            if(!$aMemberDetails['address1']) {
                $bValid = false;
                $this->oError->addError(VCMS::get('Error.Req.Address1'));
            }

    		/*
    		if (!$aMemberDetails['address2']) {
    			$bValid = false;
    			$this->oError->addError("Address 2 is required.");
    		}
    		*/

    		if (!$aMemberDetails['city']) {
    			$bValid = false;
    			$this->oError->addError(VCMS::get('Error.Req.City'));
    		}

    		if (!$aMemberDetails['zip']) {
    			$bValid = false;
    			$this->oError->addError(VCMS::get('Error.Req.Zip'));
    		}

    		if(!empty($aMemberDetails['country'])) {
    			$aMemberDetails['fk_country_id'] = $aMemberDetails['country'];
    		}

    		if (!$aMemberDetails['fk_country_id']) {
    			$bValid = false;
    			$this->oError->addError(VCMS::get('Error.Req.Country'));
    		}

        }

		if (!$aMemberDetails['contact']) {
			$bValid = false;
			$this->oError->addError(VCMS::get('Error.Req.Phone'));
		} else {

            // Check phone number is correct
            // we can improve on this further down the line
            if (!preg_match('/^[0-9\-\+]*$/', $aMemberDetails['contact'])) {
     			$bValid = false;
    			$this->oError->addError(VCMS::get('Error.Req.Phone.Digits'));
            }

            // Check phone number length is acceptable
            // Again, this is just a simple length check - we can improve on validity checking later
            $iContactLength = strlen($aMemberDetails['contact']);

            if ($iContactLength < 10 || $iContactLength > 26) {
      			$bValid = false;
    			$this->oError->addError(VCMS::get('Error.Req.Phone.Length'));
            }
        }

		# Update the user details
		if ($bValid) {

            # Login the user
            if(isset($aMemberDetails['ajax'])) {
                $this->performLogin($aMemberDetails['email'], $aMemberDetails['password'], true, false);
            }

            # Update the user
			$bValid = $this->oMember->saveMemberDetails($aMemberDetails);

			if (!$bValid) {
				$this->oError->addError(VCMS::get('Error.SaveMemberDetails'));
			} else {
				// Step 1 completed at this point,
				// so send out the new registration email to the customer
				$sEmail = $aMemberDetails['email'];

				$fBalance = ($aMemberDetails['balance']==""?"0.00":$aMemberDetails['balance']);

                // Return the balance value to 2 decimal places, rounded down
                // This will then be further formatted to 2display as 2 decimal places
                $fBalance = floor($fBalance * 100) / 100;

                /*
				$aPersonalisationData = array('username'  => $aMemberDetails['email'],
                                                              'firstname' => $aMemberDetails['firstname'],
                                                              'current balance' => number_format_locale($fBalance, 2, '.', ','),
                                                              'member_id' => $this->oMember->iMemberID,
                                                              'email_ref' => 'new_registration');
*/
                                $aPersonalisationData = array('firstname' => $aMemberDetails['firstname'],
                                                              'current balance' => number_format_locale($fBalance, 2, '.', ','),
                                                              'member_id' => $this->oMember->iMemberID,
                                                              'email_ref' => 'new_registration');

				Email::sendNewRegistrationEmail($this->oMember->iMemberID, $sEmail, $aPersonalisationData);
			}
		}

        # Success at the end?
        if ($bValid) {

            /**
             * @todo, determine what process the user is in
             * 1) new user, no order => thank you + welcome modal
             * 2) new user, with order => transferring you modal => add funds
             */
            ob_start();

            $this->assignActiveLotteryDetails();

            $this->smarty->assign('email', $aMemberDetails['email']);

            if($this->oCart->getCartTotal() > 0) {
                $returnModal = $this->smarty->display('pods/registration/transferring-modal');
            } else {
                $returnModal = $this->smarty->display('pods/registration/welcome-modal');
            }

            $responseModal = ob_get_clean();

            if(isset($aMemberDetails['ajax'])) {
				header('Content-Type: application/json');
				echo json_encode(array('error' => false, 'saved' => true, 'userID' => $this->oMember->getMemberID(), 'contents' => $responseModal));
				exit;
			} else {
                $this->oMember->setRedirectPage('/addFunds');
                if(!$this->performLogin($aMemberDetails['email'], $aMemberDetails['password'], true, true)) {
                    header("location: /");
                    exit;
                }
            }
        } else {
            if(isset($aMemberDetails['ajax'])) {
				header('Content-Type: application/json');
				echo json_encode(array('error' => true, 'data' => $this->oError->getErrorList(), 'errorItem' => serialize($this->oError)));
				exit;
			 }
            $this->smarty->assign("registerform", $aMemberDetails);
        }
    }

	/**
	 * checkRegister: Used to inline validate the email address / password combo
	 * doesn't save the user at all
	 *
	 * @return JSON
	 */
	public function checkRegister() {

		# Store defaults
        $aMemberDetails = $_POST;
        $bValid = true;

        # Clear current error queue
        $this->oError->clearMessages();

        # Validation checks
        if (!$this->oMember->checkUsernameAvailable($aMemberDetails['email'])) {
            $bValid = false;
            $this->oError->addError("E-Mail address is already in use.");
        }
        if (strlen($aMemberDetails['password']) < 6) {
            $bValid = false;
            $this->oError->addError("Password too short. Needs to be 6 characters or more.");
        }
        if ($aMemberDetails['password'] != $aMemberDetails['password2']) {
            $bValid = false;
            $this->oError->addError("Password doesn't match.");
        }
        if (empty($aMemberDetails['email']) || !filter_var($aMemberDetails['email'], FILTER_VALIDATE_EMAIL)) {
            $bValid = false;
            $this->oError->addError("Email address is invalid.");
            unset($aMemberDetails['email']);
        }

		 # Did it pass?
        if (!$bValid) {
			header('Content-Type: application/json');
			echo json_encode(array('error' => true, 'data' => $this->oError->getErrorList()));
        } else {
			header('Content-Type: application/json');
			echo json_encode(array('error' => false));
		}

		exit;

	}

	/**
	 * register: The main inline registration, creates the user based on email / password
	 * combination.
	 *
	 */
    public function register($internalRegister = false) {

        # Store defaults
        $aMemberDetails = $_POST;
        $bValid = true;

        # Clear current error queue
        $this->oError->clearMessages();

        # Validation checks
        if (!$this->oMember->checkUsernameAvailable($aMemberDetails['email'])) {
            $bValid = false;
            $this->oError->addError("E-Mail address is already in use.");
        }
        if (strlen($aMemberDetails['password']) < 6) {
            $bValid = false;
            $this->oError->addError("Password too short. Needs to be 6 characters or more.");
        }
        if ($aMemberDetails['password'] != $aMemberDetails['password2']) {
            $bValid = false;
            $this->oError->addError("Password doesn't match.");
        }
        if (empty($aMemberDetails['email']) || !filter_var($aMemberDetails['email'], FILTER_VALIDATE_EMAIL)) {
            $bValid = false;
            $this->oError->addError("Email address is invalid.");
            unset($aMemberDetails['email']);
        }

        # Save the new user, get the ID
        if($bValid) {

            # Register on email / password
            if(!isset($aMemberDetails['checkOnly'])) {

                $userID = $this->oMember->addMemberSlim($aMemberDetails['email'], $aMemberDetails['password']);

                # Was it all valid?
                if($userID !== false) {

                    $bValid = true;

                    if(!isset($aMemberDetails['ajax'])) {
                        $this->performLogin($aMemberDetails['email'], $aMemberDetails['password'], true, false);
                    }

                    /**
                     * @todo, prepare to handle the Silverpop messages at this point we have a user
                     * registered with an email and password.
                     *
                     * Step two collates more user information though we may get drop-outs.
                     */
                } else {
                    $bValid = false;
                    $this->oError->addError("Problem encountered while adding a member.");
                }

                # Double-tap checking
                if ($bValid) {

                    $_SESSION['registeringUserId'] = $userID;

                    # AJAX response back
                    if(!$internalRegister && isset($aMemberDetails['ajax'])) {
                        header('Content-Type: application/json');
                        echo json_encode(array('error' => false, 'saved' => true, 'user' => $userID));
                        exit;
                    }
                }
            } else {

                # Valid, check again
                if(!$internalRegister && $bValid) {
                    header('Content-Type: application/json');
                    echo json_encode(array('error' => false, 'check' => true));
                    exit;
                }
            }
        }

        # Did it pass?
        if (!$bValid) {

            # Handle AJAX responses
            if (isset($aMemberDetails['ajax'])) {
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'data' => $this->oError->getErrorList()));
                exit;
            }

            # Remove password fields
            unset($aMemberDetails['password']);
            unset($aMemberDetails['password2']);

            # Back to registration page
            $this->smarty->assign("registerform", $aMemberDetails);
        }
    }

    public function performLogin($sUsername, $sPassword, $bRemember = false, $redirect = true) {

        $isAjax = false;

        # AJAX, send back status
        if (isset($_POST['ajax'])) {

            $isAjax = true;

            # Clear current error queue
            $this->oError->clearMessages();
        }
        
        # Try to login
        $bLoggedIn = $this->oMember->performLogin($sUsername, $sPassword, $bRemember);
                     
        # Did we login?
        if ($bLoggedIn) {

            // Perform redirect if member's signed up country is different to the current session
            $aMemberDetails = $this->oMember->getMemberDetailsArray(); 
            
//            $sss = print_r($aMemberDetails, true);
//           echo ("<!--- xxx " . $sss . " --->");
            
            $this->saveSessionObjects();
            
            # @todo, verify this is still correct
            $sRedirectURL = "/checkout";
            if($_POST['login_source'] === 'basket') {
                $sRedirectURL = "/checkout.php?force=1";
            }

            if(!empty($_SESSION['registeringUserId'])) {
                $sRedirectURL = "/addFunds";
                unset($_SESSION['registeringUserId']);
            }

            # Replace the default page

            if ($this->oMember->getRedirectPage()) {
                $sPageURL=$this->oMember->getRedirectPage();
                if(is_numeric($iMemberHomePageId)){
                    $sPageURL = Navigation::getPageURL($sPageURL);
                }
                if ($sPageURL) {
                    $sRedirectURL = "/" . $sPageURL;
                    unset($sPageURL);
                }
            }

            // Only do this next bit of code if local
            if (!isset($_SESSION['is_localhost']))
            {
                // Check if the session country ID is different to the customers
                // and redirect if so
                if ($_SESSION['iso2country'] != $aMemberDetails['signup_country_iso2code'])
                {

                    // Build array of mapping data for writing to session
                    /* $aMapData = array('iso_code'    => $aMemberDetails['member_iso2code'],
                                      'language_id' => $aMemberDetails['member_language'],
                                      'currency_id' => $aMemberDetails['member_currency']);*/

                    $aMapData = array('code'     => $aMemberDetails['signup_country_iso2code'],
                                      'language' => $aMemberDetails['signup_language_id'],
                                      'currency' => $aMemberDetails['signup_currency_id']);

                    // Write session data
                    $this->writeMappingSessionInfo($aMapData); 

                    // If country code is GB, just redirect to the main site
                    if (strtolower($aMemberDetails['signup_country_iso2code']) == 'gb')
                    {
                        //$sNewRedirectURL = 'http://nath.' . self::DEFAULT_HOMESITE . $sRedirectURL;
                        $sNewRedirectURL = 'http://' . self::DEFAULT_HOMESITE . $sRedirectURL;
                    }
                    else
                    {
                        //$sNewRedirectURL = 'http://' . strtolower($aMemberDetails['signup_country_iso2code']) . '.nath.' . self::DEFAULT_HOMESITE . $sRedirectURL;
                        $sNewRedirectURL = 'http://' . strtolower($aMemberDetails['signup_country_iso2code']) . self::DEFAULT_HOMESITE . $sRedirectURL;
                    }

                    // Construct new redirect URL (redirect handled by AJAX later on)
                    $sRedirectURL = $sNewRedirectURL;

                    // Empty cart, unset the customer's session cart and currency on this site                
                    $this->oCart->emptyCart();
                    unset($_SESSION['cart']);
                    unset($_SESSION['currency']);
                    
                }
            }
            
            # Handle AJAX responses
            if($isAjax) {

                if(empty($_POST['register'])) {

                    /**
                     * @todo, determine what process the user is in
                     * 1) new user, no order => thank you + welcome modal
                     * 2) new user, with order => transferring you modal => add funds
                     */
                    ob_start();

                    $this->smarty->assign('email', $aMemberDetails['email']);
                    $this->smarty->assign('name', $aMemberDetails['firstname']);
                    $this->smarty->assign('cart', $this->oCart);

                    if($this->oCart->getCartTotal() > 0) {
                        if($this->oMember->getMemberBalance() <= 0) {
                            $returnModal = $this->smarty->display('pods/login/transferring-payment');
                        } else {
                            $returnModal = $this->smarty->display('pods/login/confirmation-ticket');
                        }
                    } else {
                        echo '';
                    }

                    $responseModal = ob_get_clean();

                    header('Content-Type: application/json');
                    echo json_encode(array('error' => false, 'cart' => $this->oCart->getCartTotal(), 'balance' => $this->oMember->getMemberBalance(), 'redirect' => $sRedirectURL, 'contents' => $responseModal));
                    exit;

                }

            } else {

                # Redirect to a page
                if ($redirect) {
					$sRedirectURL = ltrim($sRedirectURL, '/');
                    header("Location: /{$sRedirectURL}");
                    exit;
                }
            }
        } else {

            # Wrong details
            # $this->oError->addError("Invalid username or password.");

            # Handle AJAX responses
            if ($isAjax) {
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'data' => $this->oError->getErrorList()));
                exit;
            }
        }

        # Return the boolean status
        return $bLoggedIn;
    }

    public function Logout() {
        $this->oMember->Logout();
        # $this->saveSessionObjects();
    }

    protected function assignMemberFullDetails() {
        $this->assignCountriesList();
        $this->smarty->assign("accountnavigation", $this->oNavigation->getNavigationMenu($this->oPage->getSectionID(), true, 1));
        $memberDetails = $this->oMember->getMemberDetails($this->iLangID);
        $this->smarty->assign("memberDetails", $memberDetails);
        $this->smarty->assign('memberdetails', $memberDetails);
        $this->smarty->assign('memberfulldetails', $memberDetails);
    }

    public function assignPaymentForm() {

        # Came from the checkout process
        if (!empty($_GET['checkout'])) {
            $_SESSION['checkout'] = true;
        }

        if (PaymentGateways::checkPaymentGateway(PaymentGateways::NETELLER))
            $this->smarty->assign("neteller", PaymentGateways::NETELLER);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::MONEYBOOKERS))
            $this->smarty->assign("moneybookers", PaymentGateways::MONEYBOOKERS);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::TRANSACTIUM) && !$this->oMember->isCountryBlocked())
            $this->smarty->assign("transactium", PaymentGateways::TRANSACTIUM);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::PAYFREX))
            $this->smarty->assign("payfrex", PaymentGateways::PAYFREX);
        $this->smarty->assign("gateways", $this->oCart->oPaymentGateway->getPaymentGateways($this->oMember->isCountryBlocked()));
        $this->smarty->assign("widthdrawalgateways", $this->oCart->oPaymentGateway->getWithdrawalGateways($this->oMember->isCountryBlocked()));
        $this->smarty->assign("minamount", $this->oCart->oPaymentGateway->getMinimumTopup());
        $this->smarty->assign("maxamount", $this->oCart->oPaymentGateway->getMaximumTopup());
        $this->smarty->assign("topupamount", $this->oMember->getRemainingBalance($this->oCart->getCartTotal(), $this->oCart->oPaymentGateway->getMinimumTopup()));
    }

    public function assignCountriesList() {
        $this->smarty->assign("countries", $this->oMember->getAllCountries($this->iLangID));
    }

    public function processTopup() {

        /**
         * Check to see if any new address details have been added here
         */
        if($_POST['address1'] || $_POST['address2'] || $_POST['city'] || $_POST['zip'] || $_POST['country']){

            $aDetails=array(
                'address1'=>$_POST['address1'],
                'address2'=>$_POST['address2'],
                'city'=>$_POST['city'],
                'zip'=>$_POST['zip'],
                'fk_country_id'=>$_POST['country']
            );

            // Update member details
            // This function will also ensure session information is correctly updated
            $this->oMember->saveMemberDetailsAddFunds($_POST);

            $this->oMember->address1=$_POST['address1'];
            $this->oMember->address2=$_POST['address2'];
            $this->oMember->city=$_POST['city'];
            $this->oMember->zip=$_POST['zip'];
            $this->oMember->fk_country_id=$_POST['country'];
            $this->oMember->county_state=$_POST['county_state'];
            $this->saveSessionObjects();
        }


        if ($_GET['action'] == 'addCredit') {
            if ($_POST['gateway'] && $_POST['amount']) {
                $aPaymentGateway = explode("-", $_POST['gateway']);
                $this->oCart->assignPaymentGateway($aPaymentGateway[0]);
                if ($this->oCart->oPaymentGateway->assignAmount($_POST['amount'])) {
                    $this->oCart->oPaymentGateway->bSkipConfirmationPage = true;
                    $this->smarty->assign("charges", $this->oCart->oPaymentGateway->getCharges());
                    $this->smarty->assign("amount", $this->oCart->oPaymentGateway->getAmount());
                    $this->smarty->assign("gatewayid", $this->oCart->oPaymentGateway->getGatewayID());
                    $this->smarty->assign("gateway", $this->oCart->oPaymentGateway->getGatewayName());
                    $this->smarty->assign("pm", $aPaymentGateway[1]);
                    $this->saveSessionObjects();
                } else {
                    $this->oError->addError("Invalid Amount entered.");
                    $this->saveSessionObjects();
                    // Raise a message in the audit log to say Payfrex has died.
                    $sAuditLogMsg = "Error in assigning amount to addFunds";
                    AuditLog::LogItem($sAuditLogMsg, 'ERROR_ASSIGN_ADD_FUNDS', 'transactions', 0);

                    header("location: /errorFunds.php");
                    exit;
                }
            } else {
                // Raise a message in the audit log to say Payfrex has died.
                $sAuditLogMsg = "Missing gateway or amount";
                AuditLog::LogItem($sAuditLogMsg, 'MISSING_GATEWAY_OR_AMOUNT', 'transactions', 0);

                $this->saveSessionObjects();
                header("location: /errorFunds.php");
                exit;
            }
        }

        if ($_GET['action'] == 'confirmTransaction' || ( $_GET['action'] == 'addCredit' && $this->oCart->oPaymentGateway->skipConfirmationPage() )) {
            
            $this->oCart->assignPaymentGateway((!empty($_POST['gatewayid']) ? $_POST['gatewayid'] : $_POST['gateway']));
            $this->oCart->oPaymentGateway->assignAmount($_POST['amount']);

            if ($this->oCart->oPaymentGateway->addTransaction($this->oMember->getMemberID(), $this->oCurrency->getSelectedCurrencyID())) {


                # Response URL
                $aURL = $this->oCart->oPaymentGateway->ProcessPayment($this, $this->oMember, $_POST);
                        
                if (is_array($aURL)) {
                    $this->smarty->assign("transactiumJSURL", $aURL['js']);
                    $this->smarty->assign("transactiumRedirectURL", $aURL['redirect']);
                }

                # Handle 'virtual terminal' topups
                if ($this->oCart->oPaymentGateway->getGatewayID() == PaymentGateways::LOVELOTTO) {

                    /**
                     * If session[checkout] is set, we want to proceed to the 'review order' screen again
                     * once we have some funds.
                     */
                    //$this->oCart->commitPurchase($this->oMember, $this->oCurrency);
                    //LLCache::add('paymentStatus-'.session_id(), 'Complete');
                    /**
                     * Need to make the page sleep for a couple of seconds here
                     * to give time for the callback from Payfrex to record the transaction
                     * and update the member's balance correctly
                     * During this time the user will see some information in a popup about what is happening
                     */
                    /*
                    if($_SESSION['checkout']){
                        LLCache::add('paymentStatus-'.session_id(), 'CompletePurchase');
                        sleep(5);
                        header('Location: /checkout.php?force=1');
                    }else{
                        LLCache::add('paymentStatus-'.session_id(), 'CompleteTopup');
                        sleep(5);
                        header('Location: /myaccount');
                    }
                     * /
                     */
                }

                # Payfrex frame
                $this->smarty->assign('paymentIframe', $aURL);

                # Withdrawals?
                if(!empty($aURL) && $aURL!='') {
                    header('Location: '.$aURL);
                    exit;
                }
                die();

            }
        }

        if (PaymentGateways::checkPaymentGateway(PaymentGateways::NETELLER))
            $this->smarty->assign("neteller", PaymentGateways::NETELLER);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::MONEYBOOKERS))
            $this->smarty->assign("moneybookers", PaymentGateways::MONEYBOOKERS);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::TRANSACTIUM) && !$this->oMember->isCountryBlocked())
            $this->smarty->assign("transactium", PaymentGateways::TRANSACTIUM);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::PAYFREX))
            $this->smarty->assign("moneybookers", PaymentGateways::PAYFREX);
    }

    public function processWithdrawal() {

        if ($_GET['action'] == 'processWithdrawal') {

            if ($_POST['gateway'] && $_POST['amount']) {
                $aPaymentGateway = explode("-", $_POST['gateway']);
                $this->oCart->assignPaymentGateway($aPaymentGateway[0]);
                if ($this->oCart->oPaymentGateway->assignAmount($_POST['amount'], true)) {
                    $this->oCart->oPaymentGateway->bSkipConfirmationPage = true;
                    $this->oCart->oPaymentGateway->setPaymentType('withdrawal');

                    $this->smarty->assign("charges", $this->oCart->oPaymentGateway->getCharges());
                    $this->smarty->assign("amount", $this->oCart->oPaymentGateway->getAmount());
                    $this->smarty->assign("gatewayid", $this->oCart->oPaymentGateway->getGatewayID());
                    $this->smarty->assign("gateway", $this->oCart->oPaymentGateway->getGatewayName());
                    $this->smarty->assign("pm", $aPaymentGateway[1]);
                    $this->saveSessionObjects();
                } else {
                    $this->oError->addError("Invalid Amount entered.");
                    $this->saveSessionObjects();
                    header("location: /withdraw.php");
                    exit;
                }
            } else {
                $this->saveSessionObjects();
                header("location: /withdraw.php");
                exit;
            }
        }

        if ($_GET['action'] == 'confirmTransaction' || ( $_GET['action'] == 'processWithdrawal' && $this->oCart->oPaymentGateway->skipConfirmationPage() )) {
            // Reverse number format and check the customer is not trying to withdraw more than their account balance!
            // (this is obvioualy quite a big bug!
            
            if ($this->oMember->getmemberBalance() >= reverseNumberFormat($_POST['amount'])) {

                $this->oCart->assignPaymentGateway($_POST['gateway']);
                $this->oCart->oPaymentGateway->assignAmount($_POST['amount'], true); // extra parameter needed here to indicate it's a withdrawal
                $this->oCart->oPaymentGateway->setPaymentType('withdrawal');


                if ($test = $this->oCart->oPaymentGateway->addTransaction($this->oMember->getMemberID(), $this->oCurrency->getSelectedCurrencyID())) {

                    # Response URL
                    $aURL = $this->oCart->oPaymentGateway->ProcessPayment($this, $this->oMember, $_POST);

                    if (is_array($aURL)) {
                        $this->smarty->assign("transactiumJSURL", $aURL['js']);
                        $this->smarty->assign("transactiumRedirectURL", $aURL['redirect']);
                    }

                    # Handle 'virtual terminal' topups
                    if ($this->oCart->oPaymentGateway->getGatewayID() == PaymentGateways::LOVELOTTO) {

                        /**
                         * If session[checkout] is set, we want to proceed to the 'review order' screen again
                         * once we have some funds.
                         */
                        //$this->oCart->commitPurchase($this->oMember, $this->oCurrency);
                        header('Location: /withdrawalreceipt.php');
                    }

                    # Payfrex frame
                    $this->smarty->assign('paymentIframe', $aURL);

                    # Withdrawals?
                    if(!empty($aURL)) {
                        header('Location: '.$aURL);
                        exit;
                    }

                }
            } else {

                // Can probably handle this better, but a die statement will do for the moment.
                // with translated text from CMS
                die(VCMS::get('Withdraw.AmountError'));
            }

        }

        if (PaymentGateways::checkPaymentGateway(PaymentGateways::NETELLER))
            $this->smarty->assign("neteller", PaymentGateways::NETELLER);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::MONEYBOOKERS))
            $this->smarty->assign("moneybookers", PaymentGateways::MONEYBOOKERS);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::TRANSACTIUM) && !$this->oMember->isCountryBlocked())
            $this->smarty->assign("transactium", PaymentGateways::TRANSACTIUM);
        if (PaymentGateways::checkPaymentGateway(PaymentGateways::PAYFREX))
            $this->smarty->assign("moneybookers", PaymentGateways::PAYFREX);
    }


    public function assignCreditReceipt() {

        #print_d($this->oCart);
            // Force the user data update
            $this->oMember->forceMemberDetailUpdate();

            // Re-save object
            $_SESSION['memberobject'] = serialize($this->oMember);

        $this->oMember->updateBalance();
        if ($this->oCart->oPaymentGateway->getGatewayID()) {
            $iTransactionStatus = $this->oCart->oPaymentGateway->updateTransaction($this->oMember->getMemberID());
            if ($iTransactionStatus != 3) {
                $this->oError->addNotice("Your transaction has been processed but is not confirmed yet from " . $this->oCart->oPaymentGateway->getGatewayName() . ". You will receive an email from us as soon as the transaction is confirmed and the money added to your account.");
            }

            // Code copied from API call
            // Re-assign member details
            $this->smarty->assign("memberdetails", $this->oMember->getMemberDetails());
            $this->smarty->assign("transactionamount", $this->oCart->oPaymentGateway->getAmount());
            $this->smarty->assign("transactionreference", $this->oCart->oPaymentGateway->getTransactionReference());
            $this->smarty->assign("gatewayname", $this->oCart->oPaymentGateway->getGatewayName());
            $this->assignMemberFullDetails();

            //$this->oCart->resetPaymentGateway();
        } else {
            $this->oError->addError("Unauthorised Access");
            header("location: /myaccount.php");
            exit;
        }
    }


    public function assignWithdrawalReceipt() {

        // This is essentially a copy of the code from the assignCreditRecweipt()
        // function above, but moved out into its own function so that
        // we can do different processes if need be

        // Force the user data update
        $this->oMember->forceMemberDetailUpdate();

        // Re-save object
        $_SESSION['memberobject'] = serialize($this->oMember);

        $this->oMember->updateBalance();
        if ($this->oCart->oPaymentGateway->getGatewayID()) {
            $iTransactionStatus = $this->oCart->oPaymentGateway->updateTransaction($this->oMember->getMemberID());
            if ($iTransactionStatus != 3) {
                $this->oError->addNotice("Your transaction has been processed but is not confirmed yet from " . $this->oCart->oPaymentGateway->getGatewayName() . ". You will receive an email from us as soon as the transaction is confirmed and the money added to your account.");
            }

            // Code copied from API call
            // Re-assign member details
            $this->smarty->assign("memberdetails", $this->oMember->getMemberDetails());
            $this->smarty->assign("transactionamount", $this->oCart->oPaymentGateway->getAmount());
            $this->smarty->assign("transactionreference", $this->oCart->oPaymentGateway->getTransactionReference());
            $this->smarty->assign("gatewayname", $this->oCart->oPaymentGateway->getGatewayName());
            $this->assignMemberFullDetails();

            //$this->oCart->resetPaymentGateway();
        } else {
            $this->oError->addError("Unauthorised Access");
            header("location: /myaccount.php");
            exit;
        }
    }


    public function assignCartDetails() {
        $this->getActiveLotteries();
        $this->smarty->assign("cartdetails", $this->oCart->getCartDetails());
    }

    public function setLimit($iLimit) {
        $this->iLimit = $iLimit;
    }

    public function outputResetPassword() {

        $token = $_GET['token'];

        # Got a valid token?
        if(empty($token)) {
            continue;
        }

        # Attempt to find the key
        $sSQL = "SELECT * FROM members_reset_token WHERE token_id = '{$token}'";
        $resetToken = DAL::executeGetRow($sSQL);

        # Get the post data?
        if(isset($_POST['password'])) {

            $valid = true;

            # Check they're the same
            if($_POST['password'] == $_POST['password_confirm']) {
                $valid = false;
                $this->oError->addNotice(VCMS::get('ResetPassword.InvalidPassword'));
            } elseif(strlen($_POST['password']) < 6) {
                $valid = false;
                $this->oError->addNotice(VCMS::get('ResetPassword.PasswordTooShort'));
            }

            # Validation passed?
            if($valid === true) {

                # Now update the members table
                if(DAL::Update("members", array("password" => Encryption::Encrypt($_POST['password'])), "member_id = ".$resetToken['member_id'])) {

                    # Get the user
                    $sSQL = "SELECT firstname, balance, member_id, email, password FROM members WHERE member_id = '{$resetToken['member_id']}'";
                    $aMemberDetails = DAL::executeGetOne($sSQL);

                    // Return the balance value to 2 decimal places, rounded down
                    // This will then be further formatted to 2display as 2 decimal places
                    $fBalance = floor($aMemberDetails['balance'] * 100) / 100;

                    $aPersonalisationData = array(
                        'username'        => $aMemberDetails['email'],
                        'firstname'       => $aMemberDetails['firstname'],
                        'current balance' => number_format_locale($fBalance, 2, '.', ','),
                        'balance'         => number_format_locale($fBalance, 2, '.', ','),
                        'member_id'       => $aMemberDetails['member_id'],
                        'email_ref'       => 'password_reset',
                        'token'           => $token
                    );

                    Email::passwordResetEmail($aMemberDetails['email'], $aPersonalisationData);

                    # Set as used
                    DAL::Update('members_reset_token', array('token_used' => 1), 'token_id = "'.$token.'"');

                    # Login the user
                    $this->performLogin($aMemberDetails['email'], $aMemberDetails['password'], true, false);

                    # Redirec to account
                    header('Location: /myaccount');
                    exit;

                }

            }

        }

        # Did we find a token?
        if(!empty($resetToken)) {
            $this->sTemplate = 'pods/reset-password';
        } else {
            $this->oError->addNotice(VCMS::get('ResetPassword.InvalidToken'));
        }

    }

    public function resetPassword() {
        if($_POST['a'] == "forgotpassword" && $_POST['email']) {
            if($this->oMember->resetPassword($_POST['email'])) {
                $this->oError->addNotice("The new password has been sent by email to " . $_POST['email']);
            } else {
                $this->oError->addNotice('<i class="fa fa-warning"></i> '.VCMS::get('ResetPassword.InvalidEmailAddress'));
            }
        }
    }

    public function checkActions() {
        if ($_POST['a']) {
            switch ($_POST['a']) {
                case 'changepassword':
                    if ($this->oMember->changePassword($_POST['oldpassword'], $_POST['password'], $_POST['password2'])) {
                        $this->oError->addNotice("Password Changed");
                    }
                    break;
                case 'emailpreferences':
                    $bGeneralOffers = ($_POST['emailgo']) ? 1 : 0;
                    $bLotteryDraws = ($_POST['emailld']) ? 1 : 0;
                    if ($this->oMember->changeEmailPreferences($bGeneralOffers, $bLotteryDraws)) {
                        $this->oError->addNotice("Email preferences saved");
                    }
                    break;
                case 'changesettings':
                    if ($this->oMember->saveMemberDetails($_POST, true)) {
                        $this->oError->addNotice("Details saved Successfuly");
                    }
                    break;
                case 'tellafriend':
                    $aEmails = array_unique($_POST['email']);
                    if (count($aEmails) > 1) {
                        $sLinkTo = "http://www.lovelotto.com?aid=6&promo=" . $this->oMember->getMemberID();
                        $sMessage = "Hi,\r\n\r\n" . stripslashes($_POST['message']) . "\r\n\r\n{$sLinkTo}\r\n\r\nLoveLotto.com enables you to play the worldwide Lotteries from anywhere in the world, from the comfort of your home!";
                        foreach ($aEmails as $key => $sEmail) {
                            if ($sEmail) {
                                try {
                                    Email::sendTextEmail("", "", $sEmail, $sMessage, WEBSITETITLE, $this->oMember->getUsername(), $this->oMember->getFullName());
                                    $this->oError->addNotice("Successfully sent email to " . $sEmail);
                                } catch (Exception $e) {
                                    $this->oError->addError("Could not send email to " . $sEmail);
                                }
                            }
                        }
                    } else {
                        $this->oError->addError("At least one email address must be filled in");
                    }

                    break;
            }
        }
    }

    /**
     * Retrieve the XML from this transaction as an array
     * @param integer $iTransactionID transaction ID
     *
     * @return array if found, else null
     */
    public function getTransactionXML($sTransactionIDStr)
    {

        // Remove the 2 extra characters at the beginning and end
        $temp1 = substr($sTransactionIDStr, 2);
        $temp2 = substr($temp1, 0, -2);


        // Find this transaction's API response
        $sSQL = "SELECT api_response FROM transactions WHERE transaction_id = " . (int) base64_decode($temp2);
        $sXML = DAL::executeGetOne($sSQL);

        // Only do the next bit if we have XML
        if ($sXML != "")
        {
            // Get XML into array
            $oXML = simplexml_load_string($sXML);
            $sJSON = json_encode($oXML);
            $aParsedXML = json_decode($sJSON,TRUE);

            // Need to get the optional params here
            $aOptionalParams = $aParsedXML['optionalTransactionParams']['entry'];

            if (is_array($aOptionalParams))
            {
                foreach ($aOptionalParams as $aParams)
                {
                    $sKey = $aParams['key'];
                    $aParsedXML['fromPayfrex'][$sKey] = $aParams['value'];
                }
            }

            return $aParsedXML;
        }

        return null;
    }

    /**
     * Assign the Payfrex error message to the page
     *
     * @param string $sErrorMessage the rror message
     * @param integer $iTransactionID the transaction ID
     */
    public function assignPayfrexErrorMessage($sErrorMessage, $iTransactionID)
    {

        // Check here if a particular message has been received
        // If so, change the text accordingly.
        if ($this->str_startswith($sErrorMessage, "The transaction has been blocked by"))
        {
            $sErrorMessage = VCMS::get("Payment.AmountTooLowError");
        }

        // Assign message string
        $this->smarty->assign("payfrexerror", $sErrorMessage);
        $this->smarty->assign("transactionid", $iTransactionID);
    }


    /**
     * Check if a particular error message fro Payfrex begins with certain text
     *
     * @param string $source source string
     * @param string $prefix search terms
     * @return boolean
     */
    private function str_startswith($source, $prefix)
    {
        return strncmp($source, $prefix, strlen($prefix)) == 0;
    }
}
