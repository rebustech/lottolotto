<?php
/**
 * AjaxControl smarty object
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */
class AjaxControl extends SmartyObject {

    public function __construct($sTemplate, $iCache) {
        parent::__construct($sTemplate, $iCache, $this->createCacheName());
    }

    public function displayControl() {
        parent::displayControl();
    }

    public function isCached() {
        return parent::isCached();
    }

    public function assign($varname, $data) {
        $this->smarty->assign($varname, $data);
    }

    private function createCacheName() {
        $sCacheName = "";
        foreach ($_GET as $key => $value) {
            $sCacheName .= "|" . $key . $value;
        }

        foreach ($_POST as $key => $value) {
            $sCacheName .= "|" . $key . $value;
        }

        return $sCacheName;
    }

}