<?php
/**
 * Homepage smarty object
 * Used to handle the building of the main home page including the assigning of the
 * lotteries and jackpots to display
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */

class HomePage extends LottoPage
{
	public function __construct($iPageID, $sTemplate, $bSSL = false) {
		parent::__construct($iPageID, $sTemplate, $bSSL);
	}

	public function displayPage(){
		parent::displayPage();
	}

	public function assignHighestJackpot()
	{
		if ( is_array($this->aLottos) )
		{
			$sJackpot = 0;
			$sJackpotCurrency = "";
			foreach ($this->aLottos as $aLotto )
			{
				if ( $aLotto['jackpot'] > $sJackpot )
				{
					$sJackpotCurrency = $aLotto['jackpotcurrency']['symbol'];
					$sJackpot = $aLotto['jackpot'];
				}
			}
			$sJackpot = number_format_locale( round(($sJackpot / 1000000),1), 1, '.',',');
			$this->smarty->assign('bigjackpot', $sJackpotCurrency . ((substr($sJackpot,-1) == 0 )?substr($sJackpot,0,-2):$sJackpot) );
		}
	}
}
?>