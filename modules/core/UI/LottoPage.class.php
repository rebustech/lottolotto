<?php
/**
 * LottoPage smarty object
 * Used to display each of the lottery pages along with play page details etc...
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage SmartyObjects
 */

class LottoPage extends Page
{
	protected $iLotteryID = NULL;
	protected $oLottery = NULL;
	protected $aLottos = array();

	public function __construct($iPageID, $sTemplate, $bSSL = true) {
		parent::__construct($iPageID, $sTemplate, $bSSL);

	}

	public function displayPage(){
		parent::displayPage();
	}

	public function assignLottery($iLotteryID) {
		$this->iLotteryID = $iLotteryID;
		$this->assignLotteryByUrl();
	}

	public function assignLotteryByUrl() {
		$this->assignLotteryObject();
		$this->addToCart();
		$this->assignLotteryVariables();
	}

	protected function assignLotteryObject() {

		# Get the lottery from the ID
		if(!empty($this->iLotteryID)) {

			$sClassName = Lottery::getLotteryClassName($this->iLotteryID);

		}

		# Valid class?
		if(isset($sClassName)) {

			$this->oLottery = new $sClassName($this->iLangID);

			# Pass into Smarty
			$this->smarty->assign('oLottery', $this->oLottery);

		}

		/**
		 * Pass the data to the game engine components
		 * @todo, Implementation for getByUrl
		 */
		if(method_exists('GameEngine', 'getByUrl')) {
			try {
				$oGameEngine = GameEngine::getByUrl($_SERVER['REQUEST_URI']);
			}
			catch(LottoException $e) {
				header('Location: /404.html');
				exit;
			}

		} elseif(!empty($this->oLottery->fk_game_engine_id)) {

			$oGameEngine = GameEngine::getById($this->oLottery->fk_game_engine_id);

		}

		# Assign into Smarty
		if(!empty($oGameEngine)) {
			$this->smarty->assign('oGameEngine', $oGameEngine);
		} else {
			header('Location: /404.html');
			exit;
		}

	}

	protected function assignLotteryVariables() {

		# Older method
		if(empty($this->oLottery)) {
			return;
		}

		# Get all draw dates
		$aDraws = $this->oLottery->getAvailableDates();

		$aLottoDetails["lottoid"] = $this->oLottery->getID();
		$aLottoDetails["lottomaxnumber"] = $this->oLottery->getMaxNumber();
		$aLottoDetails["lottominnumber"] = $this->oLottery->getMinNumber();
		$aLottoDetails["lottomaxtickets"] = $this->oLottery->getMaxTickets();
		$aLottoDetails["lottonumberscount"] = $this->oLottery->getNumbersCount();
		$aLottoDetails["lottobonusnumbers"] = $this->oLottery->getBonusNumbers();
		$aLottoDetails["lottomaxrecurring"] = $this->oLottery->getMaxRecurring();
		$aLottoDetails["bonusminnumber"] = $this->oLottery->getMinBonusNumber();
		$aLottoDetails["bonusmaxnumber"] = $this->oLottery->getMaxBonusNumber();
		$aLottoDetails["bonustext"] = $this->oLottery->getBonusText();
		$aLottoDetails["lottocolor"] = $this->oLottery->getLottoColor();
		$aLottoDetails["lottodates"] = $aDraws;

		$aLottoDetails["title"] = $this->oLottery->getTitle();
		$aLottoDetails["comment"] = $this->oLottery->getComment();
		$aLottoDetails["description"] = $this->oLottery->getDescription();
		$aLottoDetails["jackpotcurrency"] = $this->oLottery->getCurrencyDetails();

		$aLottoDetails["jackpot"] = $aDraws[0]['jackpot'];
		$aLottoDetails["drawdate"] = $aDraws[0]['drawdate'];
		$aLottoDetails["rollover"] = $aDraws[0]['rollover'];
		$aLottoDetails["cutofftime"] = $aDraws[0]['cutofftime'];
		$aLottoDetails["price"] = $aDraws[0]['price'];

		$aLottoDetails["playpage"] = $this->oLottery->getPlayPage();
		$aLottoDetails["logo"] = $this->oLottery->getLogo();
		$aLottoDetails["logotext"] = $this->oLottery->getLogoText();
		$aLottoDetails["template"] = $this->oLottery->getTemplate();

		array_push($this->aLottos, $aLottoDetails);

		# Push everything to Smarty
		$this->smarty->assign('lottos', $this->aLottos);
		$this->smarty->assign('aLottoDetails', $aLottoDetails);

		# Assigned FAQs
		$this->assignFAQQuestions($this->oLottery->getFAQCategoryID());
		unset($aLottoDetails);

	}

	protected function addToCart()
	{
		if ( $_POST['action'] == 'addToCart' )
		{
			if ( $_POST['lotteryid'] == $this->iLotteryID )
			{
				$iIterations = $_POST['weekPlays'];
				$dStartDrawDate = $_POST['drawDate'];
				for ( $i = 1; $i <= $this->oLottery->getMaxTickets(); $i++ )
				{
					$aNumberSet = $_POST['ticket_' . $i];
					if ( in_array("AP", $aNumberSet) )
					{
						$aNumberSet = $this->oLottery->generateLuckyDip();
					}
					if ( $this->oLottery->validateNumbers($aNumberSet) )
					{
						$aDrawDates = $this->oLottery->getDrawDates($dStartDrawDate, $iIterations);

						foreach ($aDrawDates as $dDrawDate )
						{
							$oLotteryItem = new CartItem();
							$oLotteryItem->setDrawDate($dDrawDate['drawdate']);
							$oLotteryItem->setCost($dDrawDate['price']);
							$oLotteryItem->setCutoffTime($dDrawDate['cutoff']);
							$oLotteryItem->setLotteryID($this->iLotteryID);
							$oLotteryItem->setLotteryTitle($this->oLottery->getTitle());
							$oLotteryItem->setNumbers($aNumberSet);
							$this->oCart->addToCart($oLotteryItem);
							unset($oLotteryItem);
						}
					}
					else
					{
						$aNumberSet = array_unique($aNumberSet);
						if ( count($aNumberSet) > 1 || !empty($aNumberSet[0]) )
						{
							$this->oError->addError("Invalid Lottery Ticket Entered.");
						}
					}
					unset($aNumberSet);
				}
				$this->saveSessionObjects();
				header("location: /" . $this->sLangCode . "/shopping-cart.php");
				exit;
			}
		}
	}

	public function assignLotteryDetails($iLotteryID)
	{
		$sClassName = Lottery::getLotteryClassName($iLotteryID);
		if ( $sClassName )
		{
			$oLottery = new $sClassName($this->iLangID);
		}
		$aLottoDetails["title"] = $oLottery->getTitle();
		$aLottoDetails["comment"] = $oLottery->getComment();
		$aLottoDetails["description"] = $oLottery->getDescription();
		$aLottoDetails["jackpotcurrency"] = $oLottery->getCurrencyDetails();

		$aNextDraw = $oLottery->getAvailableDates();
		$aLottoDetails["jackpot"]           = $aNextDraw[0]['jackpot'];
		$aLottoDetails["drawdate"]          = $aNextDraw[0]['drawdate'];
		$aLottoDetails["cutofftime"]        = $aNextDraw[0]['cutofftime'];
		$aLottoDetails["rollover"]          = $aNextDraw[0]['rollover'];
		$aLottoDetails["price"]             = $aNextDraw[0]['price'];
		$aLottoDetails["timezone"]          = $aNextDraw[0]['timezone'];
		$aLottoDetails["playpage"]          = $oLottery->getPlayPage();
		$aLottoDetails["resultspage"]       = $oLottery->getResultsPage();
		$aLottoDetails["logo"]              = $oLottery->getLogo();
		$aLottoDetails["logotext"]          = $oLottery->getLogoText();
		$aLottoDetails["template"]          = $oLottery->getTemplate();
		$aLottoDetails["lottoid"]           = $iLotteryID;
		$aLottoDetails["lottonumberscount"] = $oLottery->getNumbersCount();
		$aLottoDetails["lottobonusnumbers"] = $oLottery->getBonusNumbers();
		$aLottoDetails["bonustext"]         = $oLottery->getBonusText();
		$aLottoDetails["lottocolor"]        = $oLottery->getLottoColor();

		array_push($this->aLottos, $aLottoDetails);

	}

	public function sortLotteries()
	{
		foreach ($this->aLottos as $key => $row) {
			$jackpot[$key]  = $this->oCurrency->convertToBase($row['jackpot'],$row['jackpotcurrency']['currency_id']);
			$cutofftime[$key] = $row['cutofftime'];
		}
		array_multisort($jackpot, SORT_DESC, $cutofftime, SORT_ASC, $this->aLottos);
		$this->smarty->assign("lottos", $this->aLottos);
		unset($aLottoDetails);
	}

	public function assignLotteryResults($iLotteryID, $iLimit = 1) {
		$oLotteryResults = new LotteryResults($iLotteryID, $this->iLangID);
		//$aResults = $oLotteryResults->getResults($iLimit);

        if(empty($_GET['year'])) {
            $iYear = date("Y");
        } else {
            $iYear = $_GET['year'];
        }

        $aResults = $oLotteryResults->getResultsByYear($iYear);
        $iResultStartYear = $oLotteryResults->getResultsStartYear();

		# Pass into view
		# $this->smarty->assign('resultsYearStart', $iResultStartYear);

		$bFound = false;
		foreach ( $this->aLottos as $key=>$aLotto )
		{
			if ( $aLotto['lottoid'] == $iLotteryID )
			{
				$this->aLottos[$key]["results"] = $aResults;
				$bFound = true;
			}
		}
		if ( !$bFound ) {
			$sClassName = Lottery::getLotteryClassName($iLotteryID);

			if ( $sClassName )
			{
				$oLotto = new $sClassName($this->iLangID);
				$aLotto["lottoid"] 			= $iLotteryID;
				$aLotto["title"] 			= $oLotto->getTitle();
				$aLotto["comment"] 			= $oLotto->getComment();
				$aLotto["description"] 		= $oLotto->getDescription();
				$aLotto["jackpotcurrency"] 	= $oLotto->getCurrencyDetails();
				$aLotto["lottonumberscount"] = $oLotto->getNumbersCount();
				$aLotto["lottobonusnumbers"] = $oLotto->getDrawnBonusNumbersCount();
				$aLotto["bonustext"]         = $oLotto->getBonusText();
				$aLotto["bonuspluraltext"]   = $oLotto->getBonusTextPlural();
				$aLotto["lottocolor"]        = $oLotto->getLottoColor();
				$aLotto["resultspage"]       = $oLotto->getResultsPage();
				$aLotto["playpage"] 		= $oLotto->getPlayPage();
				$aLotto["logo"]              = $oLotto->getLogo();
				$aLotto["results"] = $aResults;
                $aLotto["resultsstartyear"] = $iResultStartYear;
				$aLotto['pastResults'] = $oLotteryResults->getPastResultBreakdown();
				$aLotto['game'] = array(
					'numbers' => array(
						'low' => $oLotto->getMinNumber(),
						'high' => $oLotto->getMaxNumber(),
						'drawn' => $oLotto->getNumbersCount()
					),
					'bonus' => array(
						'low' => $oLotto->getMinBonusNumber(),
						'high' => $oLotto->getMaxBonusNumber(),
						'drawn' => $oLotto->getDrawnBonusNumbersCount()
					)
				);

				$aDraws =  $oLotto->getAvailableDates();
				$aLotto["nextjackpot"] = $aDraws[0]['jackpot'];
				$aLotto["nextdrawdate"] = $aDraws[0]['drawdate'];
				$aLotto["nextrollover"] = $aDraws[0]['rollover'];
				$aLotto["nextcutofftime"] = $aDraws[0]['cutofftime'];
				$aLotto["price"] = $aDraws[0]['price'];

				# Get the breakdown
                if(empty($_GET['draw']) && !empty($_GET['year'])) {
 					$aResultsBreakdown = $oLotteryResults->getResultBreakdown($aResults[0]['id']);
					$aLotto['resultbreakdown'] = $aResultsBreakdown;
                } elseif(empty($_GET['draw'])  && empty($_GET['year'])) {
					$aResultsBreakdown = $oLotteryResults->getResultBreakdown($aDraws[0]['id']);
					$aLotto['resultbreakdown'] = $aResultsBreakdown;
				}

				# Pass into Smarty
				$this->smarty->assign('mlLottery', $oLotto);

				# Get by ID
				$oGameEngine = GameEngine::getById($oLotto->fk_game_engine_id);

				# Assign into Smarty
				if(!empty($oGameEngine)) {
					$this->smarty->assign('oGameEngine', $oGameEngine);
				}

				# Delete it
				unset($oLotto);

			}
			array_push($this->aLottos, $aLotto);
		}

		$this->smarty->assign("lottos", $this->aLottos);
		unset($oLotteryResults);
		unset($aResults);
	}


        /**
         * Assigns the supplementary German results for this 6aus49 lottery
         * @param integer $iDraw draw ID
         */
        public function assignGnSupplementaryResults($iDraw)
        {

            // Lottery objects
            $oSpiel77Results = new LotteryResults(Lottery::SPIEL77, $this->iLangID);
            $oSuper6Results = new LotteryResults(Lottery::SUPER6, $this->iLangID);


            // Most recent draw
            if ($iDraw == "")
            {

                $aSpiel77Results = $oSpiel77Results->getResultBreakdown();
                $aSuper6Results = $oSuper6Results->getResultBreakdown();


                $aSpiel77Results['numbers'] = implode($aSpiel77Results['numbers']);
                $aSuper6Results['numbers'] = implode($aSuper6Results['numbers']);


                $aSuppLottos = array('spiel77' => $aSpiel77Results,
                                     'super6'  => $aSuper6Results);
            }
            else
            {
                // Historical data

                // Get the draw date of this 6aus49 draw
                $oOrigResults = new LotteryResults(Lottery::GERMANLOTTO, $this->iLangID);
                $sDrawDate = $oOrigResults->getDrawDateFromDrawID($iDraw);

                // If we have a date, get the results for the two lotteries.
                if ($sDrawDate != "") {
                    $aSpiel77Results = $oOrigResults->getSupplementaryDataForDrawDate(Lottery::SPIEL77, $sDrawDate);
                    $aSuper6Results = $oOrigResults->getSupplementaryDataForDrawDate(Lottery::SUPER6, $sDrawDate);


                    if (is_array($aSpiel77Results['numbers']))
                    {
                        $aSpiel77Results['numbers'] = implode($aSpiel77Results['numbers']);
                        $aSuppLottos['spiel77'] = $aSpiel77Results;
                    }

                    if (is_array($aSuper6Results['numbers']))
                    {
                        $aSuper6Results['numbers'] = implode($aSuper6Results['numbers']);
                        $aSuppLottos['super6'] = $aSuper6Results;
                    }

                }
            }
            $this->smarty->assign("supp_lottos", $aSuppLottos);
        }

        /**
         * Assigns Glucksspirale results for this 6aus49 lottery
         * @param integer $iDraw draw ID
         */
        public function assignGlucksspiraleResults($iDraw)
        {

            // Lottery objects
            $oGlucksSpiraleResults = new GlucksspiraleLotteryResults(Lottery::GLUCKSSPIRALE, $this->iLangID);

            // Most recent draw
            if ($iDraw == "")
            {
                $aGlucksSpiraleResults = $oGlucksSpiraleResults->getResultBreakdown();
            }
            else
            {
                // Historical data

                // Get the draw date of this 6aus49 draw
                $oOrigResults = new LotteryResults(Lottery::GERMANLOTTO, $this->iLangID);
                $sDrawDate = $oOrigResults->getDrawDateFromDrawID($iDraw);

                // If we have a date, get the results for the two lotteries.
                if ($sDrawDate != "") {
                    $aGlucksSpiraleResults = $oGlucksSpiraleResults->getSupplementaryDataForDrawDate(Lottery::GLUCKSSPIRALE, $sDrawDate);

                }
            }

            $this->smarty->assign("supp_glucks", $aGlucksSpiraleResults);
        }

	public function assignActiveLotteryResultBreakdown()
	{
		$aLotteries = Lottery::getEnabledLotteries();
		foreach ($aLotteries as $aLottery)
		{
			$this->assignLotteryResultBreakdown($aLottery['lottery_id'], NULL, false);
		}
	}
	public function assignLotteryResultBreakdown($iLotteryID, $iDraw = NULL, $bSetSEO = true)
	{

        if (!$this->isCached())
		{

			$iDraw = (int)$iDraw;
			$oLotteryResults = new LotteryResults($iLotteryID, $this->iLangID);
			$aResults = $oLotteryResults->getResultBreakdown($iDraw);

			if(empty($aResults['winnings'])){
				$aResults = $oLotteryResults->getResultBreakdown();
			}

			$bFound = false;
			foreach ( $this->aLottos as $key=>$aLotto )
			{
				if ( $aLotto['lottoid'] == $iLotteryID )
				{
					if(empty($this->aLottos[$key]['resultbreakdown'])) {
						$this->aLottos[$key]["resultbreakdown"] = $aResults;
					}
					$bFound = true;
				}
			}
			if ( !$bFound )
			{
				$sClassName = Lottery::getLotteryClassName($iLotteryID);
				if ( $sClassName )
				{
					$oLotto = new $sClassName($this->iLangID);
					$aLotto["lottoid"] 			= $iLotteryID;
					$aLotto["title"] 			= $oLotto->getTitle();
					$aLotto["comment"] 			= $oLotto->getComment();
					$aLotto["description"] 		= $oLotto->getDescription();
					$aLotto["jackpotcurrency"] 	= $oLotto->getCurrencyDetails();
					$aLotto["lottonumberscount"] = $oLotto->getNumbersCount();
					$aLotto["lottobonusnumbers"] = $oLotto->getDrawnBonusNumbersCount();
					$aLotto["bonustext"]         = $oLotto->getBonusText();
					$aLotto["bonuspluraltext"]   = $oLotto->getBonusTextPlural();
					$aLotto["lottocolor"]        = $oLotto->getLottoColor();
					$aLotto["resultspage"]       = $oLotto->getResultsPage();
					$aLotto["playpage"] 		= $oLotto->getPlayPage();
					$aLotto["logo"]              = $oLotto->getLogo();
					$aLotto["resultbreakdown"]	= $aResults;

					$aDraws = $oLotto->getAvailableDates();
					$aLotto["nextjackpot"] = $aDraws[0]['jackpot'];
					$aLotto["nextdrawdate"] = $aDraws[0]['drawdate'];
					$aLotto["nextrollover"] = $aDraws[0]['rollover'];
					$aLotto["nextcutofftime"] = $aDraws[0]['cutofftime'];
					$aLotto["price"] = $aDraws[0]['price'];
					unset($oLotto);
				}
				array_push($this->aLottos, $aLotto);
			}
			elseif($bSetSEO) {
				$sDate =  " for " . date("d F Y", strtotime($aResults["lotterydate"]));
				$this->oPage->setPageTitle( $this->oPage->getPageTitle() . $sDate );
				$this->oPage->setBrowserTitle( $this->oPage->getBrowserTitle() . $sDate );
				$sKeywords = str_replace(",", $sDate . ",",  $this->oPage->getSEOKeywords());
				$this->oPage->setSEOKeywords( $sKeywords . $sDate);
				$this->oPage->setSEODescription( "Draw" . $sDate . ". " . $this->oPage->getSEODescription() );
			}

			$this->smarty->assign("lottos", $this->aLottos);

			unset($oLotteryResults);
			unset($aResults);
		}
	}

        /**
         * Master function to check suppliued lottery numbers
         * NB this will only work for the non-special or non-supplementary lotteries
         *
         * @param integer $iLotteryID lottery id
         * @param integer $iDrawID draw id
         * @param array $aCustomerNumbers array of numbers supplied by the customer for checking
         *
         * @return array
         */
        public function checkLottoNumbers($iLotteryID, $iDrawID, $aCustomerNumbers)
        {

            // Lottery object
            $oLottery = new Lottery($iLotteryID, $this->iLangID);

            // Lottery results for the requested draw
            $oLotteryResults = new LotteryResults($iLotteryID, $this->iLangID);
            $aResults = $oLotteryResults->getResultBreakdown($iDrawID);

            // Get the drawn numbers into seperate main/bonus arrays
            $aDrawnNumbers = array_slice($aResults['numbers'], 0, $oLottery->getNumbersCount());
            $aDrawnBonusNumbers = array_slice($aResults['numbers'], $oLottery->getNumbersCount(), $oLottery->getDrawnBonusNumbersCount() );


            // We can now use array_intersect to get the matches
            // NB the $aCustomerNumbers array contains 2 elements
            // $aCustomerNumbers['m'] is the array of 'main' numbers
            // ans $aCustomerNumbers['b'] is the array of 'bonus' numbers

            $aMainMatches = array_intersect($aDrawnNumbers, $aCustomerNumbers['m']);
            $aBonusMatches = array_intersect($aDrawnBonusNumbers, $aCustomerNumbers['b']);

            // return matches
            return array('main' => $aMainMatches, 'bonus' => $aBonusMatches);
        }

        public function checkSupplementaryLottoNumbers($iLotteryID, $iDrawID, $aCustomerNumber)
        {
            // Because the numbers for Spiel77/Super6 are *currently* entered in on seperate boxes
            // they need to be imploded into a string
            // (this may well change in the future though,so that the string is passed in
            // so if so, this line will need to be amended

            $iCustomerNumber = implode($aCustomerNumber);


            // Lottery results for the requested draw
            // Need to get the draw date from the corresponding German Lotto
            $oOrigResults = new LotteryResults(Lottery::GERMANLOTTO, $this->iLangID);
            $sDrawDate = $oOrigResults->getDrawDateFromDrawID($iDrawID);
            $iDrawnNumber = $oOrigResults->getSupplementaryNumbersForDrawDate($iLotteryID, $sDrawDate);

            // Compare the drawn number with the customer number
            return ($this->_compareNumbers($iLotteryID, $iDrawnNumber, $iCustomerNumber));

        }


     /**
     * Decremental comparison of 2 strings
     * (copy of function from GermanSupplementary game engine)
     *
     * @param integer $iDrawnNumber number drawn in this lottery
     * @param integer $iTicketNumber number on the ticket
     * @return integer
     */
    private function _compareNumbers($iLotteryID, $iDrawnNumber, $iTicketNumber)
    {
        $oLottery = new Lottery($iLotteryID, $this->iLangID);

        // Convert numbers to strings
        $sDrawnNumber = (string) $iDrawnNumber;
        $sTicketNumber = (string) $iTicketNumber;

        // Set match variable here to be false
        // as do/while loop will exit as soon as it's set to true
        $bMatch = false;

        // Record last match position
        $iMatchPos = 0;

        // Position counter, starting at max length
        $i = $oLottery->getNumbersCount();

        // On;y perform the next bit of code if match is true
        // and we're not at the start of the string
        do
        {
            // Start position for comparison is the negative value of i
            $iStartPos = -1 * abs($i);

            // Get substrings up to current position
            $sDrawSub   = substr($sDrawnNumber, $iStartPos);
            $sTicketSub = substr($sTicketNumber, $iStartPos);

            // Compare the two strings
            if (strcmp($sDrawSub, $sTicketSub) === 0)
            {
                // Stringsmatch, so set flag accordingly
                $bMatch = true;

                // Record current position as the match position
                $iMatchPos = -1 * abs($i);

            }

            // Decrease substring counter
            $i--;

        } while ($bMatch == false && $i >= 1);

        // Return match status
        if ($bMatch == false)
        {
            $iMatch = "";
        }
        else
        {
            $iMatch = substr($sDrawnNumber, $iMatchPos);
        }



        // Return original number and matched bit
        return array('original' => $sDrawnNumber,
                     'match'    => $iMatch);
    }

    /**
     * Decremental comparison of number strings for Gluecksspirale
     *
     * @param array $aDrawnNumber array of all drawn numbers
     * @param integer $iTicketNumber number on the ticket
     * @return integer
     */
    private function _compareGSNumbers($aDrawnNumbers, $iTicketNumber)
    {

        // Convert ticket number to strings and reverse them
        $sTicketNumber = (string) $iTicketNumber;

        // Reverse array of drawn numbers, so that we start fron the largest number
        $aDrawnNumbers = array_reverse($aDrawnNumbers);

        // Set match variable here to be false
        // as foreach loop will exit as soon as it's set to true
        $bMatch = false;

        // Record last match position
        $iMatchPos = 0;

        // Loop through the array of drawn numbers
        foreach ($aDrawnNumbers as $iDrawnNumber)
        {
            // Convert to string
            $sDrawnNumber = (string) $iDrawnNumber;

            // Get the length of the current draw number
            $iLength = strlen($sDrawnNumber);

            // Start position for comparison is the negative value of the number length
            $iStartPos = -1 * abs($iLength);

            // Substring of tucket number
            $sTicketSub = substr($sTicketNumber, $iStartPos);

            // Compare the two strings
            if (strcmp($sDrawnNumber, $sTicketSub) === 0)
            {
                // Stringsmatch, so set flag accordingly
                $bMatch = true;

                // Record current position as the match position
                $iMatchPos = $iLength;
                exit(1);
            }

        }

        // Return position at where match was found
        return $iMatchPos;
    }
}
?>
