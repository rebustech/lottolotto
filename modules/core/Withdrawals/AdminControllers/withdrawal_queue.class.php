<?php

class withdrawal_queue extends AdminController{
    /**
     * Decide what to show in a generic details view
     */
    function index(){
        $oView=new LLView();

        $sLanguageFilter=($_POST['languagefilter'])?'l.language_id='.$_POST['languagefilter']:'l.is_active=1';

        $sSQL='SELECT b.id AS block_id,b.key,b.group,b.tag,b.first_seen,v.id AS variant_id,v.name AS variant_name,v.version AS variant_version,l.language_id,l.title as language,l.icon,c.id AS content_id,c.version AS content_version FROM variants_blocks b
   INNER JOIN variants_variants v ON b.id=v.fk_block_id
   INNER JOIN languages l ON '.$sLanguageFilter.'
    LEFT JOIN variants_variant_content c ON c.fk_language_id=l.language_id AND c.fk_variant_id=v.id
        WHERE c.version<v.version
           OR c.id IS NULL
           LIMIT 0,20';

        $oView->aData=\DAL::executeQuery($sSQL);
        $oView->aLanguages=\Language::getLanguages();

        return $oView->output('withdrawal_queue');
    }

}