<?php
/**
 * Installs the installer
 * @package LoveLotto
 * @subpackage Installer
 */

namespace LL\Withdrawals;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    static function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        $aJobs[]=new \LL\Installer\Task('\LL\Withdrawals\Installer', 'installGatewayChanges', 'Install Withdrawals Management');
        return $aJobs;
    }

    /**
     * Adds the installer,module and handlers menu options to the main admin menu
     */
    static function installGatewayChanges(){
        $oTable=new \LL\Installer\Table('gateways');
        $oTable->addField(new \LL\Installer\Field('withdrawal_gateway', 'tinyint', 4));
        $oTable->addField(new \LL\Installer\Field('payment_gateway', 'tinyint', 4));

        $oTable->compile();


        self::installModule('Withdrawals',1);

    }
}
