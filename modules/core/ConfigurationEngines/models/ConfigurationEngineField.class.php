<?php
/**
 * Class for a configuration engine field. Configuration engines are used by Game engines,
 * game engine components/modifiers, payment gateways and ticket engines to enable
 * configuation pages to be created
 *
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
abstract class ConfigurationEngineField{
    var $sCaption;
    var $sName;
    var $bIsRequired=false;
    var $mValue='';

    /**
     * This is the view used to render this field in the admin panel. You should
     * override this in your model
     * @var string
     */
    var $sView='ConfigurationEngines/views/string';

    /**
     * Create a new field in the configuration engine
     * @param string $name Name of the field
     * @param string $caption Caption to be added to the form. Don't forget to use lang::get
     * @param boolean $isRequired Sets whether the field is required or not, default is true
     */
    function __construct($sName='',$sCaption='',$bIsRequired=true) {
        $this->sCaption=$sCaption;
        $this->sName=$sName;
        $this->bIsRequired=$bIsRequired;
    }

    /**
     * This should return null if the value is valid, or an error message
     * if not valid. Only one error can be returned per field, but you could
     * aggregate the error message as it's not checked against anything else
     * @return mixed
     */
    function validate(){
        if($this->bIsRequired && $this->mValue==''){
            return lang::get($this->sName.' MISSING');
        }else{
            return null;
        }
    }

    function output(){
        $oView=new LLView();
        foreach(get_object_vars($this) as $k=>$v){
            $oView->$k=$v;
        }
        return $oView->output($this->sView);
    }

    function processPost($aData){
        $this->mValue=$aData[$this->sName];
    }

    function serialize(){
        return $this->mValue;
    }

    function parseData($mValue){
        $this->mValue=$mValue;
    }
}