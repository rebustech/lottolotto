<?php
/**
 * Class for a configuration engine. Configuration engines are used by Game engines,
 * game engine components/modifiers, payment gateways and ticket engines to enable
 * configuation pages to be created
 *
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
class ConfigurationEngine{
    var $aFields=array();

    function validate(){
        foreach($this->aFields as $aField){
            $bFieldError=$aField->validate();
            if($bFieldError!=null){
                $aErrors[]=$bFieldError;
            }
        }
        return $aErrors();
    }

    function output(){
        foreach($this->aFields as $oField){
            $oField->output();
        }
    }

    function processPost($aData=null){
        if($aData===null) $aData=$_POST['config'];

        foreach($this->aFields as $oField){
            $oField->processPost($aData);
        }
    }

    function parseData($sData){
        $oData=json_decode($sData);
        foreach($oData as $sKey=>$mValue){
            if(is_object($this->aFields[$sKey])){
                $this->aFields[$sKey]->parseData($mValue);
            }
        }
    }

    function serialize(){
        foreach($this->aFields as $oField){
            $aData[$oField->sName]=$oField->serialize();
        }
        $oData=json_encode($aData);
        return $oData;
    }


    /**
     * Adds the provided field to this engine
     * @param ConfigurationEngineField $oField The field to add
     */
    function addField(ConfigurationEngineField $oField){
        $this->aFields[]=$oField;
    }
}