<?php

/**
 * Class for a simple currency configuration engine field.
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
class ConfigurationCurrency extends ConfigurationEngineField{
    var $view='ConfigurationEngines/views/currency';
}
