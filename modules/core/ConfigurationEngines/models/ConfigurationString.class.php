<?php

/**
 * Class for a simple string configuration engine field. 
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
class ConfigurationString extends ConfigurationEngineField{
    var $view='ConfigurationEngines/views/string';
}
