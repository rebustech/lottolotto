<?php

/**
 * Class for a simple string configuration engine field.
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
class ConfigurationSwitch extends ConfigurationEngineField{
    var $view='ConfigurationEngines/views/switch';
}
