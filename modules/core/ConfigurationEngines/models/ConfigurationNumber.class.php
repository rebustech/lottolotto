<?php

/**
 * Class for a simple number configuration engine field.
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
class ConfigurationNumber extends ConfigurationEngineField{
    var $view='ConfigurationEngines/views/number';
}
