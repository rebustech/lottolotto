<?php

/**
 * Class for a simple string configuration engine field.
 * @package LoveLotto
 * @subpackage ConfigurationEngines
 * @author Jonathan Patchett
 */
class ConfigurationSelection extends ConfigurationEngineField{
    var $view='ConfigurationEngines/views/selection';
    var $aOptions;

    function addOption($sKey,$sOption){
        $this->aOptions[$sKey]=$sOption;
    }
}
