<label id="field_<?=$this->sName?>" class="long<?=($this->bIsRequired)?' required':''?>">
    <span><?=$this->sCaption?></span>
    <div>
        <select id="field_<?=$this->sName?>" class="long<?=($this->bIsRequired)?' required':''?>">
            <option/>
            <? foreach($this->aOptions as $k=>$v): ?>
                <option value="<?=$k?>"><?=$v?></option>
            <? endforeach; ?>
        </select>
    </div>
</label>