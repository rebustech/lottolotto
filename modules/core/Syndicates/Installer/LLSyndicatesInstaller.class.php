<?php

namespace LL\Syndicates;

class Installer{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\Syndicates\Installer', 'installTables', 'Install core game engine components');
        return $aJobs;
    }


    static function installTables(){
        $oTable=new \LL\Installer\Table('syndicate_lines');
        $oTable->addField(new \LL\Installer\Field('last_booked', 'DATETIME'));
        $oTable->compile();

        /**
         *
         */
        $oTable=new \LL\Installer\Table('syndicate_members');
        $oTable->addField(new \LL\Installer\Field('start_date', 'DATE'));
        $oTable->addField(new \LL\Installer\Field('end_date', 'DATE'));
        $oTable->compile();
    }
}
