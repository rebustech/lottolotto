<?php

/**
 * Syndicate class
 *
 * @package LoveLotto
 * @subpackage GenericSyndicate
 * @author Nathan Pace
 */
class GenericSyndicate {
    // Code that was previously in this class has been moved out to a seperate
    // 'trait' file, which we pull in here to use as a class
    // Using name "GenericSyndicate" here to avoid conflicts
    use tSyndicate;
}
