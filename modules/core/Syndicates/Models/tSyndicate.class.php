<?php

/**
 * tSyndicate trait
 *
 * @package LoveLotto
 * @subpackage Syndicate
 * @author Nathan Pace
 */
trait tSyndicate {

    // List of all possible statuses a syndicate could have
    private static $aStatuses = array(1=>"Open and Empty",
                                      2=>"Open",
                                      3=>"Closed, awaiting draw",
                                      4=>"Drawn",
                                      5=>"Processed",
                                      9=>'Cancelled');

    public function __construct()
    {
        // Constructor method
    }

    /**
     * Call the JSON API to return the random numbers
     *
     * @param integer $iAmount
     * @param integer $iMax
     * @return array
     */
    public function getRandomNumbersFromJSON($iAmount, $iMax, $iMin=1)
    {

        $oJSON = new JsonRPCClient('https://api.random.org/json-rpc/1/invoke');

        // Setup what we need
        $apiData = array(
                "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f",
                "n" => $iAmount,
                "min" => $iMin,
                "max" => $iMax,
                "replacement" => false
        );

        // Process the request
        $response = $oJSON->execute('generateIntegers', $apiData);

        // Encode and decode data
        $aDataEnc = json_encode($response);
        $aDataDec = json_decode($aDataEnc);

        // Assign random numbers to an array
        $aNumbers = $aDataDec->random->data;

        // Sort the array numerically before returning
        sort($aNumbers);

        return $aNumbers;
    }

    /**
     * Inserts syndicate details into database and returns auto-increment id
     *
     * @param array $aDetails
     * @return integer
     */
    public function createSyndicate($aDetails)
    {

        $dStartDate = date("Y-m-d H:i:s");

        $tablename = "syndicates";
        $data = array(
            'start_date' => $dStartDate,
            'status' => 1,
            'current_shares' => 0,
            'max_shares' => $aDetails['iMaxShares'],
            'price_per_share' => $aDetails['fPricePerShare'],
            'game_engine_id' => $aDetails['game_engine_id']
        );
        return DAL::Insert($tablename, $data, true);
    }

    /**
     * Adds lotteries to each syndicate
     *
     * @param array $aDetails
     * @return boolean
     */
    public function addLotteryToSyndicate($aDetails, $iSyndicateId)
    {
        $tablename = "syndicate_lotteries";
        $data = array(
            'lottery_id' => $aDetails['iLotteryId'],
            'syndicate_id' => $iSyndicateId,
            'numbers_playing_from' => $aDetails['iNumRandomNumbers'],
            'numbers_to_select' => $aDetails['iNumInEachCombination'],
            'original_numbers' => str_replace("-", "|", $aDetails['sOriginalNumbers']),
            'bonus_numbers' => str_replace("-", "|", $aDetails['sBonusNumbers']),
            'num_generated' => $aDetails['iOriginalNumLinesGenerated'],
            'num_lines' => $aDetails['iNumLines']
        );
        return DAL::Insert($tablename, $data);
    }




    /**
     * Inserts each syndicate line into the database
     *
     * @param integer $iSyndicateId
     * @param integer $iNumLines
     * @param array $aLines
     * @param string $sBonusNums
     */
    public function insertLines($iSyndicateId, $iLotteryId, $aLines, $sBonusNums)
    {

        $tablename = "syndicate_lines";

        foreach ($aLines as $sLine)
        {
            // Add bonus numbers to the line if present
            if ($sBonusNums != "")
            {
                $sLine .= "|" . str_replace("-", "|", $sBonusNums);
            }
            $data = array(
               'syndicate_id' => $iSyndicateId,
               'lottery_id' => $iLotteryId,
               'line_numbers' => $sLine
           );
           DAL::Insert($tablename, $data);
        }
    }

    /**
     * Gets all possible combinations of numbers of the specified size
     * from the supplied master array of numbers
     *
     * @param array $aNumberArray
     * @param integer $iSubsetSize
     * @return array
     */
    public function getCombinations($aNumberArray, $iSubsetSize)
    {

        // Combinations class does most of the work here
        $aCombinations = new Combinations($aNumberArray,$iSubsetSize);

        return $aCombinations;
    }


    /**
     * Used to return the number of combinations of $r numbers
     * that can be made from a pool of $n numbers
     *
     * @param integer $n
     * @param integer $r
     * @return integer
     */
    public function nCr($n,$r)
    {
       if ($r > $n)
       {
          return null;
       }
       if (($n-$r) < $r)
       {
          return $this->nCr($n,($n-$r));
       }
       $return = 1;

       for ($i=0;$i < $r;$i++){
          $return *= ($n-$i)/($i+1);
       }
       return $return;
    }


    /**
     * Return all syndicates stored in the system.
     * An optional status id can be specified
     *
     * @param integer $iStatus
     * @return array
     */
    public static function getSyndicates($iStatus = NULL)
    {
        $aResults = array();

        $sSQL = "SELECT s.*, s.max_shares-s.current_shares as shares_left " .
                "FROM syndicates s";

        // Optional status id
        if ($iStatus != NULL)
        {
            $iStatus = (int) $iStatus;
            $sSQL .= " WHERE status = $iStatus";
        }

        $aResults = DAL::executeQuery($sSQL);

        // Get lotteries in this syndicate
        foreach ($aResults as $key=>$aResult)
        {
            $sSQL1 = "SELECT lc.comment FROM lotteries_cmn lc " .
                     "LEFT JOIN syndicate_lotteries sl ON lc.lottery_id = sl.lottery_id " .
                     "WHERE sl.syndicate_id = " .$aResult['id'];

            $aResults[$key]['lotteries'] = DAL::executeQuery($sSQL1);

        }

        return $aResults;
    }

    /**
     * Information about one syndicate - including lotteries, lines and members
     *
     * @param integer $iSyndicateId
     * @return array
     */
    public static function getSyndicateInfo($iSyndicateId)
    {

        $iSyndicateId = (int) $iSyndicateId;

        $sSQL = "SELECT s.*, s.max_shares-s.current_shares as shares_left " .
                "FROM syndicates s " .
                "WHERE id = $iSyndicateId";


        $aInfo = DAL::executeGetRow($sSQL);

        // If information about the syndicate has been found,
        // extract lines in this syndicate  and current syndicate members
        if ($aInfo)
        {
            $aInfo['status_text'] = self::$aStatuses[$aInfo['status']];

            // Syndicate lotteries
            $sSQL1 = "SELECT sl.*, lc.comment " .
                     "FROM syndicate_lotteries sl " .
                     "LEFT JOIN lotteries_cmn lc on sl.lottery_id = lc.lottery_id " .
                     "WHERE syndicate_id = $iSyndicateId";

            $aInfo['lotteries'] = DAL::executeQuery($sSQL1);


            // Syndicate lines
            $sSQL2 = "SELECT sl.*, lc.comment " .
                     "FROM syndicate_lines sl " .
                     "LEFT JOIN lotteries_cmn lc on sl.lottery_id = lc.lottery_id " .
                     "WHERE syndicate_id = $iSyndicateId";

            $aInfo['lines'] = DAL::executeQuery($sSQL2);


            // Syndicate members
            $sSQL3 = "SELECT m.email, m.firstname, m.lastname, sm.num_shares " .
                     "FROM members m ".
                     "LEFT JOIN syndicate_members sm on m.member_id = sm.member_id " .
                     "WHERE sm.syndicate_id = $iSyndicateId";

            $aSyndicateMembers = DAL::executeQuery($sSQL3);

            foreach ($aSyndicateMembers as $aMember)
            {
                $aInfo['members'][] = $aMember;
            }
        }

        return $aInfo;
    }

    /**
     * Update the syndicate record with a booking ID.
     * Changes status to indicate syndicate is no longer empty
     *
     * @param integer $iBookingId
     * @param integer $iSyndicateId
     * @return array
     */
    public function updateSyndicateBookingId($iBookingId, $iSyndicateId)
    {
        $iBookingId = (int) $iBookingId;
        $iSyndicateId = (int) $iSyndicateId;

        $tablename = "syndicates";
        $data = array(
            'booking_id' => $iBookingId,
            'status' => 2
        );
        $params = 'id = ' . $iSyndicateId;

        return DAL::Update($tablename, $data, $params);
    }

    /**
     * Update syndicate with a game engine ID
     *
     * @param integer $iGameEngineId
     * @param integer $iSyndicateId
     * @return array
     */
    public function updateSyndicateGameEngineId($iGameEngineId, $iSyndicateId)
    {
        $iSyndicateId = (int) $iSyndicateId;

        $tablename = "syndicates";
        $data = array(
            'game_engine_id' => $iGameEngineId
        );
        $params = 'id = ' . $iSyndicateId;

        return DAL::Update($tablename, $data, $params);
    }

    /**
     * Add a member and their number of shares to the syndicate_members table
     *
     * @param integer $iSyndicateId
     * @param integer $iMemberId
     * @param integer $iNumShares
     *
     */
    private function addMemberToSyndicate($iSyndicateId, $iMemberId, $iNumShares, $sEndDate)
    {
        /**
         * Check there are enough spaces. If not make a new syndicate and use that instead
         *
         */


        $iSyndicateId = (int) $iSyndicateId;
        $iMemberId = (int) $iMemberId;

        $tablename = "syndicate_members";
        $data = array(
            'syndicate_id' => $iSyndicateId,
            'member_id' => $iMemberId,
            'num_shares' => $iNumShares,
            'start_date' => date('Y-m-d H:i:s'),
            'end_date' => $sEndDate
        );
        return DAL::Insert($tablename, $data, true);
    }

    /**
     * Checks to see if we have the correct number of space open to the public
     * At the moment that means at least iMinNumberOfSyndicates syndicates in total
     * and at least iMinOpenOfSyndicates syndicates with space in them
     *
     * If we don't have enough then create more uusing generateSyndicateFrom GameEngine
     */
    public function checkSyndicateSpaces(){
        $this->oConfig=json_decode($this->config);

        $iTotalSyndicates=\DAL::executeGetOne('SELECT count(*) FROM syndicates WHERE game_engine_id='.$this->id);
        $iTotalSyndicatesOpen=\DAL::executeGetOne('SELECT count(*) FROM syndicates WHERE current_shares<(max_shares-1) AND game_engine_id='.$this->id);

        $loops=0;
        while(($iTotalSyndicatesOpen < $this->oConfig->iMinOpenSyndicates || $iTotalSyndicates < $this->oConfig->iMinNumberOfSyndicates) && $loops<20){
            $this->generateSyndicateFromGameEngine();
            $iTotalSyndicates=\DAL::executeGetOne('SELECT count(*) FROM syndicates WHERE game_engine_id='.$this->id);
            $iTotalSyndicatesOpen=\DAL::executeGetOne('SELECT count(*) FROM syndicates WHERE current_shares<(max_shares-1) AND game_engine_id='.$this->id);
            $loops++;
        }

        if($loops==0){
            Error::Errors()->addNotice('There are already '.$iTotalSyndicatesOpen.' open syndicates in this product. Any changes you made to the product will be reflected on any new sydictates that are opened');
        }else{
            Error::Errors()->addNotice('Created '.$loops.' new syndicates for this product with the new settings.'.$iTotalSyndicatesOpen.' open syndicates will continue to operate with the previous settings');
        }

        $this->updateExistingSyndicateData();
    }

    /**
     * Updates any existing syndicates. Only certain data can be updated on already
     * existing syndicates
     *
     * ALTER TABLE `maxlotto_test`.`syndicates`
     * ADD COLUMN `low_availability_threshold` INT NULL AFTER `game_engine_id`,
     * ADD COLUMN `sold_out_threshold` INT NULL AFTER `low_availability_threshold`;
     */
    public function updateExistingSyndicateData(){
        $oConfig=json_decode($this->config);
        $aData=array(
            'low_availability_threshold'=>$oConfig->iLowAvailabilityThreshold,
            'sold_out_threshold'=>$oConfig->iSoldOutThreshold
        );
        \DAL::Update('syndicates', $aData, 'game_engine_id='.$this->id);
    }

    /**
     * Used to forcibly close a syndicate
     * @param type $iSyndicateId
     *
     * ALTER TABLE `maxlotto_test`.`syndicates`
     * ADD COLUMN `cancelled_at` DATETIME NULL AFTER `sold_out_threshold`,
     * ADD COLUMN `cancelled_by` INT(11) NULL AFTER `cancelled_at`;
     */
    public function cancelSyndicate($iSyndicateId){
        global $oSecurityObject;
        $aData=array(
            'cancelled_at'=>date('Y-m-d H:i:s'),
            'cancelled_by'=>$oSecurityObject->getiUserID()
        );
        \DAL::Update('syndicates', $aData, 'id='.$iSyndicateId);
        $this->checkSyndicateSpaces();
    }

    /**
     * Creates a new syndicate
     * Adapted from Nathans syndicate-create.php
     */
    public function generateSyndicateFromGameEngine(){
        /**
         * This method can only work when part of a game engine
         */
        if(!$this instanceof GameEngine) throw new LottoException('method generateSyndicateFromGameEngine can only be used as part of an instantiated GameEngine');


        /**
         * First off create the new syndicate
         */
        $oSyndicate=new GenericSyndicate();

        if($this->config==''){
            Error::Errors()->addError('No configuration set for this syndicate. Please check the number of shares, and the number of syndiate options');
        }

        $this->oConfig=json_decode($this->config);
        $aData=array();
        $aData['iMaxShares'] = $this->oConfig->iTotalShares;            //Total shares : comes from game engine config
        $aData['fPricePerShare'] =  $this->base_price;                  //This isn't actually used anywhere, but useful for reporting on price changes
        $aData['game_engine_id'] =  $this->id;                          //This isn't actually used anywhere, but useful for reporting on price changes

        if($aData['iMaxShares']==0){
            Error::Errors()->addError('You need to set the number of shares that each instance of this syndicate will have');
        }
        if($this->base_price==0){
            Error::Errors()->addError('You need to set a price in the Base Price field');
        }
        if($aData['game_engine_id']==0){
            Error::Errors()->addError('No game engine set. This is an internal error, please contact dev for clarification');
        }

        if($aData['iMaxShares']==0){
            Error::Errors()->addError('You need to set the number of shares that each instance of this syndicate will have');
        }

        $iSyndicateId = $oSyndicate->createSyndicate($aData);

        if($iSyndicateId==0){
            Error::Errors()->addError('Was not able to create a syndicate');
            return;
        }

        /**
         * Check that the config is correct on all the components
         */
        foreach($this->getChildComponents() as $oChild){
            if($oChild instanceOf \Lottery){

                $oChildConfig=json_decode($oChild->config);
                if($oChildConfig->iNumberCount < $oChild->getNumbersCount()){
                    Error::Errors()->addError('Configuration for '.get_class($oChild).' must have at least '.$oChild->getNumbersCount().' to work');
                }
                if($oChildConfig->iTickets<1){
                    Error::Errors()->addError('Configuration for '.get_class($oChild).' must have at least 1 ticket to work');
                }

            }else{
                Error::Errors()->addMessage('Unable to use '.get_class($oChild).' as a syndicate component. It can remain as part of the product, but will not be used in syndicates');
            }
        }


        /**
         * Now run through each lottery component and add to the syndicate
         * each component will tell us how many lines and how many numbers to use
         */
        $iLotteriesAdded=0;
        foreach($this->getChildComponents() as $oChild){
            if($oChild instanceOf \Lottery){
                $oChildConfig=json_decode($oChild->config);

                $aData=array();                                                 //Setup an array that will be used to collect all the data for passing to the syndicate system
                /**
                 * Get the lottery, slightly tricky as there's no direct link from
                 * a component to a lottery. linking by classname against handler (same thing)
                 * Then assign that to the data to be used to generate the syndicate
                 */
                $aData['iLotteryId'] = $oChild->iLotteryID;
                $iLotteryId=$oChild->iLotteryID;

                // These variables come from the instantiated lottery class
                $iNumInEachCombination = (int) $oChild->getNumbersCount();
                $iMaxNumbers = (int) $oChild->getMaxNumber();
                $iNumBonusNumbers = $oChild->getBonusNumbers();
                $iNumRandomNumbers = (int) $oChildConfig->iNumberCount;
                $iNumLines = $aNumLines[$iLotteryId];

                /**
                 * Generate the numbers from the RNG and add to the dataset
                 */
                $aNumbers = $this->getRandomNumbersFromJSON($iNumRandomNumbers,$iMaxNumbers);
                $sNumbers = implode("|", $aNumbers);
                $aData['sOriginalNumbers'] = $sNumbers;                         //Original numbers chosen by the RNG

                Error::Errors()->addNotice('Creating a syndicate using the following numbers '.$sNumbers);


                /**
                 * Wheel it
                 */
                $iNumLinesGenerated = $this->nCr($iNumRandomNumbers,$iNumInEachCombination);
                // Post onto array
                $aSyndicateToCreate[$iLotteryId] = array('num_generated' => $iNumLinesGenerated,
                                                         'numbers' => $sNumbers,
                                                         'num_lines' => $iNumLines,
                                                         'num_in_each_combination' => $iNumInEachCombination,
                                                         'num_random_numbers' => $iNumRandomNumbers,
                                                         'max_numbers' => $iMaxNumbers,
                                                         'lottery_name' => $oChild->getComment());

                /**
                 * Setup the bonus numbers and add to the dataset
                 */
                if ($iNumBonusNumbers > 0)
                {
                    $iMinBonusNum = $oChild->getMinBonusNumber();
                    $iMaxBonusNum = $oChild->getMaxBonusNumber();
                    $aBonusNumbers = $this->getRandomNumbersFromJSON($iNumBonusNumbers,$iMaxBonusNum,$iMinBonusNum);
                    $sBonusNumbers = implode("|", $aBonusNumbers);
                    $aSyndicateToCreate[$iLotteryId]['bonus_numbers'] = $sBonusNumbers;

                    $aData['sBonusNumbers'] = $sBonusNumbers;
                }
                else
                {
                    $aSyndicateToCreate[$iLotteryId]['bonus_numbers'] = '';
                    $aData['sBonusNumbers'] = '';
                }

                $aData['iNumLines'] = $oChildConfig->iTickets;                  //Lines to buy from the generated set comes from the component

                $aData['iLotteryId'] = (int) $oChild->iLotteryID;
                $aData['iNumRandomNumbers'] = (int) $iNumRandomNumbers;
                $aData['iNumInEachCombination'] = (int) $iNumInEachCombination;
                $aData['iOriginalNumLinesGenerated'] = (int) $iNumLinesGenerated;

                $oSyndicate->addLotteryToSyndicate($aData, $iSyndicateId);

                $iLotteriesAdded++;

                // Get combinations of numbers according to specifications
                $aCombinations = $oSyndicate->getCombinations($aNumbers, $aData['iNumInEachCombination']);

                // Array to hold "formatted" lines
                // which will be passed to line storing function
                $iTotalLines = 0;
                $aAllLines = array();

                foreach ($aCombinations as $aCombination)
                {
                    // Using a 1-indexed array for storing, so increase count here
                    $iTotalLines++;

                    // Numbers are in array format here, so implode onto string
                    $sCombination = implode("|", $aCombination);

                    // Add to holding array
                    $aAllLines[$iTotalLines] = $sCombination;

                }

                Error::Errors()->addNotice("{$iTotalLines} lines created<br/>");
                //print_d($aAllLines);

                // All possible combinations have been created at this stage.

                // If the requested number of lines is LESS than the number of generated lines,
                // we need to remove lines from the master generated lines array until we have the right number
                if ($aData['iNumLines'] < $iTotalLines)
                {
                    // Num of lines to remove
                    $iLinesToRemove = $iTotalLines - $aData['iNumLines'];

                    Error::Errors()->addNotice("Need to remove {$iLinesToRemove} lines - these numbers will be removed<br/>");

                    // Because the master array is 1-indexes, we can call the random number generator a second time
                    // to generate a random selection of n numbers from x numbers
                    // where n = number of lines to remove and x = number of lines in master array.
                    // Once we have the numbers (returned as an array), we can flip the array
                    // so that the values returned become the keys
                    $aRemoveNumbers = array_flip($this->getRandomNumbersFromJSON($iLinesToRemove,$iTotalLines));

                    //print_d($aRemoveNumbers);

                    // Now, use array_diff to compute the 'difference' between the two arrays
                    // (e.g. all entries in $aAllLines which are not present in $aRemoveNumbers)
                    // Using the keys of the array as the inputs
                    $aSyndicateLineKeys = array_diff ( array_keys($aAllLines) , array_keys($aRemoveNumbers) );

                    $aLinesToStore = array();

                    // We now have the required keys of the lines to select from the master array
                    foreach ($aSyndicateLineKeys as $iSyndicateLineKey)
                    {
                        $aLinesToStore[] = $aAllLines[$iSyndicateLineKey];
                        //echo ("Line $iSyndicateLineKey chosen: $aAllLines[$iSyndicateLineKey] <br/>");
                    }

                    // Insert syndicate lines
                    $oSyndicate->insertLines($iSyndicateId, $iLotteryId, $aLinesToStore, $aData['sBonusNumbers']);

                    Error::Errors()->addNotice(count($aLinesToStore) . " lines stored");
                }
                else
                {
                    // If the requested number of lines is greater than, or equal to, the number
                    // of generated combinations, then just store them.
                    $oSyndicate->insertLines($iSyndicateId, $iLotteryId, $aAllLines, $aData['sBonusNumbers']);

                    Error::Errors()->addNotice(count($aAllLines) . " lines stored");
                }



            }
        }
    }

     /**
     * Update the current number of purchased shares in this syndicate
     *
     * @param integer $iSyndicateId
     * @param integer $iMemberId
     *
     */
    private function updateCurrentSharesInSyndicate($iSyndicateId, $iNumShares)
    {
        $tablename = "syndicates";
        $data = array(
            'current_shares' => 'current_shares + ' . $iNumShares
        );
        $params = 'id = ' . $iSyndicateId;

        return DAL::Update($tablename, $data, $params);

    }


    /**
     * Catch-all function to deal with the purchase of a share or shares in a syndicate
     *
     * We can add in other functionality as desired
     *
     * @param integer $iSyndicateId
     * @param integer $iMemberId
     * @param integer $iNumShares
     *
     * @return boolean
     */
    public function purchaseSyndicateShares($iSyndicateId, $iMemberId, $iNumShares, $sEndDate)
    {
        $retval=$this->addMemberToSyndicate($iSyndicateId, $iMemberId, $iNumShares,$sEndDate);
        $this->recalculateSyndicateShares();
        return $retval;
    }

    /**
     * Returns current number of shares remaining in each syndicate
     *
     * @return array
     */
    public static function getSharesRemainingInAllSyndicates()
    {
        $aData = array();
        $sSQL = "SELECT id, max_shares-current_shares as shares_left " .
                "FROM syndicates";

        foreach (DAL::executeQuery($sSQL) as $aRow)
        {
            $aData[$aRow['id']] = $aRow['shares_left'];
        }

        return $aData;
    }

    /**
     * Locates all syndicate lotteries that need tickets booking through the normal
     * system, and creates the bookings.
     *
     * Spotting these by joining up into the booking items table will be too slow
     * so added new fields to check against
     *
     * ALTER TABLE `syndicate_lines` ADD COLUMN `last_booked` DATETIME NULL AFTER `line_numbers`;
     * ALTER TABLE `syndicate_members` ADD COLUMN `start_date` DATETIME NULL;
     * ALTER TABLE `syndicate_members` ADD COLUMN `end_date` DATETIME NULL;
     *
     * Bookings are always assigned to the syndicate manager which at the moment is
     * user 1 (us) - so that's hardcoded
     *
     * Tempted to run this every 30 mins just in case any issues arise from normal
     * usage. Regular running will auto-fix any issues.
     */
    public static function bookTickets(){
        /**
         * First off we need to refresh all the shares so we know which syndicates
         * are still active and need buying for
         */
        $this->recalculateSyndicateShares();

        /**
         * This query will get us a list of lines in syndicates that are still operating
         * where the last purchased draw is earlier than the next draw. Also brings back
         * the next draw date to make it easy to book the tickets
         */
        $sSQL='SELECT s.id,l.fk_lottery_id,l.drawdate,s.line_numbers FROM
                        (SELECT fk_lottery_id,MIN(drawdate) drawdate FROM r_lottery_dates WHERE drawdate>NOW() GROUP BY fk_lottery_id) AS l
                        INNER JOIN syndicate_lines s ON s.lottery_id=l.fk_lottery_id
                        INNER JOIN syndicates d ON d.id=s.syndicate_id
                        AND s.last_booked < l.drawdate
                        AND current_shares>0';

        $aItemsToBook=\DAL::executeQuery($sSQL);

        /**
         * NOTE : These tickets are booked straight into booking_items, there is no order associated
         *        with these - this needs some more careful thought. Having an order seems like
         *        a good idea, but also it's potentially pointless and adds complexity
         *        It might be better just to track these through a new fk_syndicate_id field on
         *        booking items
         */
        foreach($aItemsToBook as $aItemToBook){
            $aData=array(
                'fk_lottery_id'=>$aItemToBook['fk_lottery_id'],
                'fk_lottery_date'=>$aItemToBook['drawdate'],
                'numbers'=>$aItemToBook['line_numbers']
            );
            \DAL::Insert('booking_items', $aData);

            /**
             * Mark the line as booked for that draw
             */
            \DAL::Update('syndicate_lines', array('last_booked'=>$aItemToBook['drawdate']), 'id='.$aItemToBook['id']);
        }

    }

    /**
     * Just updates the number of current active shares in all syndicates based on
     * the start and end date of the shares purchased
     */
    function recalculateSyndicateShares(){
        $sSQL='UPDATE syndicates s SET current_shares=(SELECT SUM(num_shares) FROM syndicate_members m WHERE m.syndicate_id=s.id AND end_date>NOW() AND start_date<=NOW())';
        \DAL::executeQuery($sSQL);
    }

    /**
     * Gets three syndicates to show on the frontend
     *
     * 1. least full
     * 2. most full but not full
     * 3. full - if there are none full then grab another almost full
     *
     */
    function getFrontEndSyndicates($iItemsToGet=3){
        /**
         * This method can only work when part of a game engine
         */
        if(!$this instanceof GameEngine) throw new LottoException('method getFrontEndSyndicates can only be used as part of an instantiated GameEngine');

        $sSQL="SELECT 1 AS g, a1.* FROM (SELECT *,(SELECT line_numbers FROM syndicate_lines sl1 WHERE sl1.syndicate_id=s1.id LIMIT 0,1) as numbers,(SELECT num_lines FROM syndicate_lotteries sl1 WHERE sl1.syndicate_id=s1.id LIMIT 0,1) AS num_lines FROM syndicates s1 WHERE current_shares<low_availability_threshold AND game_engine_id=".$this->id." ORDER BY current_shares LIMIT 0,9) a1
                UNION
                SELECT 2 AS g, a2.* FROM (SELECT *,(SELECT line_numbers FROM syndicate_lines sl2 WHERE sl2.syndicate_id=s2.id LIMIT 0,1) as numbers,(SELECT num_lines FROM syndicate_lotteries sl2 WHERE sl2.syndicate_id=s2.id LIMIT 0,1) AS num_lines FROM syndicates s2 WHERE current_shares>=low_availability_threshold AND current_shares>sold_out_threshold AND game_engine_id=".$this->id." ORDER BY current_shares DESC LIMIT 0,9) a2
                UNION
                SELECT 3 AS g, a3.* FROM (SELECT *,(SELECT line_numbers FROM syndicate_lines sl3 WHERE sl3.syndicate_id=s3.id LIMIT 0,1) as numbers,(SELECT num_lines FROM syndicate_lotteries sl3 WHERE sl3.syndicate_id=s3.id LIMIT 0,1) AS num_lines FROM syndicates s3 WHERE current_shares>=sold_out_threshold AND game_engine_id=".$this->id." ORDER BY current_shares LIMIT 0,9) a3";
        $aPossibles=\DAL::executeQuery($sSQL);

        /**
         * Now we take the $aPossibles array and use it to fill up the $aFinal array which will
         * contain the 3 syndicates to show
         */
        $aFinal=array(); //Final entries array, will be filled with the $iItemsToGet syndicates we're going to show
        $iPosition=1;    //Tracks the current final entry, these range from 1 to $iItemsToGet
        $iResetTo=2;     //Each time $iPosition gets to $iItemsToGet, $iPosition will be reset to this. Each time round the loop this increments so that we basically backfill the $aFinal array
        $iRequire=1;     //The minimum value of g from the above query. This is 1=Syndicates with plenty of space, 2=Syndicates with few spaces, 3=Full
        $bCanReset=false;
        foreach($aPossibles as $aPossible){
            if($aPossible[g]<=$iRequire && $iPosition<=$iItemsToGet){
                $aPossible['numbers']=explode('|',$aPossible['numbers']);
                $bCanReset=true;
                $aFinal[$iPosition]=$aPossible;
            }
            $iPosition++;
            if($iPosition>$iItemsToGet){
                if($bCanReset){
                    $iPosition=$iResetTo++;
                }else{
                    $iPosition=1;
                }
                $iRequire++;
                $bCanReset=false;
            }
        }
        return $aFinal;
    }

    /**
     * Gets all the lines in a syndicate for display on the frontend
     * @param type $iSyndicateId
     * @return type array
     */
    function getSyndicateLines($iSyndicateId){
        $sSQL='SELECT l.line_numbers,l.lottery_id,c.comment,c.className,c.number_count,c.bonus_numbers
                 FROM syndicate_lines l
           INNER JOIN lotteries_cmn c ON c.lottery_id=l.lottery_id
           WHERE syndicate_id='.$iSyndicateId;
        $aData=\DAL::executeQuery($sSQL);
        foreach($aData as &$aRow) $aRow['line_numbers']=explode('|',$aRow['line_numbers']);
        return $aData;
    }
}
