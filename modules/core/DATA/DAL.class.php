<?php

/**
 * Data Access Layer
 * Part of the original package from LoveLotto
 * Provides database access using the mysql methods. As mysql is now deprecated
 * this should be upgraded asap to pdo or mysqli
 *
 * @todo Upgrade to use mysqli or pdo - note: we will need to check the rest of the system
 *       for this aswell as we can't really use both alongside each other
 *       it is highly likely mysql_fetchXXX will have been used a lot
 *
 * @package LoveLotto
 * @subpackage Data
 */
class DAL {
    /**
     * Uses lazy connections, so this keeps track if if a connection is needed
     * @var bool (sort of - actually 0=not connected, 1=connected)
     */
    static private $isConnected = 0;
    static $iQueryCount=0;
    static $fQueryTime=0;
    static $iUpdateCount;
    static $iInsertCount;

    /**
     * Define available events
     */
    const EVENT_BEFORE_DAL_UPDATE='EVENT_BEFORE_DAL_UPDATE';
    const EVENT_AFTER_DAL_UPDATE='EVENT_AFTER_DAL_UPDATE';

    /**
     * Connect to the database
     * Connection details are determined by includes/constants.php
     * @param string $sCharSet which charset to use to connect to the database
     */
    public function connect($sCharSet = 'utf8') {
        try {
            $db_host = DBSERVERADDRESS;
            $db_username = DBUSERNAME;
            $db_password = DBPASSWORD;
            $db_name = DATABASENAME;
            $con = mysql_connect($db_host, $db_username, $db_password) or self::catchError("Unable to connect");
            mysql_select_db($db_name, $con) or self::catchError("Database not found");
            self::setCharSet($sCharSet);
            self::$isConnected = 1;
        } catch (Exception $e) {
            self::$isConnected = 0;
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }

    static function setCharSet($sCharSet = 'utf8'){
        mysql_set_charset($sCharSet) or self::catchError("CharSet Config Failed");
    }

    /**
     * Just run an sql statement and return an array of associative arrays contining
     * all the data
     * @param string $sSQL The sql to exectute
     */
    static public function executeQuery($sSQL) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        self::$iQueryCount++;
        try {

            if (self::$isConnected == 0) {
                self::connect();
            }
            $rQuery = @mysql_query($sSQL) or self::catchError('executeQuery - Query Failed', $sSQL);
            $aResult = Array();
            while ($aRowData = @mysql_fetch_assoc($rQuery))
                array_push($aResult, $aRowData);
            unset($rQuery);
            unset($aRowData);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            return $aResult;
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }


    /**
     * Get all unique records from an sql statement
     * Not sure why this exists, surely a group by statment would be better
     * I am marking this as depricated for that reason - do not use in any future coding
     * WARNING : This method does not guard against SQL injection, please ensure that all SQL
     * passed to this method is cleaned first
     * @deprecated DO NOT USE - WE SHOULD LOOK INTO REMOVING ALL USAGE OF THIS
     * @param string $sSQL The SQL to execute
     * @param string $sUniqueKey The field to use as a unique key
     * @return array
     */
    static public function executeGetUnique($sSQL, $sUniqueKey) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        self::$iQueryCount++;
        try {
            if (self::$isConnected == 0) {
                self::connect();
            }
            $rQuery = @mysql_query($sSQL) or self::catchError('executeQuery - Query Failed', $sSQL);
            $aResult = Array();
            $aKeys = array();
            while ($aRowData = @mysql_fetch_assoc($rQuery)) {
                if (!array_key_exists($aRowData[$sUniqueKey], $aKeys))
                    array_push($aResult, $aRowData);
                $aKeys[$aRowData[$sUniqueKey]] = 1;
            }
            unset($rQuery);
            unset($aRowData);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            return $aResult;
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }


    /**
     * Execute an sql statement and return the first row of data as an associative array
     * WARNING : This method does not guard against SQL injection, please ensure that all SQL
     * passed to this method is cleaned first
     * @param string $sSQL The SQL to execute.
     * @return array
     */
    static public function executeGetRow($sSQL) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        self::$iQueryCount++;
        try {

            if (self::$isConnected == 0) {
                self::connect();
            }
            $rQuery = mysql_query($sSQL) or self::catchError("executeGetRow - Query Failed", $sSQL);
            $aResult = mysql_fetch_assoc($rQuery);
            unset($rQuery);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            return $aResult;
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }


    /**
     * Executes a scalar query - so runs the SQL and returns the value of the first field of the first
     * record returned.
     * WARNING : This method does not guard against SQL injection, please ensure that all SQL
     * passed to this method is cleaned first
     * @param string $sSQL SQL To execute
     * @return mixed
     */
    public static function executeGetOne($sSQL) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        self::$iQueryCount++;
        try {

            if (self::$isConnected == 0) {
                self::connect();
            }
            $rQuery = @mysql_query($sSQL) or self::catchError("executeGetOne - Query Failed", $sSQL);
            $aResult = @mysql_fetch_row($rQuery);
            unset($rQuery);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            return $aResult[0];
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }



    /**
     * Runs a query and return the number of affected rows. Does not return any results,
     * or a resource to read results from, so can only be used for INSERT, UPDATE, DELETE, REPLACE
     * etc... operations
     * WARNING : This method does not guard against SQL injection, please ensure that all SQL
     * passed to this method is cleaned first
     * @param string $sSQL The SQL to execute
     * @return int The number of rows affected (result of mysql_affected_rows)
     */
    static public function Query($sSQL) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        self::$iQueryCount++;
        try {

            if (self::$isConnected == 0) {
                self::connect();
            }
            @mysql_query($sSQL) or self::catchError("Query - Query Failed", $sSQL);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            return (@mysql_affected_rows());
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }


    /**
     * Returns the number of rows returned by an SQL statement
     * Basically just runs mysql_num_row
     * @todo test this - I doubt this works!
     * @param string $sSQL The SQL to execute
     * @return int
     */
    static public function getLastRowCount($sSQL) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        try {
            if (self::$isConnected == 0) {
                self::connect();
            }
            $rQuery = @mysql_num_rows($sSQL) or self::catchError("getLastRowCount - Query Failed", $sSQL);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            return $count;
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }
    }


    /**
     * Builds and runs an insert statement with the provided data
     * @param string $sTableName The table to insert data into
     * @param array $aFields Associative array of data to insert
     * @param boolean $bReturn Type of return. if false returns true, otherwise returns the number of records affected
     * @return mixed
     */
    static public function Insert($sTableName, $aFields, $bReturn = false,$sInstruction='INSERT IGNORE ') {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        } else {
            $sDebug = false;
        }

        self::$iQueryCount++;
        self::$iInsertCount++;
        try {
            if (self::$isConnected == 0) {
                self::connect();
            }

            $sSQL = $sInstruction.' INTO '.$sTableName;
            foreach ($aFields as $key => $value) {
                $aFieldList[] = '`'.$key.'`';
                if (!is_null($value)) {
                    $aSetFields[] = "'".mysql_escape_string($value)."'";
                } else {
                    $aSetFields[] = 'null';
                }
            }
            $sSQL .= '('.implode(",", $aFieldList).') VALUES ('.implode(",", $aSetFields).')';
            @mysql_query($sSQL) or self::catchError("Insert - Query Failed", $sSQL);

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            if ($bReturn) {
                $bReturnValue = @mysql_insert_id();
            } else {
                $bReturnValue = true;
            }
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }

        return $bReturnValue;
    }

    /**
     * Builds and runs an update statement with the provided data
     * JP: Added several event handlers
     * JP: Added hooks into a static method on a dynamically loaded model with the same name as $sTableName
     * @param string $sTableName The table to insert data into
     * @param array $aFields Associative array of data to insert
     * @param string $sWhereQuery where clause used to match the record(s) to update
     * @param boolean $bReturn Type of return. if false returns true, otherwise returns the number of records affected
     * @return boolean
     */
    static public function Update($sTableName, $aFields, $sWhereQuery, $bReturn = false,$sDebug=false) {

        if(!empty($_COOKIE['debugSite'])) {
            $sDebug = true;
            $startTime = microtime(true);
        }

        self::$iQueryCount++;
        self::$iUpdateCount++;
        /**
         * Set up an event and a hook before update happens
         */
        $sModelName=$sTableName.'_Model';
        if(class_exists($sModelName)){
            $oModel=new $sModelName();
            $oModel->beforeDALUpdate($aFields, $sWhereQuery);
        }
        Events::trigger(self::EVENT_BEFORE_DAL_UPDATE, new DAL());

        try {
            if (self::$isConnected == 0) {
                self::connect();
            }
            if (is_array($aFields))
                $sSQL = "UPDATE " . $sTableName . "
                    SET ";
            foreach ($aFields as $key => $value) {
                $key = "`" . $key . "`";
                if (!is_null($value) && $value!='null') {
                    if (!is_numeric($value)) {
                        //$value = mysql_escape_string($value);
                        $value = "\"" . mysql_escape_string($value) . "\"";
                    }
                    $aSetFields[] = $key . " = " . $value;
                } else {
                    $aSetFields[] = $key . " = null ";
                }
            }
            $sSQL .= implode(",", $aSetFields);
            $sSQL .= " WHERE " . $sWhereQuery;

            if($sDebug) {
                echo 'Running Query: '.self::$iQueryCount.'<br />';
                var_dump($sSQL);
                echo 'Query took: '.(microtime(true) - $startTime).'<br />';
            }

            @mysql_query($sSQL) or self::catchError("Update - Query Failed", $sSQL);

            if(class_exists($sModelName)){
                $oModel=new $sModelName();
                $oModel->afterDALUpdate($aFields, $sWhereQuery);
            }
            Events::trigger(self::EVENT_AFTER_DAL_UPDATE, new DAL());


            if ($bReturn) {
                $bReturnValue = @mysql_affected_rows();
            } else {
                $bReturnValue = true;
            }
        } catch (Exception $e) {
            self::catchError('Exception: ' . $e->getMessage(), $sSQL);
        }

        return $bReturnValue;
    }

    /**
     * Disconnects from the database
     */
    public function disconnect() {
        self::$isConnected = !@mysql_close();
    }

    /**
     * Exception handling for the DAL. This will display the exception details on
     * certain (hardcoded at the moment) machines, or email the error otherwise
     * @todo We should look at creating a more universal exception handler
     * @param string $sErrorMessage The error message to display
     * @param string $sSQL The SQL that was being executed when the exception occurred
     * @return type
     */
    protected function catchError($sErrorMessage = "", $sSQL = "") {
        //throw new LottoException($sErrorMessage);
        if (( substr($_SERVER['SERVER_NAME'], strlen($_SERVER['SERVER_NAME']) - strlen(".com")) != ".com" ) || ( $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ) || $_GET['show_sql_errors']=='1234') {
            echo '<pre>Standard Message: ' . $sErrorMessage . "\r\n";
            echo 'Standard Code: ' . mysql_errno() . "\r\n";
            echo 'DBMS/Host: ' . DBSERVERADDRESS . "\r\n";
            echo 'DBMS/User: ' . DBUSERNAME . "\r\n";
            echo 'DBMS/Name: ' . DATABASENAME . "\r\n";
            echo 'DBMS/Debug Message: ' . mysql_error() . "\r\n";
            echo 'DBMS/SQL Call: ' . $sSQL . "\r\n";
            debug_print_backtrace();
            echo "</pre>";
            exit(1);
        } else {
            $sContent = 'Website: ' . $_SERVER['SERVER_NAME'] . "<br/>";
            $sContent .= 'Website Title: ' . WEBSITETITLE . "<br/>";
            $sContent .= 'Remote Address: ' . (($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']) . "<br/>";
            $sContent .= 'Remote URI: ' . $_SERVER['REQUEST_URI'] . "<br/>";
            $sContent .= 'Referer: ' . $_SERVER['HTTP_REFERER'] . '<br/>';
            $sContent .= 'Error Time: ' . date("l dS \of F Y h:i:s A") . "<br/>";
            $sContent .= 'Standard Message: ' . $sErrorMessage . "<br/>";
            $sContent .= 'Standard Code: ' . mysql_errno() . "<br/>";
            $sContent .= 'DBMS/Host: ' . DBSERVERADDRESS . "<br/>";
            $sContent .= 'DBMS/User: ' . DBUSERNAME . "<br/>";
            $sContent .= 'DBMS/Name: ' . DATABASENAME . "<br/>";
            $sContent .= 'DBMS/Debug Message: <pre>' . print_r(mysql_error(), true) . "</pre><br/>";
            $sContent .= 'DBMS/SQL Call: <pre>' . $sSQL . "</pre>";
            $oEmail = new Mailer();
            $oEmail->setSubject(WEBSITETITLE . " SQL Error");
            $oEmail->shortHeader();
            $oEmail->setFrom("no-reply@lovelotto.com", WEBSITETITLE . " Webmaster");
            $oEmail->addTo('jon@network649.com');
            return $oEmail->sendEmail($sContent);
            ?>
            <script language="javascript">
                document.location.href = '/400.html';
            </script>
            <?php

            exit(1);
        }
    }

    /**
     * Just returns an object containing stats from the DAL
     * @return \stdClass
     */
    static function getStats(){
        $oStats=new stdClass();
        $oStats->iQueryCount=self::$iQueryCount;
        $oStats->iUpdateCount=self::$iUpdateCount;
        $oStats->iInsertCount=self::$iInsertCount;
        return $oStats;
    }

    /**
     * Gets all available information about a table structure and returns
     * as an object containing
     * ->fields = array of fields
     * ->fields->name = name
     * ->fields->type = type
     * ->fields->comment = comment
     * ->fields->required = required (i.e. is null allowed?)
     * ->fields->default = default
     * ->constraints = array of constraints
     * ->constraints->references_table
     * ->constraints->references_field
     * @param type $sTableName
     */
    public static function getTableDetails($sTableName) {
        $sql = 'SHOW CREATE TABLE `' . $sTableName . '`';
        $result = DAL::executeQuery($sql);
        $return = $result[0]['Create Table'];

        //Get the fields
        $aReturnSplit = explode("\n", $return);
        //Ignore first item as it will be the create statement
        foreach ($aReturnSplit as $sLine) {
            $aLine = explode(' ', trim($sLine));
            $oItem = new stdClass();
            switch ($aLine[0]) {
                case 'CREATE':
                    break;
                case 'KEY':
                    $oKey=new stdClass();
                    $oKey->sName = str_replace('`', '', str_replace('`', '', $aLine[1]));
                    $oKey->sFields = str_replace('(', '', str_replace(')', '', substr($aLine[2],0,-1)));
                    $out->aKeys[$oKey->sName]=$oKey;
                    break;
                case 'PRIMARY':
                    $sField = str_replace('(`', '', str_replace('`)', '', substr($aLine[2],0,-1)));
                    $out->aFields[$sField]->bPrimaryKey = true;
                    $out->aPrimaryKey[]=$sField;
                    break;
                case 'CONSTRAINT':
                    $oConstraint = new stdClass();
                    $sConstraintKey = str_replace('`', '', str_replace('`', '', $aLine[1]));
                    $sField = str_replace('(`', '', str_replace('`)', '', $aLine[4]));
                    $oConstraint->sTable = str_replace('`', '', $aLine[6]);
                    $oConstraint->sTableField = str_replace('(`', '', str_replace('`)', '', $aLine[7]));
                    $out->aFields[$sField]->oConstraint = $oConstraint;

                    $oConstraint->sLocalField=$sField;
                    $out->aConstraints[$sConstraintKey]=$oConstraint;
                    break;
                case ')':
                    //All done - break out of the loop (although this should be the last time round anyway)

                    break;
                default:
                    //This will be a field
                    $oItem->sName = str_replace('`', '', $aLine[0]);
                    $aType = explode('(', $aLine[1]);
                    $oItem->sType = $aType[0];
                    $oItem->iLength = intval($aType[1]);
                    $oItem->bRequired = true;
                    $oItem->bPrimaryKey = false;
                    //Rest of the field will depend on what type
                    foreach ($aLine as $iLineItemNum => $sLineItem) {
                        switch ($sLineItem) {
                            case 'DEFAULT':
                                $oItem->sDefault = $aLine[$iLineItemNum + 1];
                                break;
                            case 'COMMENT':
                                $oItem->sComment = implode(' ', array_slice($aLine, $iLineItemNum + 1));
                                break;
                            case 'NOT':
                                if ($aLine[$iLineItemNum + 1] == 'NULL')
                                    $oItem->bRequired = false;
                                break;
                        }
                    }
                    $out->aFields[$oItem->sName] = $oItem;
            }
        }
        return $out;
    }


}
?>
