<?php
/**
 * A very simple class for managing configuration values
 * Was going to do this with XML but just using a php include as it's much faster
 *
 * To use just do Config::$config->sSomeConfigValue='Value';
 *
 * @package LoveLotto
 * @subpackage Configuration
 */
class Config{
    static $config;

    function __construct(){
        self::$config=$this;
    }

    /**
     * Load the config in from the relevent file if it exists
     */
    function loadConfig(){
        require_once($_SERVER['HTTP_HOST'].'.config.php');
    }

}