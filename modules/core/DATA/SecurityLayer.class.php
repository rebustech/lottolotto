<?php

class SecurityLayer {

    static function preventDirectAccess($sFile) {
        $sFile = str_replace('\\', '/', $sFile);
        $sCalledFile = str_replace('\\', '/', $_SERVER['SCRIPT_FILENAME']);
        if ($sFile == $sCalledFile) {
            header('Location: ..');
        }
    }

}
