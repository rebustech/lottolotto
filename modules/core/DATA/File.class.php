<?php

/**
 * Custom file handling.
 * @package LoveLotto
 * @subpackage Data
 */
class File {

    public static function changeFolderName($sPath, $sRootFolder, $sCurrentFolder, $sFolder) {

        $bReturn = false;

        if (rename($sPath . "$sRootFolder/$sCurrentFolder", $sPath . "$sRootFolder/$sFolder")) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function createFolder($sPath, $sRootFolder, $sFolder) {
        $bReturn = false;
        $findme = '/';
        $pos = substr_count($sFolder, $findme);
        if ($pos == 2) {
            $folders = explode("/", $sFolder);
            $typefolder = $sFolder[0];
            $mainfolder = $sFolder[1];
            $subfolder = $sFolder[2];
            if (mkdir($sPath . "$sRootFolder/$typefolder/$mainfolder", 2775)) {
                $bReturn = true;
            }
            $bReturn = false;
            if (mkdir($sPath . "$sRootFolder/$typefolder/$mainfolder/$subfolder", 2775)) {
                $bReturn = true;
            }
        } else {
            if (mkdir($sPath . "$sRootFolder/$sFolder", 2775)) {
                $bReturn = true;
            }
        }

        return $bReturn;
    }

//eof

    public static function uploadFile($sPath, $sFolder, $sFileName, $sContent) {

        $bReturn = false;

        $file = $sPath . "$sRootFolder/$sFolder/$sFileName";
        $hFile = fopen("$file", "w+");
        fwrite($hFile, $sContent);
        fclose($hFile);

        if (file_exists($sPath . "$sRootFolder/$sFolder/$sFileName"))
            $bReturn = true;

        return $bReturn;
    }

//eof

    public static function readFile($sPath, $folder, $filename) {

        $bReturn = false;
        if (file_exists($sPath . "$folder/$filename")) {
            $file = $sPath . "$folder/$filename";
            $handle = fopen("$file", "r");
            $contents = fread($handle, filesize($file));
            fclose($handle);
            $bReturn = $contents;
        }

        return $bReturn;
    }

//eof

    public static function uploadTestFile($folder) {

        $bReturn = false;
        $filename = "test.txt";

        $result = Language::getLanguageFolders();
        foreach ($result as $key => $value) {
            $rootfolder = $value[folder];
            $text = "Test";
            $file = "../$rootfolder/$folder/$filename";
            $handle = fopen("$file", "w+");
            fwrite($handle, $text);
            fclose($handle);
            if (file_exists("../$rootfolder/$folder/$filename"))
                $bReturn = true;
            unlink($file);
            if (file_exists("../$rootfolder/$folder/$filename"))
                $bReturn = false;
        }

        return $bReturn;
    }

//eof

    public static function deleteFolder($sPath, $rootfolder, $folder) {

        $bReturn = false;

        $dirname = $sPath . "$rootfolder/$folder";

        if (is_dir($dirname)) {
            $handle = opendir($dirname);
            while (( $name = readdir($handle)) !== false) {
                if ($name != "." && $name != "..") {
                    unlink("$dirname/$name");
                }
            }
            closedir($handle);
        }
        if (rmdir($sPath . "$rootfolder/$folder")) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function deletePage($sPath, $folder, $page) {

        $bReturn = false;

        if (unlink($sPath . "/$folder/$page")) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function checkFolderExists($sPath, $sRootFolder, $sFolder) {

        $bReturn = false;

        if (is_dir($sPath . "$sRootFolder/$sFolder")) {
            $bReturn = true;
        }
        return $bReturn;
    }

//eof

    public static function checkFileExists($sPath, $sRootFolder, $sFolder, $sFile) {

        $bReturn = false;

        if (is_file($sPath . "$sRootFolder/$sFolder/$sFile")) {
            $bReturn = true;
        }


        return $bReturn;
    }

//eof

    public static function checkIsFolderEmpty($sPath, $rootfolder, $folder) {

        $bReturn = false;
        $dirname = $sPath . "$rootfolder/$folder";

        if (is_dir($dirname)) {
            $bReturn = true;
            $handle = opendir($dirname);
            while (( $name = readdir($handle)) !== false) {
                if ($name != "." && $name != ".." && $name != "index.php" && $name != "cmr2.log") {
                    $bReturn = false;
                    break;
                }
            }
            closedir($handle);
        }
        return $bReturn;
    }

//eof

    public static function uploadImage($rootfolder, $file, $name) {

        $bReturn = false;
        $path = "../$rootfolder/images/icons/$name";

        if (move_uploaded_file($file, $path)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function uploadImageFullPath($file, $path) {

        $bReturn = false;

        if (move_uploaded_file($file, $path)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function deleteMMItem($sRootFolder, $sMMFolder, $sName) {

        $bReturn = false;

        $sPath = MMPATH . "$sRootFolder/$sMMFolder/$sName";

        if (unlink($sPath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function uploadTempImage($sFile, $sName) {


        $bReturn = false;
        $sPath = $_SERVER['DOCUMENT_ROOT'] . "/tempimages/$sName";

        if (move_uploaded_file($sFile, $sPath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function deleteTempImage($sName) {

        $bReturn = false;
        $sPath = $_SERVER['DOCUMENT_ROOT'] . "/tempimages/$sName";

        if (unlink($sPath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function copyFile($sSourceFile, $sDestinationFile) {
        return copy($sSourceFile, $sDestinationFile);
    }

//eof

    public static function copyMMItem($sFile, $sRootFolder, $sMMFolder, $sName) {

        $bReturn = false;
        $sPath = MMPATH . "$sRootFolder/$sMMFolder/$sName";

        if (copy($sFile, $sPath)) {
            $bReturn = true;
        }
        return $bReturn;
    }

//eof

    public static function copyImage($file, $sPath) {

        $bReturn = false;

        if (copy($file, $sPath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function renameFile($sRootFolder, $sFolder, $sFileName, $sOldFileName) {

        $bReturn = false;
        $sOldPath = SYSTEMPATH . "media/$sRootFolder/$sFolder/$sOldFileName";
        $sNewPath = SYSTEMPATH . "media/$sRootFolder/$sFolder/$sFileName";

        if (rename($sOldPath, $sNewPath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof

    public static function renameFolder($sRootFolder, $sFolder, $sNewFolder) {
        $bReturn = false;
        $sOldPath = SYSTEMPATH . "media/$sRootFolder/$sFolder/$sOldFileName";
        $sNewPath = SYSTEMPATH . "media/$sRootFolder/$sFolder/$sFileName";

        if (rename($sOldPath, $sNewPath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

    public static function checkFolderPermissions($path) {
        /* $bReturn = false;
          @chmod($path, 2775);
          if (substr(sprintf('%o', fileperms($path)), -4) == '2775') {
          $bReturn = true;
          }
          return $bReturn; */
        return true;
    }

//eof

    public static function replaceAccentedCharacters($sString) {

        $search = explode(",", "Š,Č,Ž,Ş,ţ,ļ,ī,ā,ē,ş,ă,š,ž,č,ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,´,(,),',ö,ü,é,ı,ì,ğ,ş,á,ñ,š,ò,č,á,ū,ķ,ë,ç,à,ï,ù,î,ž,!,’,Ç,@,®,ē,ņ,ú,ä,ϊ,Î,É,ñ,Ó,Ú,É,Á,À");
        $replace = explode(",", "S,C,Z,S,t,l,i,a,e,s,a,s,z,c,c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,-,-,-,-,o,u,e,i,i,g,s,a,n,s,o,c,a,u,k,e,c,a,i,u,i,z,-,-,C,-,,e,n,u,a,i,I,E,n,O,U,E,A,A");
        return str_replace($search, $replace, $sString);
    }

    public static function checkRootFolderPermissions($path) {

        return true;
    }

//eof

    public static function downloadFileFromURL($sURL, $sFilename) {
        try {
            $ch = curl_init($sURL);
            $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/tempimages/$sFilename", "w+");
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

//eof

    public static function deleteFile($sFileNamePath) {

        $bReturn = false;

        if (unlink($sFileNamePath)) {
            $bReturn = true;
        }

        return $bReturn;
    }

//eof
}

//eoc
?>