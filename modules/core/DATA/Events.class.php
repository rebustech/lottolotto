<?php

/**
 * A really simple implementation of the observer design pattern designed to be able to be
 * retrofitted to any part of the system
 *
 * @pacakge LoveLotto
 * @subpackage Core
 */
class Events {

    static $aSubsribers;

    static function trigger($sEventName, $oObject) {
        if (!is_array(self::$aSubsribers[$sEventName]))
            return true;
        foreach (self::$aSubsribers[$sEventName] as $oHandler) {
            if ($oObject instanceof $oHandler->sClass) {
                $oTargetMethod = $oHandler->sTargetMethod;
                $oHandler->oTargetObject->$oTargetMethod($oObject);
            }
        }
    }

    static function listen($sEventName, $sClass, $oTargetObject, $sTargetMethod) {
        $oListener = new EventListener($sEventName, $sClass, $oTargetObject, $sTargetMethod);
        self::$aSubsribers[$sEventName][] = $oListener;
    }

}
