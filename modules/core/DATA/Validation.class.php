<?php

/**
 * Validation helper
 * Appears to do very little - we should extend this further to make use of PHP Filters
 * @package LoveLotto
 * @subpackage Data
 */
class Validation {

    /**
     * Checks if string is alpanumeric
     *
     * @access       public
     * @version      1.0
     */
    static public function checkAlphanumeric($sString) {
        try {
            $bReturn = false;

            if (ctype_alnum(str_replace("-", "", $sString)))
                $bReturn = true;

            return $bReturn;
        } catch (Exception $e) {
            //TODO: TRACE/LOGGING WITH STATIC LOGGING FUNCTION
            return false; //or standard error mgs. ie: we;re sorry we are experiencing problems right now blah
        }
    }

// eof

    static public function validateHTML($sString, &$aErrors) {
        return true;
        /* libxml_use_internal_errors(true);
          libxml_clear_errors();
          $options = (strpos($sString, '<!DOCTYPE') !== false) ? (LIBXML_DTDLOAD + LIBXML_DTDVALID) : 0;
          simplexml_load_string($sString, 'SimpleXMLElement', $options);
          $errors = libxml_get_errors();
          $aErrors = $errors;
          return (empty($errors) or $errors[0]->level == LIBXML_ERR_WARNING) ? true : false; */
    }

}
