<?php
/**
 * Helpful Cookie Class
 * Part of the original package from LoveLotto
 *
 * @package LoveLotto
 * @subpackage Data
 */
class Cookies
{
    /**
     * Sets a cookie
     * @param string $sName
     * @param string $sData
     * @param int $iHours
     * @return bool
     */
    public function setCookie($sName, $sData, $iHours)
    {
        return @setcookie($sName, $sData, time()+($iHours*60*60), "/", "." . $_SERVER['SERVER_NAME']);
    }

    /**
     * Gets a cookie
     * @param string $sName
     * @return string
     */
    public function getCookie($sName)
    {
        $bData = NULL;
        if ( $_COOKIE[$sName] )
        {
                $bData = $_COOKIE[$sName];
        }
        return $bData;
    }

    /**
     * Removes a cookie
     * @param string $sName
     * @return bool
     */
    public function deleteCookie($sName)
    {
        return @setcookie($sName, "", time()-(60*60*24*30), "/", "." . $_SERVER['SERVER_NAME']);
    }
}
