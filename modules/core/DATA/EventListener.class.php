<?php

/**
 * A really simple implementation of the observer design pattern designed to be able to be
 * retrofitted to any part of the system
 *
 * @pacakge LoveLotto
 * @subpackage Core
 */
class EventListener {

    var $sEventName;
    var $sClass;
    var $oTargetObject;
    var $sTargetMethod;

    function __construct($sEventName, $sClass, $oTargetObject, $sTargetMethod) {
        $this->$sEventName = $sEventName;
        $this->$sClass = $sClass;
        $this->$oTargetObject = $oTargetObject;
        $this->$sTargetMethod = $sTargetMethod;
    }

}
