<?php

/**
 * Custom email class that makes use of the PHPMailer class
 * @package LoveLotto
 * @subpackage Data
 */
require __DIR__ . '/../PHPMailer-master/PHPMailerAutoload.php';

class Mailer extends PHPMailer {

    protected $bHTML = true;
    protected $bMinimal = false;

    public function __construct() {
        $this->IsSMTP(); // telling the class to use SMTP

        $this->Host = 'smtp.server';  // SMTP server
        $this->setFrom('emailaddress', 'name');
        $this->Port = '465';  // SMTP Port
        $this->SMTPAuth = true;  // If your SMTP requires authentication
        //$this->SMTPSecure = "";
        $this->Username = '';
        $this->Password = '';
        /*
          $this->Host = 'in.mailjet.com';  // SMTP server
          $this->Port = '465';  // SMTP Port
          $this->SMTPAuth = true;  // If your SMTP requires authentication
          $this->SMTPSecure = "ssl";
          $this->Username = '';
          $this->Password = ''; */
    }

    public function sendEmail($sContent) {

        if (SEND_MAILS == false)
            return;

        try {
            $sTo = $this->getToAddresses();
            $this->IsHTML($this->bHTML);
            if ($this->bHTML) {
                if ($this->bMinimal) {
                    $this->Body = $this->getShortHeader() . $sContent . $this->getShortFooter();
                    $this->Body = str_replace("[%email%]", $sTo[0][0], $this->Body);
                } else {
                    $this->Body = $this->getHeader() . $sContent . $this->getFooter();
                    $this->Body = str_replace("[%email%]", $sTo[0][0], $this->Body);
                }
            } else {
                $this->Body = $sContent;
                $this->Body = str_replace("[%email%]", $sTo[0][0], $this->Body);
            }

            if (!$this->Send()) {
                //throw new Exception("Could not send the email.");
            }
        } catch (Exception $e) {
            //throw $e;
        }

        return true;
    }

    protected function getShortHeader() {
        return file_get_contents(__DIR__ . '/Templates/header.html');
    }

    protected function getShortFooter() {
        return file_get_contents(__DIR__ . '/Templates/footer.html');
    }

    protected function getHeader() {
        return file_get_contents(__DIR__ . '/Templates/header.html');
    }

    protected function getFooter() {
        return file_get_contents(__DIR__ . '/Templates/footer.html');
    }

    public function plainText() {
        $this->bHTML = false;
    }

    public function shortHeader() {
        $this->bMinimal = true;
    }

    public function addTo($sEmail, $sName = "") {
        $this->AddAddress($sEmail, $sName);
    }

    public function setFrom($sEmail, $sName = "", $auto = true) {
        $this->FromName = $sName;
        $this->From = $sEmail;
    }

    public function setSubject($sSubject) {
        $this->Subject = $sSubject;
    }

}
