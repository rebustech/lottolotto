<?php

/**
 * Custom number handling used to convert numbers to nice strings
 * @package LoveLotto
 * @subpackage Data
 */
class Numbers {

    private static $units = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine');
    private static $teens = array('ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen');
    private static $tens = array(2 => 'twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety');
    private static $suffix = array('thousand', 'million', 'billion', 'trillion', 'quadrillion');

    /**
     * Convert the provided number to a nice string
     * @param int $int number to conver
     * @return string
     * @throws Exception
     */
    private static function ToString($int) {
        // Check for purely numeric chars
        if (!preg_match('#^[\d.]+$#', $int)) {
            throw new Exception('Invalid characters in input');
        }

        // Handle decimals
        if (strpos($int, '.') !== false) {
            $decimal = substr($int, strpos($int, '.') + 1);
            $int = substr($int, 0, strpos($int, '.'));
        }

        // Lose insignificant zeros
        $int = ltrim($int, '0');

        // Check for valid number
        if ($int == '') {
            $int = '0';
        }

        // Lose the negative, don't use abs() so as to allow large numbers
        if ($negative = ($int < 0)) {
            $int = substr($int, 1);
        }

        // Number too big?
        if (strlen($int) > 18) {
            throw new Exception('Out of range');
        }

        // Keep original number
        $orig = $int;

        /**
         * Main number deciphering bit thing
         */
        switch (strlen($int)) {

            // Single digit number
            case '1':
                $text = self::$units[$int];
                break;


            // Two digit number
            case '2':
                if ($int{0} == '1') {
                    $text = self::$teens[$int{1}];
                } else if ($int{1} == '0') {
                    $text = self::$tens[$int{0}];
                } else {
                    $text = self::$tens[$int{0}] . '-' . self::$units[$int{1}];
                }
                break;


            // Three digit number
            case '3':
                if ($int % 100 == 0) {
                    $text = self::$units[$int{0}] . ' hundred';
                } else {
                    $text = self::$units[$int{0}] . ' hundred and ' . self::GetText(substr($int, 1));
                }
                break;


            // Anything else
            default:
                $pieces = array();
                $suffixIndex = 0;

                // Handle the last three digits
                $num = substr($int, -3);
                if ($num > 0) {
                    $pieces[] = self::GetText($num);
                }
                $int = substr($int, 0, -3);

                // Now handle the thousands/millions etc
                while (strlen($int) > 3) {
                    $num = substr($int, -3);

                    if ($num > 0) {
                        $pieces[] = self::GetText($num) . ' ' . self::$suffix[$suffixIndex];
                    }
                    $int = substr($int, 0, -3);
                    $suffixIndex++;
                }

                $pieces[] = self::GetText(substr($int, -3)) . ' ' . self::$suffix[$suffixIndex];

                /**
                 * Figure out whether we need to add "and" in there somewhere
                 */
                $pieces = array_reverse($pieces);

                if (count($pieces) > 1 AND strpos($pieces[count($pieces) - 1], ' and ') === false) {
                    $pieces[] = $pieces[count($pieces) - 1];
                    $pieces[count($pieces) - 2] = 'and';
                }

                // Create the text
                $text = implode(' ', $pieces);

                // Negative number?
                if ($negative) {
                    $text = 'minus ' . $text;
                }
                break;
        }

        /**
         * Handle any decimal part
         */
        if (!empty($decimal)) {
            $pieces = array();
            $decimal = preg_replace('#[^0-9]#', '', $decimal);

            for ($i = 0, $len = strlen($decimal); $i < $len; ++$i) {
                $pieces[] = self::$units[$decimal{$i}];
            }

            $text .= ' point ' . implode(' ', $pieces);
        }


        return $text;
    }

    public function GetText($int) {
        return self::ToString($int);
    }

}
