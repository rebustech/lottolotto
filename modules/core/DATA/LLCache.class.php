<?php

class LLCache {

    static $memcache;
    static $connected = false;

    static function connect() {

        if (class_exists('Memcache')) {
            self::$memcache = new Memcache;
            self::$memcache->addServer('127.0.0.1');
        } elseif (class_exists('Memcached')) {
            self::$memcache = new Memcached();
            self::$memcache->addServer('127.0.0.1', 11211);
        } else {
            //die('Memcache no work');
            self::$memcache = new local_cache();
        }
        self::$connected = true;
    }

    static function flush() {
        if (!self::$connected)
            self::connect();
        self::$memcache->flush();
    }

    static function stats() {
        if (!self::$connected)
            self::connect();
        return self::$memcache->getStats();
    }

    static function get($key) {
        $key = base64_encode($key);
        if (!self::$connected)
            self::connect();
        $out = self::$memcache->get($key);
        return $out;
    }

    static function getObject($key) {
        $key = base64_encode($key);
        if (!self::$connected)
            self::connect();
        $sData = self::$memcache->get($key);
        if ($sData != '') {
            return unserialize($sData);
        } else {
            return null;
        }
    }

    static function add($key, $value, $compress = false, $ttl = 120) {
        $key = base64_encode($key);
        if (!self::$connected)
            self::connect();
        if ($compress)
            $flag = MEMCACHE_COMPRESSED;
        else
            $flag = 0;
        return self::$memcache->set($key, $value, $flag, $ttl);
    }

    static function delete($key) {
        $key = base64_encode($key);
        if (!self::$connected)
            self::connect();
        self::$memcache->delete($key);
    }

    static function addObject($key, $object, $ttl = 0) {
        $key = base64_encode($key);
        if (!self::$connected)
            self::connect();
        self::$memcache->delete($key);
        if (self::$memcache->add($key, serialize($object), 0, $ttl) === true) {
            return true;
        } else {
            return self::$memcache->replace($key, serialize($object), 0, $ttl);
        }
    }

}

class local_cache {

    static $aData = array();

    function get($sKey) {
        return self::$aData[$sKey];
    }

    static function getObject($key) {
        $key = base64_encode($key);

        return unserialize(self::$aData[$sKey]);
    }

    function add($key, $value, $compress = false, $ttl = 120) {
        self::$aData[$key] = $value;
    }

    function set($key, $value, $compress = false, $ttl = 120) {
        self::$aData[$key] = $value;
    }

    function replace($key, $value, $compress = false, $ttl = 120) {
        self::$aData[$key] = $value;
    }

    function addObject($key, $value, $compress = false, $ttl = 120) {
        $key = base64_encode($key);
        self::$aData[$key] = serialize($object);
        return true;
    }

    function delete($key) {
        unset(self::$aData[$key]);
    }

}
