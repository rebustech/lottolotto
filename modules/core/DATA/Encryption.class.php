<?php

/**
 * Custom built encryption class
 * @package LoveLotto
 * @subpackage Data
 */
class Encryption {

    static private $saltkey = ENCRYPTKEY;

    const MCRYPTKEY = 'JS868FA60C0CK436451';

    static function mEncrypt($sText) {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, MCRYPTKEY, $sText, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

//eof

    static function mDecrypt($sText) {

        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, MCRYPTKEY, base64_decode($sText), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

//eof

    static function shuffleText($uW, $iBookingID) {
        $uW = strval($uW);
        $iBookingID = strval($iBookingID);
        $aLength = str_split(sprintf("%02d", strlen($uW)));
        while (strlen($uW) < 16)
            $uW .= 9;
        $iBookingID = substr($iBookingID, -4);
        $aShuffleKey = str_split($iBookingID);
        $uW = strrev($uW);
        if ($aShuffleKey[0] < 5)
            $aShuffleKey[0] *= 2;$str = "AkYbOIpRwq";
        $aReplaceString = str_split($str);
        $aPartToReplace = str_split(substr($uW, 0, $aShuffleKey[0]));
        $oPq = '';
        foreach ($aPartToReplace as $Char)
            $oPq .= $aReplaceString[$Char];$uW = $oPq . substr($uW, $aShuffleKey[0]);
        if ($aShuffleKey[1] > 7)
            $aShuffleKey[1] = 7;if ($aShuffleKey[1] == 0)
            $aShuffleKey[1] = 1;$iFirstPart = substr($uW, 0, -($aShuffleKey[1] + 4));
        $iMiddlePart = substr($uW, -($aShuffleKey[1] + 4), 4);
        $iLastPart = substr($uW, -$aShuffleKey[1]);
        $iMiddlePart = strrev($iMiddlePart);
        $uW = $iMiddlePart . $iLastPart . $iFirstPart;
        $uW = base64_encode($uW);
        $uW = substr($uW, $aShuffleKey[2]) . "zT" . substr($uW, 0, $aShuffleKey[2]);
        $uW = strrev($uW);
        $oPq = '';
        $iCount = 0;
        if ($aShuffleKey[3] > 4)
            $aShuffleKey[3] -= 4;for ($i = 0; $i < 100; $i = $i + 2)
            if (++$iCount <= $aShuffleKey[3])
                $oPq .= strrev(substr($uW, $i, 2));$uW = $oPq . substr($uW, strlen($oPq));
        $uW = strrev($uW);
        $uW = $aLength[0] . $uW . $aLength[1];
        return $uW;
    }

//eof

    static function unshuffleText($iCode, $iBookingID) {

        $iCode = strval($iCode);
        $iBookingID = strval($iBookingID);
        $iBookingID = substr($iBookingID, -4);
        $aCode = str_split($iCode);
        $iLength = (int) ($aCode[0] . $aCode[count($aCode) - 1]);
        $ibC = substr($iCode, 1, strlen($iCode) - 2);
        $aShuffleKey = str_split($iBookingID);
        $ibC = strrev($ibC);
        $iCount = 0;
        if ($aShuffleKey[3] > 4)
            $aShuffleKey[3] -= 4;for ($i = 0; $i < 100; $i = $i + 2)
            if (++$iCount <= $aShuffleKey[3])
                $Sq .= strrev(substr($ibC, $i, 2));$ibC = $Sq . substr($ibC, strlen($Sq));
        $ibC = strrev($ibC);
        $ibC = substr($ibC, -$aShuffleKey[2]) . substr($ibC, 0, strlen($ibC) - $aShuffleKey[2] - 3);
        $ibC = base64_decode($ibC);
        if (strpos($ibC, "�") !== false)
            $ibC = substr($ibC, 0, strpos($ibC, "�"));$ibC = str_split($ibC);
        foreach ($ibC as $key => $value) {
            if (preg_match('![^a-zA-Z0-9/+=]!', $value))
                unset($ibC[$key]);
        }$ibC = implode('', $ibC);
        if ($aShuffleKey[1] > 7)
            $aShuffleKey[1] = 7;if ($aShuffleKey[1] == 0)
            $aShuffleKey[1] = 1;$iFirstPart = substr($ibC, $aShuffleKey[1] + 4);
        $iMiddlePart = substr($ibC, 0, 4);
        $iLastPart = substr($ibC, 4, $aShuffleKey[1]);
        $iMiddlePart = strrev($iMiddlePart);
        $ibC = $iFirstPart . $iMiddlePart . $iLastPart;
        if ($aShuffleKey[0] < 5)
            $aShuffleKey[0] *= 2;$str = "AkYbOIpRwq";
        $aReplaceString = array_flip(str_split($str));
        $qN = str_split(substr($ibC, 0, $aShuffleKey[0]));
        $Sq = '';
        foreach ($qN as $Char)
            $Sq .= $aReplaceString[$Char];$ibC = $Sq . substr($ibC, $aShuffleKey[0]);
        $ibC = strrev($ibC);
        $ibC = substr($ibC, 0, $iLength);
        return $ibC;
    }

//eof

    static function Encrypt($sText) {
        try {

            $encryptedText = self::salt($sText, $pos = 'a', $stype = 'a', $debug = 0);
            return $encryptedText;
        } catch (Exception $e) {
            //TODO: TRACE/LOGGING WITH STATIC LOGGING FUNCTION
            return ""; //or standard error mgs. ie: we;re sorry we are experiencing problems right now blah
        }
    }

//eof

    static function Validate($sOriginal, $sHash) {

        try {
            return self::pepper($sOriginal, $sHash, $debug = 0);
        } catch (Exception $e) {
            //TODO: TRACE/LOGGING WITH STATIC LOGGING FUNCTION
            return ""; //or standard error mgs. ie: we;re sorry we are experiencing problems right now blah
        }
    }

//eof

    private function salt($string, $pos = 'a', $stype = 'a', $debug = 0) {

        $stringA = sha1($string);
        if ($pos == 'a'): $pos = rand(10, 38);
        endif;
        if ((rand(1, 3) == 1) || ($stype == 'b')) {
            $salt = md5(self::$saltkey);
            $stype = 'b';
            $slen = 32;
        } else {
            $salt = sha1(self::$saltkey);
            $stype = 'n';
            $slen = 40;
        }
        $afterstr = substr($stringA, $pos);
        $startbeginning = -(strlen($afterstr));
        $beforestr = substr($stringA, 0, $startbeginning);
        $salted = $beforestr . $salt . $afterstr . $stype . $pos;

        if ($debug == 1) {
            echo '<br>self::$saltkey = ' . self::$saltkey;
            echo '<br>$stringA = ' . $stringA;
            echo '<br>$pos = ' . $pos;
            echo '<br>$salt = ' . $salt . '<br>$stype = ' . $stype . '<br>$slen = ' . $slen;
            echo '<br>$afterstr = ' . $afterstr;
            echo '<br>$startbeginning = ' . $startbeginning;
            echo '<br>$beforestr = ' . $beforestr;
            echo '<br><br>$salted = ' . $salted;
        }

        return $salted;
    }

    private function pepper($str, $dbhash, $debug = 0) { // str = string to be checked against DBHASH
        // Find the original sha1 hash  and check it with the new one
        $hashA = sha1($str); // new hash to be checked

        $pos = substr($dbhash, -2);

        $stype = substr($dbhash, -3, 1); // n or b

        if ($stype == 'n') {
            $slen = 40;
        } else {
            $slen = 32;
        }

        $beforesalt = substr($dbhash, 0, $pos);

        $aftersaltA = substr($dbhash, ($pos + $slen));

        $aftersalt = substr($aftersaltA, 0, -3);

        $saltA = substr($dbhash, $pos, ((-strlen($aftersalt)) - 3));

        if ($stype == 'n') {
            $salt = sha1(self::$saltkey);
        } else {
            $salt = md5(self::$saltkey);
        }

        $unsalted = $beforesalt . $aftersalt;

        if ($debug == 1) {
            echo '<br><br>self::$saltkey = ' . self::$saltkey;
            echo '<br>$str = ' . $str;
            echo '<br>$dbhash = ' . $dbhash;
            echo '<br>$hashA = ' . $hashA;
            echo '<br>$pos = ' . $pos;
            echo '<br>$stype = ' . $stype;
            echo '<br>$slen = ' . $slen;
            echo '<br>$beforesalt = ' . $beforesalt;
            echo '<br>$aftersaltA = ' . $aftersaltA;
            echo '<br>$aftersalt = ' . $aftersalt;
            echo '<br>$saltA = ' . $saltA;
            echo '<br>$salt = ' . $salt;
            echo '<br>$unsalted = ' . $unsalted . '<br>if = ';
        }

        if (($hashA == $unsalted) && ($salt == $saltA)) {
            if ($debug == 1): echo 'true';
            endif;
            return true;
        } else {
            if ($debug == 1): echo 'false';
            endif;
            return false;
        }
    }

    static function encodeText($sText) {

        $sText = base64_encode($sText);
        $sText = strrev($sText);
        $sTextPart1 = substr($sText, 0, 6);
        $sTextPart2 = substr($sText, 6);
        return $sTextPart2 . $sTextPart1;
    }

//eof

    static function decodeText($sCode) {

        $sTextPart1 = substr($sCode, -6);
        $sTextPart2 = substr($sCode, 0, strlen($sCode) - 6);
        $sText = strrev($sTextPart1 . $sTextPart2);
        return base64_decode($sText);
    }

//eof

    /*
     * Code below was originally in the payfrex class,
     * however it has mow been moved out to here in order
     * to allow it to be used in more places
     */

    // Encryption function
    static function mpEncrypt($input, $key) {
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);

        $input = self::pkcs5_pad($input, $size);

        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $data = base64_encode($data);
        return $data;
    }

    // Padding function used in encryption function above
    static function pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    // Decryption function
    static function mpDecrypt($sStr, $sKey) {
        $decrypted = mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128, $sKey, base64_decode($sStr), MCRYPT_MODE_ECB
        );
        $dec_s = strlen($decrypted);
        $padding = ord($decrypted[$dec_s - 1]);
        $decrypted = substr($decrypted, 0, -$padding);
        return $decrypted;
    }

    // Generate random string of characters
    static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}

//eoc
?>