<?php

/**
 * Lottery definition base class (abstract)
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class Lottery extends GameEngineComponent {

    //static $sTableName='lotteries_cmn';
    //static $sKeyField='lottery_id';


    const NATIONALLOTTERY = 1;
    const EUROPEANLOTTERY = 2;
    const USAMEGAMILLIONS = 3;
    const USAPOWERBALL = 4;
    const OZLOTTO = 5;
    const OZPOWERBALL = 6;
    const ELGORDO = 7;
    const SUPERENA = 8;
    const UKTHUNDERBALL = 9;
    const FRENCHLOTTO = 10;
    const GERMANLOTTO = 11;
    const BRMEGASENALOTTO = 12;
    const EUROJACKPOT = 13;
    const GLUCKSSPIRALE = 26;
    const SUPER6 = 27;
    const SPIEL77 = 28;

    public $iLotteryID = NULL;
    public $iLangID = NULL;
    public $iNumberCount = NULL;
    public $iBonusNumbers = NULL;
    public $iBonusMinNumber = NULL;
    public $iBonusMaxNumber = NULL;
    public $sBonusText = NULL;
    public $sBonusTextPlural = NULL;
    public $bBonusFromMainNumbers = false;
    public $iDrawnBonusNumbers = NULL;
    public $iMinNumber = NULL;
    public $iMaxNumber = NULL;
    public $iMaxTickets = NULL; //The number of tickets a user is allowed to play per purchase
    public $iMaxRecurring = NULL; //The number of times a user is allowed to play. I.E. Play this ticket for x weeks
    public $sComment = NULL;
    public $sTitle = NULL;
    public $sDescription = NULL;
    public $aCurrency = NULL;
    public $sLogo = "";
    public $sLogoText = "";
    public $sTemplate = "";
    public $iPlayPageID = "";
    public $iResultsPageID = "";
    public $iFAQCategoryID = NULL;
    public $sLottoColor = "e63645";

    /**
     * Array containing list of days of the week this lottery runs
     * This is deprecated now. use aLottoDayNumbers instead
     * @deprecated
     * @var array
     */
    public $aLottoDays = NULL;

    /**
     * Array containing list of days of the week this lottery runs
     * @var array
     */
    public $aLottoDayNumbers = NULL;
    public $sLottoDrawTime;
    public $dPrice;
    public $iCutOff;
    // This was originally in LLGameEngineSystemPlay.class.php
    // but has been moved here so that we can override more easily
    public $aSystemRules = array(
        '9/12' => 12,
        '10/15' => 15,
        '12/22' => 22,
        '10/30' => 30,
        '11/66' => 66,
        '22/77' => 77,
        '26/130' => 130,
        '12/132' => 132,
        '7' => 7,
        '8' => 28,
        '9' => 84,
        '10' => 210,
        '11' => 462,
        '12' => 924,
        '13' => 1716,
        '14' => 3003,
        '15' => 5005,
        '16' => 8008,
        '17' => 12376,
        '18' => 18564,
        '19' => 27132,
        '20' => 38760
    );
    public $aSystemRulesGroups = array(
        "part" => array(
            '9/12' => '12',
            '10/15' => '15',
            '12/22' => '22',
            '10/30' => '30',
            '11/66' => '66',
            '22/77' => '77',
            '26/130' => '130',
            '12/132' => '132'
        ),
        'full' => array(
            '7' => '7',
            '8' => '28',
            '9' => '84',
            '10' => '210',
            '11' => '462',
            '12' => '924',
            '13' => '1716',
            '14' => '3003',
            '15' => '5005',
            '16' => '8008',
            '17' => '12376',
            '18' => '18564',
            '19' => '27132',
            '20' => '38760'
        )
    );

    public function __construct($iLotteryID = null, $iLangID = null) {
        $this->iLotteryID = $iLotteryID;
        $this->iLangID = $iLangID;
        /**
         * This is needed by the frontend, but breaks the backend as on the backend
         * no lottery id is passed in, so only running if we have details
         */
        if ($iLotteryID !== null)
            $this->assignLotteryDetails();
    }

    /**
     * Adds the game configuration fields common to all lotteries.
     * @return array (of ConfigurationEngineFields)
     */
    function _getGameConfigurationFields() {
        $aGameConfigurationFields = parent::_getGameConfigurationFields();
        $aGameConfigurationFields['iCutoff'] = new ConfigurationNumber('iCutoff', 'Cutoff (Minutes)');
        $aGameConfigurationFields['iTickets'] = new ConfigurationNumber('iTickets', 'Tickets to purchase');
        $aGameConfigurationFields['iNumberCount'] = new ConfigurationSwitch('iNumberCount', 'Number of numbers (syndicates)');
        $aGameConfigurationFields['iDays'] = new ConfigurationString('iDays', 'Days to purchase');
        $aGameConfigurationFields['iWeeks'] = new ConfigurationNumber('iWeeks', 'Weeks to purchase');
        return $aGameConfigurationFields;
    }

    protected function assignLotteryDetails() {

        /**
          ALTER TABLE `lotteries_cmn`
          ADD `numbers_per_line_mobile` SMALLINT(3) NOT NULL DEFAULT '5' ,
          ADD `numbers_per_line_tablet` SMALLINT(3) NOT NULL DEFAULT '5' ;
         */
        $sSQL = "SELECT
					lc.classname,
					lc.comment,
					lc.number_count,
					lc.bonus_numbers,
					lc.min_number,
					lc.max_number,
					lc.max_tickets,
					lc.max_recurring,
					lc.match_numbers,
					lc.drawn_bonus_numbers,
					lc.min_bonus,
					lc.max_bonus,
					lc.colour,
					lc.play_page_id,
					lc.results_page_id,
					lc.faqcategory_id,
					lc.logo_text,
					lc.logo,
					lc.template,
					lc.numbers_per_line,
					lc.numbers_per_line_mobile,
					lc.numbers_per_line_tablet,
					lc.image_lottery_sm_text,
					lc.image_lottery_sm,
					lc.image_lottery_lg,
					lc.image_lottery_lg_text,
					ll.title,
					ll.description,
					ll.bonus as bonus_text,
					ll.bonus_plural as bonus_text_plural,
					c.currency_id as currency_id,
					c.symbol as currencysymbol,
					c.shortname as currencyname,
					c.fullname as currencytitle
				FROM lotteries_cmn lc
				INNER JOIN lotteries_lang ll ON ll.fk_lottery_id = lc.lottery_id
				INNER JOIN currencies c ON c.currency_id = lc.fk_currency_id
				INNER JOIN languages l ON ll.fk_language_id = l.language_id
				WHERE ( l.language_id = {$this->iLangID}
					OR l.default = 1 )
					AND lc.is_active = 1
					AND l.is_active = 1
					AND lc.lottery_id = {$this->iLotteryID}
				ORDER BY l.default DESC
				LIMIT 1";

        $aLotteryDetails = DAL::executeGetRow($sSQL);
        if (!empty($aLotteryDetails)) {
            $this->sClassName = $aLotteryDetails['classname'];
            $this->iNumbersPerLine = $aLotteryDetails['numbers_per_line'];
            $this->iNumbersPerLineMobile = $aLotteryDetails['numbers_per_line_mobile'];
            $this->iNumbersPerLineTablet = $aLotteryDetails['numbers_per_line_tablet'];
            $this->iNumberCount = $aLotteryDetails['number_count'];
            $this->iBonusNumbers = $aLotteryDetails['bonus_numbers'];
            $this->iMinNumber = $aLotteryDetails['min_number'];
            $this->iMaxNumber = $aLotteryDetails['max_number'];
            $this->iBonusMaxNumber = $aLotteryDetails['max_bonus'];
            $this->iBonusMinNumber = $aLotteryDetails['min_bonus'];
            $this->iMaxTickets = $aLotteryDetails['max_tickets'];
            $this->iMaxRecurring = $aLotteryDetails['max_recurring'];
            $this->sComment = $aLotteryDetails['comment'];
            $this->sTitle = $aLotteryDetails['title'];
            $this->sDescription = $aLotteryDetails['description'];
            $this->sBonusText = $aLotteryDetails['bonus_text'];
            $this->sBonusTextPlural = $aLotteryDetails['bonus_text_plural'];
            $this->sTemplate = $aLotteryDetails['template'];
            $this->sLogo = $aLotteryDetails['logo'];
            $this->sLogoText = $aLotteryDetails['logo_text'];
            $this->iPlayPageID = $aLotteryDetails['play_page_id'];
            $this->iResultsPageID = $aLotteryDetails['results_page_id'];
            $this->iMatchNumbers = $aLotteryDetails['match_numbers'];
            $this->iFAQCategoryID = $aLotteryDetails['faqcategory_id'];
            $this->sLottoColor = $aLotteryDetails['colour'];
            $this->bBonusFromMainNumbers = (isset($aLotteryDetails['bonus_from_main_numbers']) ? $aLotteryDetails['bonus_from_main_numbers'] : '');
            $this->iDrawnBonusNumbers = $aLotteryDetails['drawn_bonus_numbers'];
            $this->lotteryImage_small = $aLotteryDetails['image_lottery_sm'];
            $this->lotteryImage_smallText = $aLotteryDetails['image_lottery_sm_text'];
            $this->lotteryImage_large = $aLotteryDetails['image_lottery_lg'];
            $this->lotteryImage_largeText = $aLotteryDetails['image_lottery_lg_text'];
            $this->aCurrency = array
                (
                "currency_id" => $aLotteryDetails['currency_id'],
                "symbol" => $aLotteryDetails['currencysymbol'],
                "shortname" => $aLotteryDetails['currencyname'],
                "fullname" => $aLotteryDetails['currencytitle']
            );
        }
    }

    public static function getEnabledLotteries() {
        $sSQL = "SELECT
					lc.lottery_id,
					lc.classname,
					lc.colour as lottocolor,
					lc.number_count as number_count,
					lc.bonus_numbers as bonus_numbers,
					lc.play_page_id,
					lc.comment
				FROM lotteries_cmn lc
				WHERE lc.is_active = 1
				ORDER BY lc.comment ASC, lc.lottery_id ASC";

        $aResults = DAL::executeQuery($sSQL);
        foreach ($aResults as $key => $aResult) {
            $aResults[$key]['playpage'] = Navigation::getPageURL($aResult['play_page_id']);
        }
        return $aResults;
    }

    public static function getLotteryClassName($iLotteryID, $bIsActive = 1) {
        if ($iLotteryID) {
            $sSQL = "SELECT l.classname
					FROM lotteries_cmn l
					WHERE l.lottery_id = {$iLotteryID}
						";
            if ($bIsActive) {
                $sSQL .= " AND l.is_active = 1 ";
            }

            return DAL::executeGetOne($sSQL);
        } else {
            return NULL;
        }
    }

    public function getAvailableDates() {
        $sSQL = "SELECT	rld.drawdate,
						rld.price,
						rld.jackpot,
                        rld.rollover,
						lc.timezone as timezone,
						TIME_TO_SEC(TIMEDIFF(DATE_SUB(rld.drawdate, INTERVAL (rld.cutoff + (60 * lc.timezone)) MINUTE),NOW())) as cutofftime
				FROM r_lottery_dates rld
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = rld.fk_lottery_id
				WHERE rld.fk_lottery_id = {$this->iLotteryID}
				AND DATE_SUB(rld.drawdate, INTERVAL (rld.cutoff + (60 * lc.timezone)) MINUTE) > NOW()
				ORDER BY rld.drawdate ASC
				LIMIT " . $this->iMaxRecurring;

        return DAL::executeQuery($sSQL);
    }

    public function getDrawDates($dStartDrawDate, $iIterations) {
        if ($this->iLotteryID == '')
            return array();

        $sSQL = "SELECT	DATE_SUB(rld.drawdate, INTERVAL ((60 * (lc.timezone))) MINUTE) AS drawdate,
                                rld.drawdate AS `localtime`,
                                rld.price,
                                rld.jackpot,
                                rld.rollover,
                                TIME_TO_SEC(TIMEDIFF(DATE_SUB(rld.drawdate, INTERVAL (rld.cutoff + (60 * lc.timezone)) MINUTE ),NOW())) AS cutofftime,
                                DATE_SUB(rld.drawdate, INTERVAL (rld.cutoff + (60 * (lc.timezone))) MINUTE) AS cutoff,
                                rld.cutoff AS cto,
                                lc.timezone
                           FROM r_lottery_dates rld
                     INNER JOIN lotteries_cmn lc ON lc.lottery_id = rld.fk_lottery_id
                          WHERE rld.fk_lottery_id = {$this->iLotteryID}
                            AND rld.drawdate >= '{$dStartDrawDate}'
                       ORDER BY rld.drawdate ASC
                          LIMIT {$iIterations}";
        return DAL::executeQuery($sSQL);
    }

    public function getLastResultDate() {
        $sSQL = "SELECT MAX(fk_lotterydate) FROM lottery_draws WHERE numbers IS NOT NULL AND fk_lottery_id={$this->iLotteryID}";
        return DAL::executeGetOne($sSQL);
    }

    public function getID() {
        return $this->iLotteryID;
    }

    public function getMaxTickets($iMaxTickets = null) {
        return $this->iMaxTickets;
    }

    public function getTitle() {
        return $this->sTitle;
    }

    public function getDescription() {
        return $this->sDescription;
    }

    public function getComment() {
        return $this->sComment;
    }

    public function getMaxNumber() {
        return $this->iMaxNumber;
    }

    public function getMinNumber() {
        return $this->iMinNumber;
    }

    public function getMaxBonusNumber() {
        return $this->iBonusMaxNumber;
    }

    public function getMinBonusNumber() {
        return $this->iBonusMinNumber;
    }

    public function getNumbersCount() {
        return $this->iNumberCount;
    }

    public function getDrawnBonusNumbersCount() {
        return $this->iDrawnBonusNumbers;
    }

    public function getBonusNumbers() {
        return $this->iBonusNumbers;
    }

    public function getMaxRecurring() {
        return $this->iMaxRecurring;
    }

    public function getCurrencyDetails() {
        return $this->aCurrency;
    }

    public function getCurrencyID() {
        return $this->aCurrency['currency_id'];
    }

    public function getPlayPage() {
        if (!empty($this->iPlayPageID)) {
            return Navigation::getPageURL($this->iPlayPageID);
        }
    }

    public function getResultsPage() {
        if (!empty($this->iResultsPageID)) {
            return Navigation::getPageURL($this->iResultsPageID);
        }
    }

    public function getLogo() {
        return $this->sLogo;
    }

    public function getLogoText() {
        return $this->sLogoText;
    }

    public function getBonusText() {
        return $this->sBonusText;
    }

    public function getBonusTextPlural() {
        return $this->sBonusTextPlural;
    }

    public function getTemplate() {
        return $this->sTemplate;
    }

    public function getFAQCategoryID() {
        return $this->iFAQCategoryID;
    }

    public function getLottoColor() {
        return $this->sLottoColor;
    }

    public function getLottoDays() {
        return $this->aLottoDays;
    }

    public function generateLuckyDip() {
        $numbers = range($this->getMinNumber(), $this->getMaxNumber());
        shuffle($numbers);
        $numbers = array_slice($numbers, 0, $this->getNumbersCount());
        sort($numbers);
        if ($this->getBonusNumbers()) {
            $bonusnumbers = range($this->getMinBonusNumber(), $this->getMaxBonusNumber());
            shuffle($bonusnumbers);
            $numbers = array_merge($numbers, array_slice($bonusnumbers, 0, $this->getBonusNumbers()));
        }
        return $numbers;
    }

    public function validateNumbers(&$aNumbers) {
        if (count($aNumbers) !== ( $this->getNumbersCount() + $this->getBonusNumbers() )) {
            return false;
        }
        if ($this->getBonusNumbers()) {
            $numbers = array_slice($aNumbers, 0, $this->getNumbersCount());
            $bonus = array_slice($aNumbers, $this->getNumbersCount(), $this->getBonusNumbers());
            $numbers = array_filter($numbers, array($this, 'validateNumber'));
            $bonus = array_filter($bonus, array($this, 'validateBonusNumber'));
            $numbers = array_unique($numbers);
            $bonus = array_unique($bonus);
            if (count($numbers) == $this->getNumbersCount() && count($bonus) == $this->getBonusNumbers()) {
                $aNumbers = array_merge($numbers, $bonus);
                return true;
            }
        } else {
            $aNumbers = array_filter($aNumbers, array($this, 'validateNumber'));
            $aNumbers = array_unique($aNumbers);
            if (count($aNumbers) == $this->getNumbersCount()) {
                return true;
            }
        }
        return false;
    }

    protected function validateNumber($iNumber) {
        if ($iNumber >= $this->getMinNumber() && $iNumber <= $this->getMaxNumber()) {
            return true;
        } else {
            return false;
        }
    }

    protected function validateBonusNumber($iNumber) {
        if ($iNumber >= $this->getMinBonusNumber() && $iNumber <= $this->getMaxBonusNumber()) {
            return true;
        } else {
            return false;
        }
    }

    public function insertLotteryDate($dDrawDate) {

        $sLottoDateTime = date("Y-m-d", $dDrawDate) . " " . $this->sLottoDrawTime;

        $sSQL = "INSERT IGNORE INTO r_lottery_dates
				( fk_lottery_id, drawdate, cutoff, price, jackpot, rollover )
				VALUES ( '{$this->iLotteryID}', '{$sLottoDateTime}', '{$this->iCutOff}', '{$this->dPrice}', 0, 0 )";

        return DAL::executeQuery($sSQL);
    }

    public function matchWinners($aDrawDetails, $aTickets, $iDraw) {
        $aWinners = array();
        $aWinnings = unserialize($aDrawDetails['winnings']);
        $aDrawnNumbers = explode("|", $aDrawDetails['numbers']);
        $aDrawnBonusNumbers = array_slice($aDrawnNumbers, $this->getNumbersCount(), $this->getDrawnBonusNumbersCount());
        $aDrawnNumbers = array_slice($aDrawnNumbers, 0, $this->getNumbersCount());

        if (!empty($aWinnings)) {
            foreach ($aWinnings as $key => $Win) {
                unset($aWinnings[$key]);
                $sMatchTitle = "Jackpot";
                if ($key > 0) {
                    $sMatchTitle = "Match " . $Win['match'];
                    if ($Win['bonus'] > 0) {
                        $sMatchTitle .= ' plus ';
                        if ($Win['bonus'] > 1) {
                            $sMatchTitle .= $Win['bonus'] . ' ' . $this->getBonusTextPlural();
                        } else {
                            $sMatchTitle .= $this->getBonusText();
                        }
                    }
                }
                $aWinnings[$Win['match'] . '|' . $Win['bonus']] = array(
                            'prize' => $Win['prize'],
                            'winners' => 0,
                            'title' => $sMatchTitle
                );
            }
        }

        $aWinningCombinations = $this->getNumberCombinations();
        foreach ($aTickets as $aTicket) {
            $aNumbers = explode("|", $aTicket['numbers']);
            $aBonusNumbers = array_slice($aNumbers, $this->getNumbersCount(), $this->getBonusNumbers());
            $aNumbers = array_slice($aNumbers, 0, $this->getNumbersCount());
            $aMatch = array(count(array_intersect($aDrawnNumbers, $aNumbers)), count(array_intersect($aDrawnBonusNumbers, (($this->getBonusNumbers()) ? $aBonusNumbers : $aNumbers))));
            if ($this->iLotteryID == self::ELGORDO && $aMatch[0] == 1 && $aMatch[1] == 1):
                $aMatch[0] = 0;
            endif;

            $iWinningKey = array_search($aMatch, $aWinningCombinations);
            if ($iWinningKey !== FALSE) {
                $aWinners[] = array(
                    'amount' => $aWinnings[$aWinningCombinations[$iWinningKey][0] . "|" . $aWinningCombinations[$iWinningKey][1]]['prize'],
                    'fk_lottery_id' => $this->iLotteryID,
                    'fk_bookingitem_id' => $aTicket['bookingitem_id'],
                    'draw' => $iDraw,
                    'type' => $aWinnings[$aWinningCombinations[$iWinningKey][0] . "|" . $aWinningCombinations[$iWinningKey][1]]['title']
                );
            }
        }
        unset($aTickets);
        unset($aDrawDetails);
        unset($aDrawnNumbers);
        unset($aDrawnBonusNumbers);
        unset($aWinningCombinations);
        unset($aWinnings);
        return $aWinners;
    }

    public function getWinningAmounts() {
        return NULL;
    }

    // Added by Nathan 18/03/2014
    public function getCutoff() {
        return $this->iCutOff;
    }

    /**
     * Calulates the cost of an order item taking into account user options
     * @param Float $fCurrentPrice existing price on this item
     * @param OrderItem $oOrderItem the order item from the basket that will contain the user options
     * @return Float The modified price
     */
    public function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));

        # Turn into boolean
        $oOrderItem->data['quickPlay'] = ($oOrderItem->data['quickPlay'] == 'true');

        # Quick play we have a number of boards, not array
        if (isset($oOrderItem->data['quickPlay']) && $oOrderItem->data['quickPlay']) {

            # How many boards we need?
            if (is_array($oOrderItem->data['boards'])) {
                $oOrderItem->data['boards'] = $oOrderItem->data['boards'][0];
            }
            $oOrderItem->iTotalLines = $oOrderItem->data['boards'];
            $fCurrentPrice += ($fTicketPrice * $iTotalLines);
        } else {
            $oConfigData = json_decode($this->config);

            //$oOrderItem->iTotalLines=0;
            $oOrderItem->iTotalProtected = 0;
            $oOrderItem->fTicketPrice += round(str_replace(',', '', $oConfigData->Price), 2);
            foreach ($oOrderItem->data['boards'] as $iBoardNum => $oBoard) {
                //$oOrderItem->iTotalLines+=$this->getTicketsPerBoard($oBoard);
                /**
                 * Jackpot only (formerly protect jackpot)
                 */
                /**
                 * Fixed Ticket #334
                 * This was fixed for standard play by taking out the operation to mulitply
                 * each price calculation by the number of lines per board. However this
                 * broke system because it was no longer taking into account the number
                 * of boards. Changed the code to use the number of lines from the board instead
                 */
                if ($oBoard['isProtectedJackpot'] == 'true') {
                    $oOrderItem->iTotalProtected++;
                    $oBoard['fBoardPrice'] = $oOrderItem->fTicketPrice / 2;
                    $fCurrentPrice += ($oOrderItem->fTicketPrice / 2) * $weeks * $days * $oBoard['iLinesOnBoard'];
                } else {
                    $oBoard['fBoardPrice'] = $oOrderItem->fTicketPrice;
                    $fCurrentPrice += ($oOrderItem->fTicketPrice) * $weeks * $days * $oBoard['iLinesOnBoard'];
                }
            }
        }



        //if($oOrderItem->data['protectJackpot']=='true') $oOrderItem->fTicketPrice=$oOrderItem->fTicketPrice/2;
        //$oOrderItem->fTicketTotalPrice=$oOrderItem->fTicketPrice * $oOrderItem->iTotalLines * $weeks * $days;

        return $fCurrentPrice + $oOrderItem->fTicketTotalPrice;
    }

    function getTicketsPerBoard($board) {
        return 1;
    }

    /**
     * Processes a quickdeal order. Will need to look at the configuration
     * of this component to work out what to generate
     * @param type $oOrderItem
     * @param type $iBookingID
     * @param \Lottery $oLottery
     */
    public function processQuickDeal($oOrderItem, $iBookingID) {
        $oConfigData = json_decode($this->config);

        $iDrawsPerWeek = sizeof($this->aLottoDayNumbers);
        $aDrawDates = $this->getDrawDates(date('Y-m-d'), intval($oConfigData->iWeeks) * $iDrawsPerWeek);

        $aDaysToOrder = explode(',', $oConfigData->iDays);

        for ($iBoard = 1; $iBoard <= $oConfigData->iTickets; $iBoard++) {
            //Pick the correct amount of random numbers
            $oRandomNumbers = \LL\RNG::random($this->getNumbersCount(), 1, $this->getMaxNumber(), $this->getBonusNumbersCount(), 1, $this->getMaxBonusNumber());
            if ($this->getMaxBonusNumber() > 0) {
                $aRandomNumbers = array_merge($oRandomNumbers->main['random']['data'], $oRandomNumbers->bonus['random']['data']);
            } else {
                $aRandomNumbers = $oRandomNumbers->main['random']['data'];
            }

            $iDrawNumber = 1; //Counter to limit the number of draws
            foreach ($aDrawDates as $aDrawDate) {
                if ($oOrderItem->data['start_date'] == '') {
                    $oOrderItem->data['start_date'] = date('Y-m-d', strtotime($aDrawDate['drawdate']));
                }
                $oOrderItem->data['end_date'] = date('Y-m-d', strtotime($aDrawDate['drawdate']));


                //Check the day to make sure it's one the user wanted
                $sDay = date('N', strtotime($aDrawDate['drawdate']));
                if (in_array($sDay, $aDaysToOrder)) {
                    $this->bookTicket($aRandomNumbers, $aDrawDate, $iBookingID, $oOrderItem);
                }
                /**
                 * Check to see if we've now booked the right number of draws (iWeeks)
                 * if we have exit this loop
                 */
                $iDrawNumber++;
                if ($iDrawNumber > $oConfigData->iWeeks)
                    break;
            }
        }
    }

    /**
     * Process an order for tickets into booking items
     */
    public function bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem) {
        $aData = array(
            "fk_booking_id" => $iBookingID,
            "fk_order_item_id" => $oOrderItem->iBookingOrderID,
            "fk_lottery_id" => $this->getID(),
            "fk_lotterydate" => $aDrawDate['drawdate'],
            "cost" => $this->getPrice(),
            "numbers" => implode("|", $aNumbers),
            "purchased" => 0
        );

        /**
         * Insert the booking item
         * @todo : should make this OOP rather than DAL
         */
        try {
            DAL::Insert('booking_items', $aData);
        } catch (Exception $e) {
            die($e);
        }
        return $response;
    }

}
