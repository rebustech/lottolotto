<?php

namespace LL\GameEngines;

class Installer {

    use \LL\Installer\TInstaller;

    static function getInstallTasks() {
        $v = self::getModuleVersion('Core game engine components');
        if ($v < 1) {
            $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installGameEngineModule', 'Install game engine module');
            $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installBookingOrderItemTable', 'Install booking order items table');
        }
        if ($v < 1.3) {
            $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installImageFields', 'Add image fields to game engines');
        }
        $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installGameEngineComponents', 'Install core game engine components');
        $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installGameEngineCategories', 'Install game engine categories');
        $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installPartnersTable', 'Install partners table');
        $aJobs[] = new \LL\Installer\Task('\LL\GameEngines\Installer', 'installDefaultCommissionFields', 'Install commission fields in game engines');
        return $aJobs;
    }

    static function installGameEngineModule() {
        self::installModule('Core game engine components', 1.3);
    }

    static function installGameEngineComponents() {
        \DAL::Query("DELETE FROM handlers WHERE type='GameEngineCompoment'");
        \DAL::Query("DELETE FROM handlers WHERE type='GameEngineComponent'");
        \DAL::Query("DELETE FROM handlers WHERE type='GameEngineCore'");

        self::installGameEngineComponent('\\BRMegaSena', 'Mega Sena');
        self::installGameEngineComponent('\\ELGordo', 'El Gordo');
        self::installGameEngineComponent('\\EuroJackpot', 'Euro Jackpot');
        self::installGameEngineComponent('\\EuropeanLottery', 'Euromilions');
        self::installGameEngineComponent('\\FrenchLotto', 'French Lottery');
        self::installGameEngineComponent('\\GermanLotto', 'German Lottery');
        self::installGameEngineComponent('\\OzLotto', 'OZ Lotto');
        self::installGameEngineComponent('\\OzPowerball', 'OZ Powerball');
        self::installGameEngineComponent('\\SuperEna', 'Superena');
        self::installGameEngineComponent('\\UKNationalLottery', 'UK Lotto');
        self::installGameEngineComponent('\\UKThunderball', 'UK Thunderball');
        self::installGameEngineComponent('\\USAMegaMillions', 'USA Mega Millions');
        self::installGameEngineComponent('\\USAPowerball', 'USA Powerball');
        self::installGameEngineComponent('\\BallColourGameComponent', 'Ball Colour');
        self::installGameEngineComponent('\\JackpotModifierGameComponent', 'Jackpot Modifier');
        self::installGameEngineComponent('\\SuperPrizeGameComponent', 'Extra Prize');
        self::installGameEngineComponent('\\LaPrimitiva', 'La Primitiva');
        self::installGameEngineComponent('\\Bonoloto', 'Bonoloto');
        self::installGameEngineComponent('\\ElGordoRaffle', 'El Gordo');
        self::installGameEngineComponent('\\ElNinoRaffle', 'El Niño');
        self::installGameEngineComponent('\\SanIldefonsoRaffle', 'San Ildefonso');
        self::installGameEngineComponent('\\MegaSena', 'Mega Sena');
        self::installGameEngineComponent('\\Oz638', 'Oz 6/38');
        self::installGameEngineComponent('\\GlucksSpirale', 'GlücksSpirale');
        self::installGameEngineComponent('\\Megaplier', 'Megaplier');
        self::installGameEngineComponent('\\Powerplay', 'Powerplay');
        self::installGameEngineComponent('\\Maxraffle', 'Max Raffle');
        self::installGameEngineComponent('\\StandardQuickPlay', 'Standard Quick Play');
        self::installGameEngineComponent('\\SystemQuickPlay', 'System Quick Play');
        self::installGameEngineComponent('\\Pariplay', 'Pariplay Game');

        self::installGameEngineCore('\\LL\\GameEngine\\StandardPlay', 'Classic Play');
        self::installGameEngineCore('\\LL\\GameEngine\\SystemPlay', 'System Play');
        self::installGameEngineCore('\\LL\\GameEngine\\Syndicate', 'Syndicate Play');
        self::installGameEngineCore('\\LL\\GameEngine\\Pariplay', 'Instant Win (Pariplay)');
        self::installGameEngineCore('\\LL\\GameEngine\\QuickDeal', 'Quick Deal');
    }

    static function installGameEngineComponent($sClass, $sName, $dVersion = 1) {
        self::installHandler($sClass, $sName, 'GameEngineComponent', $dVersion);
    }

    static function installGameEngineCore($sClass, $sName, $dVersion = 1) {
        self::installHandler($sClass, $sName, 'GameEngineCore', $dVersion);
    }

    static function installBookingOrderItemTable($oTask) {
        $oTable = new \LL\Installer\Table('booking_order_items');
        $oTable->addIdField();
        $oTable->addField(new \LL\Installer\Field('fk_booking_id', 'INT', 11));
        $oTable->addField(new \LL\Installer\Field('raw_data', 'blob'));
        $oTable->addField(new \LL\Installer\Field('date_processed', 'DATETIME'));

        /**
         *
         */
        $oTable->addField(new \LL\Installer\Field('start_date', 'DATE'));
        $oTable->addField(new \LL\Installer\Field('end_date', 'DATE'));

        $oTable->addConstraint(new \LL\Installer\Constraint('fk_booking_id', 'bookings', 'id'));


        //$oTable->compile();
        //Need to add url, image, category,chances,classname
        \DAL::Query('ALTER TABLE game_engines ADD COLUMN `most_played` TINYINT(1);');
        \DAL::Query('ALTER TABLE game_engines ADD COLUMN `url` VARCHAR(255);');
        \DAL::Query('ALTER TABLE game_engines ADD COLUMN `image` VARCHAR(255);');
        \DAL::Query('ALTER TABLE game_engines ADD COLUMN `fk_category_id` INT(11);');
        \DAL::Query('ALTER TABLE game_engines ADD COLUMN `chances` VARCHAR(255);');
        \DAL::Query('ALTER TABLE game_engines ADD COLUMN `classname` VARCHAR(255);');


        $oTable = new \LL\Installer\Table('booking_items');
        $oTable->addField(new \LL\Installer\Field('fk_order_item_id', 'INT', 11));
        $oTable->addConstraint(new \LL\Installer\Constraint('fk_order_item_id', 'booking_order_items', 'id'));
        $oTable->compile();
    }

    static function installGameEngineCategories($oTask) {

        # Insert the categories
        \DAL::Insert('game_engine_categories', array('id' => 1, 'category_name' => 'Classic'));
        \DAL::Insert('game_engine_categories', array('id' => 2, 'category_name' => 'System'));
        \DAL::Insert('game_engine_categories', array('id' => 3, 'category_name' => 'Group'));
        \DAL::Insert('game_engine_categories', array('id' => 4, 'category_name' => 'Combo'));
        \DAL::Insert('game_engine_categories', array('id' => 5, 'category_name' => 'Quick Pick'));
        \DAL::Insert('game_engine_categories', array('id' => 6, 'category_name' => 'Instant Wins'));

        $iAdminSectionId = \DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='Products'");

        //Get the ID of the admin section
        if (\DAL::Query("SELECT * FROM admin_pages WHERE title='Categories' AND parent_id = '{$adminSectionId}'") == 0) {
            \DAL::Insert('admin_pages', array('title' => 'Categories', 'filename' => 'generic-listing.php', 'tablename' => 'game_engine_categories', 'parent_id' => $iAdminSectionId, 'icon' => 'fa-files-o'));
        }
    }

    static function installImageFields() {
        $oTable = new \LL\Installer\Table('game_engines');
        $oTable->addField(new \LL\Installer\Field('image_envelope', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_lobby', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_results', 'varchar', 255))->drop();
        $oTable->addField(new \LL\Installer\Field('image_widget', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_envelope_mobile', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_icon_sm', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_icon_sm_text', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_icon_lg', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_icon_lg_text', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('envelope_products', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('syndicate_products', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('combo_products', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('fk_faq_multi_category', 'varchar', 255));
        $oTable->compile();

        $oTable = new \LL\Installer\Table('lotteries_cmn');
        $oTable->addField(new \LL\Installer\Field('image_results', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_lottery_sm', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_lottery_sm_text', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_lottery_lg', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('image_lottery_lg_text', 'varchar', 255));
        $oTable->addField(new \LL\Installer\Field('enable_jackpot_only', 'tinyint', 1));
        $oTable->compile();
    }

    /**
     * Lookup table to control which countries apply
     */
    static function installPartnersTable() {
        $oMCWhiteLabels = new \LL\Installer\Table('game_engine_affiliate_groups');
        $oMCWhiteLabels->addField(new \LL\Installer\Field('fk_game_engine_id', 'int', 11));
        $oMCWhiteLabels->addField(new \LL\Installer\Field('fk_affiliate_group_id', 'int', 11));
        $oMCWhiteLabels->addConstraint(new \LL\Installer\Constraint('fk_game_engine_id', 'game_engines', 'id'));
        $oMCWhiteLabels->addConstraint(new \LL\Installer\Constraint('fk_affiliate_group_id', 'affiliate_groups', 'id'));
        $oMCWhiteLabels->compile();
    }

    /**
     * Install the default commission field for each row
     */
    static function installDefaultCommissionFields() {
        $oTable = new \LL\Installer\Table('game_engines');
        $oTable->addField(new \LL\Installer\Field('default_commission', 'double'));
        $oTable->compile();
    }

}
