<?php

/**
 * Defines all the methods required to make a game engine compoment. In practice
 * you should create a game engine compoment by extending Abstract class GameEngineComponent
 * as this also provides default functionality
 *
 * Purpose: This provides a reference point to ensure that
 *
 * @package LoveLotto
 * @subpackage GameEngines
 * @author J.Patchett
 *
 */
interface IGameEngineComponent {

    /**
     * The game engine system is heirachical. Get parent compoment will get the parent
     * game engine component or return null if we're at the top.
     * getParentComponent should first check llcache to see if the parent object
     * is cached. If not, get from the database and then store in llcache.
     * @return GameEngineComponent
     */
    public function getParentComponent();

    public function getChildComponents();

    /**
     * Performs any modifications to the price of this game
     */
    public function getPrice($fCurrentPrice = 0);

    /**
     * Calculates the odds of winning on this component, this is combined with the odds
     * of winning on all other components to determine the overall chance of winning
     * These are the odds displayed to a customer
     */
    public function calculateOdds($fCurrentOdds);

    public function getAvailableDates();

    public function getMaxTickets($iMaxTickets = null);

    public function getCartItemName($sCartName = '');

    /**
     * Calculates the odds of winning for insurance purposes. This is different from
     * calculateOdds because not all prize tiers are insured and so the odds being
     * used for calculating are not always the same (for example second chance increases
     * the odds of winning, but doesn't always affect insurance because it may only
     * be available on lower prize tiers)
     * @param float $fCurrentOdds
     * @return float
     */
    public function calculateOddsOfWinning($fCurrentOdds);

    /**
     * Calculates the maxmium possible prize for someone winning on this game
     */
    public function calculatePrize($fCurrentPrize);

    public function beforeOrderCancel($oOrder);

    public function afterOrderCancel($oOrder);

    /**
     * Called when a booking item has a winning result. Needs to assign winnings
     * to players account
     * @param type $oBookingItem
     */
    public function afterWin($oBookingItem, $oResult);

    /**
     * Called when a booking item has not had any winnings
     * @param type $oBookingItem
     * @param type $oResult
     */
    public function afterLoose($oBookingItem, $oResult);

    public function afterPayout($oBookingItem, $oTransaction);

    /**
     * Remove the booking item, if it's been drawn assign to the house along with
     * any winnings associated with that transaction
     * @param type $oBookingItem
     * @param type $oTransaction
     */
    public function afterChargeBack($oBookingItem, $oTransaction);

    /**
     * Returns an array of configuration fields used in the backend
     * @return ConfigurationEngine
     */
    function getBackendConfigurationFields();

    function _getBackendConfigurationFields();

    /**
     * Returns an array of configuration fields used in the backend when configuring
     * a game (i.e. a game made up of individual components)
     * @return ConfigurationEngine
     */
    function getGameConfigurationFields();

    function _getGameConfigurationFields();

    function populateGameConfigurationFields();

    /**
     * Returns an array of configuration fields used in the backend on an individual instance
     * of a game
     * @return ConfigurationEngine
     */
    function getInstanceConfigurationFields();

    function _getInstanceConfigurationFields();

    function getDump();

    function getDumpOfChildren($aChildComponents);

    function save($aData = array());
}
