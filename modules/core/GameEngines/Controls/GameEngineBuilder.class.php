<?php

/**
 * The mighty Game Engine Builder control
 *
 * @package LoveLotto
 * @subpackage Controls
 */
class GameEngineBuilder extends Control{
    var $sView='game_engine_builder';
    var $oGameEngine;

    /**
     * Add the game builder javascript in just before the close body tag
     * @param GameEngine $oGameEngine Specify the game engine that is being used
     */
    function __construct(GameEngine $oGameEngine) {
        $this->oGameEngine=$oGameEngine;
        //Get the JSON configuration for the game engine in it's current state and add just before the /body
        LLResponse::$sPostBody.=LLResponse::addInlineScript($this->getJSON());
        //Add the game builder javascript just before /body (and after the above JSON output)
        LLResponse::$sPostBody.=LLResponse::addScript('/administration/API/LLGameEnginesAssets/gameBuilderScripts');
    }

    function getJSON(){
        $aAllGameEngines=\LL\Handler::getHandlersOfType('GameEngineComponent');
        foreach($aAllGameEngines as $aGameEngine){
            $sGameEngineHandler=$aGameEngine['class'];
            $oGameEngine=new $sGameEngineHandler;

            $aGameEngine['aConfigFields']=$oGameEngine->getGameConfigurationFields();
            //Render a blank form for use on the frontend;
            $sGameForm='';
            foreach($aGameEngine['aConfigFields'] as $oConfigField){
                $sGameForm.=$oConfigField->output();
            }
            $aGameEngine['sConfigFormTemplate']=$sGameForm;
            $aGameEngines[$sGameEngineHandler]=$aGameEngine;
        }


//        echo json_encode($aGameEngines);

        $out='var gameEngineBuilderData='.json_encode($this->oGameEngine->getDump()).";\n";
        $out.='var gameEnginePalette='.json_encode($aGameEngines);
        return $out;
    }

    function Output() {
        $oView=new LLView();
        $oView->oGameEngine=$this->oGameEngine;
        return $oView->output($this->sView);
    }
}
