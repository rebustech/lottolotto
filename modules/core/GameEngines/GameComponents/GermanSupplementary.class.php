<?php

/*
 * GermanSupplementary.
 *
 * This class sits between the original Lottery class and the Super6/Spiel77 classes
 * in order to provide functionality which is common to both of them
 * (namely the matchWinners function)
 *
 * All other functionality common to each seperate lottery remains in each class.
 */

class GermanSupplementary extends Lottery {

    /**
     * Overrides original matchWinners function in Lottery class
     *
     * @param type $aDrawDetails
     * @param type $aTickets
     * @param type $iDraw
     * @return type
     */
    public function matchWinners($aDrawDetails, $aTickets, $iDraw) {
        $aWinners = array();
        $aWinnings = unserialize($aDrawDetails['winnings']);


        // Numbers for these types of lotteries are just one concatenated string
        $iDrawnNumber = $aDrawDetails['numbers'];


        if (!empty($aWinnings)) {
            foreach ($aWinnings as $key => $Win) {
                unset($aWinnings[$key]);
                $sMatchTitle = "Jackpot";
                if ($key > 0) {
                    $sMatchTitle = "Match " . $Win['match'];
                }
                $aWinnings[$Win['match'] . '|' . $Win['bonus']] = array(
                    'prize' => $Win['prize'],
                    'winners' => 0,
                    'title' => $sMatchTitle
                );
            }
        }

        $aWinningCombinations = $this->getNumberCombinations();
        foreach ($aTickets as $aTicket) {

            $iTicketNumber = $aTicket['numbers'];

            // Compare the two numbers
            $aMatch = array($this->_compareNumbers($iDrawnNumber, $iTicketNumber), 0);

            $iWinningKey = array_search($aMatch, $aWinningCombinations);
            if ($iWinningKey !== FALSE) {
                $aWinners[] = array(
                    'amount' => $aWinnings[$aWinningCombinations[$iWinningKey][0] . "|" . $aWinningCombinations[$iWinningKey][1]]['prize'],
                    'fk_lottery_id' => $this->iLotteryID,
                    'fk_bookingitem_id' => $aTicket['bookingitem_id'],
                    'draw' => $iDraw,
                    'type' => $aWinnings[$aWinningCombinations[$iWinningKey][0] . "|" . $aWinningCombinations[$iWinningKey][1]]['title']
                );
            }
        }

        // Garbage collection
        unset($aTickets);
        unset($aDrawDetails);
        unset($iDrawnNumber);
        unset($iTicketNumber);
        unset($aWinningCombinations);
        unset($aWinnings);

        return $aWinners;
    }

    /**
     * Decremental comparison of 2 strings
     *
     * @param integer $iDrawnNumber number drawn in this lottery
     * @param integer $iTicketNumber number on the ticket
     * @return integer
     */
    private function _compareNumbers($iDrawnNumber, $iTicketNumber) {

        // Convert numbers to strings
        $sDrawnNumber = (string) $iDrawnNumber;
        $sTicketNumber = (string) $iTicketNumber;

        // Set match variable here to be false
        // as do/while loop will exit as soon as it's set to true
        $bMatch = false;

        // Record last match position
        $iMatchPos = 0;

        // Position counter, starting at max length
        $i = $this->getNumbersCount();

        // On;y perform the next bit of code if match is true
        // and we're not at the start of the string
        do {
            // Start position for comparison is the negative value of i
            $iStartPos = -1 * abs($i);

            // Get substrings up to current position
            $sDrawSub = substr($sDrawnNumber, $iStartPos);
            $sTicketSub = substr($sTicketNumber, $iStartPos);

            // Compare the two strings
            if (strcmp($sDrawSub, $sTicketSub) === 0) {
                // Stringsmatch, so set flag accordingly
                $bMatch = true;

                // Record current position as the match position
                $iMatchPos = -1 * abs($i);
                ;
            }

            // Decrease substring counter
            $i--;
        } while ($bMatch == false && $i >= 1);


        // Garbage collection
        unset($sDrawnNumber);
        unset($sTicketNumber);
        unset($sDrawSub);
        unset($sTicketSub);
        unset($bMatch);
        unset($iStartPos);

        // Return position at where match was found
        return $iMatchPos;
    }

    /**
     *
     */
    public function bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem) {

        $aData = array(
            "fk_booking_id" => $iBookingID,
            "fk_lottery_id" => $this->getID(),
            "fk_order_item_id" => $oOrderItem->iBookingOrderID,
            "fk_lotterydate" => $aDrawDate['drawdate'],
            "cost" => $this->getPrice(0, $oOrderItem),
            "numbers" => implode("", $oOrderItem->data['extras']),
            "purchased" => 0
        );

        /**
         * Insert the booking item
         * @todo : should make this OOP rather than DAL
         */
        try {
            DAL::Insert('booking_items', $aData);
        } catch (Exception $e) {
            
        }
        return $response;
    }

}
