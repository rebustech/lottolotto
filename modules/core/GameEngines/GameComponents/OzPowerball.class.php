<?php

/**
 * Lottery definition class for the OZ Powerball
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class OzPowerball extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::OZPOWERBALL, $iLangID);

        $this->iCutOff = 240;
        $this->dPrice = 1.5;
        $this->aLottoDays = array("Thursday");
        $this->aLottoDayNumbers = array(4);
        $this->sLottoDrawTime = "20:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(6, 1);
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(2, 1);

        return $aMatchTypes;
    }

}
