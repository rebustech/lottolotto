<?php

/**
 * Lottery definition class for USA Mega Millions
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class USAMegaMillions extends Lottery {

    var $bCanHaveSystem = true;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::USAMEGAMILLIONS, $iLangID);

        $this->iCutOff = 240;
        $this->dPrice = 2.5;
        $this->aLottoDays = array("Tuesday", "Friday");
        $this->aLottoDayNumbers = array(2, 5);
        $this->sLottoDrawTime = "23:00";
        $this->fk_game_engine_id = 7;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 1);
        $aMatchTypes[] = array(1, 1);
        $aMatchTypes[] = array(0, 1);

        return $aMatchTypes;
    }

    public function getWinningAmounts() {
        $aAmounts = array();
        $aAmounts[] = 12000000;
        $aAmounts[] = 1000000;
        $aAmounts[] = 5000;
        $aAmounts[] = 500;
        $aAmounts[] = 50;
        $aAmounts[] = 5;
        $aAmounts[] = 5;
        $aAmounts[] = 2;
        $aAmounts[] = 1;

        return $aAmounts;
    }

}
