<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Super66 extends GermanSupplementary {

    var $iNumberCount = 6;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::SUPER6, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 1;
        $this->aLottoDays = array("Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(3, 6);
        $this->sLottoDrawTime = "18:50";

        //$this->fk_game_engine_id = 3;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 0);
        $aMatchTypes[] = array(1, 0);

        return $aMatchTypes;
    }

    public function getWinningAmounts() {
        $aAmounts = array();

        $aAmounts[] = 100000;
        $aAmounts[] = 6666;
        $aAmounts[] = 666;
        $aAmounts[] = 66;
        $aAmounts[] = 6;
        $aAmounts[] = 2.50;

        return $aAmounts;
    }

    /**
     * Book the tickets
     *
     * Uses the function in the parent class, but with a modifed
     * array of numbers
     *
     * @param array $aNumbers numbers to book
     * @param array $aDrawDate draw date
     * @param integer $iBookingID
     *
     * @return type
     */
    public function bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem) {

        // Super 6 only needs the last 6 numbers
        $aNumbers = array_slice($oOrderItem->data['extras'], 1);

        // Call parent
        $aData = array(
            "fk_booking_id" => $iBookingID,
            "fk_lottery_id" => $this->getID(),
            "fk_order_item_id" => $oOrderItem->iBookingOrderID,
            "fk_lotterydate" => $aDrawDate['drawdate'],
            "cost" => $this->getPrice(0, $oOrderItem),
            "numbers" => implode("", $aNumbers),
            "purchased" => 0
        );

        /**
         * Insert the booking item
         * @todo : should make this OOP rather than DAL
         */
        try {
            DAL::Insert('booking_items', $aData);
        } catch (Exception $e) {
            
        }
        return $response;
    }

    function getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));
        foreach ($oOrderItem->data['games'] as $game) {
            $fCurrentPrice+=1.25 * $weeks * $days;
            $oOrderItem->fExtrasPrice+=1.25 * $weeks * $days;
            $oOrderItem->aProductLists[] = 'Super6';
        }
        return $fCurrentPrice;
    }

}
