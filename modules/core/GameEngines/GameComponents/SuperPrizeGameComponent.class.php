<?php

/**
 * Allows a game designer to modify the jackpot
 */
class SuperPrizeGameComponent extends Lottery {

    var $sFrontendView = 'pods/games/euromillions';

    function _getGameConfigurationFields() {
        $aGameConfigurationFields = parent::_getGameConfigurationFields();
        $aGameConfigurationFields['Prize'] = new ConfigurationString('Prize', 'Prize');
        //$aGameConfigurationFields['GameKey']=new ConfigurationString('GameKey','GameKey');
        $aGameConfigurationFields['Price'] = new ConfigurationString('Price', 'Price');
        $aGameConfigurationFields['CSSClass'] = new ConfigurationString('CSSClass', 'CSSClass');
        return $aGameConfigurationFields;
    }

    function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $oConfigData = json_decode($this->config);
        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));
        foreach ($oOrderItem->data['games'] as $game) {
            if ($game == 'extra-' . $this->id) {
                $fCurrentPrice+=$oConfigData->Price * $weeks * $days;
                $oOrderItem->fExtrasPrice+=$oConfigData->Price * $weeks * $days;
                $oOrderItem->aProductLists[] = $oConfigData->Prize;
            }
        }
        return $fCurrentPrice;
    }

}
