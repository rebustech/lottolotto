<?php

/**
 * Megaplier game component
 */
class Pariplay extends Lottery {

    var $sFrontendView = 'pods/games/pariplay';

    function _getGameConfigurationFields() {
        $aGameConfigurationFields = parent::_getGameConfigurationFields();
        $aGameConfigurationFields['sGameCode'] = new ConfigurationNumber('sGameCode', 'Game Code');
        return $aGameConfigurationFields;
    }

}
