<?php
/**
 * Megaplier game component
 */
class Megaplier extends Lottery{
    var $sFrontendView='pods/games/megaplier';
    var $iLotteryID=Lottery::USAMEGAMILLIONS;

    function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $oConfigData=json_decode($this->config);

        $weeks=$oOrderItem->data['playDuration'];
        $days=(sizeof($oOrderItem->data['day']));
        foreach($oOrderItem->data['games'] as $game){
            if($game=='megaplier'){
                $fCurrentPrice+=$oConfigData->Price*$weeks*$days*$oOrderItem->iTotalLines;
                $oOrderItem->fExtrasPrice+=$oConfigData->Price*$weeks*$days*$oOrderItem->iTotalLines;
                $oOrderItem->aProductLists[]='Megaplier';
            }
        }
        return $fCurrentPrice;
    }
}