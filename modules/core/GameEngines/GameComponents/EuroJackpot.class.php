<?php

/**
 * Lottery definition class for EuroJackpot
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class EuroJackpot extends Lottery {

    var $bCanHaveSystem = true;
    // Overriding array in Lottery (which was moced from original system play game engine)
    public $aSystemRules = array(
        '9/12' => 12,
        '10/15' => 15,
        '12/22' => 22,
        '10/30' => 30,
        '11/66' => 66,
        '22/77' => 77,
        '26/130' => 130,
        '12/132' => 132,
        '6' => 6,
        '7' => 21,
        '8' => 56,
        '9' => 126,
        '10' => 252,
        '11' => 462,
        '12' => 792,
        '13' => 1287,
        '14' => 2002,
        '15' => 3003,
        '16' => 4368,
        '17' => 6188,
        '18' => 8568,
        '19' => 11628,
        '20' => 15504
    );
    public $aSystemRulesGroups = array(
        "part" => array(
            '9/12' => '12',
            '10/15' => '15',
            '12/22' => '22',
            '10/30' => '30',
            '11/66' => '66',
            '22/77' => '77',
            '26/130' => '130',
            '12/132' => '132'
        ),
        'full' => array(
            '6' => 6,
            '7' => 21,
            '8' => 56,
            '9' => 126,
            '10' => 252,
            '11' => 462,
            '12' => 792,
            '13' => 1287,
            '14' => 2002,
            '15' => 3003,
            '16' => 4368,
            '17' => 6188,
            '18' => 8568,
            '19' => 11628,
            '20' => 15504
        )
    );

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::EUROJACKPOT, $iLangID);

        $this->iCutOff = 120;
        $this->dPrice = 2.5;
        $this->aLottoDays = array("Friday");
        $this->aLottoDayNumbers = array(5);
        $this->sLottoDrawTime = "20:30";
        $this->fk_game_engine_id = 5;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 2);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 2);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 2);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(2, 2);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(1, 2);
        $aMatchTypes[] = array(2, 1);

        return $aMatchTypes;
    }

}

?>