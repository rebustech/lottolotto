<?php

/**
 * Lottery definition class for SuperEna
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class SuperEna extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::SUPERENA, $iLangID);

        $this->iCutOff = 180;
        $this->dPrice = 2.5;
        $this->aLottoDays = array("Tuesday", "Thursday", "Saturday");
        $this->aLottoDayNumbers = array(2, 4, 6);
        $this->sLottoDrawTime = "19:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 0);

        return $aMatchTypes;
    }

}
