<?php

/**
 * Lottery definition class for OZLotto
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class OzLotto extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::OZLOTTO, $iLangID);

        $this->iCutOff = 240;
        $this->dPrice = 2;
        $this->aLottoDays = array("Tuesday");
        $this->aLottoDayNumbers = array(2);
        $this->sLottoDrawTime = "20:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(7, 0);
        $aMatchTypes[] = array(6, 1);
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 1);
        return $aMatchTypes;
    }

}
