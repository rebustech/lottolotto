<?php

/**
 * Lottery defination class for ElGordo
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class ElGordo extends Lottery {

    //protected $iMatchNumbers = 1;
    //protected $iBonusNumbers = 1;
    //protected $iBonusMinNumber = 0;
    //protected $iBonusMaxNumber = 9;

    public function __construct($iLangID=1) {
        parent::__construct(Lottery::ELGORDO, $iLangID);

        $this->iCutOff = 240;
        $this->dPrice = 3;
        $this->aLottoDays = array("Sunday");
        $this->aLottoDayNumbers = array(1);
        $this->sLottoDrawTime = "13:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 1);
        $aMatchTypes[] = array(2, 0);
        $aMatchTypes[] = array(0, 1);

        return $aMatchTypes;
    }

}