<?php

/**
 * Lottery definition class for USA Powerball
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class USAPowerball extends Lottery {

    var $bCanHaveSystem = true;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::USAPOWERBALL, $iLangID);

        $this->iCutOff = 240;
        $this->dPrice = 2.75;
        $this->aLottoDays = array("Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(3, 6);
        $this->sLottoDrawTime = "23:00";
        $this->fk_game_engine_id = 6;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 1);
        $aMatchTypes[] = array(1, 1);
        $aMatchTypes[] = array(0, 1);

        return $aMatchTypes;
    }

    public function getWinningAmounts() {
        $aAmounts = array();
        $aAmounts[] = 40000000;
        $aAmounts[] = 1000000;
        $aAmounts[] = 10000;
        $aAmounts[] = 100;
        $aAmounts[] = 100;
        $aAmounts[] = 7;
        $aAmounts[] = 7;
        $aAmounts[] = 4;
        $aAmounts[] = 4;

        return $aAmounts;
    }

}
