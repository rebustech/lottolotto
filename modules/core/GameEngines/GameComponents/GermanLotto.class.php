<?php

/**
 * Lottery definition class for German Lotto Aus 6/49
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class GermanLotto extends Lottery {

    var $bCanHaveSystem = true;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::GERMANLOTTO, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 1;
        $this->aLottoDays = array("Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(3, 6);
        $this->sLottoDrawTime = "18:50";
        $this->fk_game_engine_id = 3;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(6, 1);
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 1);

        return $aMatchTypes;
    }

}
