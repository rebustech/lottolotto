<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class GlucksSpirale extends Lottery {

    var $sFrontendView = 'pods/games/glucks';
    var $iNumberCount = 9;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::GLUCKSSPIRALE, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 1;
        $this->aLottoDays = array("Saturday");
        $this->aLottoDayNumbers = array(6);
        $this->sLottoDrawTime = "18:50";
    }

    function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));
        foreach ($oOrderItem->data['games'] as $game) {
            if ($game == 'super6') {
                $fCurrentPrice+=1.25 * $weeks * $days;
                $oOrderItem->fExtrasPrice+=1.25 * $weeks * $days;
                $oOrderItem->aProductLists[] = 'Super6';
            }
            if ($game == 'spiel77') {
                $fCurrentPrice+=2.50 * $weeks * $days;
                $oOrderItem->fExtrasPrice+=2.50 * $weeks * $days;
                $oOrderItem->aProductLists[] = 'Spiel77';
            }
            if ($game == 'glucks') {
                $fCurrentPrice+=5 * $weeks;
                $oOrderItem->fExtrasPrice+=5 * $weeks;
                $oOrderItem->aProductLists[] = 'Glückspirale';
            }
        }
        return $fCurrentPrice;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(7, 0);
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 0);
        $aMatchTypes[] = array(1, 0);

        return $aMatchTypes;
    }

    /**
     * Overrides original matchWinners function in Lottery class
     *
     * @param type $aDrawDetails
     * @param type $aTickets
     * @param type $iDraw
     * @return type
     */
    public function matchWinners($aDrawDetails, $aTickets, $iDraw) {
        $aWinners = array();
        $aWinnings = unserialize($aDrawDetails['winnings']);
        $aDrawnNumbers = explode("|", $aDrawDetails['numbers']);


        if (!empty($aWinnings)) {
            foreach ($aWinnings as $key => $Win) {
                unset($aWinnings[$key]);
                $sMatchTitle = "Jackpot";
                if ($key > 0) {
                    $sMatchTitle = "Match " . $Win['match'];
                }
                $aWinnings[$Win['match'] . '|' . $Win['bonus']] = array(
                            'prize' => $Win['prize'],
                            'winners' => 0,
                            'title' => $sMatchTitle
                );
            }
        }

        $aWinningCombinations = $this->getNumberCombinations();
        foreach ($aTickets as $aTicket) {
            //$aDrawnNumbers = explode("|", $aTicket['numbers']);
            $iTicketNumber = $aTicket['numbers'];

            $aMatch = array($this->_compareNumbers($aDrawnNumbers, $iTicketNumber), 0);

            $iWinningKey = array_search($aMatch, $aWinningCombinations);
            if ($iWinningKey !== FALSE) {
                $aWinners[] = array(
                    'amount' => $aWinnings[$aWinningCombinations[$iWinningKey][0] . "|" . $aWinningCombinations[$iWinningKey][1]]['prize'],
                    'fk_lottery_id' => $this->iLotteryID,
                    'fk_bookingitem_id' => $aTicket['bookingitem_id'],
                    'draw' => $iDraw,
                    'type' => $aWinnings[$aWinningCombinations[$iWinningKey][0] . "|" . $aWinningCombinations[$iWinningKey][1]]['title']
                );
            }
        }

        // Garbage collection
        unset($aTickets);
        unset($aDrawDetails);
        unset($aDrawnNumbers);
        unset($iTicketNumber);
        unset($aWinningCombinations);
        unset($aWinnings);

        return $aWinners;
    }

    /**
     * Decremental comparison of number strings
     *
     * @param array $aDrawnNumber array of all drawn numbers
     * @param integer $iTicketNumber number on the ticket
     * @return integer
     */
    private function _compareNumbers($aDrawnNumbers, $iTicketNumber) {

        // Convert ticket number to strings and reverse them
        $sTicketNumber = (string) $iTicketNumber;

        // Reverse array of drawn numbers, so that we start fron the largest number
        $aDrawnNumbers = array_reverse($aDrawnNumbers);

        // Set match variable here to be false
        // as foreach loop will exit as soon as it's set to true
        $bMatch = false;

        // Record last match position
        $iMatchPos = 0;

        // Loop through the array of drawn numbers
        foreach ($aDrawnNumbers as $iDrawnNumber) {
            // Convert to string
            $sDrawnNumber = (string) $iDrawnNumber;

            // Get the length of the current draw number
            $iLength = strlen($sDrawnNumber);

            // Start position for comparison is the negative value of the number length
            $iStartPos = -1 * abs($iLength);

            // Substring of tucket number
            $sTicketSub = substr($sTicketNumber, $iStartPos);

            // Compare the two strings
            if (strcmp($sDrawnNumber, $sTicketSub) === 0) {
                // Stringsmatch, so set flag accordingly
                $bMatch = true;

                // Record current position as the match position
                $iMatchPos = $iLength;
                exit(1);
            }
        }

        // Garbage collection
        unset($sTicketNumber);
        unset($sDrawnNumber);
        unset($iLength);
        unset($iStartPos);
        unset($sTicketSub);
        unset($bMatch);

        // Return position at where match was found
        return $iMatchPos;
    }

    /**
     *
     */
    public function bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem) {
        foreach ($oOrderItem->data['games'] as $game) {
            if ($game == 'super6') {
                $oSuper66 = new Super66();
                $oSuper66->bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem);
            }
            if ($game == 'spiel77') {
                $oSuper77 = new Super77();
                $oSuper77->bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem);
            }
            if ($game == 'glucks') {
                $this->_bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem);
            }
        }
    }

    public function _bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem) {
        $aData = array(
            "fk_booking_id" => $iBookingID,
            "fk_lottery_id" => $this->getID(),
            "fk_order_item_id" => $oOrderItem->iBookingOrderID,
            "fk_lotterydate" => $aDrawDate['drawdate'],
            "cost" => $this->getPrice(),
            "numbers" => implode("", $oOrderItem->data['extras']),
            "purchased" => 0
        );

        /**
         * Insert the booking item
         * @todo : should make this OOP rather than DAL
         */
        try {
            DAL::Insert('booking_items', $aData);
        } catch (Exception $e) {
            
        }
        return $response;
    }

}
