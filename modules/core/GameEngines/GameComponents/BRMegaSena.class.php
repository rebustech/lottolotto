<?php

/**
 * MegaSena definition class
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class BRMegaSena extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::BRMEGASENALOTTO, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 3;
        $this->aLottoDays = array("Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(3, 6);
        $this->sLottoDrawTime = "19:00";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);

        return $aMatchTypes;
    }

}
