<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Super77 extends GermanSupplementary {

    var $iNumberCount = 7;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::SPIEL77, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 1;
        $this->aLottoDays = array("Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(3, 6);
        $this->sLottoDrawTime = "18:50";

        //$this->fk_game_engine_id = 3;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(7, 0);
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 0);
        $aMatchTypes[] = array(1, 0);

        return $aMatchTypes;
    }

    public function getWinningAmounts() {
        $aAmounts = array();

        $aAmounts[] = 177777;
        $aAmounts[] = 77777;
        $aAmounts[] = 7777;
        $aAmounts[] = 777;
        $aAmounts[] = 77;
        $aAmounts[] = 17;
        $aAmounts[] = 5;

        return $aAmounts;
    }

    function getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));
        foreach ($oOrderItem->data['games'] as $game) {
            $fCurrentPrice+=2.50 * $weeks * $days;
            $oOrderItem->fExtrasPrice+=2.50 * $weeks * $days;
            $oOrderItem->aProductLists[] = 'Spiel77';
        }
        return $fCurrentPrice;
    }

}
