<?php

/**
 * Lottery definition class for EuroMillions
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class EuropeanLottery extends Lottery {

    var $bCanHaveSystem = true;

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::EUROPEANLOTTERY, $iLangID);

        $this->iCutOff = 300;
        $this->dPrice = 2.5;
        $this->aLottoDays = array("Tuesday", "Friday");
        $this->aLottoDayNumbers = array(2, 5);
        $this->sLottoDrawTime = "23:30";
        $this->fk_game_engine_id = 4;
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 2);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 2);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 2);
        $aMatchTypes[] = array(2, 2);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(1, 2);
        $aMatchTypes[] = array(2, 1);
        $aMatchTypes[] = array(2, 0);

        return $aMatchTypes;
    }

}
