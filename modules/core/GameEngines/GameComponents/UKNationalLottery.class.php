<?php

/**
 * Lottery definition class for the UK National Lottery
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class UKNationalLottery extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::NATIONALLOTTERY, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 2.5;
        $this->aLottoDays = array("Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(3, 6);
        $this->sLottoDrawTime = "19:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(6, 0);
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 0);

        return $aMatchTypes;
    }

}
