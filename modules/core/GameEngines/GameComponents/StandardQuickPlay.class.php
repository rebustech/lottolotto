<?php

/**
 * Megaplier game component
 */
class StandardQuickPlay extends Lottery {

    var $sFrontendView = '';

    function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        /**
         * Don't do anything here, just populate the post with config values
         * This needs to be amended to pass config values down to children, but
         * for now this will work
         */
        $oConfigData = json_decode($this->config);
        $oOrderItem->data['quickPlay'] = true;
        $oOrderItem->data['boards'] = $oConfigData->iLines;
        $oOrderItem->data['playDuration'] = $oConfigData->iWeeks;
        $oOrderItem->data['isSubscription'] = ($oConfigData->bSubscription == 1);
        $oOrderItem->data['day'] = array(1, 2);
    }

    function _getGameConfigurationFields() {
        $aGameConfigurationFields = parent::_getGameConfigurationFields();
        $aGameConfigurationFields['iLines'] = new ConfigurationNumber('iLines', 'Lines to play');
        $aGameConfigurationFields['iWeeks'] = new ConfigurationNumber('iWeeks', 'Weeks to play');
        $aGameConfigurationFields['bSubscription'] = new ConfigurationSwitch('bSubscription', 'As subscription');
        $aGameConfigurationFields['sDays'] = new ConfigurationNumber('sDays', 'Days to play');
        return $aGameConfigurationFields;
    }

}
