<?php

/**
 * Megaplier game component
 */
class Powerplay extends Lottery {

    var $sFrontendView = 'pods/games/powerplay';
    var $iLotteryID = Lottery::USAPOWERBALL;
    var $bCanHaveSystem = true;

    function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $oConfigData = json_decode($this->config);

        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));
        foreach ($oOrderItem->data['games'] as $game) {
            if ($game == 'powerplay') {
                $fCurrentPrice+=$oConfigData->Price * $weeks * $days * $oOrderItem->iTotalLines;
                $oOrderItem->fExtrasPrice+=$oConfigData->Price * $weeks * $days * $oOrderItem->iTotalLines;
                $oOrderItem->aProductLists[] = 'Powerplay';
            }
        }
        return $fCurrentPrice;
    }

}
