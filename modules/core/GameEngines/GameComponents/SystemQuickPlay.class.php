<?php

/**
 * Megaplier game component
 */
class SystemQuickPlay extends GameEngineComponent {

    var $sFrontendView = '';

    function _getGameConfigurationFields() {
        $aGameConfigurationFields = parent::_getGameConfigurationFields();
        $aGameConfigurationFields['iLines'] = new ConfigurationNumber('iLines', 'Lines to play');
        $aGameConfigurationFields['iWeeks'] = new ConfigurationNumber('iWeeks', 'Weeks to play');
        $aGameConfigurationFields['sDays'] = new ConfigurationNumber('sDays', 'Days to play');
        $aGameConfigurationFields['bSubscription'] = new ConfigurationSwitch('bSubscription', 'As subscription');
        $aGameConfigurationFields['iSystemType'] = new ConfigurationSwitch('iSystemType', 'System Type');
        $aGameConfigurationFields['iSystemSize'] = new ConfigurationNumber('iSystemSize', 'System Size');
        return $aGameConfigurationFields;
    }

}
