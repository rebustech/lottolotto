<?php

/**
 * Lottery definition class for UK Thunderball
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class UKThunderball extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::UKTHUNDERBALL, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 2.5;
        $this->aLottoDays = array("Wednesday", "Friday", "Saturday");
        $this->aLottoDayNumbers = array(3, 5, 6);
        $this->sLottoDrawTime = "19:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 1);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 1);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 1);
        $aMatchTypes[] = array(1, 1);
        $aMatchTypes[] = array(0, 1);

        return $aMatchTypes;
    }

    public function getWinningAmounts() {
        $aAmounts = array();
        $aAmounts[] = 500000;
        $aAmounts[] = 5000;
        $aAmounts[] = 250;
        $aAmounts[] = 100;
        $aAmounts[] = 20;
        $aAmounts[] = 10;
        $aAmounts[] = 10;
        $aAmounts[] = 5;
        $aAmounts[] = 3;

        return $aAmounts;
    }

}
