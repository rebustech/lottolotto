<?php

/**
 * Lottery definition class for French Lotto
 * @package LoveLotto
 * @subpackage LotteryDefiners
 */
class FrenchLotto extends Lottery {

    public function __construct($iLangID = 1) {
        parent::__construct(Lottery::FRENCHLOTTO, $iLangID);

        $this->iCutOff = 90;
        $this->dPrice = 4.5;
        $this->aLottoDays = array("Monday", "Wednesday", "Saturday");
        $this->aLottoDayNumbers = array(1, 3, 6);
        $this->sLottoDrawTime = "20:30";
    }

    public function getNumberCombinations() {
        $aMatchTypes[] = array(5, 1);
        $aMatchTypes[] = array(5, 0);
        $aMatchTypes[] = array(4, 0);
        $aMatchTypes[] = array(3, 0);
        $aMatchTypes[] = array(2, 0);
        $aMatchTypes[] = array(0, 1);

        return $aMatchTypes;
    }

}
