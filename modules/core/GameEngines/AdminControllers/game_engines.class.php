<?php
/**
 * Game engines admin controller
 * @package LoveLotto
 * @subpackage AdminControllers
 */
use LL\Interfaces\IAdminController,    
    LL\Interfaces\ISaveable;

class game_engines extends AdminController implements IAdminController, ISaveable{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);
        if($iId==0){
            $tabs=new TabbedInterface();

            /**
             * Creation only has the main tab
             */
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-magnet';
            $oTitle=new HtmlTagControl('h1','Create Product');
            $aItems[]=$oTitle->Output();
            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

            $tabs->aItems['Items']=$oItemsTab;
            $aItems[]=$tabs->Output();

        }else{
            /**
             * Create a form with a tabbed interface
             */
            $form=new SelfSubmittingForm();
            $form->submitToGeneric($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

            $tabs=new TabbedInterface();
            $form->AddControl($tabs);

            /**
             * Main details tab
             */
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-magnet';
            $oGameEngine=GameEngine::getById($iId);
            $oTitle=new HtmlTagControl('h1','Edit Product : '.$oGameEngine->name.' ('.$oGameEngine->getHandlerName().')');
            $aItems[]=$oTitle->Output();
            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);
            $tabs->aItems['main']=$oItemsTab;

            //Add configuration fields
            $oConfigControl=new ClassConfigurationControl($oGameEngine);
            $oItemsTab->AddControl($oConfigControl);

            /**
             * Image uploads tab
             */
            $oImagesTab=new TabbedInterface(lang::get('images'));
            $oImagesTab->sIcon='fa-image';

            $oEnvelopeImageControl=new FileUploadControl('Envelope Image','images/products/envelope_'.$this->id,'envelope');
            if($oGameEngine->image_envelope!='') $oEnvelopeImageControl->sPreviewFile=$oGameEngine->image_envelope;
            $oImagesTab->AddControl($oEnvelopeImageControl);

            $oEnvelopeMobileImageControl=new FileUploadControl('Envelope Image Mobile','images/products/envelope_mobile_'.$this->id,'envelope_mobile');
            if($oGameEngine->image_envelope_mobile!='') $oEnvelopeMobileImageControl->sPreviewFile=$oGameEngine->image_envelope_mobile;
            $oImagesTab->AddControl($oEnvelopeMobileImageControl);

            $oLobbyImageControl=new FileUploadControl('Lobby Image','images/products/lobby_'.$this->id,'lobby');
            if($oGameEngine->image_lobby!='') $oLobbyImageControl->sPreviewFile=$oGameEngine->image_lobby;
            $oImagesTab->AddControl($oLobbyImageControl);

            $oWidgetImageControl=new FileUploadControl('Widget Image','images/products/widget_'.$this->id,'widget');
            if($oGameEngine->image_widget!='') $oWidgetImageControl->sPreviewFile=$oGameEngine->image_widget;
            $oImagesTab->AddControl($oWidgetImageControl);

            $oSmallImageControl=new FileUploadControl('Small No Text','images/products/icon_sm_'.$this->id,'icon_sm');
            if($oGameEngine->image_lottery_sm!='') $oSmallImageControl->sPreviewFile=$oGameEngine->image_lottery_sm;
            $oImagesTab->AddControl($oSmallImageControl);

            $oSmallTextImageControl=new FileUploadControl('Small With Text','images/products/icon_sm_text_'.$this->id,'icon_sm_text');
            if($oGameEngine->image_lottery_sm_text!='') $oSmallTextImageControl->sPreviewFile=$oGameEngine->image_lottery_sm_text;
            $oImagesTab->AddControl($oSmallTextImageControl);

            $oLargerImageControl=new FileUploadControl('Larger No Text','images/products/icon_lg_'.$this->id,'icon_lg');
            if($oGameEngine->image_lottery_lg!='') $oLargerImageControl->sPreviewFile=$oGameEngine->image_lottery_lg;
            $oImagesTab->AddControl($oLargerImageControl);

            $oLargerTextImageControl=new FileUploadControl('Larger With Text','images/products/icon_lg_text_'.$this->id,'icon_lg_text');
            if($oGameEngine->image_lottery_lg_text!='') $oLargerTextImageControl->sPreviewFile=$oGameEngine->image_lottery_lg_text;
            $oImagesTab->AddControl($oLargerTextImageControl);

            $tabs->aItems['images']=$oImagesTab;

            /**
             * Components Tab
             */
            $oComponentsTab=new TabbedInterface(lang::get('components'));
            $oComponentsTab->sIcon='fa-puzzle-piece';
            $oComponentsControl=new GameEngineBuilder($oGameEngine);
            $oComponentsTab->AddControl($oComponentsControl);
            $tabs->aItems['components']=$oComponentsTab;

            /**
             * Countries Tab
             */
            $oCountriesTab=new TabbedInterface(lang::get('countries'));
            $oCountriesTab->sIcon='fa-globe';

            //Get all available countries for the picker
            $aAllCountries=DAL::executeQuery('SELECT country_id as id, comment as name FROM game_engine_markets gem RIGHT JOIN countries_cmn c ON c.country_id=gem.fk_country_id AND fk_game_engine_id='.$iId.' WHERE gem.fk_country_id is null ORDER BY name');
            //Get assigned countries
            $aSelectedCountries=DAL::executeQuery('SELECT country_id as id, comment as name FROM game_engine_markets gem INNER JOIN countries_cmn c ON c.country_id=gem.fk_country_id WHERE fk_game_engine_id='.$iId.' ORDER BY name');
            $oCountriesControl=new ManyToManyPickListControl($aAllCountries,$aSelectedCountries);
            $oCountriesControl->sName='countries';
            $oCountriesControl->sCaption='Countries';

            $oCountriesTab->AddControl($oCountriesControl);
            $tabs->aItems['countries']=$oCountriesTab;

            /**
             * Partners tab
             */
            $oWhiteLabelsTab=new TabbedInterface(lang::get('partners'));
            $oWhiteLabelsTab->sIcon='fa-sitemap';
            $aAllWhiteLabels=DAL::executeQuery('SELECT id, name FROM game_engine_affiliate_groups gewl RIGHT JOIN affiliate_groups wl ON wl.id=gewl.fk_affiliate_group_id AND fk_game_engine_id='.$iId.' WHERE gewl.fk_affiliate_group_id is null ORDER BY name');
            //Get assigned countries
            $aSelectedWhiteLabels=DAL::executeQuery('SELECT id, name FROM game_engine_affiliate_groups gewl INNER JOIN affiliate_groups wl ON wl.id=gewl.fk_affiliate_group_id WHERE fk_game_engine_id='.$iId.' ORDER BY name');
            $oWhiteLabelsControl=new ManyToManyPickListControl($aAllWhiteLabels,$aSelectedWhiteLabels);
            $oWhiteLabelsControl->sName='affiliategroups';
            $oWhiteLabelsControl->sCaption='Partners';
            $oWhiteLabelsTab->AddControl($oWhiteLabelsControl);
            $tabs->aItems['partners']=$oWhiteLabelsTab;
          

            /**
             * Audit Log tab
             */
            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';
            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'game_engines\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');
            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            $oItemsTable->keepFields('audit_log','datetime,code,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems['audit']=$oLogTab;


            /**
             * Let the game engine make any changes to this ui such as
             * add additional tabs, or even remove them if necessary
             */
            $tabs=$oGameEngine->customiseAdminController($tabs);

            $aItems[]=$form->Output();
        }


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }

        return $sOut;

    }

    /**
     * Process a save event on the game engine page. This will have already performed a generic update,
     * but will now instantiate the relevant game engine and process the save. Each component
     * can then process any data pertinant to itself.
     */
    function afterGenericFormPost(){

        if(!is_numeric($_GET['id'])){
            throw new InvalidArgumentException('adminControllers.game_engines.afterGenericSave.IDNotNumeric');
        }
        /**
         * Pass the data to the game engine components
         */
        $gameEngineBuilderData=json_decode($_POST['gameEngineBuilderData']);
        $oGameEngine=GameEngine::getById(intval($_GET['id']));
        $oGameEngine->afterGenericFormPost();

        $oConfig=new stdClass();
        foreach($_POST['config'] as $k=>$v){
            $oConfig->$k=$v;
        }

        $oGameEngine->config=json_encode($oConfig);

        /**
         * Create new game components
         */
        $oGameEngine->processGameBuilderData($gameEngineBuilderData);

        /**
         * Process countries that this game engine is valid for
         */
        \DAL::Query('DELETE FROM game_engine_markets WHERE fk_game_engine_id='.$oGameEngine->id);
        foreach($_POST['countries'] as $country){
            \DAL::Insert('game_engine_markets', array('fk_game_engine_id'=>$oGameEngine->id,'fk_country_id'=>intval($country)));
        }

        /**
         * Process white labels that this game engine is valid for
         */
        \DAL::Query('DELETE FROM game_engine_affiliate_groups WHERE fk_game_engine_id='.$oGameEngine->id);
        foreach($_POST['affiliategroups'] as $affiliate_group){
            \DAL::Insert('game_engine_affiliate_groups', array('fk_game_engine_id'=>$oGameEngine->id,'fk_affiliate_group_id'=>intval($affiliate_group)));
        }

        $oEnvelopeImageControl=new FileUploadControl('Envelope Image','images/products/envelope_'.$oGameEngine->id,'envelope');
        if($oEnvelopeImageControl->doUpload($oGameEngine)){
            $oGameEngine->image_envelope=$oEnvelopeImageControl->sStoredFilename;
        }

        $oLobbyImageControl=new FileUploadControl('Lobby Image','images/products/lobby_'.$oGameEngine->id,'lobby');
        if($oLobbyImageControl->doUpload($oGameEngine)){
            $oGameEngine->image_lobby=$oLobbyImageControl->sStoredFilename;
        }

        $oResultsImageControl=new FileUploadControl('Results Page Image','images/products/results_'.$oGameEngine->id,'results');
        if($oResultsImageControl->doUpload($oGameEngine)){
            $oGameEngine->image_results=$oResultsImageControl->sStoredFilename;
        }

        $oWidgetImageControl=new FileUploadControl('Widget Image','images/products/widget_'.$oGameEngine->id,'widget');
        if($oWidgetImageControl->doUpload($oGameEngine)){
            $oGameEngine->image_widget=$oWidgetImageControl->sStoredFilename;
        }

        $oSmallImageControl=new FileUploadControl('Small No Text','images/products/icon_sm_'.$oGameEngine->id,'icon_sm');
        if($oSmallImageControl->doUpload($oLottery)){
            $oGameEngine->icon_sm=$oSmallImageControl->sStoredFilename;
        }

        $oSmallTextImageControl=new FileUploadControl('Small With Text','images/products/icon_sm_text_'.$oGameEngine->id,'icon_sm_text');
        if($oSmallTextImageControl->doUpload($oLottery)){
            $oGameEngine->icon_sm_text=$oSmallTextImageControl->sStoredFilename;
        }

        $oLargerImageControl=new FileUploadControl('Larger No Text','images/products/icon_lg_'.$oGameEngine->id,'icon_lg');
        if($oLargerImageControl->doUpload($oLottery)){
            $oGameEngine->icon_lg=$oLargerImageControl->sStoredFilename;
        }

        $oLargerTextImageControl=new FileUploadControl('Larger With Text','images/products/icon_lg_text_'.$oGameEngine->id,'icon_lg_text');
        if($oLargerTextImageControl->doUpload($oLottery)){
            $oGameEngine->icon_lg_text=$oLargerTextImageControl->sStoredFilename;
        }

        $oGameEngine->save();
    }
}