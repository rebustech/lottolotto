<?php

/**
 * Override for the game engine core field on the game engines forms
 * @package LoveLotto
 * @subpackage FormFieldHandlers
 */
class FormField_game_engines_engine_core extends FormFieldHandlerListing {

    var $sHandlerType = 'GameEngineCore';

}
