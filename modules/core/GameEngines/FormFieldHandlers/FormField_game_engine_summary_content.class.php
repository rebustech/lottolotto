<?php
/**
* Just a note on what this actually does:
* When rendering a field called summary_content on entity game_engine in the administration
* area, instead of automatically determining the fields type this class will be used instead
* This class extends \LL\VariantsCMS\CMSField which is one of those big green CMS buttons
* I showed you.
*
* Basically this bit of code turns this single field into a full-blown multilingual CMS editor!
*
* There are many other field types included with the system including dropdown menus that list
* classes (such as payment gateways, game engines etc...) and they all work in this way, although
* some have additional configuration.
**/
class FormField_game_engine_summary_content extends \LL\VariantsCMS\CMSField{
    
}