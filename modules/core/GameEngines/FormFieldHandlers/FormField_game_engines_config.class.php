<?php

/**
 * Override for the game engine core field on the game engines forms
 * @package LoveLotto
 * @subpackage FormFieldHandlers
 */
class FormField_game_engines_config extends \ClassConfigurationControl {

    function Output() {

        $oGameEngine = GameEngine::getById($this->iId);
        if ($oGameEngine instanceof \GameEngine) {
            $oConfig = $oGameEngine->getGameConfigurationFields();
            if (is_array($oConfig)) {
                $oConfigEngine = new ConfigurationEngine();
                $oConfigEngine->aFields = $oConfig;
                if ($oGameEngine->config != '')
                    $oConfigEngine->parseData($oGameEngine->config);
                foreach ($oConfig as $oField) {
                    $sOut.=$oField->output();
                }

                return $sOut;
            }
        }
    }

}
