<?php

/**
 * Base class for a game engine component
 */
class GameEngineComponent extends LLModel implements LLSaveable, IGameEngineComponent {

    var $bCanHaveSystem = false;
    static $sTableName = 'game_engine_components';
    static $sHandlerField = 'handler';

    /**
     * Events
     */
    const EVENT_BEFORE_GET_CONFIGURATION_FIELDS = 'EVENT_BEFORE_GET_CONFIGURATION_FIELDS';
    const EVENT_AFTER_GET_CONFIGURATION_FIELDS = 'EVENT_AFTER_GET_INSTANCE_CONFIGURATION_FIELDS';
    const EVENT_BEFORE_GET_GAME_CONFIGURATION_FIELDS = 'EVENT_BEFORE_GET_GAME_CONFIGURATION_FIELDS';
    const EVENT_AFTER_GET_GAME_CONFIGURATION_FIELDS = 'EVENT_AFTER_GET_GAME_CONFIGURATION_FIELDS';
    const EVENT_BEFORE_GET_INSTANCE_CONFIGURATION_FIELDS = 'EVENT_BEFORE_GET_INSTANCE_CONFIGURATION_FIELDS';
    const EVENT_AFTER_GET_INSTANCE_CONFIGURATION_FIELDS = 'EVENT_AFTER_GET_INSTANCE_CONFIGURATION_FIELDS';

    /**
     * Decides on which component this is a child of.
     * Anything with parent id of 0 sits at the top of the chain
     * Anything with a parent id of 1 sits in the common section of this game engine
     * @var integer
     */
    var $fk_parent_id = 0;

    /**
     * Decides which game engine this component is part of
     * @var integer
     */
    var $fk_game_engine_id;

    /**
     * JSON encoded string of config data
     * @var string
     */
    var $config;

    /**
     * Name of this component
     * @var string
     */
    var $name;

    /**
     * Name of class that will handle this game engine compoment
     * @var string
     */
    var $handler;

    /**
     * If this is a lottery it needs to be treated slightly differently as it will be a
     * source of winnings
     * @var boolean
     */
    var $is_lottery;
    var $aConfigurationFields;
    var $aGameFields;
    var $aGameCreationFields;
    var $aInstanceFields;
    var $aChildComponents = null;
    var $sFrontEndView;
    var $image_results;

    function __sleep() {
        /**
         * Remove child compoments and replace with a list of ids that will be used to
         * wake the class back up
         */
        $__aChildCompoments = array();
        foreach ($this->aChildComponents as $oChildCompoment) {
            $__aChildCompoments[] = $oChildCompoment->id;
        }
        $this->__aChildComponents = $__aChildCompoments;
    }

    function __wakeup() {
        if (is_array($this->__aChildCompoments)) {
            foreach ($this->__aChildCompoments as $iChildCompomentId) {
                $this->aChildComponents[] = GameEngineComponent::getById($iChildCompomentId);
            }
        }
    }

    /**
     * Gets the parent component by first checking in memcache, and falling back
     * to the database (this will be the local read only database)
     * @return GameEngineComponent
     */
    public function getParentComponent() {
        return self::getById($this->fk_parent_id);
    }

    public function getInstalledComponents() {
        
    }

    public function getChildComponents($bForceReload = false) {
        if ($bForceReload) {
            $this->aChildComponents = null;
        }
        if ($this->aChildComponents === null) {
            $sSql = 'SELECT id FROM game_engine_components WHERE fk_parent_id=' . $this->id;
            $aChildren = DAL::executeQuery($sSql);
            foreach ($aChildren as $aChild) {
                $iId = $aChild['id'];
                $oNewChild = self::getById($iId);
                $this->aChildComponents[] = $oNewChild;
            }
        }
        return $this->aChildComponents;
    }

    public function getAllDescendants($aDescendants = array()) {
        foreach ($this->getChildComponents() as $oChild) {
            $aDescendants = $oChild->getAllDescendants($aDescendants);
            $aDescendants[] = $oChild;
        }
        return $aDescendants;
    }

    /**
     * Performs any modifications to the price of this game
     *
     * $fCurrentPrice is passed in, modified and returned.
     * The default is just to add the cost of this component onto $fCurrentPrice
     *
     * If you need to do something more complex in a component then override _getPrice
     * method, as this will leave passing to child components to this method
     * If for some reason your component needs to prevent passing the price
     * calculation down through the tree then override getPrice and _getPrice
     * leave _getPrice blank, and put your calculation in getPrice.
     *
     * @param float $fCurrentPrice The current price
     * @return float The new price after any modifications are made
     */
    public function getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        /**
         * Pass down to all child components to see if any price modifications need
         * to be made. We don't add the returns because we let each component
         * perform whatever changes it needs to make.
         * The default thing we do is just to add the price of this component
         * as configured
         */
        //Add the component price to current price
        $fCurrentPrice = $this->_getPrice($fCurrentPrice, $oOrderItem);

        //Pass down to the children
        $aChildComponents = $this->getChildComponents(true);
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $fCurrentPrice = $oChildComponent->getPrice($fCurrentPrice, $oOrderItem);
            }
        }

        return $fCurrentPrice;
    }

    /**
     * Performs actual price calculation for this component.
     *
     * $fCurrentPrice is passed in, modified and returned.
     * The default is just to add the cost of this component onto $fCurrentPrice
     *
     * @todo Need to create a pricing matrix system which would then hook in here
     * @param float $fCurrentPrice The current price
     * @return float The new price after modification
     */
    protected function _getPrice($fCurrentPrice = 0, $oOrderItem = null) {
        $fComponentPrice = $this->config->price;
        if ($fComponentPrice <> 0) {
            $fCurrentPrice+=$fComponentPrice;
        }
        return $fCurrentPrice;
    }

    /**
     * Calculates the odds of winning on this component, this is combined with the odds
     * of winning on all other components to determine the overall chance of winning
     * These are the odds displayed to a customer
     */
    public function calculateOdds($fCurrentOdds = 0) {

        //Add the component price to current price
        $fCurrentOdds = $this->_calculateOdds($fCurrentOdds);

        //Pass down to the children
        $aChildComponents = $this->getChildComponents();
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $fCurrentOdds = $oChildComponent->calculateOdds($fCurrentOdds);
            }
        }

        return $fCurrentOdds;
    }

    protected function _calculateOdds($fCurrentOdds = 0) {

        return $fCurrentOdds;
    }

    public function getAvailableDates() {
        
    }

    public function getMaxTickets($iMaxTickets = null) {
        if ($iMaxTickets === null)
            $iMaxTickets = $this->iMaxTickets;
        $aChildComponents = $this->getChildComponents();
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $iChildMaxTickets = $oChildComponent->getMaxTickets($iMaxTickets);
                if ($iChildMaxTickets < $iMaxTickets) {
                    $iMaxTickets = $iChildMaxTickets;
                }
            }
        }
        return $iMaxTickets;
    }

    public function getCartItemName($sCartName = '') {
        $sCartName = $this->_getCartItemName($sCartName);

        $aChildComponents = $this->getChildComponents();
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $sCartName.=$oChildComponent->getCartItemName($sCartName);
            }
        }
        return $sCartName;
    }

    public function _getCartItemName($sCartName = '') {
        return $sCartName;
    }

    /**
     * Calculates the odds of winning for insurance purposes. This is different from
     * calculateOdds because not all prize tiers are insured and so the odds being
     * used for calculating are not always the same (for example second chance increases
     * the odds of winning, but doesn't always affect insurance because it may only
     * be available on lower prize tiers)
     * @param float $fCurrentOdds
     * @return float
     */
    public function calculateOddsOfWinning($fCurrentOdds) {
        return $fCurrentOdds;
    }

    /**
     * Calculates the maxmium possible prize for someone winning on this game
     */
    public function calculatePrize($fCurrentPrize) {
        return $fCurrentPrize;
    }

    /**
     * Process the order data. Generic engines/components just need to pass
     * this call down the line. For specific components that are listed
     * in the order these will need to intercept any relevant data and process
     * @param type $oOrder
     */
    public function processOrderItem($oOrderItem, $iBookingID) {
        $aResponse = array();

        //Let this component process the order as it wants to
        $aResponse = array_merge($aResponse, $this->_processOrderItem($oOrderItem, $iBookingID));
        //Then pass to all the child components
        $aChildComponents = $this->getChildComponents();
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $aResponse = array_merge($aResponse, $oChildComponent->_processOrderItem($oOrderItem, $iBookingID));
            }
        }
        return $aResponse;
    }

    public function bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem) {
        //Default does nothing - all components must do something with this
    }

    /**
     * Default process order is to do nothing - these need to be implemented
     * in each game component.
     * @param Order $oOrder
     * @param Booking $oBooking
     * @return array Array of responses
     */
    protected function _processOrderItem($oOrderItem, $iBookingID) {
        return array();
    }

    /**
     * Provides validation services to an order
     * @param type $oOrder
     * @return type
     */
    public function validate($oOrder) {
        $aResponse = array();
        $aChildComponents = $this->getChildComponents();
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $aResponse[] = $oChildComponent->validate($oOrder);
            }
        }
        return $aResponse;
    }

    public function beforeOrderCancel($oOrder) {
        
    }

    public function afterOrderCancel($oOrder) {
        
    }

    /**
     * Called when a booking item has a winning result. Needs to assign winnings
     * to players account
     * @param type $oBookingItem
     */
    public function afterWin($oBookingItem, $oResult) {
        
    }

    /**
     * Called when a booking item has not had any winnings
     * @param type $oBookingItem
     * @param type $oResult
     */
    public function afterLoose($oBookingItem, $oResult) {
        
    }

    public function afterPayout($oBookingItem, $oTransaction) {
        
    }

    /**
     * Remove the booking item, if it's been drawn assign to the house along with
     * any winnings associated with that transaction
     * @param type $oBookingItem
     * @param type $oTransaction
     */
    public function afterChargeBack($oBookingItem, $oTransaction) {
        
    }

    public function processGameBuilderData($oData) {
        $aChildren = $this->getChildComponents();
        foreach ($oData->childComponents as $oChildComponent) {
            if ($oChildComponent->id < 0) {
                $oHandler = $oChildComponent->handler;
                $oNewChild = new $oHandler();
                $oNewChild->name = $oChildComponent->name;
                $oNewChild->handler = $oHandler;
                $oNewChild->fk_parent_id = $oChildComponent->parent_id;
                //Create a new child under this one
                if ($this instanceof \GameEngine) {
                    $oNewChild->fk_game_engine_id = $this->id;
                }
                if ($oHandler instanceof \Lottery) {
                    $oNewChild->is_lottery = 1;
                }
                $oNewChild->save();
                $oNewChild->processGameBuilderData($oChildComponent);
            }
        }
    }

    public function afterGenericFormPost($aData = null) {
        if ($aData === null)
            $aData = $_POST;
        //Find any component fields with the same id as this component

        $oCurrentConfigData = $this->populateGameConfigurationFields();

        foreach ($aData['component_data'][$this->id] as $sComponentField => $mCompomentFieldData) {
            switch ($sComponentField) {
                case 'name':
                    $this->name = $mCompomentFieldData;
                    break;
                case 'handler':
                    $this->handler = $mCompomentFieldData;
                    break;
                case 'parent_id':
                    $this->fk_parent_id = $mCompomentFieldData;
                    break;
                case 'game_engine':
                    $this->fk_game_engine_id = $mCompomentFieldData;
                    break;
                default:

                    if ($mCompomentFieldData != $oCurrentConfigData->aFields[$sComponentField]->mValue) {
                        AuditLog::LogItem('Changed config data field ' . $sComponentField . ' from [' . $aCurrentConfigData->$sComponentField . '] to [' . $mCompomentFieldData . ']', 'RECORD_CHANGE', 'GameEngineCompoment', $this->id);
                        $oCurrentConfigData->aFields[$sComponentField]->mValue = $mCompomentFieldData;
                    }
            }
        }
        $this->config = $oCurrentConfigData->serialize();
        //Now pass this event down to any child components
        $aChildren = $this->getChildComponents();
        foreach ($aChildren as $oChild) {
            $oChild->afterGenericFormPost($aData);
        }

        $this->save();
    }

    /**
     * Returns an array of configuration fields used in the backend
     * @return ConfigurationEngine
     */
    function getBackendConfigurationFields() {
        Events::Trigger(self::EVENT_BEFORE_GET_CONFIGURATION_FIELDS, $this);
        $this->aConfigurationFields = $this->_getBackendConfigurationFields();
        Events::Trigger(self::EVENT_AFTER_GET_CONFIGURATION_FIELDS, $this);
        return $this->aConfigurationFields;
    }

    function _getBackendConfigurationFields() {
        
    }

    /**
     * Returns an array of configuration fields used in the backend when configuring
     * a game (i.e. a game made up of individual components)
     * @return ConfigurationEngine
     */
    function getGameConfigurationFields() {
        Events::Trigger(self::EVENT_BEFORE_GET_GAME_CONFIGURATION_FIELDS, $this);
        $this->aGameFields = $this->_getGameConfigurationFields();
        Events::Trigger(self::EVENT_AFTER_GET_GAME_CONFIGURATION_FIELDS, $this);
        return $this->aGameFields;
    }

    function _getGameConfigurationFields() {
        $aGameConfigurationFields = array();
        $aGameConfigurationFields['Price'] = new ConfigurationString('Price', 'Price Modification');
        return $aGameConfigurationFields;
    }

    // <Nathan 29/10/2014>
    // Functions used in the CMS for creating orders
    function getGameCreationFields() {
        Events::Trigger(self::EVENT_BEFORE_GET_GAME_CREATION_FIELDS, $this);
        $this->aGameCreationFields = $this->_getGameCreationFields();
        Events::Trigger(self::EVENT_AFTER_GET_CREATION_FIELDS, $this);
        return $this->aGameCreationFields;
    }

    // Base class for function above
    // Can be overridden
    function _getGameCreationFields() {
        $aGameCreationFields = array();
        $aGameCreationFields['playDuration'] = new ConfigurationString('playDuration', 'Number of Weeks');
        $aGameCreationFields['boards'] = new ConfigurationString('boards', 'Number of Boards');
        $aGameCreationFields['isSubscription'] = new ConfigurationSwitch('isSubscription', 'Subscription?');
        $aGameCreationFields['protectJackpot'] = new ConfigurationSwitch('protectJackpot', 'Protect Jackpot?');
        return $aGameCreationFields;
    }

    function populateGameConfigurationFields() {
        $oFields = new ConfigurationEngine();
        $oFields->aFields = $this->getGameConfigurationFields();
        $oFields->parseData($this->config);
        return $oFields;
    }

    public function getGameUrl() {
        return $this->url;
    }

    public function getBoardSize() {
        return $this->iMaxNumber;
    }

    public function getNumberOfLines() {
        return $this->iNumbersPerLine;
    }

    public function getNumberOfLinesMobile() {
        return $this->iNumbersPerLineMobile;
    }

    public function getNumberOfLinesTablet() {
        return $this->iNumbersPerLineTablet;
    }

    public function getNumbersCount() {
        return $this->iNumbersCount;
    }

    public function getMaxBonusNumbers() {
        return $this->iBonusMaxNumber;
    }

    public function getBonusNumbersCount() {
        return $this->iBonusNumbers;
    }

    public function getGameCssFile() {
        return $this->css_file;
    }

    /**
     * Returns an array of configuration fields used in the backend on an individual instance
     * of a game
     * @return ConfigurationEngine
     */
    function getInstanceConfigurationFields() {
        Events::Trigger(self::EVENT_BEFORE_GET_INSTANCE_CONFIGURATION_FIELDS, $this);
        $this->aInstanceFields = $this->_getInstanceConfigurationFields();
        Events::Trigger(self::EVENT_AFTER_GET_INSTANCE_CONFIGURATION_FIELDS, $this);
        return $this->aInstanceFields;
    }

    function _getInstanceConfigurationFields() {
        
    }

    function getDump() {
        $oOut = new stdClass();
        $oOut->id = $this->id;
        $oOut->name = $this->name;
        $oOut->handler = $this->handler;
        $oOut->is_active = $this->is_active;
        $oOut->parent_id = $this->fk_parent_id;
        $oOut->configurationFields = $this->getGameConfigurationFields();
        /**
         * Pull in the configuration data from the json encoded config string
         */
        $oConfigData = json_decode($this->config);
        foreach ($oOut->configurationFields as $sConfigurationField => $oConfigurationField) {
            $oOut->configurationFields[$sConfigurationField]->mValue = $oConfigData->$sConfigurationField;
        }
        $oOut->childComponents = $this->getDumpOfChildren($this->getChildComponents());
        return $oOut;
    }

    function getDumpOfChildren($aChildComponents) {
        $aOut = array();
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $aOut[] = $oChildComponent->getDump();
            }
        }
        return $aOut;
    }

    function save($aData = array()) {
        if ($this->fk_parent_id == 0)
            $this->fk_parent_id = null;

        $aData['config'] = $this->config;
        $aData['name'] = $this->name;
        $aData['handler'] = $this->handler;
        $aData['fk_game_engine_id'] = $this->fk_game_engine_id;
        $aData['fk_parent_id'] = $this->fk_parent_id;
        $aData['is_lottery'] = ($this instanceof Lottery) ? 1 : 0;

        parent::save($aData);
    }

    /**
     * This just returns a default cutoff of 30 mins. Lotteries override this
     * @return int
     */
    public function getCutoff() {
        return 30;
    }

    /**
     * Gets the view used to display this component on the frontend
     * Can be overridden to return a different view dependant on criteria
     * otherwise just returns the value of $this->sFrontendView
     * @return string
     */
    public function getFrontEndView() {
        return $this->sFrontendView;
    }

}
