<?php

namespace LL\GameEngines;

class Response {

    var $aErrors = array();
    var $aMessages = array();

    function addError($sErrorCode, $sErrorMessage) {
        $this->aErrors[$sErrorCode] = $sErrorMessage;
    }

    function addMessage($sMessage) {
        $this->aMessages[] = $sMessage;
    }

}
