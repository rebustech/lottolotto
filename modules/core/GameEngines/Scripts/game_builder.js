var gameBuilder = {};
gameBuilder.newChildParent = null;


gameBuilder.compomentMoved = function (event, ui) {
    var newParent = ui.item.parent().parent().attr('data-id');
    $(ui.item.find('> div')[0].eForm).find('input[data-field="parent_id"]').val(newParent);
};

gameBuilder.loadPalette = function () {
    var ePalette = $('.game_engine_palette ul');
    for (var iComponentIndex in gameEnginePalette) {
        var oComponent = gameEnginePalette[iComponentIndex];
        var eComponent = $('<li/>');
        eComponent.attr('data-handler', oComponent.class);
        eComponent.text(oComponent.name);
        ePalette.append(eComponent);
    }
}

gameBuilder.iNewComponentId = -1;
gameBuilder.compoment = function (sName, sHandler) {
    var _self = this;

    this.id = gameBuilder.iNewComponentId--;
    this.name = sName;
    this.childComponents = [];
    this.configurationFields = [];
    this.handler = sHandler;
    this.parent_id = null;

    this.addChild = function (oComponent) {
        _self.childComponents.push(oComponent);
    }
}

gameBuilder.createAddItemButton = function () {
    return $('<i class="fa fa-plus-circle fa-2x gameBuilderAddItem"></i>');
}

gameBuilder.addItemSelector = function () {
    gameBuilder.newChildParent = $(this);
    $('.game_engine_palette').show();
}

gameBuilder.addItem = function () {
    var oComponent = new gameBuilder.compoment($(this).text(), $(this).attr('data-handler'));
    if ($(gameBuilder.newChildParent).hasClass('topLevelComponent')) {
        eNewItem = gameBuilder.createComponentMarkup(oComponent, 1);
        $(gameBuilder.newChildParent).before(eNewItem);
        gameEngineBuilderData.childComponents.push(oComponent);
        /**
         * At top level also need to recalculate the width of the top level items
         * so that they sit side-by-side
         */
        var aTopLevelElements = $('.gameEngineTopLevels > li:not(.commonElements)');
        $(aTopLevelElements).css('width', ((100 / aTopLevelElements.length) - 2) + '%');
    } else {
        eParentComponent = $(gameBuilder.newChildParent).parent();
        oComponent.parent_id = $(eParentComponent)[0].oComponent.id;
        $(eParentComponent)[0].oComponent.childComponents.push(oComponent);
        eNewItem = gameBuilder.createComponentMarkup(oComponent, 1);
        eParentComponent.find('ul:eq(0)').append(eNewItem);
    }
    $('.game_engine_palette').removeClass('visible');
}

gameBuilder.showPalette = function () {
    gameBuilder.newChildParent = $(this);
    $('.game_engine_palette').addClass('visible');
}

gameBuilder.hideStuff = function () {
    $('.game_engine_palette .visible').removeClass('visible');
}

gameBuilder.oActiveComponent = null;
gameBuilder.showComponentConfig = function () {
    if (gameBuilder.oActiveComponent != null) {
        gameBuilder.updateActiveComponentData();
    }
    var oComponent = $(this).parent()[0].oComponent;
    gameBuilder.oActiveComponent = oComponent;
    var sComponentClass = oComponent.handler;
    var eActiveComponent = $('.game_engine_holder li > div.active');
    if (eActiveComponent.length > 0) {
        var eActiveForm = eActiveComponent[0].eForm;
        $('.game_engine_data').append(eActiveForm);
    }
    $('.game_engine_holder li > div.active').removeClass('active');
    $(this).addClass('active');
    var eForm = $(this)[0].eForm;
    $('.game_engine_config').addClass('visible');
    $('.game_engine_config .popupContent').html('').append('<h2>' + gameBuilder.oActiveComponent.name + '</h2>').append(eForm);

    /**
     * Copy the config from the seleted component into the form
     */
    for (iConfigFieldIndex in gameBuilder.oActiveComponent.configurationFields) {
        oConfigurationField = gameBuilder.oActiveComponent.configurationFields[iConfigFieldIndex];
        $('.game_engine_config *[name=' + oConfigurationField.sName).val(oConfigurationField.mValue);
    }
}

gameBuilder.updateActiveComponentData = function () {
    for (iConfigFieldIndex in gameBuilder.oActiveComponent.configurationFields) {
        oConfigurationField = gameBuilder.oActiveComponent.configurationFields[iConfigFieldIndex];
        oConfigurationField.mValue = $('.game_engine_config *[name=' + oConfigurationField.sName).val();
    }
}

/**
 * Creates the markup and functionality for the game builder from the game builder
 * JSON data embedded in the page
 */
gameBuilder.setup = function () {
    gameBuilder.loadPalette();
    //Construct the markup
    //Start with the top level items all in a ul
    var eTopLevel = $('<ul class="gameEngineTopLevels connectedSortable"/>');
    for (var iComponentIndex in gameEngineBuilderData.childComponents) {
        var oComponent = gameEngineBuilderData.childComponents[iComponentIndex];
        var eChildComponent = gameBuilder.createComponentMarkup(oComponent, gameEngineBuilderData.childComponents.length);

        eTopLevel.append(eChildComponent);
    }
    eTopLevel.append(gameBuilder.createAddItemButton().addClass('topLevelComponent'));

    if (gameEngineBuilderData.childComponents.length > 1) {
        var eCommonComponentHolder = $('<li class="draggable commonElements"><h4>Common components on any of the above</h4></li>');
        var eCommonComponentHolderUL = $('<ul class="connectedSortable"/>');
        eCommonComponentHolder.append(eCommonComponentHolderUL);
        for (var iComponentIndex in gameEngineBuilderData.commonModifiers) {
            var oComponent = gameEngineBuilderData.commonModifiers[iComponentIndex];
            var eChildComponent = gameBuilder.createComponentMarkup(oComponent);

            eCommonComponentHolderUL.append(eChildComponent);
        }
        //Create an add item button
        eCommonComponentHolder.append(gameBuilder.createAddItemButton().addClass('topChidLevelComponent'));
        eTopLevel.append(eCommonComponentHolder);

        var eCommonAllComponentHolder = $('<li class="draggable commonElements"><h4>Common components on ALL of the above</h4></li>');
        var eCommonAllComponentHolderUL = $('<ul class="connectedSortable"/>');
        eCommonAllComponentHolder.append(eCommonAllComponentHolderUL);
        eTopLevel.append(eCommonAllComponentHolder);
    }

    $('.game_engine_holder .content').html(eTopLevel);

    $('.game_engine_holder ul').sortable({
        connectWith: ".connectedSortable",
        placeholder: "ui-sortable-placeholder",
        stop: gameBuilder.compomentMoved
    }).disableSelection();

    $('.game_engine_holder').on('click', 'li > div', gameBuilder.showComponentConfig);

    /**
     * Setup events
     */
    $('body').on('click', '.gameBuilderAddItem', gameBuilder.showPalette);
    $('body').on('click', '.game_engine_palette li', gameBuilder.addItem);
    $('body').on('click', gameBuilder.hideStuff);
    $('form').submit(gameBuilder.prepareSave);

}


gameBuilder.prepareSave = function () {
    var eForm = $('#mainEditForm');
    var iComponentData = $('<textarea name="gameEngineBuilderData"></textarea>');
    iComponentData.val(JSON.stringify(gameEngineBuilderData));
    iComponentData.css('display', 'none');
    eForm.append(iComponentData);

    //Country Data
    $('#countries .selectedItems li').each(function () {
        var eSelectedCountry = $('<input type="checkbox" checked="checked" name="countries[]" value="' + $(this).attr('data-id') + '"/>');
        eSelectedCountry.css('display', 'none');
        eForm.append(eSelectedCountry);
    });

    //White Label Data
    $('#affiliategroups .selectedItems li').each(function () {
        var eSelectedCountry = $('<input type="checkbox" checked="checked" name="affiliategroups[]" value="' + $(this).attr('data-id') + '"/>');
        eSelectedCountry.css('display', 'none');
        eForm.append(eSelectedCountry);
    });
}

/**
 * Creates a game engine component
 * @param object oComponent Object data containing the component to be created
 * @param int iTotalInSet
 * @returns jQuery Returns the jQuery object for the LI that can be appended to the list
 * of components in the caller
 */
gameBuilder.createComponentMarkup = function (oComponent, iTotalInSet) {
    try {
        var eComponent = $('<li class="draggable"></li>');
        eComponent.attr('data-id', oComponent.id).attr('parent-id', oComponent.parent_id);
        if (iTotalInSet > 1)
            eComponent.css('width', ((100 / iTotalInSet) - 2) + '%');
        var eItem = $('<div/>');
        var eTitle = $('<h2/>').text(oComponent.name);
        var eClass = $('<h6/>').text('(' + oComponent.handler + ')');
        var eChildren = $('<ul class="connectedSortable"/>');
        //Add the fields

        //Build it up
        eComponent.append(eItem);
        eItem.append(eTitle);
        eItem.append(eClass);

        eComponent[0].oComponent = oComponent;

        if (oComponent.parent_id == null) {
            //Add any children
            if (oComponent.childComponents.length > 0) {
                for (var iComponentIndex in oComponent.childComponents) {
                    var oChildComponent = oComponent.childComponents[iComponentIndex];
                    var eChildComponent = gameBuilder.createComponentMarkup(oChildComponent, 1);
                    eChildren.append(eChildComponent);
                }
            }
            eComponent.append(eChildren);
            //Create an add-child button
            eComponent.append(gameBuilder.createAddItemButton().addClass('topChidLevelComponent'));
        }


        //Add config values
        eForm = $('<fieldset class="detailsform">');
        for (var iFieldIndex in oComponent.configurationFields) {
            var oField = oComponent.configurationFields[iFieldIndex];
            var eField = $('<label><span>' + oField.sCaption + '</span><div><input value="' + oField.mValue + '" name="component_data[' + oComponent.id + '][' + oField.sName + ']" data-field="' + oField.sName + '"/></div></label>');
            eForm.append(eField);
        }
        var eField = $('<label><span>Parent</span><div><input value="' + oComponent.parent_id + '" name="component_data[' + oComponent.id + '][parent_id]" data-field="parent_id"/></div></label>');
        eForm.append(eField);
        eItem[0].eForm = eForm;
        $('.game_engine_data').append(eForm);
        return eComponent;
    } catch (e) {
        alert(e);
    }
}

gameBuilder.setup();

/**
 * Add the CMS elements
 */

$(document).ready(function () {
    $('label').click(function (e) {
        if ($(this).find('div[contenteditable]'))
            return false;
    })
});