<?php

namespace LL\GameEngine;

class Pariplay extends \GameEngine {

    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType = 'instant';
    var $sDisplayPod = 'pods/play-classic';
    var $sLobbyPod = 'pods/widgets/lobby_instants';

    function customiseAdminController($oTabs) {
        /**
         * Remove the components tab, not needed for instant wins
         */
        unset($oTabs->aItems['components']);
        return $oTabs;
    }

}
