<?php

class GameEngine extends GameEngineComponent {

    static $sTableName = 'game_engines';
    static $sHandlerField = 'engine_core';
    static $bCacheLevel = 10;                 //Enable caching whenever caching is switched on

    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType = 'unknown';
    var $sDisplayPod = 'unknown';
    var $sLobbyPod = 'pods/widgets/classic';

    function getMaximumExposure() {
        return 500000;
    }

    function getOdds() {
        return 13983816;
    }

    function populateBuyerOptions() {
        
    }

    /**
     * Gets the friendly name of the handler class from the handlers table
     * Actually uses the detail from game_engine field as handler isn't used here
     * @return string Name of the handler
     */
    function getHandlerName() {
        $sSQL = "SELECT name FROM handlers WHERE class='" . mysql_escape_string($this->engine_core) . "'";
        $oHandler = \DAL::executeGetOne($sSQL);
        return $oHandler;
    }

    /**
     * Gets a game engine (product) by a URL. For the fastest
     * response only include the part of the URL from the database (i.e. classic/6aus49)
     * otherwise this method will start chopping the url by slice from the front
     * until it gets a hit or no games are found.
     * In the event of not finding a game this method throws an exception with
     * a code of LL.GameEngine.getByURL.GameEngineNotFound
     * @param string $sURL The URL of the product
     * @return GameEngine Instance of the game engine that was found
     */
    static function getByUrl($sURL) {
        $iGameEngineID = 0;
        $aURLParts = null;
        $iPasses = 0;
        $iMaxPasses = 6;

        /**
         * Strip out the querystring
         */
        $aURLParts = explode('?', $sURL);
        $sURL = $aURLParts[0];
        $aURLParts = null; //Reset to null so that it gets redone in the loop below

        while ($iGameEngineID == 0 && $sURL != '' && $iPasses < $iMaxPasses) {
            $sSQL = 'SELECT id FROM game_engines WHERE `url`=\'' . $sURL . '\'';
            $iGameEngineID = \DAL::executeGetOne($sSQL);

            if ($iGameEngineID == 0) {
                /**
                 * First time round explode the url - doing this here so that if we
                 * get a match the first time round then we don't need to bother
                 * doing the explode
                 */
                if ($aURLParts === null)
                    $aURLParts = explode('/', $sURL);
                //Remove the first array element
                array_shift($aURLParts);
                //Recombine to get a new url without the first slice
                $sURL = implode('/', $aURLParts);
            }

            ++$iPasses;
        }

        /**
         * If we still don't have a url, then we have a 404 - throw as an
         * exception which needs to picked up by the frontend
         */
        if ($iGameEngineID == 0) {
            throw new \LottoException('LL.GameEngine.GetByURL.GameEngineNotFound');
        }

        return self::getById($iGameEngineID);
    }

    /**
     * Process an order for tickets into booking items
     */
    public function processOrder($oOrder, $oBooking) {
        /**
         * Run through each order item in the order and look for any orders against this
         * game engine and pass down to the components
         */
        //foreach($oOrder->orderItems as $oOrderItem){
        //    if($oOrderItem->game_id==$this->id)
        // }
        // parent::processOrder($oOrder, $oBooking);
    }

    public function getPrice($fCurrentPrice = 0, $oOrderItem = null, $bReturnFullDetails = false) {

        $aFullDetails = array(
            'td_line1' => 'Line 1',
            'td_line2' => 'Line 2',
            'td_line3' => 'Line 3',
            'pr_line1' => 'Pricing Line 1',
            'pr_line2' => 'Pricing Line 2',
            'price1' => '0.00',
            'price2' => '0.00',
            'total' => '0.00'
        );

        /**
         * Get the price per play from the game engines
         */
        //Pass down to the children
        $fTicketPrice = 0;

        if (is_array($oOrderItem)) {
            $oNew = new stdClass();
            $oNew->data = $oOrderItem['data'];
            $oOrderItem = $oNew;
        }

        $oOrderItem->iTotalLines = 0;
        foreach ($oOrderItem->data['boards'] as $iBoardNumber => $board) {
            if (empty($board['numbers'])) {
                continue;
            }
            /**
             * Fixed Ticket #334
             * This was fixed for standard plI've ay by taking out the operation to mulitply
             * each price calculation by the number of lines per board. However this
             * broke system because it was no longer taking into account the number
             * of boards. Changed the code to use the number of lines from the board instead
             */
            $oOrderItem->data['boards'][$iBoardNumber]['iLinesOnBoard'] = $this->getTicketsPerBoard($board);
            $oOrderItem->iTotalLines+=$oOrderItem->data['boards'][$iBoardNumber]['iLinesOnBoard'];
        }


        $oOrderItem->fExtrasPrice = 0;
        $aChildComponents = $this->getChildComponents(true);
        if (is_array($aChildComponents)) {
            foreach ($aChildComponents as $oChildComponent) {
                $fCurrentPrice = $oChildComponent->getPrice($fCurrentPrice, $oOrderItem);
            }
        }

        $fPricePerTicket = $oOrderItem->fTicketPrice;


        // Work out how many tickets need to be purchased
        $iTicketsPerBoard = $this->getTicketsPerBoard($board);

        $boards = 0;



        $weeks = $oOrderItem->data['playDuration'];
        $days = (sizeof($oOrderItem->data['day']));
        $iTotalTickets = $oOrderItem->iTotalLines * $weeks * $days;



        foreach ($oOrderItem->data['day'] as $iDayNumber) {
            $aDaysList[] = VCMS::get('general.dayofweek' . $iDayNumber);
        }


        //  getAvailableDates

        $aProductLists = $oOrderItem->aProductLists;
        $aFullDetails['total'] = $fCurrentPrice;

        $aFullDetails['td_line1'] = '<strong>' . VCMS::get('BuySummary.TicketDetails') . '</strong> ';
        if ($oOrderItem->iTotalLines > 0) {
            if ($oOrderItem->iTotalLines != 1) {
                $aFullDetails['td_line1'] = VCMS::get('BuySummary.Product_' . $this->id . '.Line1[Lines]', array('Lines' => $oOrderItem->iTotalLines));
            } else {
                $aFullDetails['td_line1'] = VCMS::get('BuySummary.Product_' . $this->id . '.Line1[Lines]Single', array('Lines' => $oOrderItem->iTotalLines));
            }
        }
        $aFullDetails['td_line2'] = implode(', ', $aProductLists) . ((sizeof($aProductLists) > 0) ? ' | ' : '') . implode('/', $aDaysList);

        /**
         * Subscription Info
         */
        if ($oOrderItem->data['isSubscription'] == 'true') {
            if ($weeks != 1) {
                $aFullDetails['td_line3'] = VCMS::get('BuySummary.Subscription[Weeks]', array('Weeks' => $weeks));
            } else {
                $aFullDetails['td_line3'] = VCMS::get('BuySummary.Subscription[Weeks]Single', array('Weeks' => $weeks));
            }
        } else {
            $aFullDetails['td_line3'] = VCMS::get('BuySummary.NoSubscription');
        }

        /**
         * Add the start date
         */
        $aChildren = $this->getChildComponents();
        $oFirstLotto = $this->aChildComponents[0];
        $aDates = $oFirstLotto->getDrawDates(date('Y-m-d'), 4);
        $sStartDate = '';
        foreach ($aDates as $aDate) {
            if (date('w', strtotime($aDate['drawdate'])) == $oOrderItem->data['day'][0] && $sStartDate == '') {
                $sStartDate = date('jS F', strtotime($aDate['drawdate']));
            }
        }
        if ($sStartDate == '') {
            $aFullDetails['td_line3'].=' ' . VCMS::get('BuySummary.Starting{Date}', array('Date' => $sStartDate));
        } else {
            $aFullDetails['td_line3'].=' ' . VCMS::get('BuySummary.Starting Next Available Draw');
        }

        /**
         * Calculate discount
         */
        $discount = 0;
        if ($weeks >= 2)
            $discount = 2;
        if ($weeks >= 4)
            $discount = 3.5;
        if ($weeks >= 13)
            $discount = 4.69;
        if ($weeks >= 26)
            $discount = 6.49;
        if ($weeks >= 52)
            $discount = 9.46;

        //Check the member discount if logged in
        $oMember = Member::getLoggedInMember();
        if ($oMember) {
            $discount = $oMember->getMemberDiscount($discount);
        }

        $fDiscount = round(($fCurrentPrice / 100) * $discount, 2);

        $fFullPrice = $fCurrentPrice;
        $fCurrentPrice-=$fDiscount;


        $fPricePerTicket = number_format_locale($oOrderItem->fTicketPrice, 2);
        $fDiscount = number_format_locale($fDiscount, 2);
        $fFullPrice = number_format_locale($fFullPrice, 2);

        if ($oOrderItem->fExtrasPrice > 0) {
            $fExtrasPrice = number_format($oOrderItem->fExtrasPrice, 2);
            $sExtras = VCMS::get('BuySummary.TicketSummaryPricingExtras[Price]', array('Price' => $fExtrasPrice));
        }

        if ($weeks < 2) {
            $aFullDetails['pr_line1'] = VCMS::get('BuySummary.SingleDrawingTicketSummary[Tickets]x[Price]', array('Tickets' => $iTotalTickets, 'Price' => $fPricePerTicket)) . $sExtras;
            $aFullDetails['pr_line2'] = VCMS::get('BuySummary.SingleDrawingDiscountSummary[DiscountPercent]', array('DiscountPercent' => $discount));
        } else {
            $aFullDetails['pr_line1'] = VCMS::get('BuySummary.MultiDrawingTicketSummary', array('Tickets' => $iTotalTickets, 'Price' => $fPricePerTicket)) . $sExtras;
            $aFullDetails['pr_line2'] = VCMS::get('BuySummary.MultiDrawingDiscountSummary[DiscountPercent]', array('DiscountPercent' => $discount));
        }

        /**
         * Booking fee
         */
        $iBookingFees = ceil($iTotalTickets / 6);
        $fBookingFee = number_format(0.5, 2);

        //Check the member discount if logged in
        if ($oMember) {
            $fBookingFee = $oMember->getTicketFee($fBookingFee);
        }


        $aFullDetails['total_line2'] = VCMS::get('BuySummary.Total[BookingFee]', array('BookingFee' => number_format_locale($fBookingFee)));

        $aFullDetails['price1'] = $fFullPrice;
        $aFullDetails['price2'] = $fDiscount;
        $aFullDetails['total'] = $fCurrentPrice + $fBookingFee;
        $aFullDetails['total_formatted'] = number_format($aFullDetails['total'], 2);

        return ($bReturnFullDetails) ? $aFullDetails : $fCurrentPrice;
    }

    function getTicketsPerBoard($board) {
        return 1;
    }

    /**
     * Override of default _getPrice method because the main game engine itself
     * gets it's price from a database field (base_price), not a configuration field
     *
     */
    protected function _getPrice($fCurrentPrice) {
        $fComponentPrice = $this->base_price;
        if ($fComponentPrice <> 0) {
            $fCurrentPrice+=$fComponentPrice;
        }
        return $fCurrentPrice;
    }

    /**
     * Override of get child components as instead of reading from child components
     * the game engine itself needs to find all the top level game components
     *
     * @param type $bForceReload
     * @return \sHandler
     */
    public function getChildComponents($bForceReload = false) {
        if ($bForceReload) {
            $this->aChildComponents = null;
        }
        if ($this->aChildComponents === null) {
            $sSql = 'SELECT * FROM game_engine_components WHERE fk_parent_id is null AND fk_game_engine_id=' . $this->id;
            $aChildren = DAL::executeQuery($sSql);
            foreach ($aChildren as $aChild) {
                $sHandler = $aChild['handler'];
                $oNewChild = new $sHandler;
                $oNewChild->populateFromArray($aChild);
                $aAllChildren[] = $oNewChild;
            }
            $this->aChildComponents = $aAllChildren;
        }
        return $this->aChildComponents;
    }

    public function getCommonModifiers() {
        $sSql = 'SELECT * FROM game_engine_components WHERE is_lottery=2 AND fk_game_engine_id=' . $this->id;
        $aChildren = DAL::executeQuery($sSql);
        foreach ($aChildren as $aChild) {
            $sHandler = $aChild['handler'];
            $oNewChild = new $sHandler;
            $oNewChild->populateFromArray($aChild);
            /**
             * Pull in the configuration from the json encoded config string
             */
            $oConfigData = json_decode($aChild['config']);
            foreach ($oNewChild->aConfigurationFields as $sConfigurationField => $oConfigurationField) {
                $oNewChild->configurationFields[$sConfigurationField]->mValue = $oConfigData->$sConfigurationField;
            }
            $aAllChildren[] = $oNewChild;
        }
        return $aAllChildren;
    }

    function getDump() {
        $oOut = new stdClass();
        $oOut->id = $this->id;
        $oOut->name = $this->name;
        $oOut->is_active = $this->is_active;
        $oOut->configurationFields = $this->getGameConfigurationFields();
        $oOut->childComponents = $this->getDumpOfChildren($this->getChildComponents());
        $oOut->commonModifiers = $this->getDumpOfChildren($this->getCommonModifiers());
        return $oOut;
    }

    function save($aData = array()) {
        if ($this->fk_parent_id == 0)
            $this->fk_parent_id = null;
        $aData = array();
        $aData['name'] = $this->name;
        $aData['image_envelope'] = $this->image_envelope;
        $aData['image_envelope_mobile'] = $this->image_envelope_mobile;
        $aData['image_lobby'] = $this->image_lobby;
        $aData['image_widget'] = $this->image_widget;
        $aData['config'] = $this->config;

        LLModel::save($aData);
    }

    function customiseAdminController($oTabs) {
        return $oTabs;
    }

}
