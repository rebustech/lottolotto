<?php

namespace LL\GameEngine;

class StandardPlay extends \GameEngine {

    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType = 'classic';
    var $sDisplayPod = 'pods/play-classic';
    var $sLobbyPod = 'pods/widgets/lobby_classic';

    /**
     * Simulates a game play for testing purposes.
     */
    function simulatePlay() {
        $oGameData = array();
        $oGameData['gameID'] = $this->id;
        $oGameData['gameType'] = 'classic';
        $oGameData['playDuration'] = rand(1, 12);
        $aChildComponents = $this->getChildComponents();
        $oGameData['day'] = $aChildComponents[0]->aLottoDayNumbers;
        $numbers = $aChildComponents[0]->getNumbersCount();
        $iBonusNumbers = $aChildComponents[0]->getBonusNumbers();
        $oLottery = $aChildComponents[0];
        $iMaxBonus = $oLottery->getMaxBonusNumber();
        //Special case for 6aus49
        if ($oLottery->iLotteryID == 11) {
            $iBonusNumbers = 1;
            $iMaxBonus = 9;
        }

        //Random number of boards
        $oGameData['boards'] = array();
        $iNumberOfBoards = rand(1, 12);
        for ($a = 1; $a <= $oLottery->getMaxNumber(); $a++)
            $aNumbers[] = $a;
        for ($a = 1; $a <= $iMaxBonus; $a++)
            $aBonusNumbers[] = $a;
        //for($a=1;$a<=$iBonusMaxNumber;$a++) $aBonusNumbers[]=$a;
        for ($iBoardNumber = 1; $iBoardNumber < $iNumberOfBoards; $iBoardNumber++) {
            $aRandoms = array_rand($aNumbers, $numbers);
            foreach ($aRandoms as $k) {
                $oGameData['boards'][$iBoardNumber]['numbers'][] = $aNumbers[$k];
            }
            if ($iBonusNumbers > 0) {
                $aRandoms = array_rand($aBonusNumbers, $iBonusNumbers);
                if (is_array($aRandoms)) {
                    foreach ($aRandoms as $k) {
                        $oGameData['boards'][$iBoardNumber]['numbers'][] = $aBonusNumbers[$k];
                    }
                } else {
                    $oGameData['boards'][$iBoardNumber]['numbers'][] = $aBonusNumbers[$aRandoms];
                }
            }
        }
        $oGameData['boards'][$iBoardNumber]['type'] = 7;
        $oGameData['games'] = array();
        $oGameData['isSubscription'] = (rand(0, 10) < 5) ? true : false;
        $oGameData['protectJackpot'] = false;
        $oGameData['quickPlay'] = false;
        return $oGameData;
    }

    /**
     * Process an order for tickets into booking items
     */
    public function _processOrderItem($oOrderItem, $iBookingID) {
        $aDescendants = $this->getAllDescendants();
        foreach ($aDescendants as $oDescendant) {
            $this->_processLottery($oOrderItem, $iBookingID, $oDescendant);
        }
    }

    public function _processLottery($oOrderItem, $iBookingID, $oLottery) {

        /**
         * Get an array of the next available draw dates so we can book the tickets
         * Multiplied by the number of draws per week
         */
        $iDrawsPerWeek = sizeof($oLottery->aLottoDayNumbers);
        $aDrawDates = $oLottery->getDrawDates(date('Y-m-d'), intval($oOrderItem->data['playDuration']) * $iDrawsPerWeek);

        if (!is_array($oOrderItem->data['day'])) {
            $oOrderItem->data['day'] = explode(',', $oOrderItem->data['day']);
        }



        foreach ($aDrawDates as $aDrawDate) {

            //Check the day to make sure it's one the user wanted
            $sDay = date('N', strtotime($aDrawDate['drawdate']));
            if (in_array($sDay, $oOrderItem->data['day'])) {
                if ($oOrderItem->data['start_date'] == '') {
                    $oOrderItem->data['start_date'] = date('Y-m-d', strtotime($aDrawDate['drawdate']));
                }
                $oOrderItem->data['end_date'] = date('Y-m-d', strtotime($aDrawDate['drawdate']));
                foreach ($oOrderItem->data['boards'] as $oBoard) {
                    if (is_array($oBoard['extras'])) {
                        $aNumbers = array_merge($oBoard['numbers'], $oBoard['extras']);
                    } else {
                        $aNumbers = $oBoard['numbers'];
                    }
                    $oLottery->bookTicket($aNumbers, $aDrawDate, $iBookingID, $oOrderItem);
                }
            }
        }
    }

}
