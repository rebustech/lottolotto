<?php

namespace LL\GameEngine;

class Syndicate extends \GameEngine {

    use \tSyndicate;

    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType = 'syndicate';
    var $sDisplayPod = 'pods/play-syndicate';
    var $sLobbyPod = 'pods/widgets/lobby_classic';

    function _getGameConfigurationFields() {
        $aGameConfigurationFields = parent::_getGameConfigurationFields();
        $aGameConfigurationFields['iTotalShares'] = new \ConfigurationNumber('iTotalShares', 'Number of Shares');
        $aGameConfigurationFields['iMinOpenSyndicates'] = new \ConfigurationNumber('iMinOpenSyndicates', 'Minimum open syndicates');
        $aGameConfigurationFields['iMinNumberOfSyndicates'] = new \ConfigurationNumber('iMinNumberOfSyndicates', 'Minimum total syndicates');
        $aGameConfigurationFields['iLowAvailabilityThreshold'] = new \ConfigurationNumber('iLowAvailabilityThreshold', 'Low availability threshold');
        $aGameConfigurationFields['iSoldOutThreshold'] = new \ConfigurationNumber('iSoldOutThreshold', 'Sold out threshold');
        return $aGameConfigurationFields;
    }

    /**
     * Just returns the base price by the number of weeks
     */
    function getPrice($fCurrentPrice = 0, $oOrderItem = null, $bReturnFullDetails = false) {
        $aFullDetails = array(
            'td_line1' => $this->name,
            'td_line2' => $iShares . ' shares',
            'td_line3' => 'Numbers Here?',
            'pr_line1' => 'P1',
            'pr_line2' => 'P2',
            'price1' => '0.00',
            'price2' => '0.00',
            'total' => '0.00'
        );

        $iTotalSyndicates = 0;
        $iTotalShares = 0;
        foreach ($oOrderItem['data']['syndicates'] as $syndicate) {
            $iShares = intval($syndicate['shares']);
            $iSyndicateId = intval($syndicate['syndicateId']);
            $sSyndicateDetails = \DAL::executeGetOne('SELECT original_numbers FROM syndicate_lotteries WHERE syndicate_id=' . $iSyndicateId . ' LIMIT 0,1');
            $aFullDetails['td_line3'] = str_replace('|', ',', $sSyndicateDetails);

            if ($iShares < 1)
                $iShares = 1;
            $iDraws = intval($syndicate['playDuration']);
            $fCurrentPrice = $fCurrentPrice + (round(floatval($this->base_price), 2)) * $iDraws * $iShares * 2;
            $iTotalShares+=$iShares;
            $iTotalSyndicates++;
        }
        $aFullDetails['td_line2'] = $oOrderItem['data']['playDuration'] . ' Weeks';

        $fFullPrice = $fCurrentPrice;


        $aFullDetails['price1'] = $fFullPrice;
        $aFullDetails['price2'] = 0;
        $aFullDetails['total'] = $fFullPrice;
        $aFullDetails['total_formatted'] = number_format_locale($aFullDetails['total'], 2);

        /**
         * Calculate discount
         */
        $discount = 0;
        if ($iDraws >= 2)
            $discount = 2;
        if ($iDraws >= 4)
            $discount = 3.5;
        if ($iDraws >= 13)
            $discount = 4.69;
        if ($iDraws >= 26)
            $discount = 6.49;
        if ($iDraws >= 52)
            $discount = 9.46;

        //Check the member discount if logged in
        $oMember = \Member::getLoggedInMember();
        if ($oMember) {
            $discount = $oMember->getMemberDiscount($discount);
        }

        $fDiscount = round(($fFullPrice / 100) * $discount, 2);

        $fCurrentPrice-=$fDiscount;


        $fPricePerTicket = $this->base_price;
        ;
        $fDiscount = round($fDiscount, 2);
        $fFullPrice = round($fFullPrice, 2);

        /**
         * Booking fee
         */
        $fBookingFee = number_format(0.5, 2);
        //Check the member discount if logged in
        if ($oMember) {
            $fBookingFee = $oMember->getTicketFee($fBookingFee);
        }

        //$aFullDetails['total_line2']=VCMS::get('BuySummary.Total[BookingFee]',array('BookingFee'=>number_format_locale($fBookingFee)));

        $aFullDetails['price1'] = $fFullPrice;
        $aFullDetails['price2'] = $fDiscount;
        $aFullDetails['total'] = $fCurrentPrice + $fBookingFee;
        $aFullDetails['total_formatted'] = number_format($aFullDetails['total'], 2);

        $aFullDetails['pr_line1'] = \VCMS::get('BuySummary.[Shares]in[Syndicates]', array('Shares' => $iTotalShares, 'Syndicates' => $iTotalSyndicates));
        if ($iDraws < 2) {
            $aFullDetails['pr_line2'] = \VCMS::get('BuySummary.SingleDrawingDiscountSummary[DiscountPercent]', array('DiscountPercent' => $discount));
        } else {
            $aFullDetails['pr_line2'] = \VCMS::get('BuySummary.MultiDrawingDiscountSummary[DiscountPercent]', array('DiscountPercent' => $discount));
        }


        return ($bReturnFullDetails) ? $aFullDetails : $fCurrentPrice;
    }

    /**
     * Instead of buying tickets we book shares
     */
    function _processOrderItem($oOrderItem, $iBookingID) {
        foreach ($oOrderItem->data['syndicates'] as $oSyndicateOrder) {
            $iSyndicateId = $oSyndicateOrder['syndicateId'];
            $iNumShares = $oSyndicateOrder['shares'];
            $iWeeks = $oOrderItem->data['playDuration'];
            $iMemberId = $oOrderItem->oOrder->oMember->getMemberID();
            $sEndDate = date('Y-m-d H:i:s', strtotime('+ ' . ($iWeeks * 7) . ' days'));
            $this->purchaseSyndicateShares($iSyndicateId, $iMemberId, $iNumShares, $sEndDate);
        }
    }

    function save($aData = array()) {
        parent::save();
        $this->checkSyndicateSpaces();
    }

    function customiseAdminController($aTabs) {
        /**
         * Add a tab to be able to view the syndicates for this GE
         */
        $oSyndicateTab = new \TabbedInterface(\lang::get('syndicates'));
        $oSyndicateTab->sIcon = 'fa-list-alt';
        $aItemsData = \DAL::executeQuery('SELECT s.start_date,s.current_shares,s.max_shares, (s.current_shares/s.max_shares)*100 as percent, o.comment, l.original_numbers,l.bonus_numbers,l.num_lines,l.num_generated
                                        FROM syndicates s
                                        INNER JOIN syndicate_lotteries l ON l.syndicate_id=s.id
                                        INNER JOIN lotteries_cmn o ON o.lottery_id=l.lottery_id
                                        WHERE s.game_engine_id=' . $this->id . ' ORDER BY s.id DESC LIMIT 0,50');
        $oItemsTable = new \TableControl($aItemsData);
        $oItemsTable->getFieldsFromTable('syndicates');
        $oItemsTable->getFieldsFromTable('syndicate_lotteries');
        $oItemsTable->getFieldsFromTable('lotteries_cmn');
        $oItemsTable->keepFields('syndicates', 'start_date,current_shares,max_shares');
        $oItemsTable->keepFields('syndicate_lotteries', 'original_numbers,bonus_numbers,num_generated,num_lines');
        $oItemsTable->keepFields('lotteries_cmn', 'comment');
        $oItemsTable->addField(new \TableField('percent', 'spark_bar'));
        $oItemsTable->renderAs('syndicate_lotteries', 'original_numbers', 'lottoballs');
        $oItemsTable->renderAs('syndicate_lotteries', 'bonus_numbers', 'lottoballs');
        $oSyndicateTab->AddControl($oItemsTable);

        $aTabs->aItems['syndicates'] = $oSyndicateTab;

        return $aTabs;
    }

}
