<?php

namespace LL\GameEngine;

class SystemPlay extends \GameEngine {

    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType = 'system';
    var $sDisplayPod = 'pods/play-system';
    var $sLobbyPod = 'pods/widgets/lobby_system';

    // $aSystemRules has now been moved out into Lottery.class.php

    function getTicketsPerBoard($board) {
        $aDescendants = $this->getAllDescendants();
        foreach ($aDescendants as $oDescendant) {
            if (is_array($oDescendant->aSystemRules)) {
                return $oDescendant->aSystemRules[$board['type']];
            }
        }
    }

    /**
     * Process an order for tickets into booking items
     */
    public function _processOrderItem($oOrderItem, $iBookingID) {
        $aDescendants = $this->getAllDescendants();
        foreach ($aDescendants as $oDescendant) {
            $this->_processLottery($oOrderItem, $iBookingID, $oDescendant);
        }
    }

    function array_random_assoc($arr, $num = 1) {
        $keys = array_keys($arr);
        shuffle($keys);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[$keys[$i]] = $arr[$keys[$i]];
        }
        return $r;
    }

    public function _processLottery($oOrderItem, $iBookingID, $oLottery) {

        /**
         * Get an array of the next available draw dates so we can book the tickets
         * Multiplied by the number of draws per week
         */
        $iDrawsPerWeek = sizeof($oLottery->aLottoDayNumbers);
        $aDrawDates = $oLottery->getDrawDates(date('Y-m-d'), intval($oOrderItem->data['playDuration']) * $iDrawsPerWeek);

        if (!is_array($oOrderItem->data['day'])) {
            $oOrderItem->data['day'] = explode(',', $oOrderItem->data['day']);
        }

        foreach ($oOrderItem->data['boards'] as $oBoard) {
            if (!is_array($oBoard['extras']))
                $oBoard['extras'] = array();
            $aAllSets = new \Combinations($oBoard['numbers'], $oLottery->getNumbersCount());
            /**
             * PART SYSTEM
             * For part system we now have to pick at random from that array
             */
            if (substr_count($oBoard['type'], '/') > 0) {
                $iNumberToPick = $this->aSystemRules[$oBoard['type']];
                //Convert allsets to an actual array
                foreach ($aAllSets as $aSetItem) {
                    $aAllSets2[] = $aSetItem;
                }
                //Then pick a random selection
                $aAllSets = $this->array_random_assoc($aAllSets2, $iNumberToPick);
            }

            foreach ($aDrawDates as $aDrawDate) {

                //Check the day to make sure it's one the user wanted
                $sDay = date('N', strtotime($aDrawDate['drawdate']));
                if (in_array($sDay, $oOrderItem->data['day'])) {
                    if ($oOrderItem->data['start_date'] == '') {
                        $oOrderItem->data['start_date'] = date('Y-m-d', strtotime($aDrawDate['drawdate']));
                    }
                    $oOrderItem->data['end_date'] = date('Y-m-d', strtotime($aDrawDate['drawdate']));

                    foreach ($aAllSets as $aSet) {
                        $oLottery->bookTicket(array_merge($aSet, $oBoard['extras']), $aDrawDate, $iBookingID, $oOrderItem);
                    }
                }
            }
        }
    }

}
