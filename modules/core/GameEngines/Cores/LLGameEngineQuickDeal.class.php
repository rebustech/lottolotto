<?php

namespace LL\GameEngine;

/**
 * QuickDeal game engine
 *
 * The Quickdeal game engine allows administrators to create prebuilt products including
 * standard play and individual combos.
 */
class QuickDeal extends \LL\GameEngine\StandardPlay{
    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType='instant';
    var $sDisplayPod='pods/play-classic';
    var $sLobbyPod='pods/widgets/lobby_quickpick';

    /**
     * Pricing on quickdeals is set at the product level rather than the
     * component level
     * @param float $fCurrentPrice
     * @return array
     */
    function getPrice($fCurrentPrice=0,$oOrderItem=null,$bReturnFullDetails=false){
        $fFullPrice=$fCurrentPrice+=$this->base_price;

        $aFullDetails=array(
            'td_line1'=>'',
            'td_line2'=>'',
            'td_line3'=>'',
            'pr_line1'=>'',
            'pr_line2'=>'',
            'price1'=>'0.00',
            'price2'=>'0.00',
            'total'=>'0.00'
        );

        $aFullDetails['price1']=$fFullPrice;
        $aFullDetails['price2']=0;
        $aFullDetails['total']=$fFullPrice;
        $aFullDetails['total_formatted']=number_format_locale($aFullDetails['total'],2);

        return ($bReturnFullDetails)?$aFullDetails:$fCurrentPrice;

    }


    /**
     * Process an order for tickets into booking items
     */
    public function _processOrderItem($oOrderItem,$iBookingID){
        /**
         * Run through each game component and purchase the configured number of tickets through
         * that component
         *
         * Bits that we're going to need to assemble are:
         *
         * Number of lines
         * Days of week
         * Subscription enabled
         *
         * All of these come from each of the components
         */

        $aDescendants=$this->getAllDescendants();
        foreach($aDescendants as $oDescendant){
            if($oDescendant instanceof \Lottery){
                $oDescendant->processQuickDeal($oOrderItem, $iBookingID);
            }
        }
    }

    /**
     * Gets any special configuration fields for this game engine
     * @return Array
     */
    function _getConfigurationFields(){
        $aConfigurationFields=parent::_getGameConfigurationFields();
        $aConfigurationFields['bSubscription']=new ConfigurationSwitch('bSubscription','Auto Renew?');
        return $aConfigurationFields;
    }

}
