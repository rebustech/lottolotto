<?php

/**
 * Get the scripts needed for the game builder
 */
class LLGameEnginesAssetsApi extends \LL\AjaxController {

    function gameBuilderScripts() {
        header('Content-type: text/javascript');
        $aFiles = array(__DIR__ . '/../Scripts/game_builder.js');
        die($this->_getFiles($aFiles));
    }

}
