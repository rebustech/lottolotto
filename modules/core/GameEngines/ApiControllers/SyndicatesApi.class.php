<?php

class SyndicatesApi extends \LL\AjaxController {

    public function getFrontendSyndicates() {
        $iGameEngineId = filter_input(INPUT_GET, 'iGameEngineID', FILTER_SANITIZE_NUMBER_INT);
        if ($iGameEngineId > 0) {
            $oGameEngine = GameEngine::getById($iGameEngineId);
            if ($oGameEngine->id == $iGameEngineId) {
                $this->aSyndicates = $oGameEngine->getFrontEndSyndicates();
            } else {
                $this->error = 'Game engine ' . $iGameEngineId . ' not found';
            }
        } else {
            $this->error = 'iGameEngineID not supplied or not a valid integer';
        }
        $this->output();
    }

    public function lines() {
        $aSegments = explode('/', $_SERVER['REQUEST_URI']);
        $iSyndicateId = end($aSegments);
        if (!is_numeric($iSyndicateId))
            throw new LottoException('Syndicate ID not supplied');

        $s = new GenericSyndicate();
        $aData = array('lines' => $s->getSyndicateLines($iSyndicateId));

        $this->output($aData);
    }

}
