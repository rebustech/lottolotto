<?php

/**
 * Handle all Game Engine API interactions.
 *
 * @author    Lewis Theobald
 * @version	1.0
 */
class EngineApi extends \LL\AjaxController {

    /**
     * Return a random number list
     *
     * @access public
     */
    public function random() {
        if ($_GET['is_for_cache'] == 1) {
            $is_for_cache = true;
            $numbers = $_GET['numbers'];
            $min = $_GET['min'];
            $max = $_GET['max'];
            $bonusnumbers = $_GET['bonusnumbers'];
            $bonusmin = $_GET['bonusmin'];
            $bonusmax = $_GET['bonusmax'];
            $outputversion = ($_GET['outputversion']) ? $_GET['outputversion'] : 1;
        } else {
            $is_for_cache = false;
            $numbers = $_POST['numbers'];
            $min = $_POST['min'];
            $max = $_POST['max'];
            $bonusnumbers = $_POST['bonusnumbers'];
            $bonusmin = $_POST['bonusmin'];
            $bonusmax = $_POST['bonusmax'];
            $outputversion = ($_POST['outputversion']) ? $_POST['outputversion'] : 1;
        }

        $response = LL\RNG::random($numbers, $min, $max, $bonusnumbers, $bonusmin, $bonusmax, $is_for_cache);
        # Output and leave
        echo json_encode($response);
        exit;


        # Create a client
        $client = new JsonRPCClient('https://api.random.org/json-rpc/1/invoke', 5, true);

        # Test our limits
        $check = $client->execute('getUsage', array(
            "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f"
                )
        );

        # Passed?
        if ($check['requestsLeft'] > 0) {

            # Response object
            $response = new stdClass();

            # Setup what we need
            $apiData = array(
                "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f",
                "n" => $numbers,
                "min" => $min,
                "max" => $max,
                "replacement" => false
            );

            # Process the request
            if ($outputversion > 1) {
                $response->main = $client->execute('generateIntegers', $apiData);
            } else {
                $response = $client->execute('generateIntegers', $apiData);
            }

            if ($bonusmax > 0) {

                # Setup what we need
                $apiData = array(
                    "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f",
                    "n" => $bonusnumbers,
                    "min" => $bonusmin,
                    "max" => $bonusmax,
                    "replacement" => false
                );

                # Process the request
                $response->bonus = $client->execute('generateIntegers', $apiData);
            }
        } else {

            function randomNumberRange($min, $max, $quantity) {
                $numbers = range($min, $max);
                shuffle($numbers);
                return array_slice($numbers, 0, $quantity);
            }

            # Main numbers
            $response->main = array(
                'random' => array(
                    'data' => randomNumberRange($min, $max, $numbers)
                )
            );

            # Bonus numbers
            if ($bonusmax > 0) {
                $response->bonus = array(
                    'random' => array(
                        'data' => randomNumberRange($bonusmin, $bonusmax, $bonusnumbers)
                    )
                );
            }
        }

        # Output and leave
        echo json_encode($response);
        exit;
    }

    /**
     * details: Given an ID, query the details of a game engine
     *
     * @param $ID int
     */
    public function details() {
        $response = array(
            'error' => true,
            'errorText' => 'Invalid request.'
        );

        # No game ID
        if (empty($_GET['game'])) {
            echo json_encode($response);
            exit;
        }

        /**
         * Pass the data to the game engine components
         */
        $oGameEngine = GameEngine::getById($_GET['game']);

        # Create an order item
        $orderItem = array(
            'data' => $_POST
        );

        # Clean some out
        # $orderItem['data']['dayString'] = $orderItem['data']['day'];
        # Get all days
        # @todo, replace JS notation
        # $orderItem['data']['dayString'] = str_replace(array('mi', 'sa'), array('Monday', 'Saturday'), $orderItem['data']['dayString']);
        # Into an array
        $orderItem['data']['day'] = explode(',', $orderItem['data']['day']);

        # Get the lottery
        $aChildComponents = $oGameEngine->getChildComponents();
        $mlLottery = $aChildComponents[0];

        # Get the data, feedback loop
        $engineData = array();
        $engineData['pricinginfo'] = $oGameEngine->getPrice(0, $orderItem, true);
        $engineData['pricinginfo']['price1Formatted'] = VCMS::get('Results.BreakdownAmount[Amount]Currency[Currency]', array('Currency' => '&euro;', 'Amount' => number_format_locale($engineData['pricinginfo']['price1'], 2)));
        $engineData['pricinginfo']['price2Formatted'] = VCMS::get('Results.BreakdownAmount[Amount]Currency[Currency]', array('Currency' => '&euro;', 'Amount' => '-' . number_format_locale($engineData['pricinginfo']['price2'], 2)));
        $engineData['price'] = $engineData['pricinginfo']['total'];
        $engineData['priceFormatted'] = '&euro;' . number_format_locale($engineData['price'], 2);

        # Game details
        $engineData['lottery'] = $mlLottery;
        $engineData['name'] = $oGameEngine->name;
        $engineData['id'] = $oGameEngine->id;
        $engineData['className'] = $oGameEngine->classname;
        $engineData['gameType'] = $oGameEngine->sGameType;

        # Next draw date
        $aDrawData = $mlLottery->getDrawDates(date('Y-m-d'), 1);
        $datetime1 = date_create(date('Y-m-d H:i:s'));
        $datetime2 = date_create($aDrawData[0]['drawdate']);
        $interval = date_diff($datetime1, $datetime2);

        # Draws per week
        $aWeeks = array(2, 4, 13, 26, 52);
        $iDrawsPerWeek = sizeof($mlLottery->getLottoDays());

        # Next draw date
        $response['draw'] = array();
        $response['draw']['daysLeft'] = str_pad($interval->d, 2, '0', STR_PAD_LEFT);
        $response['draw']['hours'] = str_pad($interval->h, 2, '0', STR_PAD_LEFT);
        $response['draw']['minutes'] = str_pad($interval->i, 2, '0', STR_PAD_LEFT);
        $response['draw']['seconds'] = str_pad($interval->s, 2, '0', STR_PAD_LEFT);
        $response['draw']['text'] = VCMS::get('PlayPages.DrawDate[Day]', array('Day' => date('l', strtotime($aDrawData[0]['drawdate']))));
        $response['draw']['jackpot'] = VCMS::get('PlayPages.Jackpot[Amount]', array('Amount' => round($aDrawData[0]['jackpot'] / 1000000, 2)));
        $response['draw']['rollover'] = $aDrawData[0]['rollover'];
        $response['draw']['days'] = implode(',', $mlLottery->aLottoDayNumbers);

        # Countdown
        $response['html'] = array();
        $response['html']['countdown'] = ' <p class="drawDate"><i class="fa fa-clock-o"></i>
			<span id="countdownDays" class="countdownNumber"><span>' . $response['draw']['daysLeft'] . '</span></span><span> <span>' . VCMS::get('Countdown.Days') . '</span></span>
			<span id="countdownHours" class="countdownNumber"><span>' . substr($response['draw']['hours'], 0, 1) . '</span><span>' . substr($response['draw']['hours'], 1, 1) . '</span></span><span> <span>' . VCMS::get('Countdown.Hours') . '</span></span>
			<span id="countdownMins" class="countdownNumber"><span>' . substr($response['draw']['minutes'], 0, 1) . '</span><span>' . substr($response['draw']['minutes'], 1, 1) . '</span></span><span> <span>' . VCMS::get('Countdown.Mins') . '</span></span>
			<span id="countdownSecs" class="countdownNumber"><span>' . substr($response['draw']['seconds'], 0, 1) . '</span><span>' . substr($response['draw']['seconds'], 1, 1) . '</span></span><span> <span>' . VCMS::get('Countdown.Secs') . '</span></span>
		</p>';

        # How many weeks /draws
        $response['html']['weeks'] = '<option value="1">' . VCMS::get('GameOptions.1Week[Draws]Draws', array('Draws' => $iDrawsPerWeek)) . '</option>';

        # Append to the select list
        foreach ($aWeeks as $iWeek) {
            $response['html']['weeks'] .= '<option value="' . $iWeek . '">' . VCMS::get('GameOptions.[Weeks]Weeks[Draws]Draws', array('Weeks' => $iWeek, 'Draws' => $iDrawsPerWeek * $iWeek)) . '</option>';
        }

        # Add into response
        $response['engine'] = $engineData;

        # No error
        $response['error'] = false;

        # Remove the error text
        unset($response['errorText']);

        # Quit the request
        echo json_encode($response);
        exit;
    }

    /**
     * basket: Add the current game into the basket
     *
     */
    public function basket($parameters = array()) {

        if (isset($parameters[0]) && is_bool($parameters[0])) {
            $bDieWithJson = $parameters[0];
        } else {
            $bDieWithJson = true;
        }

        $response = array(
            'error' => true,
            'errorText' => 'Invalid request.'
        );

        # Re-open the session
        ini_set('session.use_only_cookies', false);
        ini_set('session.use_cookies', false);
        ini_set('session.use_trans_sid', false);
        ini_set('session.cache_limiter', null);

        # Open a session
        session_start();

        # Get the data, feedback loop
        # $engineData = $oGameEngine->getDump();
        # Create a cart instance
        $oCart = $this->setupCart();

        # Read from cart
        if ($_POST['action'] !== 'remove') {

            /**
             * Pass the data to the game engine components
             */
            $oGameEngine = GameEngine::getById($_POST['gameID']);

            /**
             * We will only ever have 1 top level lottery component
             * Use that to get this information
             */
            $aChildComponents = $oGameEngine->getChildComponents();
            $mlLottery = $aChildComponents[0];
        }

        # No type of action, so quit
        if (!empty($_POST['action'])) {

            # Add to basket
            if ($_POST['action'] == 'remove') {

                # Should pass in the index
                $oCart->removeFromCart($_POST['item']);
            } elseif ($_POST['action'] == 'addToCart') {

                # Are we editing a betslip?
                if ($_POST['editngBetslipItem']) {

                    # Remove the current one
                    $oCart->removeFromCart($_POST['editingBetslipIndex']);

                    # Make sure they don't store it
                    unset($_POST['editingBetslipItem']);
                    unset($_POST['editingBetslipIndex']);
                }

                # How long for?
                $length = $_POST['playDuration'];

                # Create an order item
                $orderItem = array(
                    'data' => $_POST
                );

                # Remove action
                unset($orderItem['data']['action']);

                # Into an array
                $orderItem['data']['day'] = explode(',', $orderItem['data']['day']);

                # Get each day
                $_POST['daysText'] = array();

                # Turn each day (numeric) => day (text)
                foreach ($orderItem['data']['day'] as $date) {
                    $_POST['daysText'][$date] = VCMS::get('DrawDates.' . date('D', strtotime('Next Sunday + ' . $date . ' days')));
                }

                if (count($orderItem['data']['boards']) != 1) {
                    $_POST['cartLinesText'] = VCMS::get('Cart.Item[Type]Play[Boards]Plural', array('Type' => VCMS::get('Cart.' . $orderItem['data']['gameType']), 'Lines' => count($orderItem['data']['boards'])));
                } else {
                    $_POST['cartLinesText'] = VCMS::get('Cart.Item[Type]Play[Boards]Single', array('Type' => VCMS::get('Cart.' . $orderItem['data']['gameType']), 'Lines' => count($orderItem['data']['boards'])));
                }

                # Full text string
                $_POST['daysOfDraw'] = implode(' / ', $_POST['daysText']);

                # Get the data, feedback loop
                $_POST['pricinginfo'] = $oGameEngine->getPrice(0, $orderItem, true);
                $_POST['price'] = $_POST['pricinginfo']['total'];
                $_POST['priceFormatted'] = VCMS::get('Results.BreakdownAmount[Amount]Currency[Currency]', array('Currency' => '&euro;', 'Amount' => number_format_locale($_POST['price'], 2)));

                # Additional data
                $_POST['gameTitle'] = $oGameEngine->name;
                $_POST['gameCss'] = $oGameEngine->getGameCssFile();
                $_POST['bodyCss'] = $oGameEngine->classname;
                $_POST['cssId'] = $oGameEngine->css_id;
                $_POST['cssLogoSmall'] = $oGameEngine->css_logo_small;

                # Next draw date
                $aDrawData = $mlLottery->getDrawDates(date('Y-m-d'), 1);

                # Get the draw data
                $timestamp = strtotime($aDrawData[0]['drawdate']);
                $_POST['drawDate'] = date('d', $timestamp) . ' ';
                $_POST['drawDate'] .= VCMS::get('ResultDates' . date('F', $timestamp)) . ' ';
                $_POST['drawDate'] .= date('Y', $timestamp);

                /**
                 * @todo, return the first draw date for the game
                 */
                # Create the boards manually
                if ($_POST['quickPlay'] == 'true') {

                    # Create a client
                    $client = new JsonRPCClient('https://api.random.org/json-rpc/1/invoke', 5, true);

                    # Test our limits
                    $check = $client->execute('getUsage', array(
                        "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f"
                            )
                    );

                    # How many boards we need?
                    if (is_array($_POST['boards'])) {
                        $numberOfBoards = $_POST['boards'][0];
                    } else {
                        $numberOfBoards = $_POST['boards'];
                    }

                    $check['requestsLeft'] = 0;

                    # Passed?
                    if ($check['requestsLeft'] > 0) {

                        # Setup what we need
                        $apiData = array(
                            "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f",
                            "n" => $mlLottery->getNumbersCount(),
                            "min" => 1,
                            "max" => $mlLottery->getBoardSize(),
                            "replacement" => false
                        );

                        # Clear the array
                        $_POST['boards'] = array();

                        # Recreate the boards
                        for ($i = 0; $i < $numberOfBoards; $i++) {
                            $response = $client->execute('generateIntegers', $apiData);
                            $_POST['boards'][] = $response['result']['random']['data'];
                        }
                    } else {

                        function randomNumberRange($min, $max, $quantity) {
                            $numbers = range($min, $max);
                            shuffle($numbers);
                            return array_slice($numbers, 0, $quantity);
                        }

                        # Clear the array
                        $_POST['boards'] = array();

                        # Recreate the boards
                        for ($i = 0; $i < $numberOfBoards; $i++) {
                            $_POST['boards'][] = randomNumberRange(1, $mlLottery->getBoardSize(), $mlLottery->getNumbersCount());
                        }
                    }
                }

                # New cart item
                $oLotteryItem = new CartItem();
                $oLotteryItem->setGameID($_POST['gameID']);
                $oLotteryItem->setData($_POST);

                /**
                 * Request the pricing and cutoff time for this game from the engine
                 */
                $oGameEngine = GameEngine::getById($_POST['gameID']);
                $oLotteryItem->setCost($_POST['price']);
                $oLotteryItem->setCutoffTime($oGameEngine->getCutOff());

                /*
                  $oLotteryItem->setDrawDate($dDrawDate['drawdate']);
                  $oLotteryItem->setCost($dDrawDate['price']);
                  $oLotteryItem->setCutoffTime($dDrawDate['cutoff']);
                  $oLotteryItem->setLotteryID($_POST['lotteryID']);
                  $oLotteryItem->setLotteryTitle($oLottery->getTitle());
                  $oLotteryItem->setNumbers($aNumberSet);
                 */

                # Add to the cart
                $oCart->addToCart($oLotteryItem);

                # Clean it up
                unset($oLotteryItem);
            }
        }

        # Save to session
        $this->saveCart($oCart);

        # Debug
        $response['cart'] = $oCart->getCartDetails();

        # Remove the error text
        unset($response['errorText']);

        # Save it
        if ($bDieWithJson) {
            session_write_close();
        }

        if ($bDieWithJson) {
            # Quit the request
            echo json_encode($response);
            exit;
        } else {
            return $response;
        }
    }

    /**
     * Ping the current state of the basket. Catch any data and keep
     * it for a later date.
     *
     * @access public
     */
    public function status() {

        # Nothing set
        if (empty($_POST) || empty($_POST['gameID'])) {
            $this->error('Missing or illegal parameters specified.');
        }

        /*
         * @todo - Implement some method to check the integrity
         * of the data we receive. Make sure there is no illegal
         * moves on the board, etc.
         *
         * Calculate the latest saved time, return the response then
         * we can handle previous queues to the timestamp.
         */

        /**
         * Pass the data to the game engine components
         */
        $oGameEngine = GameEngine::getById($_POST['gameID']);

        # Create an order item
        $orderItem = array(
            'data' => $_POST
        );

        # Clean some out
        # $orderItem['data']['dayString'] = $orderItem['data']['day'];
        # Get all days
        # @todo, replace JS notation
        # $orderItem['data']['dayString'] = str_replace(array('mi', 'sa'), array('Monday', 'Saturday'), $orderItem['data']['dayString']);
        # Into an array
        $orderItem['data']['day'] = explode(',', $orderItem['data']['day']);

        # Get the data, feedback loop
        # $engineData = $oGameEngine->getDump();
        $engineData = array();
        $engineData['pricinginfo'] = $oGameEngine->getPrice(0, $orderItem, true);
        $engineData['price'] = $engineData['pricinginfo']['total'];
        $engineData['priceFormatted'] = '&euro;' . number_format_locale($engineData['price'], 2);
        $engineData['pricinginfo']['price1Formatted'] = '&euro;' . number_format_locale($engineData['pricinginfo']['price1'], 2);
        $engineData['pricinginfo']['price2Formatted'] = '-&euro;' . number_format_locale($engineData['pricinginfo']['price2'], 2);

        # Remove from array
        if (isset($engineData->configurationFields)) {
            unset($engineData->configurationFields);
        }

        # Dump the boards
        echo json_encode(array(
            'data' => $_POST,
            'check' => true,
            'save' => time(),
            'engine' => $engineData
                )
        );

        # Quit
        exit;
    }

    /**
     * Run the Game Engine checks. Not yet implemented.
     *
     * @throws LottoException
     */
    public function run() {

        $this->error('Method not implemented');
    }

    /**
     * Gracefully handle any errors by dumping out
     * the JSON status and returning an error header.
     *
     * @param $error string - The error text to report
     */
    public function error($error = '') {

        # Bad headerse
        header('Bad Request', true, 400);

        # Quit with an error
        echo json_encode(
                array(
                    'error' => $error,
                    'status' => 400
                )
        );

        # Quit
        exit;
    }

    /**
     * setupCart: Create an instance of cart to add the items into.
     * If this doesn't exist in the session, create a new object.
     *
     */
    public function setupCart() {

        # Current session state?
        if (!empty($_SESSION['cart'])) {

            $oSC = unserialize($_SESSION['cart']);

            # Get the session
            if (is_object($oSC) && is_a($oSC, "Cart")) {
                $oCart = $oSC;
            } else {
                $oCart = new Cart();
            }
        } else {

            $oCart = new Cart();
        }

        # Return cart
        return $oCart;
    }

    public function saveCart($cart) {

        $_SESSION['cart'] = serialize($cart);
    }

    function quickDeal() {
        if ($_GET['d']) {
            $sData = substr($_GET['d'], 2, -2);
            $oData = unserialize(base64_decode($sData));
            $_POST['gameID'] = $oData->gameID;
            $_POST['action'] = 'addToCart';
            $this->basket(false);
            if ($oData->sRedirect) {
                header('Location: ' . $oData->sRedirect);
                die();
            }
        } elseif ($_GET['gameID']) {
            $oTest = new \stdClass();
            $oTest->gameID = $_GET['gameID'];
            $oTest->sRedirect = $_GET['redirect'];
            $sData = base64_encode(serialize($oTest));
            $sData = chr(rand(45, 62)) . chr(rand(45, 62)) . $sData . chr(rand(45, 62)) . chr(rand(45, 62));

            print_r($oTest);

            echo 'http://' . $_SERVER['SERVER_NAME'] . '/administration/API/engine/quickDeal?d=' . $sData;
        }
    }

}
