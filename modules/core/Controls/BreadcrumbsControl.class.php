<?php
/**
 * Creates a breadcrumb to the current page. Currently only works with pages
 * that are listed in the admin_pages table.
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 *
 * @example $oBreadcrumbs=new BreadcrumbsControl();
            echo $oBreadcrumbs->Output();
 */

class BreadcrumbsControl extends Control{
    var $aPages=array();
    var $aCurrentPage=array();

    function __construct(){
        /**
         * Start by looking to see if we can work out what page we are on now
         */
        $this->getPageFromUrl();
        $this->getBreadCrumbs();
    }

    function Output(){

        /**
         * Using a string builder on this one as it's pretty straightforward
         */
        $out='<ul class="BreadcrumbsControl">';
        $bIsFirstPage=true;
        $bIsFirstSubPage=true;
        foreach($this->aPages as $page){
            if($page['filename']!='' || $page['title']!=''){
                $out.='<li>';
                $out.=($bIsFirstSubPage==false)?' <i class="fa fa-angle-right"></i> ':'';
                if($page['icon']!='' && $bIsFirstPage){
                    $out.='<span class="fa-stack fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa '.$page['icon'].' fa-stack-1x fa-inverse"></i></span>';
                }
                if($bIsFirstPage){
                    $out.='<h3>';
                }
                if($page['linked']!==false){
                    $out.='<a href="'.$page['filename'];
                    if($page['tablename']!=''){
                        $out.='?tablename='.$page['tablename'];
                    }
                    $out.='">'.$page['title'].'</a>';
                }else{
                    $out.=$page['title'];
                }
                if($bIsFirstPage){
                    $out.='</h3>';
                }
                $out.='</li>';
                if(!$bIsFirstPage) $bIsFirstSubPage=false;
                $bIsFirstPage=false;
            }
        }
        $out.='</ul>';
        return $out;
    }

    function getPageFromUrl($sURL=null,$sTableName=null){
        $aURL=explode('?',$_SERVER['REQUEST_URI']);
        if($sURL===null) $sURL=str_replace('/administration/','',$aURL[0]);
        if($sTableName==null) $sTableName=$_GET['tablename'];

        //$sURL=basename($sURL);
        if($sURL=='generic-listing.php'){
            $this->addPage('View Items', 'fa-list');
        }

        if($sURL=='subpages.php'){
            $this->addPage('Menu', 'fa-list');
        }

        if($sURL=='generic-details.php'){
            $sURL='generic-listing.php';
            $aEditPage=array();
            if($_GET['id']){
                $this->addPage('Edit Item', 'fa-pencil');
            }else{
                $this->addPage('Create Item', 'fa-pencil');
            }

            $this->addPage('View Items', 'fa-list','generic-listing.php',$sTableName);
        }

        $sSql="SELECT * FROM admin_pages WHERE filename='{$sURL}' AND tablename='{$sTableName}'";
        $aData=DAL::executeGetRow($sSql);

        if($aData){
            $this->aCurrentPage=$aData;
        }
        return $aData;
    }

    function setTitlePage($sTitle,$sIcon,$sLink=null,$sTableName=null){
        $this->aCurrentPage=$this->_createPageArray($sTitle, $sIcon, $sLink, $sTableName);
    }

    function addPage($sTitle,$sIcon,$sLink=null,$sTableName=null){
        $this->aPages[]=$this->_createPageArray($sTitle, $sIcon, $sLink, $sTableName);
    }

    function _createPageArray($sTitle,$sIcon,$sLink=null,$sTableName=null){
        $aPage=array();
        $aPage['title']=$sTitle;
        $aPage['icon']=$sIcon;
        $aPage['filename']=$sLink;
        $aPage['tablename']=$sTableName;

        if($sLink!==null){
            $aPage['linked']=true;
        }else{
            $aPage['linked']=false;
        }

        return $aPage;
    }

    function getBreadCrumbs(){
        $aParentPage=$this->aCurrentPage;
        $iPagesScanned=0;

        $this->aPages[]=$aParentPage;

        while($aParentPage['parent_id']>0 && $iPagesScanned < 10){
            $aParentPage=$this->getParentPage($aParentPage['parent_id']);
            $this->aPages[]=$aParentPage;
            $iPagesScanned++;
        }

        $this->aPages=array_reverse($this->aPages);

        return $this->aPages;
    }

    function getParentPage($iPageId){
        $sSql="SELECT * FROM admin_pages WHERE id={$iPageId}";
        return DAL::executeGetRow($sSql);
    }

}