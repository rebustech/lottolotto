<?php

class HtmlFormTextInputControl extends HtmlTagControl{
    var $sName;
    var $sValue;
    var $sCaption;


    function __construct($sName,$sCaption,$sValue = null){
        $this->sName=$sName;
        $this->sCaption=$sCaption;
        if (!is_null($sValue)) {
            $this->sValue = $sValue;
        }
    }

    function Output() {
        $sOut='<fieldset class="detailsform">';
        $sOut.='<label class="long">';
        $sOut.='<span>'.$this->sCaption.'</span>';
        $sOut.='<div><input type="text" name="'.$this->sName.'" id="field_'.$this->sName.'" class="long" value="'.$this->sValue.'"></div></label></fieldset>';
        return $sOut;
    }
}