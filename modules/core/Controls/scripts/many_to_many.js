/*
$('.many_to_many_picklist_form ul').sortable({
  connectWith: ".connectedSortable",
  placeholder: "ui-sortable-placeholder"
}).disableSelection();
*/
$('.pseudo_select input').keyup(function(){
    var eUL=$(this).parent().find('ul');
    var sText=$(this).val();
    eUL.find('li').hide();
    eUL.find('li[data-smname*="'+sText+'"]').show();
});
$(".many_to_many_picklist_form ul > li").dblclick(function(){
    ePickList=$(this).parents('div.many_to_many_picklist_form');
    if($(this).parent().hasClass('allItems')){
        ePickList.find('ul.selectedItems').append($(this));
        sortList(ePickList.find('ul.selectedItems'));
    }else{
        ePickList.find('ul.allItems').append($(this));
        sortList(ePickList.find('ul.allItems'));
    }
});

function sortList(list){
    var listitems = list.children('li').get();
    listitems.sort(function(a, b) {
       return $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
    })
    $.each(listitems, function(idx, itm) { list.append(itm); });
}