<?php

/**
 * Use this control to display items in a many to many relationship with the record
 * being displayed. Will product a checkbox, multiselect or table showing all available
 * options with any that are already present being checked or selected
 *
 * @package Lovelotto
 * @subpackage Controls
 */
class ManyToManyFormControl extends Control{
    var $sView='/views/many_to_many_form';
    var $sItemView='/views/many_to_many_item';

    var $aAllItems;
    var $aSelectedItems;

    /**
     * Internal flag to track if scripts have been added to avoid duplicating them
     * if this control is used in multiple places
     * @var boolean
     */
    static $bScriptsAdded=false;


    /**
     * Create a new many to many control, you need to pass in two arrays, one containing
     * all the values that can be selected from, and the other an array of items that should
     * be pre-selected (i.e. already in a many-to-many relationship
     *
     * @param array $aAllItems . Must contain at least fields called (or aliased as) id,name
     * @param array $aSelectedItems . Must contain at least 1 field called (or aliased as) id, which will match items above
     * @param array $aScripts
     */
    function __construct(array $aAllItems,array $aSelectedItems,array $aScripts=[]){
        $this->aAllItems=$aAllItems;
        $this->aSelectedItems=$aSelectedItems;

        foreach($this->aSelectedItems as $aItem){
            $this->aSelected[$aItem['id']]=$aItem;
        }

        $aDefaultScripts=['/administration/API/ControlsAssets/manyToManyControl'];
        $aAllScripts=array_merge($aDefaultScripts,$aScripts);

        if(!self::$bScriptsAdded){

            foreach($aAllScripts AS $sScript){

                LLResponse::$sPostBody.=LLResponse::addScript($sScript);
            }


            self::$bScriptsAdded=true;
        }
        $this->aScripts = $aScripts;
    }

    /**
     * Generates the output for this control
     * @return string
     */
    function output(){
        $oView=new LLView();
        $oView->sItems=$this->getItemsOutput($this->aAllItems);
        return $oView->output(__DIR__.$this->sView);
    }

    /**
     * Used to create a string output of all the items in $aItems
     *
     * Iterates through an array passing the id and name values to a new view on
     * each pass, concatenating the return of each view and returning the full
     * contatenated list.
     * @param array $aItems The items to iterate through
     * @param string optional $sView The name of the view to us. If left blank uses $this->sItemView
     * @return string
     */
    protected function getItemsOutput(array $aItems, $sView=null){
        if($sView===null) $sView=$this->sItemView;
        foreach($aItems as $aItem){
            $oView=new LLView();
            $oView->aItem=$aItem;
            $oView->id=$aItem['id'];
            $oView->sName=$this->sName;
            $oView->sTitle=$aItem['name'];

            if(array_key_exists($oView->id, $this->aSelected)){
                $oView->isSelected=true;
            }else{
                $oView->isSelected=false;
            }

            $sOut.=$oView->output(__DIR__.$sView);
        }
        return $sOut;
    }
}