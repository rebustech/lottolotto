<?php
/**
 * Creates a new tabbed interface or a tab in an existing tabbed interface
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class TabbedInterface extends Control{
    var $sView='controls/TabbedInterface';

    /**
     * Caption of the tab
     * @var string
     */
    var $sTabName;
    /**
     * Class name of an icon from font-awesome
     * See http://fontawesome.io/icons/ for a list of supported icons
     * @var string
     */
    var $sIcon;

    var $bShowSaveButton=false;

    /**
     * When creating a new tab you can pass in the caption for the tab
     * @param string $sTabName
     */
    function __construct($sTabName=null) {
        $this->sTabName=$sTabName;
        $this->aItems=array();
    }

    /**
     * Adds a new tab to a tabbed interface
     * @param TabbedInterface $tab
     */
    function AddItem(TabbedInterface $tab){
        $this->aItems[]=$tab;
    }

    function addSaveButton(){
        $this->bShowSaveButton=true;
    }

    /**
     * Produces the output for the tabbed interface
     * Works using string building as this was seleted as the easiest method at this point
     *
     * @todo We should change this to use a view
     * @param string $type Internal use only
     * @return string
     */
    function Output($type='none'){
        if($this->sTabName==null){
            $sOut='<div class="TabbedInterface"><ul>';
            foreach($this->aItems as $aItem){
                $sOut.=$aItem->Output('title');
            }
            if($this->bShowSaveButton){
                $sOut.='<li class="actionbutton"><button name="action" value="save"><i class="fa fa-fw fa-lg fa-save"></i>Save</button></li>';
            }
            $sOut.='</ul>';


            $sOut.='<div class="TabbedInterfaceItems">';
            foreach($this->aItems as $aItem){
                $sOut.='<div class="TabbedInterfaceContent">';
                $sOut.=$aItem->Output('content');
                $sOut.='</div>';
            }
            $sOut.='</div></div>';
        }else{
            switch($type){
                case 'title':
                    $out='<li>';
                    if($this->sIcon!=''){
                        $out.='<i class="fa fa-fw fa-lg '.$this->sIcon.'"></i>';
                    }
                    $out.=$this->sTabName;
                    $out.='</li>';
                    return $out;
                    break;
                case 'content':
                    foreach($this->aItems as $sContent){
                        if(is_object($sContent)){
                            $this->sContent.=$sContent->Output();
                        }else{
                            $this->sContent.=$sContent;
                        }
                    }
                    return $this->sContent;
                    break;
            }
        }
        return $sOut;
    }
}
