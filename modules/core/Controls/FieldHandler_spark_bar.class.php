<?php

class FieldHandler_spark_bar extends TableField{
    static $chartnumber=1;

    function Output($sFieldValue){
        $html='<div style="text-align:center;"><div id="chart'.self::$chartnumber.'" class="dashboardChart" style="width: 30px; height: 30px; display: inline-block; vertical-align: middle;"></div>'.round($sFieldValue,1).'%</div>';


    // percent chart
        $html.='<script>
    var chart = new AmCharts.AmPieChart(AmCharts.themes.none);
    chart.dataProvider = [{
        x: 1,
        value: '.($sFieldValue).'
    }, {
        x: 2,
        value: '.(100-$sFieldValue).'
    }];
    chart.labelField = "x";
    chart.valueField = "value";
    chart.labelsEnabled = false;
    chart.balloonText = undefined;
    chart.valueText = undefined;
    chart.radius = 10; // half of a width of a div
    chart.outlineThickness = 1;
    chart.colors = ["#081", "#ddd"];
    chart.startDuration = 0;
    chart.write("chart'.self::$chartnumber++.'");</script>';

        return $html;
    }
}