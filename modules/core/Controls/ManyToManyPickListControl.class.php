<?php

/**
 * Use this control to display items in a many to many relationship with the record
 * being displayed. Will product a checkbox, multiselect or table showing all available
 * options with any that are already present being checked or selected
 */
class ManyToManyPickListControl extends ManyToManyFormControl{
    var $sView='/views/many_to_many_picklist';
    var $sItemView='/views/many_to_many_picklist_item';

    function __construct(array $aAllItems, array $aSelectedItems) {
        parent::__construct($aAllItems, $aSelectedItems);
        //LLResponse::$sPostBody.=LLResponse::addScript('/modules/Controls/scripts/pick_list_control.js');
    }

    /**
     *
     * @return type
     */
    function output(){
        $oView=new LLView();
        $oView->sName=$this->sName;
        $oView->sAllItems=$this->getItemsOutput($this->aAllItems);
        $oView->sChosenItems=$this->getItemsOutput($this->aSelectedItems);
        return $oView->output(__DIR__.$this->sView);
    }

}