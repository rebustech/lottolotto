<?php
/**
 * Inserts a configuration control from a class. Only works on classes that have
 * configuration methods such as game engines, game engine components and ticket
 * buying engines
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */

class ClassConfigurationControl extends Control{
    var $oObject;
    var $sMethodName='getBackendConfigurationFields';

    /**
     * Create a new class configuration control
     * @param object $oObject The object that contains the fields and data to be displayed
     * @param string $sMethodName default=getBackendConfigurationFields The method that will return the array of fields to display
     */
    function __construct($oObject,$sMethodName=null) {
        $this->oObject=$oObject;
        if($sMethodName!==null) $this->sMethodName=$sMethodName;
    }

    /**
     * Generates the output by concatenating the output from all the fields returned
     * by the method name specified
     * @return string
     */
    function Output(){
        $sMethodName=$this->sMethodName;
        $aFields=$this->oObject->$sMethodName();
        $sOut='<fieldset class="detailsform">';
        foreach($aFields->aFields as $oField){
            $sOut.=$oField->output();
        }
        $sOut.='</fieldset>';
        return $sOut;
    }

}