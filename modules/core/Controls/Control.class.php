<?php

/**
 * Base class for controls
 *
 * All new controls should extend from this class and optionally override Output
 * and AddControl methods
 *
 * @package LoveLotto
 * @author Jonathan Patchett
 * @subpackage Controls
 */
class Control{
    /**
     * The final output from the Output method
     * The base class just accepts any content here and the output method will
     * append to this
     * @var String
     */
    var $sContent;
    /**
     * Array of child controls. When calling output a control should handle it's
     * own output, but also (optionally) have the ability to run the output method
     * on any controls contained in this array and add to their own output.
     * @var array
     */
    var $aItems=array();

    /**
     * Produces the final output for display from this control. Basically just
     * returns $this->sContent with the content from all the control objects added
     * to $this->aItems appended
     * @return string
     */
    function Output(){
        foreach($this->aItems as $sContent){
            if(is_object($sContent)){
                $this->sContent.=$sContent->Output();
            }else{
                $this->sContent.=$sContent;
            }
        }
        return $this->sContent;
    }

    /**
     * Adds a new control to the list of contained controls
     * @param Control $oControl
     */
    function AddControl($oControl){
        $this->aItems[]=$oControl;
    }
}
