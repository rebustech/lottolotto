<?php
/**
 * Generic table field control, just outputs the value of a field based on the
 * field type. To create new handlers create a class called FieldHandlerMyFieldType
 * and extend this class
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class TableRowActionControl extends TableField{
    var $sFieldName;
    var $sFieldCaption;
    var $sFieldLink;

    function __construct($sFieldName,$sFieldCaption=null,$sFieldAction=null) {
        $this->sFieldName=$sFieldName;
        $this->sFieldAction=$sFieldAction;
        $this->sFieldCaption=$sFieldCaption;
    }

    function Output($aRow){
        $sAction=$this->sFieldAction;
        foreach($aRow as $k=>$v){
            $sAction=str_replace('{'.$k.'}',$v,$sAction);
        }
        return '<a class="button btn_'.$this->sFieldName.'" href="'.$sAction.'">'.$this->sFieldCaption.'</a>';
    }
}
