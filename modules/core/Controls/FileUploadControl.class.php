<?php

/**
 * Creates a new tabbed interface or a tab in an existing tabbed interface
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class FileUploadControl extends Control {

    var $sView = '/views/FileUploadControl';

    /**
     * Caption of the tab
     * @var string
     */
    var $sCaption;

    /**
     * Target Filename
     */
    var $sFilename;
    var $sKey;
    var $sPreviewFile;

    /**
     * When creating a new tab you can pass in the caption for the tab
     * @param string $sTabName
     */
    function __construct($sCaption, $sFilename, $sKey) {
        $this->sCaption = $sCaption;
        $this->sFilename = $sFilename;
        $this->sKey = $sKey;
    }

    /**
     * Uploads the file to the correct location for replication
     */
    function doUpload($oTargetObject = null) {
        if(!$_FILES[$this->sKey]) return false;

        FeMessage::setMessage('Uploading Image');
        // Allowed extensions.
        $aAllowedExtensions = (isset(Config::$config->allowedExtensions)) ? explode(',', Config::$config->allowedExtensions) : array("gif", "jpeg", "jpg", "png");
        $aAllowedMimeTypes = (isset(Config::$config->allowedMimeTypes)) ? explode(',', Config::$config->allowedMimeTypes) : array("image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png");
        if (!isset(Config::$config->sDataPath))
            throw new LottoException('VariantsCMSAPI.uploadImage.DataPathNotSet', 'Data path not set');

        $imageTypes = array("gif", "jpeg", "jpg", "png");

        $sPathName = Config::$config->sDataPath . 'res/' . $this->sFilename;


        //Make sure the path exists.
        $aPathInfo = pathinfo($sPathName);

        $sCheckPathName = str_replace('/', '\\', $aPathInfo['dirname']);
        $aPathSegments = explode('\\', $sCheckPathName);
        $sCheckPath = array_shift($aPathSegments);
        foreach ($aPathSegments as $sPathSegment) {
            $sCheckPath.='/' . $sPathSegment;
            @mkdir($sCheckPath);
            @chmod($sCheckPath, 0777);
        }

        // Get extension.
        $aPathInfo = pathinfo($_FILES[$this->sKey]["name"]);
        $extension = $aPathInfo['extension'];

        $this->sStoredFilename=$this->sFilename.'.'.$extension;

        // An image check is being done in the editor but it is best to
        // check that again on the server side.
        if (in_array($_FILES[$this->sKey]["type"], $aAllowedMimeTypes) && in_array($extension, $aAllowedExtensions)) {


            //if($oTargetObject) $oTargetObject->log('File uploaded to '.$this->sKey.' Filename was '.$_FILES[$this->sKey]["name"]);
            // For images pass into kraken
            if (in_array($extension, $imageTypes)) {
                // Save file in the uploads folder.
                FeMessage::setMessage('Compressing Image');

                $sKrakenSourceFile = str_replace('\\','/',$sPathName . '__temp.' . $extension);
                move_uploaded_file($_FILES[$this->sKey]["tmp_name"], $sKrakenSourceFile);

                $kraken = new Kraken(Config::$config->kraken->sApiKey, Config::$config->kraken->sApiSecret);

                $params = array(
                    # Can be a file or URL
                    "file" => $sKrakenSourceFile,
                    # Basically aysnc / sync
                    "wait" => true,
                    # Compressess beyond lossless
                    "lossy" => true,
                    "quality" => 90,
                    # WebP format
                    "webp" => false

                );

                # Send to Kraken
                $data = $kraken->upload($params);

                # Read result
                if ($data['success']) {
                    # We were valid, but no URL so no further optimisation possible
                    if (!empty($data['kraked_url'])) {
                        # Store new optimised image
                        copy($data['kraked_url'], $sPathName . '.' . $extension);
                    }
                    FeMessage::setMessage('Compressing Image Completed');
                    return true;
                }else{
                    FeMessage::setMessage('Compressing Image Failed','error');
                }
            } else {

                // Save file in the uploads folder.
                move_uploaded_file($_FILES[$this->sKey]["tmp_name"], $sPathName . '.' . $extension);

                return true;
            }
        } else {
            //if($oTargetObject) $oTargetObject->log('Invalid file type uploaded to '.$this->sKey.' Filename was '.$_FILES[$this->sKey]["name"]);
        }
        return false;
    }

    /**
     * Produces the output for the tabbed interface
     * Works using string building as this was seleted as the easiest method at this point
     *
     * @todo We should change this to use a view
     * @param string $type Internal use only
     * @return string
     */
    function Output($type = 'none') {
        $oView = new LLView();
        $oView->sCaption = $this->sCaption;
        $oView->sFilename = $this->sFilename;
        $oView->sKey = $this->sKey;
        $oView->sPreviewFile=$this->sPreviewFile;
        $sOut.=$oView->output(__DIR__ . $this->sView);
        return $sOut;
    }

}
