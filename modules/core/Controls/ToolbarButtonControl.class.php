<?php
/**
 * Creates a button. Used for toolbars, but can be used anywhere as a control
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class ToolbarButtonControl extends Control{
    /**
     * The caption for the button
     * @var string
     */
    var $sCaption;
    /**
     * An optional class name from font awesome
     * @var string
     */
    var $sIcon;
    /**
     * Url to link the button to
     * @var string
     */
    var $sLink;

    /**
     * Creates a new button control using the provided attributes
     * @param string $sCaption The caption for the button
     * @param string $sLink Url to link the button to
     * @param string $sIcon An optional class name from font awesome
     */
    function __construct($sCaption,$sLink,$sIcon='') {
        $this->sCaption=$sCaption;
        $this->sLink=$sLink;
        $this->sIcon=$sIcon;
    }

    /**
     * Creates the button output
     * @return string
     */
    function Output(){
        $out='<a href="'.$this->sLink.'" class="ToolbarButtonControl">';
        if($this->sIcon!=''){
            $out.='<i class="fa '.$this->sIcon.' fa-lg"></i>';
        }
        $out.=$this->sCaption;
        $out.='</a>';

        return $out;
    }
}