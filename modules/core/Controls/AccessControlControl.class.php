<?php
/**
 * This creates the access control control for a given set of data
 * Creates a view where access control options are shown grouped accordingly
 * and with either a tickbox, or a dropdown to allow the correct option to be selected
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class AccessControlControl extends Control{
    /**
     * Object to contain the result from DAL with a list of selected rights
     * @var Object
     */
    var $oData;
    /**
     * Should we show a tickbox or a dropdown?
     * @var Bool
     */
    var $bShowOptions=false;

    /**
     *
     * @param Object $oData
     * @param bool $bShowOptions
     */
    function __construct($oData,$bShowOptions=false){
        $this->oData=$oData;
        $this->bShowOptions=$bShowOptions;
    }

    /**
     * Generate the necessary output
     */
    function Output(){
        $aGroups=array();
        $aParents=array();

        $xml=new SimpleXMLElement('<div class="AccessControlControl TabbedInterface"></div>');

        $oTabs=$xml->addChild('ul');
        $oTabContent=$xml->addChild('div');
        $oTabContent->addAttribute('class','TabbedInterfaceItems');

        foreach($this->oData as $row){
            //Select or create a group for this item to be added to
            if(!isset($aGroups[$row['group_id']])){
                $oTabs->addChild('li',$row['group_name']);
                $oNewGroup=$oTabContent->addChild('div');
                $oNewGroup->addAttribute('class', 'AccessRightsGroup TabbedInterfaceContent');
                //$oNewGroup->addChild('h3',$row['group_name']);
                $oNewGroup->addChild('ul');
                $aGroups[$row['group_id']]=$oNewGroup;
            }

            $oParent=null;
            if($row['fk_parent_right_id']>0 && isset($aParents[$row['fk_parent_right_id']])){
                $oParent=$aParents[$row['fk_parent_right_id']];
            }else{
                $oParent=$aGroups[$row['group_id']];
            }

            //If there isn't yet a UL create one
            if(!isset($oParent->ul)){
                $oParent->addChild('ul');
            }

            $oNewItem=$oParent->ul->addChild('li');
            $oParent->addAttribute('data-haschildren','true');
            $oNewItem->addAttribute('data-id',$row['id']);
            $oNewItem->addAttribute('data-parent_id',$row['fk_parent_right_id']);
            $oItem=$oNewItem->addChild('label');
            $oInput=$oItem->addChild('input');
            $oInput->addAttribute('type', 'checkbox');
            //Is the item selected?
            if($row['fk_access_role_id']!=''){
                $oInput->addAttribute('checked','checked');
            }

            $oLabel=$oItem->addChild('span',$row['name']);
            $oLabel->addAttribute('title',$row['notes']);

            $aParents[$row['id']]=$oNewItem;

            unset($oNewItem);

        }
        return str_replace('<!--?xml version="1.0"?-->','',$xml->saveXML());
    }
}
