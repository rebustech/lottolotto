<?php
/**
 * Creates a toolbar, which is basically just a holder for any controls you want
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class ToolbarControl extends Control{
    function Output(){
        $out='<div class="ToolbarControl"><ul>';

        foreach($this->aItems as $sContent){
            $out.='<li>';
            if(is_object($sContent)){
                $out.=$sContent->Output();
            }else{
                $out.=$sContent;
            }
            $out.='</li>';
        }

        $out.='</ul></div>';

        return $out;
    }
}
