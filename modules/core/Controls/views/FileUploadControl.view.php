<fieldset class="detailsform">
    <label class="">
        <span><?=$this->sCaption?></span>
        <div>
            <input type="file" name="<?=$this->sKey?>"/>
            <?php  if($this->sPreviewFile!=''): ?>
                <img src="/res/<?=$this->sPreviewFile?>" style="max-width:400px;max-height:200px;width:auto;height:auto;"/>
            <?php endif; ?>
        </div>
    </label>
</fieldset>