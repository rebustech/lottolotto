<div class="many_to_many_picklist_form detailsform" id="<?=$this->sName?>">
    <div>
        <h2>Unavailable</h2>
        <div class="pseudo_select">
            <input type="text" class="search" placeholder="Search"/>
            <ul class="connectedSortable allItems">
                <?=$this->sAllItems?>
            </ul>
        </div>
    </div>
    <div>
        <h2>Available</h2>
        <div class="pseudo_select">
            <input type="text" class="search" placeholder="Search"/>
            <ul class="connectedSortable selectedItems">
                <?=$this->sChosenItems?>
            </ul>
        </div>
    </div>
</div>