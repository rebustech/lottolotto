<label id="field_<?=$this->sName?>_<?=$this->id?>" class="small<?=($this->bIsRequired)?' required':''?>">
    <span><?=$this->sTitle?></span>
    <div>
        <input type="checkbox" name="<?=$this->sName?>[]" value="<?=$this->id?>" <?=$this->isSelected?'checked="checked"':''?>/>
    </div>
</label>