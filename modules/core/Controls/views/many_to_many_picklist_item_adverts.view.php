<tr>
    <td>
        <label id="field_<?=$this->sName?>_<?=$this->id?>" class="small<?=($this->bIsRequired)?' required':''?>">
            <input type="checkbox" name="<?=$this->sName?>[]" value="<?=$this->id?>" <?=$this->isSelected?'checked="checked"':''?>/>
        </label>
    </td>
    <td style="whitespace:nowrap;"><?=$this->sTitle?></td>
    <td>
        <?=strip_tags($this->aItem['html'],'<img><iframe>')?>
    </td>
</tr>