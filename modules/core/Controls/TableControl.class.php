<?php
/**
 * New table generator that is intended to replace the generic-table file that
 * came with the original platform.
 *
 * Allows the ability to display the result of views and more complex queries
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 *
 * @todo Add pagination
 * @todo Add ability to sort
 * @todo Add ability to sort the fields
 */
class TableControl extends Control{
    /**
     * Associative array of data to show in the table
     * @var array
     */
    var $aData=array();
    var $aFields=array();
    var $aActions=array();

    function __construct($aData) {
        //Create a structure for the fields
        $this->aData=$aData;
    }

    /**
     * Adds a field as a TableField object
     * @param TableField $oTableField
     */
    function addField(TableField $oTableField){
        $this->aFields[$oTableField->sFieldTable.'-'.$oTableField->sFieldName]=$oTableField;
    }

    /**
     * Removes a field from the output
     * @param string $sTableName the name of the table that the field is from
     * @param string $sTableField the name of the field
     */
    function removeField($sTableName,$sTableField){
        unset($this->aFields[$sTableName.'-'.$sTableField]);
    }

    /**
     * Overrides the rendering method for a field. This can be a class name to
     * another control of any type
     * @param string $sTableName the name of the table that the field is from
     * @param string $sTableField the name of the field
     * @param string $sType the new type or classname of a control that can handle the output
     */
    function renderAs($sTableName,$sField,$sType){
        if(isset($this->aFields[$sTableName.'-'.$sField])){
            $this->aFields[$sTableName.'-'.$sField]->sFieldType=$sType;
        }
    }

    /**
     * Decide which fields from the given table to display. Any fields not listed
     * are not displayed in the final output.
     *
     * You can only apply this to one table at a time, however multiple calls
     * can be made, each one referencing a different table
     *
     * @param string $sTableName The table that we are referring to
     * @param string $sFields Comma seperated list of fields to keep
     */
    function keepFields($sTableName,$sFields){
        $aFields=explode(',',$sFields);
        foreach($aFields as $sFieldToKeep){
            if(isset($this->aFields[$sTableName.'-'.$sFieldToKeep])){
                $this->aFields[$sTableName.'-'.$sFieldToKeep]->bKeepMe=true;
            }
        }
        foreach($this->aFields as $oField){
            if($oField->sFieldTable==$sTableName){
                if(isset($this->aFields[$sTableName.'-'.$sFieldToKeep])){
                    if(!$this->aFields[$sTableName.'-'.$oField->sFieldName]->bKeepMe){
                        unset($this->aFields[$sTableName.'-'.$oField->sFieldName]);
                    }else{
                        unset($this->aFields[$sTableName.'-'.$oField->sFieldName]->bKeepMe);
                    }
                }
            }
        }
    }

    /**
     * Internal Use - Gets the fields from the table provided
     * @param string $sTableName
     */
    function getFieldsFromTable($sTableName){
        $oFields=GenericTableAdmin::getTableDetails($sTableName);
        foreach($oFields->aFields as $oField){
            $oNewField=new TableField($oField->sName,$oField->sType,  ucwords(str_replace('_',' ',$oField->sName)),$sTableName);
            $oNewField->oConstraint=$oNewField->oConstraint;
            $oNewField->bRequired=$oNewField->bRequired;
            $this->AddField($oNewField);
        }
    }

    function addAction($oAction){
        $this->aActions[]=$oAction;
    }

    /**
     * Generates the table
     * @return string
     */
    function Output() {
        //Now just run through each of the fields for each of the rows
        $out='<table class="datatable"><thead><tr>';
        foreach($this->aActions as $oAction){
            $out.='<th class="action"></th>';
        }
        foreach($this->aFields as $oField){
            $out.='<th>'.lang::get($oField->sFieldTable.'-'.$oField->sFieldName).'</th>';
        }
        $out.='</tr></thead><tbody>';
        foreach($this->aData as $aRow){
            $out.='<tr><form method="post" action="">';
            foreach($this->aActions as $oAction){
                $out.='<td class="action">';
                $out.=$oAction->Output($aRow);
                $out.='</td>';
            }
            foreach($this->aFields as $oField){
                $out.='<td><input type="hidden" name="'.$oField->sFieldName.'" value="'.$aRow[$oField->sFieldName].'"/>';
                $out.=$oField->Output($aRow[$oField->sFieldName]);
                $out.='</td>';
            }
            $out.='</form></tr>';
        }
        $out.='</tbody></table>';

        return $out;
    }
}