<?php
/**
 * Generic table field control, just outputs the value of a field based on the
 * field type. To create new handlers create a class called FieldHandlerMyFieldType
 * and extend this class
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class TableField{
    var $sFieldName;
    var $sFieldTable;
    var $sFieldType;
    var $sFieldCaption;
    var $sFieldIcon;
    var $sFieldSortable=false;
    var $sFieldGroupable=false;
    var $bRequired=false;

    static $oLastField;

    function __construct($sFieldName,$sFieldType=null,$sFieldCaption=null,$sFieldTable=null) {
        $this->sFieldName=$sFieldName;
        $this->sFieldTable=$sFieldTable;
        $this->sFieldType=$sFieldType;
        $this->sFieldCaption=$sFieldCaption;

        if($sFieldTable==null && isset(self::$oLastField)){
            $this->sFieldTable=$oLastField->sFieldTable;
        }
    }

    function Output($sFieldValue){
        switch($this->sFieldType){
            case 'varchar':
                return $sFieldValue;
                break;
            case 'int':
                return $sFieldValue;
                break;
            case 'float':
                return '€'.number_format_locale($sFieldValue,2);
                break;
            case 'double':
                return number_format_locale($sFieldValue,2);
                break;
            case 'tinyint':
                return '<input type="checkbox" '.(($sFieldValue==1)?'checked="checked"':'').'/>';
                break;
            case 'datetime':
                // Return  a blank value if the field is not able to be
                // converted to a datetime
                if (is_null($sFieldValue) || $sFieldValue == '')
                {
                   return '';
                }
                return date('d/m/Y H:i:s',strtotime($sFieldValue));
                break;
            case 'icon':
                return '<i class="fa fa-lg '.$sFieldValue.'"></i>';
                break;
            case 'lottoballs':
                $balls=explode('|',$sFieldValue);
                if($balls[0]!=''){
                    $sOut='<span class="balls">';
                    foreach($balls as $ball){
                        $sOut.='<span>'.$ball.'</span>&nbsp;';
                    }
                    $sOut.='</span>';
                }else{
                    $sOut='N/A';
                }
                return $sOut;
                break;
            default:
                $sFieldHandler='FieldHandler_'.$this->sFieldType;
                if(class_exists($sFieldHandler)){
                    $oFieldHandler=new $sFieldHandler($this);
                    return $oFieldHandler->Output($sFieldValue);
                }else{
                    return $this->sFieldType;
                }
        }
    }
}
