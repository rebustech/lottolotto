<?php

class FormFieldHandlerListing extends ConfigurationSelection{
    var $sView='Controls/views/file_selection';
    function __construct($sName = '', $sCaption = '', $bIsRequired = true) {
        parent::__construct($sName, $sCaption, $bIsRequired);

        $aHandlers=\LL\Handler::getHandlersOfType($this->sHandlerType);
        foreach($aHandlers as $aHandler){
            $this->addOption($aHandler['class'],$aHandler['name']);
        }
    }
}