<?php

class HtmlTagControl extends Control{
    var $sTag;
    var $sContent;
    var $aParams=array();

    function __construct($sTag,$sContent){
        $this->sTag=$sTag;
        $this->sContent=$sContent;
    }

    function addParam($sParamName,$sParamValue){
        if(!isset($this->aParams[$sParamName])){
            $this->aParams[$sParamName]=$sParamValue;
        }else{
            $this->aParams[$sParamName].=$sParamValue;
        }
    }

    function Output() {
        $sOut='<'.$this->sTag;
        foreach($this->aParams as $sParamName=>$sParamValue){
            $sOut.=' '.$sParamName.'="'.$sParamValue.'"';
        }
        $sOut.='>'.$this->sContent;
        if($this->aItems){
            foreach($this->aItems as $oChildControl){
                $sOut.=$oChildControl->output();
            }
        }
        $sOut.='</'.$this->sTag.'>';
        return $sOut;
    }
}