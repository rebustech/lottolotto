<?php
/**
 * Creates a new self submitting form that you can add more controls to
 *
 * @package LoveLotto
 * @subpackage Controls
 * @author Jonathan Patchett
 */
class SelfSubmittingForm extends Control{
    var $sAction='';
    var $sMethod='POST';

    /**
     * When creating a new tab you can pass in the caption for the tab
     * @param string $sTabName
     */
    function __construct() {
        $this->aItems=array();
    }

    /**
     * This has been taken from GenericTableAdmin.class.php
     * @param type $sTablename
     * @param type $iId
     * @param type $aDefaultData
     * @param type $sPreviousPage
     * @param type $iWebsiteID
     * @param type $bDeleteButton
     * @param type $iReturnID
     */
    function submitToGeneric($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID){
        $actionpage = "actions/generic_actions.php?tablename=" . $sTablename;
        if ($iId) {
            if ($bDeleteButton) {
                $sDeletePage = $actionpage . "&a=delete&id={$iId}";
            }
            $actionpage .= "&a=edit&id={$iId}";
        } else {
            $actionpage .= "&a=add";
        }

        if ($iReturnID != NULL) {
            $actionpage .= "&returnid=" . $iReturnID;
        }

        if ($sPreviousPage) {
            $previousPageQueryString = "&pp=" . substr(urldecode($sPreviousPage), 0, strpos(urldecode($sPreviousPage), "?"));
            $actionpage .= $previousPageQueryString;
            if ($sDeletePage) {
                $sDeletePage .= $previousPageQueryString;
            }
        }
        $this->sAction=$actionpage;
    }

    function Output(){
        return '<form method="'.$this->sMethod.'" action="'.$this->sAction.'" enctype="multipart/form-data" id="mainEditForm">'.parent::Output().'</form>';
    }
}
