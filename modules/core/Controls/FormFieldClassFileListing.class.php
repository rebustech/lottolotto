<?php

class FormFieldClassFileListing extends ConfigurationSelection{
    var $sView='Controls/views/file_selection';
    function __construct($sName = '', $sCaption = '', $bIsRequired = true) {
        parent::__construct($sName, $sCaption, $bIsRequired);

        $d = opendir('../system/'.$this->sClassesPath);
        while($f=readdir($d)){
            if(substr($f,-10)=='.class.php' || file_exists($f)) {
                $aFiles[]=substr($f,0,-10);
            }
        }
        sort($aFiles);
        foreach($aFiles as $sFile){
            $this->addOption($sFile,$sFile);
        }
    }
}