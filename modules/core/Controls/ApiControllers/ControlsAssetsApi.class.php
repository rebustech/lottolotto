<?php

//namespace LL\VariantsCMS;
class ControlsAssetsApi extends \LL\AjaxController{

    function manyToManyControl(){
        header('Content-type: text/javascript');
        $aFiles=array(__DIR__.'/../scripts/many_to_many.js');
        die($this->_getFiles($aFiles));
    }

    function affiliatesManyToManyControl(){
        header('Content-type: text/javascript');
        $aFiles=array(__DIR__.'/../../Affiliates/Scripts/affiliates_builder.js');
        die($this->_getFiles($aFiles));
    }

    function campaignsControl(){
        header('Content-type: text/javascript');
        $aFiles=array(__DIR__.'/../../Campaigns/Scripts/jquery.autocomplete.min.js',
            __DIR__.'/../../Campaigns/Scripts/ajaxupload.js',
            __DIR__.'/../../Campaigns/Scripts/campaign_builder.js'

        );
        die($this->_getFiles($aFiles));
    }

}