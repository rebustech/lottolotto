<?php

/**
 * Lotto Exception is the Extended Exception to be used for the XLotto Platform.
 * NOTE this class maaybe further extended. to denote error severity.
 * @author Nick
 *
 *
 * MySQL Installer
 * CREATE TABLE `Exceptions` (
	`ID` INT(50) NOT NULL AUTO_INCREMENT,
	`TriggerTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`HTMLMessage` LONGTEXT NOT NULL,
	`Status` INT(1) ZEROFILL NOT NULL DEFAULT '0' COMMENT 'Current Status. 0 = Created, 1 = Viewed, 2 = Issue Identified, 3 = Issue being Fixed, 4 = Issue Resolved',
	PRIMARY KEY (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
ALTER TABLE `Exceptions` AUTO_INCREMENT = 1000;
 *
 * INSERT INTO `admin_pages` (`title`, `tablename`, `parent_id`) VALUES ('Exception Management', 'exceptions', 130);
 */
class LottoException extends Exception {



    protected $iID;
    protected $sMessage;

    //var $



    public function __construct($sCode = NULL, $sMessage = NULL) {



        /*build the exception message */
        $this->sMessage .= "<h1>Exception Thrown</h1><p><strong>".$sCode."</strong>".$sMessage."</p>";

        $aVarsAtThrow['P'] = $_POST;
        $aVarsAtThrow['G'] = $_GET;
        $aVarsAtThrow['S'] = $_SERVER;
        $aVarsAtThrow['SS'] = $_SESSION;
        $aVarsAtThrow['C'] = $_COOKIE;
        $aVarsAtThrow['F'] = $_FILES;
        $aVarsAtThrow['DV'] = get_defined_vars();
        $aConstants = get_defined_constants(true);
        $aVarsAtThrow['CO'] = $aConstants['user'];
        if(function_exists('getallheaders')) $aVarsAtThrow['HE'] = getallheaders();
        if(function_exists('get_browser')) $aVarsAtThrow['B'] = get_browser(null, true);

        unset($aVarsAtThrow['HE']['Cookie']);


        /* Processing Stack trace */
        $aStackTrace = debug_backtrace();
        array_shift($aStackTrace);

        foreach ($aStackTrace as $iLevel => $aLevelInfo) {
            $this->sMessage .= "#" . $iLevel . ": On Line " . $aLevelInfo['line'] . " Caller->  " . $aLevelInfo['class'] . "->" . $aLevelInfo['function'] . "() </br>";
        }

        /* Processing Environment Variable */
        $this->sMessage .= "<h2>Environment Variables</h2>";

        $this->addArrayInfo($aVarsAtThrow['P'], "Get Variables");
        $this->addArrayInfo($aVarsAtThrow['G'], "Post Variables");
        $this->addArrayInfo($aVarsAtThrow['S'], "Server Variables");
        $this->addArrayInfo($aVarsAtThrow['SS'], "Session Variables");
        $this->addArrayInfo($aVarsAtThrow['C'], "Cookies");
        $this->addArrayInfo($aVarsAtThrow['F'], "Posted Files");
        $this->addArrayInfo($aVarsAtThrow['DV'], "Other Defined Variables");
        $this->addArrayInfo($aVarsAtThrow['CO'], "Constants");
        $this->addArrayInfo($aVarsAtThrow['HE'], "Input headers");
        $this->addArrayInfo($aVarsAtThrow['B'], "Browser Information");

        /*
         * Would thrown an excception if the insert failed, but yeah.
         * Add Exception into DB.
         */

        $aFields['HTMLMessage'] = $this->sMessage;

        $iInsertID = DAL::Insert("Exceptions", $aFields, true);
        $this->iID = rand()."-".$iInsertID."-".rand();

    }

    private function getFrontFacingErrorPageContent(){
            /*
             * User Based Exception Message:
             * Clear all page content to prevent hackery (If there ever was any..)
             * New Blurred version of site as backround.
             * Centralised Div with error message and link back to home.
             */
            if(function_exists('getallheaders')) $aHeaders = getallheaders();
            $sBrowser = $aHeaders['User-Agent'];


            $return = '<script>$("html").empty();</script>'
                    . '<div style="width:100%; height:100%; position:fixed; left:0; top:0; z-index:-1;"><img src="ExceptionBackground.png" style="width:100%; height:100%;"/></div>'
                    . '<div style="width:900px; margin: 0 auto; margin-top:200px; padding:30px; background-color:#fff; border: 10px solid rgba(200, 200, 200, .5); -webkit-background-clip: padding-box;  background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */ box-shadow:5px 5px 5px;">'
                    . '<h1>Sorry!</h1>'
                    . "<h3>We we're unable to complete this request successfully.</h3>"
                    . '<p>'
                    . 'If you find that this problem persists, you may like to <a href="#">contact us</a> for support.<br/>'
                    . 'When contacting us for support, please provide as much of the following information as possible -- in particular the <strong>error code</strong><br/><br/><br/>'
                    . '<strong> Error Code </strong>'.$this->iID.'<br/><br/>'
                    . '<strong> Date </strong>'.date('d-m-Y').'<br/><br/>'
                    . '<strong> Time </strong>'.date('H:i:s').'<br/><br/>'
                    . '<strong> URI </strong>'.$_SERVER['REQUEST_URI'].'<br/><br/>'
                    . '<strong> Your Browser </strong>'.$sBrowser
                    . '</p>'
                    . "</div>";
            return $return;
    }

    private function addArrayInfo($array, $name) {
        $this->sMessage .= "<h3>" . $name . "</h3>";
        if ($array) {
            foreach ($array as $sArrayKey => $sArrayItem) {
                $this->sMessage .= "&nbsp;&nbsp;&nbsp;<strong>" . ucfirst($sArrayKey) . "</strong>: ";
                if (is_object($sArrayItem)) {
                   $this->sMessage .= var_export($sArrayItem, true);
                } else {
                    $this->sMessage .= $sArrayItem . "<br/>";
                }
            }
        } else {
            $this->sMessage .= "No " . $name;
        }
    }

    public function getExceptionMessage(){
        /*
        *Account + Permission based Exception printing.
        */
        if(isset($_SESSION['AdSecurityObject'])){
            return $this->sMessage;
        }else{
            return $this->getFrontFacingErrorPageContent().$this->sMessage;
        }
    }

}