<?php
/**
 * Installs the exception handling module.
 *
 * @package LoveLotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\Exceptions;

class Installer{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs=array();
        $dVersion=self::getModuleVersion('Exceptions');
        if($dVersion<1){
            $aJobs[]=new \LL\Installer\Task('LL\Exceptions\Installer', 'installDatabase', 'Add database changes for exceptions');
        }

        return $aJobs;
    }

    static function installDatabase(){
        $sSQL="
        CREATE TABLE `Exceptions` (
                `ID` INT(50) NOT NULL AUTO_INCREMENT,
                `TriggerTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `HTMLMessage` LONGTEXT NOT NULL,
                `Status` INT(1) ZEROFILL NOT NULL DEFAULT '0' COMMENT 'Current Status. 0 = Created, 1 = Viewed, 2 = Issue Identified, 3 = Issue being Fixed, 4 = Issue Resolved',
                PRIMARY KEY (`ID`)
        )
        COLLATE='latin1_swedish_ci'
        ENGINE=InnoDB";
        \DAL::Query($sSQL);
        \DAL::Query("ALTER TABLE `Exceptions` AUTO_INCREMENT = 1000;");
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Exception Management'")>0){
            $iAdminSectionId=\DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='Admin'");
            \DAL::Query("INSERT INTO `admin_pages` (`title`, `tablename`, `parent_id`) VALUES ('Exception Management', 'exceptions', $iAdminSectionId)");
        }

        self::installModule('Exceptions',1);
    }
}