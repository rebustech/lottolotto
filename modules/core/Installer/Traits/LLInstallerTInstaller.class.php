<?php

/**
 * Helper class for installation classes
 */

namespace LL\Installer;

trait TInstaller{
    static $aMessages;
    static $aTasks=array();
    static $oCurrentTask;

    /**
     * Used to track which classes have had their install methods added to
     * the self::$aTasks array so that we don't add them more than once
     * @var array
     */
    static $aTaskClasses=array();
    var $sModuleName='Installer';

    /**
     * This will run all available installs
     */
    static function refreshAll(){
        //Now recursively find class files run any installers found
        self::refreshAllInPath(\Config::$config->sServerPath.'/../../../');
    }

    /**
     * Runs all installers in the given path
     * @param string $sPath
     */
    static function refreshAllInPath($sPath){
        //Make sure the installer itself is installed and updated first
        self::$aTasks=array();
        self::$aTasks['tasks']=array();
        \LLCache::addObject('InstallTasks', self::$aTasks);

        /**
         * Make sure we have the installer, handlers and modules as a priority
         * You can use this syntax in your own installers if you require a particular
         * installer to be ran before yours
         */
        self::requires('LL\Installer\Installer');
        self::requires('LL\Module');
        self::requires('LL\Handler');

        /**
         * Recursivly run through all the files in the directory provided looking
         * for installable files.
         */
        $aFiles=self::getClassFiles($sPath);
        foreach ($aFiles as $sClass=>$sFileName) {
            if(strstr($sFileName,'Installer')>-1){
                try{
                    //Have to examine the file to decide if we need to instantiate it
                    $sFileData=file_get_contents($sFileName);
                    //Work out the classname including namespace
                    preg_match('/^namespace (.*?);/m',$sFileData,$aNameSpaceMatches);
                    if(sizeof($aNameSpaceMatches)>0){
                        $sNameSpace='\\'.$aNameSpaceMatches[1];
                    }else{
                        $sNameSpace='';
                    }
                    preg_match_all('/^(class|trait) (.*?)[\{\s]/m',$sFileData,$aClassNameMatches,PREG_SET_ORDER);
                    //preg_match_all('/__construct\((.*?)[,\)]/m',$sFileData,$aConstructorInfo,PREG_SET_ORDER);
                    if($aClassNameMatches[0][1]=='class'){
                        $sClassName=$sNameSpace.'\\'.$aClassNameMatches[0][2];
                        self::addClassTasks($sClassName);
                    }
                }catch(Exception $e){
                    die($e->getMessage());
                }
            }
        }

        self::$aTasks['jobs']=sizeof(self::$aTasks['tasks']);
        self::$aTasks['complete']=0;
        self::$aTasks['errors']=0;

        if(self::$aTasks['jobs']==0){
            die('System up to date');
        }else{
            try{
                \LLCache::addObject('InstallTasks', self::$aTasks);
                self::processTasks(self::$aTasks['tasks']);
                die('Installation Complete');
            }catch(Exception $e){
                die($e->getMessage);
            }
            die('Installation Complete');
        }
    }

    static function addClassTasks($sClassName){
        if(!isset(self::$aTaskClasses[$sClassName])){
            self::$aTaskClasses[$sClassName]=true;
            if(method_exists($sClassName,'getInstallTasks')){
                $aNewTasks=$sClassName::getInstallTasks();
                if(is_array($aNewTasks)){
                    self::$aTasks['tasks']=array_merge(self::$aTasks['tasks'],$aNewTasks);
                }
            }
        }
    }

    static function requires($sClassName){
        self::addClassTasks($sClassName);
    }

    static function getClassFiles($sPath,$aExistingFiles=null){
        $hDir=opendir($sPath);
        while($sFileName=readdir($hDir)){
            $sFullFileName=$sPath.'/'.$sFileName;
            if($sFileName!='.' && $sFileName!='..'){
                if(is_dir($sFullFileName)){
                    $aExistingFiles=self::getClassFiles($sFullFileName,$aExistingFiles);
                }else{
                    if(substr($sFileName,-10)=='.class.php'){
                        $aExistingFiles[substr($sFileName,0,-10)]=$sFullFileName;
                    }
                }
            }
        }
        return $aExistingFiles;
    }

    protected static function processTasks($aTasks){
        foreach($aTasks as $oTask){
            self::$oCurrentTask=$oTask;
            try{
                self::updateTaskStatus($oTask,'Starting');
                $sJobClass=$oTask->sClassName;
                $sJobMethod=$oTask->sMethod;
                self::updateTaskStatus($oTask,'Checking for class '.$sJobClass);
                if(class_exists($sJobClass)){
                    self::updateTaskStatus($oTask,'Installing');
                    $sJobClass::$sJobMethod($oTask);
                    self::updateTaskStatus($oTask,'Done');
                }else{
                    self::updateTaskStatus($oTask,'Error - Class '.$sJobClass.' not found');
                }
            }catch(Exception $e){
                self::updateTaskStatus($oTask,'Error');
            }
            self::$aTasks['complete']++;
            \LLCache::addObject('InstallTasks', self::$aTasks);
        }
    }


    static function updateTaskStatus($oTask,$sStatus){
        if($oTask===null) $oTask=self::$oCurrentTask;
        if(is_object($oTask)){
            $oTask->setStatus($sStatus);
            \LLCache::addObject('InstallTasks', self::$aTasks);
        }
    }

    /**
     * Installs a new handler. These are classes that are used by various parts of
     * the system to send processing into a class of the same name as the handler
     * @param string $sHandlerClass
     * @param string $sName
     * @param string $sType
     * @param string $sFilename
     */
    static function installHandler($sHandlerClass,$sName,$sType,$sFilename,$dVersion=1.0){
        self::updateTaskStatus(null,'Installing Handler '.$sName.' Version '.$dVersion);

        $aFields['class']=$sHandlerClass;
        $aFields['name']=$sName;
        $aFields['type']=$sType;
        $aFields['version']=$dVersion;

        \DAL::Insert('handlers', $aFields);
    }

    static function addMessage($message){
        self::$aMessages[]=$message;
    }

    /**
     * Installs a new module into the modules table
     * @param type $sModuleName
     */
    static function installModule($sModuleName,$sVersion='1',$iModuleType=1){
        self::updateTaskStatus(null,'Registering Module '.$sModuleName.' Version '.$sVersion);


        $iModuleId=\DAL::executeGetOne("SELECT id FROM modules WHERE module_name='{$sModuleName}'");

        if($iModuleId==0){
            \DAL::Insert('modules', array('module_name'=>$sModuleName,'fk_module_type_id'=>$iModuleType,'version'=>$sVersion));
        }else{
            \DAL::Update('modules', array('module_name'=>$sModuleName,'fk_module_type_id'=>$iModuleType,'version'=>$sVersion),'id='.$iModuleId);
        }
    }

    static function getModuleVersion($sModuleName){
        self::updateTaskStatus(null,'Checking module version '.$sModuleName);

        $dVersion=doubleval(\DAL::executeGetOne("SELECT coalesce(max(version),0) FROM modules WHERE module_name='{$sModuleName}'"));
        return $dVersion;
    }


    /**
     * Override this to create your installer
     */
    function getJobs(){

    }
}
