<?php
/**
 * Installs the core module. This is just an entry in the modules table for completness
 * purposes only
 *
 * @package LoveLotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\Core;

class Installer{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        $aJobs[]=new \LL\Installer\Task('\LL\Core\Installer', 'installCoreModule', 'Add Core Module');
        return $aJobs;
    }

    static function installCoreModule(){
        self::installModule('Core',1);
    }
}