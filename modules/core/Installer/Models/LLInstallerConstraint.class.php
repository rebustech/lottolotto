<?php
/**
 * Model for creating and maintaining table constraints as part of the installer
 * package
 *
 * @package Lovelotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\Installer;

class Constraint{
    var $sKeyName;
    var $aFieldNames;
    var $sTableField;
    var $sForeignTable;
    var $sForeignField;
    var $sOnUpdate;
    var $bDrop=false;

    /**
     * Create a new constraint that can be added to a Table object. Key names
     * are assigned automatically from a uniqid field because mySql has some
     * serious issues with adding constraints to a table that already has
     * constraints
     * @param string $sKeyName Name of the key
     * @param string $sFieldNames Comma seperated list of field names that form the key
     */
    function __construct($sTableField,$sForeignTable,$sForeignField,$sKeyName=null,$sOnUpdate='CASCADE'){
        if($sKeyName==null) $sKeyName='AG'.uniqid();
        $this->sKeyName=$sKeyName;
        $this->sTableField=$sTableField;
        $this->sForeignTable=$sForeignTable;
        $this->sForeignField=$sForeignField;
        $this->sOnUpdate=$sOnUpdate;
    }

    /**
     * Creates the SQL required for adding a constraint to a table
     * @return string
     */
    function getCreateSql(){
        /**
         * This is what the SQL looks like:
         * CONSTRAINT `group` FOREIGN KEY (`fk_rights_group_id`) REFERENCES `access_rights_groups` (`id`) ON UPDATE CASCADE
         */
        $sSQL='CONSTRAINT `'.$this->sKeyName.'` ';
        $sSQL.='FOREIGN KEY (`'.$this->sTableField.'`) ';
        $sSQL.='REFERENCES `'.$this->sForeignTable.'` (`'.$this->sForeignField.'`) ';
        $sSQL.='ON UPDATE '.$this->sOnUpdate;

        return $sSQL;
    }

    function drop(){
        $this->bDrop=true;
    }
}