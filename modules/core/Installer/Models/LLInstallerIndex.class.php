<?php
/**
 * Model for creating and maintaining table structures as part of the installer
 * package
 *
 * @package Lovelotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\Installer;

class Index{
    var $aFieldNames;
    var $sKeyName;
    var $bDrop=false;

    /**
     * Create a new key on the table
     * @param string $sKeyName Name of the key
     * @param string $sFieldNames Comma seperated list of field names that form the key
     */
    function __construct($sKeyName,$sFieldNames){
        $this->sKeyName=$sKeyName;
        $this->aFieldNames=explode(',',$sFieldNames);
    }

    function getCreateSql(){
        $sSQL='KEY `'.$this->sKeyName.'` ';
        $sSQL.='(`'.implode('`,`',$this->aFieldNames).'`)';

        return $sSQL;
    }

    function drop(){
        $this->bDrop=true;
    }
}