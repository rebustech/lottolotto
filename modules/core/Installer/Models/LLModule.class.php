<?php

namespace LL;

class Module extends \LLModel{
    use Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new Installer\Task('\LL\Module', 'installModuleTypesTable', 'Install module types');
        $aJobs[]=new Installer\Task('\LL\Module', 'installModulesTable', 'Install modules database');
        return $aJobs;
    }

    static function installModuleTypesTable(Installer\Task $oTask){
        try{
            $oModuleTypesTable=new Installer\Table('module_types');
            $oModuleTypesTable->addIdField();
            $oModuleTypesTable->addField(new Installer\Field('name','VARCHAR',20));
            $oModuleTypesTable->compile();
            \DAL::Insert('module_types', array('id'=>1,'name'=>'Module'),false);
            \DAL::Insert('module_types', array('id'=>2,'name'=>'Custom Module'),false);
            \DAL::Insert('module_types', array('id'=>3,'name'=>'Control'),false);
            \DAL::Insert('module_types', array('id'=>4,'name'=>'Frontend Smarty Object'),false);

        }catch(Exception $e){

        }
    }

    static function installModulesTable(Installer\Task $oTask){
        try{
            $oModulesTable=new Installer\Table('modules');
            $oModulesTable->addIdField();
            $oModulesTable->addField(New Installer\Field('module_name','VARCHAR',60));
            $oModulesTable->addField(New Installer\Field('fk_module_type_id','INT',11));
            $oModulesTable->addField(New Installer\Field('version','double'));
            $oModulesTable->addIndex(New Installer\Index('test','module_name,version'));
            $oModulesTable->addConstraint(New Installer\Constraint('fk_module_type_id', 'module_types', 'id'));
            $oModulesTable->compile();

            self::installModule('Modules',2);
        }catch(Exception $e){

        }
    }
}
