<?php
/**
 * Model for creating and maintaining fields that can be added to Table objects
 *
 * @package Lovelotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\Installer;

class Field{
    var $sFieldName;
    var $sFieldType;
    var $sFieldSize;
    var $bIsNullable=true;
    var $sDefault;
    var $bAutoNumber;
    var $bDrop=false;

    /**
     * Create the field
     * @param string $sFieldName Name of the field. Note: this cannot be changed once you've created a field
     * @param string $sFieldType Type of field without size (i.e. VARCHAR,INT,TINYINT,BLOB etc...)
     * @param string $sFieldSize Optional Size of the field
     * @param boolean $bIsNullable Optional Default is true, so if you want to make a field required at database level set this to false
     * @param string $sDefault Optional Use this to set the default value of the field
     * @param boolean $bAutoNumber Optional Set to true to make this field an autonumber. Note - see Table->addIdField() for a quick way to add an autonumber field called ID
     */
    function __construct($sFieldName,$sFieldType,$sFieldSize=null,$bIsNullable=true,$sDefault=null,$bAutoNumber=false){
        $this->sFieldName=$sFieldName;
        $this->sFieldType=$sFieldType;
        $this->sFieldSize=$sFieldSize;
        $this->bIsNullable=$bIsNullable;
        $this->sDefault=$sDefault;
        $this->bAutoNumber=$bAutoNumber;
    }

    /**
     * Generates the SQL required to add this field to a table
     * @return string
     */
    function getCreateSql(){
        $sSQL='`'.$this->sFieldName.'` '.$this->sFieldType;
        if($this->sFieldSize!==null) $sSQL.='('.$this->sFieldSize.')';
        $sSQL.=($this->bIsNullable)?' NULL ':' NOT NULL ';
        if($this->sDefault!==null) $sSQL.=" DEFAULT '{$this->sDefault}'";

        if($this->bAutoNumber) $sSQL.=' AUTO_INCREMENT ';

        return $sSQL;
    }

    function drop(){
        $this->bDrop=true;
    }
}