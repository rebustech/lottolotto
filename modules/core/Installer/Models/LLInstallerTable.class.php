<?php
/**
 * Model for creating and maintaining table structures as part of the installer
 * package
 *
 * @package Lovelotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\Installer;

class Table{
    var $sTableName;
    var $aFields;
    var $aKeys;
    var $aPrimaryKeyFields;
    var $aConstraints;

    function __construct($sTableName){
        $this->sTableName=$sTableName;
    }

    function compile(){
        /**
         * Check if the table exists, if it doesn't create it using the definition
         * if it does run an alter table so it matches the definition here
         */
        try{
            $bFound=false;
            $sSQL='SHOW TABLES';
            $aData=\DAL::executeQuery($sSQL);
            foreach($aData as $aSearch){
                $t=array_pop($aSearch);
                if($t==$this->sTableName) $bFound=true;
            }
            
            if($bFound){
                $this->updateTable();
            }else{
                $this->createTable();
            }
        }catch(Exception $e){
            $this->createTable();
        }
    }

    protected function createTable(){
        /**
         * Collect up all the instructions for creating the table then build the
         * SQL by imploding them together
         */
        foreach($this->aFields as $oField){
            $aInstructions[]=$oField->getCreateSql();
        }

        /**
         * Primary Key
         */
        if(is_array($this->aPrimaryKeyFields)){
            $aInstructions[]='PRIMARY KEY (`'.implode('`,`',$this->aPrimaryKeyFields).'`)';
        }

        /**
         * Indexes and foreign key constraints
         */
        if(is_array($this->aKeys)){
            foreach($this->aKeys as $oField){
                $aInstructions[]=$oField->getCreateSql();
            }
        }
        /*
        if(is_array($this->aConstraints)){
            foreach($this->aConstraints as $oField){
                $aInstructions[]=$oField->getCreateSql();
            }
        }*/

        $sSQL='CREATE TABLE `'.$this->sTableName.'` ('.implode(', ',$aInstructions).');';

        \DAL::executeQuery($sSQL);

        $this->updateTable();
    }

    protected function updateTable(){
        /**
         * Get the current table structure, then compare and create updates/adds
         */
        $aTableFields=\DAL::getTableDetails($this->sTableName);

        /**
         * Check the fields.
         * Add any new fields and update any where the defs don't match
         */
        foreach($this->aFields as $oField){
            if($oField->bDrop===true){
                 if(isset($aTableFields->aFields[$oField->sFieldName])){
                     $aInstructions[]=' DROP COLUMN `'.$oField->sFieldName.'`';
                 }
            }else{
                if(isset($aTableFields->aFields[$oField->sFieldName])){
                    $oExistingField=$aTableFields->aFields[$oField->sFieldName];
                    if(strtolower($oExistingField->sType)!=strtolower($oField->sFieldType) || intval($oExistingField->iLength)!=intval($oField->sFieldSize)){
                        $aInstructions[]=' CHANGE COLUMN `'.$oField->sFieldName.'` '.$oField->getCreateSql();
                    }
                }else{
                    $aInstructions[]=' ADD COLUMN '.$oField->getCreateSql();
                }
            }
        }

        /**
         * Drop all foreign keys !!!
         */
        if(is_array($aTableFields->aConstraints)){
            foreach($aTableFields->aConstraints as $sConstraintName=>$oConstraint){
                $aInstructions[]=' DROP FOREIGN KEY `'.$sConstraintName.'`';
            }
        }

        /**
         * Reapply foreign keys
         */
        if(is_array($this->aConstraints)){
            foreach($this->aConstraints as $oField){
                $aInstructions[]=' ADD '.$oField->getCreateSql();
            }
        }

        /**
         * Drop or add indexes - note we can't update - too much hassle at this point in time
         */
        if(is_array($this->aKeys)){
            foreach($this->aKeys as $oKey){
                if($oKey->bDrop===true){
                    $aInstructions[]='DROP KEY `'.$oKey->sKeyName.'`';
                }else{
                    if(!isset($aTableFields->aKeys[$oKey->sKeyName])){
                        $aInstructions[]='ADD `'.$oKey->getCreateSql();
                    }
                }
            }
        }
        if(sizeof($aInstructions)>0){
            $sSQL='ALTER TABLE `'.$this->sTableName.'` '.implode(', ',$aInstructions).';';
            \DAL::executeQuery($sSQL);
        }

    }

    function addField($oField){
        $this->aFields[$oField->sFieldName]=$oField;
        return $oField;
    }

    function addIdField($sFieldName='id'){
        $oField=$this->addField(new Field($sFieldName,'INT',11,false,null,true));
        $this->aPrimaryKeyFields=array('id');
        return $oField;
    }

    function dropField($sFieldName){
        $oField=$this->aFields[$oField->sFieldName];
        $oField->bDrop=true;
        return $oField;
    }

    function addIndex($oIndex){
        $this->aKeys[$oIndex->sKeyName]=$oIndex;
        return $oIndex;
    }

    function dropIndex($sFieldName){
        $oIndex=$this->aKeys[$oIndex->sKeyName];
        $oIndex->bDrop=true;
        return $oIndex;
    }

    function addConstraint(Constraint $oConstraint){
        $this->aConstraints[$oConstraint->sKeyName]=$oConstraint;
        return $oConstraint;
    }

    function dropConstraint($sConstraintName){
        $oConstraint=$this->aConstraints[$sConstraintName];
        $oConstraint->bDrop=true;
        return $oConstraint;
    }
}