<?php

namespace LL\Installer;

class Task{
    var $sClassName;
    var $sMethod;
    var $sName;
    var $sStatus='Pending';
    var $iId;

    /**
     * Creates a new task with a status of Pending for the installer to run
     * @param object $oObject
     * @param string $sMethod
     * @param string $sName
     */
    function __construct($sClassName,$sMethod,$sName){
        $this->sClassName=$sClassName;
        $this->sMethod=$sMethod;
        $this->sName=$sName;
        $this->iId=uniqid();
    }

    function setStatus($sStatus){
        $this->sStatus=$sStatus;
    }
}