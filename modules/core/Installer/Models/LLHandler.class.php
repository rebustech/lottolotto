<?php
/**
 * Handlers are used by HandleSelectControl objects to show a list of available
 * handlers to objects such as GameEngineComponents and TicketEngines
 * @package LoveLotto
 * @subpackage Installer
 * @author J.Patchett
 */
namespace LL;

class Handler extends \LLModel{
    use Installer\TInstaller;

    var $class;
    var $name;
    var $type;
    var $file;
    var $version;

    /**
     * Gets an array of handlers of the given type
     * @param type $sType
     * @return type
     */
    static function getHandlersOfType($sType){
        //Check memcache first
        $sSQL='SELECT * FROM handlers WHERE type=\''.$sType.'\' ORDER BY name';
        return \DAL::executeQuery($sSQL);
    }

    //--- INSTALLER ----------------------------------------------------------//

    /**
     * Gets a list of jobs to perform for this installer
     * @return array
     */
    static function getInstallTasks(){
        $iVersion=self::getModuleVersion('Handlers');
        if($iVersion<1){
            $aJobs[]=new Installer\Task('\LL\Handler', 'installHandlersTable', 'Install handlers database');
        }
        return $aJobs;
    }

    /**
     * Callback to create the handlers table and install the handlers module
     * @param \LL\Installer\Task $oTask
     */
    static function installHandlersTable(Installer\Task $oTask){
        try{
            $oModulesTable=new Installer\Table('handlers');
            $oModulesTable->addIdField();
            $oModulesTable->addField(New Installer\Field('class','VARCHAR',80));
            $oModulesTable->addField(New Installer\Field('name','VARCHAR',80));
            $oModulesTable->addField(New Installer\Field('type','VARCHAR',80));
            $oModulesTable->addField(New Installer\Field('file','VARCHAR',255));
            $oModulesTable->addField(New Installer\Field('version','double'));
            $oModulesTable->compile();

            /**
             * Set the handlers module to the current version
             */
            self::installModule('Handlers',1);
        }catch(Exception $e){

        }
    }
}
