<?php
/**
 * Installer admin controller
 * Provides one admin url which is /administration/installer
 * @package LoveLotto
 * @subpackage AdminControllers
 */
class installer extends AdminController{
    /**
     * Decide what to show in a generic details view
     */
    function index(){
        $tabs=new TabbedInterface();

        //Create a tab to show assigned users
        $oItemsTab=new TabbedInterface(lang::get('actions'));
        $oItemsTab->sIcon='fa-magnet';
        $oItemsTab->AddControl(new HtmlTagControl('h2','Installer'));

        /**
         * Create a link to the installer
         */
        $oInstallLink=new HtmlTagControl('a','Install / Update all modules');
        $oInstallLink->addParam('href', '#');
        $oInstallLink->addParam('class', 'button installer_install');
        $oItemsTab->AddControl($oInstallLink);

        /**
         * Add this tab to the list of tabs
         */
        $tabs->AddControl($oItemsTab);

        LLResponse::$sPostBody.=LLResponse::addScript('/administration/API/LLInstallerAssets/js');

        return $tabs->Output();

    }

}