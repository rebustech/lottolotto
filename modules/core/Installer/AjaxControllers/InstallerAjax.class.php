<?php

class InstallerAjax extends \LL\AjaxController{
    var $bAjax=true;

    /**
     * /administration/API/InstallerAjax/status
     *
     * This gets a list of all the jobs on the current installation run along with
     * the current status. This data is pulled from memcache key 'InstallTasks' which
     * is updated by the installer.
     */
    function status(){
        echo json_encode(LLCache::getObject('InstallTasks'));
        die();
    }

    /**
     * /administration/API/InstallerAjax/run
     * This runs the installer
     */
    function run(){
        try{
            error_reporting(E_ERROR);
            ini_set('display_errors',1);
            \LL\Installer\Installer::refreshAll();
        }catch(Exception $e){
            //This should get logged
            die($e->getMessage());
        }
    }
}
