<?php
/**
 * Installs the installer
 * @package LoveLotto
 * @subpackage Installer
 */

namespace LL\Installer;

class Installer{
    use TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    static function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        $aJobs[]=new Task('\LL\Installer\Installer', 'installAdminMenus', 'Add installer to admin menus');
        return $aJobs;
    }

    /**
     * Adds the installer,module and handlers menu options to the main admin menu
     */
    static function installAdminMenus(Task $oTask){
        $iAdminSectionId=\DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='Admin'");
        //Get the ID of the admin section
        if(\DAL::Query("SELECT * FROM admin_pages WHERE filename='installer'")==0){
            \DAL::Insert('admin_pages', array('title'=>'Installer','filename'=>'installer','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Modules'")==0){
            \DAL::Insert('admin_pages', array('title'=>'Modules','filename'=>'generic-listing.php','tablename'=>'modules','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Handlers'")==0){
            \DAL::Insert('admin_pages', array('title'=>'Handlers','filename'=>'generic-listing.php','tablename'=>'handlers','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }

        self::installModule('Installer',2);

    }
}
