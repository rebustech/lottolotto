LLInstaller={
    setup:function(){
        $('.installer_install').click(LLInstaller.run)
                .after('<div id="LLInstallerStatusPanel" style="padding-top:20px;"><span style="font-size:28px;">0%</span><div class="ProgessBar" style="postion:relative;font-size:0;height:8px;margin:10px 0;background:#ccc;"><span style="background:#1972B8;display:inline-block;height:8px;">&nbsp;</span></div><table></table></div>');
    },
    run:function(){
        $('#LLInstallerStatusPanel > span').text('Preparing');
        $('#LLInstallerStatusPanel table tr').remove();
        $.get('/administration/API/InstallerAjax/run',function(data){
            if(data!='') alert(data);
        });
        setTimeout(LLInstaller.getStatus,200);
    },
    createTask:function(data){
        var sTrId=data.iId;
        if($('#LLInstallerStatusPanel tr[data-id='+sTrId+']').length==0){
            var eTr=$('<tr></tr>');
            eTr.append('<td/><td/><td/>');
            eTr.attr('data-id',sTrId);
            $('#LLInstallerStatusPanel table').append(eTr);
        }else{
            var eTr=$('#LLInstallerStatusPanel tr[data-id='+sTrId+']')
        }
        eTr.find('> td:eq(1)').text(data.sName);
        eTr.find('> td:eq(2)').text(data.sStatus);
    },
    getStatus:function(){
        $.getJSON('/administration/API/InstallerAjax/status',function(data){
            var iJobs=data['jobs'];
            var iCompleteJobs=data['complete'];

            if(typeof data['jobs'] == 'undefined'){
                iJobs=1;
                iCompleteJobs=0;
            }

            /**
             * Update the progress bar
             */
            var iPercent=(100 / iJobs) * iCompleteJobs;
            var sPercent=iPercent;
            if(iPercent>0){
                var iPercent=(100 / iJobs) * (iCompleteJobs+2);
                if(iPercent>100) iPercent=100;
            }
            $('#LLInstallerStatusPanel > span').text(parseInt(sPercent)+'%');
            $('#LLInstallerStatusPanel div span').css('width',parseInt(iPercent)+'%');

            /**
             * Update the tasks list
             */
            for(iTaskNumber in data['tasks']){
                LLInstaller.createTask(data['tasks'][iTaskNumber]);
            }

            /**
             * If there's still work outstanding get another update in 200ms
             */
            if(iCompleteJobs<iJobs){
                setTimeout(LLInstaller.getStatus,200);
            }
        })
    }
}

$(document).ready(function(){
    LLInstaller.setup();
})