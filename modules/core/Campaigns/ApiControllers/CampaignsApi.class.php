<?php
use LL\AjaxController;

/**
 * Class CampaignsApi
 * @property mixed oCampaignRecipientsValidator
 * @property CampaignPageContentValidator oCampaignPageContentValidator
 */
class CampaignsApi extends AjaxController{

    /**
     * @var bool
     */
    protected $bPageContentValidation=true;

    /**
     * @var bool
     */
    protected $bRedemptionValidation=true;


    /**
     * @var bool
     */
    protected $bRecipientsValidation=true;

    /**
     * @var CampaignOptions
     */
    protected  $campaignOptions;

    /**
     * @var
     */
    public static $campaignHelper;

    /**
     * @var CampaignOptionsValidator
     */
    protected $oCampaignOptionsValidator;

    /**
     * @var CampaignRedemptionValidator
     */
    protected $oCampaignRedemptionValidator;

    /**
     * @var CampaignRecipients
     */
    protected $campaignRecipients;

    /**
     *
     */
    public function __construct(){

        $this->campaignPageContent=new CampaignPageContent();

        $this->campaignRedemption=new CampaignRedemptions();

        $this->campaignOptions=new CampaignOptions();

        $this->campaign=new CampaignEntity();

        $this->campaignRecipients=new CampaignRecipients();

        static::$campaignHelper=new CampaignHelpers();

        $this->oCampaignRedemptionValidator=new CampaignRedemptionValidator($_POST);

        $this->oCampaignPageContentValidator=new CampaignPageContentValidator($_POST);
    }


    /**
     * @return string
     */
    public function getControl(){

        $iCampaignId=self::$campaignHelper->extractCampaignId();

        $sMethodName=self::$campaignHelper->extractMethodName();
        if(!method_exists($this->campaignOptions,$sMethodName) && !method_exists($this->campaignRecipients,$sMethodName)){
            die('404');
        }

        switch($sMethodName){
            case 'getNewCampaignOptions':
                $result=($iCampaignId) ? $this->campaignOptions->{$sMethodName}($iCampaignId,false) : $this->campaignOptions->{$sMethodName}();
                break;
            case 'getNewCampaignRecipients':
                $result= $this->campaignRecipients->{$sMethodName}($iCampaignId);

                break;
            case 'getRecipientsPlayersMatchControl':
                $result= $this->campaignRecipients->{$sMethodName}($iCampaignId);

                break;

            case 'getConditionControls':
                $result= $this->campaignRecipients->{$sMethodName}($_GET['field']);

                break;
            default:
                $result=$this->campaignOptions->{$sMethodName}();
        }

        $json=json_encode($result);

        return $json;
    }

    public function handleCampaignContentPage(){

        /*
        |--------------------------------------------------------------------------
        | If all empty
        |--------------------------------------------------------------------------
        */
        if (!array_filter($_POST) && (!isset($_FILES) || empty($_FILES))){
            return json_encode(['errors'=>['Nothing to store']]);
        }
        /*
        |--------------------------------------------------------------------------
        | If files(image/icon) are sent parse and store
        |--------------------------------------------------------------------------
        */
        elseif(isset($_FILES) && !empty($_FILES)){
            return  $this->uploadCampaignContentImage(self::$campaignHelper->extractCampaignId());
        }
        else{
            //var_dump($_POST);
            if($this->bPageContentValidation){
                if(!$this->oCampaignPageContentValidator->passes($_POST)){

                    return json_encode($this->oCampaignPageContentValidator->errors());
                }
            }


            $this->campaignPageContent->storePageContentProperties($_POST['campaign_id'],$_POST);
        }
    }
    /**
     * @todo Implement manual validation to validator
     * @return string
     */
    public function handleCampaignRecipients(){

        /*
        |--------------------------------------------------------------------------
        | if import selected and all import fields empty return validation error
        |--------------------------------------------------------------------------
        */
        if((!isset($_FILES) || empty($_FILES))
            && (!isset($_POST['recipientsXls']) || empty($_POST['recipientsXls']))
            && (!isset($_POST['recipientsFromList']) || empty($_POST['recipientsFromList']))
            && (isset($_POST['campaignRecipientsChoiceOfEntry']) && $_POST['campaignRecipientsChoiceOfEntry']==2)){

            return json_encode(['errors'=>['Nothing to import']]);

        }

        /*
        |--------------------------------------------------------------------------
        | If files(csv) are sent parse and store
        |--------------------------------------------------------------------------
        */
        if(isset($_FILES) && !empty($_FILES)){
            return  $this->uploadRecipientsCsv(self::$campaignHelper->extractCampaignId());
        }
        /*
        |--------------------------------------------------------------------------
        | If excel data is sent parse and store
        |--------------------------------------------------------------------------
        */
        elseif(isset($_POST['recipientsXls']) && (!isset($_POST['recipientsFromList']) || (isset($_POST['recipientsFromList']) && empty($_POST['recipientsFromList'])))){
            if(empty($_POST['recipientsXls'])){
                return json_encode(['errors'=>['Data from excel is empty or invalid']]);
            }
            return $this->storeXlsOptions(self::$campaignHelper->extractCampaignId(),['recipientsXls'=>$_POST['recipientsXls']]);
        }
        /*
        |--------------------------------------------------------------------------
        | Fallback to default process and store
        |--------------------------------------------------------------------------
        */
        else{
            $aGroups=[];

            /*
            |--------------------------------------------------------------------------
            | Group $_POST data in arrays by master switch
            | e.g. Array
                (
                    [master_switch_0] => location
                    [master_switch_0_country_control] => 1
                    [master_switch_1] => language
                    [master_switch_1_language_control] => 1
                )
                BECOMES
                Array
                (
                    [1c32bd1f84613a90dbf06df52a409d80] => Array
                        (
                            [master_switch] => location
                            [country_control] => 1
                        )

                    [6ec0efb0fa20a41d73f498abb65ad546] => Array
                        (
                            [master_switch] => language
                            [language_control] => 1
                        )

                )
            |--------------------------------------------------------------------------
            */

            foreach($_POST AS $key=>$val){

                preg_match('/master\_switch\_\d+?$/iu',$key,$aMatches);
                if(!empty($aMatches)){
                    if(!isset($aGroups[$aMatches[0]])){

                        $k=md5($aMatches[0]);
                        $aGroups[$k]=[];
                        $z=preg_replace('/_\d+/iu','',$aMatches[0]);
                        $aGroups[$k][$z]=$val;
                    }
                }
                preg_match('/master\_switch\_\d+?\_/iu',$key,$bMatches);

                if(!empty($bMatches)){
                    $x=preg_replace('/master\_switch\_\d+\_/iu','',$key);

                    if(!isset($aGroups[$k][$x])){

                        $aGroups[$k][$x]=$val;
                    }

                }

            }

            /*
            * If validation enabled ... validate
            */
            if($this->bRecipientsValidation){

                if(isset($_POST['campaignRecipientsChoiceOfEntry']) && $_POST['campaignRecipientsChoiceOfEntry']==1){
                    if(empty($aGroups)){
                        return json_encode(['errors'=>[lang::get('No conditions added')]]);
                    }

                    /**
                     * For every group of elements built validate
                     */
                    foreach($aGroups as $group){

                        $group['campaign_id']=$_POST['campaign_id'];
                        $group['segment_name']=$_POST['segment_name'];

                        $this->oCampaignRecipientsValidator=new CampaignRecipientsValidator($group);
                        if(!$this->oCampaignRecipientsValidator->passes($group)){

                            return json_encode($this->oCampaignRecipientsValidator->errors());
                        }
                        unset($this->oCampaignRecipientsValidator);
                    }

                    //return false;
                }
                elseif(isset($_POST['campaignRecipientsChoiceOfEntry']) && $_POST['campaignRecipientsChoiceOfEntry']==2){
                    $this->oCampaignRecipientsValidator=new CampaignRecipientsValidator($_POST);

                    /*
                    |--------------------------------------------------------------------------
                    | If using the importer unset unneeded validation rules
                    |--------------------------------------------------------------------------
                    */
                    $currentRules=$this->oCampaignRecipientsValidator->getRules();


                    unset($currentRules['master_switch']);
                    unset($currentRules['segment_name']);

                    $this->oCampaignRecipientsValidator->setRules($currentRules);
                    if(!$this->oCampaignRecipientsValidator->passes($_POST)){

                        return json_encode($this->oCampaignRecipientsValidator->errors());
                    }
                }
                else{
                    return false;
                }

            }

            $iCampaignId=$_POST['campaign_id'];

            if(isset($_POST['campaignRecipientsChoiceOfEntry']) && $_POST['campaignRecipientsChoiceOfEntry']==2){
                $aGroups=$_POST;
                $sSegmentName=null;
                $iPlayersMatchValue=null;


            }
            else{
                $sSegmentName=$_POST['segment_name'];
                $iPlayersMatchValue=$_POST['players_match'];
            }


            $sStatusMessage=($this->campaignRecipients->storeRecipientOptions($iCampaignId,$aGroups,$sSegmentName,$iPlayersMatchValue))
                ? "Recipients Conditions Saved"
                : "Recipients Conditions Not Saved";

            return json_encode([lang::get($sStatusMessage)]);
        }

    }
    /**
     * @return string
     */
    public function handleCampaignRedemption(){

        /*
         * If validation enabled ... validate
         */
        if($this->bRedemptionValidation){
            if(!$this->oCampaignRedemptionValidator->passes($_POST)){

                return json_encode($this->oCampaignRedemptionValidator->errors());
            }
        }

        // filter redemption options from the post
        $aDefaultOptions=$this->campaignRedemption->aDefaultOptions;
        $iCampaignId=$_POST['campaign_id'];
        $aSaveOptions=[];

        foreach ($aDefaultOptions as $key => $values) {
            if(array_key_exists($key, $_POST)){
                $aSaveOptions[$key]=$_POST[$key];
            }
        }

        $sStatusMessage=($this->campaignRedemption->storeRedemptionOptions($iCampaignId,$aSaveOptions))
            ? "Redemption Options Saved"
            : "Redemption Options Not Saved";
        return json_encode([lang::get($sStatusMessage)]);

        // return json_encode([lang::get('Redemption Options Saved')]);

    }


    /**
     * @return mixed
     */
    public function handleCampaignOptions(){


        $aGroups=[];

        foreach($_POST AS $key=>$val){
            preg_match('/group\d+?/i',$key,$aMatches);
            if(!empty($aMatches)){
                if(!isset($aGroups[$aMatches[0]])){
                    $aGroups[$aMatches[0]]=[];
                }
                $aGroups[$aMatches[0]][$key]=$val;
            }

        }

        foreach($aGroups AS $aGroupsKey=> $aGroup){
            foreach($aGroup AS $aGroupKey=>$sGroupCondition){

                $sNewKey=strtolower(preg_replace('/group\d+?_Condition\d+?_/','',$aGroupKey));
                //echo $sNewKey;
                if(!empty($sNewKey)){
                    //$aGroups[$aGroupsKey][$sNewKey]=$sGroupCondition;
                    //unset($aGroups[$aGroupsKey][$aGroupKey]);
                    $aGroup[$sNewKey]=$sGroupCondition;
                    $aGroup['campaign_id']=(int)$_POST['iCampaignId'];
                    unset($aGroup[$aGroupKey]);

                }
            }
            $this->oCampaignOptionsValidator=new CampaignOptionsValidator($aGroup);

            if(!$this->oCampaignOptionsValidator->passes()){

                return json_encode($this->oCampaignOptionsValidator->errors());
            }

        }

        // Get POST vars values begining with "groupx_"
        $aOptionGroups = preg_grep("/group[0-9].*/i", array_keys($_POST));

        // get the value of the options POST array
        foreach ($aOptionGroups as $sPostKey) {

            $aOptionsSetupValues[$sPostKey] = $_POST[$sPostKey];
        }


        $this->campaignOptions->storeCampaignOptions((int)$_POST['iCampaignId'],$aOptionsSetupValues);

        return json_encode([lang::get("Campaign Options Saved")]);
    }

    /**
     * @return string
     * @todo Correct status codes
     */
    public function userPromoCode(){

        $oMember=unserialize($_SESSION['memberobject']);
        $iMemberId=$oMember->getMemberID();

        $response=new stdClass();
        if(isset($_POST['sPromoCode']) && is_numeric($iMemberId)){
            $sPromoCode=$_POST['sPromoCode'];


            if(is_numeric($iMemberId)){
                $sSql='SELECT COUNT(*) AS counter FROM members
                       WHERE member_id="'.mysql_escape_string($iMemberId).'"';

                $iMemberExists=\DAL::executeGetOne($sSql);

                if($iMemberExists>0){
                    $campaign=new campaigns;

                    # Need to re-open the session
                    ini_set('session.use_only_cookies', false);
                    ini_set('session.use_cookies', false);
                    ini_set('session.use_trans_sid', false);
                    ini_set('session.cache_limiter', null);

                    # Open a session
                    session_start();

                    $engineApi=new EngineApi();
                    $cart=$engineApi->setupCart();

                    $aItemsInCart=$cart->getCartDetails();

                    $aUserCampaigns=$campaign->userCampaigns($iMemberId,$aItemsInCart);

                    if(!empty($aUserCampaigns)){
                        $sSql='SELECT COUNT(*) AS counter FROM campaign_promo_codes
                       WHERE promo_code="'.mysql_escape_string($sPromoCode).'" AND fk_campaign_id IN ('.implode(',',$aUserCampaigns).')';

                        $iPromoExists=\DAL::executeGetOne($sSql);

                        if($iPromoExists){

                            $sSql='SELECT t3.name,t4.setup_config FROM campaigns t1
                                  JOIN campaign_promo_codes t2 ON t1.id=t2.fk_campaign_id
                                  JOIN promotion_types t3 ON t1.fk_promotion_type_id=t3.id
                                  JOIN promotion_configs t4 ON t1.id=t4.fk_campaign_id
                                  WHERE t2.promo_code=\''.mysql_escape_string($sPromoCode).'\'';
                            $aPromotionTypeInfoFetch=\DAL::executeGetRow($sSql);
                            $aPromotionTypeSetupConfig=json_decode($aPromotionTypeInfoFetch['setup_config']);

                            if(isset($aPromotionTypeSetupConfig->discount_amount) && isset($aPromotionTypeSetupConfig->discount_amount_type)){


                                
                                $fCartItemsCount = $aItemsInCart['items']; 

                                foreach($aItemsInCart AS $k=>$cartItem){

                                   if(isset($cartItem['data']['promo_code']) && $cartItem['data']['promo_code']==$sPromoCode){
                                       $response->status=404;
                                       $response->errors=['message'=>'Promo code already in cart'];
                                       return json_encode($response);
                                   }
                                }

                                $item = new CartItem();
                                $fAmountInCart=$cart->getCartTotal();
                                $fNewAmount=0;
                                $fPromoValue=0;
                                switch($aPromotionTypeSetupConfig->discount_amount_type){
                                    case 'percent';
                                        $fPromoValue = $fAmountInCart * ($aPromotionTypeSetupConfig->discount_amount/100);
                                        $fNewAmount=$fAmountInCart - ($fAmountInCart * ($aPromotionTypeSetupConfig->discount_amount/100));
                                        $item->setCost(-($fAmountInCart * ($aPromotionTypeSetupConfig->discount_amount/100)));
                                        break;

                                    default:
                                        $fPromoValue = $aPromotionTypeSetupConfig->discount_amount;
                                        $fNewAmount=$fAmountInCart - $aPromotionTypeSetupConfig->discount_amount;
                                        $item->setCost(-$aPromotionTypeSetupConfig->discount_amount);
                                }
                                //var_dump($aPromotionTypeSetupConfig);
                                //var_dump($aPromotionTypeInfoFetch['name']);
                            }
                            else{
                                $response->status=404;
                                $response->errors=['message'=>'Promo code does not belong to valid campaign'];
                            }


                            $item->setData(['promo_code'=>$sPromoCode,'gameTitle'=>'Promo Code '.$sPromoCode]);
                            $cart->addToCart($item);
                            $engineApi->saveCart($cart);
                            session_write_close();
                            //var_dump($engineApi->setupCart());
                            $response->status=200;

                            $response->message='OK';

                            $response->cartPromoValue = $fPromoValue;
                            $response->cartItemsCount =  $fCartItemsCount;
                            $response->cartPromoCode=$item;
                            $response->cartOldTotal=$fAmountInCart;
                            $response->cartNewTotal=$fNewAmount;
                        }
                        else{
                            $response->status=404;
                            $response->errors=['message'=>'Invalid Promotion Type'];
                        }
                    }
                    else{

                        $response->status=404;
                        $response->errors=['message'=>'No campaigns available for user'];
                    }
                }
                else{
                    $response->status=404;
                    $response->errors=['message'=>'Invalid user'];
                }
            }
            else{
                $response->status=404;
                $response->errors=['message'=>'Invalid user'];
            }


        }
        else{
            $response->status=404;
            $response->errors=['message'=>'Invalid Input'];
        }

        return json_encode($response);

    }
    /**
     * Search tag name
     * Used in auto-complete
     * @return string
     */
    public function searchTags(){
        $sTagToSearch=$_GET['query'];
        return $this->campaign->searchTagName($sTagToSearch);
    }

    /**
     * Search Affiliates by name or id
     * Used in auto-complete
     * @return string
     */
    public function searchAffiliates(){
        $mAffiliateToSearch=$_GET['query'];
        return $this->campaign->searchAffiliateByNameOrId($mAffiliateToSearch);
    }

    /**
     * search campaigns by tag
     * Used in auto-complete
     * @return string
     */
    public function searchTaggedCampaigns(){
        $sTagToSearch=$_POST['query'];
        return $this->campaign->searchTaggedCampaigns($sTagToSearch);
    }

    /**
     * Search if promo code exists
     * @return string
     */
    public function searchPromoCode(){
        $sPromoCode=$_POST['promoCode'];
        $op=isset($_POST['prefixOnly']) && $_POST['prefixOnly'] ==1 ? '%' : '';

        return $this->campaign->searchPromoCode($sPromoCode,$op);
    }

    /**
     * Load the saved recipients segment
     * @return mixed
     */
    public function loadRecipientSegment(){
        $iSegmentId=$_GET['segment_id'];

        return $this->campaignRecipients->loadRecipientSegment($iSegmentId);

    }

    /**
     * Export Promo Codes in CSV
     */
    public function exportPromoCodesOfCampaign(){
        $iCampaignId=$_GET['campaign_id'];


        if(!empty($iCampaignId) && is_numeric($iCampaignId)){
            $aCampaignData=$this->campaign->getCampaignById($iCampaignId);
            $filename="campaign-".$aCampaignData['name']."_" . $aCampaignData['start_date'] . "_" . $aCampaignData['end_date'] .".csv";
            $aPromoCodes=$this->campaign->getPromoCodesByCampaignId($iCampaignId,['id','promo_code']);
            $now = gmdate("D, d M Y H:i:s");
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
            header("Last-Modified: {$now} GMT");

            // force download
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");

            // disposition / encoding on response body
            header("Content-Disposition: attachment;filename={$filename}");
            header("Content-Transfer-Encoding: binary");
            echo self::$campaignHelper->arrayToCsv($aPromoCodes);
            return;
        }
    }

    public function uploadCampaignContentImage($iCampaignId){
        $sImageType=isset($_POST['type']) && !empty($_POST['type'])? $_POST['type'] : 'image';
        $imageUp=CampaignHelpers::fileUpload(false,'campaigns',Config::$config->sCampaignImagesUploadPath,'campaign-'.$sImageType.'-'.$iCampaignId,false,5,false,true);
        //return json_encode($imageUp) ;
        return $imageUp->errors===null ? json_encode($imageUp) : $imageUp->errors;
    }
    /**
     * Use helper to upload file + parse csv file and send to storage
     * @param $iCampaignId
     * @return string
     */
    public function uploadRecipientsCsv($iCampaignId){

        $aCsvContents=CampaignHelpers::fileUpload(true,'csv',false,false,false,5,true);
        $csv = array_map('str_getcsv', $aCsvContents);

        $data['recipientsXls']=$csv;

        return $this->storeXlsOptions($iCampaignId,$data);

    }

    /**
     * Call the store recipient options method in model and send response to caller
     * @param $iCampaignId
     * @param $sXlsData
     * @return string
     */
    public function storeXlsOptions($iCampaignId,$sXlsData){

        $aRecipientsInsertedIds= $this->campaignRecipients->storeRecipientOptions($iCampaignId,$sXlsData,null,null);
        $response=new stdClass();
        $response->msg=count($aRecipientsInsertedIds).' '.lang::get('records added');


        return json_encode($response);
    }
} 