<?php 
//error_reporting(E_ALL);
class TagsManyToManyPickListControl extends ManyToManyFormControl{

    /**
     * @var string
     */
    var $sView='/../../Controls/views/many_to_many_picklist_generic';

    /**
     * @var string
     */
    var $sItemView='/views/many_to_many_picklist_item';

    /**
     * JS scripts to load
     * No need to use (already in PromotionSetupControl)
     * @todo Remove this
     * @var array
     */
    var $aScripts=['/administration/API/ControlsAssets/campaignsControl'];

    /**
     * @param array $aAllItems
     * @param array $aSelectedItems
     */
    function __construct(array $aAllItems, array $aSelectedItems) {

        parent::__construct($aAllItems, $aSelectedItems);

    }

    /**
     * @return string
     */
    function output(){
        $oView=new LLView();
        $oView->sName=$this->sName;
        $oView->sCaption=$this->sCaption;
        $oView->sAllItems=$this->getItemsOutput($this->aAllItems);
        $oView->sChosenItems=$this->getItemsOutput($this->aSelectedItems);
        return $oView->output(__DIR__.$this->sView);
    }

}