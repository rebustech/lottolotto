<?php

class PromotionSetupControl extends Control{

	var $sView='promotion_setup';

	protected $aSettings;

	public function __construct($sJsonConfig) {
	    $this->aSettings=self::decodeConfigSettings($sJsonConfig);
        LLResponse::$sPostBody.=LLResponse::addScript('/administration/API/ControlsAssets/campaignsControl');
	}

	protected static function decodeConfigSettings($sSettings){
	    return json_decode($sSettings);
	}

	public  function output(){
	   $oView=new LLView();

	   $oView->formFields=$this->aSettings;
	   return $oView->output($this->sView);
	}

}