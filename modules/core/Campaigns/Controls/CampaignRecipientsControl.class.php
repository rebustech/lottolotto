<?php

class CampaignRecipientsControl extends Control {


    var $sView='campaign_recipients';

    //protected $aRecipients;

    /**
     * The Campaign id
     * @var
     */
    public $iCampaignId;

    /**
     * @var CampaignEntity
     */
    private $campaign;


    private $aRecipientOptions;

    private $aRecipientSegments;

    private $iSegmentId;

    private $sSegmentName;

    public function __construct($aRecipientOptions,$iCampaignId) {

        $this->iCampaignId=$iCampaignId;

        $this->campaign = new CampaignRecipients();
        $this->aRecipientOptions=$aRecipientOptions;
        $this->aRecipientSegments=$this->campaign->getAllRecipientsSegments();
        $this->aCRMRecipientsLists=$this->campaign->getRecipientsListsFromCRM();
        $aSegmentInfo=$this->campaign->getCampaignSegment($iCampaignId);

        $this->iSegmentId=$aSegmentInfo['id'];
        $this->sSegmentName=$aSegmentInfo['segment_name'];

    }


    public function output(){
        $oView=new LLView();
        $oView->recipientOptions=$this->aRecipientOptions;
        $oView->iCampaignId=$this->iCampaignId;
        $oView->iSegmentId=$this->iSegmentId;
        $oView->sSegmentName=$this->sSegmentName;
        $oView->aCampaignSegments=$this->aRecipientSegments;
        $oView->aCRMRecipientsLists=$this->aCRMRecipientsLists;
        return $oView->output($this->sView);
    }
}