<?php

class CampaignOptionsControl extends Control{

    /**
     * The view file template to render
     * @var string
     */
    public $sView='dev_campaign_options';

    /**
     * @var
     */
    protected $aSettings;

    /**
     * The Campaign id
     * @var
     */
    public $iCampaignId ;

    /**
     * @var
     */
    protected static $campaignOptions;

    /**
     * @param $sJsonConfig
     */
    public function __construct($sJsonConfig) {
		self::$campaignOptions=new CampaignOptions();
	   	$this->aSettings=self::decodeConfigSettings($sJsonConfig);

        $sAndOrOperatorOptions=json_encode(self::$campaignOptions->getAndOrOperatorValues(null,null,null));
        $sAddInline='var campaingsAndOrOperatorData='.$sAndOrOperatorOptions;
        $sAddInline.='; var groupTitle=\'Group\';';
        $sAddInline.='; var validationMessagesShowAll=\'0\';';
        $sAddInline.=' var importCsvLabel=\'Select CSV file to upload or use the methods below to use xls data or a list from CRM\'';
        LLResponse::$sPostBody.=LLResponse::addInlineScript($sAddInline);

	}


    /**
     * Return json decoded object/array
     * @param $sSettings
     * @return mixed
     */
    protected static function decodeConfigSettings($sSettings){
	    return json_decode($sSettings);
	}


    /**
     * Render the View
     * @return string
     */
    public function output(){
	   $oView=new LLView();
	   $oView->formFields=$this->aSettings;
       $oView->iCampaignId=$this->iCampaignId;

	   return $oView->output($this->sView);
	}
}