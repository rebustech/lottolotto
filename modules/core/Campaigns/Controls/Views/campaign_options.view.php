<div id="campaignOptionsContainer">
    <section class="detailsform promotionsetupform">
        <div class="campaignConditionsGroup clearfix">
            <fieldset id="group1" class="conditionsGroup">
                <h2><?=lang::get('Group')?> 1</h2>
                <?php foreach($this->formFields->fields as $sFields): ?>
                <div id="group1ConditionAdd" class="npClear conditionAddControl clearfix">
                    <label class="small">
                        <span><?=$sFields->label?></span>
                        <div>
                            <select class="resetAppearanceForSelect" id="<?=$sFields->name?>" name="<?=$sFields->name?>">
                                <?php foreach ($sFields->options as $option):
                                    $method=isset($option->method) ? 'data-method="'.$option->method.'"' : '' ;
                                    $value=isset($option->value) && $option->value!='0' ? 'value="'.$option->value.'"' : '' ;
                                    ?>
                                    <option <?=$method?> <?=$value?>><?=lang::get($option->name)?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </label>
                </div>
                <?php endforeach?>

            </fieldset>

        </div>

        <?php //echo '<pre>'; print_r($this->formFields->fields->group1); ?>
        <!--
        <?php foreach($this->formFields->fields->group1 as $sFields): ?>
            <?php if($sFields->type == 'text' && $sFields->name != 'discount_amount'): ?>
                <label class="halfsize">
                    <span><?=lang::get($sFields->name)?></span>
                    <div>
                        <input type="text" name="<?=$sFields->name?>" id="<?=$sFields->name?>" class="small" value="<?=$sFields->default?>">
                    </div>
                </label>

            <?php elseif($sFields->type == 'text' && $sFields->name == 'discount_amount' ): ?>
                <label class="halfsize number">
                    <span><?=lang::get($sFields->name)?></span>
                    <div>
                        <input type="text" name="<?=$sFields->name?>" id="<?=$sFields->name?>" class="small" value="<?=$sFields->default?>">
                    </div>
                </label>

            <?php elseif($sFields->type == 'select' && $sFields->name != 'discount_amount_type' ): ?>
                <label class="halfsize">
                    <span style="width:30px; cursor: pointer; border-right:1px solid #ccc; height:33px; color:red;"><i class="xx fa fa-times"></i></span>
                    <span style="clear:none"><?=lang::get($sFields->name)?></span>
                    <div>
                        <select style="-webkit-appearance:menulist!important" id="<?=$sFields->name?>" name="<?=$sFields->name?>">
                            <?php foreach ($sFields->options as $option):
            $method=isset($option->method) ? 'data-method="'.$option->method.'"' : '' ;
            $value=isset($option->value) && $option->value!='0' ? 'value="'.$option->value.'"' : '' ;
            ?>
                                <option <?=$method?> <?=$value?>><?=lang::get($option->name)?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </label>

            <?php elseif($sFields->type == 'select' && $sFields->name == 'discount_amount_type' ): ?>
                <label class="small number">
                    <div>
                        <select style="-webkit-appearance:menulist!important" id="<?=$sFields->name?>" name="<?=$sFields->name?>" class="small">
                            <?php foreach ($sFields->options as $option):
            $method=isset($option->method) ? 'data-method="'.$option->method.'"' : '' ;
            $value=isset($option->value) && $option->value!='0' ? 'value="'.$option->value.'"' : '' ;
            ?>
                                <option <?=$method?> <?=$value?>><?=lang::get($option->name)?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </label>

            <?php endif; ?>
        <?php endforeach; ?>
        <div style="clear:both; padding-top:40px; border-top:1px solid #ccc" id="opt"></div>-->
    </section>
    <a href="#" id="addCampaignOptionsGroup" class="ToolbarButtonControl"><i class="fa fa-magic fa-lg"></i><?=lang::get('Add Group')?></a>
</div>