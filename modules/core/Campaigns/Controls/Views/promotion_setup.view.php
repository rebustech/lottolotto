<fieldset class="detailsform promotionsetupform">
    <h2><?=lang::get('Campaign Setup')?></h2>

    <?php foreach($this->formFields->fields as $sFields): ?>
        <?php if($sFields->type == 'text' && $sFields->name != 'discount_amount'): ?>
            <label class="halfsize">
                <span><?=lang::get($sFields->name)?></span>
                <div>
                    <input type="text" name="<?=$sFields->name?>" id="<?=$sFields->name?>" class="small" value="<?=$sFields->default?>">
                </div>
            </label>

        <?php elseif($sFields->type == 'text' && $sFields->name == 'discount_amount' ): ?>
            <label class="halfsize number">
                <span><?=lang::get($sFields->name)?></span>
                <div>
                    <input type="text" name="<?=$sFields->name?>" id="<?=$sFields->name?>" class="resetAppearanceForSelect small" value="<?=$sFields->default?>">
                </div>
            </label>

        <?php elseif($sFields->type == 'select' && $sFields->name != 'discount_amount_type' ): ?>
            <label class="halfsize">
                <span><?=lang::get($sFields->name)?></span>
                <div>
                    <select id="<?=$sFields->name?>" name="<?=$sFields->name?>">
                        <?php foreach ($sFields->options as $option): ?>
                            <option value="<?=$option->value?>"><?=lang::get($option->name)?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </label>

        <?php elseif($sFields->type == 'select' && $sFields->name == 'discount_amount_type' ): ?>
            <label class="small number">
                <div>
                    <select id="<?=$sFields->name?>" name="<?=$sFields->name?>"  class="resetAppearanceForSelect small">
                        <?php foreach ($sFields->options as $option): ?>
                            <option value="<?=$option->value?>"><?=lang::get($option->name)?></option>
                        <?php endforeach; ?>
                    </select>

                </div>
            </label>

        <?php endif; ?>
    <?php endforeach; ?>
    <button  id="storeCampaignSetup" type="submit" class="ToolbarButtonControl "><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
</fieldset>
