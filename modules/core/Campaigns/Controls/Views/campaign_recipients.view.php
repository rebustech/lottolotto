<?php //print "<pre>"; print_r($this->aCRMRecipientsLists); print "</pre>";?>
<form id="campaignRecipientsForm" action="/administration/API/Campaigns/handleCampaignRecipients" method="post"  enctype="multipart/form-data">
    <div class="npClear">
        <button  type="submit" class="ToolbarButtonControl storeCampaignRedemption"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
    </div>
    <h2><?=lang::get('Recipients Segment')?></h2>
    <div id="recipientsMsg"></div>


    <section class="detailsform recipientsSection active hovered first" id="campaignRecipientsFormContainer">
        <div class="npClear">

            <select class="resetAppearanceForSelect " id="loadRecipientsTemplate">
                <option><?=lang::get('Load Segment')?></option>
                <?php foreach($this->aCampaignSegments AS $aSegment):?>
                    <option value="<?=$aSegment['id']?>"><?=$aSegment['segment_name']?></option>
                <?php endforeach ?>
            </select>

        </div>


        <h3>
            <input type="radio" checked name="campaignRecipientsChoiceOfEntry" value="1" data-target="campaignRecipientsFormContainer">
            <?=lang::get('Add criteria to match campaign recipients')?></h3>
        <div class="npClear detailsform conditionsProperty" style="padding: 20px 0; margin-bottom: 40px;">
            <label class="halfsize "><span><?=lang::get('Segment name')?></span>

                <div>
                    <input type="text" <?php if(isset($this->sSegmentName) && !empty($this->sSegmentName)):?>
                         value="<?=$this->sSegmentName?>"
                    <?php endif?> name="segment_name" id="recipientsSegmentName" class="long" placeholder="Enter <?=lang::get('Segment name')?>">
                </div>
            </label>
        </div>

        <div class="npClear">
            <button class="addRecipientsSegmentCondition "><i class="fa fa-plus-circle fa-lg fa-fw"></i> <?=lang::get('Click to add Condition')?></button>
        </div>
        <fieldset id="campaignRecipientsConditions">

        </fieldset>
        <div>
        </div>

    </section>
    <section class="detailsform recipientsSection" id="campaignRecipientsImporterContainer">

        <h3>
            <input type="radio" name="campaignRecipientsChoiceOfEntry" value="2" data-target="campaignRecipientsImporterContainer">
            <?=lang::get('or')?> <?=lang::get('import recipients using ONE of the methods below')?>
            ( <a href="/recipientsListSample.csv"><?=lang::get('Sample CSV/XLS file')?></a> )
        </h3>

        <div class="groupCondition conditionAddControl npClear" id="importRecipientsFromCsv">
           <label class="long">
               <span class="labelPrefix" id="csvUploaderIcon"><i class="fa fa-list fa-lg"></i></span>
               <span class="labelTitleNoClear fw"><?=lang::get('Upload CSV')?></span>

               <div> <input type="file" size="20" id="campaignRecipientsCsvUpload"></div>


          </label>
        </div>
        <div class="groupCondition conditionAddControl npClear">
            <label class="long"><span><?=lang::get('Paste from Excel')?></span>
                <div>
                    <textarea rows="15" name="recipientsXls" disabled="disabled"></textarea>
                </div>
            </label>
        </div>

        <div class="groupCondition conditionAddControl npClear">
            <label class="small">
                <span class="labelTitleNoClear fw"><?=lang::get('Choose List from CRM')?></span>
                <div>
                    <select class="resetAppearanceForSelect" id="" name="recipientsFromList" disabled="disabled">
                        <option value><?=lang::get('Select')?></option>
                        <?php foreach($this->aCRMRecipientsLists AS $aRecipientList):?>
                            <option value="<?=$aRecipientList['id']?>"><?=$aRecipientList['list_name']?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </label>
        </div>

    </section>
    <input type="hidden" name="campaign_id" value="<?=$this->iCampaignId?>">
    <?php if(isset($this->iSegmentId) && is_numeric($this->iSegmentId)):?>
        <input type="hidden" name="segment_id" value="<?=$this->iSegmentId?>" id="segmentId">
    <?php endif?>


    <div class="npClear">
        <button  type="submit" class="ToolbarButtonControl storeCampaignRedemption"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
    </div>
</form>