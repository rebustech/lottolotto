<section class="detailsform">
    <?php if(!empty($this->campaigns)):?>
        <form action="actions/generic_actions.php?tablename=campaigns&a=replicate&pp=generic-listing.php" method="post" enctype="multipart/form-data" name="genericform">
            <h2><?=lang::get('Replicate Campaign')?></h2>
            <div id="group1Condition1" class="groupCondition npClear">
                <label class="small">
                    <span class="labelTitleNoClear fw">Replicate</span>
                    <div>
                        <select  class="resetAppearanceForSelect"  name="repl_campaigns_from">

                            <option value=""><?=lang::get('Select Campaign')?></option>
                            <?php foreach($this->campaigns AS $campaign):?>

                                <option value="<?=$campaign['id'] ?>"><?=$campaign['name'] ?></option>
                            <?php endforeach ?>

                        </select>
                    </div>
                </label>
                <label class="label_smaller">
                    <span class="labelTitleNoClear fw">as</span>
                    <div>
                        <input type="text" name="repl_campaigns_to" class="small number" placeholder="<?=lang::get('New Campaign Name')?>">
                    </div>
                </label>
                <label class="label_smaller">

                    <div>
                        <button style="height:30px; " type="submit"><i class="fa fa-fw fa-copy"></i> <?=lang::get('Go')?></button>
                    </div>
                </label>
            </div>
            <input type="hidden" name="action" value="replicate">
        </form>
    <?php else: ?>
        <h2><?=lang::get('No campaigns found')?></h2>
    <?php endif?>
</section>