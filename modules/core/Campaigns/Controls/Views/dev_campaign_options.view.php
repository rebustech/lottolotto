<form id="campaignOptionsForm" method="post">
<div id="sysMsg"></div>
<button  type="submit" class="ToolbarButtonControl storeCampaignOptions"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Store Options'))?></button>

<?php //print "<pre>"; print_r($this); print "</pre>"; ?>
<?php if($this->formFields->fields): ?>
    <div id="campaignOptionsContainer">
        <section class="detailsform promotionsetupform">
            <div class="campaignConditionsGroup clearfix">
                <fieldset id="group1" class="conditionsGroup">
                    <h2>Group 1</h2>
                    <?php foreach($this->formFields->fields as $sFields): ?>
                        <div id="group1ConditionAdd" class="npClear conditionAddControl clearfix">
                            <label class="small">
                                <span><?=$sFields->label?></span>
                                <div>
                                    <select required class="resetAppearanceForSelect" id="<?=$sFields->name?>" name="<?=$sFields->name?>">
                                        <?php foreach ($sFields->options as $option):
                                            $method=isset($option->method) ? 'data-method="'.$option->method.'"' : '' ;
                                            $value=isset($option->value) && $option->value!='0' ? 'value="'.$option->value.'"' : '' ;
                                            ?>
                                            <option <?=$method?> <?=$value?>><?=lang::get($option->name)?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </label>
                        </div>
                    <?php endforeach?>

                </fieldset>

            </div>

        </section>
        <a href="#" id="addCampaignOptionsGroup" class="ToolbarButtonControl"><i class="fa fa-magic fa-lg"></i><?=lang::get('Add Group')?></a>
    </div>

<?php else : ?>
    <div id="campaignOptionsContainer">
        <section class="detailsform promotionsetupform">
            <div class="campaignConditionsGroup clearfix">
                <?php $g=1 ?>
                <?php foreach($this->formFields as $sKey=>$oConditionsGroup): ?>
                    <fieldset class="conditionsGroup" id="<?=$sKey?>">
                        <h2><?=lang::get('Group')?> <?=$g?>
                            <?php if($g>1):?>
                                <span style="color:red">[</span> <a data-target-operator="#group<?=($g-1)?>" data-target="#<?=$sKey?>" class="removeGroupButton" href="#"><i class="fa fa-times"></i></a> <span style="color:red">]</span>
                            <?php endif?>
                        </h2>
                        <?php $iConditionsCount=1 ?>
                        <?php $groupOper =$oConditionsGroup->group_operator ; unset($oConditionsGroup->group_operator) ?>
                        <?php foreach($oConditionsGroup AS $sConditionKey=>$aCondition):?>
                            <?php if($sConditionKey=='select_condition'):?>
                                <div class="npClear conditionAddControl clearfix" id="<?=$sKey?>ConditionAdd">
                                    <?php //$oAddCondition=$aCondition[0]?>
                                    <?php $oAddCondition=$aCondition[0]->fields[0]?>
                                    <label class="small">
                                        <span><?=lang::get($oAddCondition->label)?></span>
                                        <div>
                                            <select required class="resetAppearanceForSelect" id="<?=$oAddCondition->name?>" name="<?=$oAddCondition->name?>">
                                                <?php foreach($oAddCondition->options AS $oAddConditionOptionsKey=>$oAddConditionOption):?>
                                                    <?php $sMethod= $oAddConditionOption->method ? 'data-method="'.$oAddConditionOption->method.'"' : '' ;?>
                                                    <option <?=$sMethod?> value="<?=$oAddConditionOption->value?>"><?=$oAddConditionOption->name?></option>
                                                <?php endforeach?>
                                            </select>
                                        </div>
                                    </label>
                                </div>
                            <?php else: ?>
                                <div class="groupCondition npClear" id="<?=$sKey?><?=$sConditionKey?>">
                                    <?php $bIsAGroupOfFields=count($aCondition[0]) >1 ? true : false;?>
                                    <?php if($bIsAGroupOfFields):?>
                                        <?php $k=0 ?>
                                        <?php foreach($aCondition[0] AS $oConditionField):?>

                                            <?php
                                            $oConditionField->elementId=preg_replace('/(\d)_/u','$1',$oConditionField->name);
                                            ?>
                                            <label class="<?php print $oConditionField->type=='select' ? 'small' : 'label_smaller'  ?>">
                                                <?php if($k==0):?>
                                                    <?php
                                                    $aConditionNumberExtract=preg_match('/\d/',$sConditionKey,$m);
                                                    $iConditionNumber=$m[0];
                                                    ?>
                                                    <span data-target-operator="#<?=$sKey?>Condition<?=$iConditionNumber-1?>" data-target="#<?=$sKey?><?=$sConditionKey?>" class="conditionRemove xx" title="Remove Condition">
                                                        <i class="fa fa-times"></i>
                                                    </span>
                                                    <span class="labelTitleNoClear fw">
                                                        <?=lang::get($oConditionField->label)?>
                                                    </span>
                                                <?php endif?>
                                                <div>
                                                    <?php switch($oConditionField->type){
                                                        case 'text':

                                                            $sTextValue=isset($oConditionField->default)  ? 'value="'.$oConditionField->default.'"' : '' ;
                                                            ?>
                                                            <input  required type="text" <?=$sTextValue?> id="<?=$oConditionField->elementId?>" name="<?=$oConditionField->name?>" class="small number" placeholder="<?=lang::get($oConditionField->label)?>">
                                                            <?php
                                                            break;
                                                        default:
                                                            ?>
                                                                <select required class="resetAppearanceForSelect" id="<?=$oConditionField->elementId?>" name="<?=$oConditionField->name?>">
                                                                    <?php foreach($oConditionField->options AS $oOption):?>
                                                                        <?php
                                                                        $sOptionValue=($oOption->value) && $oOption->value!='0' ? 'value="'.$oOption->value.'"' : '' ;
                                                                        $sOptionSelected=isset($oConditionField->default) && $oConditionField->default==$oOption->value ? 'selected' : '' ;

                                                                        ?>
                                                                        <option <?=$sOptionValue?> <?=$sOptionSelected?>><?=$oOption->name?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            <?php } ?>
                                                </div>
                                            </label>

                                            <?php $k++ ?>

                                        <?php endforeach?>

                                    <?php else: ?>
                                        <?php $aCondition[0]->elementId=preg_replace('/(\d)_/u','$1',$aCondition[0]->name); ?>
                                        <?php $sElementName=preg_replace("/\d/i", "", $aCondition[0]->elementId); ?>
                                        <?php if($sElementName!='groupOperator' && $sElementName!='groupConditionOperator'): ?>
                                            <label class="halfsize">
                                                <span data-target="#<?=$sKey?><?=$sConditionKey?>" class="conditionRemove xx" title="Remove Condition"><i class="fa fa-times"></i></span>
                                                <span class="labelTitleNoClear fw"><?=lang::get($aCondition[0]->label)?></span>
                                                <div>

                                                    <?php switch($aCondition[0]->type){
                                                        case 'text':

                                                            $sTextValue=isset($aCondition[0]->default)  ? 'value="'.$aCondition[0]->default.'"' : '' ;
                                                            ?>
                                                            <input  required type="text" <?=$sTextValue?> id="<?=$aCondition[0]->elementId?>" name="<?=$aCondition[0]->name?>" class="small number" placeholder="<?=lang::get($aCondition[0]->label)?>">
                                                            <?php
                                                            break;
                                                        default:
                                                            ?>
                                                                <select required class="resetAppearanceForSelect"  id="<?=$aCondition[0]->elementId?>" name="<?=$aCondition[0]->name?>">
                                                                    <?php foreach($aCondition[0]->options AS $oOption):?>
                                                                        <?php
                                                                        $sOptionValue=($oOption->value) && $oOption->value!='0' ? 'value="'.$oOption->value.'"' : '' ;
                                                                        $sOptionSelected=isset($aCondition[0]->default) && $aCondition[0]->default==$oOption->value ? 'selected' : '' ;

                                                                        ?>
                                                                        <option <?=$sOptionValue?> <?=$sOptionSelected?>><?=$oOption->name?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            <?php } ?>
                                                </div>
                                            </label>
                                        <?php endif?>

                                    <?php endif?>
                                </div>
                                <?php
                                $aReversed=array_reverse($this->formFields->$sKey->$sConditionKey) ;
                                $sSelectedOperatorOption='';
                                if(isset($aReversed[0]) && property_exists($aReversed[0],'name') && $aReversed[0]->name==''.$sKey.'_'.$sConditionKey.'_Operator'):?>
                                    <?php $prop='Condition'.($iConditionsCount+1);?>
                                    <?php if(property_exists($this->formFields->$sKey,$prop)):?>
                                        <!-- Condition operator -->
                                        <div id="<?=$sKey?><?=$sConditionKey?>OperatorContainer" class="groupConditionOperator npClear">
                                            <label class="small">
                                                <div>
                                                    <select required data-parent_id="<?=$sKey?><?=$sConditionKey?>" class="resetAppearanceForSelect" id="<?=$sKey?><?=$sConditionKey?>Operator" name="<?=$sKey?>_<?=$sConditionKey?>_Operator">
                                                        <?php foreach($aReversed[0]->options AS $sOperatorOption):?>
                                                            <?php $sSelectedOperatorOption=$sOperatorOption->value==$aReversed[0]->default ? 'selected' : ''?>
                                                            <option <?=$sSelectedOperatorOption?> value="<?=$sOperatorOption->value?>"><?=$sOperatorOption->name?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </label>
                                        </div>
                                        <!-- ./Condition operator -->
                                    <?php endif ?>
                                <?php endif ?>

                                <?php $iConditionsCount++ ?>
                            <?php endif ?>
                        <?php endforeach?>
                    </fieldset>

                    <?php
                    $sSelectedOperatorOption='';

                    if(null!=$groupOper):?>
                        <!-- Group Operator -->
                        <div id="<?=$sKey?>OperatorContainer" class="groupOperator groupConditionOperator npClear clearfix">
                            <label class="small">
                                <div>
                                    <select required data-parent_id="<?=$sKey?>" class="resetAppearanceForSelect" id="<?=$sKey?>Operator" name="<?=$sKey?>Operator">
                                        <?php foreach($groupOper[0]->options AS $sOperatorOption):?>
                                            <?php $sSelectedOperatorOption=$sOperatorOption->value==$groupOper[0]->default ? 'selected' : ''?>
                                            <option <?=$sSelectedOperatorOption?> value="<?=$sOperatorOption->value?>"><?=$sOperatorOption->name?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </label>
                        </div>
                        <!-- ./Group Operator -->
                    <?php endif?>

                    <?php $g++?>
                <?php endforeach?>
            </div>

        </section>
        <a href="#" id="addCampaignOptionsGroup" class="ToolbarButtonControl"><i class="fa fa-magic fa-lg"></i><?=lang::get('Add Group')?></a>
    </div>
    <button  type="submit" class="ToolbarButtonControl storeCampaignOptions" ><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Store Options'))?></button>
<?php endif; ?>
<input type="hidden" name="iCampaignId" value="<?=$this->iCampaignId?>">
</form>