<?php //print "<pre>"; print_r($this->redemptionOptions); print "</pre>";?>
<section class="detailsform promotionsetupform">
    <form id="campaignRedemptionForm" method="post">
        <button  type="submit" class="ToolbarButtonControl storeCampaignRedemption"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
        <div id="redemptionMsg"></div>

        <h2><?=lang::get('Create Promo Codes')?></h2>
        <?php if(!$this->bCampaignOptionsComplete):?>
            <h3 style="color:red;">
            <?=lang::get('YOU MUST FIRST ADD PROMOTION OPTIONS IN THE PREVIOUS TAB')?>
            </h3>
        <?php endif?>
        <small style="display:block; clear:both; color:red; margin-bottom:20px;">*<?=lang::get('Promo codes must not be case sensitive, only contain numbers and letters and must not contain the following letters and numbers: “1,0, t, I, o, d, j, l” [number (of codes), code length 12 characters')?></small>
        <?php foreach ($this->redemptionOptions['fields'] as $key => $value): ?>
            <?php if($value['closeGroup']===false ): ?><div class="groupCondition npClear"><?php endif;?>
            <?php if($value['closeGroup']===false):?>
            <label <?php if(!empty($value['labelClass'])): ?>class="<?=$value['labelClass']?>" <?php endif;?>>
        <?php endif;?>
            <?php if(!empty($value['label'])): ?>
                <span class="labelTitleNoClear fw" style="min-width:405px;"><?=lang::get($value['label'])?></span>
            <?php endif;?>
            <?php if($value['type']=='text' || $value['type']=='checkbox'): ?>
                <?php $sChecked = ($value['type']=='checkbox' && $value['value']) ? 'checked' : ''; ?>
                <input
                    <?php if(isset($value['data']) && is_array($value['data']) && !empty($value['data']) ): ?>
                        <?php foreach($value['data'] AS $k=>$v):?>
                            data-<?=$k?>="<?=$v?>"
                        <?php endforeach ?>

                    <?php endif?>
                    <?php if(isset($value['exportCsv']) && $value['exportCsv'] && null!=$value['value']){
                        $value['placeholder']='Exportable multiple values';
                    }
                    ?>
                    <?php if(isset($value['disabled']) && $value['disabled']===true ): ?> disabled <?php endif?> <?=$sChecked?> type="<?=$value['type']?>"  <?php if(null!=$value['value'] &&(!isset($value['exportCsv']) || !$value['exportCsv'])):?>value="<?=$value['value']?>"<?php endif?> id="<?=$value['name']?>" name="<?=$value['name']?>" class="<?=$value['class']?>" <?php if(null!=$value['placeholder']):?>placeholder="<?=$value['placeholder']?>"<?php endif?>>

                
                <?php if(preg_match('/checkOnType/iu',$value['class'])):?>
                    <div class="checkOnTypeResult">
                        <?php if(isset($value['exportCsv']) && $value['exportCsv'] && null!=$value['value']):?>
                            <a style="position:relative" href="/administration/API/Campaigns/exportPromoCodesOfCampaign?campaign_id=<?=$this->iCampaignId?>">
                                <?=lang::get('Export as CSV')?>
                            </a>
                        <?php endif?>
                    </div>
                <?php endif?>
                <?php if(isset($value['note'])): ?>
                    <small style="display:block; clear:both; color:red">*<?=$value['note']?></small>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($value['type']=='select'):?>
                <select <?php if(isset($value['disabled']) && $value['disabled']===true ): ?> disabled <?php endif?> <?php if(isset($value['multiple']) && $value['multiple']===true ): ?> multiple <?php endif?> class="<?=$value['class']?>"  id="<?=$value['name']?>" name="<?=$value['name']?><?php if(isset($value['multiple']) && $value['multiple']===true ): ?>[]<?php endif?>">
                    <?php foreach($value['options'] as $aOption): ?>
                        <?php
                        if(isset($value['multiple']) && $value['multiple']===true ){
                            $sOptionSelected=isset($value['value']) && in_array($aOption['value'],$value['value']) ? 'selected' : '' ;
                        }
                        else{
                            $sOptionSelected=isset($value['value']) && $aOption['value']==$value['value'] ? 'selected' : '' ;
                        }

                        ?>
                        <option value="<?=$aOption['value']?>" <?=$sOptionSelected?>><?=$aOption['name']?></option>
                    <?php endforeach;?>
                </select>
            <?php endif;?>
            <?php if($value['closeGroup']===true || $value['inGroup']===false ): ?>
            </label>
        <?php endif;?>
            <?php if(isset($value['after_element'])): ?>
                <?=$value['after_element']?>
            <?php endif;?>

            <?php if(($value['closeGroup']===false && $value['inGroup']===false ) || $value['closeGroup']===true): ?></div><?php endif;?>
        <?php endforeach; ?>
        <button  type="submit" class="ToolbarButtonControl storeCampaignRedemption"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
        <input type="hidden" name="campaign_id" value="<?=$this->iCampaignId?>">
    </form>
</section>