<form id="campaignPageContentForm" action="/administration/API/Campaigns/handleCampaignContent" method="post"  enctype="multipart/form-data">
    <div class="npClear">
        <button  type="submit" class="ToolbarButtonControl storeCampaignRedemption"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
    </div>
    <h2><?=lang::get('Campaign Page Content Settings')?></h2>
    <div id="pageContentMsg"></div>
    <section class="detailsform PageContentSection active" id="campaignPageContentSection">
        <div class="groupCondition conditionAddControl npClear">
            <label class="halfsize"><span><?=lang::get('Promo Page Destination')?></span>
                <div>
                    <select class="resetAppearanceForSelect" id="" name="campaignPageContentPromoPageDestination">
                        <option value=""><?=lang::get('Select')?></option>
                        <option <?php if($this->iCampaignContent['destination']==1) echo 'selected'; ?> value="1"><?=lang::get('Custom Landing Page')?></option>
                        <option <?php if($this->iCampaignContent['destination']==2) echo 'selected'; ?> value="2"><?=lang::get('Existing Page')?></option>
                    </select>
                </div>
            </label>
        </div>
        <div class="groupCondition conditionAddControl npClear">
            <label class="long"><span><?=lang::get('Destination URL')?></span>
                <div>
                    <input type="text" name="campaignPageContentDestinationUrl" value="<?=$this->iCampaignContent['destination_url']?>">
                </div>
            </label>
        </div>
        <div class="optionsSeparator  clear"></div>
        <div class="groupCondition conditionAddControl npClear">
            <label class="long"><span><?=lang::get('On Page Text')?></span>
                <div>
                    <textarea rows="15" name="campaignPageContentOnPageText"><?=$this->iCampaignContent['page_text']?></textarea>
                </div>
            </label>
        </div>

        <div class="groupCondition conditionAddControl npClear" id="campaignPageContentOnPageImageUploaderContainer">
            <label class="long inConjuction">
                <span class="labelPrefix" id="onPageImageUploaderIcon"><i class="fa fa-image fa-lg"></i></span>
                <span class="labelTitleNoClear fw"><?=lang::get('On-page Image')?></span>
                <input type="file" size="20" id="campaignPageContentOnPageImageUploader" class="label_smaller number">
                <div class="checkOnTypeResult">
                    <?php if($this->iCampaignContent['ContentImage']):?>
                    <img src="<?=$this->iCampaignContent['ContentImage']?>" height="40"/>
                        <?php else:?>
                        <?=lang::get('Accepted file type PNG') ?>
                    <?php endif;?>
                </div>
            </label>
        </div>
        <div class="groupCondition conditionAddControl npClear" id="campaignPageContentOnPageIconUploaderContainer">
            <label class="long inConjuction">
                <span class="labelPrefix" id="onPageIconUploaderIcon"><i class="fa fa-image fa-lg"></i></span>
                <span style="min-width:125px;" class="labelTitleNoClear fw"><?=lang::get('On-page Icon')?></span>
               <input type="file" size="20" id="campaignPageContentOnPageIconUploader" class="label_smaller number">

                <div class="checkOnTypeResult">
                    <?php if($this->iCampaignContent['ContentIcon']):?>
                    <img src="<?=$this->iCampaignContent['ContentIcon']?>" height="40"/>
                        <?php else:?>
                        <?=lang::get('Accepted file type PNG') ?>
                    <?php endif;?>
                </div>
            </label>
        </div>
        <div class="groupCondition conditionAddControl npClear">
            <label class="long"><span><?=lang::get('Bet Slip Text')?></span>
                <div>
                    <input type="text" name="campaignPageContentBetSlipText" value="<?=$this->iCampaignContent['betslip_text']?>">
                </div>
            </label>
        </div>
        <div class="groupCondition conditionAddControl npClear">
            <label class="long"><span><?=lang::get('Terms &amp; Conditions')?></span>
                <div>
                    <textarea rows="15" name="campaignPageContentTermsConditions"><?=$this->iCampaignContent['termsConditions']?></textarea>
                </div>
            </label>
        </div>
    </section>
    <div class="npClear">
        <button  type="submit" class="ToolbarButtonControl storeCampaignRedemption"><i class="fa fa-save fa-lg"></i><?=trim(lang::get('Save'))?></button>
    </div>
    <input type="hidden" name="campaign_id" value="<?=$this->iCampaignId?>">
</form>