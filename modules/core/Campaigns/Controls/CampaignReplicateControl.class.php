<?php 

class CampaignReplicateControl extends Control {


    var $sView='campaign_replicate';

    protected $aCampaigns;

    /**
     * @var CampaignEntity
     */
    private $campaign;


    public function __construct() {

       $this->campaign = new CampaignEntity();

       $this->aCampaigns=$this->campaign->getAllCampaigns();

    }



    public function output(){
        $oView=new LLView();
        $oView->campaigns=$this->aCampaigns;

        return $oView->output($this->sView);
    }
}