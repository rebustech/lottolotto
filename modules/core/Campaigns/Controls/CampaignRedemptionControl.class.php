<?php

class CampaignRedemptionControl extends Control {


    var $sView='campaign_redemption';

    protected $aCampaigns;

    /**
     * The Campaign id
     * @var
     */
    public $iCampaignId;

    /**
     * @var CampaignEntity
     */
    private $campaign;

    private $bCampaignOptionsComplete=false;

    private $aRedemptionOptions;


    public function __construct($aRedemptionOptions) {

        $iCampaignId=$this->iCampaignId;

        $this->campaign = new CampaignEntity();

        $this->bCampaignOptionsComplete=$this->campaign->checkIfOptionsConfigExists($iCampaignId);

        $this->aCampaigns=$this->campaign->getAllCampaigns();

        $this->aRedemptionOptions=$aRedemptionOptions;
    }



    public function output(){
        $oView=new LLView();
        $oView->campaigns=$this->aCampaigns;
        $oView->redemptionOptions=$this->aRedemptionOptions;
        $oView->iCampaignId=$this->iCampaignId;
        $oView->bCampaignOptionsComplete=$this->bCampaignOptionsComplete;

        return $oView->output($this->sView);
    }
}