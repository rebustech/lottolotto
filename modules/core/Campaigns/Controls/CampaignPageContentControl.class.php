<?php 

class CampaignPageContentControl extends Control{

    var $sView='campaign_page_content';

    protected $iCampaignId;

    private $aContentOptions;


    public function __construct($iCampaignId,$aContentOptions) {

        $this->iCampaignId=$iCampaignId;
        $this->aContentOptions=$aContentOptions;
    }


    public function output(){
        $oView=new LLView();

        $oView->iCampaignId=$this->iCampaignId;
        $oView->iCampaignContent=$this->aContentOptions;

        return $oView->output($this->sView);
    }
} 