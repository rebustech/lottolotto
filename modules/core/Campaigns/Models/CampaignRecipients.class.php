<?php

/**
 * Model/Repository Class for campaign redemptions
 *
 * Class CampaignRedemptions
 * @property CampaignFormDbMapping mappings
 * @todo Proper PHP Doc Commenting
 */
class CampaignRecipients extends CampaignEntity{

    /**
     * @var array
     */
    public $aDefaultOptions;

    public $oMember;

    /**
     * @var array
     */
    protected static $aPlayersMatch=[
        'type'=>'select',
        'name'=>'players_match',
        'value'=>0,
        'label'=>'Players Match',
        'labelClass'=>'small',
        'class'=>'label_smaller resetAppearanceForSelect',
        'options'=>[
            ['name'=>'Any of the following','value'=>0],
            ['name'=>'All of the following','value'=>1]
        ],
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aMasterSwitch=[
        'type'=>'select',
        'name'=>'master_switch',
        'value'=>0,
        'label'=>'Select',
        'labelClass'=>'small',
        'class'=>'halfsize smaller resetAppearanceForSelect',
        'options'=>[
            ['name'=>'Condition','value'=>''],
            ['name'=>'Date joined','value'=>'joined_date'],
            ['name'=>'Last order date','value'=>'last_order_date'],
            ['name'=>'Total order amount','value'=>'total_order_amount'],
            ['name'=>'Current balance','value'=>'current_balance'],
            ['name'=>'Lifetime deposits','value'=>'life_deposits'],
            ['name'=>'Lifetime order amount','value'=>'life_order'],
            ['name'=>'Location','value'=>'location'],
            ['name'=>'Language','value'=>'language'],
            ['name'=>'Payment method','value'=>'payment_method'],
            ['name'=>'Member rating','value'=>'member_rating'],
            ['name'=>'Signup source','value'=>'signup_source'],
            ['name'=>'Has played','value'=>'played_product'],
            ['name'=>'Jubilee','value'=>'jubilee'],
            ['name'=>'Birthday','value'=>'birthday'],
        ],
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aDateOperatorSwitch=[
        'type'=>'select',
        'name'=>'date_operator_switch',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'small',
        'class'=>'resetAppearanceForSelect',
        'options'=>[
            ['name'=>'Is after','value'=>'>'],
            ['name'=>'Is before','value'=>'<'],
            ['name'=>'Is','value'=>'='],
        ],
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aOperatorSwitch=[
        'type'=>'select',
        'name'=>'operator_switch',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'small',
        'class'=>'resetAppearanceForSelect',
        'options'=>[
            ['name'=>'More than','value'=>'>'],
            ['name'=>'Less than','value'=>'<'],
            ['name'=>'Equal','value'=>'='],
        ],
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aDatePickerControl=[
        'type'=>'text',
        'name'=>'date_picker',
        'placeholder'=>'Enter Date',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'small',
        'class'=>'halfsize smaller datepicker',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aTextControl=[
        'type'=>'text',
        'name'=>'text_control',
        'value'=>0,
        'placeholder'=>'Enter Number',
        'label'=>'',
        'labelClass'=>'small',
        'class'=>'halfsize smaller',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aCurrencyControl=[
        'type'=>'select',
        'name'=>'currency_control',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'long',
        'class'=>'label_smaller marginLeft10 resetAppearanceForSelect',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aCountryControl=[
        'type'=>'select',
        'name'=>'country_control',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'label_smaller',
        'class'=>'marginLeft10 resetAppearanceForSelect',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aLanguageControl=[
        'type'=>'select',
        'name'=>'language_control',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'label_smaller',
        'class'=>'marginLeft10 resetAppearanceForSelect',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aPaymentControl=[
        'type'=>'select',
        'name'=>'payment_method_control',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'label_smaller',
        'class'=>'marginLeft10 resetAppearanceForSelect',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aSignupSourceControl=[
        'type'=>'select',
        'name'=>'signup_source_control',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'label_smaller',
        'class'=>'marginLeft10 resetAppearanceForSelect',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    /**
     * @var array
     */
    protected static $aProductsControl=[
        'type'=>'select',
        'name'=>'products_control',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'label_smaller',
        'class'=>'marginLeft10 resetAppearanceForSelect',
        'inGroup'=>false,
        'closeGroup'=>false
    ];

    public  $mappings;

    public function __construct(){
        $this->oMember=new Member();
        $this->mappings=new CampaignFormDbMapping();
        $this->aDefaultOptions=[
            self::$aPlayersMatch['name']=>self::$aPlayersMatch,
            self::$aMasterSwitch['name']=>self::$aMasterSwitch,
            //self::$aOperatorSwitch['name']=>self::$aOperatorSwitch,
            //self::$aDateOperatorSwitch['name']=>self::$aDateOperatorSwitch,
        ];

    }


    /**
     * @param $iCampaignId
     * @param string $ctrl
     * @return mixed
     */
    public function getCampaignRecipientValues($iCampaignId,$ctrl='master_switch',$value){

        return self::formatRecipientOptions($aNewConfig,$ctrl,$value);

    }


    /**
     * @param $aRecipientValues
     * @param $ctrl
     * @return mixed
     */
    public function formatRecipientOptions($aRecipientValues,$ctrl,$value=''){

        $aReturnControl=($ctrl=='players_match') ? $this->aDefaultOptions['players_match'] : $this->aDefaultOptions['master_switch'];

        if($value) $aReturnControl['value']=$value;

        if ($ctrl=='master_switch' && $value) {
            foreach( $aReturnControl['options'] as $mskey => $msvalue ) {
                if( $msvalue['value'] != $value) {
                    unset($aReturnControl['options'][$mskey]);
                }
            }
        }

        return $aReturnControl;
    }

    /**
     * @param $iCampaignId
     */
    public function getAllSegmentsByCampaign($iCampaignId){

        $sSql='SELECT t1.*
                FROM '.static::$campaignRecipientsSegmentTable.' t1
                JOIN '.static::$campaignRecipientsSegmentCampaignPivotTable.' t2 ON t2.fk_segment_id=t1.id
                WHERE t2.fk_campaign_id='.(int)$iCampaignId;

        $aResults=\DAL::executeQuery($sSql);

        return $aResults;
    }

    /**
     * Get all segments that have conditions saved
     * Use in segment loader
     */
    public function getAllRecipientsSegments(){

        $sSql='SELECT * FROM '.static::$campaignRecipientsSegmentTable.' WHERE '.static::$campaignRecipientsSegmentConditionsColumn.' IS NOT NULL AND
                '.static::$campaignRecipientsSegmentConditionsColumn.'!=""';

        $aResults=\DAL::executeQuery($sSql);

        return $aResults;
    }

    /**
     * Fetch all recipients lists from CRM
     * @todo Get real data
     * @return array
     */
    public function getRecipientsListsFromCRM(){
        $lists= [
            [
                'id'=>1,
                'list_name'=>'List 1'
            ],
            [
                'id'=>2,
                'list_name'=>'List 2'
            ]
        ];
        return $lists;
    }

    /**
     * @param $iCampaignId
     * @param bool $json
     * @return string
     */
    public function getRecipientsPlayersMatchControl($iCampaignId,$json=false,$value=0){

        $aTemplate=array();

        $aTemplate=self::getCampaignRecipientValues($iCampaignId,$ctrl='players_match',$value);

        return ($json) ? json_encode($aTemplate) : $aTemplate;
    }

    /**
     * @param $iCampaignId
     * @param bool $json
     * @return string
     */
    public function getNewCampaignRecipients($iCampaignId,$json=false,$value=''){

        $aTemplate=array();

        $aTemplate=self::getCampaignRecipientValues($iCampaignId,$ctrl='master_switch',$value);

        return ($json) ? json_encode($aTemplate) : $aTemplate;
    }

    /**
     * @param $sControlName
     * @param $aControlValue
     * @return array
     */
    public function getConditionControls($sControlName,$aControlValue){

        switch ($sControlName) {
            case 'joined_date':
            case 'last_order_date':
                $aOperatorControl=[
                    self::$aDateOperatorSwitch['name']=>self::$aDateOperatorSwitch,
                    self::$aDatePickerControl['name']=>self::$aDatePickerControl,
                ];
                break;
            case 'total_order_amount':
            case 'current_balance':
            case 'life_deposits':
            case 'life_order':
                self::$aCurrencyControl['options']=self::getCurrencies();
                self::$aCurrencyControl['labelClass']='label_smaller';
                self::$aCurrencyControl['class']='marginLeft10 resetAppearanceForSelect';

                self::$aOperatorSwitch['labelClass']='label_smaller';

                self::$aTextControl['labelClass']='label_smaller';
                self::$aTextControl['class']='';

                $aOperatorControl=[
                    self::$aOperatorSwitch['name']=>self::$aOperatorSwitch,
                    self::$aTextControl['name']=>self::$aTextControl,
                    self::$aCurrencyControl['name']=>self::$aCurrencyControl,
                ];
                break;
            case 'location':
                self::$aCountryControl['options']=self::getCountries();

                $aOperatorControl=[
                    self::$aCountryControl['name']=>self::$aCountryControl,
                ];
                break;
            case 'language':
                self::$aLanguageControl['options']=self::getLanguages();

                $aOperatorControl=[
                    self::$aLanguageControl['name']=>self::$aLanguageControl,
                ];
                break;
            case 'payment_method':
                self::$aPaymentControl['options']=self::getPaymentMethods();

                $aOperatorControl=[
                    self::$aPaymentControl['name']=>self::$aPaymentControl,
                ];
                break;
            case 'signup_source':
                self::$aSignupSourceControl['options']=self::getAcquisitionSource();

                $aOperatorControl=[
                    self::$aSignupSourceControl['name']=>self::$aSignupSourceControl,
                ];
                break;
            case 'played_product':
                self::$aProductsControl['options']=self::getProducts();

                $aOperatorControl=[
                    self::$aProductsControl['name']=>self::$aProductsControl,
                ];
                break;
            case 'jubilee':
                $aOperatorControl=[
                    self::$aOperatorSwitch['name']=>self::$aOperatorSwitch,
                    //self::$aDateOperatorSwitch['name']=>self::$aDateOperatorSwitch,
                    self::$aDatePickerControl['name']=>self::$aDatePickerControl,
                ];
                break;
            case 'birthday':
                $aBdayFromControl=self::$aDatePickerControl;
                $aBdayFromControl['name']='date_picker_bdayFrom';
                $aBdayFromControl['label']='From';
                $aBdayFromControl['placeholder']='Date From';
                $aBdayFromControl['class']='datepicker';

                $aBdayToControl=self::$aDatePickerControl;
                $aBdayToControl['name']='date_picker_bdayTo';
                $aBdayToControl['label']='To';
                $aBdayToControl['placeholder']='Date To';
                $aBdayToControl['class']='datepicker';
                $aOperatorControl=[
                    self::$aDatePickerControl['name'].'_bdayFrom'=>$aBdayFromControl,
                    self::$aDatePickerControl['name'].'_bdayTo'=>$aBdayToControl,
                ];
                break;
            default:
                return false;
                break;
        }

        // Set the values
        if(!empty($aControlValue)){
            foreach ($aControlValue as $key => $value) {
                $aOperatorControl[$key]['value']=$value;
            }
        }

        return ['fields'=>$aOperatorControl];
    }

    public function getExistingMembersIds($aMemberDetails){
        // array of existing users ids validated either by id or email
        $aExistinMembersIds=[];

        if($aMemberDetails['email']){
            // check if email exists
            if( !$this->oMember->checkUsernameAvailable($aMemberDetails['email']) ){
                $sSql='SELECT member_id FROM members WHERE email ="'.$aMemberDetails['email'].'"';
                $aExistingMembersIds=\DAL::executeGetOne($sSql);
            }
        }
        elseif($aMemberDetails['member_id']){
            // check if id exists
            $sSql='SELECT member_id FROM members WHERE member_id='.intval($aMemberDetails['member_id']);
            $aExistingMembersIds=\DAL::executeGetOne($sSql);
        }

        return $aExistingMembersIds;
    }

    public function getNewMembersIds($aMemberDetails){
        // array of existing users ids validated either by id or email
        $aNewMembersIds=[];

        // Insert new members only if the email exists (minimum requirement)
        if($aMemberDetails['email']){
            if( $this->oMember->checkUsernameAvailable($aMemberDetails['email']) ){
                $aNewMembersIds=self::addNewMember($aMemberDetails);
            }
        }

        return $aNewMembersIds;
    }

    public function addNewMember($aMemberDetails){

        unset($aMemberDetails['member_id']);

        // check for member table requred filds
        if(!array_key_exists('username',$aMemberDetails)){
            $aMemberDetails['username']=$aMemberDetails['email'];
        }
        if(!array_key_exists('fk_country_id',$aMemberDetails)) {
            $aMemberDetails['fk_country_id']=229; # Default for UK for now to avoid FK constraints
        }
        if(array_key_exists('password',$aMemberDetails)) {
            $aMemberDetails['password']=Encryption::Encrypt($aMemberDetails['password']);
        }
        // flag them as lead
        if(!array_key_exists('fk_status_id',$aMemberDetails)) {
            $aMemberDetails['fk_status_id']=1; # Defaults to Lead for imported users 
        }
        $sTablename="members";

        // add new users as members
        $iNewMemberId=DAL::Insert($sTablename, $aMemberDetails, true);

        return $iNewMemberId;
    }

    /**
     * @param $iCampaignId
     * @param $aRecipientValues
     * @param $sSegmentName
     * @param $iPlayersMatchValue
     * @return bool
     */
    public function storeRecipientOptions($iCampaignId,array $aRecipientValues,$sSegmentName,$iPlayersMatchValue){

        if(isset($aRecipientValues['recipientsXls']) && !empty($aRecipientValues['recipientsXls'])){

            $aXlsMembers=\CampaignHelpers::csvXlsDataParser($aRecipientValues['recipientsXls']);

            // array of existing users ids validated either by id or email
            $aExistingMembersIds=[];
            $aNewMembersIds=[];
            $aInsertIds=[];

            // check for existing members
            foreach ($aXlsMembers as $aMember) {
                // get existing members ids
                $aExistingMembersIds[]=self::getExistingMembersIds($aMember);
                // get new members ids
                $aNewMembersIds[]=self::getNewMembersIds($aMember);

            }

            // array of existing and new members ids
            $aInsertIds=array_merge(
                array_filter($aExistingMembersIds),
                array_filter($aNewMembersIds)
            );

            // print_r($aInsertIds);          

            // check if the ids have already been registered for this campaign promotion
            $sSql="SELECT fk_member_id FROM campaign_promo_members_xref 
            WHERE fk_member_id IN(".implode(',',array_map('intval',$aInsertIds)).")
            AND fk_campaign_id =".$iCampaignId;
            $aExistingMemberIds=\DAL::executeQuery($sSql);

            $aExistingIds=iterator_to_array(new RecursiveIteratorIterator(
                new RecursiveArrayIterator($aExistingMemberIds)), false);

            // prepare final array of ids for insertion
            $aFinalIds=array_diff($aInsertIds,$aExistingIds);

            // Save ids in the xref table
            $aRecipientsInsertedIds=[];
            foreach ($aFinalIds as $aFinalId) {
                $aRecipientsInsertedIds[]=\DAL::Insert('campaign_promo_members_xref',[
                    'fk_campaign_id'=>$iCampaignId,
                    'fk_member_id'=>$aFinalId,
                ], true);
            }

            return $aRecipientsInsertedIds;
        }

        $oRecipientOptions=self::createOptionsObject($aRecipientValues,$iPlayersMatchValue);
        $aSegmentOptions=[
            'segment_name'=>$sSegmentName,
            'segment_conditions'=>$oRecipientOptions
        ];

        // check if campaign has an assigned segment
        $sSql="SELECT fk_segment_id FROM campaign_promo_segments_xref WHERE fk_campaign_id=".$iCampaignId;
        $iCampaignSegmentId=\DAL::executeGetOne($sSql);

        $iOptionsId=\DAL::Insert('campaign_promo_segments',$aSegmentOptions,true);

        // if this is the first segment of the campaign add it
        if($iOptionsId && !$iCampaignSegmentId){
            $iCampaignXrefId=\DAL::Insert('campaign_promo_segments_xref',[
                'fk_campaign_id'=>$iCampaignId,
                'fk_segment_id'=>$iOptionsId,
            ], true);

            return true;
        }
        // else update the existing segment_xref
        elseif($iCampaignSegmentId){
            $aUpdatedSegmentId[]=\DAL::Update('campaign_promo_segments_xref',[
                    'fk_campaign_id'=>$iCampaignId,
                    'fk_segment_id'=>$iOptionsId,
                ],
                'fk_segment_id = '.$iCampaignSegmentId, true);

            return true;
        }

        return false;
    }

    /**
     * @param string $aRecipientValues
     * @return string
     */
    public function createOptionsObject($aRecipientValues='',$iPlayersMatchValue=''){

        $aOptions=array();
        $n=0;

        $aOptions['players_match']=$iPlayersMatchValue;
        foreach ($aRecipientValues as $aRecipientValue) {

            foreach ($aRecipientValue as $key => $value) {
                if($key != 'master_switch') {
                    $aOptions[$aRecipientValue['master_switch'].'_'.$n][$key]=$value;
                }
            }

            $n++;
        }
        return json_encode($aOptions);
    }

    /**
     * @param $iSegmentId
     * @return mixed
     */
    public function loadRecipientSegment($iSegmentId){

        // return if there is no valid segment
        if(!is_numeric($iSegmentId)) return false;

        $sSql='SELECT segment_conditions FROM campaign_promo_segments WHERE id='.$iSegmentId;
        $sSegmentOptionValues=\DAL::executeGetOne($sSql);

        $d=json_decode($sSegmentOptionValues,true);

        $fullCtrl=array();
        foreach ($d as $key => $value) {
            $ctrlName=preg_replace('/_\d+/', '', $key);

            if($ctrlName=='players_match'){
                $fullCtrl[][$ctrlName]=self::getRecipientsPlayersMatchControl($iCampaignId,false,$value);
            }
            else{
                $fullCtrl[][$ctrlName]=
                    array_merge( self::getNewCampaignRecipients($iCampaignId,false,$ctrlName),
                        self::getConditionControls($ctrlName,$value));
            }
        }
        // print_r($fullCtrl);
        return json_encode($fullCtrl);
    }

    /**
     * @param $iCampaignId
     * @return mixed
     */
    public function getCampaignSegment($iCampaignId){

        $sSql='SELECT t1.id,t1.segment_name
        FROM '.static::$campaignRecipientsSegmentTable.' t1
        JOIN '.static::$campaignRecipientsSegmentCampaignPivotTable.' t2 ON t2.fk_segment_id=t1.id
        WHERE t2.fk_campaign_id='.(int)$iCampaignId.' ORDER BY t1.id DESC LIMIT 1';

        return \DAL::executeGetRow($sSql);
    }


    /**
     * @return array
     * @todo move to parent
     */
    public function getCurrencies(){

        $sSql="SELECT currency_id, fullname FROM currencies";

        $sQueryValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sQueryValues as $value) {
            $aOptions[]=['name'=>$value['fullname'],'value'=>$value['currency_id']];
        }

        return $aOptions;
    }

    /**
     * @return array
     * @todo move to parent
     */
    public function getCountries(){

        $sSql="SELECT country_id, comment FROM countries_cmn";

        $sQueryValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sQueryValues as $value) {
            $aOptions[]=['name'=>$value['comment'],'value'=>$value['country_id']];
        }

        return $aOptions;
    }

    /**
     * @return array
     * @todo move to parent
     */
    public function getLanguages(){

        $sSql="SELECT language_id, title FROM languages";

        $sQueryValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sQueryValues as $value) {
            $aOptions[]=['name'=>$value['title'],'value'=>$value['language_id']];
        }

        return $aOptions;
    }

    /**
     * @return array
     * @todo move to parent
     */
    public function getPaymentMethods(){

        $sSql="SELECT gateway_id, comment FROM gateways";

        $sQueryValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sQueryValues as $value) {
            $aOptions[]=['name'=>$value['comment'],'value'=>$value['gateway_id']];
        }

        return $aOptions;
    }

    /**
     * @return array
     * @todo move to parent
     */
    public function getAcquisitionSource(){

        $sSql="SELECT id, name FROM affiliate_adverts";

        $sQueryValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sQueryValues as $value) {
            $aOptions[]=['name'=>$value['name'],'value'=>$value['id']];
        }

        return $aOptions;
    }

    /**
     * @return array
     * @todo move to parent
     */
    public function getProducts(){

        $sSql="SELECT id, name FROM game_engines";

        $sQueryValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sQueryValues as $value) {
            $aOptions[]=['name'=>$value['name'],'value'=>$value['id']];
        }

        return $aOptions;
    }

    /**
     * Parse campaign recipients conditions and return valid campaigns to apply to member
     * @param $iMemberId
     * @param array $aActiveCampaigns
     * @return array
     */
    public function getCampaignsFromConditions($iMemberId,array $aActiveCampaigns){
        //$iMemberId=3;

        $aValidCampaigns=[];
        foreach($aActiveCampaigns AS $aCampaign){

            $iCampaignId=$aCampaign['id'];

            $oSegmentConditions=json_decode($aCampaign['segment_conditions']);

            $operator=$oSegmentConditions->players_match;

            unset($oSegmentConditions->players_match);

            $isValid = $this->parseConditions($iMemberId, $oSegmentConditions, $operator);
            if($isValid)
                array_push($aValidCampaigns,$iCampaignId);
        }

        return $aValidCampaigns;
    }

    /**
     * Check if member is part of a campaigns member list
     * @param $iMemberId
     * @return array
     */
    public function getCampaignsFromList($iMemberId){

        $sSql="SELECT fk_campaign_id AS campaign_id FROM campaign_promo_members_xref t1"
            ." JOIN members t2 ON t1.fk_member_id=t2.member_id"
            ." JOIN campaigns t3 ON t1.fk_campaign_id=t3.id"
            ." WHERE t3.is_active=1 AND DATE(now()) BETWEEN DATE(t3.start_date) AND DATE(t3.end_date) "
            ." AND t2.member_id=".(int)$iMemberId;

        $aResults=\DAL::executeQuery($sSql);

        return !empty($aResults) ? CampaignHelpers::flattenArray($aResults) : [];
    }

    /**
     * @param $oCondition
     * @return array
     * @todo move to helpers
     */
    private static function formatConditions($oCondition){
        $conditions=[
            'switch'=>null,
            'value'=>null,
            'additional'=>[]
        ];

        foreach($oCondition AS $k=>$v){
            if(preg_match('/switch/iu',$k)){
                $conditions['switch']=$v;
            }
            elseif(preg_match('/picker|control/iu',$k)){
                if(null!=$conditions['value']){
                    $additionalKey=preg_match('/date\_/iu',$k) ? 'date' : preg_replace('/\_control/iu','',$k);
                    $conditions['additional'][$additionalKey]=$v;
                }
                else{
                    $conditions['value']=$v;
                }

            }
        }

        return $conditions;
    }

    /**
     * @param $iMemberId
     * @param $oSegmentConditions
     * @param $operator
     * @return bool
     */
    protected function parseConditions($iMemberId, $oSegmentConditions, $operator)
    {
        foreach ($oSegmentConditions AS $k => $oCondition) {

            $r = static::formatConditions($oCondition);

            $sMethod = preg_replace('/\_\d+/iu', '', $k);
            $sMethod = CampaignHelpers::formatStringForMethodName($sMethod);
            $sMethodName = "get{$sMethod}";

            //echo "\n$sMethodName\n";

            if (method_exists($this->mappings, $sMethodName)) {

                /*print "<pre>";
                var_dump($this->mappings->$sMethodName($iMemberId, $r, 'bool'));
                print "</pre>";*/

                $promoCandidate = $this->mappings->$sMethodName($iMemberId, $r, 'bool');

                if ($operator == 0 && $promoCandidate === true) {
                    return true ;
                }

                if ($operator == 1 && $promoCandidate === false) {
                    return false ;
                }
            }

        }
        return $operator==1 ? true : false;
    }
}