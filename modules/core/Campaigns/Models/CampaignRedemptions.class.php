<?php

/**
 * Model/Repository Class for campaign redemptions
 *
 * Class CampaignRedemptions
 */
class CampaignRedemptions extends CampaignEntity{

    public $aDefaultOptions;

    protected static $aSinglePromoCode=[
        'type'=>'text',
        'name'=>'promo_code',
        'value'=>null,
        'placeholder'=>'Enter Promo Code (minimum three chars)',
        'label'=>'Create single promo code',
        'labelClass'=>'long inConjuction',
        'class'=>'label_smaller number checkOnType',
        'inGroup'=>false,
        'closeGroup'=>false,
        'after_element'=>'<div class="clear afterElement">OR</div>'
    ];

    protected static $aMultiplePromoCodes=[
        'type'=>'text',
        'name'=>'multiple_promo_codes',
        'value'=>null,
        'placeholder'=>'Enter code prefix for multiple codes generation',
        'data'=>['prefix'=>1],
        'label'=>'Create multiple promo codes',
        'labelClass'=>'long inConjuction',
        'class'=>'label_smaller number checkOnType',
        'inGroup'=>false,
        'closeGroup'=>false,
        'exportCsv'=>true
    ];

    protected static $aNumberofPromoCodes=[
        'type'=>'text',
        'name'=>'number_of_promo_codes',
        'value'=>0,
        'label'=>'Number of promo codes to create',
        'labelClass'=>'long inConjuction',
        'class'=>'label_smaller number',
        'inGroup'=>false,
        'closeGroup'=>false,
        'after_element'=>'<div class="optionsSeparator clear"></div>'
    ];

    protected static $aAffiliateId=[
        'type'=>'text',
        'name'=>'fk_affiliate_id',
        'value'=>0,
        'label'=>'Enter affiliate or partner [id or name] (optional)',
        'labelClass'=>'long',
        'class'=>'smaller number campaignRedemptionAffiliate',
        'inGroup'=>false,
        'closeGroup'=>false,
        'after_element'=>'<div class="clear afterElement"><h2>Set Limits</h2></div><div class="optionsSeparator noPreMargin clear"></div>',
        'placeholder'=>'Search'
    ];

    protected static $aUseInConjunction=[
        'type'=>'checkbox',
        'name'=>'use_in_conjunction',
        'value'=>0,
        'label'=>'Allow bonus to be used in conjunction with other offer',
        'labelClass'=>'long',
        'class'=>'smaller number',
        'inGroup'=>false,
        'closeGroup'=>false,
    ];

    protected static $aOtherBonusesTags=[
        'type'=>'text',
        'name'=>'campaign_tags',
        'disabled'=>true,
        'value'=>0,
        'label'=>'Enter campaign tag',
        'labelClass'=>'long',
        'class'=>'smaller number campaignTags',
        'inGroup'=>false,
        'closeGroup'=>false,
        'after_element'=>'<div class="clear afterElement">OR select campaign below</div>',
        'placeholder'=>'Search'
    ];

    protected static $aOtherBonusesCampaignNames=[
        'type'=>'select',
        'multiple'=>true,
        'disabled'=>true,
        'name'=>'campaign_names',
        'value'=>0,
        'label'=>'Campaign Names',
        'labelClass'=>'long',
        'class'=>'smaller',
        'inGroup'=>false,
        'closeGroup'=>false,
        'after_element'=>'<div class="optionsSeparator clear"></div>'
    ];

    protected static $aNumberOfPlayers=[
        'type'=>'text',
        'name'=>'number_of_players',
        'value'=>0,
        'label'=>'Number of players allowed to use this bonus',
        'labelClass'=>'long',
        'class'=>'smaller number',
        'inGroup'=>false,
        'closeGroup'=>false,
        'placeholder'=>'Insert number'
    ];

    protected static $aMaximumCampaignValue=[
        'type'=>'text',
        'name'=>'maximum_campaign_value',
        'value'=>0,
        'label'=>'Maximum total value of campaign',
        'labelClass'=>'long',
        'class'=>'label_smaller number',
        'inGroup'=>true,
        'closeGroup'=>false,
        'placeholder'=>'Insert number'
    ];

    protected static $aMaximumCampaignValueCurrency=[
        'type'=>'select',
        'name'=>'fk_maximum_value_currency',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'long',
        'class'=>'label_smaller marginLeft10 resetAppearanceForSelect',
        'inGroup'=>true,
        'closeGroup'=>true,

    ];

    protected static $aMaximumBonusPerCustomer=[
        'type'=>'text',
        'name'=>'maximum_bonus_per_customer',
        'value'=>0,
        'label'=>'Number of maximum bonus per customer',
        'labelClass'=>'long',
        'class'=>'smaller number',
        'inGroup'=>false,
        'closeGroup'=>false,
        'placeholder'=>'Insert number'
    ];

    protected static $aMaximumValuePerCustomer=[
        'type'=>'text',
        'name'=>'maximum_value_per_customer',
        'value'=>0,
        'label'=>'Maximum total value per customer',
        'labelClass'=>'long',
        'class'=>'label_smaller number',
        'inGroup'=>true,
        'closeGroup'=>false,
        'placeholder'=>'Insert number'
    ];

    protected static $aMaximumValuePerCustomerCurrency=[
        'type'=>'select',
        'name'=>'fk_maximum_value_per_customer_currency',
        'value'=>0,
        'label'=>'',
        'labelClass'=>'small',
        'class'=>'label_smaller marginLeft10 resetAppearanceForSelect',
        'inGroup'=>true,
        'closeGroup'=>true,
        'placeholder'=>'Insert number'
    ];

    protected static $aExpirationDateInWallet=[
        'type'=>'text',
        'name'=>'code_expires_at',
        'value'=>0,
        'label'=>'Expiration of bonus in wallet',
        'labelClass'=>'long',
        'class'=>'halfsize  smaller datepicker',
        'inGroup'=>false,
        'closeGroup'=>false,
        'placeholder'=>'Click to open date picker'
    ];


    public function __construct(){

        $this->aDefaultOptions=[
            self::$aSinglePromoCode['name']=>self::$aSinglePromoCode,
            self::$aMultiplePromoCodes['name']=>self::$aMultiplePromoCodes,
            self::$aNumberofPromoCodes['name']=>self::$aNumberofPromoCodes,
            self::$aAffiliateId['name']=>self::$aAffiliateId,
            self::$aUseInConjunction['name']=>self::$aUseInConjunction,
            self::$aOtherBonusesTags['name']=>self::$aOtherBonusesTags,
            self::$aOtherBonusesCampaignNames['name']=>self::$aOtherBonusesCampaignNames,
            self::$aNumberOfPlayers['name']=>self::$aNumberOfPlayers,
            self::$aMaximumCampaignValue['name']=>self::$aMaximumCampaignValue,
            self::$aMaximumCampaignValueCurrency['name']=>self::$aMaximumCampaignValueCurrency,
            self::$aMaximumBonusPerCustomer['name']=>self::$aMaximumBonusPerCustomer,
            self::$aMaximumValuePerCustomer['name']=>self::$aMaximumValuePerCustomer,
            self::$aMaximumValuePerCustomerCurrency['name']=>self::$aMaximumValuePerCustomerCurrency,
            self::$aExpirationDateInWallet['name']=>self::$aExpirationDateInWallet,
        ];
    }

    public function getCampaignRedemptionsValues($iCampaignId){

        $aOptions=array();

        // get redemptions options
        $sSql="SELECT cpcl.*
		FROM campaign_promo_code_limit_campaign cpclc
		JOIN campaign_promo_code_limits cpcl ON cpcl.id = cpclc.fk_campaign_promo_code_limit_id
		WHERE cpclc.fk_campaign_id=".(int)$iCampaignId;
        $aOptions=\DAL::executeQuery($sSql);

        // Overwrite campaign tag id with tag name
        $sSql="SELECT t.name AS campaign_tags FROM tags t
		JOIN campaign_promo_code_other_tags cpcot ON cpcot.fk_tag_id=t.id
		WHERE cpcot.fk_limit_id=".(int)$aOptions[0]['id'];
        $aTags=\DAL::executeQuery($sSql);

        // PHP >= 5.5.0
        $sTagNames=implode(',', array_column($aTags, 'campaign_tags'));
        $aOptions[0]['campaign_tags']=$sTagNames;

        // get other campaigns the promo applies to
        $sSql="SELECT fk_campaign_id AS campaign_names FROM campaign_promo_code_other_campaigns WHERE fk_limit_id=".(int)$aOptions[0]['id'];
        $aOtherCampaigns=\DAL::executeQuery($sSql);

        // campaign names is an array of campaign ids. The handling of display as a multiple select box should be done in the view
        $aOptions[0]['campaign_names']=CampaignHelpers::flattenArray($aOtherCampaigns);

        // get redemption promo codes
        $sSql="SELECT promo_code FROM campaign_promo_codes cpc WHERE cpc.fk_campaign_id=".(int)$iCampaignId;

        $aPromoCodes=\DAL::executeQuery($sSql);
        $aNewPromoCodes=[];

        if(count($aPromoCodes)>0) {
            //$aNewPromoCodes=$aPromoCodes[0];
            $i=0;
            $aMultiplePromoCodes=[];
            foreach ($aPromoCodes as $value) {
                if($aPromoCodes[$i]){
                    $aMultiplePromoCodes[]=$aPromoCodes[$i]['promo_code'];
                }
                $i++;
            }

            $aNewPromoCodes['multiple_promo_codes']=implode(',',$aMultiplePromoCodes);
        }
        else{

            $aNewPromoCodes=$aPromoCodes[0];
        }

        $aNewConfig=[];
        if(CampaignHelpers::issetNotEmptyNotNull($aOptions)){
            $aNewConfig=array_merge($aOptions[0],$aNewPromoCodes);
        }

        return self::formatRedemptionsOptions($aNewConfig);
    }

    public function formatRedemptionsOptions($aRedemptionValues){

        foreach ($this->aDefaultOptions as $key => $value){

            if($key=='fk_maximum_value_per_customer_currency' || $key=='fk_maximum_value_currency'){
                $this->aDefaultOptions[$key]['options']=self::getCurrencies();
            }

            if($key=='campaign_names'){
                $this->aDefaultOptions[$key]['options']=self::getAllCampaignsValues();
            }
        }
        // append saved values to default controls
        if(count($aRedemptionValues)>1){

            // unset unneeded db entries
            unset($aRedemptionValues['fk_campaign_promo_code_id']);
            unset($aRedemptionValues['id']);

            foreach ($aRedemptionValues as $key => $value){
                // set the saved value
                $this->aDefaultOptions[$key]['value']=$value;
            }
        }

        return ['fields'=>$this->aDefaultOptions];
    }

    public function getCurrencies(){

        $sSql="SELECT currency_id, fullname FROM currencies";

        $sGatewaysValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sGatewaysValues as $value) {
            $aOptions[]=['name'=>$value['fullname'],'value'=>$value['currency_id']];
        }

        return $aOptions;
    }

    public function getAllCampaignsValues(){

        $sSql='SELECT id, name FROM campaigns';

        $sCampaignsValues=\DAL::executeQuery($sSql);

        $aOptions=[];
        foreach ($sCampaignsValues as $value) {
            $aOptions[]=['name'=>$value['name'],'value'=>$value['id']];
        }

        return $aOptions;
    }

    public function storeRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions){

        // DB operations

        // Check if there are saved options for the campaign
        $sSql="SELECT * FROM campaign_promo_codes WHERE fk_campaign_id=".(int) $iCampaignId;

        $aPromoCodes=\DAL::executeQuery($sSql);

        if(CampaignHelpers::issetNotEmptyNotNull($aPromoCodes)){

            // Update redemption options
            $sCleared=self::clearRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions);
            $sSaved=self::saveRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions);

        }
        else{

            // Insert new redemption Options
            $sSaved=self::saveRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions);
        }

        return $sSaved;
    }

    private function getAffiliateIdByName($sAffiliateName){
        $sSql='SELECT fk_user_id AS id FROM affiliates WHERE company = "'.$sAffiliateName.'"';

        return \DAL::executeGetOne($sSql);
    }
    private function saveRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions){

        $aCampaignPromoCodeIds=array();

        // insert promocode
        //$fk_affiliate_id=(!empty($aCampaignRedemptionOptions['fk_affiliate_id']))?$aCampaignRedemptionOptions['fk_affiliate_id']:NULL;
        if(isset($aCampaignRedemptionOptions['promo_code'])){
            $aCampaignPromoCodeIds[]=\DAL::Insert('campaign_promo_codes',[
                'promo_code'=>$aCampaignRedemptionOptions['promo_code'],
                'fk_campaign_id'=>$iCampaignId,
            ], true);
        }

        if($aCampaignRedemptionOptions['multiple_promo_codes'] && $aCampaignRedemptionOptions['number_of_promo_codes']){
            $aRandPromoCodes=self::generateRandomString(
                $aCampaignRedemptionOptions['multiple_promo_codes'],
                12,
                $aCampaignRedemptionOptions['number_of_promo_codes']
            );


            foreach ($aRandPromoCodes as $sPromoCodes) {
                $aCampaignPromoCodeIds[]=\DAL::Insert('campaign_promo_codes',[
                    'promo_code'=>$sPromoCodes,
                    'fk_campaign_id'=>$iCampaignId,
                ], true);
            }

        }

        // insert promo code options
        $iUseInConjuction=(isset($aCampaignRedemptionOptions['use_in_conjunction']))?1:0;
        if(isset($aCampaignRedemptionOptions) && CampaignHelpers::issetNotEmptyNotNull($aCampaignPromoCodeIds)){

            $aRedemtionOptions=array(
                'use_in_conjunction'=>$iUseInConjuction,
                'number_of_players'=>$aCampaignRedemptionOptions['number_of_players'],
                'maximum_campaign_value'=>$aCampaignRedemptionOptions['maximum_campaign_value'],
                'fk_maximum_value_currency'=>$aCampaignRedemptionOptions['fk_maximum_value_currency'],
                'maximum_bonus_per_customer'=>$aCampaignRedemptionOptions['maximum_bonus_per_customer'],
                'maximum_value_per_customer'=>$aCampaignRedemptionOptions['maximum_value_per_customer'],
                'fk_maximum_value_per_customer_currency'=>$aCampaignRedemptionOptions['fk_maximum_value_per_customer_currency'],
                'code_expires_at'=>$aCampaignRedemptionOptions['code_expires_at']
            );
            if(CampaignHelpers::issetNotEmptyNotNull($aCampaignRedemptionOptions['fk_affiliate_id'])){
                $aRedemtionOptions['fk_affiliate_id']=is_numeric($aCampaignRedemptionOptions['fk_affiliate_id']) ? $aCampaignRedemptionOptions['fk_affiliate_id'] : self::getAffiliateIdByName($aCampaignRedemptionOptions['fk_affiliate_id']);

            }

            $iCampaignPromoOptionsId=\DAL::Insert('campaign_promo_code_limits',$aRedemtionOptions, true);
        }

        // populate campaign_promo_limit_xref table
        if( CampaignHelpers::issetNotEmptyNotNull($aCampaignPromoCodeIds) && $iCampaignPromoOptionsId ){
            foreach ($aCampaignPromoCodeIds as $iCampaignPromoCodeId) {
                $iCampaignPromoOptionsXref=\DAL::Insert('campaign_promo_code_limits_xref',['fk_promo_id'=>$iCampaignPromoCodeId,'fk_limit_id'=>$iCampaignPromoOptionsId]);
            }
        }

        // insert promo code limit campaign
        if(isset($aCampaignRedemptionOptions) && $iCampaignPromoOptionsId){
            $iCampaignPromoCodeLimitCampaign=\DAL::Insert('campaign_promo_code_limit_campaign',[
                'fk_campaign_promo_code_limit_id'=>$iCampaignPromoOptionsId,
                'fk_campaign_id'=>$iCampaignId
            ], true);

        }

        // Get tags Ids
        $aTagsIds=array();
        if( CampaignHelpers::issetNotEmptyNotNull($aCampaignRedemptionOptions['campaign_tags']) ){
            // multiple tags (if implemented)
            if(is_array($aCampaignRedemptionOptions['campaign_tags'])){
                foreach ($aCampaignRedemptionOptions['campaign_tags'] as $sTagName) {
                    $sSql="SELECT id FROM tags WHERE name='".(string)$sTagName."'";
                    $iTagId=\DAL::executeGetOne($sSql);
                    $iOtherTags=\DAL::Insert('campaign_promo_code_other_tags',['fk_limit_id'=>$iCampaignPromoOptionsId,'fk_tag_id'=>$iTagId]);
                }

            }
            // single Tag
            else{
                // get campaign id from tag name
                $sSql="SELECT id FROM tags WHERE name='".(string)$aCampaignRedemptionOptions['campaign_tags']."'";
                $iTagId=\DAL::executeGetOne($sSql);

                $iOtherTags=\DAL::Insert('campaign_promo_code_other_tags',['fk_limit_id'=>$iCampaignPromoOptionsId,'fk_tag_id'=>$iTagId]);
            }
        }

        // populate campaign_promo_other_campaigns table (Campaigns where the promotion also applies)
        // Todo: View - "Apply to other campaigns" control pass multiple selection in the $_POST
        if( CampaignHelpers::issetNotEmptyNotNull($aCampaignRedemptionOptions['campaign_names']) && $iUseInConjuction ){
            // multiple Ids
            if(is_array($aCampaignRedemptionOptions['campaign_names'])){
                foreach ($aCampaignRedemptionOptions['campaign_names'] as $iOtherCampaignId) {
                    $iOtherCampaigns=\DAL::Insert('campaign_promo_code_other_campaigns',['fk_limit_id'=>$iCampaignPromoOptionsId,'fk_campaign_id'=>$iOtherCampaignId]);


                }
            }
            // single Id
            else{
                $iOtherCampaigns=\DAL::Insert('campaign_promo_code_other_campaigns',['fk_limit_id'=>$iCampaignPromoOptionsId,'fk_campaign_id'=>$aCampaignRedemptionOptions['campaign_names']]);
            }
        }

        $bQrs=($iCampaignPromoCodeLimitCampaign) ? true : false;

        return $bQrs;
    }

    private function updateRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions){
    }


    private function clearRedemptionOptions($iCampaignId,$aCampaignRedemptionOptions){

        \DAL::executeQuery('SET foreign_key_checks=0');
        // clean up the DB
        $sSql="DELETE FROM campaign_promo_codes WHERE fk_campaign_id=".(int)$iCampaignId;
        \DAL::executeQuery($sSql);
        //campaign_promo_code_limit_campaign
        $sSql="SELECT fk_campaign_promo_code_limit_id FROM campaign_promo_code_limit_campaign WHERE fk_campaign_id=".(int)$iCampaignId;
        $iPromoLimitsId=\DAL::executeGetOne($sSql);

        $sSql="DELETE FROM campaign_promo_code_limit_campaign WHERE fk_campaign_id=".(int)$iCampaignId;
        \DAL::executeQuery($sSql);

        $sSql="DELETE FROM campaign_promo_code_limits_xref WHERE fk_limit_id=".(int)$iPromoLimitsId;
        \DAL::executeQuery($sSql);

        //campaign_promo_code_limits
        $sSql="DELETE FROM campaign_promo_code_limits WHERE id=".(int)$iPromoLimitsId;
        \DAL::executeQuery($sSql);

        // campaign_promo_code_other_campaigns
        $sSql="DELETE FROM campaign_promo_code_other_campaigns WHERE fk_limit_id=".(int)$iPromoLimitsId;
        \DAL::executeQuery($sSql);

        // campaign_promo_code_other_tags
        $sSql="DELETE FROM campaign_promo_code_other_tags WHERE fk_limit_id=".(int)$iPromoLimitsId;
        \DAL::executeQuery($sSql);

        \DAL::executeQuery('SET foreign_key_checks=1');


    }

    public function generateRandomString($prefix='code',$length=12,$iNumPromoCodes) {
        $aPromoCodes=array();
        for($i=0;$i<$iNumPromoCodes;$i++){
            // exclude 1,0, t, I, o, d, j, l
            $aPromoCodes[]=substr($prefix.str_shuffle("23456789abcefghikmnpqrsuvwxyz"), 0, $length);
        }
        return $aPromoCodes;
    }


}