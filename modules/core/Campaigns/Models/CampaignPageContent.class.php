<?php
/**
 * Model/Repository Class for campaign page content
 *
 * Class CampaignPageContent
 */
class CampaignPageContent extends CampaignEntity{

	public function storePageContentProperties($iCampaignId,$aCampaignContentOptions){

		// check if campaign content exists
		$iStoredPageContent=self::checkPageContentExists($iCampaignId);

		// update existing content
		if($iStoredPageContent){
			if(self::updatePageContent($iCampaignId,$aCampaignContentOptions))
				echo json_encode('Campaign Content Updated');
			else
				echo json_encode('Campaign Content Update Failed');
		}
		else{

			$aCampaignContentIds[]=\DAL::Insert('campaign_promo_content',[
	                'destination'=>$aCampaignContentOptions['campaignPageContentPromoPageDestination'],
	                'destination_url'=>$aCampaignContentOptions['campaignPageContentDestinationUrl'],
	                'page_text'=>$aCampaignContentOptions['campaignPageContentOnPageText'],
	                'betslip_text'=>$aCampaignContentOptions['campaignPageContentBetSlipText'],
	                'termsConditions'=>$aCampaignContentOptions['campaignPageContentTermsConditions'],
	                'fk_campaign_id'=>$iCampaignId,
	            ], true);

			echo json_encode('Page content saved');
		}
	}

	public function getPageContentProperties($iCampaignId){

		$aContentOptions=array();

		// check if campaign content exists
		$iStoredPageContent=self::checkPageContentExists($iCampaignId);

		if($iStoredPageContent){
			
			//print_r(Config::$config->webUrl.'images/campaigns/campaign-image-'.$iCampaignId.'.png');
			$sSql='SELECT * FROM campaign_promo_content WHERE fk_campaign_id='.(int)$iCampaignId;
			$aContentOptions=\DAL::executeGetRow($sSql);

			// get the image
			if(file_exists(Config::$config->sCampaignImagesUploadPath.'/campaigns/campaign-image-'.$iCampaignId.'.png')){
				$aContentOptions['ContentImage']=Config::$config->webUrl.'images/campaigns/campaign-image-'.$iCampaignId.'.png';
			}

			// get the icon
			if(file_exists(Config::$config->sCampaignImagesUploadPath.'/campaigns/campaign-icon-'.$iCampaignId.'.png')){
				$aContentOptions['ContentIcon']=Config::$config->webUrl.'images/campaigns/campaign-icon-'.$iCampaignId.'.png';
			}

			return $aContentOptions;
		}

		return false;
	}

	private function checkPageContentExists($iCampaignId){
		$sSql="SELECT id FROM campaign_promo_content WHERE fk_campaign_id=".(int)$iCampaignId;
        return \DAL::executeGetOne($sSql);
	}

	private function updatePageContent($iCampaignId,$aCampaignContentOptions){
		
		return \DAL::Update('campaign_promo_content',[
			'destination'=>$aCampaignContentOptions['campaignPageContentPromoPageDestination'],
            'destination_url'=>$aCampaignContentOptions['campaignPageContentDestinationUrl'],
            'page_text'=>$aCampaignContentOptions['campaignPageContentOnPageText'],
            'betslip_text'=>$aCampaignContentOptions['campaignPageContentBetSlipText'],
            'termsConditions'=>$aCampaignContentOptions['campaignPageContentTermsConditions'],
			],'fk_campaign_id='.(int)$iCampaignId);
	}

	private function clearPageContentOptions($iCampaignId){
		$sSql="DELETE FROM campaign_promo_content WHERE fk_campaign_id=".(int)$iCampaignId;
        \DAL::executeQuery($sSql);
	}
	

}