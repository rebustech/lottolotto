<?php

/**
 * Model/Repository Class for campaigns engine
 *
 * Class CampaignEntity
 */
class CampaignEntity {


    /**
     * The name of the DB table containing the campaign data
     * @var string
     */
    protected static $campaignsTable='campaigns';

    /**
     * The name of the DB table containing the campaign tag many to many relations
     * @var string
     */
    protected static $campaignTagsPivotTable='campaign_tag';


    /**
     * The name of the DB table containing the promotion types configurations
     * @var string
     */
    protected static $promotionTypeConfigTable='promotion_configs';

    /**
     * The name of the DB table containing the promotion codes
     * @var string
     */
    protected static $promotionCodesTable='campaign_promo_codes';

    /**
     * The name of the DB table containing the promotion types
     * @var string
     */
    protected static $promotionTypesTable='promotion_types';

    /**
     * @var string
     */
    protected static $promotionTypeSetupConfigColumn='setup_config';

    /**
     * @var string
     */
    protected static $promotionTypeOptionsConfigColumn='options_config';

    /**
     * @var string
     */
    protected static $promotionTypeRedemptionConfigColumn='redemption_config';

    /**
     * @var string
     */
    protected static $campaignRecipientsSegmentTable='campaign_promo_segments';

    /**
     * @var string
     */
    protected static $campaignRecipientsSegmentCampaignPivotTable='campaign_promo_segments_xref';

    protected static $campaignRecipientsSegmentConditionsColumn='segment_conditions';
    /**
     * @param $iCampaignId
     * @return array
     */
    public function getCampaignById($iCampaignId){

        $sSql='SELECT * FROM '.static::$campaignsTable.' WHERE id='.(int)$iCampaignId;

        return \DAL::executeGetRow($sSql);
    }
    /**
     * Fetch campaign configuration from DB
     * @param $iPromotionTypeId
     * @return mixed
     */
    public function getPromotionTypeConfigTemplateById($iPromotionTypeId){

        $sSql='SELECT config FROM '.static::$promotionTypesTable.' WHERE id='.(int)$iPromotionTypeId;

        return \DAL::executeGetOne($sSql);
    }

    public function getPromotionTypeConfigTemplateByCampaignId($iCampaignid,$decode=false){

        $sSql='SELECT t1.config
                FROM '.static::$promotionTypesTable.' t1
                JOIN '.static::$campaignsTable.' t2 ON t2.fk_promotion_type_id = t1.id AND t2.id='.(int)$iCampaignid;
        $sConfig=\DAL::executeGetOne($sSql);

        return $decode ? json_decode($sConfig) : $sConfig ;
    }

    /**
     * Check if campaign id is valid (exists in DB)
     * @param $iCampaignId
     * @return bool
     */
    public function validateCampaignId($iCampaignId){

        $bCampaignIdIsValid=false;

        if(is_numeric($iCampaignId)){

            $sSql="SELECT COUNT(*) AS counter FROM campaigns WHERE id=".(int)$iCampaignId;

            $iCounter=\DAL::executeGetOne($sSql);

            $bCampaignIdIsValid=$iCounter>0 ? true : $bCampaignIdIsValid;

        }


        return $bCampaignIdIsValid;
    }
    /**
     * Store promotion type setup config for campaign
     * @param $iCampaignId
     * @param array $aPromotionTypeSetupConfig
     */
    public function storePromotionTypeSetupConfig($iCampaignId,array $aPromotionTypeSetupConfig){

        $sPromotionTypeSetupConfig=json_encode($aPromotionTypeSetupConfig);

        if(self::checkIfConfigExists($iCampaignId)){


            \DAL::Update(static::$promotionTypeConfigTable,[static::$promotionTypeSetupConfigColumn=>$sPromotionTypeSetupConfig],'fk_campaign_id='.(int)$iCampaignId);
        }
        else{

           \DAL::Insert(static::$promotionTypeConfigTable,['fk_campaign_id'=>$iCampaignId,static::$promotionTypeSetupConfigColumn=>$sPromotionTypeSetupConfig]);
        }

    }

    /**
     * Reset all associated configurations of campaign
     * @param $iCampaignId
     */
    public function resetAllCampaignConfigurations($iCampaignId){

        $aData=['setup_config'=>null,'options_config'=>null,'redemption_config'=>null];

        \DAL::Update(static::$promotionTypeConfigTable,$aData,'fk_campaign_id='.(int)$iCampaignId);

    }
    /**
     * @param $iCampaignId
     * @param bool $decode
     * @return mixed
     */
    public function getPromotionTypeSetupConfigByCampaignId($iCampaignId,$decode=false){

        $sSql='SELECT '.static::$promotionTypeSetupConfigColumn.' FROM '.static::$promotionTypeConfigTable.' WHERE fk_campaign_id='.(int)$iCampaignId;

        $sConfig=\DAL::executeGetOne($sSql);

        return $decode ? json_decode($sConfig) : $sConfig ;
    }

    /**
     * Delete all tag relations associated with specific campaign
     * @param $iCampaignId
     */
    public function clearAllTagsByCampaignId($iCampaignId){

        $sSql='DELETE FROM '.static::$campaignTagsPivotTable.' WHERE fk_campaign_id='.(int)$iCampaignId ;

        \DAL::Query($sSql);
    }

    public function storeCampaignTags($iCampaignId,array $tags){
        foreach($tags AS $tag){
            $data=['fk_campaign_id'=>$iCampaignId,'fk_tag_id'=>$tag];
            \DAL::Insert(static::$campaignTagsPivotTable,$data);
        }
    }


    /**
     * Check if campaign has a configuration stored in DB
     * @param $iCampaignId
     * @internal param $iCampaignid
     * @return bool
     */
    public static function checkIfConfigExists($iCampaignId){

        $sSql='SELECT COUNT(*) AS counter FROM '.static::$promotionTypeConfigTable.' WHERE fk_campaign_id='.(int)$iCampaignId;

        $iCounter=\DAL::executeGetOne($sSql);
        return $iCounter>0 ? true : false ;
    }

    /**
     * Check if campaign has an options configuration stored in DB
     * @param $iCampaignId
     * @internal param $iCampaignid
     * @return bool
     */
    public static function checkIfOptionsConfigExists($iCampaignId){

        $sSql='SELECT '.static::$promotionTypeOptionsConfigColumn.' FROM '.static::$promotionTypeConfigTable.' WHERE fk_campaign_id='.(int)$iCampaignId;

        $iCounter=\DAL::executeGetOne($sSql);

        return $iCounter ? true : false ;
    }

    /*
    |--------------------------------------------------------------------------
    | Replicate
    |--------------------------------------------------------------------------
    */

    /**
     * Fetch all campaigns from DB
     * @return array
     */
    public function getAllCampaigns(){

        $sSql='SELECT id,name FROM campaigns';

        return \DAL::executeQuery($sSql);
    }

    /**
     * Search tag name for tag autocomplete
     * @param $sTagToSearch
     * @return string
     */
    public function searchTagName($sTagToSearch){

        if(!empty($sTagToSearch)){
            $aResults=new stdClass();
            $aResults->suggestions=[];
            $sTagToSearch=mysql_escape_string($sTagToSearch);
            $sSql='SELECT t1.id,t1.name FROM tags t1 JOIN '.static::$campaignTagsPivotTable.' t2 ON t2.fk_tag_id=t1.id WHERE t1.name LIKE "%'.$sTagToSearch.'%"';

            $aRawResults=\DAL::executeQuery($sSql);
            foreach($aRawResults AS $res){
                $x=new stdClass();
                $x->value=$res['name'];
                $x->data=$res['id'];

                array_push($aResults->suggestions,$x);
                unset($x);
            }
            return json_encode($aResults);
        }
    }

    public function searchAffiliateByNameOrId($mAffiliateHint){
        if(!empty($mAffiliateHint)){
            $aResults=new stdClass();
            $aResults->suggestions=[];

            $field=is_numeric($mAffiliateHint) ? 't2.fk_user_id' : 't2.company';
            $sWildCard=is_numeric($mAffiliateHint) ? '' : '%';
            $mAffiliateHint=mysql_escape_string($mAffiliateHint);
            $sSql='SELECT t2.fk_user_id as id,t2.company as name
                FROM admin_users t1
                JOIN affiliates t2 ON t2.fk_user_id=t1.user_id
                WHERE '.$field.' LIKE "'.$mAffiliateHint.$sWildCard.'"';

            $aRawResults=\DAL::executeQuery($sSql);
            foreach($aRawResults AS $res){
                $x=new stdClass();
                $x->value=$res['name'];
                $x->data=$res['id'];

                array_push($aResults->suggestions,$x);
                unset($x);
            }
            return json_encode($aResults);
        }
    }
    /**
     * @param $sTagToSearch
     * @return string
     */
    public function searchTaggedCampaigns($sTagToSearch){
        if(!empty($sTagToSearch)){
            $sTagToSearch=mysql_escape_string($sTagToSearch);
            $aResults=new stdClass();
            $aResults->campaigns=[];
            $sSql='SELECT t0.id,t0.name FROM campaigns t0 JOIN campaign_tag t1 ON t1.fk_campaign_id=t0.id WHERE t1.fk_tag_id='.(int)$sTagToSearch;
            $aRawResults=\DAL::executeQuery($sSql);
            foreach($aRawResults AS $res){
                $x=new stdClass();
                $x->name=$res['name'];
                $x->id=$res['id'];

                array_push($aResults->campaigns,$x);
                unset($x);
            }
            return json_encode($aResults);
        }
    }
    /**
     * @param $sPromoCode
     * @param $op
     * @return string
     */
    public function searchPromoCode($sPromoCode,$op){
        $sPromoCode=mysql_escape_string($sPromoCode);
        $sSql='SELECT COUNT(*) AS counter FROM '.static::$promotionCodesTable.' WHERE promo_code LIKE "'.$sPromoCode.''.$op.'"';

        $iCounter=\DAL::executeGetOne($sSql);
        return $iCounter >0 ? json_encode(['errors'=>true]) : json_encode(['false']);
    }

    /**
     * Replicate Campaign
     * @param $iCampaignToReplicate
     * @param $sCampaignName
     * @return array|bool
     */
    public function replicateCampaign($iCampaignToReplicate,$sCampaignName){

        $iNewCampaignId=static::_replicateCampaign($iCampaignToReplicate,$sCampaignName);

        if(is_int($iNewCampaignId)){
            static::_replicateCampaignTags($iCampaignToReplicate,$iNewCampaignId);
            static::_replicateCampaignPromotionConfigs($iCampaignToReplicate,$iNewCampaignId);

            return $iNewCampaignId;
        }

        return false;
    }

    /**
     * @param $iCampaignId
     * @param $sNewCampaignName
     * @return array
     * @todo This can be done with one query INSERT ... SELECT but no such method in DAL
     *
     *  $sSql='INSERT INTO '.static::$campaignsTable.' ';
        $sSql.=' (`name`, `fk_promotion_type_id`, `fk_occasion_id`, `occasion_valid_for`, `is_active`, `start_date`, `end_date`) ';
        $sSql.= ' SELECT  "'.mysql_real_escape_string($sNewCampaignName).'",`fk_promotion_type_id`, `fk_occasion_id`, `occasion_valid_for`, `is_active`, `start_date`, `end_date` ';
        $sSql.= ' FROM '.static::$campaignsTable.' WHERE id='.$iCampaignId;
     */
    private static function _replicateCampaign($iCampaignId,$sNewCampaignName){

        $sSql='SELECT * FROM '.static::$campaignsTable.' WHERE id='.(int)$iCampaignId;
        $aExistingCampaign=\DAL::executeGetRow($sSql);
        unset($aExistingCampaign['id']);
        $aExistingCampaign['name']=$sNewCampaignName;

        return \DAL::Insert(static::$campaignsTable,$aExistingCampaign,true);


    }

    /**
     * Replicate Campaign Tags
     * @param $iExistingCampaignId
     * @param $iNewCampaignId
     */
    private static function _replicateCampaignTags($iExistingCampaignId,$iNewCampaignId){

        $sSql='SELECT  fk_tag_id FROM '.static::$campaignTagsPivotTable.' WHERE fk_campaign_id='.(int)$iExistingCampaignId;
        $aExistingCampaignTags=\DAL::executeQuery($sSql);

        foreach($aExistingCampaignTags AS $tag){
            $tag['fk_campaign_id']=$iNewCampaignId;
            \DAL::Insert(static::$campaignTagsPivotTable,$tag);
        }

    }

    /**
     * Replicate Campaign Promotion Configs
     * @param $iExistingCampaignId
     * @param $iNewCampaignId
     * @return mixed
     */
    private static function _replicateCampaignPromotionConfigs($iExistingCampaignId,$iNewCampaignId){

        $sSql='SELECT setup_config, options_config, redemption_config FROM '.static::$promotionTypeConfigTable.' WHERE fk_campaign_id='.(int)$iExistingCampaignId;
        $aExistingCampaignConfigs=\DAL::executeGetRow($sSql);

        $aExistingCampaignConfigs['fk_campaign_id']=$iNewCampaignId;

        return \DAL::Insert(static::$promotionTypeConfigTable,$aExistingCampaignConfigs,true);
    }

    public function getPromoCodesByCampaignId($iCampaignId,array $columns=[]){
        $sFields=!empty($columns)?implode(',',$columns): '*';
        $sSql="SELECT $sFields FROM ".static::$promotionCodesTable." WHERE fk_campaign_id=".(int) $iCampaignId;

        $aPromoCodes=\DAL::executeQuery($sSql);

        return $aPromoCodes;
    }

    /**
     * @param $iCampaignId
     * @param array $aPageContentProperties
     * @return bool
     */
    public function storePageContentProperties($iCampaignId,array $aPageContentProperties){

        return true;

    }

    /**
     *Get all active campaigns with configurations
     */
    public function getAllActiveCampaigns(){

        $sSql="SELECT t3.id,t1.segment_conditions,t4.setup_config,t4.options_config FROM
                campaign_promo_segments t1
                JOIN campaign_promo_segments_xref t2 ON t2.fk_segment_id=t1.id
                JOIN campaigns t3 ON t3.id=t2.fk_campaign_id
                JOIN promotion_configs t4 ON t3.id=t4.fk_campaign_id
                WHERE t3.is_active=1 AND DATE(now()) BETWEEN DATE(t3.start_date) AND DATE(t3.end_date) ";

        return \DAL::executeQuery($sSql);

    }

    public function filterActiveCampaigns($aCampaignIds){

        foreach($aCampaignIds AS $iCampaignid){
            echo $iCampaignid;
        }
    }
} 