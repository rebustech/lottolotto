<?php

/**
 * Model/Repository Class for campaign options
 *
 * Class CampaignOptions
 */
class CampaignOptions extends CampaignEntity{

	//private static $promotionTypeConfigTable='promotion_configs';

	//private static $promotionTypeOptionsConfigColumn='options_config';

	// public $oGameEngine;
	

	protected static $campaignTitle='Setup your campaign';
	
	protected static $lotteries=[
		'lotteriesControlName'=>'products',
		'lotteriesControlDefault'=>0,
		'lotteriesControlLabel'=>'Lottery is',
		];

	protected static $jackpot=[
		'logicalOperatorName'=>'jackpot_operator',
		'logicalOperatorDefault'=>0,
		'logicalOperatorLabel'=>'Jackpot is',
		'textFieldName'=>'jackpot_amount',
		'textFieldDefault'=>0,
		'textFieldLabel'=>'Jackpot amount',
		'currencyName'=>'jackpot_currency',
		'currencyDefault'=>0,
		'currencyLabel'=>'Jackpot currency'
		];

	protected static $ticketsBought=[
		'logicalOperatorName'=>'tickets_operator',
		'logicalOperatorDefault'=>0,
		'logicalOperatorLabel'=>'Number of tickets bought is',
		'textFieldName'=>'tickets_amount',
		'textFieldDefault'=>0,
		'textFieldLabel'=>'Number of tickets bought'
		];

	protected static $orderPayin=[
		'logicalOperatorName'=>'orderpayinvalue_operator',
		'logicalOperatorDefault'=>0,
		'logicalOperatorLabel'=>'Order value/pay-in amount is',
		'textFieldName'=>'orderpayinvalue_amount',
		'textFieldDefault'=>0,
		'textFieldLabel'=>'Order value/pay-in amount',
		'currencyName'=>'orderpayinvalue_currency',
		'currencyDefault'=>1,
		'currencyLabel'=>'Order value/pay-in currency'
		];

	protected static $payinMethod=[
		'isnotIsOperatorName'=>'payinvalue_operator',
		'isnotIsOperatorDefault'=>0,
		'isnotIsOperatorLabel'=>'Pay-in method is',
		'payinMethodsName'=>'payin_methods',
		'payinMethodsDefault'=>1,
		'payinMethodsLabel'=>'Pay-in method currency'
		];
	
	protected static $subscription=[
		'logicalOperatorName'=>'subscriptionweeks_operator',
		'logicalOperatorDefault'=>0,
		'logicalOperatorLabel'=>'Subscription in weeks is',
		'textFieldName'=>'subscriptionweeks_number',
		'textFieldDefault'=>0,
		'textFieldLabel'=>'Subscription in weeks value'
		];
	
	protected static $campaignConditions=[
		'conditionControlName'=>'select_condition1',
		'conditionControlDefault'=>'select',
		'conditionControlLabel'=>'Add'
		];

	protected static $andOrConditions=[
		'operatorName'=>'Operator',
		'operatorDefault'=>'AND'
		];

    public $mappings;


    /**
     * Create default json options template for the new campaign
     * @param $iCampaignId
     * @param bool $json
     * @return array|string
     */
    public function getNewCampaignOptions($iCampaignId,$json=false){

		$iPromoId=self::getPromotionTypeId($iCampaignId);

		$aTemplate=array();

		if(isset($_GET['methodname'])) {
			$aTemplate=self::filterCampaignOptionsByPromoType($iPromoId);
		}
		else{
			$aTemplate['fields'][]=self::filterCampaignOptionsByPromoType($iPromoId);
		}


		return ($json) ? json_encode($aTemplate) : $aTemplate;
	}


    /**
     * @param $iCampaignId
     * @return mixed
     */
    public function getPromotionTypeId($iCampaignId){
		
		$sSql="SELECT fk_promotion_type_id FROM campaigns WHERE id=".(int)$iCampaignId;
		$iPromoId=\DAL::executeGetOne($sSql);

		return $iPromoId;
	}

    /**
     *
     * @param $iPromoId
     * @return array
     */
    public function filterCampaignOptionsByPromoType($iPromoId){

		$aFields=self::conditionControl();

		// Remove Condition No of tickets bought
		switch ($iPromoId) {
			case 2:
			case 3:
				unset($aFields['options'][3]);
				break;
			case 4:
				unset($aFields['options'][3]);
				unset($aFields['options'][4]);
				break;
			default:
				return $aFields;
				break;
		}

		$aFields['options']=array_values($aFields['options']);

		return $aFields;
	}

    /**
     * @return array
     */
    public function conditionControl(){


		$aOptions=array(
			array('name'=>'Condition','value'=>'select'),
			array('name'=>'Lottery','value'=>'lottery','method'=>'getProductValues'),
			array('name'=>'Jackpot','value'=>'jackpot','method'=>'jackpotControlGroup'),
			array('name'=>'No of tickets bought','value'=>'tickets_bought','method'=>'ticketsBoughtControlGroup'),
			array('name'=>'Order/Pay-in value','value'=>'order_payin_value','method'=>'orderPayinControlGroup'),
			array('name'=>'Pay-in methods','value'=>'payin_methods','method'=>'payinControlGroup'),
			array('name'=>'Subscription in weeks','value'=>'subscription','method'=>'subscriptionWeeksControlGroup')
			);
		$aConditionValues=self::makeSelectControl(
			self::$campaignConditions['conditionControlName'],
			$aOptions,
			self::$campaignConditions['conditionControlDefault'],
			self::$campaignConditions['conditionControlLabel']
			);

		return $aConditionValues;
	}


    /**
     * @return array
     */
    public function subscriptionWeeksControlGroup(){

		$sOperatorOptions=array();

		$sOperatorOptions[]=self::getLogicalOperators(
			self::$subscription['logicalOperatorName'],
			self::$subscription['logicalOperatorDefault'],
			self::$subscription['logicalOperatorLabel']
			);

		$sOperatorOptions[]=self::getValueText(
			self::$subscription['textFieldName'],
			self::$subscription['textFieldDefault'],
			self::$subscription['textFieldLabel']
			);

		// if the request is made from the ajax controler return the options as a [fields] array
		return (isset($_GET['methodname'])) ? ['fields'=>$sOperatorOptions] : $sOperatorOptions;
	}


    /**
     * @return array
     */
    public function payinControlGroup(){

		$sOperatorOptions=array();

		$sOperatorOptions[]=self::getIsnotIsOperatorValues(
			self::$payinMethod['isnotIsOperatorName'],
			self::$payinMethod['isnotIsOperatorDefault'],
			self::$payinMethod['isnotIsOperatorLabel']
			);

		$sOperatorOptions[]=self::getPaymentMethodsValues(
			self::$payinMethod['payinMethodsName'],
			self::$payinMethod['payinMethodsDefault'],
			self::$payinMethod['payinMethodsLabel']
			);

		// if the request is made from the ajax controler return the options as a [fields] array
		return (isset($_GET['methodname'])) ? ['fields'=>$sOperatorOptions] : $sOperatorOptions;
	}


    /**
     * @return array
     */
    public function orderPayinControlGroup() {

		$sOperatorOptions=array();

		$sOperatorOptions[]=self::getLogicalOperators(
			self::$orderPayin['logicalOperatorName'],
			self::$orderPayin['logicalOperatorDefault'],
			self::$orderPayin['logicalOperatorLabel']
			);

		$sOperatorOptions[]=self::getValueText(
			self::$orderPayin['textFieldName'],
			self::$orderPayin['textFieldDefault'],
			self::$orderPayin['textFieldLabel']
			);

		$sOperatorOptions[]=self::getCurrencyValues(
			self::$orderPayin['currencyName'],
			self::$orderPayin['currencyDefault'],
			self::$orderPayin['currencyLabel']
			);

		// if the request is made from the ajax controler return the options as a [fields] array
		return (isset($_GET['methodname'])) ? ['fields'=>$sOperatorOptions] : $sOperatorOptions;
	}


    /**
     * @return array
     */
    public function ticketsBoughtControlGroup(){

		$sOperatorOptions=array();

		$sOperatorOptions[]=self::getLogicalOperators(
			self::$ticketsBought['logicalOperatorName'],
			self::$ticketsBought['logicalOperatorDefault'],
			self::$ticketsBought['logicalOperatorLabel']
			);

		$sOperatorOptions[]=self::getValueText(
			self::$ticketsBought['textFieldName'],
			self::$ticketsBought['textFieldDefault'],
			self::$ticketsBought['textFieldLabel']
			);

		// if the request is made from the ajax controler return the options as a [fields] array
		return (isset($_GET['methodname'])) ? ['fields'=>$sOperatorOptions] : $sOperatorOptions;
	}

    /**
     * @return array
     */
    public function jackpotControlGroup(){

		$sOperatorOptions=array();

		$sOperatorOptions[]=self::getLogicalOperators(
			self::$jackpot['logicalOperatorName'],
			self::$jackpot['logicalOperatorDefault'],
			self::$jackpot['logicalOperatorLabel']
			);
		
		$sOperatorOptions[]=self::getValueText(
			self::$jackpot['textFieldName'],
			self::$jackpot['textFieldDefault'],
			self::$jackpot['textFieldLabel']
			);

		$sOperatorOptions[]=self::getCurrencyValues(
			self::$jackpot['currencyName'],
			self::$jackpot['currencyDefault'],
			self::$jackpot['currencyLabel']
			);

		// if the request is made from the ajax controler return the options as a [fields] array
		return (isset($_GET['methodname'])) ? ['fields'=>$sOperatorOptions] : $sOperatorOptions;
	}


    /**
     * @param $sName
     * @param $sDefault
     * @param $sLabel
     * @return array
     */
    public function getLogicalOperators($sName,$sDefault,$sLabel){

		$aOptions=array(
            array('name'=>'equal to','value'=>'='),
			array('name'=>'more than','value'=>'>'),
			array('name'=>'less than','value'=>'<')

			);
		$aOperatorValues=self::makeSelectControl($sName,$aOptions,$sDefault,$sLabel);

		return $aOperatorValues;
	}

    /**
     * @param $sName
     * @param $sDefault
     * @param $sLabel
     * @return array
     */
    public function getValueText($sName,$sDefault,$sLabel){

		$aFieldOptions=array('type'=>'text','name'=>$sName,'default'=>$sDefault,'label'=>$sLabel);
	
		return $aFieldOptions;
	}


    /**
     * @param $sName
     * @param $sDefault
     * @param $sLabel
     * @return array
     *
     */
    public function getAndOrOperatorValues($sName,$sDefault,$sLabel){

		$aOptions=array(
			array('name'=>'AND','value'=>'AND'),
			array('name'=>'OR','value'=>'OR'),
			);
		$aAndOrValues=self::makeSelectControl($sName,$aOptions,$sDefault,$sLabel);

		return $aAndOrValues;
	}


    /**
     * @param $sName
     * @param $sDefault
     * @param $sLabel
     * @return array
     */
    public function getIsnotIsOperatorValues($sName,$sDefault,$sLabel){

		$aOptions=array(
			array('name'=>'is','value'=>'='),
			array('name'=>'is not','value'=>'!='),
			);
		$aIsnotIsValues=self::makeSelectControl($sName,$aOptions,$sDefault,$sLabel);

		return $aIsnotIsValues;
	}

    /**
     * assign the default values to the new template
     * @return array
     */
    public function getProductValues(){

		// $sSql="SELECT lottery_id, comment FROM lotteries_cmn";

		// $sLotteriesValues=\DAL::executeQuery($sSql);

		// $aOptions=array();

		// foreach ($sLotteriesValues as $value) {
		// 	$aOptions[]=array('name'=>$value['comment'],'value'=>$value['lottery_id']);
		// }

		$sSql="SELECT id, name FROM game_engines";

		$sLotteriesValues=\DAL::executeQuery($sSql);

		$aOptions=array();

		foreach ($sLotteriesValues as $value) {
			$aOptions[]=array('name'=>$value['name'],'value'=>$value['id']);
		}
		
		$aProductdsValues=self::makeSelectControl(
			self::$lotteries['lotteriesControlName'],
			$aOptions,
			self::$lotteries['lotteriesControlDefault'],
			self::$lotteries['lotteriesControlLabel']
			);

		// if the request is made from the ajax controler return the options as a [fields] array
		
		return (isset($_GET['methodname'])) ? ['fields'=>[$aProductdsValues]] : $aProductdsValues;
	}


    /**
     * @param $sName
     * @param $sDefault
     * @param $sLabel
     * @return array
     */
    public function getCurrencyValues($sName,$sDefault,$sLabel){
		
		$sSql="SELECT currency_id, fullname FROM currencies";

		$sCurrenciesValues=\DAL::executeQuery($sSql);

		$aOptions=array();
        $aOptions[]=array('name'=>'Currency','value'=>null);
		foreach ($sCurrenciesValues as $value) {
			$aOptions[]=array('name'=>$value['fullname'],'value'=>$value['currency_id']);
		}
		
		$aCurrenciesValuesTemplate=self::makeSelectControl($sName,$aOptions,$sDefault,$sLabel);

		return $aCurrenciesValuesTemplate;
	}


    /**
     * @param $sName
     * @param $sDefault
     * @param $sLabel
     * @return array
     */
    public function getPaymentMethodsValues($sName,$sDefault,$sLabel){
		
		$sSql="SELECT gateway_id, comment FROM gateways";

		$sGatewaysValues=\DAL::executeQuery($sSql);

		$aOptions=array();
        $aOptions[]=array('name'=>'Pay In method','value'=>null);
		foreach ($sGatewaysValues as $value) {
			$aOptions[]=array('name'=>$value['comment'],'value'=>$value['gateway_id']);
		}
		
		$aGatewaysValuesTemplate=self::makeSelectControl($sName,$aOptions,$sDefault,$sLabel);

		return $aGatewaysValuesTemplate;
	}

    /**
     * Fetch campaign configuration from DB
     * @param $iCampaignId
     * @param bool $decode
     * @internal param $iPromotionTypeId
     * @return mixed
     */
	public function getCampaignOptionsConfigValues($iCampaignId,$decode=false){

		$sSql="SELECT ". self::$promotionTypeOptionsConfigColumn."
		FROM ". self::$promotionTypeConfigTable."
		WHERE fk_campaign_id=".(int)$iCampaignId;

		$sConfig=\DAL::executeGetOne($sSql);

		$sFormatedConfig=self::formatStoredOptions($sConfig,$iCampaignId);

		return $decode ? json_decode($sConfig) : $sFormatedConfig ;
		
		//return $decode ? json_decode($sConfig) : $sConfig ;
	}

	public function filterCampaignStoredOptionsByPromoType($iPromoId,$aDefaultOptions){

		switch ($iPromoId) {
			case 2:
			case 3:
				unset($aDefaultOptions['ticketsBought']);
				break;
			case 4:
				unset($aDefaultOptions['ticketsBought']);
				unset($aDefaultOptions['orderPayin']);
				break;
			default:
				return $aDefaultOptions;
				break;
		}

		return $aDefaultOptions;
	}

    /**
     * @param string $sConfig
     * @param $iCampaignId
     * @return string
     */
    public function formatStoredOptions($sConfig='',$iCampaignId){
		// get the config values into an array
		$aStoredOptions=json_decode($sConfig);

		// create an array with the default controls values
		$aDefaultOptions=array(
			'products'=>self::$lotteries,
			'jackpot'=>self::$jackpot,
			'ticketsBought'=>self::$ticketsBought,
			'orderPayin'=>self::$orderPayin,
			'payinMethod'=>self::$payinMethod,
			'subscription'=>self::$subscription,
			'andor'=>self::$andOrConditions,
			);

		// get promotion id
		$iPromoId=self::getPromotionTypeId($iCampaignId);

		// remove options depending on selected promotion type
		$aDefaultOptions=self::filterCampaignStoredOptionsByPromoType($iPromoId,$aDefaultOptions);
		
		//print "<pre>"; print_r($aDefaultOptions); print "</pre>";

		$aGroupArray=array();

		//print "<pre>"; print_r($aStoredOptions); print "</pre>";

		// search in the default controls values array for elements with the same 
		// key values as the stored options keys
		foreach ($aStoredOptions->fields as $aGroupsKeys => $aGroupsValues) {

			$aFieldsArray=array();
			$newOptions[$aGroupsKeys]=array();

			foreach ($aGroupsValues as $sGroupkey => $sGroupvalue) {

				// remove numeric occurencies from the keys
				$sCleanKey=preg_replace("/condition\d+_/i", "", $sGroupkey);
				preg_match("/condition\d/i", $sGroupkey, $sCleanConditionKey);

				$current_key=CampaignHelpers::recursive_array_search($sCleanKey,$aDefaultOptions,true);
				if($current_key){

					$defaultsKey=CampaignHelpers::recursive_array_search($sCleanKey,$aDefaultOptions[$current_key],true);

					$sDefaultValue=preg_replace("/Name/i", "Default",$defaultsKey);

					$newOptions[$aGroupsKeys][$sCleanConditionKey[0]][$current_key][$sDefaultValue]=$sGroupvalue;


					//print_r($sGroupkey);
					// append condition and/or operator
					// if($aGroupsValues->{$sCleanConditionKey[0].'_Operator'})
					// 	$newOptions[$aGroupsKeys][$sCleanConditionKey[0].'_Operator']=$aGroupsValues->{$sCleanConditionKey[0].'_Operator'};	

				}
			}


			//Add and/or operators between groups
			$sGroupOperatorDefault=$aGroupsKeys.'Operator';
			if($aGroupsValues->$sGroupOperatorDefault){
				$newOptions[$aGroupsKeys][$aGroupsKeys.'_OperatorDefault']=$aGroupsValues->$sGroupOperatorDefault;
			}

		}

		//check newoptions for empty conditions
		$newNewOptions=array();
		foreach($newOptions as $groupConditionsKey=>$groupConditionsValue){

			$i=1;
			foreach ($groupConditionsValue as $conditionKey=>$conditionValue) {

				if($conditionKey!=$groupConditionsKey.'_OperatorDefault'){

					//count the array elements and remove orphan end/or operators
					if( count($conditionValue)==1 && isset($conditionValue['andor']) ){
						unset($newOptions[$groupConditionsKey][$conditionKey]);
					}
					else{
						$newNewOptions[$groupConditionsKey]['Condition'.$i]=$newOptions[$groupConditionsKey][$conditionKey];
						$i++;
					}
				}
				// add the group condition operator
				else{
					$newNewOptions[$groupConditionsKey][$conditionKey]=$newOptions[$groupConditionsKey][$conditionKey];
				}			
			}	
		}
		
		$newOptions=$newNewOptions;

		//print "<pre>"; print_r($newNewOptions); print "</pre>";

		//print "<pre>"; print_r($newOptions); print "</pre>";


		$sOut=array();

		$n=1;
		foreach($newOptions as $groupsKey=>$groupsValue){

			// Add condition selector for each group
			self::$campaignConditions['conditionControlName']='select_condition'.$n;
			//$sOut[$groupsKey]['select_condition'][]=self::conditionControl();
			$sOut[$groupsKey]['select_condition'][]=self::getNewCampaignOptions($iCampaignId,false);

			foreach($groupsValue as $conditionKey=>$conditionValue){

				foreach ($conditionValue as $optionsKey => $optionsValue) {

					//print "<pre>"; print_r($optionsKey); print "</pre>";

					switch ($optionsKey) {
						case 'select_condition':
							$sOut[$groupsKey][$conditionKey][]=self::getProductValues();
							break;
						case 'products':
							self::$lotteries['lotteriesControlName']=$groupsKey.'_'.$conditionKey.'_'.$optionsKey;
							self::$lotteries['lotteriesControlDefault']=$optionsValue['lotteriesControlDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::getProductValues();
							break;
						case 'jackpot':
							// reset condition names
							self::$jackpot['logicalOperatorName']='jackpot_operator';
							self::$jackpot['textFieldName']='jackpot_amount';
							self::$jackpot['currencyName']='jackpot_currency';

							self::$jackpot['logicalOperatorName']=$groupsKey.'_'.$conditionKey.'_'.self::$jackpot['logicalOperatorName'];
							self::$jackpot['logicalOperatorDefault']=$optionsValue['logicalOperatorDefault'];
							
							self::$jackpot['textFieldName']=$groupsKey.'_'.$conditionKey.'_'.self::$jackpot['textFieldName'];
							self::$jackpot['textFieldDefault']=$optionsValue['textFieldDefault'];

							self::$jackpot['currencyName']=$groupsKey.'_'.$conditionKey.'_'.self::$jackpot['currencyName'];
							self::$jackpot['currencyDefault']=$optionsValue['currencyDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::jackpotControlGroup();
							break;
						case 'ticketsBought':
							// reset condition names
							self::$ticketsBought['logicalOperatorName']='tickets_operator';
							self::$ticketsBought['textFieldName']='tickets_amount';

							self::$ticketsBought['logicalOperatorName']=$groupsKey.'_'.$conditionKey.'_'.self::$ticketsBought['logicalOperatorName'];
							self::$ticketsBought['logicalOperatorDefault']=$optionsValue['logicalOperatorDefault'];
							
							self::$ticketsBought['textFieldName']=$groupsKey.'_'.$conditionKey.'_'.self::$ticketsBought['textFieldName'];
							self::$ticketsBought['textFieldDefault']=$optionsValue['textFieldDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::ticketsBoughtControlGroup();
							break;
						case 'orderPayin':
							// reset condition names
							self::$orderPayin['logicalOperatorName']='orderpayinvalue_operator';
							self::$orderPayin['textFieldName']='orderpayinvalue_amount';
							self::$orderPayin['currencyName']='orderpayinvalue_currency';

							self::$orderPayin['logicalOperatorName']=$groupsKey.'_'.$conditionKey.'_'.self::$orderPayin['logicalOperatorName'];
							self::$orderPayin['logicalOperatorDefault']=$optionsValue['logicalOperatorDefault'];
							
							self::$orderPayin['textFieldName']=$groupsKey.'_'.$conditionKey.'_'.self::$orderPayin['textFieldName'];
							self::$orderPayin['textFieldDefault']=$optionsValue['textFieldDefault'];
							
							self::$orderPayin['currencyName']=$groupsKey.'_'.$conditionKey.'_'.self::$orderPayin['currencyName'];
							self::$orderPayin['currencyDefault']=$optionsValue['currencyDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::orderPayinControlGroup();
							break;
						case 'payinMethod':
							// reset condition names
							self::$payinMethod['isnotIsOperatorName']='payinvalue_operator';
							self::$payinMethod['payinMethodsName']='payin_methods';

							self::$payinMethod['isnotIsOperatorName']=$groupsKey.'_'.$conditionKey.'_'.self::$payinMethod['isnotIsOperatorName'];
							self::$payinMethod['isnotIsOperatorDefault']=$optionsValue['isnotIsOperatorDefault'];
							
							self::$payinMethod['payinMethodsName']=$groupsKey.'_'.$conditionKey.'_'.self::$payinMethod['payinMethodsName'];
							self::$payinMethod['payinMethodsDefault']=$optionsValue['payinMethodsDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::payinControlGroup();
							break;
						case 'subscription':
							// reset condition names
							self::$subscription['logicalOperatorName']='subscriptionweeks_operator';
							self::$subscription['textFieldName']='subscriptionweeks_number';

							self::$subscription['logicalOperatorName']=$groupsKey.'_'.$conditionKey.'_'.self::$subscription['logicalOperatorName'];
							self::$subscription['logicalOperatorDefault']=$optionsValue['logicalOperatorDefault'];
							
							self::$subscription['textFieldName']=$groupsKey.'_'.$conditionKey.'_'.self::$subscription['textFieldName'];
							self::$subscription['textFieldDefault']=$optionsValue['textFieldDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::subscriptionWeeksControlGroup();
							break;
						case 'andor':
							self::$andOrConditions['operatorName']=$groupsKey.'_'.$conditionKey.'_Operator';						
							self::$andOrConditions['operatorDefault']=$optionsValue['operatorDefault'];
							$sOut[$groupsKey][$conditionKey][]=self::getAndOrOperatorValues(
								self::$andOrConditions['operatorName'],
								self::$andOrConditions['operatorDefault'],
								$sLabel='AND-OR');
							break;
						
						default:
							# code...
							break;
					}

				// 	//$sConditionsOperatorName=$groupsKey.'_'.$conditionKey.'_Operator';
				// 	$sConditionsOperatorName=$conditionKey.'_Operator';
				// 	$sConditionOperatorDefault=$groupsValue[$conditionKey.'_Operator'];

				// 	//print_r($sConditionsOperatorName);

				// 	if($sConditionOperatorDefault){
				// 		$sOut[$groupsKey][$groupsKey.'_'.$conditionKey.'_Operator'][]
				// 		=self::getAndOrOperatorValues(
				// 			$sConditionsOperatorName,
				// 			$sConditionOperatorDefault,
				// 			$sLabel='AND-OR');
				// 	}

				// 	// print "<pre>";
				// 	// print_r($sOut[$groupsKey][$groupsKey.'_'.$conditionKey.'_Operator']);
				// 	// print "</pre>";
				}


			}

			//print "<pre>"; print_r($groupsValue[$groupsKey.'_OperatorDefault']); print "</pre>";
			$sGroupOperatorName=$groupsKey.'Operator';
			$sGroupOperatorDefault=$groupsValue[$groupsKey.'_OperatorDefault'];

			if($sGroupOperatorDefault){
				$sOut[$groupsKey]['group_operator'][]=self::getAndOrOperatorValues($sGroupOperatorName,$sGroupOperatorDefault,'Group Operator');
			}
			$n++;
		}

		//print "<pre>"; print_r($sOut); print "</pre>";

		//return json_encode(['fields'=>[$sOut]]);
		return json_encode($sOut);
	}

	public function storeCampaignOptions($iCampaignId,$aCampaignOptions){


		// Split the options array into groups
		$aOptionsTemplate=array();
		$aGroupOptions=array();
		
		foreach ($aCampaignOptions as $key => $value) {
			// create group key
			preg_match("/group\d+/", $key, $groupKey);

			// remove groupx_ from original array keys
			$sCleanKey=preg_replace("/group\d+_/", "", $key);

			$aGroupOptions[$groupKey[0]][$sCleanKey]=$value;
		}

		if(CampaignHelpers::issetNotEmptyNotNull($aGroupOptions)){

			$aOptionsTemplate['fields']=$aGroupOptions;
			$sOptionsSetupConfig=json_encode($aOptionsTemplate);

			//print "<pre>"; print_r($aOptionsTemplate);


	        if(campaignEntity::checkIfConfigExists($iCampaignId)){

	            \DAL::Update(self::$promotionTypeConfigTable,[self::$promotionTypeOptionsConfigColumn=>$sOptionsSetupConfig],'fk_campaign_id='.(int)$iCampaignId);
	        }
	        else{

	           \DAL::Insert(self::$promotionTypeConfigTable,['fk_campaign_id'=>$iCampaignId,self::$promotionTypeOptionsConfigColumn=>$sOptionsSetupConfig]);
	        }
    	}
		

		return null;
	}


    /**
     * @param $iMemberId
     * @param array $aActiveCampaigns
     * @param array $aCartContents
     * @return bool
     */
    public function getCampaignsFromConditions($iMemberId,array $aActiveCampaigns,array $aCartContents){

        $this->mappings=new CampaignFormDbMapping();
        $isValid=[];

        foreach($aActiveCampaigns as $aCampaign){

            $iCampaignId=$aCampaign['id'];

            $aCampaignOptions=json_decode($aCampaign['options_config'],true);

        	$isValid[]=$this->parseConditions($iMemberId,$aCampaignOptions,$aCartContents,$iCampaignId);       	
        }

        return array_filter($isValid);
    }


    /**
     * @param $iMemberId
     * @param $oConditions
     * @return bool
     */
    protected function parseConditions($iMemberId,$oConditions,$aCartContents,$iCampaignId){

    	foreach ($oConditions['fields'] as $k => $oCondition) {

            $r = static::formatConditions($oCondition);
           
            foreach ($r as $key => $value) {
            	$sMethod = preg_replace('/[0-9]/', '', $key);
            	$sMethod = CampaignHelpers::formatStringForMethodName($sMethod);
	        	$sMethodName = "get{$sMethod}";

		        if (method_exists($this->mappings, $sMethodName)) {
		        	if ($sMethodName=='getTickets' || $sMethodName=='getOrderpayinvalue' 
		        		|| $sMethodName=='getProducts' || $sMethodName=='getJackpot'){
		            	$promoCandidate = $this->mappings->$sMethodName($iMemberId,$value,$aCartContents,'bool');
		            }
		            else
		            	$promoCandidate = $this->mappings->$sMethodName($iMemberId,$value,'bool');

		            if($promoCandidate==1)
		            	$aGroupRs[$k][]='true=='.$promoCandidate;
		            elseif(!$promoCandidate) 
		            	$aGroupRs[$k][]='true==0';            
		            elseif($sMethodName!='getGroupOperator')
		            	$aGroupRs[$k][]=$promoCandidate;
		            elseif($sMethodName=='getGroupOperator')
		            	$aGroupOpers[$k]=$promoCandidate;
		        }
            }
    	}

    	$aOptionsRs=self::getOptionsResults($aGroupRs,$aGroupOpers,$iCampaignId);

    	return $aOptionsRs;
    }


    private static function getOptionsResults($aGroupRs,$aGroupOpers,$iCampaignId){

    	foreach ($aGroupRs as $key => $value){

    		$sCondOper=implode(' ', $value);
			if( eval("return {$sCondOper};") ) $groupRs='true==1';
			else $groupRs='true==0';
			$grpRs[]=['result'=>$groupRs,'oper'=>$aGroupOpers[$key]];
    	}

    	$grpOps=iterator_to_array(new RecursiveIteratorIterator(
			new RecursiveArrayIterator($grpRs)), false);

    	$sGroupOper=implode(' ', $grpOps);

    	if( eval("return {$sGroupOper};") )
    		return $iCampaignId;
		
		return false;
    }

    
    private static function formatConditions($oCondition){

        $conditions=[];
        $n=0;
        foreach($oCondition as $k=>$v){

        	// match group operator
        	preg_match("/group[0-9]operator/iu",$k,$matches);
        	if(!empty($matches)){
        	 	$sKeyName='groupOperator';
        	}

        	// match conditions
			preg_match("/_(.*?)_/", $k, $matches);
			if(!empty($matches)){
				$sKeyName=$matches[1];
			}

			// match between conditions operators
			preg_match("/condition[0-9]\_operator/iu", $k, $matches);
			if(!empty($matches)){
				$sKeyName='operator'.$n;
				$n++;
			}

			// match exotic conditions
			if(empty($sKeyName)){
				preg_match("/_(.*)/", $k, $matches);
				$sKeyName=($matches[1]=='products') ? $matches[1] : $matches[1].$n;
			}

			if($sKeyName=='payin') $sKeyName='payinvalue';

            if(preg_match('/_operator/iu',$k)){
                $conditions[$sKeyName]['switch']=$v;
            }
            elseif(preg_match('/amount|number|products|methods|currency|group[0-9]operator/iu',$k)){
                if(null!=$conditions[$sKeyName]['value']){
                    $additionalKey=preg_match('/\_currency/iu',$k) ? 'currency' : preg_replace('/\_control/iu','',$k);
                    $conditions[$sKeyName]['additional'][$additionalKey]=$v;
                }
                else{
                    $conditions[$sKeyName]['value']=$v;
                }
            }
        }

        return $conditions;
    }


	/* Helper Functions*/

    /**
     * @param $sName
     * @param $aOptions
     * @param $sDefault
     * @param $sLabel
     * @return array
     * @todo move to helpers
     */
	public function makeSelectControl($sName,$aOptions,$sDefault,$sLabel){
		
		$aControl=array(
			'type'=>'select',
			'name'=>$sName,
			'label'=>$sLabel,
			'default'=>$sDefault,
			'options'=>$aOptions
			);

		return $aControl;
	}
}