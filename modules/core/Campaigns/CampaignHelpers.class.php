<?php

/**
 * Class CampaignHelpers
 * @todo Proper PHP Doc Commenting
 */
class CampaignHelpers {


    protected static $validator;

    public function __construct(){

        static::$validator=new CampaignRecipientsValidator([]);
    }

    /**
     * @param $sString
     * @return bool
     */
    public  function issetNotEmptyNotNull($sString){

        return is_array($sString) ? !empty($sString) : isset($sString) && !empty($sString) && !is_null($sString);

    }

    /**
     * @return bool
     */
    public function extractMethodName(){

        return self::issetNotEmptyNotNull($_GET['methodname']) ? $_GET['methodname'] : false ;

    }

    /**
     * @return bool|int
     */
    public function extractCampaignId(){
        if(self::issetNotEmptyNotNull($_POST['campaign_id'])){

            return (int)$_POST['campaign_id'];
        }
        return self::issetNotEmptyNotNull($_GET['id']) ? (int)$_GET['id'] : false ;
    }

    /**
     * @param array $array
     * @return array
     * @author kevin Waterson
     * http://www.phpro.org/examples/Flatten-Array.html
     */
    public static function flattenArray(array $array){
        $ret_array = array();
        foreach(new RecursiveIteratorIterator(new RecursiveArrayIterator($array)) as $value)
        {
            $ret_array[] = $value;
        }
        return $ret_array;
    }

    /**
     * @param $array
     * @return null|string
     * @author Alain Tiemblo [ http://stackoverflow.com/a/13474770 ]
     */
    public function arrayToCsv($array){
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    /**
     * @param $sData
     * @return array
     */
    public static function csvXlsDataParser($sData){
        if(is_array($sData)){
            $x=$sData;
        }
        else{
            $data = preg_split("/[\r\n]+/",$sData, -1, PREG_SPLIT_NO_EMPTY);
            foreach($data as $d){
                $x[]=preg_split('/\t/', $d);
            }
        }


        // add xls header column as key value
        $aXall=[];
        $i=0;
        foreach ($x as $xkey => $xvalue) {
            
            if($i==0){
                $aKey=$xvalue;
            }

            $aXall[]=array_combine($aKey,$xvalue);

            $i++;
        }
        
        // remove the xls header column from the array
        unset($aXall[0]);

        $aUsers=array_filter( array_map([self,'createUserArray'], $aXall) );
     
        return $aUsers;
    }

    /**
     * @return mixed
     */
    public static function getTableFieldName(){
        $aRawFieldNames=\DAL::executeQuery("SHOW COLUMNS FROM members");

        $aFieldNames=array_map('self::proccessFieldNames', $aRawFieldNames);
        
        return call_user_func_array('array_merge',$aFieldNames);
    }

    /**
     * @param $items
     * @return array
     */
    public static function proccessFieldNames($items){
        $aFields=[];
        $aFieldsKeys=[];
        $aFieldsValues=[];

        foreach ($items as $key => $value) {
            if($key == 'Field'){
                //$sNewValue=preg_replace("/_/"," ",$value);
                $aFieldsKeys[]=$value;
            }
            else if($key == 'Type'){
                $aValue=explode('(', $value);
                $aFieldsValues[]=$aValue[0];
            }
        }
        $aFields=array_combine($aFieldsKeys,$aFieldsValues);

        return $aFields;
    }

    /**
     * @param $items
     * @return array
     */
    public static  function createUserArray($items){
        $aUserDetails=[];
        $aValidUserDetails=[];

        // get table field names
        $aFields=self::getTableFieldName();
        
        // construct user array with valid table fields
        $aUserDetails=array_intersect_key($items,$aFields);

        // validate xls data
        $aValidUserDetails=self::validateUserFields($aUserDetails,$aFields);
        
        return $aValidUserDetails;
    }

    /**
     * @param $usersData
     * @param $fieldsData
     * @return array
     * @todo must use the validator
     */
    public static function validateUserFields($usersData,$fieldsData){
        $user=[];

        foreach ($usersData as $key => $value) {
            if(array_key_exists($key,$fieldsData)){
                $fieldType=$fieldsData[$key];

                if($value){ 
                    switch ($fieldType) {
                        case 'varchar':
                            if($key=='email') $user[$key]=self::chkIsEmail($value);
                            else $user[$key]=$value;
                            break;
                        case 'int':
                        case 'tinyint':
                            $user[$key]=self::chkIsInt($value);
                            break;
                        case 'float':
                            $user[$key]=self::chkIsFloat($value);
                            break;
                        default:
                            $user[$key]=$value;
                            break;
                    } 
                }               
            }
        }

        return $user;
    }

    /**
     * @param $item
     * @return bool
     */
    public static function chkIsEmail($item){

        return static::$validator->validateEmail(null,$item) ? $item : false;
    }

    /**
     * @param $item
     * @return bool
     */
    public static function chkIsInt($item){

        return static::$validator->validateInt(null,$item) ? $item : false;

    }

    /**
     * @param $item
     * @return bool
     */
    public function chkIsFloat($item){

        return static::$validator->validateFloat(null,$item) ? $item : false;
    }

    /**
     * @param $bDeleteFile
     * @param $sUploadDir
     * @param bool $sUploadPath
     * @param bool $sFilename
     * @param bool $bCache
     * @param int $iExecutionTime
     * @param bool $bReturnContents
     * @return array
     */
    public static function fileUpload($bDeleteFile,$sUploadDir,$sUploadPath=false,$sFilename=false,$bCache=false,$iExecutionTime=5,$bReturnContents=false,$bReturnFileUrl){

        $sUploadPath = !$sUploadPath ? ini_get("upload_tmp_dir") : $sUploadPath;

        if(!$bCache){
            // Make sure file is not cached (as it happens for example on iOS devices)
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
        }

        //Execution time
        @set_time_limit($iExecutionTime * 60);

        // Settings
        $targetDir = $sUploadPath . DIRECTORY_SEPARATOR . $sUploadDir;
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 5 * 3600; // Temp file age in seconds


        // Create target dir
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }
        $response=new stdClass();
        // Get a file name
        if($sFilename){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $sFileMimeType=finfo_file($finfo,$_FILES["file"]["tmp_name"]) ;
            finfo_close($finfo);

            switch($sFileMimeType){
                case 'image/png':
                    $fileName=$sFilename.'.png';
                    break;
                case 'image/jpeg':
                    $fileName=$sFilename.'.jpg';
                    break;
                case 'application/vnd.ms-excel':
                case 'text/plain':
                case 'text/csv' :
                case 'text/tsv' :
                    $fileName=$sFilename.'.csv';
                    break;
                default:
                    $response->error=[
                        'code'=>100,
                        'message'=>lang::get('Mime Type Not Recognised')
                    ];
                    return $response;
            }
        }
        elseif (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = uniqid("file_");
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

        if(file_exists($filePath)){
            unlink($filePath);
        }

        if(isset($_POST['mimetype']) && !empty($_POST['mimetype'])){

            switch($_POST['mimetype']){
                case 'image':
                    $aMimeTypes = ['image/jpeg','image/png'];
                    break;
                default:
                    $aMimeTypes = ['application/vnd.ms-excel','text/plain','text/csv','text/tsv'];
            }
            if(!in_array($_FILES['file']['type'],$aMimeTypes)){

                $response->error=[
                    'code'=>100,
                    'message'=>lang::get('Mime Type Not Allowed')
                ];
                return $response;
            }
        }



        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        // Remove old temp files
        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                $response->error=[
                    'code'=>100,
                    'message'=>'Failed to open temp directory.'
                ];
                return $response;

            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }


// Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                $response->error=[
                    'code'=>103,
                    'message'=>'Failed to move uploaded file.'
                ];
                return $response;

            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                $response->error=[
                    'code'=>101,
                    'message'=>'Failed to open input stream.'
                ];
                return $response;

            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                $response->error=[
                    'code'=>101,
                    'message'=>'Failed to open input stream.'
                ];
                return $response;

            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        @fclose($out);
        @fclose($in);

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }

        $aFileContents=file($_FILES["file"]["tmp_name"]);

        if($bDeleteFile){
            unlink($filePath);

        }
        if($bReturnFileUrl){
            $response->error=null;
            $response->msg='Image Uploaded';
            $response->imageUrl=Config::$config->webUrl.'images/'.$sUploadDir.'/'.$fileName;
            return $response;
        }
        return $bReturnContents ? $aFileContents : true;
    }

    /**
     * @param $str
     * @return string
     */
    public static function formatStringForMethodName($str){
        $aXpl=explode('_',$str);
        foreach($aXpl AS $k=>$r){
            $aXpl[$k]=ucfirst($r);
        }
        return implode('',$aXpl);
    }


    public static function getNumCond($var1,$op,$var2) {

        switch ($op) {
            case "=":
                return $var1 == $var2;
            case "!=": 
                return $var1 != $var2;
            case ">=":
                return $var1 >= $var2;
            case "<=":
                return $var1 <= $var2;
            case ">":
                return $var1 >  $var2;
            case "<":
                return $var1 <  $var2;
            default:
                return true;
        }   
    }

    /**
     * @param $needle
     * @param $haystack
     * @param bool $strict
     * @return bool|int|string
     */
    public function recursive_array_search($needle,$haystack,$strict=false){
        foreach($haystack as $key=>$value) {
            $current_key=$key;
            if($needle===$value OR (is_array($value) && self::recursive_array_search($needle,$value,$strict) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    public function recursiveFindKey(array $array, $needle){
        $iterator  = new RecursiveArrayIterator($array);
        $recursive = new RecursiveIteratorIterator($iterator,
                             RecursiveIteratorIterator::SELF_FIRST);
        foreach ($recursive as $key => $value) {
            if ($key === $needle) {
                return $value;
            }
        }
    }
} 