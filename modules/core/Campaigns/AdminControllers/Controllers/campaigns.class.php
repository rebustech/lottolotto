<?php
//error_reporting(E_ALL);
/**
 * Campaigns admin controller
 * @package Campaigns Engine
 * @subpackage AdminControllers
 */
class campaigns extends AdminController {


    /**
     * Campaign Model/Repository
     * @var CampaignEntity
     */
    private $campaign;

    /**
     * Campaign helpers
     * @var CampaignHelpers
     */
    public static $campaignHelper;

    function __construct(){

        $this->campaign = new CampaignEntity();

        $this->campaignOptions = new CampaignOptions();

        $this->campaignRedemptions = new CampaignRedemptions();

        $this->campaignRecipients = new CampaignRecipients();

        $this->campaignContent = new CampaignPageContent();

        static::$campaignHelper=new CampaignHelpers();
    }

    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        $tabs=new TabbedInterface();


        //if(!isset($_GET['id']) && !is_numeric($_GET['id'])){
        $oMainDetailsTab=new TabbedInterface(lang::get('campaign_setup'));
        $oMainDetailsTab->sIcon='fa-wrench';
        $oMainDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
        $tabs->aItems['details']=$oMainDetailsTab;
        //}

        // Show Replicate tab only on new campaign
        if(!isset($_GET['id']) || !is_numeric($_GET['id'])){
            //Create a tab to show replicate campaign options
            $oReplicateTab=new TabbedInterface(lang::get('campaign_replicate'));
            $oReplicateTab->sIcon='fa-copy';
            $oReplicateControl=new CampaignReplicateControl();
            $oReplicateControl->sName='promotion_setup';
            $oReplicateControl->sCaption='Campaign Replication';

            $oReplicateTab->AddControl($oReplicateControl);

            $tabs->aItems['replicate']=$oReplicateTab;
        }


        // Show tabs when editing an existing campaign
        if(isset($_GET['id']) && is_numeric($_GET['id'])){

            /*
            |--------------------------------------------------------------------------
            | Tags Tab
            |--------------------------------------------------------------------------
            */

            $oTagsTab=new TabbedInterface(lang::get('tags'));
            $oTagsTab->sIcon='fa-tags';

            //Get all available tags for the picker
            if($iId>0){
                $sAllTagsSql='SELECT id,name
                                FROM tags
                                WHERE id NOT IN (
                                SELECT fk_tag_id FROM campaign_tag WHERE fk_campaign_id='.(int)$iId.'
                                )';

            }
            else{
                $sAllTagsSql='SELECT id,name FROM tags';
            }
            $aAllTags=DAL::executeQuery($sAllTagsSql);
            //Get assigned tags
            $sSelectedTags='SELECT t1.id,t1.name
                        FROM tags t1
                        JOIN campaign_tag t2 ON t2.fk_tag_id=t1.id AND fk_campaign_id='.(int)$iId;

            $aSelectedTags=DAL::executeQuery($sSelectedTags);
            $oTagsControl=new TagsManyToManyPickListControl($aAllTags,$aSelectedTags);
            $oTagsControl->sName='tags';
            $oTagsControl->sCaption='Tags';

            $oTagsTab->AddControl($oTagsControl);
            $tabs->aItems['tags']=$oTagsTab;



            /*
            |--------------------------------------------------------------------------
            | Campaign Promotion Setup Config
            |--------------------------------------------------------------------------
            */

            //Create a tab to show campaign options
            $oSetupTab=new TabbedInterface(lang::get('campaign_promotion_setup'));
            $oSetupTab->sIcon='fa-gears';

            //Get configuration template
            $sControlConfigTemplate=$this->campaign->getPromotionTypeConfigTemplateByCampaignId((int)$iId,true);

            //Check if campaign has setup conf stored
            $bConfigExists=$this->campaign->checkIfConfigExists((int)$iId);

            if($bConfigExists){

                //Get the campaign setup conf
                $sCurrentConfig=$this->campaign->getPromotionTypeSetupConfigByCampaignId((int)$iId,true);

                //Loop through template fields and set the default values to current ones
                foreach($sControlConfigTemplate->fields AS $oField){
                    if(property_exists($sCurrentConfig, $oField->name)){
                        $oField->default=$sCurrentConfig->{$oField->name};
                    }
                }

            }


            $oSetupControl=new PromotionSetupControl(json_encode($sControlConfigTemplate));
            $oSetupControl->sName='promotion_setup';
            $oSetupControl->sCaption='Promotion Setup Config';

            $oSetupTab->AddControl($oSetupControl);


            $tabs->aItems['promotion_setup']=$oSetupTab;


            /*-----------------------------------------------------------------------------------------------*/
            //If campaign setup is saved
            if($bConfigExists){
                //Create a tab to show campaign options
                $oOptionsTab=new TabbedInterface(lang::get('campaign_options'));
                $oOptionsTab->sIcon='fa-dot-circle-o';

                $bOptionsConfigExists=$this->campaign->checkIfOptionsConfigExists((int)$iId);

                $sCurrentOptions=$this->campaignOptions->getNewCampaignOptions((int)$iId,true);

                if($bOptionsConfigExists){
                    $sCurrentOptions=$this->campaignOptions->getCampaignOptionsConfigValues((int)$iId,false);
                }

                $oOptionsControl=new CampaignOptionsControl($sCurrentOptions);
                $oOptionsControl->sName='options_setup';
                $oOptionsControl->sCaption='Campaign Options Config';
                $oOptionsControl->iCampaignId=$iId;

                $oOptionsTab->AddControl($oOptionsControl);

                $tabs->aItems['options']=$oOptionsTab;

                //if($bOptionsConfigExists){
                /*
                |--------------------------------------------------------------------------
                | Redemption
                |--------------------------------------------------------------------------
                */

                $oRedemptionTab=new TabbedInterface(lang::get('campaign_redemption'));
                $oRedemptionTab->sIcon='fa-filter';

                $aRedemptionOptions=$this->campaignRedemptions->getCampaignRedemptionsValues((int)$iId);

                $oRedemptionControl=new CampaignRedemptionControl($aRedemptionOptions);
                $oRedemptionControl->sName='campaign_redemption';
                $oRedemptionControl->sCaption='Campaign Redemption';
                $oRedemptionControl->iCampaignId=$iId;

                $oRedemptionTab->AddControl($oRedemptionControl);

                $tabs->aItems['redemption']=$oRedemptionTab;

                /*
                |--------------------------------------------------------------------------
                | Recipients
                |--------------------------------------------------------------------------
                */

                $oRecipientsTab=new TabbedInterface(lang::get('campaign_recipients'));
                $oRecipientsTab->sIcon='fa-users';

                $aRecipientOptions=$this->campaignRecipients->getCampaignRecipientValues((int)$iId);

                $oRecipientsControl=new CampaignRecipientsControl($aRecipientOptions,$iId);
                $oRecipientsControl->sName='campaign_recipients';
                $oRecipientsControl->sCaption='Campaign Recipients';


                $oRecipientsTab->AddControl($oRecipientsControl);

                $tabs->aItems['recipients']=$oRecipientsTab;

                /*
                |--------------------------------------------------------------------------
                | Content
                |--------------------------------------------------------------------------
                */
                $oPageContentTab=new TabbedInterface(lang::get('campaign_page_content'));
                $oPageContentTab->sIcon='fa-list';

                $aPageContentControl=$this->campaignContent->getPageContentProperties((int)$iId);

                $oPageContentControl=new CampaignPageContentControl($iId,$aPageContentControl);
                $oPageContentControl->sName='campaign_page_content';
                $oPageContentControl->sCaption='Campaign Page Content';

                $oPageContentTab->AddControl($oPageContentControl);

                $tabs->aItems['pageContent']=$oPageContentTab;

                // }
            }

        }

        /*
        |--------------------------------------------------------------------------
        | Audit Log
        |--------------------------------------------------------------------------
        */
        //Create a tab to show audit log
        $oLogTab=new TabbedInterface(lang::get('audit_log'));
        $oLogTab->sIcon='fa-th-list';
        $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'campaigns\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');
        $oItemsTable=new TableControl($aItemsData);
        $oItemsTable->getFieldsFromTable('audit_log');
        //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
        $oItemsTable->keepFields('audit_log','datetime,code,message');
        $oLogTab->AddControl($oItemsTable);
        $tabs->aItems['rights']=$oLogTab;



        $aItems[]=$tabs->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }

        return $sOut;

    }


    /**
     * Handles a post as part of any admin controller
     * Typically this will pass the post into custom controls
     */
    public function afterGenericFormPost(){

        /*
       |--------------------------------------------------------------------------
       | Replicate
       |--------------------------------------------------------------------------
       */
        if(static::$campaignHelper->issetNotEmptyNotNull($_POST['action']) &&
            $_POST['action']=='replicate' &&
            static::$campaignHelper->issetNotEmptyNotNull($_POST['repl_campaigns_to']) &&
            static::$campaignHelper->issetNotEmptyNotNull($_POST['repl_campaigns_from'])){

            $iNewCampaignId=$this->campaign->replicateCampaign($_POST['repl_campaigns_from'],$_POST['repl_campaigns_to']);

            if(is_int($iNewCampaignId)){
                header("Location: /administration/generic-details.php?tablename=campaigns&id=".$iNewCampaignId."&pp=generic-listing.php");
                die();
            }

        }

        //Save all other tabs


        //If campaign id is set
        if(static::$campaignHelper->issetNotEmptyNotNull($_GET['id'])&& is_numeric($_GET['id'])){

            /*
            |--------------------------------------------------------------------------
            | Promotion Type Setup
            |--------------------------------------------------------------------------
            */
            //If promotion type id is set
            if(static::$campaignHelper->issetNotEmptyNotNull($_POST['fk_promotion_type_id'])){

                //Set the array to be saved with config values
                $aPromotionTypeSetupValues=[];

                //Get json config
                $sPromotionTypeConfigJson=$this->campaign->getPromotionTypeConfigTemplateById($_POST['fk_promotion_type_id']);

                //Decode to an array
                $aPromotionTypeConfig=json_decode($sPromotionTypeConfigJson);

                //Check if there is a POST value foreach config field
                foreach($aPromotionTypeConfig->fields AS $sPromotionTypeConfigItem){

                    //Extract field name
                    $sFieldName=$sPromotionTypeConfigItem->name;

                    if(static::$campaignHelper->issetNotEmptyNotNull($_POST[$sFieldName])){
                        $aPromotionTypeSetupValues[$sFieldName]=$_POST[$sFieldName];
                    }

                }

                if(!empty($aPromotionTypeSetupValues)){

                    $this->campaign->storePromotionTypeSetupConfig($_GET['id'],$aPromotionTypeSetupValues);
                }
            }

            /*
            |--------------------------------------------------------------------------
            | Tags
            |--------------------------------------------------------------------------
            */

            $this->campaign->clearAllTagsByCampaignId($_GET['id']);

            if(static::$campaignHelper->issetNotEmptyNotNull($_POST['tags'])){

                $this->campaign->storeCampaignTags($_GET['id'],$_POST['tags']);

            }

        }
    }

    /**
     * @param $iMemberId
     * @param array $aCartDetails
     * @return array
     */
    public function userCampaigns($iMemberId,array $aCartDetails){

        $aActiveCampaigns=$this->campaign->getAllActiveCampaigns();

        /*
        |--------------------------------------------------------------------------
        | Options
        | Use array_intersect ?
        | array_intersect() returns an array containing all the values of array1 that are present in all the arguments.
        | Note that keys are preserved.
        |--------------------------------------------------------------------------
        */

        $aCampaignsFromOptions = $this->campaignOptions->getCampaignsFromConditions($iMemberId,$aActiveCampaigns,$aCartDetails);

        //var_dump($aCampaignsFromOptions);

       /*
       |--------------------------------------------------------------------------
       | Recipients
       |--------------------------------------------------------------------------
       */


        $aCampaignsFromList = $this->campaignRecipients->getCampaignsFromList($iMemberId);

        //var_dump($aCampaignsFromList);

        $aCampaignsFromConditions = $this->campaignRecipients->getCampaignsFromConditions($iMemberId,$aActiveCampaigns);

        //var_dump($aCampaignsFromConditions);

        /*
        |--------------------------------------------------------------------------
        | merge and intersect accordingly
        |--------------------------------------------------------------------------
        */
        $aCondList=array_merge($aCampaignsFromList,$aCampaignsFromConditions);

        return array_intersect($aCampaignsFromOptions, $aCondList);

        // return array_merge($aCampaignsFromList,$aCampaignsFromConditions);

        
    }
}