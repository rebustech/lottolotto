<?php 

class CampaignPageContentValidator extends \CampaignValidator implements \CampaignValidableInterface{

    static $rules=[
        'campaign_id'                               =>  'required|int|exists:campaigns,id',
        'campaignPageContentPromoPageDestination'   =>  'required',
        'campaignPageContentDestinationUrl'         =>  'required|url',
        'campaignPageContentOnPageText'             =>  'required',
        'campaignPageContentBetSlipText'            =>  'required',
        'campaignPageContentTermsConditions'        =>  'required'
    ];
} 