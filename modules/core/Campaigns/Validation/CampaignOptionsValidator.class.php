<?php

class CampaignOptionsValidator extends \CampaignValidator implements \CampaignValidableInterface {

    static $rules = [
        'products'		                => 	'if_present:int,exists:lotteries_cmn,lottery_id',
        'campaign_id'                   =>  'required|int|exists:campaigns,id',
        'operator'                      =>  'if_present:operator',
        'jackpot_amount'                =>  'if_present:int',
        'jackpot_currency'              =>  'if_present:int,exists:currencies,currency_id',
        'tickets_amount'                =>  'if_present:int',
        'orderpayinvalue_amount'        =>  'if_present:int',
        'orderpayinvalue_currency'      =>  'if_present:int,exists:currencies,currency_id',
        'payin_methods'                 =>  'if_present:int,exists:gateways,gateway_id',
        'subscriptionweeks_number'      =>  'if_present:int'
    ];
}