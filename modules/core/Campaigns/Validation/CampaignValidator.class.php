<?php

/**
 * Class CampaignValidator
 * Borrowed logic from Laravel PHP Framework
 */

abstract class CampaignValidator implements \CampaignValidableInterface {

    protected $messages=[
        'Required'          =>  'Field %s is required',
        'Max'               =>  'Field %s must contain no more than %d characters',
        'Min'               =>  'Field %s must be at least %d characters',
        'Int'               =>  'Field %s must be an integer',
        'Url'               =>  'Field %s must be a valid URL',
        'RequiredIf'        =>  'Field %s must be combined with field %s',
        'Exists'            =>  'Field %s must exist in Database',
        'Unique'            =>  'Field %s must be unique',
        'Date'              =>  'Field %s must be a valid date',
        'NotIn'             =>  'Field %s contains invalid characters',
        'RequiresConfig'    =>  'You must first add campaign option in the previous tab before saving redemption'
    ];
    /**
     * @var array
     */
    protected $errors=[];
    /**
     * The data under validation.
     *
     * @var array
     */
    protected $data;
    /**
     * @todo Move this per validation class
     * @var string
     */
    protected $sDecimalSeparator='.';

    public function __construct(array $data)
    {
        $this->data=$data;
        static::$rules = $this->explodeRules(static::$rules);

    }

    /**
     *
     * Take an array of fields and validate them against the defined rules
     * @internal param array $aAttributes
     * @return bool
     */
    public function passes(){

        foreach (static::$rules as $attribute => $rules)
        {
            $aReqIfEmpty=preg_grep('/required_if_empty/iu',$rules);
            if(!empty($aReqIfEmpty)){
                $sRule=$this->parseRule($aReqIfEmpty[0]);
                if($this->dataIsEmptyOrNotSet($sRule[1][0])){

                    unset($rules[0]);
                }

                else{

                    $rules=[];
                }
            }

            $aReqIfEmpty=preg_grep('/required_if\:/iu',$rules);
            if(!empty($aReqIfEmpty)){
                $sRule=$this->parseRule($aReqIfEmpty[0]);
                if(!$this->dataIsEmptyOrNotSet($sRule[1][0])){

                    unset($rules[0]);
                }

                else{

                    $rules=[];
                }
            }


            foreach ($rules as $rule)
            {
                if(!preg_match('/if_present/iu',$rule)){
                    $aRuleSet=$this->parseRule($rule);

                    $this->preValidate($attribute,$aRuleSet);

                }
                /*
                |--------------------------------------------------------------------------
                | If Attribute must be validated only if exists
                |--------------------------------------------------------------------------
                */
                else{
                    //Format the rule with default formatting
                    $rule=preg_replace('/,/','|',preg_replace('/if_present\:/iu','',$rule),1);
                    $this->validateIfPresent($attribute,$rule);
                }


            }
        }
        return count($this->errors) === 0;
    }

    /**
     * @param $attribute
     * @param $rule
     * @param array $parameters
     * @return bool
     */
    protected function validate($attribute, $rule,array $parameters=[])
    {

        if (trim($rule) == '') return;

        $method = "validate{$rule}";

        if(method_exists($this,$method)){

            $valid=!empty($parameters) ? $this->$method($attribute,$this->data[$attribute],$parameters) : $this->$method($attribute,$this->data[$attribute]) ;
            if(!$valid){

                !empty($parameters) ? array_push($this->errors,sprintf($this->messages[$rule],$attribute,$parameters[0])) : array_push($this->errors,sprintf($this->messages[$rule],$attribute));
                return false;
            }


            return true;
        }
        else{
            var_dump('Method not found '.$method);
        }

    }

    /**
     * @param $attribute
     * @param $value
     * @param array $parameters
     * @return bool
     */
    protected function validateNotIn($attribute,$value,array $parameters){
        foreach($parameters AS $str){
            if(strpos($value,$str)){

                return false;
            }
        }
        return true;
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $parameters
     * @return bool
     */
    protected function validateIn($attribute,$value,array $parameters){
        return in_array($value,$parameters);
    }

    /**
     * @param $attribute
     * @param $rules
     */
    protected function validateIfPresent($attribute,$rules){

        if(isset($this->data[$attribute])){
            $aRulesSet=explode('|',$rules);
            if(!empty($aRulesSet)){
                foreach($aRulesSet AS $sRule){
                    $rule=$this->parseRule($sRule);
                    $this->preValidate($attribute,$rule);
                }
            }
        }
    }

    /**
     * @param $attribute
     * @param $rule
     */
    protected function preValidate($attribute,$rule){
        if(isset($rule[1]) && is_array($rule[1]) && !empty($rule[1])){
            $this->validate($attribute, $rule[0],$rule[1]);

        }
        else{
            $this->validate($attribute, $rule[0]);
        }
    }
    /**
     * @param $rule
     * @return array
     */
    protected function parseRule($rule)
    {
        $parameters = array();

        // The format for specifying validation rules and parameters follows an
        // easy {rule}:{parameters} formatting convention. For instance the
        // rule "Max:3" states that the value may only be three letters.
        if (strpos($rule, ':') !== false)
        {
            list($rule, $parameter) = explode(':', $rule, 2);

            $parameters = str_getcsv($parameter);
        }
        if(preg_match('/_/iu',$rule)){
            $aXplRule=explode('_',$rule);
            foreach($aXplRule AS $k=>$r){
                $aXplRule[$k]=ucfirst($r);
            }
            $rule=implode('',$aXplRule);
        }
        return array(ucfirst($rule), $parameters);
    }

    /**
     * @param $rules
     * @return mixed
     */
    protected function explodeRules($rules)
    {
        foreach ($rules as $key => &$rule)
        {
            $rule = (is_string($rule)) ? explode('|', $rule) : $rule;
        }

        return $rules;
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    protected function validateRequired($attribute,$value){

        if (is_null($value))
        {
            return false;
        }
        elseif (is_string($value) && trim($value) === '')
        {
            return false;
        }
        elseif (is_array($value) && count($value) < 1)
        {
            return false;
        }

        return true;
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     *
     */
    public function validateInt($attribute, $value){
        return filter_var($value, FILTER_VALIDATE_INT) !== false;
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function validateDate($attribute,$value){

        $date = date_parse($value);

        return checkdate($date['month'], $date['day'], $date['year']);
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function validateIp($attribute, $value)
    {
        return filter_var($value, FILTER_VALIDATE_IP) !== false;
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function validateFloat($attribute, $value)
    {
        return filter_var($value, FILTER_VALIDATE_FLOAT,["options"=>["decimal" => $this->sDecimalSeparator]]) !== false;
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function validateEmail($attribute, $value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function validateUrl($attribute, $value)
    {
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    protected function validateMin($attribute,$value,$parameters){

        return mb_strlen($value,'utf-8') >= (int)$parameters[0];
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    protected function validateMax($attribute,$value,$parameters){
        return mb_strlen($value,'utf-8') <= (int)$parameters[0];
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     * @todo Must check if value is array
     */
    protected function validateUnique($attribute,$value,$parameters){
        $sTable=$parameters[0];
        $sColumn=$parameters[1];

        $sSql='SELECT COUNT(*) AS counter FROM '.$sTable.' WHERE `'.$sColumn.'`="'.$value.'"';

        $iCounter=\DAL::executeGetOne($sSql);

        return $iCounter>0  ? false : true ;

    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    protected function validateExists($attribute,$value,$parameters){

        $sTable=$parameters[0];
        $sColumn=$parameters[1];
        if(is_array($value)){
            foreach($value AS $v){
                $sSql='SELECT COUNT(*) AS counter FROM '.$sTable.' WHERE `'.$sColumn.'`="'.$v.'"';

                $iCounter=\DAL::executeGetOne($sSql);
                if($iCounter==0){
                    return false;
                }
            }
            return true;
        }
        else{
            $sSql='SELECT COUNT(*) AS counter FROM '.$sTable.' WHERE `'.$sColumn.'`="'.$value.'"';

            $iCounter=\DAL::executeGetOne($sSql);

            return $iCounter>0  ? true : false ;
        }


    }

    protected function validateRequiresConfig($attribute,$value,$parameters){

        $sTable=$parameters[0];
        $sColumn=$parameters[1];

        $sSql='SELECT COUNT(*) AS counter FROM '.$sTable.' WHERE (`'.$sColumn.'` is not null AND `'.$sColumn.'` !="") AND fk_campaign_id="'.$value.'"';

        $iCounter=\DAL::executeGetOne($sSql);

        return $iCounter>0  ? true : false ;



    }
    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    protected function validateRequiredIf($attribute, $value, $parameters){

        return isset($this->data[$parameters[0]]) && !empty($this->data[$parameters[0]]);
    }

    protected function validateRequiredIfEmpty($attribute, $value, $parameters){

        return isset($this->data[$parameters[0]]) && !empty($this->data[$parameters[0]]);
    }
    protected function dataIsEmptyOrNotSet($key){

        return !isset($this->data[$key]) || empty($this->data[$key]);
    }
    /**
     * @param $attribute
     * @param $value
     * @return bool
     */
    protected function validateOperator($attribute,$value){

        return in_array($value,['OR','AND']);
    }
    /**
     * Return the errors that occurred from validation
     * @return array
     */
    public function errors()
    {
        $errorsContainer=new stdClass();
        $errorsContainer->errors=$this->errors;
        return $errorsContainer;
    }

    public function getRules(){
        return static::$rules;
    }

    public function setRules($rules){
        static::$rules=$rules;
    }
}