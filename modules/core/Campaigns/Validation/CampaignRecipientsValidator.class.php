<?php 

class CampaignRecipientsValidator extends \CampaignValidator implements \CampaignValidableInterface{

    static $rulesReferenceMethod=true;

    static $rules=[
        'campaign_id'                   =>  'required|int|exists:campaigns,id',
        'date_operator_switch'          =>  'if_present:required,in:<,>,=',
        'date_picker'                   =>  'if_present:required,date',
        'text_control'                  =>  'if_present:required,int',
        'currency_control'              =>  'if_present:int,exists:currencies,currency_id',
        'bdayFrom_control'              =>  'if_present:date',
        'bdayTo_control'                =>  'if_present:date',
        'operator_switch'               =>  'if_present:required,in:<,>,=',
        'recipientsCsv'                 =>  'if_present:mimetype,csv',
        'segment_name'                  =>  'required|min:3|unique:campaign_promo_segments,segment_name',
        'master_switch'                 =>  'required',
        'products_control'              =>  'if_present:int,exists:game_engines,id',
        'language_control'              =>  'if_present:int,exists:languages,language_id',
        'country_control'               =>  'if_present:int,exists:countries_cmn,country_id',
        'signup_source_control'         =>  'if_present:int,exists:affiliate_adverts,id'
    ];



} 