<?php

class CampaignRedemptionValidator extends \CampaignValidator implements \CampaignValidableInterface {

    static $rules = [
        'campaign_id'                               =>  'required|int|exists:campaigns,id|requires_config:promotion_configs,options_config',
        'promo_code'		                        => 	'required_if_empty:multiple_promo_codes|min:3|max:12|unique:campaign_promo_codes,promo_code|not_in:1,0,t,I,o,d,j,l',
        'multiple_promo_codes'                      =>  'required_if_empty:promo_code|required|min:3||not_in:1,0,t,I,o,d,j,l',
        'number_of_promo_codes'                     =>  'required_if:multiple_promo_codes|required|int',
        //'campaign_tags'                             =>  'required_if:use_in_conjunction|required|exists:tags,name',
        'campaign_names'                            =>  'required_if:use_in_conjunction|required|exists:campaigns,id',
        'number_of_players'                         =>  'int',
        'maximum_campaign_value'                    =>  'int',
        'fk_maximum_value_currency'                 =>  'int|exists:currencies,currency_id',
        'maximum_bonus_per_customer'                =>  'int',
        'maximum_value_per_customer'                =>  'int',
        'fk_maximum_value_per_customer_currency'    =>  'int|exists:currencies,currency_id',
        'code_expires_at'                           =>  'date'
    ];
}