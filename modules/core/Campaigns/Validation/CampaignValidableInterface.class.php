<?php

interface CampaignValidableInterface {
    public function passes();
    public function errors();
} 