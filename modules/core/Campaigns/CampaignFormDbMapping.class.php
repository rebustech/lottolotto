<?php

class CampaignFormDbMapping {

    var $oCart = NULL;


    private function getCartFromSession(){
        $oSC = isset($_SESSION['cart']) ? unserialize($_SESSION['cart']) : null;
        if (is_object($oSC) && is_a($oSC, "Cart")) {
            $this->oCart = $oSC;
        }

        $shortCart=$this->oCart->getCartShortDetails();
        // print_r($shortCart);

        return $shortCart;
    }

    /**
     * Check if member has played specific game
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool|string|void
     */
    public function getPlayedProduct($iMemberId,array $aConditions,$sReturn){

        $sSql='SELECT game_engines.id
                FROM game_engines
                JOIN booking_order_items ON game_engines.id=booking_order_items.fk_game_engine_id
                JOIN bookings ON booking_order_items.fk_booking_id=bookings.booking_id
                JOIN members ON bookings.fk_member_id=members.member_id
                WHERE members.member_id='.(int)$iMemberId.' AND game_engines.id='.(int)$aConditions['value'].'
                GROUP BY game_engines.id';

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);

    }

    /**
     * Check if the last order of member matches condition
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool|string|void
     */
    public function getLastOrderDate($iMemberId,array $aConditions,$sReturn){

        $sSql='SELECT booking_date, DATE(MAX(booking_date)) AS t
                FROM bookings
                JOIN members ON bookings.fk_member_id=members.member_id
                WHERE members.member_id='.(int)$iMemberId.'
                HAVING t  '.$aConditions['switch'].' \''.$aConditions['value'].'\'';

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if member language is set to specific language
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getLanguage($iMemberId,array $aConditions,$sReturn){

        $sSql='SELECT fk_language_id
                FROM countries_lang
                JOIN members ON countries_lang.fk_country_id=members.fk_country_id
                WHERE members.member_id='.(int)$iMemberId.'
                AND countries_lang.fk_language_id='.(int)$aConditions['value'].'';

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if member birthday is in date range
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getBirthday($iMemberId,array $aConditions,$sReturn){

        $sSql='SELECT member_id
                FROM members
                WHERE dob IS NOT NULL
                AND member_id='.(int)$iMemberId.'
                AND  DATE(dob) BETWEEN DATE(\''.$aConditions['value'].'\') AND DATE(\''.$aConditions['additional']['date'].'\')';

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if member joined before, in or after specific date
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getJoinedDate($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT join_date
    	FROM members WHERE member_id=".(int)$iMemberId
            ." AND DATE(join_date) ".$aConditions['switch']." '".$aConditions['value']."'";

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if total member order amount is within condition
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getTotalOrderAmount($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT SUM(total) AS t FROM bookings
    	WHERE fk_currency_id=".$aConditions['additional']['currency']
            ." AND fk_member_id=".(int)$iMemberId
            ." HAVING t ".$aConditions['switch']." ".$aConditions['value'];

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if current member balance is within condition
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     * @todo add currency id join when implemented
     */
    public function getCurrentBalance($iMemberId,array $aConditions,$sReturn){

        $sSql='SELECT members.balance FROM members
                WHERE members.member_id='.(int)$iMemberId.'
                AND members.balance '.$aConditions['switch'].' '.(int)$aConditions['value'].'
                ';

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getSubscriptionWeeks($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT DATEDIFF(DATE(now()),DATE(join_date)) as days FROM members WHERE member_id=".(int)$iMemberId;

        $days=\DAL::executeGetOne($sSql);

        if(null!=$days){
            $weeks=$days / 7 ;

            $aConditions['switch']=($aConditions['switch']=='=')?'==':$aConditions['switch'];

            $a=eval('return '.$weeks.$aConditions['switch'].$aConditions['value'].';');

            $aProcessedResults=$a===true ? ['1'] : [] ;
        }
        else{
            $aProcessedResults=[];
        }
        return static::returnSelectedResponse($sSql,$aProcessedResults,$sReturn);
        //
    }

    /**
     * Check if total member deposits amount is within condition
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getLifeDeposits($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT SUM(amount) AS a FROM transactions
    	WHERE fk_member_id=".(int)$iMemberId
            ." AND fk_currency_id=".$aConditions['additional']['currency']
            ." AND confirmed=1 HAVING a ".$aConditions['switch']." '".$aConditions['value']."'";

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if total member orders match condition
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     * @todo  check with jp if we are getting the right fields
     */
    public function getLifeOrder($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT SUM(ABS(amount)) AS a FROM transactions
    	WHERE fk_member_id=".(int)$iMemberId
            ." AND fk_currency_id=".$aConditions['additional']['currency']
            ." AND confirmed=1"
            ." AND amount<0	HAVING a".$aConditions['switch'].$aConditions['value'];

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if member location is same with condition specified
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getLocation($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT fk_country_id FROM members WHERE member_id=".(int)$iMemberId
            ." AND fk_country_id=".(int)$aConditions['value'];

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * Check if member ever used selected payment method (gateway)
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getPaymentMethod($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT gateways.gateway_id FROM gateways"
            ." JOIN transactions ON gateways.gateway_id=transactions.fk_gateway_id"
            ." JOIN members ON transactions.fk_member_id=members.member_id"
            ." WHERE members.member_id=".(int)$iMemberId
            ." AND gateways.gateway_id=".(int)$aConditions['value'];

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }

    /**
     * @param $iMemberId
     * @param array $aConditions
     * @param $sReturn
     * @return bool
     */
    public function getSignupSource($iMemberId,array $aConditions,$sReturn){

        $sSql="SELECT bookings.fk_source_advert_id FROM bookings"
            ." JOIN members ON bookings.fk_member_id=members.member_id"
            ." WHERE members.member_id=".(int)$iMemberId
            ." AND bookings.fk_source_advert_id=".(int)$aConditions['value'];

        $aResults=\DAL::executeQuery($sSql);

        return static::returnSelectedResponse($sSql,$aResults,$sReturn);
    }


    // payment method (not availliable in cart page)
    public function getPayinvalue($iMemberId,array $aConditions,$sReturn){

        // $sSql="SELECT bookings.fk_source_advert_id FROM bookings"
        //     ." JOIN members ON bookings.fk_member_id=members.member_id"
        //     ." WHERE members.member_id=".(int)$iMemberId
        //     ." AND bookings.fk_source_advert_id=".(int)$aConditions['value'];

        // $aResults=\DAL::executeQuery($sSql);

        // return static::returnSelectedResponse($sSql,$aResults,$sReturn);

        // print_r($aConditions);

        return true;
    }

    public function getProducts($iMemberId,array $aConditions,$aCartItems,$sReturn){
        $aProductIdsInCart=[];

        foreach($aCartItems as $key => $aCartItem){
            if(is_numeric($key) && isset($aCartItem['gameID'])){

                $oGameEngine = GameEngine::getById($aCartItem['gameID']);
                if($oGameEngine){  
                    $aChildComponents = $oGameEngine->getChildComponents();
                    $mlLottery = $aChildComponents[0];               

                    if($mlLottery->fk_game_engine_id == $aConditions['value']){
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getTickets($iMemberId,array $aConditions,$aCartItems,$sReturn){
        $shortCart=self::getCartFromSession();

        if(CampaignHelpers::getNumCond($shortCart['items'],$aConditions['switch'],$aConditions['value']))
            return true;
        return false;
    }
    
    public function getOrderpayinvalue($iMemberId,array $aConditions,$aCartItems,$sReturn){
        $shortCart=self::getCartFromSession();

        // todo: get cart item currency

        if(CampaignHelpers::getNumCond($shortCart['total'],$aConditions['switch'],$aConditions['value']))
            return true;
        return false;
    }
    
    public function getJackpot($iMemberId,array $aConditions,$aCartItems,$sReturn){

        // Get availiable jackpots
        $lotteryDrawPrizes = LLCache::get('GE-lotteryDraws');

        foreach($aCartItems as $key => $aCartItem){
            if(is_numeric($key)){
                
                // get cart items lotteryId
                $oGameEngine = GameEngine::getById($aCartItem['gameID']);
                
                if($oGameEngine){  
                    $aChildComponents = $oGameEngine->getChildComponents();
                    $mlLottery = $aChildComponents[0];

                    if($lotteryDrawPrizes[$mlLottery->iLotteryID]){

                        if( CampaignHelpers::getNumCond($lotteryDrawPrizes[$mlLottery->iLotteryID]['jackpot'],$aConditions['switch'],$aConditions['value']) ){
                                                        
                            if($lotteryDrawPrizes[$mlLottery->iLotteryID]['currency_id'] == $aConditions['additional']['currency']){
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function getGroupOperator($iMemberId,array $aConditions,$sReturn){
        return $aConditions['value'];
    }

    public function getOperator($iMemberId,array $aConditions,$sReturn){
        return $aConditions['switch'];
    }




    /**
     * @param $sSql
     * @param $aResults
     * @param $sReturn
     * @return bool
     */
    private static function returnSelectedResponse($sSql,$aResults,$sReturn){

        switch($sReturn){
            case 'sql':
                $result=$sSql;
                break;
            case 'bool':
                $result=!empty($aResults);
                break;
            default:
                $result=$aResults;
        }

        return $result ;

    }
} 