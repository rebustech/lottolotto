<?php
namespace LL\Campaigns;

/**
 * Class Installer
 *
 * Following database tables will be created:
 * affiliate_groups, campaigns_groups_link, affiliate_advert_types, affiliate_adverts, campaigns_adverts_link,
 * affiliate_groups_adverts_link.
 *
 * Following tables will be modified:  members, bookings
 * Following tables will be seeded: affiliate_advert_types
 *
 * Following menu items will be created in administration: manage Groups, Manage Adverts under Campaigns
 * and Manage Affiliate Advert Types under Admin
 *
 * @package LL\Campaigns
 * @author Nikos Poulias
 */

use \LL\Installer\Table,
    \LL\Installer\Field,
    \LL\Installer\Constraint,
    \LL\Installer\Task
    ;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){

        $aJobs[]=new Task('\LL\Campaigns\Installer', 'installSchema', 'Install Campaigns Database');
        $aJobs[]=new Task('\LL\Campaigns\Installer', 'installMenuItems', 'Install Campaigns Admin Menu Items');
        $aJobs[]=new Task('\LL\Campaigns\Installer', 'populatePromotionTypes', 'Import Promotion Types');
        $aJobs[]=new Task('\LL\Campaigns\Installer', 'populateOccasionsTypes', 'Import Occasions Entries');
        $aJobs[]=new Task('\LL\Campaigns\Installer', 'populateTags', 'Import dummy tags data');

        return $aJobs;
    }

    static function installSchema(Task $oTask){


        self::installPromotionTypesTable();
        self::installOccasionsTables();
        self::installCampaignsTable();
        self::installPromotionConfigTable();
        self::installTagsTables();
        self::installCampaignPromoCodesTable();
        self::installCampaignPromoCodeLimitsTable();
        self::installCampaignsLimitsXrefTable();
        self::installCampaignPromoCodeLimitCampaignsPivotTable();

        // these tables are joining with the limit ID and not with the campaign ID
        self::installCampaignPromoCodeOtherCampaignsTable();
        self::installCampaignPromoCodeOtherTagsTable();

        // Recipients / Condition segments tables
        self::installCampaignPromoSegmentsTable();
        self::installCampaignPromoSegmentsXrefTable();
        self::installCampaignPromoMembersXrefTable();

        //member Status
        self::installMemberStatusTable();
        self::populateMemberStatusTable();
        self::addMemberStatusToMembersTable();

        // Promo Content table
        self::installCampaignPromoContentTable();
    }

    static function installMenuItems(Task $oTask){
        self::importNewAdminPages();
    }

    /**
     * Create member status table
     */
    static function installMemberStatusTable(){

        //Create table
        $oTable=new Table('member_status');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('status','varchar',255));

        //Run
        $oTable->compile();

    }

    /**
     * Populate member status codes if table empty
     */
    static function populateMemberStatusTable(){

        $aStatusCodes=[
            '1'     =>   'Lead',
            '2'     =>   'Active',
            '3'     =>   'Inactive'
        ];

        $iCounter=\DAL::executeGetOne('SELECT COUNT(*) AS counter FROM member_status');

        if($iCounter==0){

            foreach($aStatusCodes AS $key=>$statusCode){
                \DAL::Insert('member_status',['id'=>$key,'status'=>$statusCode]);
            }

        }
    }

    static function addMemberStatusToMembersTable(){

        $sCheckIfColumnExistsSql='SELECT *
            FROM information_schema.COLUMNS
            WHERE
                TABLE_SCHEMA = \''.DATABASENAME.'\'
            AND TABLE_NAME = \'members\'
            AND COLUMN_NAME = \'fk_status_id\'';

        $aCheck=\DAL::executeQuery($sCheckIfColumnExistsSql);
        if(empty($aCheck)){
            $sAlterMembersTableSql='ALTER TABLE members ADD fk_status_id INT(11) NOT NULL DEFAULT \'2\' , ADD INDEX (fk_status_id)';
            \DAL::executeQuery($sAlterMembersTableSql);
            $sAddConstraintSql='ALTER TABLE members ADD CONSTRAINT members_member_status FOREIGN KEY (fk_status_id) REFERENCES '.DATABASENAME.'.member_status(id) ON DELETE RESTRICT ON UPDATE CASCADE';
            \DAL::executeQuery($sAddConstraintSql);
        }

    }
    static function installPromotionTypesTable(){

        //Create table
        $oTable=new Table('promotion_types');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('name','varchar',255));
        $oTable->addField(new Field('config','TEXT'));
        $oTable->addField(new Field('is_active','TINYINT',null,false,1));

        //Run
        $oTable->compile();

    }

    static function installPromotionConfigTable(){

        //Create table
        $oTable=new Table('promotion_configs');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('fk_campaign_id','INT',11,NULL));
        $oTable->addField(new Field('setup_config','TEXT'));
        $oTable->addField(new Field('options_config','TEXT'));
        //$oTable->addField(new Field('redemption_config','TEXT'));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));

        //Run
        $oTable->compile();

    }

    static function installOccasionsTables(){

        //Create table
        $oTable=new Table('occasions');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('name','varchar',255));

        //Run
        $oTable->compile();

    }

    static function installCampaignsTable(){

        //Create table
        $oTable=new Table('campaigns');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('name','varchar',255));

        $oTable->addField(new Field('fk_promotion_type_id','INT',11,NULL));
        $oTable->addField(new Field('fk_occasion_id','INT',11,NULL));
        $oTable->addField(new Field('occasion_valid_for','INT',11));

        $oTable->addField(new Field('is_active','TINYINT',NULL,false,0));
        $oTable->addField(new Field('start_date','DATE'));
        $oTable->addField(new Field('end_date','DATE'));
        //$oTable->addField(new Field('created_at','TIMESTAMP',NULL,false,'CURRENT_TIMESTAMP'));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_promotion_type_id','promotion_types','id'));
        $oTable->addConstraint(new Constraint('fk_occasion_id','occasions','id'));

        //Run
        $oTable->compile();

    }

    static function installTagsTables(){

        //Create table
        $oTable=new Table('tags');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('name','varchar',255));

        //Run
        $oTable->compile();

        //Create Pivot table
        $oPivotTable=new Table('campaign_tag');

        //Add primary key
        $oPivotTable->addIdField();

        //Add other fields
        $oPivotTable->addField(new Field('fk_campaign_id','int',11));
        $oPivotTable->addField(new Field('fk_tag_id','int',11));

        //Add foreign key constraints
        $oPivotTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));
        $oPivotTable->addConstraint(new Constraint('fk_tag_id','tags','id'));

        //Run
        $oPivotTable->compile();

    }



    static function installPromotionsTypesConfTable(){

        //Create table
        $oTable=new Table('promotion_type_conf');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('name','varchar',255));
        $oTable->addField(new Field('values','varchar',255));
        $oTable->addField(new Field('fk_promotion_type_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_promotion_type_id','promotion_types','id'));

        //Run
        $oTable->compile();

    }

    static function installCampaignPromoCodesTable(){

        //Create table
        $oTable=new Table('campaign_promo_codes');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('promo_code','varchar',255));
        $oTable->addField(new Field('fk_campaign_id','int',11));
        //$oTable->addField(new Field('fk_affiliate_id','int',11));
        $oTable->addField(new Field('times_used','int',11));

        //Add foreign key constraints
        //$oTable->addConstraint(new Constraint('fk_affiliate_id','admin_users','user_id'));
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoCodeLimitsTable(){
        //Create table
        $oTable=new Table('campaign_promo_code_limits');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        // $oTable->addField(new Field('fk_campaign_promo_code_id','int',11));
        $oTable->addField(new Field('fk_affiliate_id','int',11));
        $oTable->addField(new Field('use_in_conjunction','TINYINT',NULL,false,0));
        $oTable->addField(new Field('number_of_players','int',11));
        $oTable->addField(new Field('maximum_campaign_value','int',11));
        $oTable->addField(new Field('fk_maximum_value_currency','int',11));
        $oTable->addField(new Field('maximum_bonus_per_customer','int',11));
        $oTable->addField(new Field('maximum_value_per_customer','int',11));
        $oTable->addField(new Field('fk_maximum_value_per_customer_currency','int',11));
        $oTable->addField(new Field('code_expires_at','DATE'));

        //Add foreign key constraints
        //$oTable->addConstraint(new Constraint('fk_campaign_promo_code_id','campaign_promo_codes','id'));
        $oTable->addConstraint(new Constraint('fk_affiliate_id','admin_users','user_id'));
        $oTable->addConstraint(new Constraint('fk_maximum_value_currency','currencies','currency_id'));
        $oTable->addConstraint(new Constraint('fk_maximum_value_per_customer_currency','currencies','currency_id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignsLimitsXrefTable(){
        //Create table
        $oTable=new Table('campaign_promo_code_limits_xref');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('fk_promo_id','int',11));
        $oTable->addField(new Field('fk_limit_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_promo_id','campaign_promo_codes','id'));
        $oTable->addConstraint(new Constraint('fk_limit_id','campaign_promo_code_limits','id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoCodeLimitCampaignsPivotTable(){

        //Create table
        $oTable=new Table('campaign_promo_code_limit_campaign');

        //Add primary key
        $oTable->addIdField();


        $oTable->addField(new Field('fk_campaign_promo_code_limit_id','int',11));
        $oTable->addField(new Field('fk_campaign_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_campaign_promo_code_limit_id','campaign_promo_code_limits','id'));
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoCodeOtherCampaignsTable(){

        //Create table
        $oTable=new Table('campaign_promo_code_other_campaigns');

        //Add primary key
        $oTable->addIdField();


        $oTable->addField(new Field('fk_limit_id','int',11));
        $oTable->addField(new Field('fk_campaign_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_limit_id','campaign_promo_code_limits','id'));
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoCodeOtherTagsTable(){

        //Create table
        $oTable=new Table('campaign_promo_code_other_tags');

        //Add primary key
        $oTable->addIdField();


        $oTable->addField(new Field('fk_limit_id','int',11));
        $oTable->addField(new Field('fk_tag_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_limit_id','campaign_promo_code_limits','id'));
        $oTable->addConstraint(new Constraint('fk_tag_id','tags','id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoSegmentsTable(){

        //Create table
        $oTable=new Table('campaign_promo_segments');

        //Add primary key
        $oTable->addIdField();

        $oTable->addField(new Field('segment_name','varchar',255));
        $oTable->addField(new Field('segment_conditions','text'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoSegmentsXrefTable(){
        //Create table
        $oTable=new Table('campaign_promo_segments_xref');

        //Add primary key
        $oTable->addIdField();

        $oTable->addField(new Field('fk_campaign_id','int',11));
        $oTable->addField(new Field('fk_segment_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));
        $oTable->addConstraint(new Constraint('fk_segment_id','campaign_promo_segments','id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoMembersXrefTable(){
        //Create table
        $oTable=new Table('campaign_promo_members_xref');

        //Add primary key
        $oTable->addIdField();

        $oTable->addField(new Field('fk_campaign_id','int',11));
        $oTable->addField(new Field('fk_member_id','int',11));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));
        $oTable->addConstraint(new Constraint('fk_member_id','members','member_id'));

        //Run
        $oTable->compile();
    }

    static function installCampaignPromoContentTable(){

        //Create table
        $oTable=new Table('campaign_promo_content');

        //Add primary key
        $oTable->addIdField();

        //Add other fields
        $oTable->addField(new Field('fk_campaign_id','int',11,NULL));
        $oTable->addField(new Field('destination','tinyint',null,false,0));
        $oTable->addField(new Field('destination_url','varchar',2083));
        $oTable->addField(new Field('page_text','text'));
        $oTable->addField(new Field('betslip_text','text'));
        $oTable->addField(new Field('termsConditions','longtext'));

        //Add foreign key constraints
        $oTable->addConstraint(new Constraint('fk_campaign_id','campaigns','id'));

        //Run
        $oTable->compile();
    }

    /**
     * Create menu items in the administration interface
     * if they don't exists
     */
    static function importNewAdminPages(){

        $iParentId=\DAL::executeGetOne('SELECT id FROM admin_pages WHERE title=\'Campaigns\'');

        if(is_null($iParentId) || $iParentId==0){
            $aData=['title'=>'Campaigns','filename'=>'generic-listing.php','tablename'=>'campaigns','parent_id'=>0,'icon'=>'fa-dot-circle-o'];

            $iParentId =  \DAL::Insert('admin_pages',$aData,true);
        }

        $iCounter=\DAL::executeGetOne('SELECT COUNT(*) AS counter FROM admin_pages WHERE title=\'Promotion Types\' AND parent_id='.$iParentId);
        if($iCounter==0){
            $aData=['title'=>'Promotion Types','filename'=>'generic-listing.php','tablename'=>'promotion_types','parent_id'=>$iParentId];

            \DAL::Insert('admin_pages',$aData);
        }

        $iCounter=\DAL::executeGetOne('SELECT COUNT(*) AS counter FROM admin_pages WHERE title=\'Tags\' AND parent_id='.$iParentId);

        if($iCounter==0){
            $aData=['title'=>'Tags','filename'=>'generic-listing.php','tablename'=>'tags','parent_id'=>$iParentId];

            \DAL::Insert('admin_pages',$aData);
        }
    }

    /**
     * Import data from specifications doc
     * if table is empty
     * @todo Have config decoded
     */
    static function populatePromotionTypes(){

        $iCounter=\DAL::executeGetOne('SELECT COUNT(*) AS counter FROM promotion_types');

        if($iCounter==0){
            $aData=[];
            $aData[]=['name'=>'Discount on Purchase','config'=>'{"title":"Discount on pay-in","fields":[{"type":"text","name":"discount_amount","default":null},{"type":"select","name":"discount_amount_type","default":"currency","options":[{"name":"Currency","value":"currency"},{"name":"Percent","value":"percent"}]}]}','is_active'=>1];
            $aData[]=['name'=>'Discount on Pay-in','config'=>'{"title":"Discount on pay-in","fields":[{"type":"text","name":"discount_amount","default":null},{"type":"select","name":"discount_amount_type","default":"currency","options":[{"name":"Currency","value":"currency"},{"name":"Percent","value":"percent"}]}]}','is_active'=>1];
            $aData[]=['name'=>'Extra balance on Pay-in','config'=>'{"title":"Discount on pay-in","fields":[{"type":"text","name":"discount_amount","default":null},{"type":"select","name":"discount_amount_type","default":"currency","options":[{"name":"Currency","value":"currency"},{"name":"Percent","value":"percent"}]}]}','is_active'=>1];
            $aData[]=['name'=>'Direct Bonus','config'=>'{"title":"Discount on pay-in","fields":[{"type":"text","name":"discount_amount","default":null},{"type":"select","name":"discount_amount_type","default":"percent","options":[{"name":"Percent","value":"percent"}]}]}','is_active'=>1];
            $aData[]=['name'=>'Money-back','config'=>'{"title":"Money-back criteria","fields":[{"type":"select","name":"money_back_criteria","default":"all","options":[{"name":"All Tickets","value":"all"},{"name":"Winning Tickets only","value":"winning_tickets"},{"name":"Loosing Tickets only","value":"loosing_tickets"}]}]}','is_active'=>1];
            $aData[]=['name'=>'Ticket for Free','config'=>'{"title":"Number of tickets to recieve free ticket","fields":[{"type":"text","name":"num_of_tickets_to_rcv","default":null}]}','is_active'=>1];
            $aData[]=['name'=>'Subscription Discount','config'=>'{"title":"Discount on pay-in","fields":[{"type":"text","name":"discount_amount","default":null},{"type":"select","name":"discount_amount_type","default":"currency","options":[{"name":"Currency","value":"currency"},{"name":"Percent","value":"percent"}]}]}','is_active'=>1];

            foreach($aData AS $aDataItem){
                \DAL::Insert('promotion_types',$aDataItem);
            }
        }

    }

    /**
     * Import dummy data
     * if table is empty
     */
    static function populateTags(){

        $iCounter=\DAL::executeGetOne('SELECT COUNT(*) AS counter FROM tags');

        if($iCounter==0){

            for($i=1;$i<6;$i++){
                \DAL::Insert('tags',['name'=>'Tag '.$i]);
            }

        }

    }

    /**
     * Import data from specifications doc
     * if table is empty
     */
    static function populateOccasionsTypes(){

        $iCounter=\DAL::executeGetOne('SELECT COUNT(*) AS counter FROM occasions');

        if($iCounter==0){
            $aData=[];
            $aData[]=['name'=>'Birthday'];
            $aData[]=['name'=>'Jubilee'];

            foreach($aData AS $aDataItem){
                \DAL::Insert('occasions',$aDataItem);
            }
        }
    }
}
