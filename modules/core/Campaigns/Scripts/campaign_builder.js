/**
 * Created by np0 on 7/21/14.
 */

var campaignsBuilder={};

campaignsBuilder.setup=function(){
    $('form').not('#campaignOptionsForm').not('#campaignRedemptionForm').not('#campaignRecipientsForm').not('#campaignPageContentForm').submit(campaignsBuilder.prepareSave);
    //$('form').submit(campaignsBuilder.validate);
}


campaignsBuilder.prepareSave=function(){
    var eForm=$('#contentarea form');


    /*
     |--------------------------------------------------------------------------
     | Tags
     |--------------------------------------------------------------------------
     */
    var tagsContainer=$('div#tags.many_to_many_picklist_form');

    if(tagsContainer.length > 0){

        $('#tags .selectedItems li').each(function(){
            var eSelectedTag=$('<input type="checkbox" checked="checked" name="tags[]" value="'+$(this).attr('data-id')+'"/>');
            eSelectedTag.css('display','none');
            eForm.append(eSelectedTag);
        });
    }

    /*
     |--------------------------------------------------------------------------
     | Promotion Type Setup Data
     |--------------------------------------------------------------------------
     */


    $( "fieldset.promotionsetupform div input" ).each(function() {

        var eSelectedField=$('<input type="hidden"  name="'+$(this).attr("name")+'" value="'+$(this).val()+'"/>');

        eForm.append(eSelectedField);
    });

    $( "fieldset.promotionsetupform div select" ).each(function() {
        var eSelectedField=$('<input type="hidden"  name="'+$(this).attr("name")+'" value="'+$(this).val()+'"/>');

        eForm.append(eSelectedField);
    });

    /*
     |--------------------------------------------------------------------------
     | Campaign Options Type Setup Data
     |--------------------------------------------------------------------------
     */

    $('.campaignConditionsGroup > fieldset').each(function() {

        var groupID='';

        if($(this).is('fieldset')) groupID=$(this).prop('id');

        var iConditionsInGroup=campaignsBuilder.countGroupConditions(groupID);

        if(iConditionsInGroup>0){

            $(this).find('input, select').each(function() {

                var eSelectedField=$('<input type="hidden" name="'+$(this).attr("name")+'" value="'+$(this).val()+'"/>');

                eForm.append(eSelectedField);
            });


            $(this).next('.groupOperator').find('select').each(function() {

                var eSelectedField=$('<input type="hidden" name="'+$(this).attr("id")+'" value="'+$(this).val()+'"/>');

                eForm.append(eSelectedField);
            });

        }

    });


}
campaignsBuilder.setup();

/*
 |----------------------------------------------------------------------------------------------------------------------
 | Campaign Options
 |----------------------------------------------------------------------------------------------------------------------
 */

/*-----------------------------------  Event Bindings ---------------------------------*/




/**
 * Remove Condition Button
 */
$("#campaignOptionsContainer").on("click",'.xx', function(e) {
    e.preventDefault();

    // Set the target to remove
    var target = $(this).parents('div.groupCondition');

    // Create Regex to extract Group Number And Condition Number
    var regex = /#group(\d+)Condition(\d+)/gi;
    match = regex.exec($(this).data('target'));
    var iGroupNo=match[1];
    var iConditionNo=match[2];

    // Find number of conditions in group
    var iConditionsInGroup=campaignsBuilder.countGroupConditions('group'+iGroupNo);

    // If only One condition remove
    if(iConditionsInGroup==1){
        //console.log('Found only one condition in group. Removing...')
        target.remove();
        return;
    }
    //console.log('#group'+iGroupNo+'Condition'+(iConditionNo));
    //If this is the last condition in group
    //console.log(iConditionsInGroup+'==='+iConditionNo);
    if(iConditionsInGroup==iConditionNo){

        //Operator to remove is the previous one

        var targetOperator=$(this).parents('div.groupCondition').prev('.groupConditionOperator');
        var op='previous';
    }
    else{
        //Else the next in dom
        var op='next';
        var targetOperator=$(this).parents('div.groupCondition').next('.groupConditionOperator');
    }
    //console.log(targetOperator);

    // if there is an operator remove it to
    if(targetOperator && typeof targetOperator!='undefined'){
        targetOperator.remove();
        //alert('target operator for '+'#group'+iGroupNo+'Condition'+(iConditionNo)+' is element with uid '+targetOperator.attr("id")+' ->'+op);
    }
    else{
        //alert('No target operator for '+'#group'+iGroupNo+'Condition'+(iConditionNo));
    }

    //Remove Target
    target.remove();




    //If condition removed not the last one reset all element ids
    if(iConditionsInGroup!=iConditionNo){
        campaignsBuilder.resetAllAfterConditionRemove('#group'+iGroupNo);
    }

});


$("#campaignRecipientsForm").on("click",'input[data-target=campaignRecipientsFormContainer]', function() {
     $("a#campaignRecipientsFromCsvUploader_browse").addClass("a_disabled");
});

$("#campaignRecipientsForm").on("click",'input[data-target=campaignRecipientsImporterContainer]', function() {
     $("a#campaignRecipientsFromCsvUploader_browse").removeClass("a_disabled");
});

/**
 * Remove Group Button
 */
$("#campaignOptionsContainer").on("click",'.removeGroupButton', function(e) {

    e.preventDefault();


    var target = $(this).data('target');

    //Total number of groups
    var groupsCounter=campaignsBuilder.countGroups();

    //Group Number
    var iGroupNo=campaignsBuilder.extractGroupNumberFromGroupId(target);

    //If group is the last one
    if(iGroupNo==groupsCounter){

        //The and or operator to remove with group is the previous one
        var targetOperator=$(this).parents('fieldset').prev('.groupOperator');

    }
    else{

        //else the next one
        var targetOperator=$(this).parents('fieldset').next('.groupOperator');

    }

    //If target element exists in dom remove it
    if($(target).length) $(target).detach();

    //If target operator element exists in dom remove it
    if($(targetOperator).length) $(targetOperator).detach();


    // Find all remaining groups except group No1
    var remainingGroups = $('.campaignConditionsGroup').find('fieldset[id^="group"]').not("#group1");

    //Foreach Remaing Group
    remainingGroups.each(function(index,value){

        var newGroupNo=index+2;
        var groupRemoveButton=campaignsBuilder.createRemoveGroupButton('group'+newGroupNo);

        //Change the id of the element to match ascending
        $(this).attr("id",'group'+newGroupNo);

        //Change the title and add remove button
        $(this).children('h2').html(groupTitle+' '+newGroupNo+groupRemoveButton);

        //For every div select and input element in the group fieldset
        $(this).find('div, select, input').each(function(){

            //Rename the id
            campaignsBuilder.renameElementProperty($(this),'id',/^group\d+/gi,/group\d+/g,"group"+newGroupNo);
            campaignsBuilder.renameElementProperty($(this),'id',/^select_condition\d+/gi,/select_condition\d+/g,"select_condition"+newGroupNo);

            //Rename the name
            campaignsBuilder.renameElementProperty($(this),'name',/^group\d+/gi,/group\d+/g,"group"+newGroupNo);
            campaignsBuilder.renameElementProperty($(this),'name',/^select_condition\d+/gi,/select_condition\d+/g,"select_condition"+newGroupNo);


        });


        //Find the condition remove buttons
        $(this).find('span.conditionRemove').each(function(){

            //campaignsBuilder.renameElementProperty($(this),'data-target',/^group\d+/gi,/group\d+/g,"group"+newGroupNo);


            //Replace  the data target property
            var attr = $(this).attr('data-target');
            if (typeof attr !== typeof undefined && attr !== false){
                if(/^#group\d+/gi.test(attr)) {

                    var newTarget=attr.replace(/group\d+/g,"group"+newGroupNo);
                    $(this).attr("data-target",newTarget);
                    $(this).data('target',newTarget)

                }
            }

        });

    });

});


/**
 *
 * @param sGroupId
 */
campaignsBuilder.resetAllAfterConditionRemove=function(sGroupId){
    console.log('Reseting...');
    var iConditionsInGroup=campaignsBuilder.countGroupConditions(sGroupId);
    var oConditionContainers=campaignsBuilder.findAllConditionContainersInGroup(sGroupId);
    var iContainerLength=oConditionContainers.length;

    oConditionContainers.each(function(index,value){
        sCurId=$(this).attr("id");
        sNewId=sGroupId+'Condition'+(index+1);
        var container=$(this);
        container.attr("id",sNewId);

        var oNextOperator=container.next('.groupConditionOperator');
        if(oNextOperator) oNextOperator.attr("id",sNewId+'OperatorContainer');

        container.find('input, select, span.conditionRemove.xx').each(function(key,val) {
            var sChildName=$(this).attr("name");
            if(sChildName){
                var sChildNewName=sChildName.replace(/Condition\d+/g,"Condition"+(index+1));
                $(this).attr("name",sChildNewName);

            }

            var spanChildTarget=$(this).data("target");

            if(spanChildTarget){

                var sChildNewTarget=spanChildTarget.replace(/Condition\d+/g,"Condition"+(index+1));

                $(this).attr("data-target",sChildNewTarget);
                $(this).data("target",sChildNewTarget);
            }

            var sChildId=$(this).attr("id");
            if(sChildId){
                var sChildNewId=sChildName.replace(/Condition\d+/g,"Condition"+(index+1));
                $(this).attr("id",sChildNewId);
            }




        });


    });

}



/**
 * Add Condition Select Box
 */
$('#campaignOptionsContainer').on('change', 'select[id^="select_condition"]',function(e) {

    var oOptionSelected = $("option:selected", this);
    var sMethodName = oOptionSelected.data('method');
    var sParentId = $(this).attr('id');
    var sGroup = $(this).parents('fieldset').attr('id');
    var iGroupId= parseInt(sGroup.replace("group", ""));
    var iConditionsInGroup=$(this).parents('fieldset').find('.groupCondition').length;

    if(sMethodName){
        campaignsBuilder.loadElementX(sMethodName,sParentId,iGroupId,iConditionsInGroup);
    }

    $(this).prop('selectedIndex',0);

});

/**
 * Add Group Button
 */
$( "a#addCampaignOptionsGroup" ).click(function(e) {
    e.preventDefault();
    var campaignId=$('input[name="iCampaignId"]').val();

    $.get( "/administration/API/Campaigns/getControl?methodname=getNewCampaignOptions&id="+campaignId, function( data ) {

        var campaignGroupsInDom=$('.campaignConditionsGroup fieldset[id^="group"]').length;
        var groupId='group'+(campaignGroupsInDom+1);

        var newGroup=campaignsBuilder.createNewGroupOfConditions(groupId,campaignGroupsInDom+1);
        var selectCondition=campaignsBuilder.createAddConditionControl(groupId+'ConditionAdd','small',data,campaignGroupsInDom+1);
        selectCondition.appendTo(newGroup);

        $oAndOrGroupOperator=campaignsBuilder.createAndOrGroupOperatorField(campaignGroupsInDom);
        $oAndOrGroupOperator.appendTo('.campaignConditionsGroup');
        newGroup.appendTo('.campaignConditionsGroup');

    });

});
/*----------------------------------  ./Event Bindings --------------------------------*/


/*--------------------------------  Create html Elements ------------------------------*/

/**
 * Add new Group container
 * @param sGroupId
 * @param iGroupId
 * @returns {*|jQuery|HTMLElement}
 */
campaignsBuilder.createNewGroupOfConditions=function(sGroupId,iGroupId){

    var newGroup=$('<fieldset id="'+sGroupId+'" class="conditionsGroup">');
    $('<h2>'+groupTitle+' '+iGroupId+' <span style="color:red">[</span> <a data-target-operator="#group'+(iGroupId-1)+'" data-target="#'+sGroupId+'" class="removeGroupButton" href="#"><i class="fa fa-times"></i></a> <span style="color:red">]</span></h2>').appendTo(newGroup);

    return newGroup;
}
/**
 * Add new conditions control select box
 * @param divContainerId
 * @param labelClass
 * @param conditionsControlData
 * @param groupId
 * @returns {*|jQuery|HTMLElement}
 */
campaignsBuilder.createAddConditionControl=function(divContainerId,labelClass,conditionsControlData,groupId){

    sNextConditionControlElId='select_condition'+groupId;

    var topContainer=$('<div id="'+divContainerId+'" class="npClear conditionAddControl clearfix">');

    var labelContainer=$('<label class="'+labelClass+'">');

    $('<span>'+conditionsControlData.label+'</span>').appendTo(labelContainer);

    var selectDivContainer=$('<div></div>');

    var sel=campaignsBuilder.createSelectBox(conditionsControlData.options,sNextConditionControlElId,'',sNextConditionControlElId);

    sel.appendTo(selectDivContainer);

    selectDivContainer.appendTo(labelContainer);

    labelContainer.appendTo(topContainer);

    return topContainer;

}

/**
 * Create select box
 * @param aOptions
 * @param sId
 * @param sParentId
 * @param sName
 * @param i
 * @returns {*|jQuery|HTMLElement}
 */
campaignsBuilder.createSelectBox = function(aOptions,sId,sParentId,sName,i,addContainer){

    var sDataParent=sParentId ?  'data-parent_id="'+sParentId+'"' : '';
    var sel = $('<select '+sDataParent+' class="resetAppearanceForSelect" id="'+sId+'" name="'+sName+'">');

    $(aOptions).each(function() {
        var sMethod=this.method?'data-method="'+this.method+'"':'';
        sel.append($("<option "+sMethod+">").attr('value',this.value).text(this.name));
    });

    if(addContainer){
        var oLabelDiv=$('<div>');

        sel.appendTo(oLabelDiv);

        return oLabelDiv;
    }
    return sel;
}

/**
 * Create the OR AND Operator Select Box
 * @param iGroupId
 * @param iConditionNumber
 * @returns {*|jQuery|HTMLElement}
 */
campaignsBuilder.createAndOrOperatorField=function(iGroupId,iConditionNumber){

    var sElementIdAttr='group'+iGroupId+'Condition'+(iConditionNumber-1)+'Operator';
    var sElementNameAttr='group'+iGroupId+'_Condition'+(iConditionNumber-1)+'_Operator';
    var sElementParentIdAttr='group'+iGroupId+'Condition'+(iConditionNumber-1);

    var oOperatorContainer=$('<div id="'+sElementIdAttr+'Container" class="groupConditionOperator npClear">');
    var oLabelContainer=$('<label class="small"></label>');
    var oInLabelDivContainer=$('<div>');


    var sel=campaignsBuilder.createSelectBox(campaingsAndOrOperatorData.options,sElementIdAttr,sElementParentIdAttr,sElementNameAttr,0);
    sel.appendTo(oInLabelDivContainer);
    oInLabelDivContainer.appendTo(oLabelContainer);
    oLabelContainer.appendTo(oOperatorContainer);

    return oOperatorContainer;


}

campaignsBuilder.createAndOrGroupOperatorField=function(iGroupId){

    var sElementIdAttr='group'+iGroupId+'Operator';
    var sElementNameAttr='group'+iGroupId+'Operator';
    var sElementParentIdAttr='group'+iGroupId ;

    var oOperatorContainer=$('<div  id="'+sElementIdAttr+'Container" class="groupOperator groupConditionOperator npClear clearfix">');
    var oLabelContainer=$('<label class="small"></label>');
    var oInLabelDivContainer=$('<div>');


    var sel=campaignsBuilder.createSelectBox(campaingsAndOrOperatorData.options,sElementIdAttr,sElementParentIdAttr,sElementNameAttr,0);
    sel.appendTo(oInLabelDivContainer);
    oInLabelDivContainer.appendTo(oLabelContainer);
    oLabelContainer.appendTo(oOperatorContainer);

    return oOperatorContainer;


}
/**
 * Create the button aside of group name that removes group on click
 * @param sTargetGroup
 * @returns {string}
 */
campaignsBuilder.createRemoveGroupButton=function(sTargetGroup){

    return ' <span style="color:red">[</span> <a  data-target="#'+sTargetGroup+'" class="removeGroupButton" href="#"><i class="fa fa-times"></i></a> <span style="color:red">]</span>';
}

/*--------------------------------  ./Create html Elements ------------------------------*/


/*--------------------------------  Ajax Loaders - Actions  ------------------------------*/

/**
 * Fetch the control via ajax
 * @param sMethodName
 * @param sParentId
 * @param iGroupId
 * @param iConditionsInGroup
 *
 * @todo 404 error code must be an http code
 */
campaignsBuilder.loadElementX=function(sMethodName,sParentId,iGroupId,iConditionsInGroup){

    $.get( "/administration/API/Campaigns/getControl?methodname="+sMethodName, function( data ){
        if(data!='404'){
            var iNumOfFields=data.fields.length;
            if(iNumOfFields>1){
                campaignsBuilder.addGroupedFields(data.fields,iGroupId,iConditionsInGroup+1);
            }else{
                campaignsBuilder.addField(data.fields,iGroupId,iConditionsInGroup+1);
            }

        }
    });
}
/*--------------------------------  ./Ajax Loaders - Actions  ------------------------------*/
/**
 *
 * @param sGroupId
 * @returns {*|jQuery}
 */
campaignsBuilder.findAllConditionContainersInGroup=function(sGroupId){

    return $('fieldset#'+sGroupId).find('.groupCondition');
}

/**
 *
 * @param sGroupId
 * @returns {Number|jQuery}
 */
campaignsBuilder.countGroupConditions=function(sGroupId){

    return $('fieldset#'+sGroupId).find('.groupCondition').length;
}

campaignsBuilder.countGroups=function(){

    return $('.campaignConditionsGroup').find('fieldset[id^="group"]').length;
}
/**
 * Add condition field
 * @param aFields
 * @param iGroupId
 * @param iConditionNumber
 */
campaignsBuilder.addField=function(aFields,iGroupId,iConditionNumber){
    if(iConditionNumber-1>0){
        // $('<div id="group'+iGroupId+'Condition'+(iConditionNumber-1)+'Operator"  class="groupConditionOperator npClear"><label class="small"><div><select class="resetAppearanceForSelect"><option value="AND">AND</option><option value="OR">OR</option></select></div></label></div>').appendTo('.campaignConditionsGroup fieldset#group'+iGroupId+'');
        var andOrOp=campaignsBuilder.createAndOrOperatorField(iGroupId,iConditionNumber);

        //console.log(andOrOp);
        andOrOp.appendTo('.campaignConditionsGroup fieldset#group'+iGroupId);
    }

    var oGroupContainer=$('<div id="group'+iGroupId+'Condition'+iConditionNumber+'" class="groupCondition npClear">');
    $(aFields).each(function(key,field){
        var sClass='halfsize';
        var oLabelContainer1=$('<label class="'+sClass+'">');
        $('<span  data-target="#group'+iGroupId+'Condition'+iConditionNumber+'" class="conditionRemove xx" title="Remove Condition"><i class="fa fa-times"></i></span>').appendTo(oLabelContainer1);
        $('<span class="labelTitleNoClear fw">'+field.label+'</span>').appendTo(oLabelContainer1);
        var oSelectContainer=$('<div>');
        if(field.type=='select'){
            var oSelectBox=campaignsBuilder.createSelectBox(field.options,'group'+iGroupId+'Condition'+iConditionNumber+field.name,null,'group'+iGroupId+'_Condition'+iConditionNumber+'_'+field.name,key);
            oSelectBox.appendTo(oSelectContainer);

        }
        else if(field.type=='text'){
            $('<input type="text" name="group'+iGroupId+'_Condition'+iConditionNumber+'_'+field.name+'" class="'+sClass+'"  placeholder="'+field.label+'">').appendTo(oSelectContainer);
        }
        oSelectContainer.appendTo(oLabelContainer1);
        oLabelContainer1.appendTo(oGroupContainer);

        oGroupContainer.appendTo('.campaignConditionsGroup fieldset#group'+iGroupId+'');

        //var added = campaignsBuilder.appendToForm();

    });
}

/**
 * Add condition grouped fields
 * @param aFields
 * @param iGroupId
 * @param iConditionNumber
 *
 * @todo Merge with addField (no repetition)
 */
campaignsBuilder.addGroupedFields=function(aFields,iGroupId,iConditionNumber){
    //Group Number
    //Previous Condition Number in group
    // console.log(iGroupId+"-"+iConditionNumber);
    //console.log(aFields);
    if(iConditionNumber-1>0){
        // $('<div id="group'+iGroupId+'Condition'+(iConditionNumber-1)+'Operator"  class="groupConditionOperator npClear"><label class="small"><div><select class="resetAppearanceForSelect"><option value="AND">AND</option><option value="OR">OR</option></select></div></label></div>').appendTo('.campaignConditionsGroup fieldset#group'+iGroupId+'');
        var andOrOp=campaignsBuilder.createAndOrOperatorField(iGroupId,iConditionNumber);
        andOrOp.appendTo('.campaignConditionsGroup fieldset#group'+iGroupId+'');
    }
    var oGroupContainer=$('<div id="group'+iGroupId+'Condition'+iConditionNumber+'" class="groupCondition npClear">');
    $(aFields).each(function(key,field){
        var sClass=field.type=='select' ? 'small' : 'small number';
        var sLabelClass=field.type=='select' ? 'small' : 'label_smaller';
        var oLabelContainer1=$('<label class="'+sLabelClass+'">');
        if(key==0){
            $('<span  data-target="#group'+iGroupId+'Condition'+iConditionNumber+'" class="conditionRemove xx" title="Remove Condition"><i class="fa fa-times"></i></span>').appendTo(oLabelContainer1);
            $('<span class="labelTitleNoClear fw">'+field.label+'</span>').appendTo(oLabelContainer1);
        }
        var oSelectContainer=$('<div>');
        if(field.type=='select'){
            var oSelectBox=campaignsBuilder.createSelectBox(field.options,'group'+iGroupId+'Condition'+iConditionNumber+field.name,null,'group'+iGroupId+'_Condition'+iConditionNumber+'_'+field.name,key);
            oSelectBox.appendTo(oSelectContainer);

        }
        else if(field.type=='text'){
            $('<input required type="text" name="group'+iGroupId+'_Condition'+iConditionNumber+'_'+field.name+'" class="'+sClass+'"  placeholder="'+field.label+'">').appendTo(oSelectContainer);
        }
        oSelectContainer.appendTo(oLabelContainer1);
        oLabelContainer1.appendTo(oGroupContainer);


    });
    //console.log(oGroupContainer);
    oGroupContainer.appendTo('.campaignConditionsGroup fieldset#group'+iGroupId+'');


}

/*
 |--------------------------------------------------------------------------
 | Helpers
 |--------------------------------------------------------------------------
 */

/**
 *
 * @param oEl
 * @param sAttr
 * @param sRegexToMatch
 * @param sOldValue
 * @param sNewValue
 */
campaignsBuilder.renameElementProperty=function(oEl,sAttr,sRegexToMatch,sOldValue,sNewValue){

    var attr = oEl.attr(''+sAttr+'');

    if (typeof attr !== typeof undefined && attr !== false) {

        if(sRegexToMatch.test(attr)) {

            var newProp=attr.replace(sOldValue,sNewValue);
            oEl.attr(""+sAttr+"",newProp);
            //console.log(sAttr);
            /*if(/data\-/gi.test(sAttr)){
             alert(sAttr);
             var newDataProp=attr.replace(/data-\d+/g,"");
             oEl.data(newDataProp,newProp);
             }
             */
        }


    }
}

/**
 *
 * @param sGroupId
 * @returns {*}
 */
campaignsBuilder.extractGroupNumberFromGroupId=function(sGroupId){

    var regex = /#group(\d+)/gi;
    match = regex.exec(sGroupId);
    return match[1];
}

$(function(){
    $("#campaignOptionsForm").submit(function(e){

        e.preventDefault();
        $("button.storeCampaignOptions").find('i').removeClass('fa-save').addClass('fa-circle-o-notch fa-spin');
        var campaignData=$("#campaignOptionsForm").serialize();
        $.post("/administration/API/Campaigns/handleCampaignOptions", campaignData,
            function(data){

                if(data!==null){

                    if(data.errors!==undefined && data.errors.length>0 ){
                        $('#sysMsg').html('');
                        for (var i = 0; i < data.errors.length; i++) {

                            $('#sysMsg').append('<div class="campaignOptionsValidationError">'+data.errors[i]+'</div>');

                        }

                    }
                    else{
                        $('#sysMsg').html('<div class="campaignOptionsValidationSuccess">'+data+'</div>');
                    }

                }
                $("button.storeCampaignOptions").find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-save');
            },"json");
    });

    /*
     |--------------------------------------------------------------------------
     | Promotion Setup
     |--------------------------------------------------------------------------
     */
    $('#storeCampaignSetup').click(function(e){
        $('form#campaigns').submit();
    });
    /*
     |--------------------------------------------------------------------------
     | Redemption Save
     |--------------------------------------------------------------------------
     */
    $("#campaignRedemptionForm").submit(function(e){

        e.preventDefault();
        $("button.storeCampaignRedemption").find('i').removeClass('fa-save').addClass('fa-circle-o-notch fa-spin');
        var campaignRedemtpionData=$("#campaignRedemptionForm").serialize();
        $.post("/administration/API/Campaigns/handleCampaignRedemption", campaignRedemtpionData,
            function(data){
                if(data!==null){

                    if(data.errors!==undefined){
                        $('#redemptionMsg').html('');
                        if(validationMessagesShowAll==1){

                            for (var i = 0; i < data.errors.length; i++) {

                                $('#redemptionMsg').append('<div class="campaignOptionsValidationError">'+data.errors[i]+'</div>');

                            }

                        }
                        else{
                            $('#redemptionMsg').append('<div class="campaignOptionsValidationError">'+data.errors[0]+'</div>');
                        }


                    }
                    else{
                        $('#redemptionMsg').html('<div class="campaignOptionsValidationSuccess">'+data+'</div>');
                    }

                }
                $("button.storeCampaignRedemption").find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-save');
            },"json");
    });

    /*
     |--------------------------------------------------------------------------
     | Redemption check available
     |--------------------------------------------------------------------------
     */
    var responseIcons=Array('<span class="checkResultSuccess"><i class="fa fa-check fa-fw"></i> Available</span>','<span class="checkResultError"><i class="fa fa-times fa-fw"></i> In use</span>');
    var successIcon='<span class="checkResultSuccess"><i class="fa fa-check fa-fw"></i> Available</span>';
    var failureIcon='<span class="checkResultError"><i class="fa fa-times fa-fw"></i> In use</span>';
    $('.checkOnType').keyup(function() {
        if($(this).val().length < 3 || $(this).val().length >12){
            $(this).closest('div').find('.checkOnTypeResult').html('<span class="checkResultError">Code length must be from 3 to 12 characters</span>');
            return false;
        }

        if (/1|0|t|I|o|d|j/.test($(this).val())){
            $(this).closest('div').find('.checkOnTypeResult').html('<span class="checkResultError">Code includes invalid characters</span>');
            return false;
        }
        var item = responseIcons[Math.floor(Math.random()*responseIcons.length)];
        //clearTimeout(thread);
        //var $this = $(this); thread = setTimeout(function(){findMember($this.val())}, 1000);
        if($(this).val().length>2){
            var searchForPrefix = $(this).data('prefix') && $(this).data('prefix')==1 ? $(this).data('prefix') : 0;

            var promoElement=$(this);
            var promoCodeToSearch=$(this).val();
            $.post("/administration/API/Campaigns/searchPromoCode", { promoCode: promoCodeToSearch,prefixOnly:searchForPrefix },
                function(data){
                    if(data!==null){

                        if(data.errors!==undefined){
                            promoElement.closest('div').find('.checkOnTypeResult').html(failureIcon);

                        }
                        else{
                            promoElement.closest('div').find('.checkOnTypeResult').html(successIcon);
                        }

                    }

                },"json");

        }

    });

    /*
     |--------------------------------------------------------------------------
     | Redemption Tags autocomplete
     |--------------------------------------------------------------------------
     */
    var a = $('.campaignTags').autocomplete({
        serviceUrl:'/administration/API/Campaigns/searchTags',
        minChars:2,
        //delimiter: /(,|;)\s*/, // regex or character
        maxHeight:400,
        width:300,
        zIndex: 9999,
        deferRequestBy: 10, //miliseconds
        //params: { country:'Yes' }, //aditional parameters
        noCache: false, //default is false, set to true to disable caching
        onSelect: function(value){

            $.post("/administration/API/Campaigns/searchTaggedCampaigns", { query: value.data },
                function(data){
                    if(data!==null){

                        var options = $("#campaign_names");
                        //Remove all options of drop down
                        options.find('option').remove().end();

                        //Append each result
                        $.each(data.campaigns, function() {
                            options.append($("<option />").val(this.id).text(this.name));
                        });

                        //Select all
                        $('#campaign_names option').prop('selected', true);
                    }

                },"json");
        }
        // local autosugest options:
        //lookup: ['Tag1', 'qweqweqwe', 'jkhjkhj', 'sfsfsdf', 'asdasd'] //local lookup values
    });

    /*
     |--------------------------------------------------------------------------
     | Affiliates Auto complete
     |--------------------------------------------------------------------------
     */
    var aff = $('.campaignRedemptionAffiliate').autocomplete({
        serviceUrl:'/administration/API/Campaigns/searchAffiliates',
        minChars:1,
        //delimiter: /(,|;)\s*/, // regex or character
        maxHeight:400,
        width:300,
        zIndex: 9999,
        deferRequestBy: 10, //miliseconds
        //params: { country:'Yes' }, //aditional parameters
        noCache: false
    });

    /*
     |--------------------------------------------------------------------------
     | Redemption Toggle disabled in promo code names or prefix
     | Cant do both
     |--------------------------------------------------------------------------
     */
    $( "#promo_code" ).blur(function() {
        if($(this).val()){
            $('#multiple_promo_codes').attr('disabled',true);
            $('#number_of_promo_codes').attr('disabled',true);
        }
        else{
            $('#multiple_promo_codes').attr('disabled',false);
            $('#number_of_promo_codes').attr('disabled',false);
        }

    });

    $( "#multiple_promo_codes" ).blur(function() {
        if($(this).val()){
            $('#promo_code').attr('disabled',true);

        }
        else{
            $('#promo_code').attr('disabled',false);

        }

    });
    /*
     |--------------------------------------------------------------------------
     | Redemption checkbox enable use of campaign in conjunction with others
     |--------------------------------------------------------------------------
     */
    var relStat=$( "#use_in_conjunction" ).val() >0?false:true;
    $('#campaign_tags').attr('disabled',relStat);
    $('#campaign_names').attr('disabled',relStat);

    $('#use_in_conjunction').click(function(){
        $('#campaign_tags').attr('disabled',!this.checked);
        $('#campaign_names').attr('disabled',!this.checked);
    });




});

/*
 |--------------------------------------------------------------------------
 | Campaign Recipients
 |--------------------------------------------------------------------------
 */
campaignsBuilder.recipients={
    setup:function(){



        if($('#segmentId').length){
            campaignsBuilder.recipients.loadSegment($('#segmentId').val());
        }


        $('.recipientsSection').on('focus',".datepicker", function(){
            $(this).datepicker({ dateFormat: 'yy-mm-dd'});
        });
        /*
        Add hover effect on hover on section
         */
        $( "section.recipientsSection" )
            .mouseover(function() {
                $(this).addClass('hovered');
            })
            .mouseout(function() {
                $(this).removeClass('hovered');
            });

        /*
        Add remove condition bindings
         */
        $("#campaignRecipientsForm").on("click",'.xx', function(e) {
            e.preventDefault();
            target=$(this).data('target');
            $(target).remove();
            $(target).closest('br').remove();
            var iConditionsInForm=campaignsBuilder.recipients.countFormElementsInContainer('campaignRecipientsConditions','groupCondition');
            if(iConditionsInForm==1){
                $('#matchPlayersOperatorContainer').remove();
            }
        });

        /*
        Section activate radio button binding
         */
        $('#campaignRecipientsForm').on("click",'input[name="campaignRecipientsChoiceOfEntry"]', function() {
            var sTarget = $(this).data('target');
            campaignsBuilder.recipients.toggleActiveSections(sTarget);
            if($(this).val()==2){
                $('#voodooPeople').removeClass('voodooPeople');
            }
            else{
                $('#voodooPeople').addClass('voodooPeople');
            }
        });


        /*
        Add new condition binding
         */
        $("#campaignRecipientsForm").on("click",'.addRecipientsSegmentCondition', function(e) {
            e.preventDefault();
            campaignsBuilder.recipients.loadSegmentConditionFormElement('getNewCampaignRecipients');
        });
        /*
        Load segment binding
         */
        $('#campaignRecipientsForm').on("change",'#loadRecipientsTemplate', function(e) {
            e.preventDefault();

            var iSegmentId=$(this).val();
            campaignsBuilder.recipients.loadSegment(iSegmentId);

        });

        /*
        Load parent condition parameters (children) elements
         */
        $("#campaignRecipientsForm").on("change",'select[name^="master_switch"]', function(e) {
            e.preventDefault();
            var theVal=$(this).val();
            var theEl=$(this);
            var theName=$(this).attr('name');
            var countChildren=theEl.parents('div.groupCondition').find('label').length;

            if(theVal && countChildren==1){
                $.get( "/administration/API/Campaigns/getControl?methodname=getConditionControls&field="+theVal, function( data ){
                    // data={"fields":{"date_operator_switch":{"type":"select","name":"date_operator_switch","value":0,"label":"","labelClass":"small","class":"resetAppearanceForSelect","options":[{"name":"Is after","value":">"},{"name":"Is before","value":"<"},{"name":"Is","value":"="}],"inGroup":false,"closeGroup":false},"date_picker":{"type":"text","name":"date_picker","value":0,"label":"","labelClass":"label_smaller","class":"small number","inGroup":false,"closeGroup":false}}};

                    if(data!='404'){//

                        $.each(data.fields, function(key, field) {
                            //console.log(key);
                            //console.log(field.type);
                            //console.log('--------------------');
                            if(field.type=='select'){
                                var oLabel=$('<label class="'+field.labelClass+'">');
                                var oSelect=$('<select name="'+theName+'_'+field.name+'" class="'+field.class+'">');
                                $(field.options).each(function() {

                                    oSelect.append($("<option>").attr('value',this.value).text(this.name));
                                });
                                oSelect.appendTo(oLabel);
                                var k = theEl.parents('div.groupCondition');
                                oLabel.appendTo(k);
                            }
                            if(field.type=='text'){
                                var oLabel=$('<label class="'+field.labelClass+'">');
                                var oText=$('<input type="text" name="'+theName+'_'+field.name+'" placeholder="'+field.placeholder+'" class="'+field.class+'">');

                                oText.appendTo(oLabel);
                                var k = theEl.parents('div.groupCondition');
                                oLabel.appendTo(k);
                            }
                        });
                        var theLbl=theEl.find("option:selected").text();
                        theEl.html('<option value="'+theVal+'" selected>'+theLbl+'</option>');


                    }
                });
            }

        });
    }
};
campaignsBuilder.recipients.toggleActiveSections=function(activeElementId){
    $('form#campaignRecipientsForm').find('section').removeClass('active');
    $('form#campaignRecipientsForm').find('section').find('select,textarea,button,input[type="text"],input[type="file"]').attr("disabled", false);
    $('form#campaignRecipientsForm').find('section').not('#'+activeElementId).find('select,button,textarea,input[type="text"],input[type="file"]').attr("disabled", "disabled");
    $('section#'+activeElementId).addClass('active');
}
campaignsBuilder.recipients.loadSegmentConditionFormElement=function(sMethodName){

    $.get( "/administration/API/Campaigns/getControl?methodname="+sMethodName, function( data ){
        if(data!='404'){//
            //var iNumOfFields=data.fields.length;

            campaignsBuilder.recipients.addSegmentConditionFormElement(data,'#campaignRecipientsConditions');

        }
    });
}
/**
 *
 * @param aOptions
 * @param sName
 * @param sTarget
 * @param sValue
 */
campaignsBuilder.recipients.createPlayersMatchControl=function(aOptions,sName,sTarget,sValue){

    var oSelectBoxMatchControl=campaignsBuilder.createSelectBox(aOptions,sName,null,sName,null,true);
    if(sValue){
        oSelectBoxMatchControl.val(sValue);
    }
    var oMatchControlContainer=$('<div id="matchPlayersOperatorContainer" class="conditionsProperty">');
    var oLabelControl=campaignsBuilder.recipients.createLabelofElement('small','Players match');

    oSeperator=$('<div class="optionsSeparator clear"></div>');

    campaignsBuilder.recipients.composeElementBlock($(sTarget),oMatchControlContainer,oLabelControl,oSelectBoxMatchControl,true);
    oMatchControlContainer.append(oSeperator);
}
/**
 *
 * @param field
 * @param target
 */
campaignsBuilder.recipients.addSegmentConditionFormElement=function(field,target){

    var iConditionsInForm=campaignsBuilder.recipients.countFormElementsInContainer('campaignRecipientsConditions','groupCondition');
    console.log('Conditions in Form '+iConditionsInForm);

    if(iConditionsInForm==1){
        //getRecipientsPlayersMatchControl
        $.get( "/administration/API/Campaigns/getControl?methodname=getRecipientsPlayersMatchControl", function( data ){
            if(data!='404'){//
                campaignsBuilder.recipients.createPlayersMatchControl(data.options,data.name,target);
            }
        });

    }

    var sElementClass=field.class;
    var sElementslabelClass=field.labelClass;

    //var oLabelContainer1=$('<label class="'+sElementslabelClass+'"></label>');
    var oContainer=$('<div class="groupCondition conditionAddControl npClear" id="recipientsGroupCondition'+iConditionsInForm+'"></div>');
    //$('<span class="labelTitleNoClear fw">'+field.label+'</span>').appendTo(oLabelContainer1);
    var oSelectContainer=campaignsBuilder.recipients.createLabelofElement(sElementslabelClass,field.label,true,'recipientsGroupCondition'+iConditionsInForm);
    if(field.type=='select'){
        var oSelectBox=campaignsBuilder.createSelectBox(field.options,field.name+'_'+iConditionsInForm,null,field.name+'_'+iConditionsInForm,null,true);
        oSelectBox.appendTo(oSelectContainer);

    }
    else if(field.type=='text'){
        $('<input type="text" name="group'+iGroupId+'_Condition'+iConditionNumber+'_'+field.name+'_'+iConditionsInForm+'" class="'+sClass+'"  placeholder="'+field.placeholder+'">').appendTo(oSelectContainer);
    }
   // oSelectContainer.appendTo(oLabelContainer1);

    oSelectContainer.appendTo(oContainer);

    oContainer.appendTo(target);
    //console.log(oLabelContainer1);
    // });
}

/**
 * Count Elements by class in container by id
 * @todo can be generic
 * @param sContainerId
 * @param sElementClass
 * @returns {Number|jQuery}
 */
campaignsBuilder.recipients.countFormElementsInContainer=function(sContainerId,sElementClass){
    return $('#'+sContainerId).find('.'+sElementClass).length;
}

/**
 * Create elements label as a container
 * (based on existing max lotto markup)
 * @param sLabelStyleClass
 * @param sLabelTitleText
 * @param bLabelAddRemovalButton
 * @param sRemovalButtonTarget
 * @returns {*|jQuery|HTMLElement}
 */
campaignsBuilder.recipients.createLabelofElement=function(sLabelStyleClass,sLabelTitleText,bLabelAddRemovalButton,sRemovalButtonTarget){

    var oLabelMatchControl=$(' <label class="'+sLabelStyleClass+'"></label>');
    var oLabelMatchControlTitle= sLabelTitleText!='' ? $('<span class="labelTitleNoClear fw">'+sLabelTitleText+'</span>') : '';
    if(bLabelAddRemovalButton){
        var oLabelMatchControlRemovalButton=$('<span data-target="#'+sRemovalButtonTarget+'" class="conditionRemove xx" title="Remove Condition"><i class="fa fa-times"></i></span>');
        oLabelMatchControlRemovalButton.appendTo(oLabelMatchControl);
    }
    if(oLabelMatchControlTitle!==''){
        oLabelMatchControlTitle.appendTo(oLabelMatchControl);
    }

    return oLabelMatchControl
}

campaignsBuilder.recipients.createFormElement=function(theElement,elNamePrefix){
    //console.log(theElement);
   switch(theElement.type){
       case 'select':
           var theFormField=campaignsBuilder.createSelectBox(theElement.options,theElement.name,null,theElement.name);

           theFormField.val(theElement.value);
           if (elNamePrefix){
               var theName=theFormField.attr("name");
               var theNewName=elNamePrefix+'_'+theName;
               theFormField.attr("name",theNewName);
           }

           var theLabel=campaignsBuilder.recipients.createLabelofElement(theElement.labelClass,'',false,null);
           return theLabel.append(theFormField);
           break;
      default:
           var theFormField=$('<input value="'+theElement.value+'" type="'+theElement.type+'" class="'+theElement.class+'" name="'+theElement.name+'" placeholder="'+theElement.placeholder+'">')
          if (elNamePrefix){
              var theName=theFormField.attr("name");
              var theNewName=elNamePrefix+'_'+theName;
              theFormField.attr("name",theNewName);
          }
          var theLabel=campaignsBuilder.recipients.createLabelofElement(theElement.labelClass,'',false,null);
           return theLabel.append(theFormField);
           break;
   }
}
campaignsBuilder.recipients.loadSegment=function(iSegmentId){
    if(isNumeric(iSegmentId)){
        if($('#segmentName').length){
            $('#recipientsSegmentName').val($('#segmentName').val());
        }
        $.get( "/administration/API/Campaigns/loadRecipientSegment?segment_id="+iSegmentId, function( data ){
            var parent=$('fieldset#campaignRecipientsConditions');
            parent.html('');
            if(data!='404'){//
                //console.log(data);
                //console.log(isNumeric(iSegmentId));
                $.each(data, function(key, maxEl) {
                    var itemsInResponse=data.length;
                    //console.log(key+'_______________________________________________________');
                    $.each(maxEl,function(innerKey,f){
                        if(innerKey=='players_match'){
                            if(itemsInResponse>2)
                                campaignsBuilder.recipients.createPlayersMatchControl(f.options, f.name,'#campaignRecipientsConditions', f.value);
                        }
                        else{
                            var elname='';
                            $.each(f.options,function(){
                                elname=this.name;
                            });

                            var iConditionsInForm=campaignsBuilder.recipients.countFormElementsInContainer('campaignRecipientsConditions','groupCondition');
                            var childrenNamePrefix='master_switch_'+iConditionsInForm;
                            var oLabel=campaignsBuilder.recipients.createLabelofElement('small','Select',true,'');


                            var k =campaignsBuilder.createSelectBox([{"name":""+elname+"","value":""+innerKey+""}],'master_switch_'+iConditionsInForm,null,'master_switch_'+iConditionsInForm,null,true);
                            var oContainer=campaignsBuilder.recipients.createBlockContainerOfElement();

                            campaignsBuilder.recipients.composeElementBlock(parent,oContainer,oLabel,k);

                            $.each(f.fields,function(indx,field){
                                theEl=campaignsBuilder.recipients.createFormElement(field,childrenNamePrefix);
                                oContainer.append(theEl);
                            });
                        }


                    });
                });
                // alert('Segment Loaded');

            }
        },"json");
    }
}
/**
 *
 * @returns {*|jQuery|HTMLElement}
 */
campaignsBuilder.recipients.createBlockContainerOfElement=function(){
    var iConditionsInForm=campaignsBuilder.recipients.countFormElementsInContainer('campaignRecipientsConditions','groupCondition');
    var oContainer=$('<div class="groupCondition conditionAddControl npClear" id="recipientsGroupCondition'+iConditionsInForm+'"></div>');
    return oContainer;
}

/**
 *
 * @param oTheContainer
 * @param oTheElContainer
 * @param oTheLabel
 * @param oTheFormField
 * @param prepend
 * @todo Data target must be replaced in separate object
 */
campaignsBuilder.recipients.composeElementBlock=function(oTheContainer,oTheElContainer,oTheLabel,oTheFormField,prepend){

    var labelRemoveTarget=oTheElContainer.attr("id");

    newLabelHtml = oTheLabel.html().replace('data-target="#"', 'data-target="#'+labelRemoveTarget+'"');
    oTheLabel.html(newLabelHtml);

    var step1 = oTheLabel.append(oTheFormField);
    var step2 = oTheElContainer.append(step1);
    if(prepend){
        oTheContainer.prepend(step2);
    }
    else{
        step2.appendTo(oTheContainer);
    }

}
/*
 |--------------------------------------------------------------------------
 | Recipients Save
 |--------------------------------------------------------------------------
 */
$("#campaignRecipientsForm").submit(function(e){

    e.preventDefault();
    $("#campaignRecipientsForm button.storeCampaignRedemption").find('i').removeClass('fa-save').addClass('fa-circle-o-notch fa-spin');
    var campaignRecipientsData=$("#campaignRecipientsForm").serialize();
    $.post("/administration/API/Campaigns/handleCampaignRecipients", campaignRecipientsData,
        function(data){
            if(data!==null){
                window.scrollTo(0, 0);
                if(data.errors!==undefined){
                    $('#recipientsMsg').html('');
                    if(validationMessagesShowAll==1){

                        for (var i = 0; i < data.errors.length; i++) {

                            $('#recipientsMsg').append('<div class="campaignOptionsValidationError">'+data.errors[i]+'</div>');

                        }

                    }
                    else{
                        $('#recipientsMsg').append('<div class="campaignOptionsValidationError">'+data.errors[0]+'</div>');
                    }


                }
                else{
                    $('#recipientsMsg').html('<div class="campaignOptionsValidationSuccess">'+data+'</div>');
                }

            }
            $("#campaignRecipientsForm button.storeCampaignRedemption").find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-save');
        },"json");
});
/*
 |--------------------------------------------------------------------------
 | Content Save
 |--------------------------------------------------------------------------
 */
$("#campaignPageContentForm").submit(function(e){

    e.preventDefault();
    $("#campaignPageContentForm button.storeCampaignRedemption").find('i').removeClass('fa-save').addClass('fa-circle-o-notch fa-spin');
    var campaignContentData=$("#campaignPageContentForm").serialize();
    $.post("/administration/API/Campaigns/handleCampaignContentPage", campaignContentData,
        function(data){
            if(data!==null){
                window.scrollTo(0, 0);
                if(data.errors!==undefined){
                    $('#pageContentMsg').html('');
                    if(validationMessagesShowAll==1){

                        for (var i = 0; i < data.errors.length; i++) {

                            $('#pageContentMsg').append('<div class="campaignOptionsValidationError">'+data.errors[i]+'</div>');

                        }

                    }
                    else{
                        $('#pageContentMsg').append('<div class="campaignOptionsValidationError">'+data.errors[0]+'</div>');
                    }


                }
                else{
                    $('#pageContentMsg').html('<div class="campaignOptionsValidationSuccess">'+data+'</div>');
                }

            }
            $("#campaignPageContentForm button.storeCampaignRedemption").find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-save');
        },"json");
});


(function(){
    campaignsBuilder.recipients.setup();

    /*
    |--------------------------------------------------------------------------
    | Recipients CSV Upload
    |--------------------------------------------------------------------------
    */
    new AjaxUpload('campaignRecipientsCsvUpload', {
        action: '/administration/API/Campaigns/handleCampaignRecipients?id='+$('input[name="campaign_id"]').val(),
        name: 'file',
        data:{'mimetype':'csv'},
        responseType:'json',
        onSubmit: function(file, extension) {

            $('#importRecipientsFromCsv').find('i').removeClass('fa-list').addClass('fa-circle-o-notch fa-spin');
        },
        onComplete: function(file, response) {
            rcptLog(response,'recipientsMsg');
            $('#importRecipientsFromCsv').find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-list');
        }
    });
    /*
    |--------------------------------------------------------------------------
    | Campaign Content Image
    |--------------------------------------------------------------------------
    */
    new AjaxUpload('campaignPageContentOnPageImageUploader', {
        action: '/administration/API/Campaigns/handleCampaignContentPage?id='+$('input[name="campaign_id"]').val(),
        name: 'file',
        data:{'mimetype':'image','type':'image'},
        responseType:'json',
        onSubmit: function(file, extension) {

            $('#campaignPageContentOnPageImageUploaderContainer').find('i').removeClass('fa-image').addClass('fa-circle-o-notch fa-spin');
        },
        onComplete: function(file, response) {

            $('#campaignPageContentOnPageImageUploaderContainer').find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-image');
            var cont = $('#campaignPageContentOnPageImageUploaderContainer  div.checkOnTypeResult');
            cont.html('');
            if(response.error && response.error!==null){
                rcptLog(response,'pageContentMsg');
            }
            else{
                var img = $('<img src="'+response.imageUrl+'?'+Math.random()+'" height="40">');
                cont.append(img);
                rcptLog(response,'pageContentMsg');
            }


        }
    });

    /*
     |--------------------------------------------------------------------------
     | Campaign Content Icon
     |--------------------------------------------------------------------------
     */
    new AjaxUpload('campaignPageContentOnPageIconUploader', {
        action: '/administration/API/Campaigns/handleCampaignContentPage?id='+$('input[name="campaign_id"]').val(),
        name: 'file',
        data:{'mimetype':'image','type':'icon'},
        responseType:'json',
        onSubmit: function(file, extension) {

            $('#campaignPageContentOnPageIconUploaderContainer').find('i').removeClass('fa-image').addClass('fa-circle-o-notch fa-spin');
        },
        onComplete: function(file, response) {

            $('#campaignPageContentOnPageIconUploaderContainer').find('i').removeClass('fa-circle-o-notch fa-spin').addClass('fa-image');
            var cont = $('#campaignPageContentOnPageIconUploaderContainer  div.checkOnTypeResult');
            cont.html('');
            if(response.error && response.error!==null){
                rcptLog(response,'pageContentMsg');
            }
            else{
                var img = $('<img src="'+response.imageUrl+'?'+Math.random()+'" height="40">');
                cont.append(img);
                rcptLog(response,'pageContentMsg');
            }


        }
    });
    /**
     *
     * @param strX
     */
    function rcptLog(strX,target) {
        var log = $('#'+target);

        if(strX.error){
            var container=$('<div class="campaignOptionsValidationError">'+strX.error.message+'</div>');
        }
        else{
            var container=$('<div class="campaignOptionsValidationSuccess">'+strX.msg+'</div>');
        }

        log.html('');
        log.append(container);
        $('html, body').animate({
            scrollTop: $("#contentarea").offset().top
        }, 2000);
        //log.scrollTop(log[0].scrollHeight);
    }

})();

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}













