<?php

class scraper extends \LL\AjaxController{
    var $oLottery=null;

    function getData($sURL,$bUseCache=true){
        $sFileName=base64_encode($sURL);
        $sCachePath='H:\\sites\\lldata\\';
        if(!file_exists($sCachePath.$sFileName) || $bUseCache==false){
            $sData=file_get_contents($sURL);
            file_put_contents($sCachePath.$sFileName, $sData);
            return $sData;
        }
        return file_get_contents($sCachePath.$sFileName);
    }

    function runXPath($oDoc,$sXPath){
        $xpath = new DOMXpath($oDoc);
        $elements = $xpath->query($sXPath);
        return $elements;
    }

    function getDrawPages(){

    }

    function locateDraw($iLotteryID,$dDate){
        $sSQL='SELECT id FROM lottery_draws WHERE fk_lottery_id='.$iLotteryID.' AND date(fk_lotterydate)=\''.$dDate.'\'';
        $iLotteryDrawID=\DAL::executeGetOne($sSQL);
        if($iLotteryDrawID==0){
            //Create a draw
            $aData=array('fk_lottery_id'=>$iLotteryID,'fk_lotterydate'=>$dDate.' '.$this->oLottery->sLottoDrawTime);
            $iLotteryDrawID=\DAL::Insert('lottery_draws', $aData,true);
        }
        return $iLotteryDrawID;
    }
}