<?php

class Megamillions_ScraperApi extends scraper{

    function __construct() {
        set_time_limit(0);
        $this->oLottery=new USAMegaMillions();
    }

    function scrape(){
        /**
         * Get the first date that we have no results for
         */
        $sSQL='SELECT MIN(fk_lotterydate) FROM lottery_draws WHERE fk_lottery_id=4 AND fk_lotterydate<NOW() AND (numbers=NULL OR winnings=NULL) ';
        $sNextDrawDate=\DAL::executeGetOne($sSQL);
        if($sNextDrawDate==null){
            return false;
        }
        $dNextDrawDate=strtotime(date('Y-m-d',strtotime($sNextDrawDate))).' 00:00:00';

        $sBallsData=$this->getData('http://www.calottery.com/sitecore/content/Miscellaneous/download-numbers/?GameName=mega-millions',false);

        $aDraws=explode("\n",$sBallsData);

        //Trim off the header rows (5 of them)
        array_shift($aDraws);
        array_shift($aDraws);
        array_shift($aDraws);
        array_shift($aDraws);
        array_shift($aDraws);

        array_pop($aDraws);

        foreach($aDraws as $sDraw){
            //Collapse spaces as the number of sapces isn't always the same
            for($a=0;$a<10;$a++){
                $sDraw=str_replace('   ','  ',$sDraw);
            }
            $aParts=explode('  ',$sDraw);

            //Parse the date
            $sDrawNumber=$aParts[0];
            $sDate=date('Y-m-d',strtotime($aParts[1]));

            if(strtotime($sDate)>=$dNextDrawDate){

                //Find the draw to update
                $iDrawId=$this->locateDraw($this->oLottery->iLotteryID, $sDate);

                //Create a results object to lodge the data with correctly
                $oResult=new \LL\Results\LotteryResult($iDrawId,$this->oLottery);
                $oResult->sLotteryDate=$sDate;

                //Scrape out the balls
                $oResult->oDrawBalls->addBall($aParts[2]);
                $oResult->oDrawBalls->addBall($aParts[3]);
                $oResult->oDrawBalls->addBall($aParts[4]);
                $oResult->oDrawBalls->addBall($aParts[5]);
                $oResult->oDrawBalls->addBall($aParts[6]);
                $oResult->oDrawBalls->addBall($aParts[7]);

                $oResult->sDrawNumber=$sDrawNumber;

                \DAL::query('UPDATE lottery_draws SET draw=\''.$sDrawNumber.'\' WHERE id='.$iDrawId);


                //Have to go elsewhere for prize breakdown
                $this->scrapePrizesForDate(strtotime($aParts[1]),$oResult);

                $oResult->save();
                $oResult->savePrizes();
                $oResult->saveResults();
            }
        }
    }

    function scrapePrizesForDate($iDate,$oResult){
        $sUSADate=intval(date('m',$iDate)).'-'.intval(date('d',$iDate)).'-'.date('Y',$iDate);
        $sURL='http://www.megamillions.com/winning-numbers/'.$sUSADate;
        $sData=$this->getData($sURL,false);
        $sData=strtolower($sData);

        $oResult->url=$sURL;

        $oDoc=new DOMDocument();
        $oDoc->loadHTML($sData);

        $sXPath='//table[@class="winning-numbers-jackpot-table"]//tbody//tr';

        $aRows=$this->runXPath($oDoc, $sXPath);
        $iPrizeTierNumber=1;
        foreach($aRows as $oRow){
            $iNumberOfWinners=intval(preg_replace('/[^0-9\.]/','',$oRow->childNodes->item(2)->nodeValue));
            $dPrizePerWinner=floatval(preg_replace('/[^0-9\.]/','',$oRow->childNodes->item(4)->nodeValue));
            if($iNumberOfWinners==0) $dPrizePerWinner=0;
            $oResult->oDrawPrizes->addPrize($iPrizeTierNumber,$iNumberOfWinners,$dPrizePerWinner);
            $iPrizeTierNumber++;
        }

    }
}