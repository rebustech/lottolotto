<?php

class Eurojackpot_scraperApi extends German_scraperApi{
    var $aURLS=array();
    var $sBaseURL='http://www.europeanlotteryguild.com/lottery_results/euromillions_results/draw_history?results_date=';

    function __construct() {
        set_time_limit(0);
        $this->oLottery=new EuroJackpot();
    }

    function scrape(){
        $this->scrapeDates();
    }

    function getBalls($sData,$oResult){
        $oData=json_decode($sData);
        foreach($oData as $oDate){
            foreach($oDate->ej->gewinnzahlen as $sBall){
                $oResult->oDrawBalls->addBall($sBall);
            }
            foreach($oDate->ej->zwei_aus_acht as $sBall){
                $oResult->oDrawBalls->addBall($sBall);
            }
        }
    }

    function getJackpot($sData){

    }

    function getResults($sData,$oResult){
        $oData=json_decode($sData);

        foreach($oData as $oDate){
            foreach($oDate->ej->quoten as $oRow){
                if($oRow->quote==0){
                    $oRow->quote=$oDate->ej->spieleinsatz;
                }
                $oResult->oDrawPrizes->addPrize($oRow->klasse,$oRow->anzahl,$oRow->quote);
            }
        }
    }


    function scrapeDates(){
        $sSQL='SELECT DATE(fk_lotterydate) AS drawdate FROM lottery_draws r WHERE r.fk_lottery_id=13 AND (numbers IS NULL OR winnings IS NULL)';
        $aMissingDates=\DAL::executeQuery($sSQL);
        foreach($aMissingDates as $sMissingDate){
            $sDate=date('Y-m-d',strtotime($sMissingDate['drawdate']));
            $sURL='https://www.lotto.de/bin/ej_archiv?drawday='.$sDate; //Comes back as JSON :)

            $sData=$this->getData($sURL);

            //Find the draw to update
            $iDrawId=$this->locateDraw($this->oLottery->iLotteryID, $sMissingDate['drawdate']);

            //Scrape out the balls
            $oResult=new \LL\Results\LotteryResult($iDrawId,$this->oLottery);
            $oResult->sLotteryDate=$sDate;
            $this->getBalls($sData,$oResult);

            //Scrape out the prize breakdown
            $this->getResults($sData, $oResult);

            $oResult->save();
            $oResult->savePrizes();
            $oResult->saveResults();
        }
    }

}
