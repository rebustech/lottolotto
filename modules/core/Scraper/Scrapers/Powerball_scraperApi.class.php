<?php

class Powerball_ScraperApi extends scraper{

    function __construct() {
        set_time_limit(0);
        $this->oLottery=new USAPowerball();
    }

    function scrape(){
        /**
         * Get the first date that we have no results for
         */
        $sSQL='SELECT MIN(fk_lotterydate) FROM lottery_draws WHERE fk_lottery_id=4 AND fk_lotterydate<NOW() AND (numbers=NULL OR winnings=NULL) ';
        $sNextDrawDate=\DAL::executeGetOne($sSQL);
        if($sNextDrawDate==null){
            return false;
        }
        $dNextDrawDate=strtotime($sNextDrawDate);

        $sBallsData=$this->getData('http://www.powerball.com/powerball/winnums-text.txt',false);

        $aDraws=explode("\n",$sBallsData);
        array_shift($aDraws);
        foreach($aDraws as $sDraw){
            $aParts=explode('  ',$sDraw);
            $aDateParts=explode('/',$aParts[0]);

            //Sort out the American date format into Julian
            $sDate=$aDateParts[2].'-'.$aDateParts[0].'-'.$aDateParts[1];

            if(strtotime($sDate)>$dNextDrawDate){

                //Find the draw to update
                $iDrawId=$this->locateDraw($this->oLottery->iLotteryID, $sDate);

                //Create a results object to lodge the data with correctly
                $oResult=new \LL\Results\LotteryResult($iDrawId,$this->oLottery);

                //Scrape out the balls
                $oResult->oDrawBalls->addBall($aParts[1]);
                $oResult->oDrawBalls->addBall($aParts[2]);
                $oResult->oDrawBalls->addBall($aParts[3]);
                $oResult->oDrawBalls->addBall($aParts[4]);
                $oResult->oDrawBalls->addBall($aParts[5]);
                $oResult->oDrawBalls->addBall($aParts[6]);
                $oResult->oDrawBalls->addBall($aParts[7]);

                //Have to go elsewhere for prize breakdown
                $this->scrapePrizesForDate($aParts[0], $oResult);

                $oResult->save();
                $oResult->savePrizes();
                
            }
        }
    }

    function scrapePrizesForDate($sUSADate,$oResult){

            $sURL='http://www.powerball.com/powerball/pb_winner_summary.asp?DrawDate='.urlencode($sUSADate);
            $sData=$this->getData($sURL,false);

            $oDoc=new DOMDocument();
            $oDoc->loadHTML($sData);

            $sXPath='//table//strong[text()="POWERBALL"]/../../../../../../table[1]//tr[position()<last()]';

            $aRows=$this->runXPath($oDoc, $sXPath);
            $iPrizeTierNumber=0;
            foreach($aRows as $oRow){
                $sRow1=$oRow->nodeName;
                $aNumberOfWinners[$iPrizeTierNumber]=intval(preg_replace('/[^0-9\.]/','',$oRow->childNodes->item(2)->nodeValue));
                $iPrizeTierNumber++;
            }

            $sXPath='//table//strong[text()="POWERBALL"]/../../../../../../table[2]//tr[position()<last()]';
            $aRows=$this->runXPath($oDoc, $sXPath);
            $iPrizeTierNumber=1;
            foreach($aRows as $oRow){
                $iNumberOfWinners=$aNumberOfWinners[$iPrizeTierNumber];
                //These results pages show total payout rather than prize per winner.
                $aPrizePerWinner[$iPrizeTierNumber]=floatval(preg_replace('/[^0-9\.]/','',$oRow->childNodes->item(2)->nodeValue))/$iNumberOfWinners;
                $iPrizeTierNumber++;
            }

            for($iPrizeNumber=1;$iPrizeNumber<=9;$iPrizeNumber++){
                $iNumberOfWinners=$aNumberOfWinners[$iPrizeNumber];
                $dPrizePerWinner=$aPrizePerWinner[$iPrizeNumber];
                $oResult->oDrawPrizes->addPrize($iPrizeNumber,$iNumberOfWinners,$dPrizePerWinner);
            }

    }
}