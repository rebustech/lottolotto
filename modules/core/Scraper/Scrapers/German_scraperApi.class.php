<?php

class German_scraperApi extends scraper{
    var $aURLS=array();
    var $sBaseURL='http://www.europeanlotteryguild.com/lottery_results/german_lotto_results/draw_history?results_date=';

    function __construct() {
        set_time_limit(0);
        $this->oLottery=new GermanLotto();
    }

    function scrape(){
        $this->scrapeDates();
    }

    function scrapeResultPages(){
        foreach($this->aURLS as $sURL){
            $sURL='http://www.europeanlotteryguild.com'.$sURL;
            $sData=$this->getData($sURL);

            //Date comes from the URL
            $aURL=explode('?date=',$sURL);
            $sDate=$aURL[1];

            if(strtotime($sDate)>strtotime('2014-06-01')){

                //Find the draw to update
                $iDrawId=$this->locateDraw($this->oLottery->iLotteryID, $sDate);

                //Scrape out the balls
                $oResult=new \LL\Results\LotteryResult($iDrawId,$this->oLottery);
                $this->getBalls($sData,$oResult);

                //Scrape out the prize breakdown
                $this->getResults($sData, $oResult);

                $oResult->save();
            }
        }

    }

    function getBalls($sData,$oResult){
        $oData=json_decode($sData);
        foreach($oData as $oDate){
            foreach($oDate->lotto->gewinnzahlen as $sBall){
                $oResult->oDrawBalls->addBall($sBall);
            }
            $oResult->oDrawBalls->addBall($oDate->lotto->superzahl);
        }
    }

    function getJackpot($sData){

    }

    function getResults($sData,$oResult){
        $oData=json_decode($sData);

        foreach($oData as $oDate){
            foreach($oDate->lotto->quoten as $oRow){
                if($oRow->quote==0){
                    $oRow->quote=$oDate->lotto->spieleinsatz;
                }
                $oResult->oDrawPrizes->addPrize($oRow->klasse,$oRow->anzahl,$oRow->quote);
            }
        }
    }

    function scrapeDates(){
        error_reporting(E_ERROR);
        $sSQL='SELECT d.drawdate FROM lottery_draws r RIGHT JOIN r_lottery_dates d ON r.fk_lotterydate=d.drawdate AND d.fk_lottery_id=r.fk_lottery_id WHERE d.fk_lottery_id=11 AND d.drawdate<NOW() AND (numbers IS NULL OR winnings IS NULL)';
        $aMissingDates=\DAL::executeQuery($sSQL);
        foreach($aMissingDates as $sMissingDate){
            $sDate=date('Y-m-d',strtotime($sMissingDate['drawdate']));
            $sURL='https://www.lotto.de/bin/6aus49_archiv?drawday='.$sDate; //Comes back as JSON :)

            $sData=$this->getData($sURL,false);

            //Find the draw to update
            $iDrawId=$this->locateDraw($this->oLottery->iLotteryID, $sMissingDate['drawdate']);

            //Scrape out the balls
            $oResult=new \LL\Results\LotteryResult($iDrawId,$this->oLottery);
            $oResult->sLotteryDate=$sDate;
            $this->getBalls($sData,$oResult);

            //Scrape out the prize breakdown
            $this->getResults($sData, $oResult);

            $oResult->save();
            $oResult->savePrizes();
            $oResult->saveResults();
        }
    }

}
