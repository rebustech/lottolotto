<?php

class booking_items_Model extends LLModel{
    static $sTableName='booking_items';
    static $sKeyField='bookingitem_id';

    var $bookingitem_id;
    var $fk_booking_id;
    var $fk_lottery_id;
    var $fk_lotterydate;
    var $cost;
    var $numbers;
    var $purchased;
    var $fk_ticket_engine_id;
    var $fk_game_engine_id;
    var $game_options;
    var $ticket_engine_assigned_at;
    var $purchased_at;

    public function afterGenericUpdate($aFieldsBeforeSave = null) {
        /**
         * Check to see if a ticket buying model has been assigned yet
         */
        if($this->fk_ticket_engine_id==0) TicketBuyingController::purchaseTicket($this);
    }

    function save($aData=array()){
        $aData['fk_booking_id']=$this->fk_booking_id;
        $aData['fk_lottery_id']=$this->fk_lottery_id;
        $aData['fk_lotterydate']=$this->fk_lotterydate;
        $aData['cost']=$this->cost;
        $aData['numbers']=$this->numbers;
        $aData['purchased']=$this->purchased;
        $aData['fk_ticket_engine_id']=$this->fk_ticket_engine_id;
        $aData['ticket_engine_assigned_at']=$this->ticket_engine_assigned_at;
        $aData['purchased_at']=$this->purchased_at;
        $aData['hedging_cost']=$this->hedging_cost;

        parent::save($aData);
    }
}