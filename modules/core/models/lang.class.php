<?php
/**
 * Used to imnplement multiple languages in the backend system by means of language
 * files.
 * @example lang::get('language_item_key') - this would get the value from the currently
 * active language file for the key provided.
 * @package LoveLotto
 * @subpackage MultiLingual
 * @author Jonathan Patchett
 */
class lang{
    static $aKeys=array();
    static $sLanguage='en-gb';

    /**
     * Changes the active language
     * @param string $sLanguage The language to use
     */
    static function set_language($sLanguage){
        self::$sLanguage;
    }

    /**
     * Gets the value to display for any given language key
     * @example lang::get('language_item_key') - this would get the value from the currently active language file for the key provided.
     * @param string $sKey The key to use
     * @param string $sLanguage optional The language to use. If not provided the current active language is used
     * @return string The value that can be displayed
     */
    static function get($sKey,$sLanguage=null){
        return self::get_local($sKey, $sLanguage);
    }

    /**
     * Reads a key from memcache.
     * Internal use only
     * @param string $sKey The key to use
     * @param string $sLanguage optional The language to use. If not provided the current active language is used
     * @return string The value that can be displayed
     */
    protected static function get_memcache($sKey,$sLanguage=null){
        //NOT YET IMPLEMENTED, BUT WE SHOULD
    }

    /**
     * Reads a key from local disk. Used in the event that a key is not found
     * in memcache or memcache is not available
     * @param string $sKey The key to use
     * @param string $sLanguage optional The language to use. If not provided the current active language is used
     * @return string The value that can be displayed
     */
    protected static function get_local($sKey,$sLanguage=null){
        if($sLanguage===null) $sLanguage=self::$sLanguage;
        if(!array_key_exists($sKey, self::$aKeys[$sLanguage])){
            if(array_key_exists($sLanguage, self::$aKeys)){
                $sFilename=$_SERVER['DOCUMENT_ROOT'].'/language/'.$sLanguage.'/lang.txt';
                file_put_contents($sFilename, "\r\n$sKey:", FILE_APPEND);
                return $sKey;
            }else{
                //Create an empty array so that we don't keep trying to load the language again and again on subsequent calls
                self::$aKeys[$sLanguage]=array();
                $sPath=$_SERVER['DOCUMENT_ROOT'].'/language/'.$sLanguage;
                $d=opendir($sPath);
                while($f=readdir($d)){
                    $sFilename=$sPath.'/'.$f;
                    if(is_file($sFilename)) self::load_language_file($sFilename, $sLanguage);
                }
            }
        }
        if(!array_key_exists($sKey, self::$aKeys[$sLanguage])){
            //Pop on to the end of lang.php
            $sFilename=$_SERVER['DOCUMENT_ROOT'].'/language/'.$sLanguage.'/lang.txt';
            file_put_contents($sFilename, "\r\n$sKey:", FILE_APPEND);
            return $sKey;
        }else{
            return (trim(self::$aKeys[$sLanguage][$sKey])=='')?$sKey:self::$aKeys[$sLanguage][$sKey];
        }
    }

    /**
     * Loads a language file into memory. Really only for internal use
     * @param string $sFilename The filename of the language file
     * @param string $sLanguage The language key that the contents will be loaded into
     */
    static function load_language_file($sFilename,$sLanguage){
        $aValues=file($sFilename);
        foreach($aValues as $sValue){
            $aParts=explode(':',$sValue,2);
            self::$aKeys[$sLanguage][$aParts[0]]=$aParts[1];
        }
    }
}