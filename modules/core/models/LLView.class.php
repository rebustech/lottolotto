<?php

class LLView {

    public $cacheTime = 3600;

    function output($sView) {

		$cached = (isset($this->cache) && $this->cache);
		$cacheKey = 'view-'.crc32($sView);

		# Remove variable, stop it going into other views
		unset($this->cache);

		# We want to cache
		if($cached) {
			$cacheValue = \LLCache::get($cacheKey);
			if(!is_null($cacheValue) && !empty($cacheValue)) {
				return $cacheValue;
			}
		}

        # Set the global variables, access with $key or $this->$key
        foreach($this as $key => $value) {
                $$key = $value;
        }

		# Load for caching
        ob_start();
        include $sView.'.view.php';
        $contents = ob_get_clean();

		# We want to cache
		if($cached) {
			\LLCache::add($cacheKey, $contents, false, $this->cacheTime);
		}

		# Finally, spit it out
		return $contents;

    }

	/**
	 * display: Handle the Smarty method
	 * and re-work the file paths
	 */
	public function display($sView, $data = array()) {

		# Assign the data parts
		if(!empty($data)) {
			foreach($data as $key => $var) {
				$this->assign($key, $var);
			}
		}

		# Strip out Smarty views
		$sView = str_replace('.tpl', '', $sView);
		$path = str_replace('/templates', '/views', TEMPLATEPATH).'/';

		# Same output method
		echo $this->output($path.$sView);

	}

	/**
	 * assign: Handle the Smarty method
	 */
	public function assign($name, $var) {
		$this->$name = $var;
	}

	/**
	 * is_cached: Defined in Smarty
	 *
	 * @todo, remove all calls
	 */
	public function is_cached() {
		return (isset($this->cache) ? $this->cache : false);
	}
}
