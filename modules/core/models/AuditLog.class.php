<?php

class AuditLog extends LLModel{
    static $sTableName='audit_log';
    static $bOutput=false;
    static $lastCode='';

    var $id;
    var $datetime;
    var $ip_address;
    var $code;
    var $message;
    var $model;
    var $fk_id;
    var $user_id;

    /**
     * Adds an item to the audit log
     * @param string $sMessage
     * @param string $sCode
     * @param mixed $mModel
     * @param int $iId
     */
    static function LogItem($sMessage,$sCode,$mModel,$iId){
        return;
        self::$lastCode=$sCode;

        $oLogItem=new AuditLog();
        $oLogItem->message=$sMessage;
        $oLogItem->code=$sCode;
        $oLogItem->model=(is_object($mModel))?get_class($mModel):$mModel;
        $oLogItem->fk_id=$iId;
        $oLogItem->ip_address=$_SERVER['REMOTE_ADDR'];
        $oLogItem->save();

        if(self::$bOutput){
            echo "\r\n<li>{$oLogItem->model} : {$sCode} : $sMessage";
        }

    }

    function save($aData=array()){
        return;
        global $oSecurityObject;
        $aData['datetime']=date('Y-m-d H:i:s');
        $aData['message']=$this->message;
        $aData['code']=$this->code;
        $aData['model']=$this->model;
        $aData['fk_id']=$this->fk_id;
        $aData['ip_address']=$this->ip_address;
        if(is_object($oSecurityObject)) $aData['user_id']=$oSecurityObject->getiUserID();
        parent::save($aData);
    }
}

/**
* Error handler, passes flow over the exception logger with new ErrorException.
*/
function log_error( $num, $str, $file, $line, $context = null )
{
    switch($num){
        case E_ERROR:
        case E_PARSE:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
            log_exception( new ErrorException( $str, 0, $num, $file, $line ) );
            break;
    }
}

/**
* Uncaught exception handler.
*/
function log_exception( Exception $e,$bDie=false )
{
    $sMessage='Exception Occured: '.get_class($e).' - '.$e->getMessage(). ' in '.$e->getFile().' on line '.$e->getLine();

    @file_put_contents('/f1/www/error/lasterror.txt', $sMessage);

    AuditLog::LogItem($sMessage, AuditLog::$lastCode, $e, 0);
    //header( "Location: {$config["error_page"]}" );

    if($bDie) die($sMessage);
}

/**
* Checks for a fatal error, work around for set_error_handler not working on fatal errors.
*/
function check_for_fatal()
{
    $error = error_get_last();
    if ( $error["type"] == E_ERROR )
        log_error( $error["type"], $error["message"], $error["file"], $error["line"] );
}

//register_shutdown_function( "check_for_fatal" );
set_error_handler( "log_error" );
set_exception_handler( "log_exception" );
ini_set( "display_errors", "off" );
error_reporting( E_ALL );
