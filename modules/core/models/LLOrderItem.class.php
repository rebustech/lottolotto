<?php

namespace LL;

class OrderItem extends \LLModel{
    static $sTableName='booking_order_items';
    static $sKeyField='id';
    static $sHandlerField='';
    static $bCacheLevel=0;      //Default caching level is no caching

    const STATUS_OPEN=1;
    const STATUS_CLOSED=2;
    const STATUS_CANCELLED=3;

    var $id;
    var $fk_game_engine_id;
    var $config;
    var $cost;
    var $fk_booking_id;
    var $created_at;
    var $processed_at;
    var $cancelled_at;
    var $is_subscription;
    var $last_bill_date;
    var $next_bill_date;
    var $start_date;
    var $end_date;
    var $fk_status_id;

    private $_oConfig;

    function save($aData=array()){
        $aData['id']=$this->id;
        $aData['fk_game_engine_id']=$this->fk_game_engine_id;
        $aData['config']=$this->config;
        $aData['cost']=$this->cost;
        $aData['fk_booking_id']=$this->fk_booking_id;
        $aData['created_at']=$this->created_at;
        $aData['processed_at']=$this->processed_at;
        $aData['cancelled_at']=$this->cancelled_at;
        $aData['is_subscription']=$this->is_subscription;
        $aData['last_bill_date']=$this->last_bill_date;
        $aData['next_bill_date']=$this->next_bill_date;
        $aData['start_date']=$this->start_date;
        $aData['end_date']=$this->end_date;
        $aData['fk_status_id']=$this->fk_status_id;
        parent::save($aData);
    }

    function getConfig(){
        if(!isset($this->_oConfig)){
            $this->_oConfig=json_decode($this->config);
        }
        return $this->_oConfig;
    }

    function getGameEngine(){
        if(!isset($this->_oGameEngine)){
            $this->_oGameEngine=GameEngine::getById($this->fk_game_engine_id);
        }
        return $this->_oGameEngine;
    }

    /**
     * Closes this order. This is only called when all booking items inside
     * the order are closed
     */
    function closeOrder(){
        if($this->fk_status_id<2) $this->fk_status_id=2;
        $this->save();
    }

    /**
     * Cancels this order and cancels all future booking items
     */
    function cancelBet(){

    }

}