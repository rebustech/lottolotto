<?php
/**
 * Model for a draw
 * @package LoveLotto
 * @subpackage Models
 * @author Jonathan Patchett
 */

namespace LL;

class Transaction extends \LLModel{
    static $sTableName='transactions';
    static $sKeyField='transaction_id';
    
   
    function loadWinnerInformation(){
        $sSQL='SELECT * FROM transactions t
                        INNER JOIN lottery_winnings l ON t.transaction_id=l.fk_transaction_id
                        INNER JOIN booking_items b ON l.fk_bookingitem_id=b.bookingitem_id
                        INNER JOIN lotteries_cmn lo ON lo.lottery_id=b.fk_lottery_id
                        INNER JOIN bookings o ON b.fk_booking_id=o.booking_id
                        WHERE fk_gateway_id=7 AND transaction_id='.$this->transaction_id;
        
        $aData=\DAL::executeGetRow($sSQL);
        $this->populateFromArray($aData);
    }
}