<?php

/**
 * This is just a generic place to statically place items that need to be used
 * in the reponse, such as additional headers, scripts etc...
 */
class LLResponse{
    static $sPageTitle='Hello';
    static $sHead='';
    static $sPreBody='';
    static $sPostBody='';

    /**
     * Convenience method to get the html for adding an external script
     * @param type $sSrc
     * @return type
     */
    static function addScript($sSrc){
        return '<script src="'.$sSrc.'"></script>';
    }

    /**
     * Convenience method to get the html for adding some inline script - useful
     * for adding json configuration values to $sPostBody
     * @param type $sScript
     * @return type
     */
    static function addInlineScript($sScript){
        return '<script>'.$sScript.'</script>';
    }
}