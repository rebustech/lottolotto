<?php
namespace LL\GBGroup;

class Address {
    var $address1;
    var $address2;
    var $city;
    var $county_state;
    var $postcode;
    var $country;

    function __construct($oProperties=null) {
        if($oProperties!==null){
            $this->address1=$oProperties->address1;
            $this->address2=$oProperties->address2;
            $this->city=$oProperties->city;
            $this->county_state=$oProperties->county;
            $this->postcode=$oProperties->postcode;
            $this->country=$oProperties->country;
        }
    }
}