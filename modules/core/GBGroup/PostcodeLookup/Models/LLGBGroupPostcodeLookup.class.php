<?php

namespace LL\GBGroup;

class PostcodeLookup {
    static function getPostcodeAddressList($sPostCode){
        for($a=4;$a<12;$a++){
            $oAddr=new Address();
            $oAddr->address1=$a.' Top Llan Road';
            $oAddr->address2='Glan Conwy';
            $oAddr->city='Colwyn Bay';
            $oAddr->county='Clwyd';
            $oAddr->country='UK';

            $aResponse[]=new Address($oAddr);
        }
        return $aResponse;
        
    }
}