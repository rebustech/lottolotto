<?php

class LLGBGroupPostcodeLookupApi extends \LL\AjaxController {
    function getPostcodeAddressList(){
        $sPostcode=$_POST['postcode'];
        $this->address_list=  LL\GBGroup\PostcodeLookup::getPostcodeAddressList($sPostCode);
        die($this->output());
    }
}