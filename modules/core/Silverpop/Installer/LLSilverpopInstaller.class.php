<?php
/**
 * Installs Silverpop database changes
 * @package LoveLotto
 * @subpackage PayFrex
 */

namespace LL\Silverpop;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        if(self::getModuleVersion('Silverpop')<1){
        }
        $aJobs[]=new \LL\Installer\Task('LL\Silverpop\Installer', 'installDatabase', 'Install Silverpop Database updates');
        $aJobs[]=new \LL\Installer\Task('LL\Silverpop\Installer', 'installPasswordUpdates', 'Install Member Password updates');

        return $aJobs;
    }

    /**
     * Installs all the tables we need for the new CMS
     */
    static function installDatabase(\LL\Installer\Task $oTask){
        self::installMembersAlertsTable();
    }

    static function installPasswordUpdates(\LL\Installer\Task $oTask){
        self::installMembersPasswordResetTable();
    }

    static function installMembersPasswordResetTable() {

        $oTTable=new \LL\Installer\Table('members_reset_token');

        $oTTable->addField(new \LL\Installer\Field('member_id','int',11, false, 0));
        $oTTable->addField(new \LL\Installer\Field('token_id','varchar',24, false, 0));
        $oTTable->addField(new \LL\Installer\Field('token_expires','datetime',null, true));
        $oTTable->addField(new \LL\Installer\Field('token_used','tinyint',1, false, 0));

        $oTTable->aPrimaryKeyFields = array('token_id');

        $oTTable->compile();

        /*
        CREATE TABLE `members_reset_token` (
          `member_id` int(11) unsigned NOT NULL,
          `token_id` varchar(24) NOT NULL,
          `token_expires` datetime DEFAULT NULL,
          `token_used` tinyint(1) DEFAULT '0',
          PRIMARY KEY (`member_id`,`token_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        */

    }

    /**
     * Modification to transactions table
     */
    static function installMembersAlertsTable(){

        $oTTable=new \LL\Installer\Table('members_alerts');

        $oTTable->addField(new \LL\Installer\Field('member_id','int',11, false, 0));
        $oTTable->addField(new \LL\Installer\Field('jackpot_6aus49','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('jackpot_eurojackpot','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('jackpot_euromillions','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('jackpot_megamillions','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('jackpot_powerball','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('result_6aus49','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('result_eurojackpot','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('result_euromillions','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('result_megamillions','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('result_powerball','tinyint', 4, false, 0));
        $oTTable->addField(new \LL\Installer\Field('max_news','tinyint', 1, false, 0));
        $oTTable->addField(new \LL\Installer\Field('max_jackpots','tinyint', 1, false, 0));
        $oTTable->addField(new \LL\Installer\Field('max_jackpot_over','tinyint', 1, false, 0));

        $oTTable->compile();

        /*
        $sSQL = <<<EOT
CREATE TABLE IF NOT EXISTS `members_alerts` (
  `member_id` int(11) unsigned NOT NULL,
  `jackpot_6aus49` tinyint(4) NOT NULL DEFAULT '0',
  `jackpot_eurojackpot` tinyint(4) NOT NULL DEFAULT '0',
  `jackpot_euromillions` tinyint(4) NOT NULL DEFAULT '0',
  `jackpot_megamillions` tinyint(4) NOT NULL DEFAULT '0',
  `jackpot_powerball` tinyint(4) NOT NULL DEFAULT '0',
  `result_6aus49` tinyint(4) NOT NULL DEFAULT '0',
  `result_eurojackpot` tinyint(4) NOT NULL DEFAULT '0',
  `result_euromillions` tinyint(4) NOT NULL DEFAULT '0',
  `result_megamillions` tinyint(4) NOT NULL DEFAULT '0',
  `result_powerball` tinyint(4) NOT NULL DEFAULT '0',
  `max_news` tinyint(1) NOT NULL DEFAULT '1',
  `max_jackpots` tinyint(1) NOT NULL DEFAULT '1',
  `max_jackpot_over` smallint(6) NOT NULL DEFAULT '5',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Email alerts the member signs up for';
EOT;

        echo($sSQL);
         */

    }


}
