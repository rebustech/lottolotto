<?php

/**
 * Lotto Exception is the Extended Exception to be used for the XLotto Platform.
 * NOTE this class may be further extended. to denote error severity.
 * 
 * 
 * @author Nick
 * 
 * 
 * 
 * */
class Silverpop {

    protected static $sEngageJSID;
    protected static $sTransactJSID;
    
    
    protected static $iMarketingDatabaseListId = '2482800';
    protected static $iTransactionalDatabaseListId = '2482058';

    public static function addUpdateContactToBothDatabases($iID, $update = false)
    {
        /*
         * Wrapper function to individual databases
         */
        Silverpop::addModifyContactToMarketingDatabase($iID, $update);
        Silverpop::addModifyContactToTransactionalDatabase($iID, $update);
    }

    public static function addModifyContactToMarketingDatabase($iID, $update = "false")
    {
        $aVals = array();
        
        // Get member details from the database
        // Need to include text of the member's country and their
        // email preferences
        $sAccountSQL = "SELECT m.*, cc.comment AS country " .
                       "FROM members m " .
                       "LEFT JOIN countries_cmn cc ON m.fk_country_id = cc.country_id " .
                       "WHERE m.member_id = '" . $iID . "';";
     
//        echo ("Adding contact to marketing<br/>");
        $aAccountRow = DAL::executeGetRow($sAccountSQL);
        $sAction = 'AddRecipient';

        $aVals['LIST_ID'] = self::$iMarketingDatabaseListId;
        $aVals['CREATED_FROM'] = '2';
        $aVals['UPDATE_IF_FOUND'] = var_export($update, true);
        
        foreach ($aAccountRow as $sAccountFieldName => $sAccountDataField)
        {
            // Balance field is known as current balance in Silverpop
            if ($sAccountFieldName == "balance")
            {
                $sAccountFieldName = "current balance";
            }
            
            // Need to do a quick format on DOB here
            // so that Silverpop accepts it
            if ($sAccountFieldName == "dob")
            { 
              $aTemp = explode("-", $sAccountDataField);
              $sAccountDataField = $aTemp[1] . '/' . $aTemp[2] . '/' . $aTemp[0];
            }
            $aVals['COLUMN`' . $sAccountFieldName] = $sAccountDataField;
        }
        

        $sAlertsSQL = "SELECT ma.* FROM members_alerts ma " .
                      "WHERE ma.member_id = '" . $iID . "';";
        $aAlertsRow = DAL::executeGetRow($sAlertsSQL);
        
        
        foreach ($aAlertsRow as $sAlertFieldName => $sAlertDataField)
        {
            $sRef = strtoupper(substr($sAlertFieldName, 0, 3));
            
            if ($sRef == 'RES' || $sRef == 'JAC')
            {
                $sAlertFieldName = "alert_" . $sAlertFieldName;
            }
            
            $sAlertDataField = ($sAlertDataField=="1"?"Yes":"No");
            
            $aVals['COLUMN`' . $sAlertFieldName] = $sAlertDataField;
        }        
         
        // Reset the member ID field here to the parameter value passed,
        // to ensure the correct one is passed over
        $aVals['COLUMN`member_id'] = $iID;
                
        $sJSID = Silverpop::logIntoEngage();

        $sAddContactString = Silverpop::buildCurlRequest($sAction, $aVals);
        
//        echo(htmlentities($sAddContactString));
        
       Silverpop::runCurl($sAddContactString, $sJSID);
        
//        $sReturnXML = Silverpop::runCurl($sAddContactString, $sJSID);  
//        $aReturnArray = Silverpop::arrayBuild($sReturnXML);      
//        print_d($aReturnArray);
//        die();
    }

    public static function addModifyContactToTransactionalDatabase($iID, $update = "false")
    {
        $aVals = array();
        
        // Get member details from the database
        // Need to include text of the member's country and their
        // email preferences
        $sAccountSQL = "SELECT m.*, cc.comment AS country FROM members m " .
                       "LEFT JOIN members_alerts ma ON m.member_id = ma.member_id " .
                       "LEFT JOIN countries_cmn cc ON m.fk_country_id = cc.country_id " .
                       "WHERE m.member_id = '" . $iID . "';";
        
        //echo ("Adding contact to transactional<br/>");
        $aAccountRow = DAL::executeGetRow($sAccountSQL);
        $sAction = 'AddRecipient';
        $aVals['LIST_ID'] = self::$iTransactionalDatabaseListId;
        $aVals['CREATED_FROM'] = '1';
        $aVals['SEND_AUTOREPLY'] = 'true';
        $aVals['UPDATE_IF_FOUND'] = var_export($update, true); 
        
        /*
         * Var export used to get the text of the bool.
         */

        foreach ($aAccountRow as $sAccountFieldName => $sAccountDataField)
        {
            // Balance field is known as current balance in Silverpop
            if ($sAccountFieldName == "balance")
            {
                $sAccountFieldName = "current balance";
            }

            $aVals['COLUMN`' . $sAccountFieldName] = $sAccountDataField;

        }
        
        // Reset the member ID field here to the parameter value passed,
        // to ensure the correct one is passed over
        
        $aVals['COLUMN`member_id'] = $iID;
        
        
        $sJSID = Silverpop::logIntoTransact();
        $sAddContactString = Silverpop::buildCurlRequest($sAction, $aVals);
        
//        echo("<pre>");
//        echo(htmlentities($sAddContactString));
//        echo("</pre>");
//        
//        die();
        $sReturnXML = Silverpop::runCurl($sAddContactString, $sJSID);
        
        Silverpop::arrayBuild($sReturnXML);
        //$aReturnArray = Silverpop::arrayBuild($sReturnXML);
        
        //print_d($aReturnArray);
        //die();
        
        //return $aReturnArray;
    }
    
    
    public static function testLogin()
    {
        //testing function to run a basic login.
        Silverpop::logIntoEngage();
        
        
    }
    
    //public static function sendTransactEmail($iAccountID, $sHTML, $sEmail){
    
    public static function sendTransactEmail($sEmail, $aPersonalisationData, $iCampaignId){
        //echo("------Attempting to send transact email-------<br>"); 

       // echo("Email is {$sEmail}<br/>");
        //print_d($aPersonalisationData);

        
         $sJSID = Silverpop::logIntoTransact();
         $sAction = 'XTMAILING';
         
         //At this time iAccountID is actually email address.
         
         $aVals['CAMPAIGN_ID'] = $iCampaignId;
         $aVals['TRANSACTION_ID'] = time();
         $aVals['SHOW_ALL_SEND_DETAIL'] = 'true';
         $aVals['SEND_AS_BATCH'] = 'false';
         $aVals['NO_RETRY_ON_FAILURE'] = 'false';
         
         // Columns to update in the Transact entry
         $aVals['SAVE_COLUMNS'][0] = 'member_id';
         $aVals['SAVE_COLUMNS'][1] = 'current balance';
         $aVals['SAVE_COLUMNS'][2] = 'firstname';
         $aVals['SAVE_COLUMNS'][3] = 'email_ref';
         
         //$aVals['RECIPIENT']['EMAIL'] = $iAccountID;         
         $aVals['RECIPIENT']['EMAIL'] = $sEmail;         
         $aVals['RECIPIENT']['BODY_TYPE'] = 'HTML';
         
         $count = 0;
         foreach ($aPersonalisationData as $sTag=>$sVal)
         {
            $aVals['RECIPIENT']['PERSONALIZATION'][$count]['TAG_NAME'] = $sTag;         
            $aVals['RECIPIENT']['PERSONALIZATION'][$count]['VALUE'] = '<![CDATA['.$sVal.']]>';
            $count++;
         }
         
         
         $sSendEmailString = Silverpop::buildCurlRequest($sAction, $aVals);
         
         // the extra parameter here indicated that we're sending email
         // and therefore a different URL needs to be used
         
        // echo("<pre>");
        // echo(htmlentities($sSendEmailString));
        // die();
         
         $arr = Silverpop::runCurl($sSendEmailString, $sJSID, true);
         
         $aReturnArray = Silverpop::arrayBuild($arr);
        
        //print_d($aReturnArray);
       //die();
         
    }

    
    public static function scheduleEngageEmail($aCampaignDetails, $sMailingName, $aPersonalData, $sScheduleTime=null)
    {
        
        
        $sAction = 'ScheduleMailing';

        $aVals['LIST_ID'] = self::$iMarketingDatabaseListId;
        $aVals['TEMPLATE_ID'] = $aCampaignDetails['template'];
        $aVals['LIST_ID'] = $aCampaignDetails['list'];
        $aVals['MAILING_NAME'] = $sMailingName;
        $aVals['SEND_HTML'] = true;
        $aVals['VISIBILITY'] = 1;
        
        if (!is_null($sScheduleTime))
        {
            $aVals['SCHEDULED'] = $sScheduleTime;
        }
        
        $count = 0;
        foreach ($aPersonalData as $sFieldName => $sData)
        {
            $aVals['SUBSTITUTIONS']['SUBSTITUTION'][$count]['NAME'] = $sFieldName;
            $aVals['SUBSTITUTIONS']['SUBSTITUTION'][$count]['VALUE'] = '<![CDATA['.$sData.']]>';
            $count++;
        }
        
        $sScheduleMailString = Silverpop::buildCurlRequest($sAction, $aVals);
        
        $sJSID = Silverpop::logIntoEngage();
        Silverpop::runCurl($sScheduleMailString, $sJSID);
        
//       echo(htmlentities($sScheduleMailString));        
//        $sReturnXML = Silverpop::runCurl($sScheduleMailString, $sJSID);  
//        $aReturnArray = Silverpop::arrayBuild($sReturnXML);
//        
//        print_d($aReturnArray);
//        die();
    }
    
    
    private static function logIntoEngage($sLoginName = null, $sPassword = null)
    {
        //echo("Logging into Engage<br/>");
        //runs a simple login based on passed variables
        if (!$sLoginName)
        {
            //$sLoginName = "spapie@maxlottos.com";
            $sLoginName = "nathan.pace@igamingsports.com";
        }
        if (!$sPassword)
        {
            //$sPassword = "@8MIDhpkGmbjYiRptn3H17SoifVZUg3vFc08KmRLWHjrEmbsqRCBTTFevIJu61eYxYLDW6fbhLqHeMekK37UfA1lDFuRSBA4O6YnSpL7VvZ5KwqNGUdYCXZeZd0PWSbOz";
            $sPassword = "A9tMcta3!";
        }
        return $iJSID = Silverpop::basicLogin($sLoginName, $sPassword);
    }

    private static function logIntoTransact($sLoginName = null, $sPassword = null)
    {
        //echo("Logging into Transact<br/>");
        if (!$sLoginName)
        {
            //$sLoginName = "spapi@maxlottos.com";
            $sLoginName = "nathan@snathe.net";
        }
        if (!$sPassword)
        {
            //$sPassword = "@r4JyZuHsnuFqoRaMBor52PUTpvM7295YjNF8VYKR46OT76sW6W9ZXJzKSCKA9K7Oa5imRgvvdx8mWG1cXkzh007vwzqxdu7zJuoddIIinZoLeyVU64TGV3RGE3whCWNI";
            $sPassword = "A9tMcta3!";
        }
        return $iJSID = Silverpop::basicLogin($sLoginName, $sPassword);
    }

    private static function basicLogin($sLoginName, $sPassword)
    {
        //echo("Now in Basic login<br/>");
        /*
         * performs actual login, and returns jSessionID
         */
        $aLoginDetails['USERNAME'] = $sLoginName;
        $aLoginDetails['PASSWORD'] = $sPassword;
        $sSilverRequest = Silverpop::buildCurlRequest("Login", $aLoginDetails);
        
        //echo("Silverpop request string is:<br>");
        //echo(htmlentities($sSilverRequest));
        //echo("<br>---------------------<br>");
        
        $sReturnString = Silverpop::runCurl($sSilverRequest);
        $aReturnArray = Silverpop::arrayBuild($sReturnString);
        
        //echo("Return string from CURL is:<br/> " .$sReturnString . "<br/>");
        //echo("Built array is:<br/> ");
        //print_d($aReturnArray);
        
        $sJSessionVariable = $aReturnArray[0][0][3]['value'];
 
        return $sJSessionVariable;
    }

    private static function buildCurlRequest($sAction, $aVariables)
    {
        //echo("Building CURL request<br/>");
        /*
         * puts together the string for the CURL command.
         */
        $sRequestStart = "";
        $sRequestEnd = "";
        
        if($sAction != 'XTMAILING'){
            $sRequestStart = '<Envelope><Body>';
            $sRequestEnd = '</Body></Envelope>';
        }
        $sFinalRequest = $sRequestStart .
                '<' . $sAction . '>' .
                Silverpop::arrayBreak($aVariables) .
                '</' . $sAction . '>' .
                $sRequestEnd;
        return $sFinalRequest;
    }
    
    
    /**
     * Takes array and converts it to XML ready for sending to Silverpop
     * @param type $aArrayBreak array to break up
     * @return string
     */
    private static function arrayBreak($aArrayBreak)
    {

        $sReturnString = "";
        foreach ($aArrayBreak as $sKey => $sValue)
        {
            /* HAndle multiple fields with same name */
            if (strstr($sKey, "`"))
            {
                $aValDetails = explode("`", $sKey);
                $sKey = $aValDetails[0];
                $sTempVal = $sValue;
                unset($sValue);
                $sValue['NAME'] = $aValDetails[1];
                $sValue['VALUE'] = $sTempVal;
            }
            
            /*
             * Because each personalization item needs its own set 
             * of <PERSONALIZATION> tags, we handle them differently to
             * the other tags
             */
            if ($sKey == 'PERSONALIZATION')
            {
                // Loop for each personalization tag
                foreach ($sValue as $iCount=>$aPersonalisationData)
                {
                   $sReturnString .= '<' . $sKey . '>';
                   $sReturnString .= '<TAG_NAME>' . $aPersonalisationData['TAG_NAME'] . '</TAG_NAME>';
                   $sReturnString .= '<VALUE>' . $aPersonalisationData['VALUE'] . '</VALUE>';
                   $sReturnString .= '</' . $sKey . '>';
                }

            }
            /*
             * We Also need to handle the SAVE_COLUMNS differently too
             */
            else if ($sKey == 'SAVE_COLUMNS')
            {
                $sReturnString .= '<' . $sKey . '>';
                 // Loop for each personalization tag
                foreach ($sValue as $sSaveColumn)
                {
                   $sReturnString .= '<COLUMN_NAME>'. $sSaveColumn . '</COLUMN_NAME>';
                }
                $sReturnString .= '</' . $sKey . '>';               
            }
            /*
             * We also need to handle any SUBSTITUTIONs differently
             * These are used for Engage scheduled mailings
             */
            else if ($sKey == 'SUBSTITUTION')
            {
                // Loop for each personalization tag
                foreach ($sValue as $iCount=>$aSubstitutionData)
                {
                   $sReturnString .= '<' . $sKey . '>';
                   $sReturnString .= '<NAME>' . $aSubstitutionData['NAME'] . '</NAME>';
                   $sReturnString .= '<VALUE>' . $aSubstitutionData['VALUE'] . '</VALUE>';
                   $sReturnString .= '</' . $sKey . '>';
                }

            }
            else 
            {
                $sReturnString .= '<' . $sKey . '>';
                if (is_array($sValue))
                {
                    $sReturnString .= Silverpop::arrayBreak($sValue);
                }
                else
                {
                    $sReturnString .= $sValue;
                }
                $sReturnString .= '</' . $sKey . '>';
            }
        }
        return $sReturnString;
    }

    private static function arrayBuild($sXML)
    {
        //echo("Building Array<br/>");
        /*
         * Takes an XML string and turns it into an array
         */
        $opened = array();
        $opened[1] = 0;
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $sXML, $xmlarray);
        $array = array_shift($xmlarray);
        unset($array["level"]);
        unset($array["type"]);
        $arrsize = sizeof($xmlarray);
        for ($j = 0; $j < $arrsize; $j++)
        {
            $val = $xmlarray[$j];
            switch ($val["type"])
            {
                case "open":
                    $opened[$val["level"]] = 0;
                case "complete":
                    $index = "";
                    for ($i = 1; $i < ($val["level"]); $i++)
                        $index .= "[" . $opened[$i] . "]";
                    $path = explode('][', substr($index, 1, -1));
                    $value = &$array;
                    foreach ($path as $segment)
                        $value = &$value[$segment];
                    $value = $val;
                    unset($value["level"]);
                    unset($value["type"]);
                    if ($val["type"] == "complete")
                        $opened[$val["level"] - 1] ++;
                    break;
                case "close":
                    $opened[$val["level"] - 1] ++;
                    unset($opened[$val["level"]]);
                    break;
            }
        }
        //echo("Built array is:<br/>");
        //print_d($array);
        return $array;
    }

    private static function runCurl($sCURLrequest, $sJSID = null, $bSendEmail = false)
    {
        
        /*
         * If sending an email, then use a different URL than if adding
         * records to databases
         * 
         * (this caught me out for a couple of weeks!)
         */
        if ($bSendEmail === true)
        {
            $sCURLUrl = "http://transact3.silverpop.com/XTMail";
        }
        else
        {
            $sCURLUrl = "http://api3.silverpop.com/XMLAPI" . $sJSID;
        }
        
        //echo("Running CURL<br/>");
        //echo("URL is $sCURLUrl<br/>");
        //echo("Session is **|" . $sJSID . "|***<br/>");
        
        /*
         * Sends CURL Request.
         */
        $fields = array('xml' => $sCURLrequest);
        $postFields = http_build_query($fields, '', '&');
        
        // Get cURL resource
        $curl = curl_init();
        
        // Set some options - we are passing in a useragent too here

        curl_setopt($curl, CURLOPT_URL, $sCURLUrl);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        //echo ("Response is " . htmlentities($resp). "<br/>");
        return $resp;
        
    }
    
    
    /* 
     * =================================================================
     * Below this divider is relational table code
     * We don't actually require this now, however I'm leaving it here
     * in case we wish to revisit the functionality at all
     * =================================================================
     */
    
    /**
     * Update the results relational table
     * (used for weekly emails)
     * 
     * @param integer $iLotteryId Lottery ID
     */
    public static function updateResultsRelationalTables($iLotteryId)
    {
        
       // Table ID - need to ensure we can switch between languages here
       $iTableId = 2593359;
       
       
       // Get latest set of stored results from database
       $sResultsSQL = "SELECT ld.numbers,
                              UNIX_TIMESTAMP(ld.fk_lotterydate) as lotteryts,
                              lc.number_count,
                              lc.bonus_numbers
                       FROM lottery_draws ld
                       INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
                       WHERE lc.lottery_id = {$iLotteryId} 
                       ORDER BY ld.fk_lotterydate DESC
                       LIMIT 1";                      
       $aResultsRow = DAL::executeGetRow($sResultsSQL);           
       
       // Sort out main and bonus numbers here
       $aTemp = explode("|", $aResultsRow['numbers']);
       $aNumbers['normal'] = array_slice($aTemp, 0, $aResultsRow['number_count']);
       $aNumbers['bonus'] = array_slice($aTemp, $aResultsRow['number_count']);
       
       // Start to build array to be turned into XML
       // We're only dealing with one row of data
       $aToCURL[0]['lottery_type'] = $iLotteryId;
       $aToCURL[0]['_dummy']       = 0;    // Dummy field used for linking to DB
       $aToCURL[0]['draw_day']     = date('l', $aResultsRow['lotteryts']);
       $aToCURL[0]['draw_date']    = date('jS F Y', $aResultsRow['lotteryts']);
       
       foreach ($aNumbers['normal'] as $count=>$iNum)
       {
           $iRef = $count+1;
           $aToCURL[0]['ball_normal_' . $iRef] = $iNum;
       }
       
       foreach ($aNumbers['bonus'] as $count2=>$iNum2)
       {
           $iRef2 = $count2+1;
           $aToCURL[0]['ball_bonus_' . $iRef2] = $iNum2;          
       }
       
       // Build the CURL request
       $sUpdateTableString = Silverpop::buildCurlRequestRelationalTable($aToCURL, $iTableId);
       
       // Log into Engage and perform CURL request
       $sJSID = Silverpop::logIntoEngage();
       $aRet = Silverpop::runCurl($sUpdateTableString, $sJSID);
       
//        echo("<pre>");
//        print_r($aToCURL);
//        echo(htmlentities($sUpdateTableString));
//      
//       $aReturnArray = Silverpop::arrayBuild($aRet);
//       
//       print_d($aReturnArray);
//       die();      
    }
    
 
    /**
     * Update the jackpots relational table
     * (used for weekly emails)
     * 
     * @param integer $iLotteryId Lottery ID
     */
    public static function updateJackpotsRelationalTables($iLotteryId)
    {
       // Table ID - need to ensure we can switch between languages here
       $iTableId = 2593358;
       
       // Get jackpot information from database
       $sJackpotSQL =  "SELECT UNIX_TIMESTAMP (drawdate) as nextdrawts, jackpot, rollover
                        FROM r_lottery_dates 
                        WHERE drawdate > CURDATE() 
                        AND fk_lottery_id = {$iLotteryId}
                        LIMIT 1";            
       $aJackpotRow = DAL::executeGetRow($sJackpotSQL);           
       
       // Start to build array to be turned into XML
       // We're only dealing with one row of data
       $aToCURL[0]['lottery_type']   = $iLotteryId;
       $aToCURL[0]['_dummy']         = 0; // Dummy field used for linking to DB
       $aToCURL[0]['next_draw_day']  = date('l', $aJackpotRow['nextdrawts']);
       $aToCURL[0]['next_draw_date'] = date('jS F Y', $aJackpotRow['nextdrawts']);
       $aToCURL[0]['next_jackpot']   = $aJackpotRow['jackpot'];
       
       // Build CURL request
       $sUpdateTableString = Silverpop::buildCurlRequestRelationalTable($aToCURL, $iTableId);
       
       // Log into Engage and perform CURL request
       $sJSID = Silverpop::logIntoEngage();
       $aRet = Silverpop::runCurl($sUpdateTableString, $sJSID);
       
//        echo("<pre>");
//        echo(htmlentities($sUpdateTableString));
//      
//       $aReturnArray = Silverpop::arrayBuild($aRet);
//        
//       print_d($aReturnArray);
//       die();
    }
    
    
    /**
     * Build the CURL request for updating the Relational tables
     * This is a pared-down version of the main buildCurlRequest function
     * as the format is slightly different
     * 
     * @param array $aRows  Rows to add
     * @param integer $iTableId  Table ID
     * @return string
     */
    private static function buildCurlRequestRelationalTable($aRows, $iTableId)
    {
        $sAction = "InsertUpdateRelationalTable";
        
        $sRequestStart = '<Envelope><Body>';
        $sRequestEnd = '</Body></Envelope>';

        $sFinalRequest = $sRequestStart .
                         '<' . $sAction . '>' .
                         '<TABLE_ID>' . $iTableId . '</TABLE_ID>' .
                         '<ROWS>';
        
        // Loop through rows and add to XML
        foreach ($aRows as $aRow)
        {
            $sFinalRequest .= '<ROW>';
            foreach ($aRow as $sKey=>$sVal)
            {
                $sFinalRequest .= '<COLUMN name="' . $sKey . '">' .
                                  '<![CDATA[' . $sVal . ']]>' . 
                                  '</COLUMN>';
            }
            $sFinalRequest .= '</ROW>';
        }
        
        $sFinalRequest .= '</ROWS>' .
                          '</' . $sAction . '>' .
                          $sRequestEnd;
        
        // Return XML
        return $sFinalRequest;       
    }      
    
    
    
}
