<?php
namespace LL\Affiliates;

/**
 * Class Installer
 *
 * Following database tables will be created:
 * affiliate_groups, affiliates_groups_link, affiliate_advert_types, affiliate_adverts, affiliates_adverts_link,
 * affiliate_groups_adverts_link.
 *
 * Following tables will be modified:  members, bookings
 * Following tables will be seeded: affiliate_advert_types
 *
 * Following menu items will be created in administration: manage Groups, Manage Adverts under Affiliates
 * and Manage Affiliate Advert Types under Admin
 *
 * @package LL\Affiliates
 * @author Nikos Poulias
 */

use \LL\Installer\Table,
    \LL\Installer\Field,
    \LL\Installer\Constraint,
    \LL\Installer\Task
    ;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){

        $aJobs[]=new Task('\LL\Affiliates\Installer', 'installSchema', 'Install Affiliates Database');
        $aJobs[]=new Task('\LL\Affiliates\Installer', 'installMenuItems', 'Install Affiliates Admin Menu Items');

        return $aJobs;
    }

    static function installSchema(Task $oTask){
        self::installAffiliateTypes();
        self::installAffiliatesTable();

        return;

        self::installAffiliateGroups();
        self::installAffiliateGroupPivot();
        self::installAffiliateAdvertTypes();
        self::installAffiliateAdverts();
        self::populateAffiliateAdvertTypes();
        self::installAffiliateAdvertsPivot();
        self::installAffiliateGroupsAdvertPivot();
        self::alterMembersTable();
        self::alterBookingsTable();
    }

    static function installMenuItems(Task $oTask){
        self::importNewAdminPages();
    }


    static function installAffiliatesTable(){

        //Create table
        $oAffTable=new Table('affiliates');

        $oAffTable->addField(new Field('fk_type_id','int',11));

        //Add foreign key constraints
        $oAffTable->addConstraint(new Constraint('fk_type_id','affiliate_types','id'));

        //Run
        $oAffTable->compile();

    }

    static function installAffiliateGroups(){

        //Create table
        $oAffTable=new Table('affiliate_groups');

        //Add primary key
        $oAffTable->addIdField();

        //Add other fields
        $oAffTable->addField(new Field('name','varchar',255));
        $oAffTable->addField(new Field('details','text'));
        $oAffTable->addField(new Field('fk_group_owner','int',11));

        //Add foreign key constraints
        $oAffTable->addConstraint(new Constraint('fk_group_owner','admin_users','user_id'));

        //Run
        $oAffTable->compile();

    }

    static function installAffiliateTypes(){

        //Create table
        $oAffTable=new Table('affiliate_types');

        //Add primary key
        $oAffTable->addIdField();

        //Add other fields
        $oAffTable->addField(new Field('name','varchar',255));
        $oAffTable->addField(new Field('details','text'));

        //Run
        $oAffTable->compile();


        //CHange the links on the main nav
        \DAL::Update('admin_pages',array('filename'=>'affiliates','tablename'=>''),"filename='affiliates.php'");

    }

    static function installAffiliateGroupPivot(){

        //Create table
        $oAffGroupPivotTable=new Table('affiliates_groups_link');

        //Add primary key
        $oAffGroupPivotTable->addIdField();

        //Add other fields
        $oAffGroupPivotTable->addField(new Field('fk_affiliate_id','int',11));
        $oAffGroupPivotTable->addField(new Field('fk_affiliate_group_id','int',11));

        //Add foreign key constraints
        $oAffGroupPivotTable->addConstraint(new Constraint('fk_affiliate_id','admin_users','user_id'));
        $oAffGroupPivotTable->addConstraint(new Constraint('fk_affiliate_group_id','affiliate_groups','id'));

        //Run
        $oAffGroupPivotTable->compile();

    }

    static function installAffiliateAdvertTypes(){

        //Create table
        $oAffAdvTypesTable=new Table('affiliate_advert_types');

        //Add primary key
        $oAffAdvTypesTable->addIdField();

        //Add other fields
        $oAffAdvTypesTable->addField(new Field('name','varchar','255'));
        $oAffAdvTypesTable->addField(new Field('width','int',50));
        $oAffAdvTypesTable->addField(new Field('height','int',50));

        //Run
        $oAffAdvTypesTable->compile();

    }

    static function populateAffiliateAdvertTypes(){
        $aData=[];
        //Rectangles
        $aData[]=['name'=>'Medium Rectangle','width'=>300,'height'=>250];
        $aData[]=['name'=>'Vertical Rectangle','width'=>240,'height'=>400];
        $aData[]=['name'=>'Large Rectangle','width'=>336,'height'=>280];
        $aData[]=['name'=>'Rectangle','width'=>180,'height'=>150];
        $aData[]=['name'=>'3:1 Rectangle','width'=>300,'height'=>100];

        //Banners & Buttons
        $aData[]=['name'=>'Full Banner','width'=>468,'height'=>60];
        $aData[]=['name'=>'Half Banner','width'=>234,'height'=>60];
        $aData[]=['name'=>'Micro Bar','width'=>88,'height'=>31];
        $aData[]=['name'=>'Button 1','width'=>120,'height'=>90];
        $aData[]=['name'=>'Button 2','width'=>120,'height'=>60];
        $aData[]=['name'=>'Vertical Banner','width'=>120,'height'=>240];
        $aData[]=['name'=>'Square Button','width'=>125,'height'=>125];
        $aData[]=['name'=>'Leaderboard','width'=>728,'height'=>90];

        //Skyscrapers
        $aData[]=['name'=>'Wide Skyscraper','width'=>160,'height'=>600];
        $aData[]=['name'=>'Skyscraper','width'=>120,'height'=>600];
        $aData[]=['name'=>'Half Page Ad','width'=>300,'height'=>600];

        foreach($aData AS $aDataItem){
           // \DAL::Insert('affiliate_advert_types',$aDataItem);
        }

    }

    static function installAffiliateAdverts(){

        //Create table
        $oAffAdvTable=new Table('affiliate_adverts');

        //Add primary key
        $oAffAdvTable->addIdField();

        //Add other fields
        $oAffAdvTable->addField(new Field('name','varchar','255'));
        $oAffAdvTable->addField(new Field('html','text'));
        $oAffAdvTable->addField(new Field('fk_type_id','int',11));
        $oAffAdvTable->addField(new Field('is_flash','tinyint',4));
        $oAffAdvTable->addField(new Field('is_active','tinyint',4));
        $oAffAdvTable->addField(new Field('valid_from','datetime'));
        $oAffAdvTable->addField(new Field('valid_to','datetime'));

        //Add foreign key constraints
        $oAffAdvTable->addConstraint(new Constraint('fk_type_id','affiliate_advert_types','id'));

        //Run
        $oAffAdvTable->compile();

    }



    static function installAffiliateAdvertsPivot(){

        //Create table
        $oAffGroupPivotTable=new Table('affiliates_adverts_link');

        //Add primary key
        $oAffGroupPivotTable->addIdField();

        //Add other fields
        $oAffGroupPivotTable->addField(new Field('fk_advert_id','int',11));
        $oAffGroupPivotTable->addField(new Field('fk_affiliate_id','int',11));

        //Add foreign key constraints
        $oAffGroupPivotTable->addConstraint(new Constraint('fk_advert_id','affiliate_adverts','id'));
        $oAffGroupPivotTable->addConstraint(new Constraint('fk_affiliate_id','admin_users','user_id'));

        //Run
        $oAffGroupPivotTable->compile();

    }

    static function installAffiliateGroupsAdvertPivot(){

        //Create table
        $oAffGroupPivotTable=new Table('affiliate_groups_adverts_link');

        //Add primary key
        $oAffGroupPivotTable->addIdField();

        //Add other fields
        $oAffGroupPivotTable->addField(new Field('fk_advert_id','int',11));
        $oAffGroupPivotTable->addField(new Field('fk_affiliate_group_id','int',11));

        //Add foreign key constraints
        $oAffGroupPivotTable->addConstraint(new Constraint('fk_advert_id','affiliate_adverts','id'));
        $oAffGroupPivotTable->addConstraint(new Constraint('fk_affiliate_group_id','affiliate_groups','id'));

        //Run
        $oAffGroupPivotTable->compile();

    }

    static function alterMembersTable(){

        //Add new collumn
        \DAL::Query('ALTER TABLE members ADD COLUMN fk_source_advert_id INT(11);');

        //Add index
        \DAL::Query('ALTER TABLE members ADD INDEX(fk_source_advert_id)');

        //Add constraint
        \DAL::Query('ALTER TABLE members ADD CONSTRAINT members_affiliate_advert FOREIGN KEY (fk_source_advert_id) REFERENCES affiliate_adverts(id) ON DELETE CASCADE ON UPDATE CASCADE');
    }

    static function alterBookingsTable(){

        //Add new collumn
        \DAL::Query('ALTER TABLE bookings ADD COLUMN fk_source_advert_id INT(11);');

        //Add index
        \DAL::Query('ALTER TABLE bookings ADD INDEX(fk_source_advert_id)');

        //Add constraint
        \DAL::Query('ALTER TABLE bookings ADD CONSTRAINT bookings_affiliate_advert FOREIGN KEY (fk_source_advert_id) REFERENCES affiliate_adverts(id) ON DELETE CASCADE ON UPDATE CASCADE');
    }

    static function importNewAdminPages(){

        $sSql='SELECT id FROM admin_pages WHERE title="Affiliates" AND parent_id=0';
        $iParentId=\DAL::executeGetOne($sSql);

        if($iParentId>0){
            $aData=[];
            $aData[]=['title'=>'Manage Groups','filename'=>'generic-listing.php','tablename'=>'affiliate_groups','parent_id'=>$iParentId];
            $aData[]=['title'=>'Manage Adverts','filename'=>'generic-listing.php','tablename'=>'affiliate_adverts','parent_id'=>$iParentId];
            $aData[]=['title'=>'Affiliate Types','filename'=>'generic-listing.php','tablename'=>'affiliate_types','parent_id'=>129];


            foreach($aData AS $aDataItem){
                \DAL::Insert('admin_pages',$aDataItem);
            }
        }
        else{
            //Throw rocks
        }


        $sSql='SELECT id FROM admin_pages WHERE title="Admin" AND parent_id=0';
        $iParentId=\DAL::executeGetOne($sSql);

        if($iParentId>0){
            $aData=[];
            $aData[]=['title'=>'Manage Affiliate Advert Types','filename'=>'generic-listing.php','tablename'=>'affiliate_advert_types','parent_id'=>$iParentId];

            foreach($aData AS $aDataItem){
                \DAL::Insert('admin_pages',$aDataItem);
            }
        }
        else{
            //Throw anything
        }


        /**
         * Remove the white labels menu
         */
        \DAL::executeQuery("DELETE FROM admin_pages WHERE title='White Labels'");
    }

}
