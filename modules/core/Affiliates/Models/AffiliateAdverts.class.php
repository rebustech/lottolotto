<?php
/**
 * Model for adverts
 * @package LoveLotto
 * @subpackage Models
 * @author Jonathan Patchett
 */
class AffiliateAdverts{
    public $iUserId;
	public function test($iUserId){

    	$sSQL="SELECT name FROM handlers";
    	$oHandler=\DAL::executeGetOne($sSQL);
    	return $oHandler;
	}

	public function get_Affiliate_Adverts($iUserId){

		$sSQL="SELECT id, name, html, fk_type_id  FROM affiliate_adverts
               WHERE is_active=1
               AND current_timestamp() BETWEEN valid_from AND valid_to";
    	$oHandler=\DAL::executeQuery($sSQL);

    	return $oHandler;
	}

    /**
     * @param $affiliateAdvertId
     * @return null|array
     */
    public function getAffiliateAdvertById($affiliateAdvertId) {
        $sSql = "SELECT * FROM affiliate_adverts WHERE id = {$affiliateAdvertId}";
        $oHandler = \DAL::executeGetRow($sSql);

        return $oHandler;
    }


    /**
     * Get affiliate advert by id and user id.
     * Used for affiliates dashboard so affiliates can't spoof advert id
     * @param $affiliateAdvertId
     * @return array
     */
    public function getAffiliateAdvertByIdWithUserId($affiliateAdvertId) {
        $sSql = "SELECT * FROM affiliates_adverts_link
                 INNER JOIN affiliate_adverts ON affiliates_adverts_link.fk_advert_id = affiliate_adverts.id
                 WHERE affiliates_adverts_link.fk_advert_id = {$affiliateAdvertId}
                 AND affiliates_adverts_link.fk_affiliate_id = {$this->iUserId}";

        $return = \DAL::executeGetRow($sSql);

        // Return false for current affiliate
        // Check if current affiliate is part of a group that has this advert
        if (!$return) {
            $sSql = "SELECT * FROM affiliate_groups_adverts_link
                     INNER JOIN affiliates_groups_link ON affiliates_groups_link.fk_affiliate_id = {$this->iUserId}
                     WHERE fk_advert_id = {$affiliateAdvertId}";
        }

        $return = \DAL::executeGetRow($sSql);

        return $return;
    }

    public function getAffiliateAdvertTargetURLById($affiliateAdvertId) {
        $oHandler = $this->getAffiliateAdvertById($affiliateAdvertId);

        if (count($oHandler) === 0) {
            return null;
        }

        if (is_null($oHandler["target_url"])) {
            $oHandler["target_url"] = "http://www.maxlotto.com";
        }

        return $oHandler;
    }

    /**
     * @param null $type
     */
    private function getAffiliateAdvert($type = null, $advertID = null) {

        // Get banners if no type is passed in
        $sSql = "SELECT * FROM affiliates_adverts_link aal
                 INNER JOIN affiliate_adverts aa ON aal.fk_advert_id = aa.id
                 INNER JOIN affiliate_advert_types aat ON aa.fk_type_id = aat.id
                 WHERE fk_affiliate_id = {$this->iUserId}
                 AND aat.name != 'Text'
                 AND aat.name != 'Widget'
                 AND aa.is_active = 1
                 AND current_timestamp() BETWEEN aa.valid_from AND aa.valid_to";


        // Get an advert by ID for a specific affiliate
        if (!is_null($advertID)) {
            $sSql = "SELECT aa.*, aa.name AS aaName, aal.*, aat.* FROM affiliates_adverts_link aal
                 INNER JOIN affiliate_adverts aa ON aal.fk_advert_id = aa.id
                 INNER JOIN affiliate_advert_types aat ON aa.fk_type_id = aat.id
                 WHERE fk_affiliate_id = {$this->iUserId}
                 AND aa.id = {$advertID}
                 AND aa.is_active = 1
                 AND current_timestamp() BETWEEN aa.valid_from AND aa.valid_to";
        }

        // If a type is passed in then return that type of advert
        if (!is_null($type)) {
            $sSql = "SELECT * FROM affiliates_adverts_link aal
                     INNER JOIN affiliate_adverts aa ON aal.fk_advert_id = aa.id
                     INNER JOIN affiliate_advert_types aat ON aa.fk_type_id = aat.id
                     WHERE fk_affiliate_id = {$this->iUserId}
                     AND aat.name = '{$type}'
                     AND aa.is_active = 1
                     AND current_timestamp() BETWEEN aa.valid_from AND aa.valid_to";
        }

        // Return all adverts for current user or group a user belongs to
        if ($type === "all") {

            // Affiliate
            $sSql = "SELECT aa.name as aaName, aa.id as aaID, aa.*, aal.*, lotteries.comment, aat.*, ge.name AS ge_name, languages.* FROM affiliates_adverts_link aal
                 INNER JOIN affiliate_adverts aa ON aal.fk_advert_id = aa.id
                 INNER JOIN affiliate_advert_types aat ON aa.fk_type_id = aat.id
                 LEFT JOIN lotteries_cmn lotteries ON aa.fk_lottery_id = lotteries.lottery_id
                 LEFT JOIN game_engines ge ON ge.id = aa.fk_game_engine_id
                 LEFT JOIN languages ON aa.fk_language_id = languages.language_id
                 WHERE fk_affiliate_id = {$this->iUserId}
                 AND aa.is_active = 1
                 AND current_timestamp() BETWEEN aa.valid_from AND aa.valid_to";

            $oHandler = \DAL::executeQuery($sSql);

            // Group
            $sSql = "SELECT aa.name as aaName, aa.id as aaID, aa.*, lotteries.comment, aat.*, ge.name AS ge_name, languages.*  FROM affiliates_groups_link agl
                    INNER JOIN affiliate_groups_adverts_link agal ON agl.fk_affiliate_group_id = agal.fk_affiliate_group_id
                    INNER JOIN affiliate_adverts aa ON agal.fk_advert_id = aa.id
                    INNER JOIN affiliate_advert_types aat ON aa.fk_type_id = aat.id
                    LEFT JOIN lotteries_cmn lotteries ON aa.fk_lottery_id = lotteries.lottery_id
                    LEFT JOIN game_engines ge ON ge.id = aa.fk_game_engine_id
                    LEFT JOIN languages ON aa.fk_language_id = languages.language_id
                    WHERE agl.fk_affiliate_id = {$this->iUserId}
                    AND aa.is_active = 1
                    AND current_timestamp() BETWEEN aa.valid_from AND aa.valid_to;";
            $oHandler2 = \DAL::executeQuery($sSql);

            // Assign affiliate and group results
            $result = array_merge($oHandler, $oHandler2);
            return $result;
        }

        $oHandler = \DAL::executeQuery($sSql);
        return $oHandler;
    }

    /**
     * @return array
     */
    public function getTextAdverts() {
        return $this->getAffiliateAdvert("Text");
    }

    /**
     */
    public function getBanners() {
        return $this->getAffiliateAdvert();
    }

    /**
     * @param array
     */
    public function getWidgets() {
        return $this->getAffiliateAdvert("Widget");
    }

    public function getAll() {
        return $this->getAffiliateAdvert("all");
    }

    public function getAdvertByID($advertID) {
        return $this->getAffiliateAdvert(null, $advertID);
    }


    /**
     * Get number of clicks for an advert by affiliate
     * @param $advertID
     * @param $affiliateID
     * @return int
     */
    public function getClicksByAdvertIDAndAffiliateID($advertID, $affiliateID = null) {

        // Assign affiliate ID if null
        if (is_null($affiliateID)) {
            $affiliateID = $this->iUserId;
        }

        $sSql = "SELECT id FROM affiliate_adverts_tracking WHERE type = 'click' AND fk_affiliate_adverts_id = '{$advertID}' AND fk_affiliate_id = '{$affiliateID}'";

        $result = \DAL::executeQuery($sSql);

        // Count the number of rows returned;
        $result = count($result);

        return $result;
    }

    /**
     * Get number of impressions for an advert by affiliate
     * @param $advertID
     * @param $affiliateID
     * @return int
     */
    public function getImpressionsByAdvertIDAndAffiliateID($advertID, $affiliateID = null) {

        // Assign affiliate ID if null
        if (is_null($affiliateID)) {
            $affiliateID = $this->iUserId;
        }

        $sSql = "SELECT id FROM affiliate_adverts_tracking WHERE type = 'impression' AND fk_affiliate_adverts_id = '{$advertID}' AND fk_affiliate_id = '{$affiliateID}'";

        $result = \DAL::executeQuery($sSql);

        // Count the number of rows returned;
        $result = count($result);

        return $result;
    }


    public function getClicksAndImpressions() {
        $sSql = "SELECT DISTINCT affiliate_adverts.id, affiliate_adverts.name, affiliate_adverts.clicks, affiliate_adverts.impressions FROM affiliate_adverts";

        return $sSql;
    }

    public function getAdvertWithLotteryData($lotteryID) {
        $result = null;

        if ($lotteryID) {
            $sSql = "SELECT * FROM r_lottery_dates
                         JOIN lotteries_cmn ON r_lottery_dates.fk_lottery_id = lotteries_cmn.lottery_id
                         WHERE r_lottery_dates.fk_lottery_id = {$lotteryID}
                         ORDER BY r_lottery_dates.drawdate DESC";
            $result = \DAL::executeGetRow($sSql);
        }

        return $result;
    }


    public function getAdvertTypeInfo($advertID) {
        $advert = $this->getAffiliateAdvertById($advertID);
        $advertTypeID = $advert['fk_type_id'];

        // Get the advert type
        $sSql = "SELECT * FROM affiliate_advert_types WHERE id = {$advertTypeID}";
        $result = \DAL::executeGetRow($sSql);

        $return['name'] = $result['name'];

        // Return type of ad
        if ($result['name'] !== "Widget" && $result['name'] !== "Text") {
            $return['name'] = "banner";
        }
        $return['width'] = $result['width'];
        $return['height'] = $result['height'];
        return $return;
    }

}
