<?php

/**
 * Class AffiliatesTracker
 * Track clicks and impressions
 *
 * @author Wez Pyke
 * @version 1.0.0
 */
class AffiliatesAdvertTracker extends AffiliateAdverts {

    public $advertID;
    public $affiliateID;
    public $campaignID;
    public $userAgentString;
    public $referrer;
    public $ip;
    public $dateFrom = null;
    public $dateTo = null;

    public function __construct($advertID) {
        $this->advertID = $advertID;
    }


    /**
     * Update the click or impression number on an advert
     * @param $type
     */
    private function _updateAdvert($type) {
        $sSql = "UPDATE affiliate_adverts SET {$type} = {$type} + 1 WHERE id = {$this->advertID}";
        DAL::executeQuery($sSql);
    }

    /**
     * Track an impression or click
     * @param $type - Click or impression
     * @return array $currentAdvert
     */
    private function _track($type) {

        // Return if no advert id is provided
        if (!$this->advertID) {
            return ["target_url" => "/"];
        }

        // Get the advert for the ID provided
        $currentAdvert = $this->getAffiliateAdvertTargetURLById($this->advertID);

        // If advert ID is invalid then return a target url to root
        if (is_null($currentAdvert)) {
            return ["target_url" => "/"];
        }

        // Update click or impressions for advert
        $pluralType = $type . "s";
        $this->_updateAdvert($pluralType);

        // Set the date and time
        $datetime = date('Y-m-d H:i:s');

        // Assign data to fields
        $aData = array(
            'fk_affiliate_adverts_id' => $this->advertID,
            'type'       => $type,
            'ip'         => $this->ip,
            'user_agent' => $this->userAgentString,
            'datetime'   => $datetime,
            'fk_affiliate_id' => $this->affiliateID
        );

        // If campaignID is set then add it to be tracked
        if ($this->campaignID) {
            $aData['fk_campaign_id'] = $this->campaignID;
        }

        // Save the data
        DAL::Insert('affiliate_adverts_tracking', $aData);

        return $currentAdvert;
    }

    public function trackClick() {
        return $this->_track('click');
    }

    public function trackImpression() {
        return $this->_track('impression');
    }


    /**
     * Get the clicks and impressions for an advert
     * @return array $oResults
     */
    protected function get($type = null, $limit = null) {

        $sSql = "SELECT * FROM affiliate_adverts_tracking WHERE fk_affiliate_adverts_id = {$this->advertID} AND fk_affiliate_id = {$this->affiliateID}";

        // Return only clicks
        if ($type === "clicks") {
            $sSql .= " AND type = 'click'";
        }

        // Return only impressions
        if ($type === "impressions") {
            $sSql .= " AND type = 'impression'";
        }

        if ($this->dateFrom && $this->dateTo) {
            $sSql .= " AND datetime BETWEEN '{$this->dateFrom}' AND '{$this->dateTo}'";
        }

        $sSql .= " ORDER BY datetime DESC";

        if (!is_null($limit)) {
            $sSql .= " LIMIT {$limit}";
        }


        $oResults = \DAL::executeQuery($sSql);
        return $oResults;
    }


    /**
     * Get clicks
     * @return array
     */
    public function getClicks($limit = null) {
        $clicks = $this->get("clicks", $limit);

        return $clicks;
    }

    /**
     * Get impressions
     * @return array
     */
    public function getImpressions($limit = null) {
        $impressions = $this->get("impressions", $limit);

        return $impressions;
    }


    public function getClicksAndImpressions($limit = null) {
        $clicksAndImpressions = $this->get();
        return $clicksAndImpressions;
    }

    public function getClicksCount() {
        $clicks = count($this->getClicks());
        return $clicks;
    }

    public function getImpressionsCount() {
        $impressions = count($this->getImpressions());
        return $impressions;
    }

}