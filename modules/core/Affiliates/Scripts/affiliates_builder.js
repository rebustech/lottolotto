/**
 * Created by np0 on 7/8/14.
 */
var affiliatesBuilder={};

affiliatesBuilder.setup=function(){
    $('form').submit(affiliatesBuilder.prepareSave);
}

affiliatesBuilder.prepareSave=function(){
    var eForm=$('#contentarea form');

    var affiliatesContainer=$('div#affiliates.many_to_many_picklist_form');

    if(affiliatesContainer.length > 0){

        //Affiliates Data
        $('#affiliates .selectedItems li').each(function(){
            var eSelectedAffiliate=$('<input type="checkbox" checked="checked" name="affiliates[]" value="'+$(this).attr('data-id')+'"/>');
            eSelectedAffiliate.css('display','none');
            eForm.append(eSelectedAffiliate);
        });
    }


    var groupsContainer=$('div#groups.many_to_many_picklist_form');

    if(groupsContainer.length > 0){

        //Affiliates Data
        $('#groups .selectedItems li').each(function(){

            var eSelectedGroup=$('<input type="checkbox" checked="checked" name="groups[]" value="'+$(this).attr('data-id')+'"/>');
            eSelectedGroup.css('display','none');
            eForm.append(eSelectedGroup);
        });
    }


}
affiliatesBuilder.setup();