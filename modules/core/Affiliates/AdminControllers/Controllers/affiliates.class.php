<?php

//error_reporting(E_ALL);
/**
 * Affiliate Groups controller. Generates the display for a single group including basic details
 * affiliates in group and audit log
 * @package LoveLotto
 * @subpackage AdminControllers
 * @author J Patchett
 */
class affiliates extends AdminController implements LLSaveable{

    function index(){
        $this->listItems();
    }

    function edit(){
        $iId=$_GET['id'];
        $this->GenericDetails('affiliates',$iId);
    }

    function create(){
        $this->GenericDetails('affiliates');
    }

    function listItems(){

        $oBreadcrumbs=new BreadcrumbsControl();
        $oBreadcrumbs->addPage('Affiliates / Partner Management', 'fa-users');
        $oBreadcrumbs->addPage('Affiliates', 'fa-users');
        $oBreadcrumbs->addPage('Listing', 'fa-list');
        echo $oBreadcrumbs->Output();


        $tabs=new TabbedInterface();

        $oMainDetailsTab=new TabbedInterface(lang::get('all_partners'));
        $oMainDetailsTab->sIcon='fa-user';

        $oToolbar=new ToolbarControl();
        $oToolbar->AddControl(new ToolbarButtonControl('New',"/administration/affiliates/create",'fa-magic'));
        $oMainDetailsTab->AddControl($oToolbar);

        //Create a tab to show audit log
        $aItemsData=DAL::executeQuery('SELECT * FROM affiliates');
        $oItemsTable=new TableControl($aItemsData);
        $oItemsTable->getFieldsFromTable('affiliates');
        $oItemsTable->keepFields('affiliates','fk_user_id,company,web,city,country,commission');
        $oItemsTable->addAction(new TableRowActionControl('fk_user_id','<i class="fa fa-pencil" title="edit"></i>','/administration/affiliates/edit?id={fk_user_id}'));
        $oMainDetailsTab->AddControl($oItemsTable);

        $tabs->aItems['details']=$oMainDetailsTab;

        /*
        |--------------------------------------------------------------------------
        | Audit Tab
        |--------------------------------------------------------------------------
        */

        $oLogTab=new TabbedInterface(lang::get('audit_log'));
        $oLogTab->sIcon='fa-th-list';

        $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'affiliates\' ORDER BY `datetime` DESC LIMIT 0,50');

        $oItemsTable=new TableControl($aItemsData);
        $oItemsTable->getFieldsFromTable('audit_log');
        $oItemsTable->keepFields('audit_log','datetime,code,message');
        $oLogTab->AddControl($oItemsTable);

        $tabs->aItems[]=$oLogTab;

        echo $tabs->Output();

    }

    /**
     * Decide what to show in a generic details view
     * @param $sTablename
     * @param int $iId
     * @param null $aDefaultData
     * @param string $sPreviousPage
     * @param int $iWebsiteID
     * @param bool $bDeleteButton
     * @param null $iReturnID
     * @return string
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);


        $oBreadcrumbs=new BreadcrumbsControl();
        $oBreadcrumbs->addPage('Affiliates / Partner Management', 'fa-users');
        $oBreadcrumbs->addPage('Affiliates', 'fa-users','/administration/affiliates');
        $oBreadcrumbs->addPage('Listing', 'fa-list','/administration/affiliates/listItems');
        $oBreadcrumbs->addPage('Edit Item', 'fa-pencil');
        echo $oBreadcrumbs->Output();

        /*
        |--------------------------------------------------------------------------
        | Details Tab
        |--------------------------------------------------------------------------
        */

        $oAffiliate=\LL\Affiliates\Affiliate::getById($iId);

        $tabs=new TabbedInterface();

        $form=new SelfSubmittingForm();
        $form->sAction='saveItem?id='.$iId;

        $tabs=new TabbedInterface();
        $tabs->addSaveButton();

        $form->AddControl($tabs);

        $aAccountFieldsToShow=array('username'=>false,'firstname'=>false,'lastname'=>false,'email'=>false,'tel1'=>false,'tel2'=>false,'isactive'=>false);

        $oAccountDetailsTab=new TabbedInterface(lang::get('user_account'));
        $oAccountDetailsTab->sIcon='fa-key';
        $oAccountDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm('admin_users', $oAffiliate->fk_user_id , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false,$aAccountFieldsToShow);

        if(isset($_GET['id'])&& is_numeric($_GET['id'])){
            $oAccountDetailsTab->AddControl(new HtmlTagControl('h2', 'Change Password'));
            $oAccountDetailsTab->AddControl(new HtmlTagControl('p', 'Leave blank to leave unchanged'));
            $oAccountDetailsTab->AddControl(new HtmlFormTextInputControl('newpassword','New Password'));

            $oAccountDetailsTab->AddControl(new HtmlTagControl('h2', 'Change PIN'));
            $oAccountDetailsTab->AddControl(new HtmlTagControl('p', 'Leave blank to leave unchanged'));
            $oAccountDetailsTab->AddControl(new HtmlFormTextInputControl('newpin','New Pin'));
        } else {
            $oAccountDetailsTab->AddControl(new HtmlFormTextInputControl('newpassword','Password'));
            $oAccountDetailsTab->AddControl(new HtmlFormTextInputControl('newpin','Pin'));
        }

        $tabs->aItems[]=$oAccountDetailsTab;

        $oMainDetailsTab=new TabbedInterface(lang::get('affiliate_details'));
        $oMainDetailsTab->sIcon='fa-user';
        $oMainDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);
        $tabs->aItems[]=$oMainDetailsTab;



        /*
        |--------------------------------------------------------------------------
        | Commission Tab
        |--------------------------------------------------------------------------
         */
        if (isset($_GET['id']) && is_numeric($_GET['id'])) {

            $oCommission = new TabbedInterface(lang::get('commission'));
            $oCommission->sIcon = 'fa-sitemap';

            // Get default commission for each product
            $sAllProductsQuery = "SELECT * FROM game_engines";
            $aAllProducts = DAL::executeQuery($sAllProductsQuery);

            foreach ($aAllProducts as $product) {
                $sProductQuery = "SELECT * FROM affiliates_commissions WHERE fk_game_engine_id = {$product['id']} AND fk_affiliate_id = {$iId}";
                $aProductQuery = \DAL::executeGetRow($sProductQuery);

                // Grab custom commission if there is one
                if ($aProductQuery) {
                    $oCommission->AddControl(new HtmlFormTextInputControl('commission_' . $product['id'], $product['name'], $aProductQuery['commission']));
                } else {
                    $oCommission->AddControl(new HtmlFormTextInputControl('commission_' . $product['id'], $product['name'], $product['commission']));
                }

            }
            $tabs->aItems[] = $oCommission;
        }

        if(isset($_GET['id'])&& is_numeric($_GET['id'])){

            /*
            |--------------------------------------------------------------------------
            | Affiliates Tab
            |--------------------------------------------------------------------------
            */

            //Create a tab to show white labels that can make use of this engine
            $oGroupsTab=new TabbedInterface(lang::get('groups'));
            $oGroupsTab->sIcon='fa-sitemap';
            $aAllGroups=DAL::executeQuery('SELECT distinct wl.id, name FROM affiliates_groups_link gewl RIGHT JOIN affiliate_groups wl ON wl.id=gewl.fk_affiliate_group_id AND fk_affiliate_id='.$iId.' ORDER BY name');
            //Get assigned countries
            $aSelectedGroups=DAL::executeQuery('SELECT distinct wl.id, name FROM affiliates_groups_link gewl INNER JOIN affiliate_groups wl ON wl.id=gewl.fk_affiliate_group_id WHERE fk_affiliate_id='.$iId.' ORDER BY name');
            $oGroupsControl=new ManyToManyFormControl($aAllGroups,$aSelectedGroups);
            $oGroupsControl->sName='affiliategroups';
            $oGroupsControl->sCaption='Groups';
            $oGroupsTab->AddControl($oGroupsControl);
            $tabs->aItems[]=$oGroupsTab;

            //Create a tab to show white labels that can make use of this engine
            $oAdvertsTab=new TabbedInterface(lang::get('adverts'));
            $oAdvertsTab->sIcon='fa-eye';
            $aAllAdverts=DAL::executeQuery('SELECT distinct wl.id, name,wl.html, wl.is_active FROM affiliates_adverts_link gewl RIGHT JOIN affiliate_adverts wl ON wl.id=gewl.fk_advert_id AND fk_affiliate_id='.$iId.' WHERE wl.is_active=1 ORDER BY name');
            //Get assigned countries
            $aSelectedAdverts=DAL::executeQuery('SELECT distinct wl.id, name,wl.html, wl.is_active FROM affiliates_adverts_link gewl INNER JOIN affiliate_adverts wl ON wl.id=gewl.fk_advert_id WHERE fk_affiliate_id='.$iId.' ORDER BY name');
            $oAdvertsControl=new ManyToManyFormControl($aAllAdverts,$aSelectedAdverts);
            //Override the views on the advert control
            $oAdvertsControl->sItemView='/views/many_to_many_picklist_item_adverts';

            $oAdvertsControl->sName='affiliateadverts';
            $oAdvertsControl->sCaption='Groups';
            $oAdvertsTab->AddControl($oAdvertsControl);
            $tabs->aItems[]=$oAdvertsTab;

            /*
            |--------------------------------------------------------------------------
            | Audit Tab
            |--------------------------------------------------------------------------
            */


            $oCustomersTab=new TabbedInterface(lang::get('customers'));
            $oCustomersTab->sIcon='fa-users';

            $aCustomersData=DAL::executeQuery('SELECT * FROM members WHERE fk_affiliate_id='.intval($iId).' ORDER BY `member_id` DESC');
            $oCustomersTable=new TableControl($aCustomersData);
            $oCustomersTable->getFieldsFromTable('members');
            $oCustomersTable->keepFields('members','id,firstname,lastname,email,contact');
            $oCustomersTable->addAction(new TableRowActionControl('fk_user_id','<i class="fa fa-pencil" title="edit"></i>','/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id={member_id}'));
            $oCustomersTab->AddControl($oCustomersTable);

            $tabs->aItems[]=$oCustomersTab;



            $oOrdersTab=new TabbedInterface(lang::get('orders'));
            $oOrdersTab->sIcon='fa-credit-card';

            $aOrdersData=DAL::executeQuery('SELECT b.*,s.comment AS `status` FROM bookings b INNER JOIN `status` s ON b.fk_status_id=s.status_id WHERE fk_affiliate_id='.intval($iId).' ORDER BY `booking_id` DESC');
            $oOrdersTable=new TableControl($aOrdersData);
            $oOrdersTable->getFieldsFromTable('bookings');
            $oOrdersTable->keepFields('bookings','id,booking_reference,booking_date,total,promo');
            $oOrdersTable->addField(new TableField('status','varchar','Status',''));
            $oOrdersTable->addAction(new TableRowActionControl('fk_user_id','<i class="fa fa-pencil" title="edit"></i>','/administration/generic-details.php?tablename=bookings&id={booking_id}'));
            $oOrdersTab->AddControl($oOrdersTable);

            $tabs->aItems[]=$oOrdersTab;





            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';

            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'affiliate_groups\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');

            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,code,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems[]=$oLogTab;

        }

        /*
        |--------------------------------------------------------------------------
        | Combine and return
        |--------------------------------------------------------------------------
        */

        $aItems[]=$form->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }


        echo $sOut;
    }

    /**
     * Handles a post as part of any admin controller
     * Typically this will pass the post into custom controls
     */
    public function afterGenericFormPost(){
        if(isset($_GET['id'])&& is_numeric($_GET['id'])){
            $sSql='DELETE FROM affiliates_groups_link WHERE fk_affiliate_group_id='.$_GET['id'];
            \DAL::Query($sSql);
            if(!empty($_POST['affiliates'])){
                foreach($_POST['affiliates'] AS $affiliate){
                    $data=['fk_affiliate_group_id'=>$_GET['id'],'fk_affiliate_id'=>$affiliate];
                    \DAL::Insert('affiliates_groups_link',$data);
                }
            }
        }
    }

    function saveItem(){
        /**
         * Redirect back to the edit page (with js as output already started by system)
         */
        $oBreadcrumbs=new BreadcrumbsControl();
        $oBreadcrumbs->addPage('Affiliates / Partner Management', 'fa-users');
        $oBreadcrumbs->addPage('Affiliates', 'fa-users');
        $oBreadcrumbs->addPage('Listing', 'fa-list');
        $oBreadcrumbs->addPage('Edit Item', 'fa-pencil');
        $oBreadcrumbs->addPage('Save', 'fa-pencil');
        echo $oBreadcrumbs->Output();

        $iId=$_GET['id'];

        /**
         * Process the save
         */


        //User
        $aData=array(
            'username'=>$_POST['username'],
            'firstname'=>$_POST['firstname'],
            'lastname'=>$_POST['lastname'],
            'email'=>$_POST['email'],
            'tel1'=>$_POST['tel1'],
            'tel2'=>$_POST['tel2'],
            'isactive'=>($_POST['isactive'])?1:0,
            'fk_admintype_id'=>2
        );

        if($iId>0){
            \DAL::Update('admin_users', $aData, 'user_id='.$iId);
        }else{
            $iId=\DAL::Insert('admin_users', $aData,true,'INSERT');
        }

        //Affiliate
        $aData=array(
            'company'=>$_POST['company'],
            'web'=>$_POST['web'],
            'address1'=>$_POST['address1'],
            'address2'=>$_POST['address2'],
            'zipcode'=>$_POST['zipcode'],
            'city'=>$_POST['city'],
            'region'=>$_POST['region'],
            'country'=>$_POST['country'],
            'fax'=>$_POST['fax'],
            'vatno'=>$_POST['vatno'],
            'commission'=>$_POST['commission'],
            'fk_user_id'=>$iId
        );
        \DAL::Insert('affiliates', $aData,true);

        //Groups
        $sGroups=implode(',',$_POST['affiliategroups']);
        if(sizeof($sGroups)==0){
            \DAL::executeQuery('DELETE FROM affiliates_groups_link WHERE fk_affiliate_id='.$iId);
        }else{
            \DAL::executeQuery('DELETE FROM affiliates_groups_link WHERE fk_affiliate_id='.$iId.' AND fk_affiliate_group_id NOT IN ('.$sGroups.')');
        }
        if($_POST['affiliategroups']){
            foreach($_POST['affiliategroups'] as $iGroup){
                \DAL::Insert('affiliates_groups_link', array('fk_affiliate_id'=>$iId,'fk_affiliate_group_id'=>$iGroup));
            }
        }
        //Adverts
        $sAdverts=implode(',',$_POST['affiliateadverts']);
        if(sizeof($sAdverts)==0){
            \DAL::executeQuery('DELETE FROM affiliates_adverts_link WHERE fk_affiliate_id='.$iId);
        }else{
            \DAL::executeQuery('DELETE FROM affiliates_adverts_link WHERE fk_affiliate_id='.$iId.' AND fk_advert_id NOT IN ('.$sAdverts.')');
        }
        if($_POST['affiliateadverts']){
            foreach($_POST['affiliateadverts'] as $iAdvert){
                \DAL::Insert('affiliates_adverts_link', array('fk_affiliate_id'=>$iId,'fk_advert_id'=>$iAdvert));
            }
        }

        /**
         * Change Password?
         */
        if($_POST["newpassword"]!=''){
            if(UsersAdmin::changePassword($iId, $_POST["newpassword"])){
                echo "Password changed.";
            }
            else{
                echo "Problem encountered while changing password. {messagetype}achtungFail{/messagetype}";
            }
        }

        /**
         * Change PIN?
         */
        if($_POST["newpin"]!=''){
            if(UsersAdmin::changePin($iId, $_POST["newpin"])){
                    echo "PIN changed.";
            }
            else{
                    echo "Problem encountered while changing PIN. {messagetype}achtungFail{/messagetype}";
            }
        }

        /**
         * Commission
         */
        // Do query to see if row exists in affiliates_commissions
        $sSql = "SELECT * FROM game_engines";
        $gameEngines = \DAL::executeQuery($sSql);

        foreach ($gameEngines as $gameEngine) {
            $defaultCommission = $gameEngine['commission'];
            $field = $_POST['commission_' . $gameEngine['id']];

            // Remove commission_ from form data
            $field = str_replace('commission_', '', $field);

            // Don't do anything if the field and default commission are the same
            if ($field !== $defaultCommission) {

                $sSql = "SELECT * FROM affiliates_commissions WHERE fk_game_engine_id = {$gameEngine['id']} AND fk_affiliate_id = {$iId}";
                $affiliateCommission = \DAL::executeGetOne($sSql);

                // If row exists then update
                if ($affiliateCommission) {

                    $sSql = "UPDATE affiliates_commissions SET commission = {$field} WHERE fk_game_engine_id = {$gameEngine['id']} AND fk_affiliate_id = {$iId}";
                    \DAL::executeQuery($sSql);
                } else {

                    // Create new commission row for user
                    $sSql = "INSERT INTO affiliates_commissions (fk_game_engine_id, fk_affiliate_id, commission) VALUES({$gameEngine['id']}, {$iId}, {$field})";
                    \DAL::executeQuery($sSql);
                }


            } else {

                // Same as default, remove any rows from affiliates_commissions
                $sSql = "DELETE FROM affiliates_commissions WHERE fk_game_engine_id = {$gameEngine['id']} AND fk_affiliate_id = {$iId}";
                \DAL::executeQuery($sSql);
            }


        }


        echo '<p><i class="fa fa-circle-o-notch fa-spin"></i> Saving Details... Please Wait</p>';
        echo '<script>document.location="edit?id='.$iId.'#0";</script>';
        die();
    }
}