<?php

/**
 * Adverts controller. Generates the display for a single advert including basic details
 * affiliates associated with advert, affiliate groups associated with advert and audit log
 * @package LoveLotto
 * @subpackage AdminControllers
 * @author Nikos Poulias
 */
class affiliate_adverts extends AdminController implements LLSaveable{

    /**
     *
     * Decide what to show in a generic details view
     * @param $sTablename
     * @param int $iId
     * @param null $aDefaultData
     * @param string $sPreviousPage
     * @param int $iWebsiteID
     * @param bool $bDeleteButton
     * @param null $iReturnID
     * @return string
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        /*
        |--------------------------------------------------------------------------
        | Details Tab
        |--------------------------------------------------------------------------
        */
        $tabs=new TabbedInterface();
        $oDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oDetailsTab->sIcon='fa-user';

        $oMainDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oMainDetailsTab->sIcon='fa-user';
        $oMainDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
        //$oDetailstabs->aItems['main']=$oMainDetailsTab;
        $tabs->aItems['details']=$oMainDetailsTab;

        ?>

        <script>
            $(document).ready(function () {

                var type = $("#field_lookup_fk_type_id").val();

                if (type !== "Flash") {
                    // Hide flash fallback url on load
                    $('#field_flash_fallback_url').parent().parent().hide();
                }

                $('body').on('click', '[data-return-text]', function () {
                    var selectedType = $(this).data('return-text');

                    // Hide or show flash fallback url field
                    if (selectedType === "Flash") {
                        $('#field_flash_fallback_url').parent().parent().show();
                    } else {
                        $('#field_flash_fallback_url').parent().parent().hide();
                    }
                });
            });
        </script>

        <?php

        if(isset($_GET['id'])&& is_numeric($_GET['id'])){
            /*
           |--------------------------------------------------------------------------
           | Affiliates Tab
           |--------------------------------------------------------------------------
           */

            ?>

            <script>
                $(document).ready(function () {
                    var type = $("#field_lookup_fk_type_id").val();
                    var advertID = <?php echo $_GET['id']; ?>;
                    if (type === "Text" || type === "Widget" || type === "Flash") {
                        $(".preview-banner").remove();
                    }
                });
            </script>

            <div class="preview-banner">
                <img src="<?php echo WEBSITEMAINURL ?>/tracker.php?trackingkey=<?php echo urlencode(base64_encode($_GET['id'])) ?>&do-not-track=true&banner=true">
            </div>

            <?php

            //Create a tab to show available affiliates
            $oAffiliatesTab=new TabbedInterface(lang::get('affiliates'));
            $oAffiliatesTab->sIcon='fa-users';

            //Get all available affiliates for the picker


            if($iId>0){
                $sAllAffiliatesSql='SELECT t1.fk_user_id AS id, CONCAT_WS(" ",t2.firstname,t2.lastname) as name
                                FROM affiliates t1
                                JOIN admin_users t2 ON t2.user_id=t1.fk_user_id
                                WHERE t1.fk_user_id NOT IN (
                                SELECT fk_affiliate_id FROM affiliates_adverts_link WHERE fk_advert_id='.(int)$iId.'
                                )';

            }
            else{
                $sAllAffiliatesSql='SELECT t1.fk_user_id AS id, CONCAT_WS(" ",t2.firstname,t2.lastname) as name
                                FROM affiliates t1
                                JOIN admin_users t2 ON t2.user_id=t1.fk_user_id';
            }
            $aAllAffiliates=DAL::executeQuery($sAllAffiliatesSql);
            //Get assigned affiliates
            $sSelectedAffiliates='SELECT t1.fk_user_id AS id, CONCAT_WS(" ",t2.firstname,t2.lastname) as name
                                FROM affiliates t1
                                JOIN admin_users t2 ON t2.user_id=t1.fk_user_id
                                JOIN affiliates_adverts_link t3 ON t3.fk_affiliate_id=t1.fk_user_id
                                WHERE t3.fk_advert_id='.(int)$iId;

            $aSelectedAffiliates=DAL::executeQuery($sSelectedAffiliates);
            $oAffiliatesControl=new AdvertsManyToManyPickListControl($aAllAffiliates,$aSelectedAffiliates);
            $oAffiliatesControl->sName='affiliates';
            $oAffiliatesControl->sCaption='Affiliates';

            $oAffiliatesTab->AddControl($oAffiliatesControl);
            $tabs->aItems['affiliates']=$oAffiliatesTab;


            /*
            |--------------------------------------------------------------------------
            | Groups Tab
            |--------------------------------------------------------------------------
            */

            //Create a tab to show available affiliates
            $oAffiliatesGroupTab=new TabbedInterface(lang::get('groups'));
            $oAffiliatesGroupTab->sIcon='fa-users';

            //Get all available affiliates for the picker


            if($iId>0){
                $sAllAffiliateGroupsSql='SELECT id,name
                                      FROM affiliate_groups
                                      WHERE id NOT IN (
                                      SELECT fk_affiliate_group_id FROM affiliate_groups_adverts_link
                                      WHERE fk_advert_id='.(int)$iId.'
                                      )';

            }
            else{
                $sAllAffiliateGroupsSql='SELECT id,name
                                      FROM affiliate_groups
                                      ORDER BY name ASC';
            }
            $aAllAffiliateGroups=DAL::executeQuery($sAllAffiliateGroupsSql);

            //Get assigned affiliates
            $sSelectedAffiliatesGroups='SELECT t1.id,t1.name
                                    FROM affiliate_groups t1
                                    LEFT JOIN affiliate_groups_adverts_link t2 ON t2.fk_affiliate_group_id=t1.id
                                    WHERE t2.fk_advert_id='.(int)$iId;

            $aSelectedAffiliatesGroups=DAL::executeQuery($sSelectedAffiliatesGroups);
            $oAffiliatesGroupControl=new AffGroupsManyToManyPickListControl($aAllAffiliateGroups,$aSelectedAffiliatesGroups);
            $oAffiliatesGroupControl->sName='groups';
            $oAffiliatesGroupControl->sCaption='Groups';

            $oAffiliatesGroupTab->AddControl($oAffiliatesGroupControl);
            $tabs->aItems['groups']=$oAffiliatesGroupTab;


            /*
            |--------------------------------------------------------------------------
            | Audit Tab
            |--------------------------------------------------------------------------
            */

            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';

            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'affiliate_adverts\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');

            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,code,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems[]=$oLogTab;
        }
        $aItems[]=$tabs->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }



        return $sOut;
    }

    /**
     * Handles a post as part of any admin controller
     * Typically this will pass the post into custom controls
     */
    public function afterGenericFormPost()
    {
        if(isset($_GET['id'])&& is_numeric($_GET['id'])){
            $sSql='DELETE FROM affiliates_adverts_link WHERE fk_advert_id='.$_GET['id'];
            \DAL::Query($sSql);
            if(!empty($_POST['affiliates'])){

                foreach($_POST['affiliates'] AS $affiliate){
                    $data=['fk_advert_id'=>$_GET['id'],'fk_affiliate_id'=>$affiliate];
                    \DAL::Insert('affiliates_adverts_link',$data);
                }
            }

            $sSql='DELETE FROM affiliate_groups_adverts_link WHERE fk_advert_id='.$_GET['id'];
            \DAL::Query($sSql);
            if(!empty($_POST['groups'])){

                foreach($_POST['groups'] AS $group){
                    $data=['fk_advert_id'=>$_GET['id'],'fk_affiliate_group_id'=>$group];
                    \DAL::Insert('affiliate_groups_adverts_link',$data);
                }
            }
        }


    }
}
