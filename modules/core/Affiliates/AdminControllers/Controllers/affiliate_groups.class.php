<?php

//error_reporting(E_ALL);
/**
 * Affiliate Groups controller. Generates the display for a single group including basic details
 * affiliates in group and audit log
 * @package LoveLotto
 * @subpackage AdminControllers
 * @author Nicholas Poulias
 */
class affiliate_groups extends AdminController implements LLSaveable{

    /**
     * Decide what to show in a generic details view
     * @param $sTablename
     * @param int $iId
     * @param null $aDefaultData
     * @param string $sPreviousPage
     * @param int $iWebsiteID
     * @param bool $bDeleteButton
     * @param null $iReturnID
     * @return string
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        /*
        |--------------------------------------------------------------------------
        | Details Tab
        |--------------------------------------------------------------------------
        */
        $tabs=new TabbedInterface();
        $oDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oDetailsTab->sIcon='fa-user';

        $oMainDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oMainDetailsTab->sIcon='fa-user';
        $oMainDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
        $tabs->aItems['details']=$oMainDetailsTab;

        if(isset($_GET['id'])&& is_numeric($_GET['id'])){

            /*
            |--------------------------------------------------------------------------
            | Affiliates Tab
            |--------------------------------------------------------------------------
            */

            //Create a tab to show available affiliates
            $oAffiliatesTab=new TabbedInterface(lang::get('affiliates'));
            $oAffiliatesTab->sIcon='fa-users';

            //Get all available affiliates for the picker


            if($iId>0){
                $sAllAffiliatesSql='SELECT t1.fk_user_id AS id, CONCAT_WS(" ",t2.firstname,t2.lastname) as name
                                FROM affiliates t1
                                JOIN admin_users t2 ON t2.user_id=t1.fk_user_id
                                WHERE t1.fk_user_id NOT IN (
                                SELECT fk_affiliate_id FROM affiliates_groups_link WHERE fk_affiliate_group_id='.(int)$iId.'
                                )';

            }
            else{
                $sAllAffiliatesSql='SELECT t1.fk_user_id AS id, CONCAT_WS(" ",t2.firstname,t2.lastname) as name
                            FROM affiliates t1
                            JOIN admin_users t2 ON t2.user_id=t1.fk_user_id';
            }
            $aAllAffiliates=DAL::executeQuery($sAllAffiliatesSql);

            //Get assigned affiliates
            $sSelectedAffiliates='SELECT t1.fk_user_id AS id, CONCAT_WS(" ",t2.firstname,t2.lastname) as name
                                FROM affiliates t1
                                JOIN admin_users t2 ON t2.user_id=t1.fk_user_id
                                JOIN affiliates_groups_link t3 ON fk_affiliate_id=t1.fk_user_id
                                WHERE t3.fk_affiliate_group_id='.(int)$iId;


            $aSelectedAffiliates=DAL::executeQuery($sSelectedAffiliates);
            $oAffiliatesControl=new AffGroupsManyToManyPickListControl($aAllAffiliates,$aSelectedAffiliates);
            $oAffiliatesControl->sName='affiliates';
            $oAffiliatesControl->sCaption='Affiliates';

            $oAffiliatesTab->AddControl($oAffiliatesControl);
            $tabs->aItems['affiliates']=$oAffiliatesTab;

            /*
            |--------------------------------------------------------------------------
            | Audit Tab
            |--------------------------------------------------------------------------
            */

            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';

            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'affiliate_groups\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');

            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,code,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems[]=$oLogTab;

        }

        /*
        |--------------------------------------------------------------------------
        | Combine and return
        |--------------------------------------------------------------------------
        */

        $aItems[]=$tabs->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }


        return $sOut;
    }

    /**
     * Handles a post as part of any admin controller
     * Typically this will pass the post into custom controls
     */
    public function afterGenericFormPost(){
        if(isset($_GET['id'])&& is_numeric($_GET['id'])){
            $sSql='DELETE FROM affiliates_groups_link WHERE fk_affiliate_group_id='.$_GET['id'];
            \DAL::Query($sSql);
            if(!empty($_POST['affiliates'])){
                foreach($_POST['affiliates'] AS $affiliate){
                    $data=['fk_affiliate_group_id'=>$_GET['id'],'fk_affiliate_id'=>$affiliate];
                    \DAL::Insert('affiliates_groups_link',$data);
                }
            }
        }
    }
}