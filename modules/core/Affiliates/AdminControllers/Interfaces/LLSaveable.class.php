<?php

interface LLSaveable{
    /**
     * Handles a post as part of any admin controller
     * Typically this will pass the post into custom controls
     */
    public function afterGenericFormPost();
}