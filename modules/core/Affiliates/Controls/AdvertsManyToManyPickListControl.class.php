<?php

class AdvertsManyToManyPickListControl extends ManyToManyFormControl{

    /**
     * @var string
     */
    var $sView='/Views/many_to_many_picklist';

    /**
     * @var string
     */
    var $sItemView='/../Affiliates/Controls/Views/many_to_many_picklist_item';

    /**
     * JS scripts to load
     * @var array
     */
    var $aScripts=['/administration/API/ControlsAssets/affiliatesManyToManyControl'];

    /**
     * @param array $aAllItems
     * @param array $aSelectedItems
     */
    function __construct(array $aAllItems, array $aSelectedItems) {
        parent::__construct($aAllItems, $aSelectedItems,$this->aScripts);
    }

    /**
     * @return string
     */
    function output(){
        $oView=new LLView();
        $oView->sName=$this->sName;
        $oView->sCaption=$this->sCaption;
        $oView->sAllItems=$this->getItemsOutput($this->aAllItems);
        $oView->sChosenItems=$this->getItemsOutput($this->aSelectedItems);
        return $oView->output(__DIR__.$this->sView);
    }

}