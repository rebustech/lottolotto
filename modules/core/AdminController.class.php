<?php
/**
 * Abstract base class for AdminControllers. Admin Controllers are used by the backend
 * to produce the output of the admin panel. These provide overrides for existing pages
 * in the original admin system. Eventually we should be able to build a router for these
 * but for now generic-details.php and generic-table.php are the main controllers and
 * have had routing ability added to them.
 *
 * @todo Move this into LL namespace
 *
 * @package LoveLotto
 * @subpackage Core
 * @author J.Patchett
 */
class AdminController extends \LL\Controller{
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        return 'Not Implemented';
    }
}