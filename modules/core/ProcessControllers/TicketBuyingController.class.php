<?php

class TicketBuyingController extends ProcessController{

    static function selectEngineToUse($iLotteryId,$sDrawDate,$iJackpot){
        $dDrawDate=date('Y-m-d H:i:s',strtotime($sDrawDate));

        /**
         * Get a list of available ticket purchasing engines for this lottery
         */
        $sSql='SELECT * FROM ticket_engines WHERE fk_lottery_id='.$iLotteryId.' AND is_active=1 AND is_suspended=0';
        $aTicketEngineList=DAL::executeQuery($sSql);


        /**
         * Now find the best price engine that can handle this request
         * Start by assigning the null engine - if we use this it means that
         * no suitable engine could be find and we'll have to notifiy someone
         */
        $oEngine=new NullTicketEngine();
        $fBestPrice=10000000000000000;
        if(sizeof($aTicketEngineList)==0){
            AuditLog::LogItem('Unable to find any hedging engines for Lottery ID '.$iLotteryId.' on '.$sDrawDate,'NO_HEDGING_AVAILABLE','TicketBuyingController',0);
        }else{
            AuditLog::LogItem('Validating hedging engines for Lottery ID '.$iLotteryId.' on '.$sDrawDate,'CHECKING_DRAW_HEDGING','TicketBuyingController',0);
            foreach($aTicketEngineList as $aTicketEngineData){
                $oTicketEngine=BaseTicketEngine::getById($aTicketEngineData['id']);
                /**
                 * Check if the engine can provide for these odds with the maximum exposure
                 */
                if($oTicketEngine->canProvide($iJackpot)){
                    /**
                     * If it can see if it's the best price and over-rule $oEngine if it is
                     */
                    $fPrice=$oTicketEngine->getCost($iJackpot);
                    if($fPrice<$fBestPrice){
                        $fBestPrice=$fPrice;
                        $oEngine=$oTicketEngine;
                    }
                }
            }
        }
        if($oEngine instanceof NullTicketEngine){
            AuditLog::LogItem('Unable to find a suitable hedging engine for '.$iLotteryId.' on '.$sDrawDate,'WARN_NO_HEDGING_FOR_DRAW','TicketBuyingController',0);
        }else{
            AuditLog::LogItem('Using '.$oTicketEngine->name.' to hedge Lottery ID '.$iLotteryId.' on '.$sDrawDate,'FOUND_DRAW_HEDGING','TicketBuyingController',0);
        }
        return $oEngine;
    }


    static function purchaseTicket($oBookingItem){
        return; //Hedging now based at the draw level.
        /**
         * Find out which engine will be used. We'll assign the engine to this purchase
         * then attempt to purchase. Some engines purcahse in bulk by looking for unpurchased
         * tickets assigned to them
         */
        $oTicketEngine=TicketBuyingController::selectEngineToUse($oBookingItem);
        if($oTicketEngine->id>0){
            $oBookingItem->fk_ticket_engine_id=$oTicketEngine->id;
            $oBookingItem->ticket_engine_assigned_at=date('Y-m-d H:i:s');
            $oBookingItem->hedging_cost=$oTicketEngine->getCost($oBookingItem);
            $oBookingItem->log('TICKET_ENGINE_SELECTED','Selected ticket engine '.$oTicketEngine->id);

            /**
             * Attempt to buy the ticket now - only some types of engine will provide live purcahsing
             */
            if($oTicketEngine->lodgeBet($oBookingItem)){
                $oBookingItem->purchased=1;
                $oBookingItem->purchased_at=date('Y-m-d H:i:s');
                $oBookingItem->log('TICKET_PURCHASED');
            }
        }else{
            $oBookingItem->log('TICKET_ENGINE_NOT_SELECTED','Unable to select a ticket engine');
        }
        $oBookingItem->save();
    }
}