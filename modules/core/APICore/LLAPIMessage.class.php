<?php

namespace LL\API;

class Message{
    var $sCode;
    var $sMessage;
    var $iMessageType;

    static $aMessages;

    const MESSAGE_TYPE_INFO=1;
    const MESSAGE_TYPE_WARNING=2;
    const MESSAGE_TYPE_ERROR=3;

    function __construct($sCode,$sMessage='',$iMessageType=self::MESSAGE_TYPE_INFO){
        $this->sCode=$sCode;
        $this->sMessage=$sMessage;
        $this->iMessageType=$iMessageType;

        self::$aMessages[$sCode]=$this; //Using the code as the key so we don't repeat the same message;
    }
}