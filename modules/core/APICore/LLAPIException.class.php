<?php

namespace LL\API;

class Exception extends \LottoException{

    function __construct($sCode = NULL, $sMessage = NULL) {
        parent::__construct($sCode, $sMessage);

        //Add the error to the API message history
        new Message($sCode,$sMessage,Message::MESSAGE_TYPE_ERROR);
    }

}