<?php

namespace LL\PaymentProcessing;

class Installer{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\PaymentProcessing\Installer', 'installPaymentProcessingModule', 'Install payment processing module');
        return $aJobs;
    }

    static function installPaymentProcessingModule(){
        $iAdminSectionId=\DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='Customers'");
        //Get the ID of the admin section
        $aData=array('title'=>'Pending Winnings',
                     'filename'=>'pending-winnings.php',
                     'parent_id'=>$iAdminSectionId,
                     'icon'=>'fa-edit');
        
        if(\DAL::Query("SELECT * FROM admin_pages WHERE filename='pending-winnings.php'")==0){
            \DAL::Insert('admin_pages', $aData);
        }else{
            \DAL::Update('admin_pages', $aData, "title='Pending Winnings'"); 
        }

        self::installModule('Payment Processing winnings');
    }
    
}