<?php

class PaymentAjax extends \LL\AjaxController{
    var $bAjax=true;

    /**
     * /administration/API/InstallerAjax/status
     *
     * This gets a list of all the jobs on the current installation run along with
     * the current status. This data is pulled from memcache key 'InstallTasks' which
     * is updated by the installer.
     */
    function status(){
        session_write_close();
        echo json_encode(LL\PaymentProcessing\Status::getStore());
        die();
    }

}
