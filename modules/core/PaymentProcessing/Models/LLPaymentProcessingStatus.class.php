<?php

namespace LL\PaymentProcessing;

class Status{
    static $current;
    
    static $storeKey;
    
    var $status;
    var $title;
    var $iStep;
    var $aSteps;
    
    static function getStore(){        
        return \LLCache::getObject(self::getStoreKey());
    }
    
    static function updateStore(){
        \LLCache::addObject(self::getStoreKey(), self::getCurrent());        
    }       
    
    static function getStoreKey(){
        //return 'paymentStatus-'.base64_encode($_SERVER['HTTP_COOKIE']);
        
        if (array_key_exists('paymentStatusStoreKey', $_SESSION))
        {
            return $_SESSION['paymentStatusStoreKey'];
        }
        
        return self::$storeKey;
    }
    
    static function generateStoreKey(){
        
        $sStoreKey = uniqid();
        
        self::$storeKey=$sStoreKey;
        self::updateStore();
        
        // Add to session
        $_SESSION['paymentStatusStoreKey'] = $sStoreKey;
        
        return $sStoreKey;
    }
       
    static function getCurrent(){
        if(!self::$current) self::$current=new Status();
        return self::$current;
    }
    
    static function setStatus($sStatus){
        self::getCurrent()->status=$sStatus;
        self::updateStore();
    }

    static function setTitle($sTitle){
        self::getCurrent()->title=$sTitle;
        self::updateStore();
    }
    
    static function setStep($iStep,$sTitle=null){
        if($sTitle===null){
            self::getCurrent()->iStep=$iStep;
        }else{
            self::getCurrent()->aSteps[$iStep]=$sTitle;
        }
        self::updateStore();
    }
    
    static function reset(){
        self::$current=new Status();
        self::updateStore();
    }
    
}