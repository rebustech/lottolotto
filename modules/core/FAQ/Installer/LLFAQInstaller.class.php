<?php
/**
 * Installs the New "Variants CMS"
 * @package LoveLotto
 * @subpackage VariantsCMS
 */

namespace LL\FAQ;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('LL\FAQ\Installer', 'installDatabase', 'Install Variants CMS Database');
        return $aJobs;
    }

    /**
     * Adds some CMS fields
     */
    static function installDatabase(\LL\Installer\Task $oTask){
        $oTable=new \LL\Installer\Table('faq_cmn');
        $oTable->addField(new \LL\Installer\Field('question', 'int',11));
        $oTable->addField(new \LL\Installer\Field('answer', 'int',11));
        $oTable->addConstraint(new \LL\Installer\Constraint('question', 'variants_blocks', 'id'));
        $oTable->addConstraint(new \LL\Installer\Constraint('answer', 'variants_blocks', 'id'));
        $oTable->compile();

        $oTable=new \LL\Installer\Table('faq_categories_cmn');
        $oTable->addField(new \LL\Installer\Field('title', 'int',11));
        $oTable->addField(new \LL\Installer\Field('introduction', 'int',11));
        $oTable->addConstraint(new \LL\Installer\Constraint('title', 'variants_blocks', 'id'));
        $oTable->addConstraint(new \LL\Installer\Constraint('introduction', 'variants_blocks', 'id'));
        $oTable->compile();

    }
}