<?php

namespace LL\TicketEngines;

class CoreInstaller{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\CoreInstaller', 'installTicketModules', 'Install core ticket buying modules');
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\CoreInstaller', 'installTables', 'Install hedging table');
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\CoreInstaller', 'installMenus', 'Install hedging menu');
        $aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\CoreInstaller', 'amendLotteryDrawsTable','Apply changes to draws table');
        return $aJobs;
    }

    static function installTicketModules(){
        self::installModule('Self Insurance',1);
        self::installHandler('\\SelfInsuredTicketEngine', 'Self Insurance', 'TicketEngine',1);
        self::installModule('Test Ticket Engine',1);
        self::installHandler('\\TestTicketEngine', 'Test Ticket Engine', 'TicketEngine',1);
    }

    static function installMenus(){
        $iAdminSectionId=\DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='Hedging'");
        //Get the ID of the admin section
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Lodging Sessions'")==0){
            \DAL::Insert('admin_pages', array('title'=>'Lodging Sessions','filename'=>'generic-listing.php','tablename'=>'ticket_purchase_sessions','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Manage Hedging'")==0){
            \DAL::Insert('admin_pages', array('title'=>'Manage Hedging','filename'=>'generic-listing.php','tablename'=>'ticket_engines','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }
        \DAL::Update('admin_pages',array('filename'=>'/administration/LL/TicketEngines/ManageDraws','tablename'=>''),'id='.$iAdminSectionId);

        ///administration/API/LLTicketEngines/status
    }

    static function installTables(){

        $oTable=new \LL\Installer\Table('ticket_engines');
        $oTable->addField(new \LL\Installer\Field('last_run_date', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('last_successful_run_date', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('last_failure_run_date', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('last_result', 'varchar',20));
        $oTable->addField(new \LL\Installer\Field('is_active', 'tinyint'));
        $oTable->addField(new \LL\Installer\Field('is_suspended', 'tinyint'));
        $oTable->addField(new \LL\Installer\Field('successful_tickets', 'bigint'));
        $oTable->addField(new \LL\Installer\Field('failed_tickets', 'bigint'));
        $oTable->addField(new \LL\Installer\Field('successful_runs', 'int'));
        $oTable->addField(new \LL\Installer\Field('failed_rus', 'int'));
        $oTable->addField(new \LL\Installer\Field('priority', 'int'));
        $oTable->compile();
    }

    static function amendLotteryDrawsTable(){


        $oTable=new \LL\Installer\Table('r_lottery_dates');
        $oTable->addField(new \LL\Installer\Field('fk_ticket_engine', 'int',11));
        $oTable->addField(new \LL\Installer\Field('ticket_engine_assigned_at', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('fk_ticket_engine_override', 'int',11));
        $oTable->addField(new \LL\Installer\Field('ticket_engined_overridden_at', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('cancelled_at', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('postponed_at', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('postponed_to', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('voided_at', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('status', 'varchar(100)'));

        $oTable->addConstraint(new \LL\Installer\Constraint('fk_ticket_engine', 'ticket_engines', 'id'));
        $oTable->addConstraint(new \LL\Installer\Constraint('fk_ticket_engine_override', 'ticket_engines', 'id'));

        $oTable->compile();

    }
}
