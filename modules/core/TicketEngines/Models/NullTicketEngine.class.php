<?php
/**
 * This engine basically means something cannot be hedged anywhere and is used as the
 * default engine when selecting an engine
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */
class NullTicketEngine extends BaseTicketEngine{


}