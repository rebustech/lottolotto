<?php
/**
 * Provides the engine that is used for providing our own insurance
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */
class SelfInsuredTicketEngine extends InsuredTicketEngine{

}