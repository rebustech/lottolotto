<?php
/**
 * The base ticket engine class. All ticket engines should extend this class and
 * override as required
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan patchett
 */
class BaseTicketEngine extends LLModel{
    static $sHandlerField='handler';

    var $name,$comments,$handler,$threshold_min,$threshold_max,$config,$effective_date,$fk_lottery_id,$last_run_date,$last_successful_run_date,$last_failure_run_date,$last_result,$is_active,$is_suspended,$successful_tickets,$failed_tickets,$successful_runs,$failed_rus,$priority;

    /**
     * Events
     */
    const EVENT_BEFORE_GET_CONFIGURATION_FIELDS='BEFORE_GET_CONFIGURATION_FIELDS';
    const EVENT_AFTER_GET_CONFIGURATION_FIELDS='AFTER_GET_CONFIGURATION_FIELDS';

    /**
     * Constants
     */
    const TABLE_NAME='ticket_engines';

    /**
     *
     * @var type
     */
    var $id;
    /**
     * The minimum prize payout from this engine
     * This wil be read in from the database
     * @var float
     */
    var $minPayout=0;
    /**
     * The maximum prize payout from this engine
     * This wil be read in from the database
     * @var float
     */
    var $maxPayout=100000000;
    /**
     * The minimum fee that will be charged per draw
     * @var float
     */
    var $minDrawCharge=0;

    /**
     * The ID of the account that is used for transactions against this fund
     * @var int
     */
    var $fk_account_id=0;

    /**
     * The base cost for purchasing a ticket with this method
     * @var float
     */
    var $baseCost=1000000000000;

    /**
     * Returns an array of configuration fields used in the backend
     * @return ConfigurationEngine
     */
    function getBackendConfigurationFields(){
        Events::Trigger(self::EVENT_BEFORE_GET_CONFIGURATION_FIELDS,$this);
        $this->aConfigurationFields=$this->_getBackendConfigurationFields();
        Events::Trigger(self::EVENT_AFTER_GET_CONFIGURATION_FIELDS,$this);
        return $this->aConfigurationFields;
    }

    function _getBackendConfigurationFields(){
    }

    /**
     * Check a ticket to see if this engine can provide hedging cover
     * @param type $oTicket
     * @return boolean
     */
    function canProvide($iJackpot){
        $fMaximumExposure=$iJackpot;

        return ($fMaximumExposure<=$this->threshold_max && $fMaximumExposure>=$this->threshold_min);
    }

    /**
     * Calculate the cost to cover this ticket
     * @param type $oTicket
     * @return float
     */
    function getCost($iJackpot){
        return $this->baseCost;
    }

    /**
     * Lodges a bet against this ticket with this engine in order to provide cover
     * @param type $oTicket
     * @return boolean
     */
    function lodgeBet($oTicket){
        return false;
    }

    /**
     * Given a winning ticket draws down $cAmount from this funds prize fund
     * into the prize fund utilised by the game engine associated with the ticket
     * @param type $oTicket
     * @param float $cAmount
     */
    function drawDown($oTicket,$cAmount){

    }

    /**
     * Called after a win scan has been completed in order to ensure that any
     * calculations or actions relating to a draw are carried out
     * @param draw $oDraw
     */
    function afterWinScan(draw $oDraw){

    }

    /**
     * Called when bookings for a draw are closed off. This is used to transfer
     * any data to 3rd party providers that work in bulk, and to ensure that any
     * unpurchased tickets are purchased at this point
     * @param draw $oDraw
     */
    function beforeDraw(draw $oDraw){

    }

    function populateFromPost(){
        $this->populateFromArray($_POST);
        $aConfigFields=$this->getBackendConfigurationFields();
        $aConfigFields->processPost();
        $this->sConfig=$aConfigFields->serialize();
    }

    function save(){
        $aFields=array(
            'id'=>$this->id,
            'name'=>$this->name,
            'comments'=>$this->comments,
            'handler'=>$this->handler,
            'threshold_min'=>$this->threshold_min,
            'threshold_max'=>$this->threshold_max,
            'config'=>$this->sConfig,
            'effective_date'=>$this->effective_date,
            'fk_lottery_id'=>$this->fk_lottery_id,
            'last_run_date'=>$this->last_run_date,
            'last_successful_run_date'=>$this->last_successful_run_date,
            'last_failure_run_date'=>$this->last_failure_run_date,
            'last_result'=>$this->last_result,
            'is_active'=>$this->is_active,
            'is_suspended'=>$this->is_suspended,
            'successful_tickets'=>$this->successful_tickets,
            'failed_tickets'=>$this->failed_tickets,
            'successful_runs'=>$this->successful_runs,
            'failed_rus'=>$this->failed_rus,
            'priority'=>$this->priority
        );

        if($this->id==''){
            $this->id=DAL::Insert(self::TABLE_NAME, $aFields,true);
        }else{
            DAL::Update(self::TABLE_NAME, $aFields, 'id='.$this->id);
        }
    }

    /**
     * Instantiates a ticket engine of the correct class for the id provided
     * @param int $id ID of the ticket engine to return
     * @return \BaseTicketEngine
     */
    static function getById($id){
        $aData=DAL::executeGetRow('SELECT * FROM ticket_engines WHERE id='.intval($id));
        $sEngineName=$aData['handler'];
        if(class_exists($sEngineName)){
            $oEngine=new $sEngineName;
        }else{
            $oEngine=new BaseTicketEngine;
        }
        $oEngine->populateFromArray($aData);

        //Map the fields
        $oEngine->baseCost=$aData['base_cost'];
        $oEngine->maxPayout=$aData['max_payout'];
        $oEngine->minDrawCharge=$aData['min_draw_charge'];
        $oEngine->minPayout=$aData['min_payout'];
        $oEngine->sConfig=$aData['config'];

        return $oEngine;
    }

}