<?php

class LLTicketEnginesApi extends  LL\AjaxController{

    /**
     * Run through each of the upcoming draws
     * if no te is assigned then assign one
     * then use the assigned te or if an override has been selected then
     * use that
     * Simply trigger a new ticket buying session with the ticked engine, the
     * ticket engine will then request the tickets to purchase
     */
    function cron(){
        //Enable error reporting and log to display
        error_reporting(E_ERROR | E_COMPILE_ERROR | E_USER_ERROR);
        ini_set(display_errors,0);
        set_time_limit(110);
        AuditLog::$bOutput=true;

        AuditLog::LogItem('Hedging started', 'HEDGING_STARTED','LLTicketEnginesApi',0);

        $aDraws=\DAL::executeQuery('SELECT * FROM vw_next_draws');



        foreach($aDraws as $aDraw){
            try{
                $oTicketEngine=TicketBuyingController::selectEngineToUse($aDraw['fk_lottery_id'], $aDraw['drawdate'], $aDraw['jackpotm']*1000000);
                \DAL::update('r_lottery_dates',array('fk_ticket_engine'=>$oTicketEngine->id),'id='.$aDraw['id']);
                /*
                $iTicketEngine=($aDraw['fk_ticket_engine_override'])?$aDraw['fk_ticket_engine_override']:$aDraw['fk_ticket_engine'];
                if($iTicketEngine==''){
                }else{
                    $oTicketEngine=BaseTicketEngine::getById($iTicketEngine);
                    //Check the engine is valid still. If not we need to flag this up
                    if($oTicketEngine->is_active==0 || $oTicketEngine->is_suspended==1){
                        $oTicketEngine=TicketBuyingController::selectEngineToUse($aDraw['fk_lottery_id'], $aDraw['drawdate'], $aDraw['jackpotm']*1000000);
                    }
                }*/                
                echo "\r\nChecking Draw ".$aDraw['comment'].' '.$aDraw['unpurchased'].' unpurchased tickets '.$oTicketEngine->name;
                //Check again to see if we have a ticket engine
                if($oTicketEngine->id>0 && $aDraw['unpurchased']>0){
                    AuditLog::LogItem('Sending '.$aDraw['unpurchased'].' tickets to '.$oTicketEngine->name, 'BUYING_TICKETS', $oTicketEngine, $oTicketEngine->id);
                    $oDraw=draw::getById($aDraw['id']);
                    $oDraw->populateFromArray($aDraw);
                    $oDraw->jackpot=$oDraw->jackpotm*1000000;
                    $oTicketEngine->beforeDraw($oDraw);
                    AuditLog::LogItem('Completed buying '.$aDraw['unpurchased'].' tickets from '.$oTicketEngine->name, 'BUYING_TICKETS', $oTicketEngine, $oTicketEngine->id);
                }else{
                    AuditLog::LogItem('No tickets to buy for '.$oTicketEngine->name, 'BUYING_TICKETS','$oTicketEngine',$oTicketEngine->id);

                }
            }catch(Exception $e){
                log_exception($e);
            }
        }
        AuditLog::LogItem('Hedging finished', 'HEDGING_FINISHED','LLTicketEnginesApi',0);
    }

    function status(){
        $this->aListData=\DAL::executeQuery('SELECT * FROM vw_next_draws');
        $this->output($this->aListData);
    }

}