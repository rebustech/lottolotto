<?php

namespace LL\TicketEngines;

class ManageDraws extends \AdminController{

    function index(){
        switch($_POST['action']){
            case 'edit':
                return $this->Edit();
                break;
            default:
                return $this->ListAll();
        }
    }

    function reconcile(){
        $sSQL='SELECT e.name,
                bi.fk_lotterydate,
                  SUM((`bi`.`purchased` = 0)) AS `unpurchased`,
                  SUM(((`bi`.`purchased` = 0) AND (`bi`.`fk_ticket_purchasing_session_id` <> NULL))) AS `queued`,
                  SUM((`bi`.`purchased` = 1)) AS `purchased`,
                  COUNT(bi.bookingitem_id) AS `total`

                FROM booking_items bi
                INNER JOIN ticket_purchase_sessions p ON bi.fk_ticket_purchasing_session_id=p.id
                INNER JOIN ticket_engines e ON p.fk_ticket_engine_id=e.id

                GROUP BY e.name,bi.fk_lotterydate
                ORDER BY bi.fk_lotterydate,e.name';

        $tabs=new \TabbedInterface();
        $oDetailsTab=new \TabbedInterface(\lang::get('next_draws'));
        $oDetailsTab->sIcon='fa-user';

        //Create a tab to show assigned users
        $oListTab=new \TabbedInterface(\lang::get('member_bookings'));
        $oListTab->sIcon='fa-rocket';
        $aListData=\DAL::executeQuery($sSQL);
        $oListTab=new \TableControl($aListData);
        $oListTab->addField(new \TableField('name','varchar','Engine','name'));
        $oListTab->addField(new \TableField('fk_lotterydate','varchar','Date','fk_lotterydate'));
        $oListTab->addField(new \TableField('queued','int','Queued','queued'));
        $oListTab->addField(new \TableField('purchased','int','Purchase','purchased'));
        $oListTab->addField(new \TableField('unpurchased','int','Unpurchased','unpurchased'));
        $oListTab->addField(new \TableField('total','int','Total','total'));

       //$oListTab->addAction(new \TableRowActionControl('edit','Edit','edit'));
        $oListTab->AddControl($oBookingsTable);

        $tabs->aItems['bookings']=$oListTab;

        $aItems[]=$tabs->Output();

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;


    }

    function Edit(){
        $tabs=new \TabbedInterface();
        $oDetailsTab=new \TabbedInterface(\lang::get('member_details'));
        $oDetailsTab->sIcon='fa-user';

        $oMainDetailsTab=new \TabbedInterface(\lang::get('member_details'));
        $oMainDetailsTab->sIcon='fa-user';
        $oMainDetailsTab->aItems[]=\GenericTableAdmin::createGenericDetailsForm('r_lottery_dates', $_POST['id']);
        //$oDetailstabs->aItems['main']=$oMainDetailsTab;
        $tabs->aItems['details']=$oMainDetailsTab;

        $aItems[]=$tabs->Output();

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;

    }

    function ListAll(){

        $tabs=new \TabbedInterface();
        $oDetailsTab=new \TabbedInterface(\lang::get('next_draws'));
        $oDetailsTab->sIcon='fa-user';

        //Create a tab to show assigned users
        $oListTab=new \TabbedInterface(\lang::get('member_bookings'));
        $oListTab->sIcon='fa-rocket';
        $aListData=\DAL::executeQuery('SELECT * FROM vw_next_draws');
        $oListTab=new \TableControl($aListData);
        $oListTab->getFieldsFromTable('lotteries_cmn');
        $oListTab->getFieldsFromTable('r_lottery_dates');
        $oListTab->keepFields('lotteries_cmn','comment');
        $oListTab->keepFields('r_lottery_dates','id,drawdate,cutoff,price,jackpotm,rollover');
        $oListTab->addField(new \TableField('jackpotm','int','Jackpot','jackpotm'));
        $oListTab->addField(new \TableField('purchased','int','Purchase','purchased'));
        $oListTab->addField(new \TableField('unpurchased','int','Unpurchased','unpurchased'));
        $oListTab->addField(new \TableField('ticket_engine_name','int','Assigned Engine','ticket_engine_name'));
        $oListTab->addField(new \TableField('ticket_engine_override','int','Override Engine','ticket_engine_override'));
        $oListTab->addField(new \TableField('last_result','int','Last Result','last_result'));
        //$oContactLogTable->renderAs('contact_types','icon','icon');
        //$oContactLogTable->renderAs('contact_types','direction_icon','icon');
        //$oTransactionsTable->keepFields('admin_users','username,firstname,lastname,email,isactive');

        $oListTab->addAction(new \TableRowActionControl('edit','Edit','edit'));
        $oListTab->AddControl($oBookingsTable);

        $tabs->aItems['bookings']=$oListTab;

        $aItems[]=$tabs->Output();

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }

}