<?php

/**
 * This class is used to manage purchasing sessions
 *
 * @package LoveLotto
 * @subpackage TicketEngines
 * @author Jonathan Patchett
 */
class TicketPurchasingSession extends LLModel{
    var $iTicketEngineId;
    var $iAgentId;
    var $iLotteryId;
    var $oDraw;

    const TBL_TICKET_PURCHASE_SESSIONS='ticket_purchase_sessions';

    function __construct($id=null){
        if($id!==null){
            $sSql='SELECT * FROM '.self::TBL_TICKET_PURCHASE_SESSIONS.' WHERE id='.$id;
            $aResult=DAL::executeGetRow($sSql);
            $this->populateFromArray($aResult);
        }
    }
    /**
     * Creates a new session. The session will be recorded
     * in the database and the ID of the session will be stored against each ticket
     * (bookingitem) at the point of purchase
     * @param integer $iTicketEngineId The ID of the ticket engine being used
     * @param integer $iLotteryId The ID of the lottery we are purchasing for. Tickets can
     * only ever be purchased for one lottery at at a time.
     * @param integer $iAgentId If applicable the ID of the ticket purchasing agent
     * @return TicketPurchasingSession
     */
    static function startSession($iTicketEngineId,$oLotteryDraw,$iAgentId=0){
        \AuditLog::LogItem('Starting ticket buying session for '.$iTicketEngineId, 'TPS_STARTED', 'TicketPurchasingSession', 0);
        $oSesssion=new TicketPurchasingSession();
        $oSesssion->oDraw=$oLotteryDraw;
        $iLotteryDrawId=$oLotteryDraw->id;
        $aValues=array('fk_ticket_engine_id'=>$iTicketEngineId,
                       'fk_lottery_draw_id'=>$iLotteryDrawId,
                       'fk_agent_id'=>$iAgentId,
                       'opened_at'=>date('Y-m-d H:i:s'));
        $iSessionId=DAL::Insert('ticket_purchase_sessions', $aValues,true);
        $oSesssion->id=$iSessionId;
        AuditLog::LogItem('Created ticket buying session for '.$iTicketEngineId, 'TPS_CREATED', 'TicketPurchasingSession', $iSessionId);
        return $oSesssion;
    }

    /**
     * Closes the session
     */
    function endSession(){
        \AuditLog::LogItem('Ending ticket buying session for '.$iTicketEngineId, 'TPS_END', 'TicketPurchasingSession', $this->id);

        DAL::Update('booking_items',array('purchased'=>1,'purchased_at'=>date('Y-m-d H:i:s')),'fk_ticket_purchasing_session_id='.$this->id);
        DAL::Update(self::TBL_TICKET_PURCHASE_SESSIONS, array('closed_at'=>date('Y-m-d H:i:s')), 'id='.$this->id);
    }

    function abandonSession(){
        \AuditLog::LogItem('Abandon ticket buying session for '.$iTicketEngineId, 'TPS_ABANDON', 'TicketPurchasingSession', $this->id);

        DAL::Update('booking_items',array('purchased'=>0,'fk_ticket_purchasing_session_id'=>null),'fk_ticket_purchasing_session_id='.$this->id);
        DAL::Update(self::TBL_TICKET_PURCHASE_SESSIONS, array('closed_at'=>date('Y-m-d H:i:s')), 'id='.$this->id);
    }

    /**
     * Gets $iLimit tickets for purchasing, this also marks the session id against the
     * booking item, but leaves the purcahsed flag at 0.
     * For brokerage models $iLimit should always be 1 in order to prevent tickets being
     * earmarked for purchase but never purchased. For insurance models use as many as you want
     * or use 0 for all
     *
     * @param integer $iLimit default 1 Set to 0 for no limit
     * @param boolean $bReBook default true If true will reassign unpurchased tickets from
     * other sessions to this session. If false unpurchased tickets from already assigned
     * to a purchasing session will be left alone
     * @return array Returns an array of ticket objects
     */
    function getTickets($iLimit=1,$bReBook=false){
        \AuditLog::LogItem('Obtaining '.$iLimit.' tickets for buying session', 'TPS_GET_TICKETS', 'TicketPurchasingSession', $this->id);
        /**
         * This needs to be modified to select only tickets for the next draw
         */

        $sSql='UPDATE `booking_items` AS bi
                SET fk_ticket_purchasing_session_id='.$this->id.'
                WHERE purchased=0
                  AND bi.fk_lottery_id='.$this->oDraw->fk_lottery_id.'
                  AND bi.fk_lotterydate=\''.$this->oDraw->drawdate.'\'';
        if(!$bReBook){
            $sSql.=' AND bi.fk_ticket_purchasing_session_id is null';
        }
        if($iLimit>0){
            $sSql.=' LIMIT '.$iLimit;
        }

        DAL::executeQuery($sSql);

        /**
         * Now that we have marked the items we want to buy return them
         */
        $sSql='SELECT * FROM `booking_items` AS bi
                INNER JOIN `bookings` AS b ON bi.`fk_booking_id`=b.`booking_id`
                INNER JOIN `members` AS m ON b.fk_member_id=m.member_id
                WHERE purchased=0 AND fk_ticket_purchasing_session_id='.$this->id;

        \AuditLog::LogItem($sSql, 'TPS_GET_TICKETS', 'TicketPurchasingSession', $this->id);


        return DAL::executeQuery($sSql);
    }

    /**
     * Writes raw response from ticket engine and stores it against the current session
     * @param string $sResponse
     */
    function writeResponseFromEngineToSession($sResponse)
    {
        \AuditLog::LogItem($sResponse, 'TPS_RESPONSE', 'TicketPurchasingSession', $this->id);
        DAL::Update(self::TBL_TICKET_PURCHASE_SESSIONS, array('raw_response'=>$sResponse), 'id='.$this->id);
    }


    /**
     * Writes the raw data sent over to the ticket buying API
     * @param string $sResponse
     */
    function writeRequestToEngine($sRequest)
    {
        \AuditLog::LogItem($sRequest, 'TPS_REQUEST', 'TicketPurchasingSession', $this->id);
        DAL::Update(self::TBL_TICKET_PURCHASE_SESSIONS, array('raw_request'=>$sRequest), 'id='.$this->id);
    }

}
