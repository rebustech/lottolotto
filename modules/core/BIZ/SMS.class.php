<?php
/**
 * Send SMS using txtNation
 */

class SMS {
    private $eKey;
    private $gateway;

    public function __construct() {

        // Set eKey and gateway
        $this->eKey = "67ca7bb42c66ce3f24ac2b01bb3bcfa4";
        $this->gateway = "http://client.txtnation.com/gateway.php";
    }

    public function send($message, $to) {

        // Set values for request to send to txtNation
        $isReply = 0;
        $id = rand(0, 99999999999);
        $number = $to;
        $network = "international";
        $message = urlencode($message);
        $ekey = $this->eKey;
        $companyCode = "fftradingnv";

        // Request to send
        $request = $this->gateway . "?reply={$isReply}&id={$id}&number={$number}&network={$network}&message={$message}&ekey={$ekey}&cc={$companyCode}&value=0&cy=EUR";

        // Send a GET request
        $response = file_get_contents($request);

        // Return if SMS was sent successfully or not
        if ($response !== "SUCCESS") {
            return false;
        }

        return $response;
    }
}