<?php
/**
 * Class for handling frontend error messages such as validation errors
 * @package LoveLotto
 * @subpackage FrontEndModels
 */
class Error {
        static $oError;
        static $bShow=false;

	private $sPrefix = "";
	private $sErrorSession = "Errors";
	private $sNoticeSession = "Notices";
	private $aErrors = array();
	private $aNotices = array();
	function __construct($sPrefix = ""){
		$this->sPrefix = $sPrefix;
		$this->sErrorSession = $this->sPrefix . $this->sErrorSession;
		$this->sNoticeSession = $this->sPrefix . $this->sNoticeSession;
		$this->loadErrors();
		$this->loadNotices();

                self::$oError=$this;
	}

        static function Errors(){
            if(!self::$oError){
                self::$oError=new Error();
            }
            return self::$oError;
        }

	//Errors
	public function getErrorCount(){
		$this->loadErrors();
		return count($this->aErrors);
	}

	private function loadErrors(){
		if(isset($_SESSION[$this->sErrorSession])){
			$this->aErrors = $_SESSION[$this->sErrorSession];
		}
	}

	private function saveErrors(){
		$_SESSION[$this->sErrorSession] = $this->aErrors;
	}

	public function addError($sError) {
                if(self::$bShow) echo $sError;
		$this->loadErrors();
		$this->aErrors[base64_encode($sError)]=$sError;
		$this->saveErrors();
	}

	public function clearErrors(){
		$this->aErrors = array();
 		unset($_SESSION[$this->sErrorSession]);
	}

	public function getErrorList($sListClass = "error"){
		$this->loadErrors();
		return $this->createList($this->aErrors, $sListClass);
	}

	public function getErrorString(){
		$this->loadErrors();
		return $this->createString($this->aErrors);
	}

	//Notices
	public function getNoticeCount(){
		$this->loadNotices();
		return count($this->aNotices);
	}

	private function loadNotices(){
		if(isset($_SESSION[$this->sNoticeSession])){
			$this->aNotices = $_SESSION[$this->sNoticeSession];
		}
	}

	private function saveNotices(){
		$_SESSION[$this->sNoticeSession] = $this->aNotices;
	}

	public function addNotice($sNotice) {
                if(self::$bShow) echo $sNotice;
		$this->loadNotices();
		$this->aErrors[base64_encode($sNotice)]=$sNotice;
		$this->saveNotices();
	}

	public function clearNotices(){
		$this->aNotices = array();
 		unset($_SESSION[$this->sNoticeSession]);
	}

	public function getNoticeList($sListClass = "notice"){
		$this->loadNotices();
		return $this->createList($this->aNotices, $sListClass);
	}

	public function getNoticeString(){
		$this->loadNotices();
		return $this->createString($this->aNotices);
	}

	//Formatting
	private function createString($aMessages){
		$sOutput = "";
		if(sizeof($aMessages) > 0){
			foreach($aMessages as $sCurrentMessage){
				$sOutput .= $sCurrentMessage . "<br />";
			}
		}
		return $sOutput;
	}

	public function createList($aMessages, $sListClass = ""){
		$sOutput = "";
		if(sizeof($aMessages) > 0){
			$sOutput .= "<ul class=\"{$sListClass}\">";
			foreach($aMessages as $sCurrentMessage){
				$sOutput .= "<li>" . $sCurrentMessage . "</li>";
			}
			$sOutput .= "</ul>";
		}
		return $sOutput;
	}

	public function clearMessages(){
		$this->clearErrors();
		$this->clearNotices();
	}
}
?>
