<?php
/**
 * Appears to be a a controller for sending various system generated emails
 *
 * Modified to act as a wrapper to various Silverpop calls
 *
 * @package LoveLotto
 * @subpackage FrontEndModels
 */
class Email {

    // Each Silverpop email is assigned to a particular automated campaign
    // which each has its own ID number.
    // Each language has its own campaigns as an array
    // so only the correct language array is returned
    private static $aCampaignsMaster = array('en' => array('newCustReg'      => 20983389,
                                                            'addFunds'        => 21012428,
                                                            'purchaseFail'    => 20983389,
                                                            'passwordRequest' => 21521233,
                                                            'passwordReset'   => 21012598,
                                                            'firstOrderLoss'  => 21263759,
                                                            'orderConfirm'    => 21070666,
                                                            'orderWin'        => 21070690,
                                                            'orderHelp'       => 21263729,
                                                            'orderIncomplete' => 21263774,
                                                            'orderCancel'     => 21456441,
                                                            'crm'             => 21091575,
                                                            'exclusionChanged' => 21508867,
                                                            'exclusionLifted' => 21508923,
                                                            'depLimitChanged'  => 21508927,
                                                            'depLimitReached'  => 21508939,
                                                            'abandonedOrder'  => 21508864,
                                                            'accountVerify'   => 21508949,
                                                            'withdrawRequest' => 21510618,
                                                            'newCustCRM'      => 000),
                                              'de' => array('newCustReg'      => 21077742,
                                                            'addFunds'        => 21077762,
                                                            'purchaseFail'    => 20983389,
                                                            'passwordRequest' => 21521235,
                                                            'passwordReset'   => 21077750,
                                                            'firstOrderLoss'  => 21263798,
                                                            'orderConfirm'    => 21077746,
                                                            'orderWin'        => 21077768,
                                                            'orderHelp'       => 21263794,
                                                            'orderIncomplete' => 21263802,
                                                            'orderCancel'     => 21456484,
                                                            'crm'             => 21512697,
                                                            'exclusionChanged' => 21512605,
                                                            'exclusionLifted'  => 21512612,
                                                            'depLimitChanged'  => 21512622,
                                                            'depLimitReached'  => 21512630,
                                                            'abandonedOrder'   => 21512636,
                                                            'accountVerify'    => 21512644,
                                                            'withdrawRequest'  => 21512651,
                                                            'newCustCRM'      => 000));
    
    
    private static $aAlertsLists = array('en' => array('jp_6aus49'       => 2592786,
                                                        'jp_eurojackpot'  => 2593363,
                                                        'jp_euromillions' => 2593361,
                                                        'jp_megamillions' => 2593364,
                                                        'jp_powerballl'   => 2593365,
                                                        'rs_6aus49'       => 2593366,
                                                        'rs_eurojackpot'  => 2593368,
                                                        'rs_euromillions' => 2593367,
                                                        'rs_megamillions' => 2593369,
                                                        'rs_powerball'    => 2593370,
                                                        'system'          => 0),
                                        'de' => array('jp_6aus49'       => 2593372,
                                                      'jp_eurojackpot'  => 2593371,
                                                      'jp_euromillions' => 2593374,
                                                      'jp_megamillions' => 2593375,
                                                      'jp_powerballl'   => 2593376,
                                                      'rs_6aus49'       => 2593377,
                                                      'rs_eurojackpot'  => 2593379,
                                                      'rs_euromillions' => 2593378,
                                                      'rs_megamillions' => 2593380,
                                                      'rs_powerball'    => 2593381,
                                                      'system'          => 0));
    
    private static $aAlertsTemplates = array('en' => array('jp_6aus49'       => 21143722,
                                                            'jp_eurojackpot'  => 21143677,
                                                            'jp_euromillions' => 21143677,
                                                            'jp_megamillions' => 21143725,
                                                            'jp_powerballl'   => 21143730,
                                                            'rs_6aus49'       => 21169731,
                                                            'rs_eurojackpot'  => 21169700,
                                                            'rs_euromillions' => 21169729,
                                                            'rs_megamillions' => 21169735,
                                                            'rs_powerball'    => 21169739,
                                                            'system'          => 0),
                                         'de' => array('jp_6aus49'       => 21143752,
                                                       'jp_eurojackpot'  => 21143739,
                                                       'jp_euromillions' => 21143746,
                                                       'jp_megamillions' => 21143758,
                                                       'jp_powerballl'   => 21143763,
                                                       'rs_6aus49'       => 21169841,
                                                       'rs_eurojackpot'  => 21169742,
                                                       'rs_euromillions' => 21169835,
                                                       'rs_megamillions' => 21169843,
                                                       'rs_powerball'    => 21169846,
                                                       'system'          => 0));
    
    
    
    /**
     * Return the correct country ID for this email address
     * used to determine correct language email to use
     *
     * @param string $sEmail email address
     * @return array
     */
    private static function getCampaignsForLanguage($sEmail)
    {
        /*
        
        // Get customer's signup language ID details
        $sSQL = "SELECT l.* 
                 FRON languages l
                 LEFT JOIN members m on m.signup_language_id = l.language_id
                 WHERE m.username = '$sEmail'";
        
        $aLangInfo= DAL::executeGetRow($sSQL);

       // Default to code of the config language if no lang info found
        if (!is_array($aLangInfo) || $aLangInfo['is_active'] == 0)
        {
            $sCountry == Language::getLangCodeFromID(Config::$config->iLanguageId);
        }
        else
        {
            $sCountry == '$aLangInfo['code'];
        }
         
        */
        
        
        // Country ID
        $iCountryId = Config::$config->iLanguageId;

        // We'll defaoult to English here
        switch ($iCountryId)
        {
            case 3:
                $sCountry = "de";
            break;

            default:
                $sCountry = "en";

        }

        // Return the correct language array
        return self::$aCampaignsMaster[$sCountry];
    }


    /**
     * Get correct campaign for sending out either results or jackpot alerts
     *
     * @param string $sType 2-character string for type of email (result or jackpot)
     * @param integer $iLotteryID lottery id
     * @return string
     */
    private static function getCorrectCampaign($sType, $iLotteryID, $sLang)
    {
        //$sType will either be jp or rs
        switch ($iLotteryID)
        {
            case 2 :
              $sType = $sType . "_euromillions";
            break;


            case 3 :
              $sType = $sType . "_megamillions";
            break;


            case 4 :
              $sType = $sType . "_powerball";
            break;


            case 11 :
               $sType = $sType . "_6aus49";
            break;


            case 13 :
                $sType = $sType . "_eurojackpot";
            break;
        
            case 0 :
                $sType = "system";
            break;
        }
        
        return array('template' => self::$aAlertsTemplates[$sLang][$sType],
                     'list'     => self::$aAlertsLists[$sLang][$sType]);
    }

    /**
     * Get latest results for the specified lottery
     *
     * @param integer $iLotteryId Lottery Id
     * @return array
     */
    public static function getLatestResults($iLotteryId, $iLangId=1) {
        
        $aReturn = array('Lottery Name' => '',
                         'Draw Day' => '',
                         'Draw Date' => '');

        $sSQL = "SELECT DATE_FORMAT(ld.fk_lotterydate, '%W') as draw_day,
                        DATE_FORMAT(ld.fk_lotterydate, '%D %M %Y') as draw_date,
                        ld.fk_lotterydate as lotterydate,
                        ld.numbers,
                        lc.bonus_numbers,
                        ll.title as lottery_name
                 FROM lottery_draws ld
                 INNER JOIN lotteries_cmn lc ON ld.fk_lottery_id = lc.lottery_id 
                 INNER JOIN lotteries_lang ll ON lc.lottery_id = ll.fk_lottery_id
                 INNER JOIN languages l ON l.language_id = ll.fk_language_id
                 WHERE ld.processed = 1
                 AND ld.fk_lottery_id = {$iLotteryId}
                 AND ll.fk_language_id = {$iLangId}
                 ORDER BY lotterydate DESC
                 LIMIT 1";

        $aRow =  DAL::executeGetRow($sSQL);

        // Sort out returned numbers into main and bonus numbers,
        //
        $aNumbers =  explode('|', $aRow['numbers']);
        
        
        if ($aRow['bonus_numbers'] > 0)
        {
            $iBonusNumPos = 0 - (int) $aRow['bonus_numbers'];
            
            $aMain = array_slice($aNumbers, 0, $iBonusNumPos);
            $aBonus = array_slice($aNumbers, $iBonusNumPos);
            
            $countm = 0;
            $countb = 0;
            foreach ($aMain as $iNumM)
            {
                $countm++;
                $key = 'M' . $countm;
                $aReturn[$key] = $iNumM;
            }
            
            foreach ($aBonus as $iNumB)
            {
                $countb++;
                $key = 'B' . $countb;
                $aReturn[$key] = $iNumB;
            }
        }
        else
        {
            $count = 0;
            foreach ($aNumbers as $iNum)
            {
                $count++;
                $key = 'M' . $count;
                $aReturn[$key] = $iNum;
            }
            
        }

        $aReturn['Lottery Name'] = $aRow['lottery_name'];
        $aReturn['Draw Day'] = $aRow['draw_day'];
        $aReturn['Draw Date'] = $aRow['draw_date'];
        
        
        return $aReturn;
    }    
    
    
    /**
     * Get the next jackpot estimate for the supplied lottery id
     * (based on original function - has been modified)
     *
     * @param integer $iLotteryId
     * @return array
     */
    protected static function getNextLotteryJackpot($iLotteryId, $iLangId=1) {
        
        $aReturn = array('JP' => 0,
                         'Lottery Name' => '',
                         'Draw Day' => '',
                         'Draw Date' => '');
        
        $sSQL = "SELECT 
                    ll.title as lottery_name,
                    ROUND((rld.jackpot/1000000),1) as jackpot,
                    DATE_FORMAT(rld.drawdate, '%W') as draw_day,
                    DATE_FORMAT(rld.drawdate, '%D %M %Y') as draw_date,
                    rld.rollover as rollover,
                    DATE_SUB(rld.drawdate, INTERVAL (rld.cutoff + (60 * lc.timezone)) MINUTE) as cutoff_time
                 FROM lotteries_cmn lc
                    INNER JOIN lotteries_lang ll ON lc.lottery_id = ll.fk_lottery_id
                    INNER JOIN r_lottery_dates rld ON rld.fk_lottery_id = lc.lottery_id AND rld.drawdate > DATE_ADD(NOW(), INTERVAL (rld.cutoff + 180 + (60 * lc.timezone) ) MINUTE )
                    INNER JOIN languages l ON l.language_id = ll.fk_language_id
                    INNER JOIN currencies c ON c.currency_id = lc.fk_currency_id
                 WHERE lc.lottery_id = {$iLotteryId}
                 AND ll.fk_language_id = {$iLangId}
                 ORDER BY YEAR(cutoff_time) ASC, DAYOFYEAR(cutoff_time) ASC, rld.jackpot DESC
                 LIMIT 1";
        
        $aRow = DAL::executeGetRow($sSQL);
        
        $aReturn['JP'] = $aRow['jackpot'];
        $aReturn['Lottery Name'] = $aRow['lottery_name'];
        $aReturn['Draw Day'] = $aRow['draw_day'];
        $aReturn['Draw Date'] = $aRow['draw_date'];
        
        return $aReturn;
    }


    // Checks matches in drawn and purchased numbers
    // and returns correct string
    protected static function formatMatchString($sPurchasedNumbers, $sDrawnNumbers, $iBonusNumbers)
    {
        $aPurchased = explode('|',$sPurchasedNumbers);
        $aDrawn = explode('|', $sDrawnNumbers);

        $aReturn = array();

        if ($iBonusNumbers > 0)
        {
            $iBonusNumPos = 0 - (int) $iBonusNumbers;

            $aReturn['purchased']['main']  = array_slice($aPurchased, 0, $iBonusNumPos);
            $aReturn['purchased']['bonus'] = array_slice($aPurchased, $iBonusNumPos);

            $aReturn['drawn']['main']  = array_slice($aDrawn, 0, $iBonusNumPos);
            $aReturn['drawn']['bonus'] = array_slice($aDrawn, $iBonusNumPos);
        }
        else
        {
            $aReturn['purchased']['main'] = $aPurchased;
            $aReturn['drawn']['main']  = $aDrawn;
        }

        return $aReturn;
    }

// --------------------------------------------------------------------------//
//  Transactional emails - generally sent to 1 user at a time
// --------------------------------------------------------------------------//
    /**
     * Sends email on new account registration
     *
     * @param integer $iMemberId Member ID
     * @param string $sEmail Email address
     * @param array $aPersonalisationData Personalisation data to pass to email
     */
    public static function sendNewRegistrationEmail($iMemberId, $sEmail, $aPersonalisationData) {
         
        // Add contact to marketing database ONLY       
        Silverpop::addModifyContactToMarketingDatabase($iMemberId);

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        // Send the email, passing in the campaign ID
        // Entry will automatically be created in Transact.
        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['newCustReg']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $iMemberId,
                              'contact_type' => 1,
                              'details' => 'New Registration Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }


    /**
     * Sends email on successful completion of adding funds process
     * 
     * @param integer $iMemberId Member ID
     * @param string $sEmail Email address
     * @param array $aPersonalisationData Personalisation data to pass to email
     */
    //public static function addFundsReceipt($iMemberId, $sEmail, $sTransactionReference, $dDate, $fTotal, $fBalance) {
    public static function addFundsReceipt($iMemberId, $sEmail, $aPersonalisationData) {    
        /*$aPersonalisationData = array('username' => $sEmail,
                                      'current balance' => $fBalance,
                                      'total' => $fTotal,
                                      'date' => $dDate,
                                      'reference' => $sTransactionReference);*/
        
        // Update contact in marketing database ONLY       
        Silverpop::addModifyContactToMarketingDatabase($iMemberId, true);
        
        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        
        $sAuditLogMsg = "Sending add funds recipt email {$aCampaigns['addFunds']} to {$sEmail}";
        AuditLog::LogItem($sAuditLogMsg, 'ADD_FUNDS_EMAIL_SENT', 'transactions', 0);
        
        
        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['addFunds']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $iMemberId,
                              'contact_type' => 1,
                              'details' => 'Add Funds Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }

    /**
     * Sends email if addung funds process fails
     *
     * @param string $sEmail email address
     * @param float $fBalance current balance
     */
    public static function purchaseFailedEmail($sEmail, $fBalance, $iMemberId) {
        $aPersonalisationData = array('username' => $sEmail,
                                      'current balance' => $fBalance);

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);


        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['purchaseFail']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $iMemberId,
                              'contact_type' => 1,
                              'details' => 'Purchase Failed Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }


    /**
     * Send password reset email
     *
     * @param string $sEmail email address
     * @param float $fBalance current balance
     * @param string $sFirstName first name
     * @param string $sPasswordLink password link
     */
   //public static function passwordResetEmail($sEmail, $fBalance, $sFirstName, $sPasswordLink) {
    public static function passwordResetEmail($sEmail, $aPersonalisationData) {
       /* $aPersonalisationData = array('username' => $sEmail,
                                      'balance' => $fBalance,
                                      'firstname' => $sFirstName,
                                      'password_link_in_here' => $sPasswordLink);*/
       
       // Get the correct list of campaigns to use
       $aCampaigns = self::getCampaignsForLanguage($sEmail);

       Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['passwordReset']);
       
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Password Reset Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }
    
    
    /**
     * Send email after a customer's first order has lost
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     */
    public static function sendFirstOrderLosing($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['firstOrderLoss']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'First Order Losing Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }


    /**
     * Send email to a customer with a winning order
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     */
    public static function sendOrderWinning($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['orderWin']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Order Winning Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }

    /**
     * Send order confirmation email
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     */
    public static function sendOrderConfirmation($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['orderConfirm']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Order Confirmation Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }
    
    
    /**
     * Sends a single email via the CRM system
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendCRMEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['crm']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'CRM Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }
    
    
    /**
     * Send exclusion changed email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendExclusionChangedEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['exclusionChanged']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Exclusion Changed Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }
    
     /**
     * Send exclusion Lifted email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendExclusionLiftedEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['exclusionLifted']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Exclusion Lifted Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }
    
    
    /**
     * Send order cancelled email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendOrderCancelledEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['orderCancel']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Order Cancelled Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }  
    
    /**
     * Sendaccount verification email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendAccountVerifyEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['accountVerify']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Account Verification Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }  
    
    /**
     * Send deposit limit changed email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendDepositLimitChangedEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['depLimitChanged']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Deposit Limit Changed Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    } 
    
    
    /**
     * Send deposit limit changed email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendDepositLimitReachedEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['depLimitReached']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Deposit Limit Reached Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }
    
     /**
     * Send deposit limit changed email
     * 
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     * @return boolean
     */
    public static function sendWithdrawRequestEmail($sEmail, $aPersonalisationData) {

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['withdrawRequest']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Withdrawal Request Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
        
        return true;
    }   

    /**
     * Send password request email (i.e. when customer requests password change)
     *
     * @param string $sEmail email address
     * @param array $aPersonalisationData array of personalisation data
     */
 
    public static function passwordChangeRequestEmail($sEmail, $aPersonalisationData) {

              // Get the correct list of campaigns to use
       $aCampaigns = self::getCampaignsForLanguage($sEmail);

       Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['passwordRequest']);
       
        // Write CRM note
        $aCRMNoteData = array('member_id' => $aPersonalisationData['member_id'],
                              'contact_type' => 1,
                              'details' => 'Password Change Request Email');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }
    
    
    /**
     * Sends email on new account registration when done via the CRM system
     *
     * @param integer $iMemberId Member ID
     * @param string $sEmail Email address
     * @param array $aPersonalisationData Personalisation data to pass to email
     */
    public static function newRegistrationCRMEmail($iMemberId, $sEmail, $aPersonalisationData) {
         
        // Add contact to marketing database ONLY       
        Silverpop::addModifyContactToMarketingDatabase($iMemberId);

        // Get the correct list of campaigns to use
        $aCampaigns = self::getCampaignsForLanguage($sEmail);

        // Send the email, passing in the campaign ID
        // Entry will automatically be created in Transact.
        Silverpop::sendTransactEmail($sEmail, $aPersonalisationData, $aCampaigns['newCustCRM']);
        
        // Write CRM note
        $aCRMNoteData = array('member_id' => $iMemberId,
                              'contact_type' => 1,
                              'details' => 'New Registration Email - CRM');
        \LL\CRM\CRM::add_crm_note_auto($aCRMNoteData);
    }
    
    
// --------------------------------------------------------------------------//
//  Engage emails - generally sent to bulk users
// --------------------------------------------------------------------------//
    
   /**
     * Send jackpot alert emails
     * These will be sent to ALL members in a stored query via Engage
     * 
     * @param integer $iLotteryId lottery ID
     * @param integer $sLang language string
     */
    public static function sendNextJackpotAlerts($iLotteryId, $sLang) {
        
        $aJackpotData = self::getNextLotteryJackpot($iLotteryId);
        
        // Get the correct campaign ID based on language
        $aCampaignDetails = self::getCorrectCampaign('jp', $iLotteryId, $sLang);      
        
        $sMailingName =  $aJackpotData['Lottery Name'] . 
                         " Jackpot Alert|" . $aJackpotData['Draw Day'] .
                         " " . $aJackpotData['Draw Date'] . '|' . $sLang . '|' . uniqid();
        
        // Schedule the mailing to send after a 5 minute delay
        $sScheduleTime = null;
        
        // Username and balance will be pulled from the engage database
        Silverpop::scheduleEngageEmail($aCampaignDetails, $sMailingName, $aJackpotData, $sScheduleTime);        
    }
    
    
    /**
     * Send results alerts emails
     * These will be sent to ALL members in a stored query via Engage
     * @param integer $iLotteryId lottery ID
     * @param integer $sLang language string
     */
    public static function sendResultsAlerts($iLotteryId, $sLang) {
       
       $aResultsData = self::getLatestResults($iLotteryId);
        
       // Get the correct campaign ID based on language
       $aCampaignDetails = self::getCorrectCampaign('rs', $iLotteryId, $sLang);
       
       
       $sMailingName =  $aResultsData['Lottery Name'] . 
                         " Latest Results|" . $aResultsData['Draw Day'] .
                         " " . $aResultsData['Draw Date']. '|' . $sLang . '|' . uniqid();
        
       // Schedule mailing if required
       $sScheduleTime = null;         
       
       // Username and balance will be pulled from the engage database
       Silverpop::scheduleEngageEmail($aCampaignDetails, $sMailingName, $aResultsData, $sScheduleTime);       
    }
    
    
    /**
     * Send a general system email
     * 
     * @param string $sLang Language 
     * @param string $sMailBody body of email
     */
    public static function sendGeneralSystemEmail($sLang, $sMailBody) {
        
        
        $aPersonalisationData = array('mail_body' => $sMailBody);
  
        $aCampaignDetails = self::getCorrectCampaign(null, 0, $sLang);
        
        $dtDate = date("d/m/Y");
        
        $sMailingName = "System Email " . $dtDate . '|' . $sLang . '|' . uniqid();
        
        // Schedule mailing if required
        $sScheduleTime = null;
       
        Silverpop::scheduleEngageEmail($aCampaignDetails, $sMailingName, $aPersonalisationData, $sScheduleTime);
        
        
    }
// --------------------------------------------------------------------------//
//
// Everything below this divider was legacy code and can probably be removed
// once the system is fully up and running
//
// --------------------------------------------------------------------------//



}
