<?php
/**
 * Glucksspirale-specific functions
 * 
 * Results DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class GlucksspiraleLotteryResults extends LotteryResults {

    public function __construct($iLotteryID, $iLangID) {
        parent::__construct($iLotteryID, $iLangID);
    }

      
    public function getResultBreakdown($iDraw = NULL, $iLotteryID = NULL) {

        // Glucksspirale prize breakdowns are given in Roman numerals
        
        $aRoman = array(1=>'I', 'II', 'III', 'IV', 'V', 'VI', 'VII');
        
        // Amend query if we have draw details
        if ($iDraw) {
            $sWhere = " AND (ld.draw = {$iDraw} OR ld.id = {$iDraw}) ";
        }
        
        
        // If no lottery ID is supplied, use the one in the object
        if (is_null($iLotteryID))
        {
            $iLotteryID = $this->iLotteryID;
        }
        
        
        // Hack to ensure first result of a year select is displayed
        // Not pretty, but it'll have to do
        if (!empty($_GET['year']) && is_null($iDraw))
        {
            $iYear = (int) $_GET['year'];
            $sWhere =  $sWhere = " AND YEAR(ld.fk_lotterydate) = {$iYear} ";
        }
        
        // SQL query
        $sSQL = "SELECT
                    ld.numbers,
                    ld.fk_lotterydate as lotterydate,
                    ld.jackpot,
                    ld.draw,
                    ld.id,
                    ld.winnings
                FROM lottery_draws ld
                INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
                WHERE lc.lottery_id = {$iLotteryID}
                        {$sWhere}
                ORDER BY ld.fk_lotterydate DESC
                LIMIT 2";

        $aDraws = DAL::executeQuery($sSQL);

        // Get the drawn numbers
        $aTemp = explode("|", $aDraws[0]['numbers']);
        
        // Because Glucksspirale has 2 seperate numbers for each of the bottom two levels
        // we seperate these out now
        $aTopFive = array_slice($aTemp, 0, 5);
        $aBottomTwo = array_slice($aTemp, -4);

        $aNumbers = array();
        $iCount = 0;
        
        // Add the top 5 to the numbers array
        foreach ($aTopFive as $iNum)
        {
            $iCount++;
            $aNumbers[] = str_pad($iNum, $iCount,"0", STR_PAD_LEFT);
        }

        // Get the bottom  4 numbers and get them into 2 groups of 2
        $aNumbers[] = str_pad($aBottomTwo[0], 6,"0", STR_PAD_LEFT) . "<br/>" . str_pad($aBottomTwo[1], 6,"0", STR_PAD_LEFT);
        $aNumbers[] = str_pad($aBottomTwo[2], 7,"0", STR_PAD_LEFT) . "<br/>" . str_pad($aBottomTwo[3], 7,"0", STR_PAD_LEFT);     
        
        // Add the numbers and currency to the array
        $aDraws[0]['numbers'] =  $aNumbers;
        
        $aDraws[0]['currency'] = $this->getCurrencyDetails();
        /**
         * If we have no results use the previous draw as a template and clear out all but the bottom tier
         */
        if($aDraws[0]['winnings']=='b:0;'){
            $aDraws[0]['winnings'] = unserialize($aDraws[1]['winnings']);
            foreach($aDraws[0]['winnings'] as $sKey=>$aPrize){
                if($sKey!=sizeof($aDraws[0]['winnings'])){
                    $aDraws[0]['winnings'][$sKey]['prize']=-1;
                }
                $aDraws[0]['winnings'][$sKey]['winners']=-1;
            }
            $aDraws[0]['hasbreakdown']=false;
        }else{
            $aDraws[0]['hasbreakdown']=true;
            $aDraws[0]['winnings'] = unserialize($aDraws[0]['winnings']);
        }
        
        foreach ($aDraws[0]['winnings'] as $iPos=>$aData)
        {
            $aDraws[0]['winnings'][$iPos]['match'] = $aRoman[8-$aDraws[0]['winnings'][$iPos]['match']];
        }
        
        // Return the draw array
        return $aDraws[0];
    }

    
    
    public function getSupplementaryDataForDrawDate($iLotteryID, $sDrawDate)
    {
  
        $sSQL = "SELECT id FROM lottery_draws 
                 WHERE fk_lottery_id = {$iLotteryID}
                 AND DATE(fk_lotterydate) = '{$sDrawDate}'";
        $iDrawID = DAL::executeGetOne($sSQL);
        
        // Return a blank string if not found
        if ($iDrawID == "")
        {
            return "";           
        }
        
        // Return the results
        return $this->getResultBreakdown($iDrawID, $iLotteryID);
    }
            

}
