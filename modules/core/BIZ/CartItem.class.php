<?php

/**
 * Cart item
 * @package LoveLotto
 * @subpackage FrontEndModels
 */
class CartItem extends LLModel {

    protected $fPrice = NULL;
    /**
     * @deprecated Will not be saved, part of the old monogame system
     * @var array
     */
    protected $aNumbers = array();
    public $gameID = NULL;
    public $data = NULL;
    /**
     * Reference to the game engine (product) used to power this item
     * @var type
     */
    public $oGameEngine;

    /**
     * @deprecated Will not be saved, part of the old monogame system
     * @var integer
     */
    protected $iLotteryID = NULL;
    protected $sLotteryTitle = NULL;
    /**
     * @deprecated Will not be saved, part of the old monogame system
     * @var type
     */
    protected $dDrawDate = NULL;
    protected $dCutoffTime = NULL;

    public function __construct() {

    }

    function save($aData=array()) {
        $aData['price']=$this->fPrice;
        $aData['game_id']=$this->gameID;
        $aData['raw_data']=$this->data;
        $aData['cutoff_time']=$this->dCutoffTime;
        $aData['details']=$this->sLotteryTitle;
        parent::save($aData);
    }

    /**
     * Sets the local gameID variable
     * @deprecated
     * @param type $gameID
     */
    public function setGameID($iGameID) {
        $this->gameID = $iGameID;
        $this->oGameEngine=GameEngine::getById($iGameID);
    }

    /**
     * Sets the local rawdata variable
     * @deprecated
     * @param type $rawData
     */
    public function setData($rawData) {
        $this->data = $rawData;
    }

    /**
     * Sets the cost of this item
     * @deprecated
     * @param type $fPrice
     */
    public function setCost($fPrice) {
        $this->fPrice = $fPrice;
    }

    public function setLotteryID($iLotteryID) {
        $this->iLotteryID = $iLotteryID;
    }

    public function setLotteryTitle($sLotteryTitle) {
        $this->sLotteryTitle = $sLotteryTitle;
    }

    public function setDrawDate($dDrawDate) {
        $this->dDrawDate = $dDrawDate;
    }

    public function setNumbers($aNumbers) {
        foreach ($aNumbers as $key => $value) {
            $aNumbers[$key] = str_pad($value, 2, "0", STR_PAD_LEFT);
        }
        $this->aNumbers = $aNumbers;
    }

    public function setCutoffTime($dCutoff) {
        $this->dCutoffTime = $dCutoff;
    }

    public function getCost() {
        if($this->fPrice==0){
            if(!empty($this->oGameEngine)) {
                $this->fPrice = round($this->oGameEngine->getPrice(0,$this),2);
            }
        } else {
            $this->fPrice = $this->fPrice;
        }
        return reverseNumberFormat($this->fPrice);
    }

    public function getLotteryID() {
        return $this->iLotteryID;
    }

    public function getLotteryTitle() {
        return $this->sLotteryTitle;
    }

    public function getDrawDate() {
        return $this->dDrawDate;
    }

    public function getNumbers() {
        return $this->aNumbers;
    }

    public function getCutoffTime() {
        return $this->dCutoffTime;
    }

    public function convertToArray() {
        $aDetails = array();
        # $aDetails['price'] = $this->fPrice;
        $aDetails['gameID'] = $this->gameID;
        $aDetails['data'] = $this->data;
        return $aDetails;
    }

}
