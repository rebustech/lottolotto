<?php
/**
 * Results DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class LotteryResults extends Lottery {

    public function __construct($iLotteryID, $iLangID) {
        parent::__construct($iLotteryID, $iLangID);
    }

    public function getSmartyVariableName() {
        return "lottery" . $this->iLotteryID . "_";
    }
    
    public function getResultsStartYear()
    {
        $sSQL = "SELECT earliest_result FROM lotteries_cmn WHERE lottery_id = {$this->iLotteryID}";
        $iStartYear  = DAL::executeGetOne($sSQL);
        
        return $iStartYear;
    }
    
    public function getResults($iLimit = 1) {
        $sSQL = "SELECT
                    ld.id,
					ld.numbers,
					ld.fk_lotterydate as lotterydate,
					ld.jackpot,
					ld.draw
				FROM lottery_draws ld
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
				WHERE lc.lottery_id = {$this->iLotteryID}
				ORDER BY ld.fk_lotterydate DESC
				LIMIT {$iLimit}";
        $aDraws = DAL::executeQuery($sSQL);
        foreach ($aDraws as $key => $aDraw) {
            $aDraws[$key]['numbers'] = explode("|", $aDraw['numbers']);
            $aDraws[$key]['currency'] = $this->getCurrencyDetails();
        }
        return $aDraws;
    }
    
    public function getResultsByYear($iYear) {
        
        $sSQL = "SELECT
                    ld.id,
                    ld.numbers,
                    ld.fk_lotterydate as lotterydate,
                    ld.jackpot,
                    ld.draw
            FROM lottery_draws ld
            INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
            WHERE lc.lottery_id = {$this->iLotteryID}
            AND YEAR(ld.fk_lotterydate) = {$iYear}
            ORDER BY ld.fk_lotterydate DESC";
        $aDraws = DAL::executeQuery($sSQL);
        foreach ($aDraws as $key => $aDraw) {
            $aDraws[$key]['numbers'] = explode("|", $aDraw['numbers']);
            $aDraws[$key]['currency'] = $this->getCurrencyDetails();
        }
        return $aDraws;        
    }
    
    public function getResultBreakdown($iDraw = NULL, $iLotteryID = NULL) {

        if ($iDraw) {
            $sWhere = " AND (ld.draw = {$iDraw} OR ld.id = {$iDraw}) ";
        }
        
        if (is_null($iLotteryID))
        {
            $iLotteryID = $this->iLotteryID;
        }
        
        // Hack to ensure first result of a year select is displayed
        // Not pretty, but it'll have to do
        if (!empty($_GET['year']) && is_null($iDraw))
        {
            $iYear = (int) $_GET['year'];
            $sWhere =  $sWhere = " AND YEAR(ld.fk_lotterydate) = {$iYear} ";
        }
        $sSQL = "SELECT
					ld.numbers,
					ld.fk_lotterydate as lotterydate,
					ld.jackpot,
					ld.draw,
                    ld.id,
					ld.winnings
				FROM lottery_draws ld
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
				WHERE lc.lottery_id = {$iLotteryID}
					{$sWhere}
				ORDER BY ld.fk_lotterydate DESC
				LIMIT 2";
        $aDraws = DAL::executeQuery($sSQL);
        $aDraws[0]['numbers'] = explode("|", $aDraws[0]['numbers']);
        $aDraws[0]['currency'] = $this->getCurrencyDetails();
        /**
         * If we have no results use the previous draw as a template and clear out all but the bottom tier
         */
        if($aDraws[0]['winnings']=='b:0;'){
            $aDraws[0]['winnings'] = unserialize($aDraws[1]['winnings']);
            foreach($aDraws[0]['winnings'] as $sKey=>$aPrize){
                if($sKey!=sizeof($aDraws[0]['winnings'])){
                    $aDraws[0]['winnings'][$sKey]['prize']=-1;
                }
                $aDraws[0]['winnings'][$sKey]['winners']=-1;
            }
            $aDraws[0]['hasbreakdown']=false;
        }else{
            $aDraws[0]['hasbreakdown']=true;
            $aDraws[0]['winnings'] = unserialize($aDraws[0]['winnings']);
        }

        return $aDraws[0];
    }

	public function getPastResultBreakdown($allResults = false) {

		if(!$allResults) {
            $sWhere = " AND ld.fk_lotterydate >= DATE_SUB(NOW(), INTERVAL 1 YEAR) ";
        } else {
			$sWhere = '';
		}

        $sSQL = "SELECT
					ld.numbers,
					ld.fk_lotterydate as lotterydate,
					ld.jackpot,
					ld.draw,
                    ld.id,
					ld.winnings
				FROM lottery_draws ld
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
				WHERE lc.lottery_id = {$this->iLotteryID}
					{$sWhere}
				ORDER BY ld.fk_lotterydate DESC";

		# Get the results
        $aDraws = DAL::executeQuery($sSQL);

		# Parse each of them
		foreach($aDraws as &$draw) {
			$draw['numbers'] = explode("|", $draw['numbers']);
			$draw['currency'] = $this->getCurrencyDetails();
			$draw['winnings'] = unserialize($draw['winnings']);
		}

        return $aDraws;
    }

    /**
     * Gets a list of winners
     * @param type $iLotteryID
     * @param type $iLangID
     * @return array
     */
    public function getWinners($iLotteryID = NULL, $iLangID = NULL) {

        $sSQL = "SELECT
					lc.play_page_id,
					ll.title,
					lw.amount,
					lw.type,
					m.firstname,
					m.lastname
				FROM lottery_winnings lw
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = lw.fk_lottery_id
				INNER JOIN lotteries_lang ll ON ll.fk_lottery_id = lw.fk_lottery_id AND ll.fk_language_id = 1
				INNER JOIN booking_items bi ON bi.bookingitem_id = lw.fk_bookingitem_id
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
				INNER JOIN members m ON m.member_id = b.fk_member_id";
        $aWinners = DAL::executeQuery($sSQL);

        return $aWinners;
    }
    
    /**
     * Gets te draw date from the draw id
     * Used to find Spiel77/Super 6 results
     * 
     * @param integer $iDrawID draw id to search for
     * @return string
     */
    public function getDrawDateFromDrawID($iDrawID)
    {
        $sSQL = "SELECT fk_lotterydate FROM lottery_draws 
                 WHERE id = {$iDrawID} 
                 AND fk_lottery_id = {$this->iLotteryID}";
        
        $sDate = DAL::executeGetOne($sSQL);
 
        // Return the date bit of the date/time stamp
        if ($sDate != "")
        {
            $aDate = explode(" ", $sDate);
            return $aDate[0];
        }
        
        // Return blank string if not found
        return "";
    }
            
    /**
     * Get the supplementary numbers for this draw
     * Used only for Spiel77/Super6
     * 
     * @param integer $iLotteryID lottery ID
     * @param string $sDrawDate draw date
     * @return string
     */
    public function getSupplementaryNumbersForDrawDate($iLotteryID, $sDrawDate)
    {
  
        $sSQL = "SELECT numbers FROM lottery_draws 
                 WHERE fk_lottery_id = {$iLotteryID}
                 AND DATE(fk_lotterydate) = '{$sDrawDate}'";
                 
        $sNumbers = DAL::executeGetOne($sSQL);
        
        // Return a blank string if not found
        if ($sNumbers == "")
        {
            return "";
            
        }
        
        // Return numbers as a single string if found
        return str_replace("|", "", $sNumbers);                
    }
    
    
    /**
     * Get all the data for a particular draw date
     * @param integer $iLotteryID lottery ID
     * @param string $sDrawDate draw date in YYYY-MM-DD format
     * @return array
     */
    public function getSupplementaryDataForDrawDate($iLotteryID, $sDrawDate)
    {
  
        $sSQL = "SELECT id FROM lottery_draws 
                 WHERE fk_lottery_id = {$iLotteryID}
                 AND DATE(fk_lotterydate) = '{$sDrawDate}'";
        $iDrawID = DAL::executeGetOne($sSQL);
        
        // Return a blank string if not found
        if ($iDrawID == "")
        {
            return "";
            
        }
        
        // Return numbers as a single string if found
        return $this->getResultBreakdown($iDrawID, $iLotteryID);
    }
}
