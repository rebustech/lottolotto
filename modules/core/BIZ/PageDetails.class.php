<?php
/**
 * Pagedetails DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class PageDetails {

    var $iPageID = NULL;
    var $iLangID = NULL;
    var $aPageDetails = array();

    public function __construct($iPageID, $iLangID) {
        $this->iLangID = $iLangID;
        $this->iPageID = $iPageID;
    }

    public function getPageDetails() {
        $sSQL = "SELECT pc.url as url,
                        pc.is_active as is_active,
                        sc.section_id as section_id,
                        pc.parent_id as parent_id,
                        pc.cachetime as cachetime,
                        pl.page_title as page_title,
                        pl.browser_title as browser_title,
                        pl.menu_title as menu_title,
                        pl.description as description,
                        pl.keywords as keywords,
                        pl.content as content,
                        pc.left_node,
                        pc.right_node,
                        pc.fk_widget1,
                        pc.fk_widget2,
                        pc.fk_widget3,
                        pc.fk_widget4
                FROM pages_cmn pc
                LEFT JOIN pages_lang pl ON pl.fk_page_id = pc.page_id
                LEFT JOIN sections_cmn sc ON pc.fk_section_id = sc.section_id
                LEFT JOIN languages l ON l.language_id = pl.fk_language_id
                WHERE
                    pc.page_id = {$this->iPageID}

                ORDER BY l.default ASC
                LIMIT 1";

        $this->aPageDetails = DAL::executeGetRow($sSQL);
    }

    public function getCacheTime() {
        return $this->aPageDetails['cachetime'];
    }

    public function checkPageActive() {
        return $this->aPageDetails['is_active'];
    }

    public function getPageID() {
        return $this->iPageID;
    }

    public function getMenuTitle() {
        return $this->aPageDetails['menu_title'];
    }

    public function getPageTitle() {
        return $this->aPageDetails['page_title'];
    }

    public function getBrowserTitle() {
        return $this->aPageDetails['browser_title'];
    }

    public function getPageContent() {
        return $this->aPageDetails['content'];
    }

    public function getSEOKeywords() {
        return $this->aPageDetails['keywords'];
    }

    public function getSEODescription() {
        return $this->aPageDetails['description'];
    }

    public function getSectionID() {
        return $this->aPageDetails['section_id'];
    }

    public function getLeftNode() {
        return $this->aPageDetails['left_node'];
    }

    public function getRightNode() {
        return $this->aPageDetails['right_node'];
    }

    public function setPageTitle($sNewVal) {
        $this->aPageDetails['page_title'] = $sNewVal;
    }

    public function setBrowserTitle($sNewVal) {
        $this->aPageDetails['browser_title'] = $sNewVal;
    }

    public function setPageContent($sNewVal) {
        $this->aPageDetails['content'] = $sNewVal;
    }

    public function setSEOKeywords($sNewVal) {
        $this->aPageDetails['keywords'] = $sNewVal;
    }

    public function setSEODescription($sNewVal) {
        $this->aPageDetails['description'] = $sNewVal;
    }

}
