<?php

/**
 * FAQ DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class FAQs {

    protected $iLangID = 0;

    public function __construct($iLangID) {
        $this->iLangID = $iLangID;
    }

    public function getFAQCategories($iFAQCategoryID = NULL) {

        if ((int) $iFAQCategoryID) {
            $sWhere = " AND fcc.faqcategory_id = {$iFAQCategoryID} AND fcc.is_active = 1 ";
        }
        $sSQL = "SELECT fcc.faqcategory_id as faqcategory_id,
                        title, comment
                FROM faq_categories_cmn fcc
                WHERE fcc.is_active = 1
				{$sWhere}
                ORDER BY fcc.rank ASC";

        return DAL::executeQuery($sSQL);
    }

    public function getFAQs($iFAQCategoryID = NULL, $iLimit = NULL) {
        if ((int) $iFAQCategoryID) {
            $sWhere = " AND fc.fk_faqcategory_id = {$iFAQCategoryID} AND fcc.is_active = 1 ";
            $sInnerJoin = "INNER JOIN faq_categories_cmn fcc ON fcc.faqcategory_id = fc.fk_faqcategory_id AND fcc.is_active = 1 ";
        }
        $sSQL = "SELECT fc.faq_id as faq_id,
                        fl.question as question,
                        fl.answer as answer,
						fc.fk_faqcategory_id
                FROM faq_cmn fc
                INNER JOIN faq_lang fl ON fl.fk_faq_id = fc.faq_id
                INNER JOIN languages l ON l.language_id = fl.fk_language_id
                {$sInnerJoin}
                WHERE fc.is_active = 1
                AND
                    (
                        l.language_id = {$this->iLangID}
                        OR
                        l.default = 1
                    )
                AND fl.is_active = 1
                AND l.is_active = 1
                {$sWhere}
                ORDER BY fc.fk_faqcategory_id ASC, fc.rank ASC, l.default ASC";
        if ($iLimit) {
            $sSQL .= " LIMIT {$iLimit} ";
        }

        return DAL::executeGetUnique($sSQL, 'faq_id');
    }

    public function getFAQQuestions($iFAQCategoryID = NULL, $bPriority = false, $iLimit = NULL) {
        if ((int) $iFAQCategoryID) {
            $sWhere = " AND fc.fk_faqcategory_id = {$iFAQCategoryID} AND fcc.is_active = 1 ";
        }
        if ($bPriority) {
            $sWhere .= "AND fc.priority = 1 ";
        }
        $sSQL = "SELECT fc.faq_id as faq_id,
                        fl.question as question,
						fc.fk_faqcategory_id as faqcategory_id,
						fcl.title as title
                FROM faq_cmn fc
                INNER JOIN faq_lang fl ON fl.fk_faq_id = fc.faq_id
				INNER JOIN faq_categories_cmn fcc ON fcc.faqcategory_id = fc.fk_faqcategory_id AND fcc.is_active = 1
				INNER JOIN faq_categories_lang fcl ON fcl.fk_faqcategory_id = fcc.faqcategory_id
                INNER JOIN languages l ON l.language_id = fl.fk_language_id
                WHERE fc.is_active = 1
                AND
                    (
                        l.language_id = {$this->iLangID}
                        OR
                        l.default = 1
                    )
                AND fl.is_active = 1
                AND l.is_active = 1
				AND fcl.is_active = 1
                {$sWhere}
                ORDER BY fcc.rank ASC, fc.rank ASC, l.default ASC";
        if ($iLimit) {
            $sSQL .= " LIMIT {$iLimit} ";
        }
        return DAL::executeGetUnique($sSQL, 'faq_id');
    }

    function getFaqQuestionIds($iFaqCategoryId){
        $sSQL='SELECT faq_id FROM faq_cmn WHERE is_active=1 AND fk_faqcategory_id='.$iFaqCategoryId;
        return DAL::executeQuery($sSQL);
    }
}
