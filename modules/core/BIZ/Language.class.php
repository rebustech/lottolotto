<?php
/**
 * Language DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class Language {

    public static function getLanguageDetails($iLangID = NULL) {

        $cacheKey = 'Currency-getLanguageDetails';
        $sLanguage = "";

        if ($iLangID) {
            if(is_numeric($iLangID)) {
                $sLanguage = " l.language_id = {$iLangID} OR ";
                $cacheKey .= '-'.$iLangID;
            }
        }

        # Get the draws from the cache
        $languageData = LLCache::get($cacheKey);

        # Not in cache, query it
        if(empty($languageData)) {

            $sSQL = "SELECT l.language_id,
                        l.title,
                        l.charset,
						l.dbcharset,
                        l.icon,
						l.code,
						l.default
                FROM languages l
                WHERE l.is_active = 1
                    AND
                    (
                        {$sLanguage}
                        l.default = 1
                    )
                ORDER BY l.default ASC
                LIMIT 1";

            $languageData = DAL::executeGetRow($sSQL);

            # Store in the cache
            LLCache::add($cacheKey, $languageData, false, 900);

        }

        return $languageData;

    }

    public static function getLanguageDetailsByCode($sLangCode = NULL) {

        $cacheKey = 'Language-getLangaugeDetailsByCode';

        $sLanguage = "";
        if ($sLangCode) {
            $sLanguage = " AND l.code LIKE '{$sLangCode}' ";
            $cacheKey .= '-'.$sLangCode;
        }

        # Get the draws from the cache
        $languageData = LLCache::get($cacheKey);

        # Not in cache, query it
        if(empty($languageData)) {

            $sSQL = "SELECT l.language_id,
                        l.title,
                        l.charset,
						l.dbcharset,
                        l.icon,
						l.code,
						l.default
                FROM languages l
                WHERE l.is_active = 1
                      {$sLanguage}
                ORDER BY l.default ASC
                LIMIT 1";

            $languageData = DAL::executeGetRow($sSQL);

            # Store it
            LLCache::add($cacheKey, $languageData, false, 900);

        }

        return $languageData;

    }

    public static function getLanguages() {

        $cacheKey = 'Language-getLanguages';

        # Get the draws from the cache
        $languageData = LLCache::get($cacheKey);

        # Not in cache, query it
        if(empty($languageData)) {

            $sSQL = "SELECT l.language_id,
                l.title,
                l.icon,
				l.code
            FROM languages l
            WHERE l.is_active = 1

            ORDER BY l.default DESC, l.title ASC";

            $languageData =  DAL::executeQuery($sSQL);

            # Store it
            LLCache::add($cacheKey, $languageData, false, 900);

        }

        return $languageData;

    }

    public static function getLanguageTitle($iLanguageID) {

        $cacheKey = 'Language-getLanguageTitle-'.$iLanguageID;

        # Get the draws from the cache
        $languageData = LLCache::get($cacheKey);

        # Not in cache, query it
        if(empty($languageData)) {

            $sSQL = "SELECT l.title
				FROM languages l
				WHERE l.language_id = {$iLanguageID}";

            $languageData =  DAL::executeGetOne($sSQL);

            # Store it
            LLCache::add($cacheKey, $languageData, false, 900);

        }

        return $languageData;

    }

    public static function getLanguage($iLanguageID) {

        $cacheKey = 'Language-getLanguage-'.$iLanguageID;

        # Get the draws from the cache
        $languageData = LLCache::get($cacheKey);

        # Not in cache, query it
        if(empty($languageData)) {

            $sSQL = "SELECT l.language_id as id,
					l.title,
					l.icon
			FROM languages l
			WHERE l.language_id = {$iLanguageID}";

            $languageData = DAL::executeGetRow($sSQL);

            # Store it
            LLCache::add($cacheKey, $languageData, false, 900);

        }

        return $languageData;

    }

    public static function getAllAdminLanguageList() {
        $sSQL = "SELECT l.language_id as id,
                        l.title,
                        l.icon
                FROM languages l

                ORDER BY l.default DESC, l.title ASC";
        return DAL::executeQuery($sSQL);
    }
    
    
    // Get language code from the id
    // (used for Email sending)
    public static function getLangCodeFromID($iLangID) {

        $sSQL = "SELECT code
                 FROM languages l
                 WHERE l.language_id = {$iLangID}";

        $sCode = DAL::executeGetOne($sSQL);

        return $sCode;

    }
}
