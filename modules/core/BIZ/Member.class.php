<?php

/**
 * Member DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class Member extends LLModel{

    // Copied from base class, need to be edited for this class
    static $sTableName='members';
    static $sKeyField='member_id';
    static $sHandlerField='';


    public $iMemberID = NULL;
    protected $bValidated = false;
    protected $sUserName = NULL;
    protected $sEncryptionKey = NULL;
    protected $sFullName = NULL;
    protected $dLoginDate = NULL;
    protected $sIP = NULL;
    public $bIsLoggedIn = false;
    protected $fBalance = 0;
    protected $iTicketsCount = 0;
    protected $aMemberDetails = array();
    protected $iRedirectPageID = 0;
    protected $bBlockedCountry = NULL;

    /**
     * Gets the current logged in member or null if nobody is logged in.
     * @return mixed
     */
    static function getLoggedInMember(){
        $oMember = isset($_SESSION['memberobject']) ? unserialize($_SESSION['memberobject']) : null;
        if (is_object($oMember) && is_a($oMember, "Member")) {
            $oMember=self::getById($oMember->iMemberID);
            return $oMember;
        } else {
            return null;
        }
    }

    public function __construct($sUsername = NULL, $sPassword = NULL) {
        $this->clearConstructor();
        if ($sUsername && $sPassword) {
            $this->performLogin($sUsername, $sPassword);
            $this->sUserName = $sUsername;
        }
    }

    protected function clearConstructor() {
        $this->iMemberID = NULL;
        $this->bValidated = false;
        $this->sUserName = NULL;
        $this->sEncryptionKey = NULL;
        $this->sFullName = NULL;
        $this->dLoginDate = NULL;
        $this->fBalance = 0;
        $this->iTicketsCount = 0;
        $this->sIP = NULL;
        $this->bIsLoggedIn = false;
        $this->aMemberDetails = array();
        $this->bBlockedCountry = NULL;
    }

	public function forceMemberDetailUpdate() {
		$this->aMemberDetails = array();
	}

    public function setRedirectPage($sValue) {
        $this->iRedirectPageID = $sValue;
    }

    public function getRedirectPage() {
        return $this->iRedirectPageID;
    }

    public function updateTicketsCount($reference = 0) {
        $sSQL = "SELECT
					COUNT(bi.bookingitem_id)
				FROM booking_items bi
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
				INNER JOIN transactions t ON t.transaction_id = b.fk_transaction_id
				INNER JOIN r_lottery_dates rld ON rld.fk_lottery_id = bi.fk_lottery_id AND rld.drawdate = bi.fk_lotterydate
				WHERE b.fk_member_id = {$this->iMemberID}
					".(!empty($reference) ? " AND b.bookingreference = '{$reference}'" : '')."
					AND t.confirmed = 1";
        $this->iTicketsCount = DAL::executeGetOne($sSQL);
    }


    public static function updateMemberBalance($iMemberID) {
		if(empty($iMemberID)) {
			return;
		}

        // We don't want to include confirmed split withdrawal transactions here
        // so we ignore them as they have a seperate status
        $sSQL = "SELECT SUM(t.amount)
                FROM transactions t
                WHERE t.fk_member_id = {$iMemberID}
                AND t.confirmed = 1
                AND t.fk_status_id != 6";

        $fBalance = DAL::executeGetOne($sSQL);
        $sTablename = "members";
        $aData = array(
            'balance' => $fBalance
        );
        $sParams = "member_id = {$iMemberID}";

        return DAL::Update($sTablename, $aData, $sParams);
    }



    public function updateBalance() {
		if(empty($this->iMemberID)) {
			return;
		}

        // We don't want to include confirmed split withdrawal transactions here
        // so we ignore them as they have a seperate status
        $sSQL = "SELECT SUM(t.amount)
		 FROM transactions t
	         WHERE t.fk_member_id = {$this->iMemberID}
		 AND t.confirmed = 1
                 AND t.fk_status_id != 6";

        $this->fBalance = DAL::executeGetOne($sSQL);
        $sTablename = "members";
        $aData = array(
            'balance' => $this->fBalance
        );
        $sParams = "member_id = {$this->iMemberID}";

        return DAL::Update($sTablename, $aData, $sParams);
    }


    /**
     * Updates a member's email alert preferences
     *
     * @param array $aAlertData
     * @param integer $intMemberID only used when accessing from CRM
     * @return boolean
     */
    public function updateMemberAlerts($aAlertData, $iMemberID = null)
    {

        // $aAlertData should be in this format
        // 'jackpot_6aus49' => 1, 'result_6aus49' => 0
        if(empty($this->iMemberID) && $iMemberID == null) {
            return;
        }

        $sTablename = "members_alerts";


        // Use passed member id if supplied, else use member ID in object
        if (!is_null($iMemberID))
        {
            $iMemberID = (int) $iMemberID;
            $sParams = "member_id = {$iMemberID}";
        }
        else
        {
            $sParams = "member_id = {$this->iMemberID}";
        }

        return DAL::Update($sTablename, $aAlertData, $sParams);
    }


    public function getTransactions(&$iTotalTransactions, $iStart = NULL, $iLimit = NULL) {

        // SQL has been amended to ensure we don't include confirmed split withdrawal transactions
        if ($this->getMemberID()) {
            $sSQL = "
                    SELECT
						SQL_CALC_FOUND_ROWS
                        *, t.bookingreference,
                        t.amount,
                        t.transaction_date as date,
                        c.shortname as currency,
						   c.symbol AS currencysymbol,
                        g.comment as gateway,
                        s.comment as status
                    FROM transactions t
                    INNER JOIN currencies c ON c.currency_id = t.fk_currency_id
                    INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
                    INNER JOIN status s ON s.status_id = t.fk_status_id

                    WHERE t.fk_member_id = {$this->getMemberID()}
                    AND t.confirmed = 1
                    AND t.fk_status_id != 6
                    ORDER BY t.transaction_date DESC
                ";
            if (!is_null($iStart) && !is_null($iLimit)) {
                $sSQL .= " LIMIT {$iStart}, {$iLimit}";
            }
            $aTransactions = DAL::executeQuery($sSQL);
            $iTotalTransactions = DAL::executeGetOne("SELECT FOUND_ROWS()");
            return $aTransactions;
        }
    }

	public function getBookingOrderItems($iLangID, &$iTotalTickets, $iStart = NULL, $iLimit = NULL, $status = 'all', $month=null) {

            if($month==null) $month=date('m');

		# Need a valid member
        if($this->getMemberID()) {
            $sSQL = "SELECT
				lw.amount as winAmount,
				s.comment as statusType,
				bi.is_subscription,
				bi.id,
                                bi.id as fk_order_item_id,
				b.booking_id,
				bi.config,
				b.bookingreference,
				b.booking_date,
				b.total,
				bi.created_at,
				bi.cost,
				c.shortname as currency,
				c.symbol as currencysymbol,
				ge.name AS gameName,
                ge.id AS gameID,
                ge.*,
                t.*,
                t.transaction_id
			FROM booking_order_items bi
			LEFT JOIN bookings b ON b.booking_id = bi.fk_booking_id
			LEFT JOIN game_engines ge ON ge.id = bi.fk_game_engine_id
			LEFT JOIN lottery_winnings lw ON lw.fk_bookingitem_id = bi.id
			INNER JOIN transactions t ON t.transaction_id = b.fk_transaction_id
			INNER JOIN currencies c ON c.currency_id = b.fk_currency_id
			INNER JOIN status s ON s.status_id = b.fk_status_id
			WHERE b.fk_member_id = '{$this->getMemberID()}'
				AND ge.active = 1
				".(($status != 'all' && $status != '-') ? "AND s.status_id = '{$status}'" : '')."
                                    AND month(bi.created_at)=$month
			ORDER BY bi.created_at DESC,
				bi.id DESC";

			if(isset($_GET['debug'])) {
				var_dump($sSQL);
			}

            $aTickets = DAL::executeQuery($sSQL);
            if (!is_null($iStart) && !is_null($iLimit)) {
                $aTickets = array_slice($aTickets, $iStart, $iLimit);
            }

            $iTotalTickets = $this->iTicketsCount;
            return $aTickets;
        }
    }

	public function getBookingItems($iLangID, &$iTotalTickets, $reference = 0, $iStart = NULL, $iLimit = NULL, $status = 'all') {

		if($status == 'ticket') {
			$ticketId = $reference;
			$reference = 0;
			$status = 'all';
		} else {
			$ticketId = 0;
		}

        if(is_null($iStart) || !isset($iStart)) {
            $iStart = 0;
        }

        if(is_null($iLimit) || !isset($iLimit)) {
            $iLimit = 20;
        }

		# Need a valid member
        if($this->getMemberID()) {

            $sSQL = "SELECT
				s.comment AS statusType,
				b.bookingreference,
				b.booking_date,
				b.total,
                b.booking_id,
				bi.bookingitem_id,
				b.booking_date as purchase_date,
				bi.cost,
                boi.id,
				boi.config,
				boi.is_subscription,
				boi.created_at,
				bi.fk_lotterydate as draw_date,
				IF(DATE_SUB(rld.drawdate, INTERVAL rld.cutoff MINUTE) < NOW(), 1, 0) as drawn,
				ld.processed as processed,
				c.shortname as currency,
				c.symbol as currencysymbol,
				cc.symbol as lotterycurrencysymbol,
				cc.currency_id as currency_id,
				lc.comment as lottery_title,
				bi.numbers as numbers,
				lw.transaction_amount as winnings,
				lw.amount as winning_amount,
				lw.paid as paid,
				lw.type as winning_match,
				lc.lottery_id as lottery_id,
				lc.number_count as number_count,
				lc.bonus_numbers as bonus_numbers,
				lc.colour as lottery_colour,
                ge.name AS gameName,
                ge.id AS gameID,
                ge.engine_core,
                t.gatewayreference
			FROM booking_items bi
			INNER JOIN booking_order_items boi ON (boi.id = bi.fk_order_item_id)
			INNER JOIN bookings b ON b.booking_id = boi.fk_booking_id
            LEFT JOIN game_engines ge ON ge.id = boi.fk_game_engine_id
			INNER JOIN r_lottery_dates rld ON rld.fk_lottery_id = bi.fk_lottery_id AND rld.drawdate = bi.fk_lotterydate
			LEFT JOIN lottery_winnings lw ON lw.fk_bookingitem_id = bi.bookingitem_id
			LEFT JOIN lottery_draws ld ON ld.fk_lottery_id = bi.fk_lottery_id AND ld.fk_lotterydate = bi.fk_lotterydate
            INNER JOIN transactions t ON t.transaction_id = b.fk_transaction_id
			INNER JOIN status s ON s.status_id = b.fk_status_id
			INNER JOIN lotteries_cmn lc ON lc.lottery_id = bi.fk_lottery_id
			INNER JOIN currencies c ON c.currency_id = b.fk_currency_id
			INNER JOIN currencies cc ON cc.currency_id = lc.fk_currency_id
			WHERE b.fk_member_id = '{$this->getMemberID()}'
				".(!empty($reference) ? "AND bi.fk_order_item_id = '{$reference}'" : '')."
				".(!empty($ticketId) ? "AND bi.bookingitem_id = '{$ticketId}'" : '')."
				".(($status != 'all' && $status != '-') ? "AND s.status_id = '{$status}'" : '')."
			ORDER BY b.booking_date ASC,
				bi.fk_lotterydate ASC,
				bi.fk_lottery_id ASC,
				bi.bookingitem_id DESC
			".(!is_null($iStart) && !is_null($iLimit) ? "LIMIT {$iStart}, {$iLimit}" : '');

            $aTickets = DAL::executeQuery($sSQL);
            if(!is_null($iStart) && !is_null($iLimit)) {
                $aTickets = array_slice($aTickets, $iStart, $iLimit);
            }

            /*
			# Explode all numbers
            foreach($aTickets as $key => $aTicket) {
                $aTickets[$key]['numbers'] = explode("|", $aTicket['numbers']);
            }
            */

			# Update the tally
			if(empty($reference)) {
				$this->updateTicketsCount($reference);
			}

			# Number of tickets matching
            $iTotalTickets = $this->iTicketsCount;

            return $aTickets;
        }
    }

	public function getBookingByReference($iLangID, $reference) {

		 if($this->getMemberID()) {
            $sSQL = "SELECT
				s.comment AS statusType,
				b.bookingreference,
				b.booking_date,
				b.total,
				b.booking_date as purchase_date,
				c.shortname as currency,
				c.symbol as currencysymbol,
                t.gatewayreference
			FROM bookings b
			INNER JOIN transactions t ON t.transaction_id = b.fk_transaction_id
			INNER JOIN currencies c ON c.currency_id = b.fk_currency_id
			INNER JOIN status s ON s.status_id = b.fk_status_id
			WHERE b.fk_member_id = '{$this->getMemberID()}' AND b.bookingreference = '{$reference}'
			ORDER BY b.booking_date DESC
			LIMIT 1";

            $booking = DAL::executeGetRow($sSQL);
           	return $booking;
        }

	}

	public function getBookingByOrderItem($iLangID, $orderItem) {

		 if($this->getMemberID()) {
            $sSQL = "SELECT
				bi.*,
				s.comment AS statusType,
				b.bookingreference,
				b.booking_date,
				b.total,
				b.booking_date as purchase_date,
				c.shortname as currency,
				c.symbol as currencysymbol
			FROM booking_order_items bi
			LEFT JOIN bookings b ON b.booking_id = bi.fk_booking_id
			INNER JOIN transactions t ON t.transaction_id = b.fk_transaction_id
			INNER JOIN currencies c ON c.currency_id = b.fk_currency_id
			INNER JOIN status s ON s.status_id = b.fk_status_id
			WHERE b.fk_member_id = '{$this->getMemberID()}' AND bi.id = '{$orderItem}'
			ORDER BY b.booking_date DESC
			LIMIT 1";

            $booking = DAL::executeGetRow($sSQL);
           	return $booking;
        }

	}

    public function getTickets($iLangID, &$iTotalTickets, $iStart = NULL, $iLimit = NULL) {

		/*
		FROM booking_items bi
		INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
		*/

        if($this->getMemberID()) {
            $sSQL = "SELECT
				s.comment AS statusType,
				b.bookingreference,
				b.booking_date,
				b.total,
				b.booking_date as purchase_date,
				c.shortname as currency,
				c.symbol as currencysymbol
			FROM bookings b
			INNER JOIN transactions t ON t.transaction_id = b.fk_transaction_id
			INNER JOIN currencies c ON c.currency_id = b.fk_currency_id
			INNER JOIN status s ON s.status_id = b.fk_status_id
			WHERE b.fk_member_id = '{$this->getMemberID()}'
			ORDER BY b.booking_date DESC";

            $aTickets = DAL::executeGetUnique($sSQL, "bookingreference");
            if(!is_null($iStart) && !is_null($iLimit)) {
                $aTickets = array_slice($aTickets, $iStart, $iLimit);
            }
            $iTotalTickets = $this->iTicketsCount;
            return $aTickets;
        }
    }


    // Static function to be used as part of the Winscan/email process
    // to ascertain if the current order is the first this member has placed

    public static function isFirstOrder($iMemberId){
        $sSQL = "SELECT COUNT(*) as num_orders FROM bookings b
                 WHERE b.fk_member_id = {$iMemberId}";

        $aRow = DAL::executeGetRow($sSQL);

        return ($aRow['num_orders'] <=1? true:false);

    }


    public function getMemberBalance($bUpdate = false) {
        if ($bUpdate) {
            $this->updateBalance();
        }
        return $this->fBalance;
    }

    public function getMemberDetails($iLangID = 1) {
        if ($this->validateMember()) {
            if (empty($this->aMemberDetails) || empty($this->aMemberDetails['firstname'])) {
                $sSQL = "SELECT
							m.*,
							ma.*,
							m.member_id,
							cl.title as country
						FROM members m
						LEFT JOIN countries_lang cl ON cl.fk_country_id = m.fk_country_id AND cl.fk_language_id = {$iLangID}
						LEFT JOIN members_alerts ma ON (m.member_id = ma.member_id)
						WHERE m.member_id = {$this->getMemberID()} ";

                $this->aMemberDetails = DAL::executeGetRow($sSQL);
                $this->getCountryHomepageCurrencyMapping();
                
                $this->updateTicketsCount();
            } else {
                $sSQL = "SELECT
							m.balance
						FROM members m
						WHERE m.member_id = {$this->getMemberID()}";
                $this->aMemberDetails['balance'] = DAL::executeGetOne($sSQL);
            }
            $this->fBalance = $this->aMemberDetails['balance'];
            $this->aMemberDetails['tickets'] = $this->iTicketsCount;
            return $this->aMemberDetails;
        }
    }

    // Copy of above function, without validation check
    // used for CRM/ telemarketing payment processing
    public function getMemberDetailsCRM($iLangID = 1) {
        if (empty($this->aMemberDetails) || empty($this->aMemberDetails['firstname'])) {
            $sSQL = "SELECT
                            m.*,
                            ma.*,
                            m.member_id,
                            cl.title as country
                    FROM members m
                    LEFT JOIN countries_lang cl ON cl.fk_country_id = m.fk_country_id AND cl.fk_language_id = {$iLangID}
                    LEFT JOIN members_alerts ma ON (m.member_id = ma.member_id)
                    WHERE m.member_id = {$this->getMemberID()} ";

            $this->aMemberDetails = DAL::executeGetRow($sSQL);
            $this->updateTicketsCount();
        } else {
            $sSQL = "SELECT
                            m.balance
                    FROM members m
                    WHERE m.member_id = {$this->getMemberID()}";
            $this->aMemberDetails['balance'] = DAL::executeGetOne($sSQL);
        }
        $this->fBalance = $this->aMemberDetails['balance'];
        $this->aMemberDetails['tickets'] = $this->iTicketsCount;
        
        // Return the details
        return $this->aMemberDetails;
    }
    
    
    public function Logout() {
        $this->clearConstructor();
        unset($_SESSION['legalagreement']);
        unset($_SESSION['member_ip']);
        unset($_SESSION['iso2country']);
        unset($_SESSION['geoip_check']);
        unset($_SESSION['redirect_check']);
        unset($_SESSION['is_localhost']);
        if(!empty($_SESSION)) {
            foreach($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
        }
        
        // Destroy session
        session_destroy();
    }

    public function performLogin($sUsername, $sPassword) {
        try {
            $bIsAuthenticated = false;
            if ($sUsername && $sPassword) {
                $sql = "SELECT 	m.member_id,
                                m.password,
                                m.is_active,
                                m.encryptionkey,
                                CONCAT_WS(' ', m.firstname, m.lastname) AS fullname,
                                m.fk_country_id as country_id,
                                m.signup_country_id,
                                m.signup_currency_id,
                                m.signup_language_id,
                                c.blocked as blocked,
                                c.code as iso2_code
                FROM members m
                INNER JOIN countries_cmn c ON c.country_id = m.fk_country_id
                WHERE m.username  LIKE '{$sUsername}'";

                $aCredentials = DAL::executeGetRow($sql);
                if($aCredentials['is_active'] == 1) {
                    $bPassword = Encryption::Validate($sPassword, $aCredentials['password']);
                    if ($bPassword) {
                        $bIsAuthenticated = true;
                        $this->sUserName = $sUsername;
                        $this->iMemberID = $aCredentials['member_id'];
                        $this->sEncryptionKey = $aCredentials['encryptionkey'];
                        $this->sFullName = $aCredentials['fullname'];
                        $this->dLoginDate = date(strtotime("now"));
                        $this->updateBalance();
                        if ($aCredentials['blocked'] || $this->bBlockedCountry) {
                            $this->bBlockedCountry = 1;
                        } else {
                            $this->bBlockedCountry = 0;
                        }
                        $this->sIP = ($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                                             
                        // Set these in aMemberDetails as they are used to determine 
                        // what country site to redirect to
                        //$this->aMemberDetails['fK_country_id'] = $aCredentials['country_id'];
                        //$this->aMemberDetails['member_iso2code'] = $aCredentials['iso2_code'];
                         
//                        $rrr = print_r($aCredentials, true);
//                        echo ("<!---rrr " . $rrr . " --->");
                        
                        
                        // Only do this next bit if we're not in a localhost environment
                        if (!isset($_SESSION['is_localhost']))
                        {
                            // Use the customer's signed up country/currency to  retrieve the mapping from the country ID
                            $aMap = GeoIP::getCountryInfoFromID($aCredentials['signup_country_id']);

    //                        $sss = print_r($aMap, true);
    //                        echo ("<!---map " . $sss . " --->");

                            $this->aMemberDetails['signup_country_id'] = $aCredentials['signup_country_id'];
                            $this->aMemberDetails['signup_country_iso2code'] = $aMap['code'];
                            $this->aMemberDetails['signup_currency_id'] = $aCredentials['signup_currency_id'];
                            $this->aMemberDetails['signup_language_id'] = $aCredentials['signup_language_id'];
                        }
                    } else {
                        $this->clearConstructor();
                        $this->addError("Invalid username or password");
                    }
                } else {
                    $this->clearConstructor();
                    $this->addError("Invalid username or password");
                }
            }
            $this->bIsLoggedIn = $bIsAuthenticated;
        } catch (Exception $e) {
            $this->clearConstructor();
            $this->bIsLoggedIn = $bIsAuthenticated;
        } 
        return $bIsAuthenticated;
    }

// eof

    public function validateMember() {
        $bValid = false;
        if ($this->bIsLoggedIn && $this->sEncryptionKey == $this->getEncryptionKey()) {
            $bValid = true;
        }
        return $bValid;
    }

    protected function getEncryptionKey() {
        if ($this->getMemberID()) {
            $sSQL = "SELECT m.encryptionkey
					FROM members m
					WHERE m.member_id = {$this->getMemberID()}";

            return DAL::executeGetOne($sSQL);
        }
    }

    public function changeEmailPreferences($bGeneralOffers, $bLotteryDraws) {
        $bValid = false;

        $aData = array(
            "email_generaloffers" => (bool) $bGeneralOffers,
            "email_lotterydraws" => (bool) $bLotteryDraws
        );
        $sTablename = "members";
        $sParams = "member_id = {$this->getMemberID()}";
        $bValid = DAL::Update($sTablename, $aData, $sParams);
        $this->aMemberDetails = array();
        return $bValid;
    }

    public function changePassword($sOldPassword, $sNewPassword, $sConfirmPassword) {
        $bValid = false;
        if ($sNewPassword && $sConfirmPassword && $sOldPassword) {
            if (strlen($sNewPassword) >= 6) {
                if ($sNewPassword == $sConfirmPassword) {
                    $sql = "SELECT password
						FROM members
						WHERE member_id ='" . $this->getMemberID() . "'";

                    $sDBPassword = DAL::executeGetOne($sql);
                    if ($sDBPassword) {

                        $bPassword = Encryption::Validate($sOldPassword, $sDBPassword);
                        if ($bPassword) {
                            $tablename = "members";
                            $data = array(
                                'password' => Encryption::Encrypt($sNewPassword)
                            );
                            $params = "member_id = '" . $this->getMemberID() . "'";
                            $bValid = DAL::Update($tablename, $data, $params);
                            if ($bValid) {
                                Email::sendChangePasswordEmail($this->aMemberDetails['firstname'], $this->aMemberDetails['lastname'], $this->getUsername(), $sNewPassword);
                            }
                        } else {
                            $this->addError(VCMS::get('Error.Password.Incorrect'));
                        }
                    }
                } else {
                    $this->addError(VCMS::get('Error.Password.Mismatch'));
                }
            } else {
                $this->addError(VCMS::get('Error.Password.Length'));
            }
        } else {
            $this->addError(VCMS::get('Error.MissingFields'));
        }
        return $bValid;
    }

	public function updateMemberDetails($updateDetails) {
		$bValid = false;
		$sTablename = "members";
		$sParams = "member_id = {$this->getMemberID()}";
		$bValid = DAL::Update($sTablename, $updateDetails, $sParams);
		return $bValid;
    }

    public function saveMemberDetails($aNewDetails, $bUpdateFromAccountPage = false) {

        // Add validation from front end
        $bNoErrors = true;

        $bValid = false;
        #if (trim($aNewDetails['firstname']) && trim($aNewDetails['lastname']) && trim($aNewDetails['contact'])) {

         // Check phone number is correct
        // we can improve on this further down the line
        if (!preg_match('/^[0-9\-\+]*$/', $aNewDetails['contact'])) {
            $bNoErrors = false;
            $this->addError(VCMS::get('Error.Req.Phone.Digits'));
        }

        // Check phone number length is acceptable
        // Again, this is just a simple length check - we can improve on validity checking later
        $iContactLength = strlen($aNewDetails['contact']);

        if ($iContactLength < 10 || $iContactLength > 26) {
            $bNoErrors = false;
            $this->addError(VCMS::get('Error.Req.Phone.Length'));
        }


        if (!$aNewDetails['contact']) {
                $bNoErrors = false;
                $this->addError(VCMS::get('Error.Req.Phone'));
        }

        if (!$aNewDetails['firstname']) {
                $bNoErrors = false;
                $this->addError(VCMS::get('Error.Req.FirstName'));
        }

        if (!$aNewDetails['lastname']) {
                $bNoErrors = false;
                $this->addError(VCMS::get('Error.Req.Surname'));
        }

        $aData = array(
            "firstname" => $aNewDetails['firstname'],
            "lastname" => $aNewDetails['lastname'],
            "contact" => $aNewDetails['contact']);

        # Set country / address
        if(!isset($aNewDetails['ajax'])) {

            if(isset($aNewDetails['city'])) {
                $aData["city"] = $aNewDetails['city'];
            }
            else
            {
                $bNoErrors = false;
                $this->addError(VCMS::get('Error.Req.City'));
            }

            if(isset($aNewDetails['address1'])) {
                $aData["address1"] = $aNewDetails['address1'];
            }
            else
            {
                $bNoErrors = false;
                $this->addError(VCMS::get('Error.Req.Address1'));
            }

            /*
            if(isset($aNewDetails['address2'])) {
                $aData["address2"] = $aNewDetails['address2'];
            }
             */

            if(isset($aNewDetails['zip'])) {
                $aData["zip"] = $aNewDetails['zip'];
            }
            else
            {
                $bNoErrors = false;
                $this->addError(VCMS::get('Error.Req.Zip'));
            }

            if(isset($aNewDetails['county_state'])) {
                $aData['county_state'] = $aNewDetails['county_state'];
            }

        }


        // DOB and title are not on the update details page
        // so don't update them
        if ($bUpdateFromAccountPage === false) {
            $aData["dob"] = $aNewDetails['dob'];
            $aData["title"] = $aNewDetails['title'];
        }

        # Date of Birth
        if(isset($aNewDetails['dob'])) {
            $aData["dob"] = $aNewDetails['dob'];
        }

        # Title field
        if(isset($aNewDetails['title'])) {
            $aData["title"] = $aNewDetails['title'];
        }

        /*
        ALTER TABLE `members`
            ADD COLUMN `gender` ENUM('M','F') NOT NULL AFTER `password`;
        */
        if(isset($aNewDetails['gender'])) {
            $aData['gender'] = $aNewDetails['gender'];
        }

        if ($bNoErrors === true) {

            $this->sFullName = $aNewDetails['firstname'] . " " . $aNewDetails['lastname'];
            $sTablename = "members";
            $sParams = "member_id = {$this->getMemberID()}";
            $bValid = \DAL::Update($sTablename, $aData, $sParams);
            $this->aMemberDetails = array();


            if ($bUpdateFromAccountPage !== false)
            {
                // Make call to update Silverpop here if member has updated
                // their details
                Silverpop::addModifyContactToMarketingDatabase($this->getMemberID(), true);
            }


            #} else {
            #    $this->addError("Please complete all required fields marked with an *.");
            #}

        }
        return $bValid;
    }

    /**
     * Save member details as part of the addFunds page
     * Only used if the address details need to be updated
     *
     * @param array $aNewDetails new member details
     * @return boolean
     */
    public function saveMemberDetailsAddFunds($aNewDetails) {

        $bValid = false;

        # Set country / address
        if(isset($aNewDetails['city'])) {
            $aData["city"] = $aNewDetails['city'];
        }

        if(isset($aNewDetails['city'])) {
            $aData["address1"] = $aNewDetails['address1'];
        }

        if(isset($aNewDetails['address2'])) {
            $aData["address2"] = $aNewDetails['address2'];
        }

        if(isset($aNewDetails['city'])) {
            $aData["zip"] = $aNewDetails['zip'];
        }

        if (isset($aNewDetails['country'])) {
            $aData["fk_country_id"] = $aNewDetails['country'];
        }

        $sTablename = "members";
        $sParams = "member_id = {$this->getMemberID()}";

        $bValid = \DAL::Update($sTablename, $aData, $sParams);

        $this->aMemberDetails = array();

        Silverpop::addModifyContactToMarketingDatabase($this->getMemberID(), true);

        return $bValid;
    }

    // AddMemberFull
    // This is used when creating a member via the CRM system
    // (i.e. telemarketing)
    
    public function addMemberFull($aNewDetails) {
            $bValid = false;

            // Generate password automatically
            // this will be passed to the email
            $sPassword = Encryption::generateRandomString(10);
            
            $aData = array(
                            "firstname" => $aNewDetails['firstname'],
                            "lastname" => $aNewDetails['lastname'],
                            "city" => $aNewDetails['city'],
                            "address1" => $aNewDetails['address1'],
                            "address2" => $aNewDetails['address2'],
                            "county_state" => $aNewDetails['county_state'],
                            "zip" => $aNewDetails['zip'],
                            "contact" => $aNewDetails['contact'],
                            "dob" => $aNewDetails['dob'],
                            "title" => $aNewDetails['title'],
                            "email" => $aNewDetails['email'],
                            "username" => $aNewDetails['email'],
                            "gender" => $aNewDetails['gender'],
                            'fk_country_id' => $aNewDetails['fk_country_id'],
                            'password' => Encryption::Encrypt($sPassword),
                            'is_active' => 1
                            );

            $sTablename = "members";

            $iMemberId = DAL::Insert($sTablename, $aData, true);

            
            $aPersonalisationData = array('firstname' => $aNewDetails['firstname'],
                                          'current balance' => number_format_locale('0.00', 2, '.', ','),
                                          'member_id' => $iMemberId,
                                          'password' => $sPassword,
                                          'email_ref' => 'new_registration_crm');

            // Send email through Silverpop
            Email::newRegistrationCRMEmail($iMemberId, $aNewDetails['email'], $aPersonalisationData);

        return $iMemberId;
	}

    public function addMember($sEmail, $sPassword, $sFirstname, $sLastname, $iCountryID) {
        $bValid = false;
        if ($this->checkUsernameAvailable($sEmail)) {
            $aData = array(
                'username' => $sEmail,
                'email' => $sEmail,
                'firstname' => $sFirstname,
                'lastname' => $sLastname,
                'fk_country_id' => $iCountryID,
                'password' => Encryption::Encrypt($sPassword),
                'is_active' => 1
            );
            $sTablename = "members";
//            $bValid = DAL::Insert($sTablename, $aData);
//            if ($bValid) {
//                Email::sendNewAccountEmail($sFirstname, $sLastname, $sEmail);
//                //Silverpop
//            }

            $iMemberId = DAL::Insert($sTablename, $aData, true);
            if ($iMemberId) {

                // Add entry to member_alerts table
                $aData2 = array('member_id'=>$iMemberId);
                $sTablename = "members_alerts";
                DAL::Insert($sTablename, $aData2);


                // Send email through Silverpop
                Email::sendNewAccountEmail($sFirstname, $sLastname, $sEmail, $iMemberId);
            }
        }

        return $bValid;
    }

    public function addMemberSlim($sEmail, $sPassword) {
        
        // Get the language and currency used to sign up from site's config
        // and use that to set the country for this customer
        // (if no config value for either value, set as 1)
       $iLangId = (isset(Config::$config->iLanguageId) ? Config::$config->iLanguageId  : 1);
       $iSignupCurrencyID = (isset(Config::$config->iCurrencyId) ? Config::$config->iCurrencyId : 1);
       
       /*
        // We'll default to English here
        switch ($iLangId)
        {
            case 3:
                $iSignupCountryID = 83;
            break;

            default:
                $iSignupCountryID = 229;

        }
*/
        // Use the GeoIP library to get the customer's country information
        // and use that to record their current country and signup country
        $oGeoIP = new GeoIP();
        $aCountryInfo = $oGeoIP->geoIPMemberCountryInfo();
        
        $bValid = false;
        if ($this->checkUsernameAvailable($sEmail)) {
            $aData = array(
                'username' => $sEmail,
                'email' => $sEmail,
                'password' => Encryption::Encrypt($sPassword),
                'fk_country_id' => $aCountryInfo['country_id'], 
                'signup_currency_id' => $iSignupCurrencyID,
                'signup_country_id' => $aCountryInfo['country_id'],
                'signup_language_id' => $iLangId,
                'is_active' => 1
            );
            $sTablename = "members";
            $bValid = DAL::Insert($sTablename, $aData, true);
			$this->iMemberID = $bValid;
        }

        return $bValid;
    }

    public function checkUsernameAvailable($sUsername) {
        $bValid = false;
        if ($sUsername) {
            $sSQL = "SELECT COUNT(m.member_id)
                    FROM members m
                    WHERE m.username LIKE \"{$sUsername}\"";
            $bValid = !DAL::executeGetOne($sSQL);
        }
        return $bValid;
    }

    public function checkUsername($sUsername) {
        $sSQL = "SELECT
						m.member_id,
						m.firstname,
						m.lastname
				FROM members m
				WHERE m.username LIKE \"{$sUsername}\" ";

        return DAL::executeGetRow($sSQL);
    }

    public function getAllCountries($iLangID) {
        $sSQL = "SELECT cc.country_id,
						cl.title
				FROM countries_cmn cc
				INNER JOIN countries_lang cl ON cl.fk_country_id = cc.country_id
				INNER JOIN languages l ON l.language_id = cl.fk_language_id
                WHERE cc.isactive = 1
                AND
                    (
                        l.language_id = {$iLangID}
                        OR
                        l.default = 1
                    )
                AND l.is_active = 1
                ORDER BY l.default ASC, cc.rank ASC, cl.title ASC";

        return DAL::executeGetUnique($sSQL, "country_id");
    }

    public function getMemberCountryCode() {
        $sSQL = "SELECT cc.code
				FROM countries_cmn cc
				INNER JOIN members m ON m.fk_country_id = cc.country_id
				WHERE m.member_id = {$this->getMemberID()}";

        return DAL::executeGetOne($sSQL);
    }
    
    public function getMemberSignupCountryCode() {
        $sSQL = "SELECT cc.code
                 FROM countries_cmn cc
                 INNER JOIN members m ON m.signup_country_id = cc.country_id
                 WHERE m.member_id = {$this->getMemberID()}";

        return DAL::executeGetOne($sSQL);
    }    
    
    public function getUsername() {
        return $this->sUserName;
    }

    public function getFullName() {
        return $this->sFullName;
    }

    public function getMemberID() {
		return $this->iMemberID;
    }

    public function addError($sError) {
        $oError = new Error();
        $oError->addError($sError);
    }

    private function IPtoLong($sIPaddr) {
        if (empty($sIPaddr)) {
            return 0;
        } else {
            $aIP = explode(".", $sIPaddr);
            return ($aIP[3] + $aIP[2] * 256 + $aIP[1] * 256 * 256 + $aIP[0] * 256 * 256 * 256);
        }
    }

    public function getCountryFromIP($iIP) {
        $iLongIP = Member::IPtoLong($iIP);
        $sSQL = "SELECT c.country_id, c.blocked
				FROM ip_ranges ip
				INNER JOIN countries_cmn c ON ip.country_code = c.code
				WHERE ";
        if (isset($sCountryCode) && count($sCountryCode) == 2):
            $sSQL .= " ( c.code LIKE '{$sCountryCode}' ) ";
        else:
            $sSQL .= " {$iLongIP} <= ip.ip_range_end AND {$iLongIP} >= ip.ip_range_start ";
        endif;
        $aCountry = DAL::executeGetRow($sSQL);
        if ($aCountry) {
            if ($aCountry['blocked'] || $this->bBlockedCountry) {
                $this->Logout();
                $this->bBlockedCountry = 1;
            } else {
                $this->bBlockedCountry = 0;
            }
        }
        return $aCountry['country_id'];
    }

    public function getRemainingBalance($fCartTotal, $fMinimumTopup) {
        $fRemaining = 0;
        if ($fCartTotal > $this->fBalance) {
            $fRemaining = $fCartTotal - $this->fBalance;
        }
        // Return 'absolute' value here so that -0.00 is not displayed
        // if a customer withdraws all their balance
        return ($fRemaining < $fMinimumTopup) ? abs($fMinimumTopup) : abs($fRemaining);
    }

    public function isCountryBlocked() {
        return ($this->bBlockedCountry) ? true : false;
    }

    public function CountryBlockSet() {
        return !is_null($this->bBlockedCountry);
    }

// generateRandomString function now moved to encryption class as a static function

    public function resetPassword($sUsername) {

        $bValid = false;
        $aMember = $this->checkUsername($sUsername);

        if ($aMember) {

            // Need to pass HTTP host as part of the call to Silverpop
            // to ensure correct email link
            $sHost = $_SERVER['HTTP_HOST'];

            $token = Encryption::generateRandomString(rand(18,24));

            DAL::Insert('members_reset_token', array(
                    'member_id' => $aMember['member_id'],
                    'token_id' => $token,
                    'token_expires' => date('Y-m-d H-i', strtotime('+15 minutes'))
                )
            );

            /**
             * Removing this ---
             * if(DAL::Update("members", array("password" => Encryption::Encrypt($sPassword)), "member_id = " . $aMember['member_id'])) {
            */
            $bValid = true;

            // Return the balance value to 2 decimal places, rounded down
            // This will then be further formatted to 2display as 2 decimal places
            $fBalance = floor($this->getMemberBalance() * 100) / 100;

            $aPersonalisationData = array('firstname'       => $aMember['firstname'],
                                          'current balance' => number_format_locale($fBalance, 2, '.', ','),
                                          'member_id'       => $aMember['member_id'],
                                          'email_ref'       => 'password_request',
                                          'token'           => $token,
                                          'host'            => $sHost);

            Email::passwordChangeRequestEmail($sUsername, $aPersonalisationData);

        }
        return $bValid;
    }

    public function getMemberAffiliateID() {
        $sSQL = "SELECT m.fk_affiliate_id as affiliate_id
				FROM members m
				WHERE m.member_id = {$this->iMemberID}";

        return DAL::executeGetOne($sSQL);
    }

    public function setMemberAffiliateID($iAffiliateID) {
        return DAL::Update("members", array("fk_affiliate_id" => $iAffiliateID), "member_id = " . $this->iMemberID);
    }

    // Set monthly deposit limit
    public function setMonthlyDepositLimit($fMonthlyLimit)
    {

        $sTablename = "members";
        $aData = array(
            'monthly_deposit_limit' => $fMonthlyLimit
        );
        $sParams = "member_id = {$this->iMemberID}";

        return DAL::Update($sTablename, $aData, $sParams);
    }

    public function suspendMember($iSuspendStart, $iSuspendEnd)
    {
        $sTablename = "members";
        $aData = array(
            'is_suspended' => 1,
            'suspension_start' => $iSuspendStart,
            'suspension_end' => $iSuspendEnd
        );
        $sParams = "member_id = {$this->iMemberID}";

        return DAL::Update($sTablename, $aData, $sParams);
    }

    public function unsuspendMember()
    {
        $sTablename = "members";
        $aData = array(
            'is_suspended' => 0,
            'suspension_start' => NULL,
            'suspension_end' => NULL
        );
        $sParams = "member_id = {$this->iMemberID}";

        return DAL::Update($sTablename, $aData, $sParams);
    }

    /**
     * Gets the ticket fee to be applied on the game engine. The intial value is
     * determined by the game engine then passed in here to check if this member
     * has any kind of a fee discount
     *
     * Pass in the current fee, method will return the new fee, or unchanged fee
     * if there is no discount
     */
    public function getTicketFee($dFee){
        if($this->fee_discount_percent>0 && strtotime($this->fee_discount_ends)>time()){
            $dFee*=($this->fee_discount_percent/100);
        }
        return $dFee;
    }

    /**
     * Gets the ticket discount to be applied on the game engine. The intial value is
     * determined by the game engine then passed in here to check if this member
     * has any kind of additional discount.
     *
     * There is also a max discount that can be applied. If both match then effectively any
     * multiline discounts calculated by the GE will be ignored.
     *
     * Pass in the current discount, method will return the new discount, or unchanged fee
     * if there is no discount
     */
    public function getMemberDiscount($iDiscount){
        if($this->discount_percent>0 && strtotime($this->discount_ends)>time()){
            $iDiscount+=$this->discount_percent;
            if($this->discount_max>0 && $iDiscount > $this->discount_max) $iDiscount=$this->discount_max;
        }
        return $iDiscount;
    }

    // Return the stored member details array without updating it
    public function getMemberDetailsArray()
    {
        return $this->aMemberDetails;
    }

    // Check country/homepage/currency mapping table for any mappings
    private function getCountryHomepageCurrencyMapping()
    {
        
        $aMapData = GeoIP::getCountryHomepageCurrencyMapping($this->aMemberDetails['fk_country_id']);
        
        // Map for this country?
        if (is_array($aMapData))
        {
            // Map found, use data returned
            $this->aMemberDetails['member_currency'] = $aMapData['currency_id'];
            $this->aMemberDetails['member_iso2code'] = $aMapData['iso_code'];
            $this->aMemberDetails['member_language'] = $aMapData['language_id'];
        }
        else
        {
            // No map found, use data in session
            $this->aMemberDetails['member_currency'] = $_SESSION['currency_id'];
            $this->aMemberDetails['member_iso2code'] = $_SESSION['iso2country'];
            $this->aMemberDetails['member_language'] = $aMapData['language_id'];
        }
        
    }
    
}
