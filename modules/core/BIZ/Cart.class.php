<?php
/**
 * Shopping cart class
 * @package LoveLotto
 * @subpackage FrontEndModels
 */
class Cart {
    static $cart;

    public $oPaymentGateway;
    protected $oCartItems = array();
    protected $fTotal = 0;
    protected $aReceiptDetails = array();

    public function __construct() {
        self::$cart=$this;

        $this->oCartItems = array();
        $this->oPaymentGateway = new PaymentGateways();
    }

    public function resetPaymentGateway() {
        $this->oPaymentGateway = new PaymentGateways();
    }

    public function getCartTotal() {
        $this->recalculate();
        return abs($this->fTotal);
    }

    public function recalculate(){
        $this->fTotal=0;
        foreach ($this->oCartItems as $oCartItem) {
            $this->fTotal+=round($oCartItem->getCost(),2);
        }
        if($this->fTotal<0) $this->fTotal=0;
        return $this->fTotal;
    }

    public function getCartDetails() {
        $this->validateTickets();
        $aCartItems = array();
        if($this->fTotal<0) $this->fTotal=0;
        $aCartItems['total'] = number_format($this->fTotal,2);
        $aCartItems['totalFormatted'] = number_format_locale($this->fTotal, 2);
        $aCartItems['items'] = count($this->oCartItems);
        foreach ($this->oCartItems as $oCartItem) {
            $aCartItems[] = $oCartItem->convertToArray();
        }
        return $aCartItems;
    }

    public function assignPaymentGateway($iPaymentGatewayID) {
        $sClassName = $this->oPaymentGateway->getClassName($iPaymentGatewayID);
        if($sClassName=='MaxLottoGateway') $sClassName='LoveLottoGateway';
        $this->oPaymentGateway = new $sClassName();
    }

    public function addToCart($oCartItem) {
        $this->oCartItems[] = $oCartItem;
        $this->fTotal += $oCartItem->getCost();
        $this->sortCart();
        $this->recalculate();
    }

    /**
     * removeFromCart: Remove an item from the cart (was using lottery ID, draw date and numbers
     * but now we only need to match the key)
     *
     * @param key int - The index to remove
     */
    public function removeFromCart($key) {

        # Fetch the item
        $item = $this->oCartItems[$key];

        # Update the total
        # @todo, Perform a re-calculate request. Instead of keeping tabs with + / -
        $this->fTotal -= round($item->getCost(),2);

        if($this->fTotal<0) $this->fTotal=0;

        # Remove the item
        unset($this->oCartItems[$key]);

        $this->sortCart();
        $this->recalculate();
    }

    protected function sortCart() {
        $oCartItems = $this->oCartItems;
        foreach ($oCartItems as $key => $oCartItem) {
            unset($oCartItems[$key]);
            $oCartItems[$oCartItem->getLotteryID() . "-" . $oCartItem->getDrawDate() . "-" . $key] = $oCartItem;
        }
        ksort($oCartItems, SORT_STRING);
        $this->oCartItems = array_values($oCartItems);
    }

    public function getCartShortDetails() {
        $aCartShortDetails = array();
        $iCartItems = count($this->oCartItems);
        if ($iCartItems) {
            $aCartShortDetails['items'] = $iCartItems;
            $aCartShortDetails['total'] = $this->fTotal;
        }
        return $aCartShortDetails;
    }

    public function validateTickets() {
        $bInvalid = false;

        foreach ($this->oCartItems as $key => $oCartItem) {

            if ($oCartItem->getCutoffTime() < date("Y-m-d H:i:s")) {

                //$bInvalid = true;
				  # Remove when invalid (for now)
				  # $this->removeFromCart($oCartItem->getLotteryID(), $oCartItem->getDrawDate(), $oCartItem->getNumbers());
				  # Should turn into
				  # $this->removeFromCart($key)
            }
        }
        if ($bInvalid) {
            //$oError = new Error();
            //$oError->addNotice("Ouch. Some of the tickets in your shopping cart have become invalid and have been removed. This was probably a result of the cut off time expiring. We're sorry but the lottery terminals are closing and we need a little time to go and buy them! Please, come back earlier next time.");
        }
    }

    public function commitPurchase($oMember, $oCurrency) {
        $bValid = false;
        $this->validateTickets();
        $this->oMember=$oMember;


        if (count($this->oCartItems) > 0) {

            // <Nathan 15/7/14>
            // Now creating two formatted date strings from one timestamps
            // instead of multiple timestamps - should work better?
            $iRawDate = time();
            $dtOrderDate = date("Y-m-d H:i:s", $iRawDate);
            $dtOrderDateEmail = date("d/m/Y H:i:s", $iRawDate);

            # Member affiliate = lifetime link
            $iMemberAffiliateID = $oMember->getMemberAffiliateID();

            # Reads all the cookie data set
            $iAffiliateID = Affiliates::getAffiliateID();
            $sPromoCode = Affiliates::getPromoCode();
            $iAdvertID = Affiliates::getAdvertID();

            # Clears out the cookie, just for the one order
            Affiliates::unsetAffiliate();

            # No affiliate, so its from ML
            if (empty($iAffiliateID) && empty($iMemberAffiliateID)) {
                $iAffiliateID = 6;
                $sPromoCode = "Organic";
            } elseif (!empty($iMemberAffiliateID) && $iMemberAffiliateID != $iAffiliateID) {
                $iAffiliateID = $iMemberAffiliateID;
                $sPromoCode = "Life";
            }

            # Link the user to the affiliate
            if (empty($iMemberAffiliateID) && Affiliates::validateAffiliateID($iAffiliateID)) {
                $oMember->setMemberAffiliateID($iAffiliateID);
            }

            $bValid = true;

            $this->assignPaymentGateway(PaymentGateways::LOVELOTTO);
            $this->oPaymentGateway->assignAmount($this->fTotal, true);

            $iTransactionID = $this->oPaymentGateway->addTransaction($oMember->getMemberID(), $oCurrency->getSelectedCurrencyID());
            $sTableName = "bookings";
            $aData = array(
                "fk_affiliate_id" => $iAffiliateID,
                "promo" => $sPromoCode,
                "fk_member_id" => $oMember->getMemberID(),
                "fk_status_id" => 1,
                "fk_transaction_id" => $iTransactionID,
                "bookingreference" => $this->oPaymentGateway->getTransactionReference(),
                "fk_currency_id" => $oCurrency->getSelectedCurrencyID(),
                "total" => $this->fTotal,
                "booking_date" => $dtOrderDate,
                "fk_source_advert_id" => $iAdvertID
            );

            $iBookingID = DAL::Insert($sTableName, $aData, true);

            /**
             * Now go through each item in the basket and create an order item for each one
             * these order items will then be passed into the relevant game engine
             * for processing
             */
            $aReceiptItems = array();
            foreach ($this->oCartItems as $oCartItem) {
                $oCartItem->oOrder=$this;
                $oCartItem->setCost=1;
                $oGameEngine=GameEngine::getById($oCartItem->gameID);

                //Commit to the order items table
                $aData = array(
                    "fk_game_engine_id" => $oCartItem->gameID,
                    "config" => json_encode($oCartItem->data),
                    "cost" => $oCartItem->getCost(),
                    "fk_booking_id" => $iBookingID,
                    "created_at" => $dtOrderDate,
                    "processed_at" => $dtOrderDate,
                    "is_subscription" => ($oCartItem->data['isSubscription'])?1:0,
                    "last_bill_date" => date('Y-m-d')
                );
                $oCartItem->iBookingOrderID = DAL::Insert('booking_order_items', $aData, true);


                $oGameEngine->processOrderItem($oCartItem, $iBookingID);

                //Now that we've processed the tickets we will have updated game info
                //Noteably start and end date of play so we need to update the order item
                $aData = array(
                    "fk_game_engine_id" => $oCartItem->gameID,
                    "config" => json_encode($oCartItem->data),
                    "cost" => $oCartItem->getCost(),
                    "fk_booking_id" => $iBookingID,
                    "created_at" => $dtOrderDate,
                    "processed_at" => $dtOrderDate,
                    "is_subscription" => ($oCartItem->data['isSubscription'])?1:0,
                    "last_bill_date" => date('Y-m-d'),
                    "start_date" => $oCartItem->data['start_date'],
                    "end_date" => $oCartItem->data['end_date']
                );
                DAL::Update('booking_order_items', $aData, 'id='.$oCartItem->iBookingOrderID);

                $aReceiptItems[]=$oCartItem;

            }
            $this->oPaymentGateway->ProcessPayment();
            $oMember->updateBalance();
            $oMember->updateTicketsCount();
            $aMember = $oMember->getMemberDetails();
            $this->aReceiptDetails = array
                (
                "bookingreference" => $this->oPaymentGateway->getTransactionReference(),
				'bookingID' => $iBookingID,
                "total" => $this->fTotal,
                "gateway" => $this->oPaymentGateway->getGatewayName(),
                "items" => $aReceiptItems
            );
            unset($aReceiptItems);
            //$this->sendReceipt($aMember['firstname'], $aMember['lastname'], $aMember['email']);
            //$this->sendReceipt($aMember['email'], $aMember['balance'], $aMember['firstname'], $dtOrderDateEmail, $aMember['member_id']);


            $this->sendReceipt($aMember['email'], $aMember['balance'], $aMember['firstname'], $dtOrderDateEmail, $aMember['member_id'], $oGameEngine->name);
            $this->emptyCart();
        }
        return $bValid;
    }

    // Changed from protected to public visibility, so that cart can be cleared 
    // when redirecting to different subdomain
    public function emptyCart() {
        $this->oCartItems = array();
        $this->fTotal = 0;
    }

    //protected function sendReceipt($sFirstName, $sLastName, $sEmail) {
   //protected function sendReceipt($sEmail, $fBalance, $sFirstName, $dtOrderDate, $iMemberId) {
   protected function sendReceipt($sEmail, $fBalance, $sFirstName, $dtOrderDate, $iMemberId, $sProductName) {
        //return Email::sendTicketReceipt($sFirstName, $sLastName, $sEmail, $this->oPaymentGateway->getTransactionReference(), $this->oCartItems, $this->fTotal);

        // Return the balance value to 2 decimal places, rounded down
        // This will then be further formatted to 2display as 2 decimal places
        $fBalance = floor($fBalance * 100) / 100;


/*
        // OLD PERSONALISATION ARRAY BEFORE SEP 12
        $aPersonalisationData = array('current balance'     => number_format_locale($fBalance, 2, '.', ','),
                                      'username'            => $sEmail,
                                      'Transaction_Num'     => $this->oPaymentGateway->getTransactionReference(),
                                      'Payment_Amount'      => number_format_locale($this->fTotal, 2, '.', ','),
                                      'Payment_Date'        => $dtOrderDate,
                                      'firstname'           => $sFirstName,
                                      'member_id'           => $iMemberId,
                                      'email_ref'           => 'order_confirm');


        // New personalisation array FROM SEP 12
        $aPersonalisationData = array('current balance'     => number_format_locale($fBalance, 2, '.', ','),
                                      'username'            => $sEmail,
                                      'payment_method'      => $this->oPaymentGateway->getGatewayName(),
                                      'ordernumber'         => $this->oPaymentGateway->getTransactionReference(),
                                      'payment_amount'      => number_format_locale($this->fTotal, 2, '.', ','),
                                      'payment_date'        => $dtOrderDate,
                                      'firstname'           => $sFirstName,
                                      'member_id'           => $iMemberId,
                                      'email_ref'           => 'order_confirm');
*/

        // Revised array from September 26th
        $aPersonalisationData = array('current balance'     => number_format_locale($fBalance, 2, '.', ','),
                                      'firstname'           => $sFirstName,
                                      'ordernumber'         => $this->oPaymentGateway->getGatewayReference(),
                                      'product_name'        => $sProductName,
                                      'order_date'          => $dtOrderDate,
                                      'member_id'           => $iMemberId,
                                      'email_ref'           => 'order_confirm');

        // email sent out via Silverpop
        Email::sendOrderConfirmation($sEmail, $aPersonalisationData);

    }

    protected function getBookingReference() {
        return TRANSACTIONREFERENCE . time();
    }

    public function getReceiptDetails() {
        return $this->aReceiptDetails;
    }

}
