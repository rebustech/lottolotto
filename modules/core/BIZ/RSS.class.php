<?php

/**
 * Simple RSS feed parser
 * Probably could do with some caching adding!
 * @package LoveLotto
 * @subpackage FrontEndModels
 */
class RSS {

    protected $sFileName;
    protected $aItems;
    protected $sTitle;
    protected $sLink;
    protected $sDescription;
    protected $sLanguage;
    protected $sImage;
    protected $sImageH;
    protected $sImageW;

    public function __construct() {

    }

    public function getFeed() {
        return $this->getDetails() . $this->getItems();
    }

    private function getDetails() {

        $details = '<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0">
	<channel>
		<title>' . $this->sTitle . '</title>
		<link>' . $this->sLink . '</link>
		<description>' . $this->sDescription . '</description>
		<language>' . $this->sLanguage . '</language>
		<lastBuildDate>' . date("D, j M Y H:i:s O", strtotime(now)) . '</lastBuildDate>';
        if (!empty($this->sImage)) {
            $details = '
		<image>
			<title>' . $this->sTitle . '</title>
			<url>' . $this->sURL . '</url>
			<link>' . $this->sLink . '</link>
			<width>' . $this->sTitleW . '</width>
			<height>' . $this->sTitleH . '</height>
		</image>';
        }
        return $details;
    }

    private function getItems() {
        foreach ($this->aItems as $aItem) {
            $items .= '
			<item>
				<guid>' . $aItem["guid"] . '</guid>
				<title>' . $aItem["title"] . '</title>
				<link>' . $aItem["link"] . '</link>
				<description><![CDATA[' . $aItem["description"] . ']]></description>
				<pubDate>' . $aItem["pubdate"] . '</pubDate>
			</item>';
        }
        $items .= '
	</channel>
</rss>';
        return $items;
    }

    public function setTitle($sTitle) {
        $this->sTitle = $sTitle;
    }

    public function setLink($sLink) {
        $this->sLink = $sLink;
    }

    public function setDescription($sDescription) {
        $this->sDescription = $sDescription;
    }

    public function setLanguage($sLanguage) {
        $this->sLanguage = $sLanguage;
    }

    public function setImage($sImage) {
        $this->sImage = $sImage;
    }

    public function setImageH($sImageH) {
        $this->sImageH = $sImageH;
    }

    public function setImageW($sImageW) {
        $this->sImageW = $sImageW;
    }

    public function setItems($aItems) {
        $this->aItems = $aItems;
    }

}