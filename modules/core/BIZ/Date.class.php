<?php
/**
 * Generic date class
 * @package LoveLotto
 * @subpackage Data
 */
class Date{

	static public function dateFromMySQL($sMySQLDate) {

		$displayDate = date("D d M Y", strtotime($sMySQLDate));
		return $displayDate;
	}

	static public function dateToMySQL($sDate) {

		//CONVERTS TO UK FORMAT
		$reformatted = preg_replace("/^\s*([0-9]{1,2})[\/\. -]+([0-9]{1,2})[\/\. -]+([0-9]{1,4})/", "\\2/\\1/\\3", $sDate);
		if ($reformatted) {
			$displayDate = date("Y-m-d", strtotime($reformatted));
			return $displayDate;
		}
	}
	static public function dateTimeToMySQL($sDate) {

		//CONVERTS TO UK FORMAT
		$reformatted = preg_replace("/^\s*([0-9]{1,2})[\/\. -]+([0-9]{1,2})[\/\. -]+([0-9]{1,4})/", "\\2/\\1/\\3", $sDate);
		if ($reformatted) {
			$displayDate = date("Y-m-d H:i:s", strtotime($reformatted));
			return $displayDate;
		}
	}
	static public function dayFromMySQL($sMySQLDate) {

		$displayDate = date("D", strtotime($sMySQLDate));
		return $displayDate;
	}

	public function getToDate($sFromDate, $iNights){
		$aDateArray = explode("/",$sFromDate);
		$sYear   = (int)$aDateArray[2];
		$sMonth = (int)$aDateArray[1];
		$sDay  = (int)$aDateArray[0];
		if($sYear && $sMonth && $sDay){
			if(checkdate($sMonth,$sDay,$sYear)){
				$sFromDate = date('Y-m-d',mktime(0,0,0,$sMonth,$sDay,$sYear));
			}
		}
		return date("d/m/Y", strtotime($sFromDate . "+" . $iNights . " days"));
	}

	static public function addDate($sOriginalDate, $iDays){

		if (!$sOriginalDate) return false;
		return date("Y-m-d", strtotime($sOriginalDate . "+" . $iDays . " days"));
	}

	static public function addTime($sOriginalDate, $iMinutes){

		if (!$sOriginalDate) return false;
		return date("Y-m-d H:i:s", strtotime($sOriginalDate . "+" . $iMinutes . " minutes"));
	}

	static public function dateDifference($sFromDate, $sToDate)
	{
		$diff = strtotime($sToDate) - strtotime($sFromDate); //Find the number of seconds
		$day_difference = ceil($diff / (60*60*24)) ;
		return $day_difference;
	}

    static public function getAge($iDifference) {

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");

        if ($iDifference > 0) {
			for($j = 0; $iDifference >= $lengths[$j]; $j++) $iDifference /= $lengths[$j];

			$iDifference = round($iDifference);
			if($iDifference != 1) $periods[$j] .= "s";

			$sReturn = $iDifference . " " . $periods[$j] . " " . $ending . " ago";
			return $sReturn;
		}
    }
}//eoc
?>