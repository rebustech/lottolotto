<?php
/**
 * Currency DAO class and model
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class Currency {

    protected $aSelectedCurrency = NULL;
    protected $aBaseCurrency = NULL;

    public function __construct($iCurrencyID = NULL) {
        $this->aBaseCurrency = $this->getBaseCurrency();
        if ($iCurrencyID) {
            $this->switchCurrency($iCurrencyID);
        } else {
            $this->aSelectedCurrency = $this->aBaseCurrency;
        }
    }

    public function switchCurrency($iCurrencyID) {
        if ($this->aSelectedCurrency['id'] != $iCurrencyID && $iCurrencyID) {
            $this->aSelectedCurrency = $this->getCurrencyByID($iCurrencyID);
        }
    }

    public function convertToBase($fAmount, $iCurrencyID) {
        return ($fAmount / self::getCurrencyXRate($iCurrencyID));
    }

    private function getBaseCurrency() {

        # Get the draws from the cache
        $baseCurrency = LLCache::get('Currency-getBaseCurrency');

        # Not in cache, query it
        if(empty($baseCurrency)) {

            # Query
            $sSQL = "SELECT c.currency_id as id,
    				c.iconfile,
    				c.shortname,
    				c.fullname,
    				c.xrate,
    				c.symbol
    				FROM currencies c
    				WHERE c.base_currency = 1
    				LIMIT 1";

            # Get the result
            $baseCurrency =  DAL::executeGetRow($sSQL);

            # Store it
            LLCache::add('Currency-getBaseCurrency', $baseCurrency, false, 900);

        }

        return $baseCurrency;
    }

    private function getCurrencyXRate($id) {

        # Get the draws from the cache
        $currencyXRate = LLCache::get('Currency-getCurrencyXRate-'.$id);

        # Not in cache, query it
        if(empty($currencyXRate)) {

            $sSQL = "SELECT c.xrate
				FROM currencies c
				WHERE c.currency_id = {$id}
				LIMIT 1";

            $currencyXRate = DAL::executeGetOne($sSQL);

            # Store it
            LLCache::add('Currency-getCurrencyXRate-'.$id, $currencyXRate, false, 900);

        }

        return $currencyXRate;

    }

    private function getCurrencyByID($id) {
        
        # Get the draws from the cache
        $currencyData = LLCache::get('Currency-getCurrencyByID-'.$id);

        # Not in cache, query it
        if(empty($currencyData)) {

            $sSQL = "SELECT c.currency_id as id,
				c.iconfile,
				c.shortname,
				c.fullname,
				c.xrate,
				c.symbol
				FROM currencies c
				WHERE c.is_active = 1
					AND
					(
						c.currency_id = {$id}
						OR
						c.base_currency = 1
					)
				ORDER BY c.base_currency ASC
				LIMIT 1";
                                                
            $currencyData = DAL::executeGetRow($sSQL);

            # Store it
            LLCache::add('Currency-getCurrencyByID-'.$id, $currencyData, false, 900);

        }
        
        return $currencyData;

    }

    public function getSelectedCurrency() {
        return $this->aSelectedCurrency;
    }

    public function getSelectedCurrencyID() {
        return $this->aSelectedCurrency['id'];
    }

    public function getSelectedCurrencySymbol() {
        return $this->aSelectedCurrency['symbol'];
    }

    public function getSelectedCurrencyXRate() {
        return $this->aSelectedCurrency['xrate'];
    }

    public function getSelectedCurrencyShortname() {
        return $this->aSelectedCurrency['shortname'];
    }
    
    public function getCurrencies() {

        # Get the draws from the cache
        $currencyData = LLCache::get('Currency-getCurrencies');

        # Not in cache, query it
        if(empty($currencyData)) {

            $sSQL = "SELECT c.currency_id AS id,
						c.shortname AS shortname,
						c.fullname AS fullname,
						c.iconfile AS iconfile,
						c.symbol AS symbol
				FROM currencies c
				WHERE c.is_active = 1
				ORDER by c.rank ASC";

            $currencyData = DAL::executeQuery($sSQL);

            # Store it
            LLCache::add('Currency-getCurrencies', $currencyData, false, 900);

        }

        return $currencyData;

    }

    public static function convertFromBase($fAmount, $fExchangeRate) {
        return number_format_locale(round($fAmount * $fExchangeRate, 2), 2);
    }

}
