<?php

class Address {

    private $client;

    public function __construct() {
        $username = "lewis@fourfiveeight.com";
        $password = "Bb7~AmeNdMMEFUbD";


        $wsse_header = new WsseAuthHeader($username, $password);
        $options = array(
            'soap_version'    => SOAP_1_1,
            'exceptions'      => true,
            'trace'           => 1,
            'wdsl_local_copy' => true
        );
        $wsdl = 'https://pilot.id3global.com/ID3gWS/ID3global.svc?wsdl';

        $this->client = new SoapClient($wsdl, $options);
        $this->client->__setSoapHeaders(array($wsse_header));
    }


    public function addressLookup($country, $postcode, $street = '', $buildingNumber = '') {

        $country = urldecode($country);

        $param = array(

            'InputData' => array(
                'Country' => $country,
                'Street' => $street,
                'SubStreet' => '',
                'City' => '',
                'SubCity' => '',
                'StateDistrict' => '',
                'POBox' => '',
                'Region' => '',
                'Principality' => '',
                'ZipPostcode' => $postcode,
                'Building' => $buildingNumber,
                'SubBuilding' => '',
                'Premise' => ''
            )
        );

        $param = array_merge(array('ProfileIDVersion' => array('ID' => '764efbf1-4e3b-4e75-966e-74c00ce0fcd9', 'Version' => '1.3')), $param);


        $param = array($param);
        try {
            $result = $this->client->__soapCall('AddressLookup', $param);
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

} 