<?php

/**
 * Navigation helper and DAO class for frontend
 * @package LoveLotto
 * @subpackage FrontEndModels
 */
class Navigation {

    protected $iLangID = NULL;

    public function __construct($iLangID = NULL) {
        $this->iLangID = $iLangID;
    }

    public static function getSectionsAndID() {
        $sSQL = "SELECT sc.section_id as id,
						sc.comment
                FROM sections_cmn sc
                ORDER BY sc.rank ASC";

        return DAL::executeQuery($sSQL);
    }

    public function getSections() {
        $sSQL = "SELECT sc.section_id,
                        sc.folder,
                        sl.title
                FROM sections_cmn sc
                INNER JOIN sections_lang sl ON sl.fk_section_id = sc.section_id
                INNER JOIN languages l ON l.language_id = sl.fk_language_id
                WHERE sc.is_active = 1
                AND
                    (
                        l.language_id = {$this->iLangID}
                        OR
                        l.default = 1
                    )
                AND sl.is_active = 1
                AND l.is_active = 1
                ORDER BY sc.rank ASC, l.default ASC";

        return DAL::executeGetUnique($sSQL, 'section_id');
    }

    public function getNavigationMenu($iSectionID = NULL, $bShowInMenu = false, $iLevel = 0, $bGetSitemap = false) {
        $sWhere = "";
        if ($iSectionID) {
            $sWhere = "AND fk_section_id = {$iSectionID} AND parent_id <> 0 ";
        }
        $sSQL = "SELECT
                    pc.page_id,
                    pc.parent_id,
                    pc.comment,
                    pc.url,
                    pc.fk_section_id as section_id,
                    pc.right_node,
                    sc.folder,
					pc.showinmenu,
					pc.showinsitemap
                FROM pages_cmn pc
                INNER JOIN sections_cmn sc ON pc.fk_section_id = sc.section_id AND sc.is_active = 1
                WHERE pc.is_active = 1
                {$sWhere}
                ORDER BY pc.left_node ASC";
        $aPages = DAL::executeGetUnique($sSQL, 'page_id');
        $aRightNodes = array();
        foreach ($aPages as $key => $Page) {
            if (count($aRightNodes) > 0) {
                while (count($aRightNodes) && $aRightNodes[count($aRightNodes) - 1] < $Page['right_node']) {
                    array_pop($aRightNodes);
                }
            }
            if ((!$bGetSitemap && count($aRightNodes) != $iLevel ) || ( $bShowInMenu && $Page['showinmenu'] != 1 ) || ( $bGetSitemap == true && $Page['showinsitemap'] == 0 )) { // If Level is 1 show it else delete it
                unset($aPages[$key]);
            } else {
                $aPages[$key]['level'] = count($aRightNodes);
            }
            $aRightNodes[] = $Page['right_node'];
        }
        return array_values($aPages);
    }

    public function getBreadcrumbs($iLeftNode, $iRightNode) {
        $sSQL = "SELECT pc.page_id,
                        pc.url,
                        pl.menu_title,
						sc.folder
                FROM pages_cmn pc
				INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id
                INNER JOIN pages_lang pl ON pl.fk_page_id = pc.page_id
                INNER JOIN languages l ON l.language_id = pl.fk_language_id
                WHERE pc.is_active = 1
                AND
                    (
                        l.language_id = {$this->iLangID}
                        OR
                        l.default = 1
                    )
                AND pl.is_active = 1
                AND l.is_active = 1
                AND pc.left_node < {$iLeftNode} AND pc.right_node > {$iRightNode}
                ORDER BY pc.left_node ASC, l.default ASC";

        $aBreadCrumbs = DAL::executeGetUnique($sSQL, 'page_id');
        return $aBreadCrumbs;
    }

    public function getPageURL($iPageID) {

        # Some random time, so that we aren't screwed all at once
        $cacheTime = 900 + rand(-60, 60);
        $cacheKey = 'Navigation-getPageURL_pageId-'.$iPageID;

        # Get the draws from the cache
        $pageData = LLCache::get($cacheKey);

        # Not in cache, query it
        if(empty($pageData)) {

            $sSQL = "
    			SELECT
    				CONCAT(sc.folder, pc.url) as url
    			FROM pages_cmn pc
    			INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id
    			WHERE pc.is_active = 1
    				AND pc.page_id = {$iPageID}
    			";

            $pageData = DAL::executeGetOne($sSQL);

            # Store it
            LLCache::add($cacheKey, $pageData, true, $cacheTime);

        }

        return $pageData;

    }

    public function getPagesByIDs($aIDs) {

        $aPages = array();
        $sPageIDs = implode(", ", $aIDs);
        $cacheKey = 'Navigation-getById-'.crc32($sPageIDs);

        # Get the data
        $aPages = LLCache::get($cacheKey);

        # Return form cache
        if(!empty($aPages)) {
            return $aPages;
        }

        if ($sPageIDs) {
            $sSQL = "SELECT CONCAT(sc.folder, pc.url) as url,
							pl.menu_title as title,
							pc.page_id
					FROM pages_cmn pc
					INNER JOIN pages_lang pl ON pl.fk_page_id = pc.page_id
					INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id
					INNER JOIN languages l ON l.language_id = pl.fk_language_id
					WHERE pc.is_active = 1
						AND
							(
								l.language_id = {$this->iLangID}
								OR
								l.default = 1
							)
						AND pl.is_active = 1
						AND l.is_active = 1
						AND pc.page_id IN ( {$sPageIDs} )
					ORDER BY Field(pc.page_id, {$sPageIDs} ) ASC ";
            $aPages = DAL::executeGetUnique($sSQL, 'page_id');
        }

        LLCache::addObject($cacheKey, $aPages, 900);

        return $aPages;
    }

    public function getSectionTitle($iSectionID) {
        $sSQL = "SELECT sl.title
                FROM sections_lang sl
                INNER JOIN languages l ON l.language_id = sl.fk_language_id
                WHERE sl.fk_section_id = {$iSectionID}
                AND
                    (
                        l.language_id = {$this->iLangID}
                        OR
                        l.default = 1
                    )
                AND sl.is_active = 1
                AND l.is_active = 1
                ORDER BY l.default ASC
				LIMIT 1";

        return DAL::executeGetOne($sSQL);
    }

}
