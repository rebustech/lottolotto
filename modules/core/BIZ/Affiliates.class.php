<?php
/**
 * Affiliate DAO and helper class
 * @package LoveLotto
 * @subpackage Models and DAO
 */
class Affiliates
{
	static protected $fCommission = 0.25;
	static protected $iTranslationID = 9;

	public function checkUsernameExists($sUsername)
	{
		$sSQL = "SELECT au.user_id as user_id
				FROM admin_users au
				WHERE au.username LIKE '{$sUsername}'";
		return DAL::executeGetOne($sSQL);
	}

	public function addAffiliate($aAffiliateDetails, &$oError)
	{
		$bValid = true;
		$aUserDetails['username'] = trim($aAffiliateDetails['username']);
		$aUserDetails['firstname'] = trim($aAffiliateDetails['firstname']);
		$aUserDetails['lastname'] = trim($aAffiliateDetails['lastname']);
		$aUserDetails['email'] = trim($aAffiliateDetails['email']);
		$aUserDetails['tel1'] = trim($aAffiliateDetails['tel']);
		$aAffiliateInfo['company'] = trim($aAffiliateDetails['company']);
		$aAffiliateInfo['web'] = trim($aAffiliateDetails['web']);
		$aAffiliateInfo['address1'] = trim($aAffiliateDetails['address1']);
		$aAffiliateInfo['address2'] = trim($aAffiliateDetails['address2']);
		$aAffiliateInfo['city'] = trim($aAffiliateDetails['city']);
		$aAffiliateInfo['region'] = trim($aAffiliateDetails['region']);
		$aAffiliateInfo['country'] = trim($aAffiliateDetails['country']);
		$aAffiliateInfo['zipcode'] = trim($aAffiliateDetails['zipcode']);
		$aAffiliateInfo['vatno'] = trim($aAffiliateDetails['vatno']);
		$aAffiliateInfo['commission'] = Affiliates::getCommissionRate();

		if ( empty($aUserDetails['email']) || !filter_var($aUserDetails['email'], FILTER_VALIDATE_EMAIL))
		{
			$bValid = false;
			$oError->addError("Email address is invalid.");
		}
		if ( !$aUserDetails['firstname'] )
		{
			$bValid = false;
			$oError->addError("Name is Required.");
		}
		if ( !$aUserDetails['lastname'] )
		{
			$bValid = false;
			$oError->addError("Surname is Required.");
		}
		if ( !$aAffiliateInfo['web'] )
		{
			$bValid = false;
			$oError->addError("Website is Required.");
		}
		if ( !$aUserDetails['tel1'] )
		{
			$bValid = false;
			$oError->addError("Contact Number is Required.");
		}
		if ( !$aAffiliateInfo['address1'] )
		{
			$bValid = false;
			$oError->addError("Address is Required.");
		}
		if ( !$aAffiliateInfo['city'] )
		{
			$bValid = false;
			$oError->addError("City is Required.");
		}
		if ( !$aAffiliateInfo['zipcode'] )
		{
			$bValid = false;
			$oError->addError("ZIP Code is Required.");
		}
		if ( !$aAffiliateInfo['country'] )
		{
			$bValid = false;
			$oError->addError("Country is Required.");
		}
		if ( empty($aUserDetails['username']) )
		{
			$bValid = false;
			$oError->addError("Username is required.");
		}
		elseif ( Affiliates::checkUsernameExists($aUserDetails['username']) )
		{
			$bValid = false;
			$oError->addError("Username is already in use.");
		}
		if ( strlen($aAffiliateDetails['password']) < 6 )
		{
			$bValid = false;
			$oError->addError("Password too short. Needs to be 6 characters or more.");
		}
		if ( $aUserDetails['password'] != $aUserDetails['password2'] )
		{
			$bValid = false;
			$oError->addError("Password doesn't match.");
		}

		$sPin = rand(1000,9999);
		$aUserDetails['password'] = Encryption::Encrypt($aAffiliateDetails['password']);
		$aUserDetails['pin'] = Encryption::Encrypt($sPin);
		if ( $bValid )
		{
			$tablename = "admin_users";
			$aUserDetails['fk_admintype_id'] = 2;
			$aUserDetails['isactive'] = 1;
			$bValid = DAL::Insert($tablename,$aUserDetails,true);
			if ( $bValid )
			{
				$aAffiliateInfo['fk_user_id'] = $bValid;
				$tablename = "affiliates";
				$bValid = DAL::Insert($tablename,$aAffiliateInfo);
				if ( $bValid )
				{
					Email::sendAffiliateRegistrationEmail($aUserDetails['firstname'], $aUserDetails['lastname'], $aUserDetails['email'], $aUserDetails['username'], $aAffiliateDetails['password'], $sPin);
				}
			}
		}
		return $bValid;
	}
	public function getCommissionRate()
	{
		return self::$fCommission;
	}

	public function editUserDetails($aDetails, $iUserID)
	{
		$bValid = false;
		if ( is_array($aDetails) )
		{
			$tablename = "affiliates";
			$params = "fk_user_id = '" . $iUserID . "'";
			$bValid = DAL::Update($tablename,$aDetails,$params);
		}
		return $bValid;
	}


	public function setAffiliate($iAffiliateID = 0, $advertId = 0, $promoCode = '', $isToken = false){

		if($isToken) {
			$sToken = $iAffiliateID;
			$aTokenElements = self::decryptToken($sToken);
			$iAffiliateID = $aTokenElements['affiliateId'];
		} else {
			$sToken = Encryption::encodeText('affiliateId='.$iAffiliateID.'|advertId='.$advertId.'|promoCode='.$promoCode);
		}

		if ( self::validateAffiliateID($iAffiliateID) )
		{
			Cookies::setCookie("AdToken", $sToken , 24*7);
			return true;
		}
		else
		{
			return false;
		}
	}

	public function decryptToken($sToken){

		//$sToken = Cookies::getCookie("AdToken");
		$sDecToken = Encryption::decodeText($sToken);
		$aTokenElements = explode('|', $sDecToken);
		$a = array();

		if ( is_array($aTokenElements) ){

			foreach ($aTokenElements as $value) {
				$key = explode('=', $value);
				$a[$key[0]] = $key[1];
			}
		}

		return $a;
	}

	public function getAffiliateID()
	{
		$sToken = Cookies::getCookie("AdToken");
		$aTokenElements = self::decryptToken($sToken);
		return $aTokenElements['affiliateId'];
	}

	public function getAdvertID()
	{
		$sToken = Cookies::getCookie("AdToken");
		$aTokenElements = self::decryptToken($sToken);
		return $aTokenElements['advertId'];
	}

	public function getPromoCode()
	{
		$sToken = Cookies::getCookie("AdToken");
		$aTokenElements = self::decryptToken($sToken);
		return $aTokenElements['promoCode'];
	}

	public function unsetAffiliate()
	{
		Cookies::deleteCookie("AdToken");

	}

	public function validateAffiliateID($iAffiliateID)
	{
		$sSQL = "SELECT au.username
				FROM admin_users au
				WHERE au.user_id = {$iAffiliateID}
					AND au.isactive = 1";
		return DAL::executeGetOne($sSQL);
	}

	public function getContract($iAffiliateID)
	{
		$sSQL = "SELECT af.contract
				FROM admin_users au
				INNER JOIN affiliates af ON af.fk_user_id = au.user_id
				WHERE au.user_id = {$iAffiliateID} ";

		return DAL::executeGetOne($sSQL);
	}
	public function setContract($iAffiliateID, $sContract)
	{
		$aDetails['contract'] = $sContract;
		$tablename = "affiliates";
		$params = "fk_user_id = '" . $iAffiliateID . "'";
		return DAL::Update($tablename,$aDetails,$params);
	}
	public function getNewContract($iLangID = 1)
	{
		$sSQL = "SELECT mtl.title
					FROM misctranslations_cmn mtc
					INNER JOIN misctranslations_lang mtl ON mtl.fk_translation_id = mtc.translation_id
					INNER JOIN languages l ON l.language_id = mtl.fk_language_id
					WHERE mtc.translation_id = " . self::$iTranslationID . "
					AND
						(
							l.language_id = {$iLangID}
							OR
							l.default = 1
						)
					AND mtc.is_active = 1
					AND mtl.is_active = 1
					AND l.is_active = 1
					ORDER BY l.default ASC";
		return DAL::executeGetOne($sSQL);
	}
}
