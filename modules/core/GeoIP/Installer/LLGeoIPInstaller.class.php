<?php
/**
 * GeoIP installer
 * @package LoveLotto
 * @subpackage PayFrex
 */

namespace LL\GeoIP;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        if(self::getModuleVersion('GeoIP')<1){
        }
        $aJobs[]=new \LL\Installer\Task('LL\Silverpop\Installer', 'installMapping', 'Install Country/Currency/Language Mapping Table');
        $aJobs[]=new \LL\Installer\Task('LL\Silverpop\Installer', 'installMemberSignupMapping', 'Install Amendments to Member table for recording signup country/currency');
        return $aJobs;
    }

    /**
     * Installs all the tables we need for the new CMS
     */
    static function installMapping(\LL\Installer\Task $oTask){
        self::installMappingTable();
    }


    static function installMappingTable() {

        $oTTable=new \LL\Installer\Table('countries_currencies_languages_mapping');

        $oTTable->addField(new \LL\Installer\Field('country_id','int',11, false, Member::DEFAULT_COUNTRY_ID));
        $oTTable->addField(new \LL\Installer\Field('currency_id','int',11, false, Member::DEFAULT_CURRENCY_ID));
        $oTTable->addField(new \LL\Installer\Field('language_id','int',11, false, Member::DEFAULT_LANGUAGE_ID));

        $oTTable->compile();

    }

    static function installMemberSignupMapping(\LL\Installer\Task $oTask){
        self::amendMembersTable();
    }


    static function amendMembersTable() {

        $oTTable=new \LL\Installer\Table('members');

        $oTTable->addField(new \LL\Installer\Field('signup_country_id','int',11, false, 229));
        $oTTable->addField(new \LL\Installer\Field('signup_currency_id','int',11, false, 1));
        $oTTable->addField(new \LL\Installer\Field('signup_language_id','int',11, false, 1));
        
        $oTTable->compile();

    }


}
