<?php

/**
 * GeoIP
 * 
 * Functions using GeoIP functionality
 *
 * @author Nathan
 */
class GeoIP {

    var $sIP;
    
    function __construct($sIP=false)
    {
       /*
        * If IP has not been passed in, 
        * use IP value on query string if present, or REMOTE_ADDR in server if not
        * else use passed in value
        */
       if ($sIP === false)
       {
            if (array_key_exists('ip', $_GET))
            {
                $this->sIP = $_GET['ip'];
            }
            else
            {
                $this->sIP = $_SERVER['REMOTE_ADDR'];
            }
       }
       else
       {
           $this->sIP = $sIP;
       }
    }

    /**
     * getGeoIPCountryCode()
     * 
     * Gets the 2-character country code for the supplied IP address
     * (in the function, on the URL for testing, or the remote address)
     * using the maxmind geoip locator
     * 
     * @param $sIP A supplied IP address. If none supplied, uses GET value, or remote address on server
     * @return string country code
     */
    function getGeoIPCountryCode($sIP)
    {

       // Call GeoIP code here
       $sGeoIPCountryCode = geoip_country_code_by_name($sIP);

       return $sGeoIPCountryCode;
    }

    /**
     * geoIPMemberCountryInfo()
     * 
     * Checks if the country found by the GeoIP search has been set 
     * as blocked or inactive in the database
     * 
     * @return array of country information
     */
    function geoIPMemberCountryInfo()
    {
        // Get GeoIP country code
        $sCountryCode = strtoupper($this->getGeoIPCountryCode($this->sIP));

        $sSQL = "SELECT * FROM countries_cmn WHERE code = '{$sCountryCode}'";
        $aCountry = DAL::executeGetRow($sSQL);

        // Non-blocked countries will have value of zero
        // Also check if the country is not active (will be zero)
        /*if ($aCountry['blocked'] != 0 || $aCountry['isactive'] == 0)
        {
            return false;
        }*/
        
        // Return whole row of data, blocked/inactive will be dealt with in pages
        return $aCountry;
    }
    
    /** 
     * isLocalhost()
     * 
     * Check whether the current host is a "localhost" (127.0.0.1)
     * 
     * return boolean
     */
    function isLocalhost()
    {
        // Localhost will return false from the geoip call
        return ($this->getGeoIPCountryCode($this->sIP)===false?true:false);
    }
    
    
    /**
     * getCountryHomepageCurrencyMapping()
     * 
     * Checks the countries_currencies_languages_map table
     * to see if there's a mapping set up for the selected country
     * 
     * reutrn array of data if a mpaaing row is found, else false
     * 
     */
    static function getCountryHomepageCurrencyMapping($iCountryID)
    {
      
        $sSQL = "SELECT cclm.*, cc.code as iso_code
                 FROM countries_currencies_languages_map cclm
                 LEFT JOIN countries_cmn cc ON cclm.country_id = cc.country_id
                 WHERE cclm.country_id = {$iCountryID} ";
                         
        $aMapDetails = DAL::executeGetRow($sSQL);

        if (is_array($aMapDetails))
        {
            return $aMapDetails;
        }
        else 
        {
            return false;
        }
    }
    
    static function getCountryInfoFromID($iCountryID)
    {    
        $sSQL = "SELECT * 
                 FROM countries_cmn 
                 WHERE country_id = {$iCountryID} ";

        $aCountryInfo = DAL::executeGetRow($sSQL);

        if (is_array($aCountryInfo))
        {
            return $aCountryInfo;
        }
        else 
        {
            return false;
        }        
    }
}
