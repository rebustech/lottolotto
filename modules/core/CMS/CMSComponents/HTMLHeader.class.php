<?php

/**
 * HTML header definition class for CMS
 * @package LoveLotto
 * @subpackage CMS
 */
class HTMLHeader extends CMSPageComponent {
    
    public function __construct() {
        parent::__construct();
        
    }
    
    public function getOutput(){
        // Defined in parent class - needs further definition here?
    }
}
