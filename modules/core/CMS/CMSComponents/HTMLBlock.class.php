<?php

/**
 * HTML block definition class for CMS
 * @package LoveLotto
 * @subpackage CMS
 */
class HTMLBlock extends CMSPageComponent {
    
    public function __construct() {
        parent::__construct();
        
    }
    
    public function getOutput(){
        // Defined in parent class - needs further definition here?
    }
    
    function _getBlockConfigurationFields() {
        $aBlockConfigurationFields=parent::_getBlockConfigurationFields();
        $aBlockConfigurationFields[]=new ConfigurationString('Text','Text');
        return $aBlockConfigurationFields;
    }
}
