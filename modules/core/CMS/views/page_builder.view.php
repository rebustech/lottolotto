<div id="cms_page_builder">
    <h2><?= $this->oPage->name ?></h2>
    <script>

        var boxes = 1;

        $(document).ready(function() {
            redefine();
            reload();
        });

        function deleteComponent(id){
            alert(id);
            /*-----------------------------------------AJAX------------------------*/
                        $.ajax({
                          type: "POST",
                          url: "../system/modules/CMS/ajaxHandlers/deleteSingleComponent.php",
                          data: {"deleteID": id},
                            success: function (data) {
                               /*alert(data);*/
                               reload();
                        }
                    });
            /*---------------------------------------------------------------------*/
        }

        function reload(){
            /*-----------------------------------------AJAX------------------------*/
                        $.ajax({
                          url: "../system/modules/CMS/ajaxHandlers/getExistingComponents.php",
                            /*data: {'component': '<div>'+$(ui.helper)[0].outerHTML+'</div>'},  */
                            success: function (data) {
                               if(data !== "FAILURE"){
                                   $('#garbageCollector').html("");
                                   var dataRow = data.split("`");
                                   var arrayLength = dataRow.length;
                                    for (var i = 0; i < arrayLength-1; i++) {
                                        var splitValue = dataRow[i].split("|");
                                        addScriptSnippet(splitValue[0]);
                                        $('#garbageCollector').append('<div class="dragHotspots reorderable cmsPageComponent" id="box'+boxes+'">'+splitValue[1]+'</div>');
                                    }

                                }
                               /*alert(data);*/
                        }
                    });
            /*---------------------------------------------------------------------*/
        }

        function redefine() {
            $(".objectDrag").draggable({helper: 'clone', cursor: 'move', containment: 'document'});
            $(".objectAdded").draggable({helper: 'original', containment: '#garbageCollector'});

            $(".dragHotspots").droppable({
                accept: ".objectDrag",
                drop: function(event, ui) {
                    var cCount = $(this).children('div').length;
                    if(cCount == '0'){
                        var originalEl = event.srcElement;
                        /*alert($(ui.helper).html()); */
                        $(originalEl).removeClass("objectDrag");
                        $(originalEl).addClass("objectAdded");
                        var myobj = $(this);
                        /*-----------------------------------------AJAX------------------------*/
                        $.ajax({
                          type: "POST",
                          url: "../system/modules/CMS/ajaxHandlers/insertNewComponent.php",
                            data: {'component': '<div>'+$(ui.helper)[0].outerHTML+'</div>'},
                            success: function (data) {
                                /*alert(data);*/
                                if(data !== "FAILURE"){
                                    var splitValue = data.split("|");
                                    addScriptSnippet(splitValue[0]);
                                    $('#garbageCollector').append('<div class="dragHotspots reorderable cmsPageComponent" id="box'+boxes+'">'+splitValue[1]+'</div>');
                                }
                            }
                    });
                        /*---------------------------------------------------------------------*/

                        /*$('.objectAdded').append('test');*/
                        boxes++;
                    }
                    redefine();
                }
            });

            $("#bin").droppable({
                accept: ".objectAdded",
                drop: function(event, ui) {
                    var originalEl = event.srcElement;
                    $(originalEl).remove();
                }
            });

            $(".objectArea").droppable({
                accept: ".objectAdded",
                drop: function(event, ui) {
                     $(this).append(ui.helper.outerHTML);
                }
            });

        }

        function save(){
            $('#garbageCollector').children('div').each(
                function(index){
                   //JS TO CALCULATE ORDER + CONTENTS +RESAVE.
                   //NEW METHOD
                   console.log($(this).html());
                }
            );

        }

        function addScriptSnippet(scriptText){
            var script   = document.createElement("script");
            script.type  = "text/javascript";
            script.text  = scriptText;
            document.body.appendChild(script);
        }

        function moveCompUp(id){
   /*-----------------------------------------AJAX------------------------*/
                        $.ajax({
                          type: "POST",
                          url: "../system/modules/CMS/ajaxHandlers/reorderComponentUp.php",
                          data: {"componentID": id},
                            success: function (data) {
                               reload();
                        }
                    });
   /*---------------------------------------------------------------------*/
        }

        function moveCompDown(id){
   /*-----------------------------------------AJAX------------------------*/
                $.ajax({
                          type: "POST",
                          url: "../system/modules/CMS/ajaxHandlers/reorderComponentDown.php",
                          data: {"componentID": id},
                            success: function (data) {
                               reload();
                        }
                    });
   /*---------------------------------------------------------------------*/
        }

    </script>
    <div class="mainPage" style="width:70%; background-color: #FFFFFF; float:left; min-height:500px;">
        <div class="dragHotspots reorderable cmsPageComponent" id="box0">Drag new items from the pallete here</div>

        <div id="garbageCollector" style="width:100%; height:95%; background-color:#FFFFFF; color:white;"> Drop items here
            <!--<div id="bin" class="dragHotspots" style="width:100px; height: 100px; background-color: #006aeb; top:200px; left:500px;">BIN</div>-->

        </div>
    </div>

    <div class="componentPallete" style="width:250px; background-color: #999; height:500px; float:right; text-align:center;">
        Pallete
        <div class="objectDrag" id="imageComo" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Image</div>
        <div class="objectDrag" id="plainHTML" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Plain HTML</div>
        <div class="objectDrag" id="header1" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Header1</div>
        <div class="objectDrag" id="header2" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Header2</div>
        <div class="objectDrag" id="header3" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Header3</div>
        <div class="objectDrag" id="header4" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Header4</div>
        <div class="objectDrag" id="paragraph" style="width:200px; color:white;border:#CCCCCC 1px solid; background-color:#00A; margin-bottom:5px;">Paragraph Tag</div>

    </div>
<br/>
<input type="button" value="Save Page" onclick="save();"><input type="button" onclick="reload();" value="Discard Changes">
</div>

<style>
#makeMeDraggable { width: 300px; height: 300px; background: red; }
#draggableHelper { width: 300px; height: 300px; background: yellow; }
</style>


<div id="temptPagePreview" style="clear:both;">
<?php

CMSPageRenderer::renderPageByID('1');       //Show results from 'printer' class, just for development...

?>

</div>