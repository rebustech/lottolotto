<i class="fa fa-mobile fa-2x" title="Mobile 320" data-width="320px"></i>
<i class="fa fa-mobile fa-2x" title="Mobile 480" data-width="480px"></i>
<i class="fa fa-tablet fa-2x" title="Tablet 720" data-width="720px"></i>
<i class="fa fa-tablet fa-2x" title="Tablet 920" data-width="920px"></i>
<i class="fa fa-desktop fa-2x" title="Desktop" data-width="100%"></i>
-
<i class="fa fa-refresh fa-2x" title="Desktop" id="cmsPreviewRefresh"></i>
<br/>
<iframe src="<?=$this->oPage->url?>" style="width:100%;height:600px" id="cmsPreviewFrame"></iframe>

<script>
    $('i[data-width]').click(function(){
        $('#cmsPreviewFrame').width($(this).attr('data-width'));
    })
    $('#cmsPreviewRefresh').click(function(){
        var src=$('#cmsPreviewFrame').attr('src');
        $('#cmsPreviewFrame').attr('src','');
        $('#cmsPreviewFrame').attr('src',src);
    })
</script>
