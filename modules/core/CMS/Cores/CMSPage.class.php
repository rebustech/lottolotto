<?php

class CMSPage extends CMSPageComponent{



    function _getGameConfigurationFields(){
        $aGameConfigurationFields=array();
        $aGameConfigurationFields[]=new ConfigurationString('Price','Price Modification');
        return $aGameConfigurationFields;
    }

    public function getChildComponents(){
        $sSql='SELECT * FROM game_engine_components WHERE is_lottery=1 AND fk_game_engine_id='.$this->id;
        $aChildren=DAL::executeQuery($sSql);
        foreach($aChildren as $aChild){
            $sHandler=$aChild['handler'];
            $oNewChild=new $sHandler;
            $oNewChild->populateFromArray($aChild);
            $aAllChildren[]=$oNewChild;
        }
        return $aAllChildren;
    }

    public function getCommonModifiers(){
        $sSql='SELECT * FROM game_engine_components WHERE is_lottery=0 AND fk_game_engine_id='.$this->id;
        $aChildren=DAL::executeQuery($sSql);
        foreach($aChildren as $aChild){
            $sHandler=$aChild['handler'];
            $oNewChild=new $sHandler;
            $oNewChild->populateFromArray($aChild);
            $aAllChildren[]=$oNewChild;
        }
        return $aAllChildren;
    }

    /**
     * Instantiates a ticket engine of the correct class for the id provided
     * @param int $id ID of the ticket engine to return
     * @return \BaseTicketEngine
     */
    static function getById($id){
        $aData=DAL::executeGetRow('SELECT * FROM pages_cmn WHERE page_id='.intval($id));
        $oPage=new CMSPage();
        $oPage->populateFromArray($aData);
        //Load all the components
        return $oPage;
    }

    function getDump(){
        $oOut=new stdClass();
        $oOut->id=$this->id;
        $oOut->name=$this->name;
        $oOut->is_active=$this->is_active;
        //$oOut->configurationFields=$this->getGameConfigurationFields();
        //$oOut->childComponents=$this->getDumpOfChildren($this->getChildComponents());
        //$oOut->commonModifiers=$this->getDumpOfChildren($this->getCommonModifiers());
        return $oOut;
    }

}