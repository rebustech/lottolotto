<?php

/**
 * Common pages admin controller
 * @package LoveLotto
 * @subpackage CMS
 */
class pages_cmn extends AdminController{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        $form=new SelfSubmittingForm();
        $form->submitToGeneric($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);


        if($iId==0){
            $tabs=new TabbedInterface();
            $form->AddControl($tabs);

            //Create a tab to show assigned users
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-magnet';
            $oTitle=new HtmlTagControl('h2','Create Page');
            $aItems[]=$oTitle->Output();
            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
            $tabs->aItems['Items']=$oItemsTab;


        } else {


            $tabs=new TabbedInterface();
            $form->AddControl($tabs);

            //Create a tab to show the main details
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-magnet';

            $oPage=CMSPage::getById($iId);

            $_SESSION['oPage'] = $oPage;
            $oTitle=new HtmlTagControl('h2','Edit Page : '.$oPage->name);

            $aItems[]=$oTitle->Output();

            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);
            $tabs->aItems['Items']=$oItemsTab;


            /**
             * Components Tab
             */
            $oComponentsTab=new TabbedInterface(lang::get('components'));
            $oComponentsTab->sIcon='fa-puzzle-piece';

            $oComponentsControl=new CMSPageBuilder($oPage);

            $oComponentsTab->AddControl($oComponentsControl);
            $tabs->aItems['components']=$oComponentsTab;

            /**
             * Preview Tab
             */
            $oPreviewTab=new TabbedInterface(lang::get('preview'));
            $oPreviewTab->sIcon='fa-globe';

            $oPreviewControl=new CMSPagePreview($oPage);

            $oPreviewTab->AddControl($oPreviewControl);
            $tabs->aItems[]=$oPreviewTab;


            //Create a tab to show available countries
            $oCountriesTab=new TabbedInterface(lang::get('countries'));
            $oCountriesTab->sIcon='fa-globe';

// Countries and Whitelabel stuff removed here

            /**
             * Audit log tab
             */
            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';

            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'pages_cmn\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');

            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,code,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems[]=$oLogTab;
        }

        /**
         * Create the output
         */
        $aItems[]=$form->Output();

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }

    function afterGenericSave(){
        // Send through to relevant page,
        // and to page's children

        echo("<pre>");
        //print_r($_POST);
        die();
    }
}