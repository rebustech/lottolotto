<?php
/**
 * Used to render pages on the frontend
 *
 * @author Matthew / Jonathan Patchett
 * @package LoveLotto
 * @subpackage CMS
 */

class CMSPageRenderer {

    /**
     * Renders a page by the given pageID
     * @param int $iPageID ID of the page to be rendered
     * @return type
     */
    public static function renderPageByID($iPageID) {
        if(!is_numeric($iPageID)){
            throw new LottoException('CMSPageRenderer.renderPageByID.InvalidID','Supplied page ID is invalid');
        }

        $sSQL = "SELECT * FROM cms_page_components WHERE `pageID` = '" . $iPageID . "' ORDER BY orders ASC";
        $aComponentData = \DAL::executeQuery($sSQL);

        return self::renderPageData($aComponentData);
    }

    /**
     * Renders a page from a given URL
     * @param string $sPageURL URL to render
     * @return string The full content of the CMS elements
     */
    public static function renderPageByURL($sPageURL) {
        $query = 'SELECT c.* FROM cms_page_components c INNER JOIN pages_cmn p ON c.`pageID`=p.page_id WHERE url=\'' . $sPageURL . '\' ORDER BY orders ASC';
        $aCompData = \DAL::executeQuery($query);
        return self::renderPageData($aCompData);
    }

    /**
     * Gets a page id from a given URL
     * @param string $sPageURL
     * @return int
     */
    public static function getPageIdByURL($sPageURL) {
        $sPageURL = str_replace('_', '-', $sPageURL);
        $query = 'SELECT page_id FROM pages_cmn p WHERE url=\'' . $sPageURL . '\'';
        $aCompData = \DAL::executeGetOne($query);
        return $aCompData;
    }

    public static function renderPageData($aCompData) {
        $renderedHTML = '';

        foreach ($aCompData as $aData) {

            //$renderedHTML .= "<br/>Component ID: ".$aData['componentID']."<br/>"; //VISUAL REPRESENTATION

            $componentID = $aData['componentID'];

            $query2 = "SELECT * FROM cms_components WHERE `ID` = '" . $componentID . "'";
            $aCompData2 = \DAL::executeGetRow($query2);          //GIVES ME THE CONFIG

            $configVal = $aCompData2['config'];


            $compName = $aCompData2['name'];
            $componentRowID = $aData['ID'];
            $componentContent = $aData['componentContent'];

            //$renderedHTML .= $componentRowID." ";

            $componentContent = substr($componentContent, 2, -2);
            $componentContent = explode('","', $componentContent);

            $componentElementDetails = explode("|", $configVal);

            unset($elementCompClasses);

            foreach ($componentElementDetails as $elementDetail) {
                $elementDetail = explode(":", $elementDetail);
                $elementCompClasses[] = $elementDetail[1];
            }

            $oView = new \LLView();
            $oView->elementCompClasses = elementCompClasses;
            $oView->componentContent = $componentContent;
            $renderedHTML = $oView->output(Config::$config->sTemplatePath . '/../views/pods/content_blocks/' . $compName);
        }
        return $renderedHTML;
    }
}
