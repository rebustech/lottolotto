<?php

/**
 * Page Builder control (copied from GameEngine/Controls/GameEngineBuilder.class.php
 *
 * @package LoveLotto
 * @subpackage Controls
 */
class CMSPagePreview extends Control{
    var $sView='page_preview';
    var $oPage;

    /**
     * Add the game builder javascript in just before the close body tag
     */
    function __construct($oPage) {

        $this->oPage=$oPage;

        //LLResponse::$sPostBody.='<link href="/system/modules/CMS/Scripts/froala/css/froala_editor.min.css" rel="stylesheet" type="text/css">';
    }

    function Output() {
        $oView=new LLView();
        $oView->oPage=$this->oPage;
        return $oView->output($this->sView);
    }
}
