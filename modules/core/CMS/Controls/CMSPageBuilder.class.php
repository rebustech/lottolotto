<?php

/**
 * Page Builder control (copied from GameEngine/Controls/GameEngineBuilder.class.php
 *
 * @package LoveLotto
 * @subpackage Controls
 */
class CMSPageBuilder extends Control{
    var $sView='page_builder';
    var $oPage;

    /**
     * Used to track if the variants.js script has been added yet
     * @var bool
     */
    static $bScriptsAdded=false;

    /**
     * Add the game builder javascript in just before the close body tag
     */
    function __construct($oPage) {

        $this->oPage=$oPage;

        //Get the JSON configuration for the game engine in it's current state and add just before the /body
        LLResponse::$sPostBody.=LLResponse::addInlineScript($this->getJSON());

        if(!self::$bScriptsAdded){
            //Add the game builder javascript just before /body (and after the above JSON output)
            LLResponse::$sPostBody.=LLResponse::addScript('/system/modules/CMS/Scripts/pageBuilder.js');
            LLResponse::$sPostBody.='<link href="/system/modules/CMS/Scripts/froala/css/froala_editor.min.css" rel="stylesheet" type="text/css">';
            self::$bScriptsAdded=true;
        }
    }

    function getJSON(){
        return 'var pageBuilderData='.json_encode($this->oPage->getDump());
    }

    function Output() {
        $oView=new LLView();
        $oView->oPage=$this->oPage;
        return $oView->output($this->sView);
    }
}
