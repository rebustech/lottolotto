<?php
/**
 * Installs the CMS module.
 *
 * @package LoveLotto
 * @subpackage Installer
 * @author J.Patchett
 */

namespace LL\CMSPageBuilder;

class Installer{
    use \LL\Installer\TInstaller;

    function getInstallTasks(){
        $aJobs=array();
        //$dVersion=$this->getModuleVersion('CMSPageBuilder');

            //$aJobs[]=new \LL\Installer\Task('\LL\TicketEngines\LoteriaManisesInstaller', 'installDatabase', 'Add database table for CMS module.');


        return $aJobs;
    }

    static function installDatabase() {
        $sSQL = "
        CREATE TABLE `cms_components` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT '0',
	`handler` VARCHAR(80) NULL DEFAULT '0',
	`config` TEXT NULL,
	`fk_parentID` INT NULL DEFAULT '0',
	PRIMARY KEY (`ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
";                                                                              //CREATE CMS_COMPONENTS TABLE

        \DAL::Query($sSQL);     //INSERT SOME TEST COMPONENTS




        $sSql = "
            CREATE TABLE `cms_page_components` (
	`ID` INT NULL,
	`pageID` INT(11) NULL,
	`componentID` INT(11) NULL,
	`componentContent` VARCHAR(500) NULL,
	`order` INT(11) NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;";                                                                //CREATE PAGE COMPONENTS TABLE

        \DAL::Query($sSQL);

        $sSql = "INSERT INTO `cms_components` (`ID`, `name`, `handler`, `config`, `fk_parentID`) VALUES
	(1, 'imageComo', '0', 'file:ImageFile|textarea:lol|textarea:ta', 0),
	(2, 'plainHTML', '0', 'textarea:Test', 0),
	(3, 'header1', '0', 'text:Header1Content', 0),
	(4, 'header2', '0', 'text:H2', 0),
	(5, 'header3', '0', 'text:H3', 0),
	(6, 'header4', '0', 'text:H4', 0),
	(7, 'paragraph', '0', 'textarea:Paragraph', 0);
";

        \DAL::Query($sSQL);


        self::installModule('CMSPageBuilder',1);
    }
}