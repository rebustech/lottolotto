<?php
/**
 * Interface for a CMS component
 * CHANGE
 */

interface ICMSPageComponent {

    //Gets the parent component
    public function getParentComponent();

    // Gets the child components of this component
    public function getChildComponents();

    public function getComponentConfigurationFields($iConfigID);

    public function getBackendFields();

    public function canBeChild($oCMSComponent);

    public function canHaveChildren();

    public function canAcceptChild($oCMSComponent);

    public function save();

    // Get component from ID
    public function getById($iID);

    public function getOutput();


    // Not sure if these two are needed in the interface?
    public function getDump();

    public function getDumpOfChildren($aChildComponents);
}
