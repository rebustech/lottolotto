<?php

// TODO: change fields to match database format

/**
 * Base class for a CMS component
 */
//abstract class CMSPageComponent extends LLModel implements ICMSPageComponent{
   abstract class CMSPageComponent extends LLModel {
    // Variables from the database
    var $iId;
    
    /**
     * Decides on which component this is a child of.
     * @var integer
     */
    var $iParentComponent=0;
    
    var $iPageId;
    
    var $sComments;
    
    var $sHandler;
    
    var $bIsCacheable;  // Informs parent if this is cacheable
    
    var $bIsActive;
 
    // Existing variables
    var $sConfig; // raw value from database

    var $aConfigurationFields; // same as $sConfig, but exploded into array
    
     /*
     * Get component from ID - does this need to be in each subclass?
     */
    public static function getById($iID){
          
        
    }
    
    /**
     * Gets the parent component
     * @return CMSComponent
     */
    public function getParentComponent(){

        $sSql='SELECT * FROM page_components WHERE id='.$this->fk_parent_id;
        
        // Just one row required here
        $aParent=DAL::executeGetRow($sSql);

        $sHandler=$aParent['handler'];

        $oNewParent->populateFromArray($aParent);

        return $oNewParent;
    }
    
    /**
     * Gets the child components of this component
     * @return array
     */
    public function getChildComponents(){
        
        // $country and $user params in this function are only used on display, 
        // not required for admin
        $sSql='SELECT * FROM page_components WHERE fk_parent_id='.$this->id;
        $aChildren=DAL::executeQuery($sSql);
        foreach($aChildren as $aChild){
            $sHandler=$aChild['handler'];
            $oNewChild=new $sHandler;
            $oNewChild->populateFromArray($aChild);
            $aAllChildren[]=$oNewChild;
        }
        return $aAllChildren;
    }
    

    

    public function canBeChild($oCMSComponent){
        
    }
    
    public function canHaveChildren(){
        
    }
    
    public function canAcceptChild($oCMSComponent){
        
    }
    
    public function save(){
        
    }
    


    
    public function getOutput(){
        // Needs to be defined in each subclass?
    }    
      
    // Keep these two for the moment - used for generating JSON
    function getDump(){
        $oOut               = new stdClass();
        
        $oOut->id           = $this->iId;
        $oOut->page_id      = $this->iPageId;
        $oOut->comments     = $this->sComments;
        $oOut->handler      = $this->sHandler;
        $oOut->is_active    = $this->bIsActive;
        $oOut->is_cacheable = $this->bIsCacheable;
        $oOut->parent_id    = $this->iParentComponent;
        
        $oOut->configurationFields=$this->getComponentConfigurationFields();
        
        $oOut->childComponents=$this->getDumpOfChildren($this->getChildComponents());
        return $oOut;
    }

    function getDumpOfChildren($aChildComponents){
        $aOut=array();
        if(is_array($aChildComponents)){
            foreach($aChildComponents as $oChildComponent){
                $aOut[]=$oChildComponent->getDump();
            }
        }
        return $aOut;
    }
}