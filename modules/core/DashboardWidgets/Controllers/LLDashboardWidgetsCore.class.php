<?php

namespace LL\DashboardWidgets;

class Core extends \LL\Controller{
    static function TicketsAll(){
        $oView=new \LLView();
        return $oView->output('ticketsAll');
    }

    static function SalesByPromotion(){
        $oView=new \LLView();
        return $oView->output('SalesByPromotion');
    }

    static function SalesByProduct(){
        $oView=new \LLView();
        return $oView->output('SalesByProduct');
    }

    static function Headlines(){

        $key='stats.topstatsview';
        $sData=\LLCache::get($key);
        if($data){
            return $sData;
        }else{
            $oView=new \LLView();
            $sSQL="
            select 'Total TAV' as title,concat('€',format(sum(total),2)) value,'TAV' as `key` FROM bookings where booking_date>date_sub(now(),interval 1 year)
            union
            select 'Total Winnings' as title,concat('€',coalesce(format(sum(amount),2),0)),'WIN' FROM lottery_winnings lw INNER JOIN booking_items bo ON bo.bookingitem_id=lw.fk_bookingitem_id where fk_lotterydate>date_sub(now(),interval 1 year)
            union
            select 'Total Active Customers',count(*),'CUST' from members
            ";
            $oView->aHeadlines=\DAL::executeQuery($sSQL);

            $sData=$oView->output('headlines');

            \LLCache::add($key, $sData);

            return $sData;

            /**
             * Took this out for now as it's too slow
             * union
                select 'Total Active Bets',count(*),'ACT' from booking_items where fk_lotterydate>now()
             */
        }       

    }
}