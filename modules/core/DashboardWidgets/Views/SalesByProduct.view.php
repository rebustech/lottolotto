<div class="dashboardChart dashboardSmallChart">
    <h2>Sales by Product (1 yr)</h2>
    <div id="getByProductChart"></div>
    <script>
    $.getJSON('/administration/API/MLChartsTicketSales/getByProduct',function(data){

        llcharts.salesByProduct = AmCharts.makeChart("getByProductChart", {
            "type": "pie",
            "theme": "light",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            "dataProvider": data,
            "valueField": "total",
            "titleField": "name",
            "labelRadius": 15,
            "labelText": "[[title]]",
            "exportConfig":{
              menuItems: [{
              icon: '/lib/3/images/export.png',
              format: 'png'
              }]
                }
        });


    });

    setInterval(function(){
        $.getJSON('/administration/API/MLChartsTicketSales/getByProduct',function(data){
            llcharts.salesByProduct.dataProvider=data;
            llcharts.salesByProduct.validateData();
        })
    },15000);

    </script>
</div>
