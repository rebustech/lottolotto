<div class="dashboardChart">
    <h2>All Sales (1 yr)</h2>
<div id="ticketsAllChart" style="height:310px"></div>
<script>


$.getJSON('/administration/API/MLChartsTicketSales/get',function(data){

    llcharts.allSales = AmCharts.makeChart("ticketsAllChart", {
        "type": "serial",
        "theme": "light",
        "pathToImages": "http://www.amcharts.com/lib/3/images/",
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0.2,
            "dashLength": 1,
            "position": "left"
        }],
        "graphs": [{
            "id":"g1",
            "balloonText": "[[category]]<br /><b><span style='font-size:14px;'>Sales: [[value]]</span></b>",
            "bullet": "round",
            "bulletBorderAlpha": 1,
                    "bulletColor":"#FFFFFF",
            "hideBulletsCount": 50,
            "title": "red line",
            "valueField": "value1",
                    "useLineColorForBulletBorder":true
        }],
        "chartScrollbar": {
            "autoGridCount": true,
            "graph": "g1",
            "scrollbarHeight": 40
        },
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "category",
        "categoryAxis": {
            "parseDates": true,
            "axisColor": "#DADADA",
            "dashLength": 1,
            "minorGridEnabled": true
        },
            "exportConfig":{
              menuRight: '20px',
          menuBottom: '30px',
          menuItems: [{
          icon: 'http://www.amcharts.com/lib/3/images/export.png',
          format: 'png'
          }]
            }
    });


    llcharts.allSales.addListener("rendered", taZoomChart);



    // this method is called when chart is first inited as we listen for "dataUpdated" event
    function taZoomChart() {
        if(!typeof chartData == 'undefined'){
            // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
            llcharts.allSales.zoomToIndexes(chartData.length - 40, chartData.length - 1);
        }
    }



});

</script>
</div>