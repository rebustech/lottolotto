<div class="dashboardChart dashboardSmallChart">
    <h2>Sales by Promo (1 yr)</h2>
    <div id="getByPromoChart"></div>
    <script>
    $.getJSON('/administration/API/MLChartsTicketSales/getByPromo',function(data){

        llcharts.salesByPromo = AmCharts.makeChart("getByPromoChart", {
            "type": "pie",
            "theme": "light",
            "pathToImages": "http://www.amcharts.com/lib/3/images/",
            "dataProvider": data,
            "valueField": "total",
            "titleField": "promo",
                "exportConfig":{
              menuItems: [{
              icon: '/lib/3/images/export.png',
              format: 'png'
              }]
                }
        });

        setInterval(function(){
            $.getJSON('/administration/API/MLChartsTicketSales/getByPromo',function(data){
                llcharts.salesByPromo.dataProvider=data;
                llcharts.salesByPromo.validateData();
            })
        },15000);


    });

    </script>
</div>
