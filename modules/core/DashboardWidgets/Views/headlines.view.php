<div class="Headlines">
<?php foreach($this->aHeadlines as $oHeadline):?>

<div class="Headline" data-key="<?=$oHeadline['key']?>">
    <h2><?=$oHeadline['title']?></h2>
    <p><?=$oHeadline['value']?></p>
</div>
<?php endforeach; ?>

<script>
    setInterval(function(){
        $.getJSON('/administration/API/MLChartsTicketSales/topStats',function(data){
            for(var dataId in data){
                var dataItem=data[dataId];
                var headline=$('.Headline[data-key='+dataItem.key+']');
                headline.find('h2').text(dataItem.title);
                headline.find('p').text(dataItem.value);
            }
        })
    },15000);
</script>
</div>