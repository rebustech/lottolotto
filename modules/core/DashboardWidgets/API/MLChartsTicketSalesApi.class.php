<?php
class MLChartsTicketSalesApi extends \LL\AjaxController{
    function get(){
        $key='stats.get';
        $sData=\LLCache::get($key);
        if($data){
            die($sData);
        }else{
            $aData=\DAL::executeQuery('SELECT * FROM vw_main_dashboard_tickets_all');
            $sData=json_encode($aData);
            \LLCache::add($key, $sData);

            die($sData);
        }
    }

    function getByPromo(){
        $key='stats.bypromo';
        $sData=\LLCache::get($key);
        if($data){
            die($sData);
        }else{
            $aData=\DAL::executeQuery('select promo,sum(total) as total from bookings group by promo');
            $sData=json_encode($aData);
            \LLCache::add($key, $sData);
            die($sData);
        }
    }

    function getByProduct(){
        $key='stats.byproduct';
        $sData=\LLCache::get($key);
        if($data){
            die($sData);
        }else{
            $aData=\DAL::executeQuery('select name,sum(cost) as total from booking_order_items oi INNER JOIN game_engines ge ON ge.id=oi.fk_game_engine_id group by name');
            $sData=json_encode($aData);
            \LLCache::add($key, $sData);

            die($sData);
        }
    }

    function topStats(){
        $key='stats.topstats';
        $sData=\LLCache::get($key);
        if($data){
            die($sData);
        }else{
            $sSQL="
            select 'Total TAV' as title,concat('€',format(sum(total),2)) value,'TAV' as `key` FROM bookings where booking_date>date_sub(now(),interval 1 year)
            union
            select 'Total Winnings' as title,concat('€',coalesce(format(sum(amount),2),0)),'WIN' FROM lottery_winnings lw INNER JOIN booking_items bo ON bo.bookingitem_id=lw.fk_bookingitem_id where fk_lotterydate>date_sub(now(),interval 1 year)
            union
            select 'Total Active Customers',count(*),'CUST' from members
            ";
            $aData=\DAL::executeQuery($sSQL);
            $sData=json_encode($aData);

            \LLCache::add($key, $sData);

            die($sData);

            //Took this out for now as too slow
            //union
            //select 'Total Active Tickets',count(*),'ACT' from booking_items where fk_lotterydate>now()
        }
    }
}