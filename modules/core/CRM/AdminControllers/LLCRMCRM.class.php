<?php

/**
 * CRM Admin Controller. This extends members.class.php for backwards compatiblity
 */

namespace LL\CRM;

class CRM extends \members{

    // Maximum number of members allowed to be returned in a search
    private $iMaxAllowedMembers = 10;

    function edit(){
        $iId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        return $this->GenericDetails('members', $iId);
    }

    function create(){
        return $this->GenericDetails('members', 0);
    }

    // Find customer screen
    function find(){

        $v=new \LLView();

        // Perform query if we have post data
        if ($_POST)
        {
            $sSQL = "SELECT member_id, firstname, lastname, email, dob FROM members WHERE ";
            $aSQLOptions = array();

            // Build query from only supplied data
            foreach ($_POST as $sField=>$sValue)
            {
                // Ignore the submit button!
                if ($sField != 'action' && !empty($sValue))
                {
                    $aSQLOptions[] = $sField . "='{$sValue}'";
                }
            }

            $sSQL .= implode(" AND ", $aSQLOptions);

            $aRows=\DAL::executeQuery($sSQL);


            $iNumRowsReturned = count($aRows);

            // Only redirect if we have exactly one result
            if ($iNumRowsReturned == 0)
            {
                $v->bNoResults=true;
            }
            // If more than the allowed number of members have been displayed
            // show an error,
            // else show a list of possible matches
            else if ($iNumRowsReturned > $this->iMaxAllowedMembers)
            {
                $v->bTooManyResults=true;
            }
            else if ($iNumRowsReturned > 1 && $iNumRowsReturned <= $this->iMaxAllowedMembers)
            {
                $v->bWithinLimits = true;
                $v->sResultConfirmString = $iNumRowsReturned . " possible results.";
                $v->aFoundMembers = $aRows;
            }
            else
            {
                $iMemberID = (int) $aRows[0]['member_id'];
                echo('Loading details for this member, please wait...');
                echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberID . '"</script>');
                die();
            }
        }

        // Output the view
        return $v->output('Views/find_member');
    }

    // Edit address
    function edit_address(){
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);
        //print_d($oMember);

        if($_POST){

            foreach ($_POST as $sKey=>$sVal)
            {
                if ($sKey != 'action')
                {
                    $aNewDetails[$sKey] = $sVal;
                }
            }
            // Set the member ID here
            $oMember->member_id = $iMemberId;

            //Copy all valid fields to a new array and populate object from there
            $oMember->populateFromArray($aNewDetails);

            //print_d($oMember);

            $oMember->save($aNewDetails);
            echo('Saving address details, please wait...');
            echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
            die();
        }

        $v=new \LLView();
        $v->oMember=$oMember;

        $v->aCountries = $this->getCountriesForDropdown();

        return $v->output('Views/edit_address');

    }

    // Edit contact (i.e telephone number)
    function edit_contact(){
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);
        //print_d($oMember);

        if($_POST){

            foreach ($_POST as $sKey=>$sVal)
            {
                if ($sKey != 'action')
                {
                    $aNewDetails[$sKey] = $sVal;
                }
            }
            // Set the member ID here
            $oMember->member_id = $iMemberId;

            // Copy all valid fields to a new array and populate object from there
            $oMember->populateFromArray($aNewDetails);

            //print_d($oMember);

            $oMember->save($aNewDetails);
            echo('Saving contact details, please wait...');
            echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
            die();
        }

        $v=new \LLView();
        $v->oMember=$oMember;
        return $v->output('Views/edit_contact');

    }


    // Edit Date of birth
    function edit_dob(){
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        $v=new \LLView();

        if($_POST){

            // Format dates to apporopriate format
            $iDay = sprintf('%02d', $_POST['dob_d']);
            $iMonth = sprintf('%02d', $_POST['dob_m']);
            $iYear = sprintf('%04d', $_POST['dob_y']);


            // Check date is valid
            if (checkdate($iMonth, $iDay, $iYear)) {

                // Now check customer is over 18 years old
                $sNewDOB  = $iYear . "-" . $iMonth . "-" . $iDay;

                // Start from midnight of their date of birth
                $iTsDOB = strtotime($sNewDOB . " 00:00:00");

                // 18 years from the current date
                $iTsMinValidDOB = strtotime("-18 years");

                // If their date of birth is less than the date 18 years ago, they're kosher
                if ($iTsDOB < $iTsMinValidDOB) {

                   $aNewDetails['dob'] = $sNewDOB;

                   // Set the member ID here
                   $oMember->member_id = $iMemberId;

                   // Copy all valid fields to a new array and populate object from there
                   $oMember->populateFromArray($aNewDetails);

                   $oMember->save($aNewDetails);
                   echo('Saving date of birth details, please wait...');
                   echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                   die();
                }
                else
                {
                    // Too young to play
                   $v->bTooYoung=true;
                }
            }
            else
            {
                // Invalid date
                $v->bInvalidDate=true;
            }
        }

        $iYear = date("Y");
        $iStartYear = $iYear-18;

        // Array of month names, keyed on month number
        // Using language file for this - can use other option?
        $aMonths = array(1=>\lang::get('m_jan'),
                            \lang::get('m_feb'),
                            \lang::get('m_mar'),
                            \lang::get('m_apr'),
                            \lang::get('m_may'),
                            \lang::get('m_jun'),
                            \lang::get('m_jul'),
                            \lang::get('m_aug'),
                            \lang::get('m_sep'),
                            \lang::get('m_oct'),
                            \lang::get('m_nov'),
                            \lang::get('m_dec'));

        $v->oMember=$oMember;
        $v->aDOB=explode("-", $oMember->dob);
        $v->aMonths = $aMonths;
        $v->iStartYear = $iStartYear;

        return $v->output('Views/edit_dob');
    }

    function change_password()
    {
        $v=new \LLView();
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);
        //print_d($oMember);

        if($_POST){

            if ($oMember->resetPassword($_POST['username'])) {
                echo('Password has been changed, please wait...');
                echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                die();
            }
            else {
                $v->bChangePasswordError=true;
            }
        }


        $v->oMember=$oMember;
        return $v->output('Views/change_password');

    }

    function send_email()
    {
        $v=new \LLView();
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        if($_POST){
            $aPersonalisationData = array('username'        => $oMember->username,
                                          'balance'         => number_format_locale($oMember->balance, 2, '.', ','),
                                          'mail_body'       => nl2br($_POST['mail_body']),
                                          'mail_subject'    => $_POST['mail_subject'],
                                          'member_id'       => $iMemberId);


            if (\Email::sendCRMEmail($oMember->email, $aPersonalisationData))
            {
                echo('Sending CRM email to ' . $oMember->firstname . ' ' . $oMember->lastname . ' (' . $oMember->email . '), please wait...');
                echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                die();
            }

        }

        $v->oMember=$oMember;
        return $v->output('Views/send_email');
    }


    function amend_limits()
    {
        $v=new \LLView();
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        // Set the member ID here
        $oMember->member_id = $iMemberId;

        $iNow = time();

        $i1MonthTS = strtotime("+1 month", $iNow);
        $i3MonthTS = strtotime("+3 months", $iNow);
        $i6MonthTS = strtotime("+6 months", $iNow);
        $i9MonthTS = strtotime("+9 months", $iNow);

        $s1MonthString = date("l j F, Y", $i1MonthTS);
        $s3MonthString = date("l j F, Y", $i3MonthTS);
        $s6MonthString = date("l j F, Y", $i6MonthTS);
        $s9MonthString = date("l j F, Y", $i9MonthTS);

        $v->s1MonthString = $s1MonthString;
        $v->s3MonthString = $s3MonthString;
        $v->s6MonthString = $s6MonthString;
        $v->s9MonthString = $s9MonthString;


        if($_POST){
            if (array_key_exists('unsuspend_confirm', $_POST) && $_POST['unsuspend_confirm'] === "unsuspend_confirm")
            {


                // Copy all valid fields to a new array and populate object from there
                $aNewDetails=array('is_suspended'       => '0',
                                   'suspension_start'   => null,
                                   'suspension_end'     => null,
                                   'suspension_lifted'  => date('Y-m-d H:i:s', $iNow)
                                  );

                $oMember->populateFromArray($aNewDetails);
                $oMember->save($aNewDetails);

                $aPersonalisationData = array('current balance' => number_format_locale($oMember->balance, 2, '.', ','),
                                              'firstname'       =>$oMember->firstname,
                                              'member_id'       => $iMemberId);

                // Send email to say they've been unsuspended
                \Email::sendExclusionLiftedEmail($oMember->email, $aPersonalisationData);

                echo("Unsuspending {$oMember->firstname} {$oMember->lastname}, please wait...");

                echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                die();
            }
            if (array_key_exists('suspend_period', $_POST) && $_POST['suspend_period'] !== "dns")
            {
                $iSuspendPeriod = (int) $_POST['suspend_period'];

                $sSuspendString = "s" . $iSuspendPeriod . "MonthString";
                $iSuspendTS = "i" . $iSuspendPeriod . "MonthTS";

                // Copy all valid fields to a new array and populate object from there
                $aNewDetails=array('is_suspended'       => 1,
                                   'suspension_start'   => date('Y-m-d H:i:s', $iNow),
                                   'suspension_end'     => date('Y-m-d H:i:s', $$iSuspendTS),
                                   'suspension_lifted'  => null
                                  );

                $oMember->populateFromArray($aNewDetails);
                $oMember->save($aNewDetails);


                $aPersonalisationData = array('current balance' => number_format_locale($oMember->balance, 2, '.', ','),
                                              'firstname'       =>$oMember->firstname,
                                              'number_of_month' => $iSuspendPeriod,
                                              'member_id'       => $iMemberId);

                // Send email to say they've been unsuspended
                \Email::sendExclusionChangedEmail($oMember->email, $aPersonalisationData);

                echo("Suspending {$oMember->firstname} {$oMember->lastname} for {$iSuspendPeriod} months, until <b>{$$sSuspendString}</b>, please wait...");

                echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                die();
            }

            if (array_key_exists('monthly_limit', $_POST) && $_POST['monthly_limit'] != "")
            {
                $fMonthlyLimit = (float) $_POST['monthly_limit'];

                // Copy all valid fields to a new array and populate object from there
                $aNewDetails['monthly_deposit_limit'] = $fMonthlyLimit;

                $oMember->populateFromArray($aNewDetails);
                $oMember->save($aNewDetails);

                $aPersonalisationData = array('current balance' => number_format_locale($oMember->balance, 2, '.', ','),
                                              'firstname'       =>$oMember->firstname,
                                              'new_deposit_limit' => $fMonthlyLimit,
                                              'member_id'       => $iMemberId);

                // Send email to say they've been unsuspended
                \Email::sendDepositLimitChangedEmail($oMember->email, $aPersonalisationData);

                echo('Updating monthly limit for ' . $oMember->firstname . ' ' . $oMember->lastname . ' to ' . $fMonthlyLimit . ', please wait...');
                echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                die();
            }


        }
        else
        {
            $iSuspensionStart = strtotime($oMember->suspension_start);
            $iSuspensionEnd = strtotime($oMember->suspension_end);
            $iSuspensionLifted = strtotime($oMember->suspension_lifted);

            $iSuspensionCoolingOffEnd = strtotime("+24 hours", $iSuspensionLifted);

            if ($oMember->is_suspended && $iNow < $iSuspensionEnd)
            {
                $v->bInSuspensionPeriod = true;
                $v->sSuspensionStartDate = date("l j F, Y \a\\t H:i:s", $iSuspensionStart);
                $v->sSuspensionEndDate = date("l j F, Y \a\\t H:i:s", $iSuspensionEnd);
            }
            else if ($oMember->is_suspended && $iNow >= $iSuspensionEnd)
            {
                $v->bUserSuspended = true;
            }

            if ($iNow < $iSuspensionCoolingOffEnd)
            {
                $v->bInCoolingOffPeriod = true;
                $v->sCoolingOffPeriodEndDate = date("l j F, Y \a\\t H:i:s", $iSuspensionCoolingOffEnd);
            }
        }

        $v->oMember=$oMember;

        return $v->output('Views/amend_limits');
    }

    function add_note()
    {
        $v=new \LLView();
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        // Need to use the global security object here in order to get
        // the user currently logged in
        global $oSecurityObject;

        if($_POST){

            if ($_POST['callback_required'] == '')
            {
                $sCallbackDateTime = null;
            }
            else
            {
                $sCallbackDateTime = $_POST['callback_required'];
            }

            if ($_POST['contact_outcome_type'] == '')
            {
                $iContactOutcomeType = 1;
            }
            else
            {
                $iContactOutcomeType = (int) $_POST['contact_outcome_type'];
            }


            $sTablename = "members_contact_log";
            $aData = array(
                'fk_member_id'      => $iMemberId,
                'fk_contact_type'   => (int) $_POST['contact_type'],
                'datetime'          => date("Y-m-d H:i:s"),
                'customer_message'  => $_POST['customer_message'],
                'fk_user_id'        => (int) $oSecurityObject->getUserID(),
                'details'           => $_POST['details'],
                'callback'          => $sCallbackDateTime,
                'fk_member_contact_outcome_id' => $iContactOutcomeType
            );



            \DAL::Insert($sTablename, $aData);

            echo('Adding note, please wait...');
            echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
            die();
        }


        $v->oMember=$oMember;

        $v->aContactTypes = $this->getContactTypesForDropdown();

        $v->aContactOutcomes = $this->getContactOutcomesForDropdown();

        return $v->output('Views/add_note');
    }

    function add_transaction()
    {
        $v=new \LLView();
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        // Need to use the global security object here in order to get
        // the user currently logged in
        global $oSecurityObject;

        if($_POST){
            /*
            $sTablename = "transactions";
            $aData = array(
                'fk_member_id'      => $iMemberId,
                'fk_contact_type'   => (int) $_POST['contact_type'],
                'datetime'          => date("Y-m-d H:i:s"),
                'fk_user_id'        => (int) $oSecurityObject->getUserID(),
                'details'           => $_POST['details'],
                'customer_message'  => $_POST['customer_message']
            );

            \DAL::Insert($sTablename, $aData);
          */
            echo('Adding transaction, please wait...');
            echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
            die();
        }


        $v->oMember=$oMember;

        return $v->output('Views/add_transaction');
    }

    function amend_alerts()
    {
        $v=new \LLView();
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        $oMember->member_id = $iMemberId;


        if($_POST){

            foreach ($_POST as $sKey=>$sVal)
            {
                if ($sKey != 'action')
                {
                    $aNewAlerts[$sKey] = $sVal;
                }
            }

            $oMember->updateMemberAlerts($aNewAlerts, $iMemberId);

            echo('Amending alert preferences, please wait...');
            echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
            die();
        }

        $sSQL = "SELECT * FROM members_alerts WHERE member_id = $iMemberId";

        $aAlerts=\DAL::executeQuery($sSQL);

        $v->oMember=$oMember;
        $v->aMemberAlerts = $aAlerts[0];

        return $v->output('Views/amend_alerts');
    }

    function callback_list()
    {
         $v=new \LLView();

         $aCallbacksInfo = $this->getCallbackList();
         $v->callbacks = $aCallbacksInfo['callbacks'];
         $v->callback_start = $aCallbacksInfo['callback_start'];

        // Output the view
        return $v->output('Views/callback_list');

    }

    // Function used when emails are sent out in the system
    public static function add_crm_note_auto($aNoteData)
    {
        $sTablename = "members_contact_log";
        $aData = array(
            'fk_member_id'      => $aNoteData['member_id'],
            'fk_contact_type'   => (int) $aNoteData['contact_type'],
            'datetime'          => date("Y-m-d H:i:s"),
            'fk_user_id'        => 841,
            'details'           => $aNoteData['details']
        );

        \DAL::Insert($sTablename, $aData);
    }

    function new_customer()
    {

        $v = new \LLView();

        if($_POST){
            $oMember= new \Member();

            if (!array_key_exists('tandc', $_POST))
            {
                 $v->bAgreeTandC=true;
            }
            else if (!$oMember->checkUsernameAvailable($_POST['email']))
            {
                $v->bEmailExists=true;
            }
            else
            {
                switch ($this->checkDOB())
                {

                    case 1: // Too young to play
                        $v->bTooYoung=true;
                    break;

                    case 2: // Invalid date
                        $v->bInvalidDate=true;
                    break;

                    case 3: // All OK
                        foreach ($_POST as $sKey=>$sVal)
                        {
                            if ($sKey != 'action')
                            {
                                $aNewDetails[$sKey] = $sVal;
                            }
                        }

                        $iMemberID = $oMember->addMemberFull($aNewDetails);

                        echo('Creating new customer, please wait...');
                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?id=' . $iMemberID . '"</script>');
                        die();
                     break;
                }

            }
        }

        $v->aCountries = $this->getCountriesForDropdown();
        $v->aTitles = $this->getTitlesForDropdown();
        $v->aMonths = $this->getMonthNames();
        $v->iStartYear = $this->getYearDropdownStart();
        return $v->output('Views/new_customer');
    }



    function create_order()
    {
     
        // Create view
        $v = new \LLView();
        $bClearCart = false;
        
        // Get stage
        if ($_GET['s'])
        {
            $iStage = filter_input(INPUT_GET, 's') or 0;
            
        }
        else
        {
            $iStage = 1;
            $bClearCart = true;
        }

        // At this point, we need to check especially for whether the first character of S is '6'
        // The URL will have its key/value pairs seperated by pipe symbols, not ampersands
        // due to Payfrex being, well, Payfrex
        // So get the URL, covert pipes to ampersands
        // and redirect again
        if ($iStage[0] == 6)
        {
            $aTargetArray = array();
            $aSArray = explode(";", $iStage);
            
            foreach ($aSArray as $sItem)
            {
                // Check if the k/v delimiter is an equals or pipe
                // and act accordingly
                if (strpos($sItem, "|") === false)
                {
                    list($k, $v) = explode("=", $sItem);
                }
                else
                {
                    list($k, $v) = explode("|", $sItem);
                }
                
                $aTargetArray[$k] = $v;
            }
            
            // Pause for a couple of seconds here, just to let everything catch up
            sleep(2);
            if ($aTargetArray['x'] == 1)
            {
                echo("Cancelling, please wait...");
                echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=4&id=' . $aTargetArray['id'] . '&tr=' . $aTargetArray['tr'] .'"</script>');                
                
            }
            else
            {
                echo("Processing payment, please wait...");
                echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=7&id=' . $aTargetArray['id'] . '&tr=' . $aTargetArray['tr'] .'"</script>');                
            }
            die();            
        }
        
        // Error message string - set to false here
        $sErrorMessage = false;
        $iTransactionID = false;

        // Get transacrtion reference, if present
        // (this will only be set after a Payfrex call)
        // Get stage
        if ($_GET['tr'])
        {
            $sTransRef = filter_input(INPUT_GET, 'tr') or false;

            // Get the response for this transaction
            $aTransactionResponse = $this->getTransactionXML($sTransRef);

            // Now find the error message
            // Will be in the 'master' array if a withdrawal,
            // else in the 'operations' 'operation' array if deposit
            if (array_key_exists('master', $aTransactionResponse))
            {
                // CREDIT
                $sErrorMessage = $aTransactionResponse['master']['message'];
                $iTransactionID = $aTransactionResponse['master']['merchantTransactionId'];
                $fTransactionAmount = $aTransactionResponse['master']['amount'];
            }
            else
            {
                // DEBIT
                $sErrorMessage = $aTransactionResponse['operations']['operation']['message'];
                $iTransactionID = $aTransactionResponse['operations']['operation']['merchantTransactionId'];
                $fTransactionAmount = $aTransactionResponse['operations']['operation']['amount'];
            }


            // Check here if a particular message has been received
            // If so, change the text accordingly.
            if ($this->str_startswith($sErrorMessage, "The transaction has been blocked by"))
            {
                $sErrorMessage = VCMS::get("Payment.AmountTooLowError");
            }


        }

        // Get Member, set ID
        $iMemberId=filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) or 0;
        $oMember=\Member::getById($iMemberId);

        $oMember->member_id = $iMemberId;
        $oMember->iMemberID = $iMemberId;

        // If no stage has been passed, this flag will have been set
        // so empty the cart here.
        if ($bClearCart === true)
        {
            $this->clear_cart();
        }

        switch ($iStage) {

            case 1:  // Initial game selection
                if($_POST)
                {

                    if ($_POST['no_order'] === 'yes')
                    {
                        $this->clear_cart();
                        
                        echo('Finishing process, please wait...');

                        echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');
                        die();
                    }

                    $iProductId = (int) $_POST['product'];

                    $oGameEngine=\GameEngine::getById($iProductId);

                    switch ($oGameEngine->sGameType) {

                        case 'system' :
                            $iType=1;
                        break;

                        case 'syndicate' :
                            $iType = 2;
                        break;

                        case 'instant' :
                            $iType = 3;
                        break;

                        default :
                            $iType=0;
                    }

                    echo('Fetching product, please wait...');

                    echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=2&id=' . $iMemberId . '&p=' .$iProductId. '&t=' . $iType .'"</script>');
                    die();

                }

                $aGameEngines = $this->getGameEngines();
                $v->aProducts = $aGameEngines;

                return $v->output('Views/create_order_1');
            break;


            case 2:  // Game details selection
               $iGameID=filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) or 0;
               $iGameTypeID=filter_input(INPUT_GET, 't', FILTER_VALIDATE_INT) or 0;

               if ($_POST)
               {
                    if ($_POST['add_different'] === 'yes')
                    {
                        echo('Returning to game selection, one moment please...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=1&id=' . $iMemberId . '"</script>');
                        die();
                    }
                    if ($_POST['start_new'] === 'yes')
                    {
                        echo('Starting a new order, please wait...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?id=' . $iMemberId . '"</script>');
                        die();
                    }
                    
                    
                    $oGameData=array();

                    $_POST['gameID'] = $iGameID;

                    // Start building the game data based on type of game
                    switch ($_POST['gameType'])
                    {
                        case 'instant' :
                            //$oGameData['quickDeal']=true;
                            $_POST['quickDeal']=true;
                        break;

                        case 'syndicate' :

                            foreach ($_POST['syndicateId'] as $iSyndicateId)
                            {
                                $_POST['syndicates'][] = array('syndicateId' => $iSyndicateId,
                                                               'shares' => $_POST['shares'],
                                                                'playDuration' => $_POST['playDuration']);
                            }

                            unset($_POST['syndicateId']);

                            if (!$_POST['isSubscription'])
                            {
                               $_POST['isSubscription'] = 0;
                            }

                        break;

                        default :

                            if (!$_POST['isSubscription'])
                            {
                               $_POST['isSubscription'] = 0;
                            }

                            $_POST['quickPlay'] = true;

                    }

                    $_POST['action'] = 'addToCart';

                    $oEngine = new \EngineApi();

                    $aBasketData = $oEngine->basket(array(false));

                    echo('Adding order to basket, please wait...');

                    echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=3&id=' . $iMemberId . '&p=' .$iGameID. '"</script>');
                    die();

               }

               $oGameEngine=\GameEngine::getById($iGameID);

               $aGEChildComponents = $oGameEngine->getChildComponents();

               $v->aGameEngine = $oGameEngine;
               $v->aDrawDays = $aGEChildComponents[0]->aLottoDayNumbers;
               $v->aDayNames = $this->getDayNames();

               switch ($iGameTypeID)
               {
                   // System needs system rules
                   case 1:
                      $v->aSystemOptions = $aGEChildComponents[0]->aSystemRules;
                      return $v->output('Views/create_order_2_system');
                   break;

                   case 2: // Syndicate

                       if (!is_null($oGameEngine->syndicate_products))
                       {
                        $aSynGameEngineIds = explode(',', $oGameEngine->syndicate_products);

                        foreach($aSynGameEngineIds as $gID) {

                            # Get the child game engine
                            $sGameEngine = \GameEngine::getById($gID);

                            # Get all the syndicates
                            $aSyndicates = $sGameEngine->getFrontEndSyndicates();

                            if (!empty($aSyndicates) )
                            {

                                $v->aSyndicates = $aSyndicates;
                            }

                        }
                      }
                      return $v->output('Views/create_order_2_syndicate');
                   break;

                   case 3: /// Instant win
                      return $v->output('Views/create_order_2_instant');
                   break;

                   default: // Classic
                      return $v->output('Views/create_order_2_classic');
                   break;
               }
            break;

            case 3 : // Confirmation of adding order to cart
                if($_POST)
                {
                    if ($_POST['add_next'] === 'yes')
                    {
                        echo('Returning to game selection, one moment please...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=1&id=' . $iMemberId . '"</script>');
                    }
                    if ($_POST['order_pay'] === 'yes')
                    {
                        echo('Redirecting to payment form, please wait...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?s=4&id=' . $iMemberId . '"</script>');
                    }
                    if ($_POST['start_new'] === 'yes')
                    {
                        echo('Starting a new order, please wait...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?id=' . $iMemberId . '"</script>');
                        
                    }
                    die();
                }
                // Get the cart details here
                $oCart = unserialize($_SESSION['cart']);

                $aaa = $oCart->getCartDetails();
                $v->orderitems = $aaa;
                $v->ordertotal = $oCart->getCartTotal();
                return $v->output('Views/create_order_3');

            break;

            case 4:  // Initial payment screen setup, show payment amount options
                $oCart = unserialize($_SESSION['cart']);

                $v->cart = $oCart;
                $v->ordertotal = $oCart->getCartTotal();
                $v->memberid = $iMemberId;

                // Assign message string and transaction ID to view,
                // if they exist
                if ($sErrorMessage !== false)
                {
                    $v->payfrexerror = $sErrorMessage;
                }
                if ($iTransactionID !== false)
                {
                    $v->transactionid = $iTransactionID;
                }

                return $v->output('Views/create_order_4');
            break;

            case 5:  // Payfrex initilisation, not shown directly to user
                $oCart = unserialize($_SESSION['cart']);

                $fAmount = $_POST['amount'];

                if ($fAmount == "amt_inpt")
                {
                    $fAmount = $_POST['amount_input'];
                }

                //echo ("Sending $fAmount to Payfrex.<br/>");

                // Assign payment gateway and amount
                $oCart->assignPaymentGateway(\PaymentGateways::PAYFREX);
                $oCart->oPaymentGateway->assignAmount($fAmount);
                //echo ("Assigned gateway and amount<br/>");

                ob_flush();

                if ($oCart->oPaymentGateway->addTransaction($iMemberId, 1)) {

                    // Response URL from Payfrex
                    $aURL = $oCart->oPaymentGateway->ProcessPaymentCRM($this, $oMember);

                    if(!empty($aURL) && $aURL!='') {
                        if (!headers_sent()) {
                            header('Location: '.$aURL);
                            #var_dump(headers_sent());echo 'URL: '.$aURL.'<br />';
                        } else {
                            echo('<script>document.location="'.$aURL.'"</script>');
                        }
                    }

                    die();
                } else {
                    die ("Problem adding transaction");
                }

            break;

            // Case 6 missed out here - dealt with at the start of the function
            // and is only used on return from Payfrex 
            
            case 7:  // Successful payment
                if($_POST)
                {
                    if ($_POST['new_order'] === 'yes')
                    {
                        echo('Starting a new order, one moment please...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/create_order?id=' . $iMemberId . '"</script>');

                    }
                    if ($_POST['finish'] === 'yes')
                    {
                        echo('Redirecting to customer details form, please wait...');

                        echo('<script>document.location="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=' . $iMemberId . '"</script>');

                    }
                    if ($_POST['new_cust'] === 'yes')
                    {
                        // Clear cart at this stage
                        $this->clear_cart();
                        
                        echo('Redirecting to customer details form, please wait...');

                        echo('<script>document.location="/administration/LL/CRM/CRM/find"</script>');

                    }                    
                    die();
                }
                // Pause here as well, just to let everything catch up
                sleep(2);
                $oCart = unserialize($_SESSION['cart']);
                
                $oCurrency = new \Currency();

                $oCart->commitPurchase($oMember, $oCurrency);
                
                $v->transactionid = $iTransactionID;
                $v->ordernumber = $oCart->oPaymentGateway->getGatewayReference();
                $v->transactionamount = $fTransactionAmount;
                $v->customerbalance = $oMember->getMemberBalance(true);
               
                return $v->output('Views/create_order_6');
            break;
        }

    }
}
