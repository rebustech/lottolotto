<?php
/**
 * members controller. Generates the display for an individual member inluding basic details
 * KYC, contact details, order history, contact history
 * @package LoveLotto
 * @subpackage AdminControllers
 * @author Jonathan Patchett
 */
class members{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        $tabs=new TabbedInterface();
        $oDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oDetailsTab->sIcon='fa-user';

        $oMainDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oMainDetailsTab->sIcon='fa-user';

        // We can now pass an array to the form creator to indicate whether to show fields
        // and make them readonly
        $aDisplayOptions = array('username' => true,
                                 'email'    => true,
                                 'balance'  => true,
                                 'firstname' => true,
                                 'lastname' => true,
                                 'contact'  => true,
                                 'address1' => true,
                                 'address2' => true,
                                 'county_state' => true,
                                 'city'     => true,
                                 'zip'      => true,
                                 'fk_country_id' => true,
                                 'verified' => true,
                                 'join_date' => true,
                                 'is_active' => true,
                                 'fk_affiliate_id' => true,
                                 'email_generaloffers' => true,
                                 'email_lotterydraws' => true,
                                 'dob'       => true,
                                 'title'     => true,
                                 'fk_source_advert_id' => true,
                                 'monthly_deposit_limit' => true,
                                 'is_suspended' => true,
                                 'suspension_start' => true,
                                 'suspension_end' => true,
                                 'suspension_lifted' => true);

        // Check here if this is being accessed from the CRM search
        // if so, we don't want to display the form
        $bAddForm = true;
        if ($_GET['a'] == 'view' || !array_key_exists('a', $_GET))
        {
            $bAddForm = false;
        }

        $oMainDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID, $bAddForm, $aDisplayOptions);
        //$oDetailstabs->aItems['main']=$oMainDetailsTab;
        $tabs->aItems['details']=$oMainDetailsTab;

         // We can now pass an array to the form creator to indicate whether to show fields
        // and make them readonly
        $aDisplayOptions2 = array('jackpot_6aus49'       => true,
                                 'jackpot_eurojackpot'  => true,
                                 'jackpot_euromillions' => true,
                                 'jackpot_megamillions' => true,
                                 'jackpot_powerball'   => true,
                                 'result_6aus49'       => true,
                                 'result_eurojackpot'  => true,
                                 'result_euromillions' => true,
                                 'result_megamillions' => true,
                                 'result_powerball'    => true,
                                 'max_news'            => true,
                                 'max_jackpots'        => true,
                                 'max_jackpot_over'    => true
);

        // Check here if this is being accessed from the CRM search
        // if so, we don't want to display the form
        $bAddForm = true;
        if ($_GET['a'] == 'view' || !array_key_exists('a', $_GET))
        {
            $bAddForm = false;
        }

        // Create a tab to show email alerts requested
        $oAlertsTab=new TabbedInterface(lang::get('member_email_alerts'));
        //$oAlertsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm('members_alerts', $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID, $bAddForm, $aDisplayOptions2);
        $oAlertsTab->sIcon='fa-envelope';
        $tabs->aItems['alerts']=$oAlertsTab;


        //Create a tab to show rights assigned to this role
        $oKYCTab=new TabbedInterface(lang::get('member_fraud'));
        $oKYCTab->sIcon='fa-gavel';
        $oKYCTab->aItems[]=GenericTableAdmin::createGenericDetailsForm('members_kyc', $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID, false);
        $tabs->aItems['kyc']=$oKYCTab;

        //Create a tab to show rights assigned to this role
        /*
        $oContactsTab=new TabbedInterface(lang::get('member_contacts'));
        $oContactsTab->sIcon='fa-envelope';
        $aContactsData=DAL::executeQuery('SELECT * FROM member_contacts WHERE fk_member_id='.intval($iId));
        $oContactsTable=new TableControl($aContactsData);
        $oContactsTable->getFieldsFromTable('member_contacts');
        //$oTransactionsTable->keepFields('admin_users','username,firstname,lastname,email,isactive');
        $oContactsTab->AddControl(new ToolbarButtonControl(lang::get('add_contact_method'),'#'));
        $oContactsTab->AddControl($oContactsTable);
        $oDetailstabs->aItems['contacts']=$oContactsTab;
        $oDetailsTab->aItems['details2']=$oDetailstabs;
        $tabs->aItems['details']=$oDetailsTab;

*/

        //Create a tab to show assigned users
        $oBookingsTab=new TabbedInterface(lang::get('member_bookings'));
        $oBookingsTab->sIcon='fa-rocket';
        $aBookingsData=DAL::executeQuery('SELECT * FROM bookings WHERE fk_member_id='.intval($iId));
        $oBookingsTable=new TableControl($aBookingsData);
        $oBookingsTable->getFieldsFromTable('bookings');
        $oBookingsTable->addAction(new TableRowActionControl('id','<i class="fa fa-search"></i>','/administration/generic-details.php?a=view&tablename=bookings&id={booking_id}'));
        //$oTransactionsTable->keepFields('admin_users','username,firstname,lastname,email,isactive');
        //$oBookingsTab->AddControl(new ToolbarButtonControl(lang::get('add_booking'),'#'));
        $oBookingsTab->AddControl($oBookingsTable);

        $tabs->aItems['bookings']=$oBookingsTab;

        $oTransactionsTab=new TabbedInterface(lang::get('member_transactions'));
        $oTransactionsTab->sIcon='fa-credit-card';
        $aTransactionsData=DAL::executeQuery('SELECT t.*,g.comment as fk_gateway_id FROM transactions t INNER JOIN gateways g ON g.gateway_id=fk_gateway_id WHERE fk_member_id='.intval($iId));
        $oTransactionsTable=new TableControl($aTransactionsData);
        $oTransactionsTable->getFieldsFromTable('transactions');
        $oTransactionsTable->keepFields('transactions','id,bookingreference,amount,status,transaction_date,confirmed,fk_gateway_id');
        $oTransactionsTable->addAction(new TableRowActionControl('id','<i class="fa fa-search"></i>','/administration/generic-details.php?a=view&tablename=transactions&id={transaction_id}'));
        $oTransactionsTab->AddControl(new ToolbarButtonControl(lang::get('add_transaction'),'#'));
        $oTransactionsTab->AddControl($oTransactionsTable);
        $tabs->aItems['transactions']=$oTransactionsTab;


        //Create a tab to show rights assigned to this role
        $oContactLogTab=new TabbedInterface(lang::get('member_activity_log'));
        $oContactLogTab->sIcon='fa-phone';
        $aContactLogData=DAL::executeQuery('SELECT * FROM vw_members_contact_log WHERE fk_member_id='.intval($iId));
        $oContactLogTable=new TableControl($aContactLogData);
        $oContactLogTable->getFieldsFromTable('contact_types');
        $oContactLogTable->getFieldsFromTable('members_contact_log');
        $oContactLogTable->getFieldsFromTable('members_contact_outcomes');
        $oContactLogTable->getFieldsFromTable('admin_users');

        $oContactLogTable->keepFields('contact_types','id,icon,direction_icon,name');
        $oContactLogTable->keepFields('members_contact_log','datetime,details,customer_message,callback');
        $oContactLogTable->keepFields('members_contact_outcomes','description');
        $oContactLogTable->keepFields('admin_users','username');
        $oContactLogTable->addAction(new TableRowActionControl('id','<i class="fa fa-search"></i>','/administration/generic-details.php?a=view&tablename=members_contact_log&id={id}'));
        $oContactLogTable->removeField('members_contact_log','fk_user_id');
        $oContactLogTable->removeField('members_contact_log','fk_member_id');
        $oContactLogTable->removeField('members_contact_log','fk_contact_type');
        $oContactLogTable->removeField('members_contact_log','id');

        $oContactLogTable->renderAs('contact_types','icon','icon');
        $oContactLogTable->renderAs('contact_types','direction_icon','icon');
        $oContactLogTable->renderAs('members_contact_log','details','varchar');
        $oContactLogTable->renderAs('members_contact_log','customer_message','varchar');


        $oContactLogTab->AddControl(new ToolbarButtonControl(lang::get('add_activity_log'),'LL/CRM/CRM/add_note?id='.intval($iId)));
        $oContactLogTab->AddControl($oContactLogTable);
        $tabs->aItems['contact']=$oContactLogTab;

        //Create a tab to show audit log
        $oLogTab=new TabbedInterface(lang::get('audit_log'));
        $oLogTab->sIcon='fa-th-list';
        $aItemsData=DAL::executeQuery('SELECT * FROM audit_log a INNER JOIN admin_users u ON u.user_id=a.user_id WHERE (model=\'members\' OR model=\'Member\') AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');
        $oItemsTable=new TableControl($aItemsData);
        $oItemsTable->getFieldsFromTable('admin_users');
        $oItemsTable->getFieldsFromTable('audit_log');
        //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
        $oItemsTable->keepFields('audit_log','datetime,code,message');
        $oItemsTable->keepFields('admin_users','email');
        $oLogTab->AddControl($oItemsTable);
        $tabs->aItems['rights']=$oLogTab;


        //Create a tab to show rights assigned to this role
        $oActionsTab=new TabbedInterface(lang::get('actions'));
        $oActionsTab->sIcon='fa-dot-circle-o';

        $oActionsTab->AddControl(new HtmlTagControl('h2','Customer'));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('amend_address'),'LL/CRM/CRM/edit_address?id='.intval($iId)));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('amend_DOB'),'LL/CRM/CRM/edit_dob?id='.intval($iId)));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('amend_contact'),'LL/CRM/CRM/edit_contact?id='.intval($iId)));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('amend_alerts'),'LL/CRM/CRM/amend_alerts?id='.intval($iId)));
        $oActionsTab->AddControl(new HtmlTagControl('br',''));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('change_password'),'LL/CRM/CRM/change_password?id='.intval($iId)));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('send_email'),'LL/CRM/CRM/send_email?id='.intval($iId)));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('add_note'),'LL/CRM/CRM/add_note?id='.intval($iId)));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('amend_limits'),'LL/CRM/CRM/amend_limits?id='.intval($iId)));

        $oActionsTab->AddControl(new HtmlTagControl('h2','KYC Checks'));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('upload_documents'),'#'));

        $oActionsTab->AddControl(new HtmlTagControl('h2','Financial'));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('add_credit_debit'),'#'));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('edit_discount'),'LL/CRM/CRM/edit_discount?id='.intval($iId)));

        $oActionsTab->AddControl(new HtmlTagControl('h2','Orders'));
        $oActionsTab->AddControl(new ToolbarButtonControl(lang::get('create_order'),'LL/CRM/CRM/create_order?id='.intval($iId)));

        $tabs->aItems['actions']=$oActionsTab;



        $aItems[]=$tabs->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }


    function getContactTypesForDropdown()
    {
        $aContactTypes = DAL::executeQuery('SELECT * FROM contact_types ORDER BY id ASC');

        return $aContactTypes;
    }

    function getCountriesForDropdown($iLanguageID = 1)
    {
        $iLanguageID = (int) $iLanguageID;

        $aCountries = DAL::executeQuery("SELECT * FROM countries_lang WHERE fk_language_id = {$iLanguageID} ORDER BY title ASC");

        return $aCountries;
    }

    function getContactOutcomesForDropdown()
    {
        $aContactTypes = DAL::executeQuery('SELECT * FROM members_contact_outcomes ORDER BY id ASC');

        return $aContactTypes;
    }


    function getCallbackList()
    {
        $tsCallbackStart = date("Y-m-d 00:00:00");
        $tsCallbackEnd = date("Y-m-d 23:59:59");

        $sSQL = "SELECT m.member_id, m.firstname, m.lastname, m.contact, mcl.callback
                 FROM members_contact_log mcl
                 LEFT JOIN members m on mcl.fk_member_id = m.member_id
                 WHERE mcl.callback BETWEEN '{$tsCallbackStart}' AND '{$tsCallbackEnd}'
                 ORDER BY mcl.callback ASC";

        $aCallbacks = DAL::executeQuery($sSQL);

        return array('callback_start' => date('D d M Y', strtotime($tsCallbackStart)),
                     'callback_end'   => $tsCallbackEnd,
                     'callbacks' => $aCallbacks);
    }
    
    
    // Array of titles for dropdown
    function getTitlesForDropdown()
    {
        // Titles for dropdown, organised by language
        return array("English" => array("Mr" => "Mr",
                                        "Mrs" => "Mrs",
                                        "Miss" => "Miss",
                                        "Ms" => "Ms",
                                        "Dr" => "Dr",
                                        "Prof" => "Prof"),
                     "German" => array("Herr" => "Herr",
                                       "Frau" => "Frau",
                                       "Fraulein" => "Fraulein"));
    }

    function getGameEngines()
    {
        // Ignore instant wins at this stage
        $sSQL = "SELECT ge.id, ge.name, ge.base_price, gec.category_name
                FROM game_engines ge
                LEFT JOIN game_engine_categories gec on ge.fk_category_id = gec.id";

        $aReturn = array();
        $aRows = \DAL::executeQuery($sSQL);

        foreach ($aRows as $aRow) {

            if ($aRow['category_name'] != 'Instant Wins')
            {
                $iGEID = $aRow['id'];

                $aReturn[$iGEID] = array('name' => $aRow['name'],
                                         'base_price' => $aRow['base_price'],
                                         'category'   => $aRow['category_name']);
            }

        }

        return $aReturn;
    }

    function getDayNames()
    {   
        return array(\lang::get('d_sun'),
                     \lang::get('d_mon'),
                     \lang::get('d_tue'),
                     \lang::get('d_wed'),
                     \lang::get('d_thu'),
                     \lang::get('d_fri'),
                     \lang::get('d_sat'));
    }
    
    function getMonthNames()
    {
        return array(1=>\lang::get('m_jan'),
                        \lang::get('m_feb'),
                        \lang::get('m_mar'),
                        \lang::get('m_apr'),
                        \lang::get('m_may'),
                        \lang::get('m_jun'),
                        \lang::get('m_jul'),
                        \lang::get('m_aug'),
                        \lang::get('m_sep'),
                        \lang::get('m_oct'),
                        \lang::get('m_nov'),
                        \lang::get('m_dec'));
    }
    
    function getYearDropdownStart()
    {
        // Return a year that's 18 years earlier than the current one
        $iYear = date("Y");
        return $iYear-18;
    }
    
    // Checks whether a date of birth is valid 
    // both for age constraints and valdity of date
    function checkDOB()
    {
        // Format dates to apporopriate format
        $iDay = sprintf('%02d', $_POST['dob_d']);
        $iMonth = sprintf('%02d', $_POST['dob_m']);
        $iYear = sprintf('%04d', $_POST['dob_y']);

        // Check date is valid
        if (checkdate($iMonth, $iDay, $iYear)) {

            // Now check customer is over 18 years old
            $sNewDOB  = $iYear . "-" . $iMonth . "-" . $iDay;

            // Start from midnight of their date of birth
            $iTsDOB = strtotime($sNewDOB . " 00:00:01");

            // 18 years from the current date
            $iTsMinValidDOB = strtotime("-18 years");

            // If their date of birth is less than the date 18 years ago, they're kosher
            if ($iTsDOB < $iTsMinValidDOB) {
               return 3;
            }
            else
            {
               // Not old enough
               return 1;
            }
        }
        else
        {   
            // Date is not valid format
            return 2;
        }
    }
    
    // These next three functions have been copied over from MemberAreaPage.class.php
    // in order to provide functionality for Payfrex error messages
    
    /**
     * Retrieve the XML from this transaction as an array
     * @param integer $iTransactionID transaction ID
     *
     * @return array if found, else null
     */
    function getTransactionXML($sTransactionIDStr)
    {

        // Remove the 2 extra characters at the beginning and end
        $temp1 = substr($sTransactionIDStr, 2);
        $temp2 = substr($temp1, 0, -2);


        // Find this transaction's API response
        $sSQL = "SELECT api_response FROM transactions WHERE transaction_id = " . (int) base64_decode($temp2);
        $sXML = DAL::executeGetOne($sSQL);

        // Only do the next bit if we have XML
        if ($sXML != "")
        {
            // Get XML into array
            $oXML = simplexml_load_string($sXML);
            $sJSON = json_encode($oXML);
            $aParsedXML = json_decode($sJSON,TRUE);

            // Need to get the optional params here
            $aOptionalParams = $aParsedXML['optionalTransactionParams']['entry'];

            if (is_array($aOptionalParams))
            {
                foreach ($aOptionalParams as $aParams)
                {
                    $sKey = $aParams['key'];
                    $aParsedXML['fromPayfrex'][$sKey] = $aParams['value'];
                }
            }

            return $aParsedXML;
        }

        return null;
    }
    
    /**
     * Check if a particular error message fro Payfrex begins with certain text
     *
     * @param string $source source string
     * @param string $prefix search terms
     * @return boolean
     */
    function str_startswith($source, $prefix)
    {
        return strncmp($source, $prefix, strlen($prefix)) == 0;
    }
    
    
    /**
     * Clear cart
     */
    function clear_cart()
    {

        // Copied from Lewis's engine code
        
        // Re-open the session
        ini_set('session.use_only_cookies', false);
        ini_set('session.use_cookies', false);
        ini_set('session.use_trans_sid', false);
        ini_set('session.cache_limiter', null);

        // Start session
        session_start();
        
        // Unset cart
        unset($_SESSION['cart']);
    }
}
