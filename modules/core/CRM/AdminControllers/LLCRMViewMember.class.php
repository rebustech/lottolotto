<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LL\CRM;

class ViewMember extends \AdminController{

    function ViewMember(){
        $sFileName = basename($_SERVER['PHP_SELF']);

        $tabs=new TabbedInterface();
        $oDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oDetailsTab->sIcon='fa-user';

        $oMainDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oMainDetailsTab->sIcon='fa-user';
        $oMainDetailsTab->aItems[]=GenericTableAdmin::createGenericDetailsPage($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
        //$oDetailstabs->aItems['main']=$oMainDetailsTab;
        $tabs->aItems['details']=$oMainDetailsTab;

        //Create a tab to show rights assigned to this role
        $oKYCTab=new TabbedInterface(lang::get('member_fraud'));
        $oKYCTab->sIcon='fa-gavel';
        $oKYCTab->aItems[]=GenericTableAdmin::createGenericDetailsForm('members_kyc', $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
        $tabs->aItems['kyc']=$oKYCTab;

        //Create a tab to show rights assigned to this role
        /*
        $oContactsTab=new TabbedInterface(lang::get('member_contacts'));
        $oContactsTab->sIcon='fa-envelope';
        $aContactsData=DAL::executeQuery('SELECT * FROM member_contacts WHERE fk_member_id='.intval($iId));
        $oContactsTable=new TableControl($aContactsData);
        $oContactsTable->getFieldsFromTable('member_contacts');
        //$oTransactionsTable->keepFields('admin_users','username,firstname,lastname,email,isactive');
        $oContactsTab->AddControl(new ToolbarButtonControl(lang::get('add_contact_method'),'#'));
        $oContactsTab->AddControl($oContactsTable);
        $oDetailstabs->aItems['contacts']=$oContactsTab;
        $oDetailsTab->aItems['details2']=$oDetailstabs;
        $tabs->aItems['details']=$oDetailsTab;

*/

        //Create a tab to show assigned users
        $oBookingsTab=new TabbedInterface(lang::get('member_bookings'));
        $oBookingsTab->sIcon='fa-rocket';
        $aBookingsData=DAL::executeQuery('SELECT * FROM bookings WHERE fk_member_id='.intval($iId));
        $oBookingsTable=new TableControl($aBookingsData);
        $oBookingsTable->getFieldsFromTable('bookings');
        //$oTransactionsTable->keepFields('admin_users','username,firstname,lastname,email,isactive');
        $oBookingsTab->AddControl(new ToolbarButtonControl(lang::get('add_booking'),'#'));
        $oBookingsTab->AddControl($oBookingsTable);

        $tabs->aItems['bookings']=$oBookingsTab;

        $oTransactionsTab=new TabbedInterface(lang::get('member_transactions'));
        $oTransactionsTab->sIcon='fa-credit-card';
        $aTransactionsData=DAL::executeQuery('SELECT * FROM transactions WHERE fk_member_id='.intval($iId));
        $oTransactionsTable=new TableControl($aTransactionsData);
        $oTransactionsTable->getFieldsFromTable('transactions');
        $oTransactionsTable->keepFields('transactions','id,gatewayreference,bookingreference,amount,status,transaction_date,confirmed');
        $oTransactionsTab->AddControl(new ToolbarButtonControl(lang::get('add_transaction'),'#'));
        $oTransactionsTab->AddControl($oTransactionsTable);
        $tabs->aItems['transactions']=$oTransactionsTab;


        //Create a tab to show rights assigned to this role
        $oContactLogTab=new TabbedInterface(lang::get('member_contact_log'));
        $oContactLogTab->sIcon='fa-phone';
        $aContactLogData=DAL::executeQuery('SELECT * FROM vw_members_contact_log WHERE fk_member_id='.intval($iId));
        $oContactLogTable=new TableControl($aContactLogData);
        $oContactLogTable->getFieldsFromTable('contact_types');
        $oContactLogTable->getFieldsFromTable('members_contact_log');
        $oContactLogTable->keepFields('contact_types','icon,direction_icon,name');
        $oContactLogTable->keepFields('members_contact_log','datetime,subject');
        $oContactLogTable->renderAs('contact_types','icon','icon');
        $oContactLogTable->renderAs('contact_types','direction_icon','icon');
        $oContactLogTab->AddControl(new ToolbarButtonControl(lang::get('add_transaction'),'#'));
        $oContactLogTab->AddControl($oContactLogTable);
        $tabs->aItems['contact']=$oContactLogTab;

        //Create a tab to show rights assigned to this role
        $oLogTab=new TabbedInterface(lang::get('audit_log'));
        $oLogTab->sIcon='fa-th-list';
        $tabs->aItems['rights']=$oLogTab;


        //Create a tab to show rights assigned to this role
        $oActionsTab=new TabbedInterface(lang::get('actions'));
        $oActionsTab->sIcon='fa-dot-circle-o';
        $tabs->aItems['actions']=$oActionsTab;

        $aItems[]=$tabs->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;

    }

}