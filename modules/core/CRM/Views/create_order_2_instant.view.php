<h2>Creating Order - Stage 2</h2>

<h4>Game Name: <?=$aGameEngine->name;?> (Instant Win/Quick Pick)</h4>
<h4>Base Price: <?=$aGameEngine->base_price;?></h4>

Press select to confirm order.

<form method="POST" action="">
    <input type="hidden" name="gameID" id="field_gameID" value="<?=$aGameEngine->id;?>" />
    <input type="hidden" name="gameType" id="field_gameType" value="<?=$aGameEngine->sGameType;?>" />
    <input type="submit" name="action" value="Add to order"> Add this game to the order
</form>
<form method="POST" action="">
    <input type="hidden" name="add_different" value="yes" />
    <input type="submit" name="action" value="Add a different game" />
    Cancel this and add a different game to the order
</form>
<form method="POST" action="">
    <input type="hidden" name="start_new" value="yes" />
    <input type="submit" name="action" value="Clear and start over" />
    Clear this order and start a new one
</form>