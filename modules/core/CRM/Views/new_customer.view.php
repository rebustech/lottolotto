<h2>Create New Customer</h2>
<?php if($this->bEmailExists) :?>
<h3>The requested email address <b><?=$_POST['email'];?></b> is already in use </h3>
<?php
    endif;
    if($this->bAgreeTandC) :
?>
<h3>The customer must agree to the terms and conditions before continuing</h3>
<?php 
    endif; 
    if($this->bInvalidDate) : 
?>
<h3>Invalid date of birth entered.</h3>
<?php
    endif;
    
    if($this->bTooYoung) : 
?>
<h3>Customer is not eligible to play (too young)</h3>
<?php endif; ?>


<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="short">
            <span>Title</span>
            <div>
                <select name="title" id="field_title" class="halfsize" style="display:inline-block;">
                 <?php foreach ($this->aTitles as $title_group=>$aTitles) : ?>
                    <optgroup label="<?=$title_group;?>">
                    <?php foreach ($aTitles as $title_id=>$title_name) : ?>
                        <option value="<?=$title_id;?>"><?=$title_name;?></option>
                    <?php endforeach; ?>
                    </optgroup>
                 <?php endforeach; ?>
                </select>
            </div>
        </label>
        <label class="long">
            <span>First Name</span>
            <div><input type="text" name="firstname" value="<?=$_POST['firstname']?>"/></div>
        </label>
        <label class="long">
            <span>Last Name</span>
            <div><input type="text" name="lastname" value="<?=$_POST['lastname']?>"/></div>
        </label>
        <label class="short">
            <span>Gender</span>
            <div>
                <select name="gender" id="field_gender" class="halfsize" style="display:inline-block;">
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                </select>
            </div>
        </label>
        <label class="long">
            <span>Email</span>
            <div><input type="text" name="email" value="<?=$_POST['email']?>"/></div>
        </label>
        <label class="long">
            <span>Date of birth</span>

            <select name="dob_d" id="field_dob_d" class="short number" style="display:inline; width: 10%;">
            <?php for ($d=1; $d<=31; $d++) : ?>
                <option value="<?=$d;?>"<?=($d==$this->aDOB[2]?" selected='selected'":"");?>><?=$d;?></option>
            <?php endfor; ?>
            </select>
            <select name="dob_m" id="field_dob_m" class="short" style="display:inline; width: 10%;">
            <?php foreach ($this->aMonths as $m=>$text) : ?>
                <option value="<?=$m;?>"<?=($m==$this->aDOB[1]?" selected='selected'":"");?>><?=$text;?></option>
            <?php endforeach; ?>
            </select>
            <select name="dob_y" id="field_dob_y" class="short number" style="display:inline; width: 10%;">
            <?php for ($y=$this->iStartYear; $y>=$this->iStartYear-100; $y--) : ?>
                <option value="<?=$y;?>"<?=($y==$this->aDOB[0]?" selected='selected'":"");?>><?=$y;?></option>
            <?php endfor; ?>
            </select>
        </label>
        <p>&nbsp;</p>
        <label class="halfsize number">
            <span>Country</span>
            <div>
                <select name="fk_country_id" id="field_fk_country_id" class="halfsize" style="display:inline-block;">
                 <?php foreach ($this->aCountries as $aCountry) : ?>
                    <option value="<?=$aCountry['fk_country_id'];?>"<?=($aCountry['fk_country_id']==$this->oMember->fk_country_id?" selected='selected'":"");?>><?=$aCountry['title'];?></option>
                 <?php endforeach; ?>
                </select>
            </div>
        </label>
        <label class="long">
            <span>Address 1</span>
            <div><input type="text" name="address1" id="field_address1" class="long" value="<?=$_POST['address1']?>"></div>
        </label>
        <label class="long">
            <span>Address 2</span>
            <div><input type="text" name="address2" id="field_address2" class="long" value="<?=$_POST['address2']?>"></div>
        </label>
        <label class="long">
            <span>City</span>
            <div><input type="text" name="city" id="field_city" class="long" value="<?=$_POST['city']?>"></div>
        </label>
        <label class="long">
            <span>County/State</span>
            <div><input type="text" name="county_state" id="field_county_state" class="long" value="<?=$_POST['county']?>"></div>
        </label>
        <label class="long">
            <span>ZIP</span>
            <div><input type="text" name="zip" id="field_zip" class="long" value="<?=$_POST['zip']?>"></div>
        </label>
        <label class="long">
            <span>Mobile Number</span>
            <div><input type="text" name="contact" id="field_contact" class="long" value="<?=$_POST['contact']?>"></div>
        </label>
        <label class="long">
            <span>Agree to Ts and Cs</span>
            <div><input type="checkbox" name="tandc" id="field_tandc" class="long" value="y"></div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Create">
</form>
