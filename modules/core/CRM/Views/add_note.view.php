<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Add Note</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Contact Type</span>
            <div>                
                <select name="contact_type" id="field_contact_type">
                 <?php foreach ($aContactTypes as $aContactType) : ?>
                    <option value="<?=$aContactType['id'];?>"><?=$aContactType['name'];?></option>
                 <?php endforeach; ?>
                </select>
            </div>
        </label>
        <label class="long">
            <span>Details</span>
            <div><textarea name="details" id="field_details" class="long" rows="4" cols="80"></textarea></div>
        </label>
        <label class="long">
            <span>Customer Message</span>
            <div><textarea name="customer_message" id="field_customer_message" class="long" rows="4" cols="80"></textarea></div>
        </label>
        <br/><br/>
        <div><b>For outbound calls, record outcome here.</b></div>
        <label class="long">
            <span>Contact Outcome</span>
            <div>                
                <select name="contact_outcome_type" id="field_contact_outcome_type">
                 <?php foreach ($aContactOutcomes as $aContactOutcome) : ?>
                    <option value="<?=$aContactOutcome['id'];?>"><?=$aContactOutcome['description'];?></option>
                 <?php endforeach; ?>
                </select>
                (leave as N/A if not an outbound call)
            </div>
        </label>
        <label class="long">
            <span>Callback Required At</span>
            <div><input name="callback_required" id="field_callback_required" class="datetimepicker" />(leave as blank if not an outbound call)</div>
        </label>      
    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>