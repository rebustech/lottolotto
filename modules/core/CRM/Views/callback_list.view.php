<h2>Callback List</h2>
<p>Showing all callbacks for <?=$callback_start;?></p>
<table class="datatable">
    <thead>
        <tr>
            <th>Customer ID</th>
            <th>Customer Name</th>
            <th>Contact Number</th>
            <th>Callback Time</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($callbacks as $aCallback) : ?>
        <tr>
            <td><?=$aCallback['member_id'];?></td>
            <td><?=$aCallback['firstname'] . " " . $aCallback['lastname'];?></td>
            <td><?=$aCallback['contact'];?></td>
            <td><?=$aCallback['callback'];?></td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
