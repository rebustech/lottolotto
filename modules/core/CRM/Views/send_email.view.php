<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Send Email (<?=$this->oMember->email;?>)</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Email Subject</span>
            <div><input type="text" name="mail_subject" id="field_mail_subject" class="long" value=""></div>
        </label>
        <label class="long">
            <span>Email Body</span>
            <div><textarea name="mail_body" id="field_mail_body" class="long" rows="10" cols="80"></textarea></div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Send Email" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>