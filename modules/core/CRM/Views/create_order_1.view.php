<h2>Creating Order - Stage 1</h2>
<h4>Select product to offer customer</h4>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <div>
                <select name="product" id="field_product" class="halfsize" style="display:inline-block;">
                 <?php foreach ($this->aProducts as $id=>$info) : ?>
                    <option value="<?=$id;?>"><?=$info['name'];?> (base price: <?=$info['base_price'];?>, category <?=$info['category'];?>)</option>
                 <?php endforeach; ?>
                </select>            
            </div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Select" /> Select this game
</form>
<hr/>
<form method="POST" action="">
    <input type="hidden" name="no_order" value="yes" />
    <input type="submit" name="action" value="Do Not Create New Order" />
    Cancel order generation, return to customer details screen.
</form>
