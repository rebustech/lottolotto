<!--script type="text/javascript" src="/administration/scripts/NumberPicker.js"></script-->
<script type="text/javascript" src="/administration/scripts/MaxLottoAdmin.js"></script>
<script type="text/javascript" src="/administration/scripts/app-checkout.js"></script>
<h2>Creating Order - Stage 4</h2>
<h3>Payment</h3>
<?php


// if there is nothing in the cart, set flag to hide "order amount" button
if ($this->ordertotal <= 0)
{
    $bShowOrderAmount = false;
}
else
{
    $bShowOrderAmount = true;
    $fDefaultAmount =  number_format($this->ordertotal,2);

}
$stepNumber = 1;
?>

<div class="row">
    <div class="small-12 columns">
        <div class="sidebarBody">
            <div class="lightContainer">
                <div class="row topPad">
                    <!-- Open: large-7 -->
                    <div class="small-12 medium-9 large-9 columns">
                    <!-- ORDER TICKET -->
<?php
# Check oCart state
if(isset($this->cart)) {
        $items = $this->cart->getCartDetails();
        $cartTotal = $this->ordertotal;
} else {
        $cartTotal = 0;
}

if($cartTotal>0) :
?>
                        <div id="mSlip">
                            <div class="ticketOrder">
                                <div class="row">
                                    <h1><i class="fa fa-shopping-cart"></i> <?=VCMS::get('Payment.MyLotteryOrder '); ?></h1>
                                </div>
                                <hr />

<?php

# Found any items?
if(isset($items) && !empty($items)) {

    # At least one ticket
    if($items['total'] > 0 || $items['items'] > 0) {
        $itemcount = 0;
        
        foreach($items as $k => $v) {

            # Skip the null stubs
            if(!is_numeric($k) && ($k == 'total' || $k == 'items' || $k == 'totalFormatted')) {
                    continue;
            }

            # Invalid ticket
            if(empty($v['data'])) {
                    continue;
            }

            # Get each day
            $v['data']['days'] = explode(',', $v['data']['day']);
            $v['data']['daysText'] = array();

            # Turn each day (numeric) => day (text)
            foreach($v['data']['days'] as $date) {
                    $v['data']['daysText'][$date] = date('D', strtotime("Sunday +{$date} days"));
            }

            $daysOfDraw = implode(' / ', $v['data']['daysText']);

            # Work from title
            if(empty($v['data']['gameType'])) {
                    if(strstr($v['data']['gameTitle'], 'Classic') !== false) {
                            $v['data']['gameType'] = 'classic';
                    } elseif(strstr($v['data']['gameTitle'], 'Group') !== false) {
                            $v['data']['gameType'] = 'group';
                    } elseif(strstr($v['data']['gameTitle'], 'System') !== false) {
                            $v['data']['gameType'] = 'system';
                    }
            }

            if(count($v['data']['boards']) != 1) {
                    $text = VCMS::get('Cart.Item[Type]Play[Boards]Plural', array('Type' => VCMS::get('Cart.'.$v['data']['gameType']), 'Lines' => count($v['data']['boards'])));
            } else {
                    $text = VCMS::get('Cart.Item[Type]Play[Boards]Single', array('Type' => VCMS::get('Cart.'.$v['data']['gameType']), 'Lines' => count($v['data']['boards'])));
            }
            
            
?>
            <div class="row orderLine" id="cartItemSitu-<?=$k;?>">
                <!--div class="small-2 medium-1 text-center columns">
                        <img src="/static/img/game-icons-ticket/'.strtolower($v['data']['cssLogoSmall']).'.png" alt="'.$v['data']['gameTitle'].'" />
                </div-->
                <div class="small-10 medium-7 columns">
                    <h2><?=++$itemcount;?>) <?=$v['data']['gameTitle'];?> - <?=(($v['data']['gameType'] != 'quickDeal') ? $text : '');?></h2>
                    <p><?=$daysOfDraw;?>. <?=VCMS::get('Cart.Draws');?>  - <?=VCMS::get('Cart.FirstDraw').' '.$v['data']['drawDate'];?> - <?=$v['data']['playDuration'].' '.($v['data']['playDuration'] != 1 ? VCMS::get('General.Weeks') : VCMS::get('General.Week'));?></p>
                </div>
                <div class="small-10 small-offset-2 medium-offset-0 medium-4 large-text-right columns">
                    <p class="price">Price: <?=$v['data']['priceFormatted'];?></p>
                    <p><a class="removeCartLink" href="javascript:NumberPicker.GameEngine.removeFromBasket('<?=$k;?>');"><?=VCMS::get('Cart.Remove');?></a></p>
                </div>
            </div>

            <hr />
<?php
        }

    }

}

?>
                                <div class="row orderLine">
                                    <div class="small-3 medium-1 columns">
                                        <h2><?=VCMS::get('General.Total'); ?>:  &euro;<?=number_format_locale($cartTotal, 2); ?> (<?=$itemcount;?> item<?=($itemcount==1?'':'s');?>)</h2>
                                    </div>
                                    <!--div class="small-9 medium-11 text-right end columns">
                                            <p class="price" id="cartSituTotal">&euro;<?=number_format_locale($cartTotal, 2); ?></p>
                                    </div-->
                                </div>
                                    <?php
                                    /*
                                    <div class="row">
                                            <div class="small-3 columns">
                                                    <em>Do you have a voucher code?</em>
                                            </div>
                                            <div class="small-9 end columns">
                                                    <input type="text" name="amount" style="display:inline-block;width:150px" value="<?=$fDefaultAmount?>"/>
                                            </div>
                                    </div>
                                    */
                                    ?>
                            </div>
                        </div>

                                        <?php
                                            endif;
                                        ?>

                                        <!-- Error message from Payfrex only displayed if we have one -->
                <?php if (isset($payfrexerror)) { ?>

                        <div class="addDepositContainer depositPrint">
                            <div class="row">
                                <div class="small-12 text-center columns">
                                    <?=VCMS::get('Payments.ProblemWithTransaction')?>
                                    <br/>
                                    <?=VCMS::get('Payments.FollowingMessageReceived')?>
                                    <br/><br/>
                                    <i><?=$payfrexerror;?></i>
                                    <br/>
                                    (<?=VCMS::get('Payments.TransactionIDIs')?>: <?=$transactionid;?>)
                                    <br/><br/>
                                    <?=VCMS::get('Payments.ContactCustServ')?>
                                </div>
                            </div>
                        </div>
                <?php } ?>
                        <!--<div class="thanksHead">
                          <h2><?=(!empty($_SESSION['is_first_order']) ? '<span>STEP '.($stepNumber++).'. Your Details <i class="fa fa-check"></i></span> <em><i class="fa fa-angle-right"></i></em>' : ''); ?>
                                STEP <?=($stepNumber++); ?>. Payment <em><i class="fa fa-angle-right"></i></em>
                                <span>STEP <?=$stepNumber++;?>. Confirmation<span>  <i class="fa fa-lock"></i></h2>
                        </div>-->

                        <div class="lightContainerInner">
						<?php

						# Any content area
						if(isset($content)) {
							echo '<div class="content">'.$content.'</div>';
						}
?>

                            <!-- Select your payment -->
                            <!--div class="row main">
                                <div class="small-12 medium-7 columns">
                                    <h1><?=($cartTotal>0)?VCMS::get('Payment.DepositFundsToCompleteOrder'):VCMS::get('Payment.DepositFunds'); ?></h1>
                                </div>
                                <div class="small-12 medium-5 text-right columns">
                                    <img src="<?=$base;?>static/img/tags.png">
                                </div>
                            </div-->
                            <hr>
                            <form target="paymentframe" method="post" action="/administration/LL/CRM/CRM/create_order?s=5&id=<?=$this->memberid;?>&confirm=payment" name="addCredit" class="addCreditForm">

                    <?php

                    $stepNumber=1;

                    if($memberdetails['address1']):

                        ?>


                    <!-- Add in address -->
                                <div class="row">
                                    <!-- Add stepComplete to tick it -->
                                    <div class="small-3 medium-1 columns">
                                        <span><?=$stepNumber++?></span>
                                    </div>
                                    <div class="small-9 medium-11 columns">
                                        <h4><?=VCMS::get('Register.EnterYourAddress'); ?> <div class="help"><a href="javascript:void(0)" data-tooltip class="has-tip" title="<?=VCMS::get('Tooltips.EnterDepositAmount')?>"><i class="fa fa-question-circle"></i></a></div></h4>
                                    </div>
                            <!--
                                    <div class="small-7 columns address" id="postcode_lookup">
                                        <input type="text" name="postcode" style="display:inline-block;width:150px" value="Postcode"/>
                                        <button class="button lightBlueButton">Lookup my address</button>
                                    </div>
                                    <div class="small-7 columns address" id="postcode_select">
                                        <input type="text" name="postcode" style="display:inline-block;width:150px" value="Postcode"/>
                                        <select>
                                            <option>9 Top Llan Road</option>
                                            <option>10 Top Llan Road</option>
                                            <option>Enter Address Manually</option>
                                        </select>
                                    </div>
                            -->
                                    <div class="small-12 medium-6 columns address" id="manual_entry">
                                        <div class="row">
                                            <div class="clearfix">
                                                <div class="small-12 columns">
                                                    <input type="text" name="address1" id="mlAddressOne" class="mlInput" placeholder="<?=VCMS::get('Register.Address1'); ?>" data-text="<?=VCMS::get('Register.Address1Message'); ?>" required value="<?=$memberdetails['address1']?>" />
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="small-12 columns">
                                                    <input type="text" name="address2" id="mlAddressTwo" class="mlInput" placeholder="<?=VCMS::get('Register.Address2'); ?>" data-text="<?=VCMS::get('Register.Address2Message'); ?>" value="<?=(!empty($registerform) ? $registerform['address2'] : ''); ?>" />
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="small-12 columns">
                                                    <input type="text" name="city" id="mlTownCity" class="mlInput" placeholder="<?=VCMS::get('Register.TownCity'); ?>" data-text="<?=VCMS::get('Register.TownCityMessage'); ?>" required value="<?=(!empty($registerform) ? $registerform['city'] : ''); ?>" />
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="small-12 columns">
                                                    <input type="text" name="county_state" id="mlCountyState" class="mlInput" placeholder="<?=VCMS::get('Register.CountyState'); ?>" data-text="<?=VCMS::get('Register.CountyStateMessage'); ?>" required value="<?=(!empty($registerform) ? $registerform['county_state'] : ''); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="small-12 medium-6 columns address" id="manual_entry">
                                        <div class="row">
                                            <div class="clearfix">
                                                <div class="small-12 columns">
                                                    <input type="text" name="zip" id="mlZip" class="mlInput" placeholder="<?=VCMS::get('Register.Postcode'); ?>" data-text="<?=VCMS::get('Register.PostcodeMessage'); ?>" required value="<?=(!empty($registerform) ? $registerform['zip'] : ''); ?>" />
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="small-12 columns">
                                                    <select name="country" id="mlCountry" class="mlInput" placeholder="<?=VCMS::get('Register.Country'); ?>" data-text="<?=VCMS::get('Register.CountryMessage'); ?>">
                                                        <option value="-" selected disabled>-- <?=VCMS::get('Register.Country'); ?> --</option>
                                                        <?php
                                                        foreach($countries as $country) {
                                                                echo '<option value="'.$country['country_id'].'" '.((($registerform['country'] == $country['country_id']) || $membercountry == $country['country_id']) ? ' selected' : '').'>'.$country['title'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <?php
                                endif;
                                ?>

                                <!-- Select or enter amount to deposit-->
                                <div class="row">
                                    <div class="small-12 columns">
                                        <div class="toDep">
                                            <p>Choose payment amount : <!--div class="help"><a href="javascript:void(0)" data-tooltip class="has-tip" title="<?=VCMS::get('Tooltips.EnterDepositAmount')?>"><i class="fa fa-question-circle"></i></a></div--></p>
                                            <label class="rHold" style="width: 15%; display:inline;" for="dep_5"><input type="radio" id="dep_5" name="amount" value="5" id="5"> €5</label>
                                            <label class="rHold" style="width: 15%; display:inline;" for="dep_10"><input type="radio" id="dep_10" name="amount" value="10" id="10">€10</label>
                                            <label class="rHold" style="width: 15%; display:inline;" for="dep_25"><input type="radio" id="dep_25" name="amount" value="25" id="25">€25</label>
                                            <label class="rHold" style="width: 15%; display:inline;" for="dep_50"><input type="radio" id="dep_50" name="amount" value="50" id="50">€50</label>
                                            <label class="rHold" style="width: 15%; display:inline;" for="dep_100"><input type="radio" id="dep_100" name="amount" value="100" id="100">€100</label>
                                            <label for="dep_amount" style="width: 15%; display:inline;">
                                                <input type="radio" name="amount" value="amt_inpt" id="depAmount" checked="checked" />
                                                €<input type="text" placeholder="<?=VCMS::get('Payment.OrderAmount'); ?>" value="<?=($fDefaultAmount<=1.5)?10:$fDefaultAmount?>" id="depAmountText" name="amount_input" />
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                /*
                                <div class="row">
                                        <div class="small-11 small-offset-1 columns">
                                                <h4 class="voucher">Enter voucher code <div class="help"><a href=""><i class="fa fa-question-circle"></i></a></div></h4>
                                        </div>
                                </div>

                                <div class="row">
                                        <div class="small-3 small-offset-1 columns">
                                                <P>Enter Voucher Code No.</P>
                                        </div>

                                        <div class="small-8 columns">
                                        <label><input type="text"/></label>
                                        </div>
                                </div>

                                <div class="row">
                                        <div class="small-11 small-offset-1">
                                                <p class="sub">A Voucher Code is a unique reference used with certain promotions. Look out for special offer emails from Maxlotto </p>
                                        </div>
                                </div>
                                */
                                ?>

                                <input type="submit" id="formSubmit" value="Confirm Amount" />
                            </form>
                            <hr />

                            <!-- >Enter payment details-->
                            <div class="row" id="paymentform" style="display:none;"> <!-- notComplete -->
                                <div class="small-3 medium-1 columns">
                                    <span><?=$stepNumber++?></span>
                                </div>
                                <div class="small-9 medium-11 columns">
                                    <h4><?=VCMS::get('Payment.EnterPaymentDetails'); ?> <div class="help"><a href="javascript:void(0)" data-tooltip class="has-tip" title="<?=VCMS::get('Tooltips.EnterPaymentDetails')?>"><i class="fa fa-question-circle"></i></a></div></h4>
                                </div>
                            </div>

                <?php
                $this->display('pods/payment-progress-popup');
                ?>

                            <iframe name="paymentframe" style="width:100%;border:0;height:800px;"></iframe>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
