<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Amend Limits</li>
</ul>
<?php if ($this->bInCoolingOffPeriod) : ?>
<p style="color: red;">
    User has been unsuspended but is still within their 24-hour cooling-off period (until <?=$this->sCoolingOffPeriodEndDate;?>)
</p>
<?php 
    endif; 
    if ($this->bInSuspensionPeriod) : ?>
<p style="color: red;">
        <b>User is currently suspended:</b>
        <br/>
        Suspension start: <b><?=$this->sSuspensionStartDate;?></b>
        <br/>
        Suspension end: <b><?=$this->sSuspensionEndDate;?></b>
</p>
<?php else : ?>
<form method="POST" action="">
    <fieldset class="detailsform">
        <?php if ($this->bUserSuspended) : ?>
        <p>User is currently suspended.  Press "Confirm Changes" to unsuspend this user</p>
        <input type="hidden" name="unsuspend_confirm" id="field_unsuspend_confirm" value="unsuspend_confirm"/>
        <?php else : ?>
        <label class="small">
            <span>Monthly Deposit Limit:</span>
            <div>
                <input type="text" name="monthly_limit" id="field_monthly_limit" value="<?=$this->oMember->monthly_deposit_limit;?>"/>
            </div>
        </label>         
        <label class="small">
            <span>Suspend User For:</span>
            <div>
                <select name="suspend_period" id="field_suspend_period">
                    <option value="dns">Do not suspend</option>
                    <option value="1">1 month (until <?=$this->s1MonthString;?>)</option>
                    <option value="3">3 months (until <?=$this->s3MonthString;?>)</option>
                    <option value="6">6 months (until <?=$this->s6MonthString;?>)</option>
                    <option value="9">9 months (until <?=$this->s9MonthString;?>)</option>
                </select>
            </div>
        </label>        
        <?php endif; ?>
    </fieldset>
    <input type="submit" name="action" value="Confirm Changes" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>
<?php endif; ?>
