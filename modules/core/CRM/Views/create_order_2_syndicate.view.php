<h2>Creating Order - Stage 2</h2>

<h4>Game Name: <?=$aGameEngine->name;?> (Syndicate)</h4>
<h4>Base Price: <?=$aGameEngine->base_price;?></h4>

<form method="POST" action="">
    <fieldset class="detailsform">
         <label class="long">
            <span>Syndicates available</span>
            <div>
                 <?php foreach ($this->aSyndicates as $aSyndicate) : ?>
                    <input type="checkbox" class="long" name="syndicateId[]" id="field_syndicateID" value="<?=$aSyndicate['id'];?>" style="margin:0;" /> 
                    Syndicate <?=$aSyndicate['id'];?> (price per share <?=$aSyndicate['price_per_share'];?>, current capacity <?=$aSyndicate['current_shares'];?>/<?=$aSyndicate['max_shares'];?> shares, numbers: <?=implode(" ", $aSyndicate['numbers']);?>)
                    <br/>
                 <?php endforeach; ?>
            </div>
        </label>
        <label class="short">
            <span>Number Of Shares In Each Syndicate</span>
            <div>
                <select name="shares" id="field_shares" class="halfsize" style="display:inline-block;">
                 <?php for ($j=1; $j<=100; $j++) : ?>
                    <option value="<?=$j;?>"><?=$j;?></option>
                 <?php endfor; ?>
                </select>            
            </div>
        </label>
        <label class="short">
            <span>Number Of Weeks To Play</span>
            <div>
                <select name="playDuration" id="field_playDuration" class="halfsize" style="display:inline-block;">
                 <?php for ($i=1; $i<=12; $i++) : ?>
                    <option value="<?=$i;?>"><?=$i;?></option>
                 <?php endfor; ?>
                </select>            
            </div>
        </label>
        <label class="long">
            <span>Subscription?</span>
            <div><input type="checkbox" name="isSubscription" id="field_isSubscription" class="long" value="1"></div>
        </label>
    </fieldset>
    <input type="hidden" name="gameID" id="field_gameID" value="<?=$aGameEngine->id;?>" />
    <input type="hidden" name="gameType" id="field_gameType" value="<?=$aGameEngine->sGameType;?>" />
    <input type="submit" name="action" value="Add to order"> Add this game to the order
</form>
<form method="POST" action="">
    <input type="hidden" name="add_different" value="yes" />
    <input type="submit" name="action" value="Add a different game" />
    Cancel this and add a different game to the order
</form>
<form method="POST" action="">
    <input type="hidden" name="start_new" value="yes" />
    <input type="submit" name="action" value="Clear and start over" />
    Clear this order and start a new one
</form>