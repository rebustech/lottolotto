<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Add Transaction</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Transaction Type</span>
            <div>                
                <select name="contact_type" id="field_contact_type">
                 <?php foreach ($aContactTypes as $aContactType) : ?>
                    <option value="<?=$aContactType['id'];?>"><?=$aContactType['name'];?></option>
                 <?php endforeach; ?>
                </select>
            </div>
        </label>
        <label class="long">
            <span>Amount</span>
            <div><textarea name="details" id="field_details" class="long" rows="4" cols="80"></textarea></div>
        </label>
        <label class="long">
            <span>Customer Message</span>
            <div><textarea name="customer_message" id="field_customer_message" class="long" rows="4" cols="80"></textarea></div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>