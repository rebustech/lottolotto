<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Edit Date of Birth</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="small">
            <span>Date of birth</span>
        </label>
        <select name="dob_d" id="field_dob_d" class="halfsize number" style="display:inline-block;">
        <?php for ($d=1; $d<=31; $d++) : ?>
            <option value="<?=$d;?>"<?=($d==$this->aDOB[2]?" selected='selected'":"");?>><?=$d;?></option>
        <?php endfor; ?>
        </select>
        <select name="dob_m" id="field_dob_m" class="halfsize" style="display:inline-block;">
        <?php foreach ($this->aMonths as $m=>$text) : ?>
            <option value="<?=$m;?>"<?=($m==$this->aDOB[1]?" selected='selected'":"");?>><?=$text;?></option>
        <?php endforeach; ?>
        </select>
        <select name="dob_y" id="field_dob_y" class="halfsize number" style="display:inline-block;">
        <?php for ($y=$this->iStartYear; $y>=$this->iStartYear-100; $y--) : ?>
            <option value="<?=$y;?>"<?=($y==$this->aDOB[0]?" selected='selected'":"");?>><?=$y;?></option>
        <?php endfor; ?>
        </select>

    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel" onclick="history.go(-1);event.preventDefault();" />
</form>
<?php if($this->bInvalidDate) : ?>
<h3>Invalid date of birth entered.</h3>
<?php
    endif;
    
    if($this->bTooYoung) : 
?>
<h3>Customer is not eligible to play (too young)</h3>
<?php endif; ?>
