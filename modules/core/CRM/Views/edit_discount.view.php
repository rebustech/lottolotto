<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Edit Member Discounts</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Fee discount %</span>
            <div><input type="text" name="fee_discount_percent" id="fee_discount_percent" class="small short" value="<?=$this->oMember->fee_discount_percent;?>"></div>
        </label>
        <label class="long">
            <span>Fee discount ends</span>
            <div><input type="text" name="fee_discount_ends" id="fee_discount_ends" class="small short datetimepicker" value="<?=$this->oMember->fee_discount_ends;?>"></div>
        </label>
        <br/>
        <label class="long">
            <span>Discount %</span>
            <div><input type="text" name="discount_percent" id="discount_percent" class="small short" value="<?=$this->oMember->discount_percent;?>"></div>
        </label>
        <label class="long">
            <span>Discount ends</span>
            <div><input type="text" name="discount_ends" id="discount_ends" class="small short datetimepicker" value="<?=$this->oMember->discount_ends;?>"></div>
        </label>
        <label class="long">
            <span>Discount max %</span>
            <div><input type="text" name="discount_max" id="discount_max" class="small short" value="<?=$this->oMember->discount_max;?>"></div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>
<h2>Information</h2>
<p>Please note that member discounts do not apply to quick deals as these are already discounted products</p>
<p>Discount % is added to any additional discounts, such as multi-draw discounts. Use Discount Max to ensure that this member doesn't receive too much
discount. To make the discount fixed and ignore any additional discounts set Discount % and Discount max % to the same.</p>