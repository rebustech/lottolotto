<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Amend Email Alerts</li>
</ul>
<form method="POST" action="">
    <table>
        <thead>
            <tr>
                <th></th>
                <th>Results Alerts</th>
                <th>Jackpot Alerts</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>6aus49</th>
                <td>
                    <select name="result_6aus49">
                        <option value="1" <?=$this->aMemberAlerts['result_6aus49']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0" <?=$this->aMemberAlerts['result_6aus49']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
                <td>
                    <select name="jackpot_6aus49">
                        <option value="1" <?=$this->aMemberAlerts['jackpot_6aus49']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0" <?=$this->aMemberAlerts['jackpot_6aus49']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Eurojackpot</th>
                <td>
                    <select name="result_eurojackpot">
                        <option value="1"<?=$this->aMemberAlerts['result_eurojackpot']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['result_eurojackpot']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
                <td>
                    <select name="jackpot_eurojackpot">
                        <option value="1"<?=$this->aMemberAlerts['jackpot_eurojackpot']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['jackpot_eurojackpot']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>EuroMillions</th>
                <td>
                    <select name="result_euromillions">
                        <option value="1"<?=$this->aMemberAlerts['result_euromillions']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['result_euromillions']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
                <td>
                    <select name="jackpot_euromillions">
                        <option value="1"<?=$this->aMemberAlerts['jackpot_euromillions']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['jackpot_euromillions']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Megamillions</th>
                <td>
                    <select name="result_megamillions">
                        <option value="1"<?=$this->aMemberAlerts['result_megamillions']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['result_megamillions']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
                <td>
                    <select name="jackpot_megamillions">
                        <option value="1"<?=$this->aMemberAlerts['jackpot_megamillions']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['jackpot_megamillions']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Powerball</th>
                <td>
                    <select name="result_powerball">
                        <option value="1"<?=$this->aMemberAlerts['result_powerball']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['result_powerball']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
                <td>
                    <select name="jackpot_powerball">
                        <option value="1"<?=$this->aMemberAlerts['jackpot_powerball']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['jackpot_powerball']==0?"selected='selected'":"";?>>No</option>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <div><span>Jackpot Alerts</span>&nbsp;<select name="max_jackpots">
                        <option value="1"<?=$this->aMemberAlerts['max_jackpots']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['max_jackpots']==0?"selected='selected'":"";?>>No</option>
                    </select></div>
    <br/>
    
    <div><span>News Alerts</span>&nbsp;<select name="max_news">
                        <option value="1"<?=$this->aMemberAlerts['max_news']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['max_news']==0?"selected='selected'":"";?>>No</option>
                    </select></div>
    <br/>
    
    <div><span>Alert When Jackpot Above Level</span>&nbsp;<select name="max_jackpot_over">
                        <option value="1"<?=$this->aMemberAlerts['max_jackpot_over']==1?"selected='selected'":"";?>>Yes</option>
                        <option value="0"<?=$this->aMemberAlerts['max_jackpot_over']==0?"selected='selected'":"";?>>No</option>
                    </select></div>
    <br/>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel" onclick="history.go(-1);event.preventDefault();" />
</form>
