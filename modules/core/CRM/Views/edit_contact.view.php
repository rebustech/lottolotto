<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Edit Contact Details</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Main Contact</span>
            <div><input type="text" name="contact" id="field_contact" class="long" value="<?=$this->oMember->contact;?>"></div>
        </label>
        <label class="long">
            <span>Extra Contact 1</span>
            <div><input type="text" name="extra_contact_1" id="field_extra_contact_1" class="long" value="<?=$this->oMember->extra_contact_1;?>"></div>
        </label>
        <label class="long">
            <span>Extra Contact 2</span>
            <div><input type="text" name="extra_contact_2" id="field_extra_contact_2" class="long" value="<?=$this->oMember->extra_contact_2;?>"></div>
        </label>
        <label class="long">
            <span>Extra Contact 3</span>
            <div><input type="text" name="extra_contact_3" id="field_extra_contact_3" class="long" value="<?=$this->oMember->extra_contact_3;?>"></div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel" onclick="history.go(-1);event.preventDefault();" />
</form>
