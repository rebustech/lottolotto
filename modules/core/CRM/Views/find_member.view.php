<h2>Customer Search</h2>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>First Name</span>
            <div><input type="text" name="firstname" value="<?=$_POST['firstname']?>"/></div>
        </label>
        <label class="long">
            <span>Last Name</span>
            <div><input type="text" name="lastname" value="<?=$_POST['lastname']?>"/></div>
        </label>
        <label class="long">
            <span>Phone Number</span>
            <div><input type="text" name="contact" value="<?=$_POST['contact']?>"/></div>
        </label>
        <label class="long">
            <span>Postcode</span>
            <div><input type="text" name="zip" value="<?=$_POST['zip']?>"/></div>
        </label>
        <label class="long">
            <span>Email</span>
            <div><input type="text" name="email" value="<?=$_POST['email']?>"/></div>
        </label>
        <label>
            <span>Transaction Number</span>
            <div><input type="text" name="transaction_number" value="<?=$_POST['transaction_number']?>"/></div>
        </label>
        <label>
            <span>Bet Order Number</span>
            <div><input type="text" name="bet_order_number" value="<?=$_POST['bet_order_number']?>"/></div>
        </label>
        <label>
            <span>Order Number</span>
            <div><input type="text" name="order_number" value="<?=$_POST['order_number']?>"/></div>
        </label>
        <label>
            <span>Bet Number</span>
            <div><input type="text" name="bet_number" value="<?=$_POST['bet_number']?>"/></div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Search">

</form>
<?php
    if($this->bNoResults) :
?>
<h3>No matches found. Please refine your search criteria.</h3>
<?php
    elseif ($this->bTooManyResults) :
?>
<h3>Too many matches found. Please add more search criteria.</h3>
<?php
    elseif ($this->bWithinLimits) :

?>
<h3>Your search has returned <?=$sResultConfirmString;?>  Please select the correct one</h3>
<table class="datatable">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email Address</th>
            <th>Date of Birth</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($aFoundMembers as $aMember) : ?>
        <tr>
            <td><?=$aMember['firstname'];?></td>
            <td><?=$aMember['lastname'];?></td>
            <td><?=$aMember['email'];?></td>
            <td><?=$aMember['dob'];?></td>
            <td><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$aMember['member_id'];?>">Select This Member</td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php
    endif;
?>