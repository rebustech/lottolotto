<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Change Password</li>
</ul>
<h4>Please confirm the customer's username below.  Clicking "Confirm" will send an email to the customer with their new password.</h4>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Username</span>
            <div><input type="text" name="username" id="field_username" class="long" readonly="readonly" value="<?=$this->oMember->username;?>"></div>
        </label>
 
    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>
<?php if($this->bChangepasswordError) : ?>
<h3>Problem changing customer's password.</h3>
<?php
    endif;
?>