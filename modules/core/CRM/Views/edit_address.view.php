<ul class="BreadcrumbsControl">
    <li>
        <span class="fa-stack fa-2x">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-male fa-stack-1x fa-inverse"></i>
        </span>
        <h3><a href="/administration/LL/CRM/CRM/find">CRM</a></h3>
    </li>
    <li><a href="/administration/generic-details.php?pp=members.php&a=view&tablename=members&pp=members.php&id=<?=$this->oMember->member_id;?>"> <?=$this->oMember->firstname;?> <?=$this->oMember->lastname;?></a></li>
    <li> <i class="fa fa-angle-right"></i> Edit Address</li>
</ul>
<form method="POST" action="">
    <fieldset class="detailsform">
        <label class="long">
            <span>Address 1</span>
            <div><input type="text" name="address1" id="field_address1" class="long" value="<?=$this->oMember->address1;?>"></div>
        </label>
        <label class="long">
            <span>Address 2</span>
            <div><input type="text" name="address2" id="field_address2" class="long" value="<?=$this->oMember->address2;?>"></div>
        </label>
        <label class="long">
            <span>City</span>
            <div><input type="text" name="city" id="field_city" class="long" value="<?=$this->oMember->city;?>"></div>
        </label>
        <label class="long">
            <span>County/State</span>
            <div><input type="text" name="county_state" id="field_county_state" class="long" value="<?=$this->oMember->county_state;?>"></div>
        </label>
        <label class="long">
            <span>ZIP</span>
            <div><input type="text" name="zip" id="field_zip" class="long" value="<?=$this->oMember->zip;?>"></div>
        </label>
        <label class="halfsize number">
            <span>Country</span>
            <div>
                <select name="fk_country_id" id="field_fk_country_id" class="halfsize" style="display:inline-block;">
                 <?php foreach ($aCountries as $aCountry) : ?>
                    <option value="<?=$aCountry['fk_country_id'];?>"<?=($aCountry['fk_country_id']==$this->oMember->fk_country_id?" selected='selected'":"");?>><?=$aCountry['title'];?></option>
                 <?php endforeach; ?>
                </select>
            </div>
        </label>
    </fieldset>
    <input type="submit" name="action" value="Confirm" />
    <input type="button" name="cancel" value="Cancel"  onclick="history.go(-1);event.preventDefault();" />
</form>
