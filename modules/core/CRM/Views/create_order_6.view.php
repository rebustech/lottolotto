<h2>Order Complete - Payment Successfully Taken</h2>

<h3>MaxLotto order number: <?=$this->ordernumber;?></h3>
<h3>Amount paid: <?=$this->transactionamount;?></h3>
<h3>Customer's new account balance: <?=$this->customerbalance;?></h3>
<p><br/>(Payfrex Transaction ID: <?=$this->transactionid;?>)</p>
<hr/>
<p>Please choose an option:</p>

<form method="POST" action="">
    <input type="hidden" name="new_order" value="yes" />
    <input type="submit" name="action" value="New Order" />
    Create a new order for this customer
</form>

<form method="POST" action="">
    <input type="hidden" name="finish" value="yes" />
    <input type="submit" name="action" value="Finish" />
    Finish and return to customer details screen
</form>

<form method="POST" action="">
    <input type="hidden" name="new_cust" value="yes" />
    <input type="submit" name="action" value="New customer Search" />
    Search for a new customer
</form>