<h2>Creating Order - Stage 3</h2>

<h3>Product added to order.</h3>

<h4>Current order contents:</h4>

<!-- Insert basket contents here -->
<?php 
    foreach ($this->orderitems as $key=>$orderitem) : 
        if (is_numeric($key)) :

?>
<p>
    Order item <?=$key+1;?><br/>
    Game title: <?=$orderitem['data']['gameTitle'];?><br/>
    Type of game: <?=$orderitem['data']['cartLinesText'];?><br/><br/>
    Order details:<br/>
    &bull; <?=$orderitem['data']['pricinginfo']['td_line1'];?>
    &bull; <?=$orderitem['data']['pricinginfo']['td_line2'];?>
    &bull; <?=$orderitem['data']['pricinginfo']['td_line3'];?>
    &bull; <?=$orderitem['data']['pricinginfo']['pr_line1'];?>
    &bull; <?=$orderitem['data']['pricinginfo']['pr_line2'];?><br/><br/>
    <b><?=$orderitem['data']['pricinginfo']['total_line2'];?>: <?=$orderitem['data']['pricinginfo']['total_formatted'];?></b> 

    
</p>
<?php
        endif;
    endforeach; 
?>
<p>
<b>Current order total: <?=$this->ordertotal;?></b>
</p>
<hr/>
<p>Please choose an option:</p>

<form method="POST" action="">
    <input type="hidden" name="add_next" value="yes" />
    <input type="submit" name="action" value="Add another item" />
    Add another game to the order
</form>
<form method="POST" action="">
    <input type="hidden" name="order_pay" value="yes" />
    <input type="submit" name="action" value="Finish and pay" />
    Finish order and proceed to payment screen
</form>
<form method="POST" action="">
    <input type="hidden" name="start_new" value="yes" />
    <input type="submit" name="action" value="Clear and start over" />
    Clear this order and start a new one
</form>
