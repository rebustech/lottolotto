<?php
/**
 * Installs Silverpop database changes
 * @package LoveLotto
 * @subpackage PayFrex
 */

namespace LL\CRM;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        if(self::getModuleVersion('CRM')<1){
        }
        $aJobs[]=new \LL\Installer\Task('LL\CRM\Installer', 'installDatabase', 'Install CRM (Member) Database updates');

        return $aJobs;
    }

    /**
     * Installs all the tables we need for the new CMS
     */
    static function installDatabase(\LL\Installer\Task $oTask){
        self::updateMembersTable();
        self::updateMembersContactLogTable();

    }

    /**
     * Modification to transactions table
     */
    static function updateMembersTable(){

        $oTTable=new \LL\Installer\Table('members');


        $oTTable->addField(new \LL\Installer\Field('monthly_deposit_limit','float', '11,2'));
        $oTTable->addField(new \LL\Installer\Field('is_suspended','tinyint', 4));
        $oTTable->addField(new \LL\Installer\Field('suspension_start','datetime'));
        $oTTable->addField(new \LL\Installer\Field('suspension_end','datetime'));
        $oTTable->addField(new \LL\Installer\Field('suspension_lifted','datetime'));
        $oTTable->addField(new \LL\Installer\Field('extra_contact_1','varchar', 50));
        $oTTable->addField(new \LL\Installer\Field('extra_contact_2','varchar', 50));
        $oTTable->addField(new \LL\Installer\Field('extra_contact_3','varchar', 50));
        $oTTable->addField(new \LL\Installer\Field('county_state','varchar', 50));
        
        /**
         * Additional fields for member discounts
         */
        $oTTable->addField(new \LL\Installer\Field('fee_discount_percent', 'int',11));
        $oTTable->addField(new \LL\Installer\Field('fee_discount_ends', 'datetime'));
        $oTTable->addField(new \LL\Installer\Field('discount_percent', 'int',11));
        $oTTable->addField(new \LL\Installer\Field('discount_ends', 'datetime'));
        $oTTable->addField(new \LL\Installer\Field('discount_max', 'int',11));

        $oTTable->compile();

        /*
	ADD COLUMN `monthly_deposit_limit` FLOAT(11,2) NULL DEFAULT '10000.00' AFTER `fk_source_advert_id`,
	ADD COLUMN `is_suspended` TINYINT NULL DEFAULT '0' AFTER `monthly_deposit_limit`,
	ADD COLUMN `suspension_start` DATETIME NULL AFTER `is_suspended`,
	ADD COLUMN `suspension_end` DATETIME NULL AFTER `suspension_start`,
	ADD COLUMN `suspension_lifted` DATETIME NULL AFTER `suspension_end`;
         */

    }

    /**
     * Modification to transactions table
     */
    static function updateMembersContactLogTable(){

        $oTTable=new \LL\Installer\Table('members_contact_log');


        $oTTable->addField(new \LL\Installer\Field('customer_message','text'));
        $oTTable->addField(new \LL\Installer\Field('subject','text'))->drop();

        $oTTable->compile();

        /*
	ADD COLUMN `monthly_deposit_limit` FLOAT(11,2) NULL DEFAULT '10000.00' AFTER `fk_source_advert_id`,
	ADD COLUMN `is_suspended` TINYINT NULL DEFAULT '0' AFTER `monthly_deposit_limit`,
	ADD COLUMN `suspension_start` DATETIME NULL AFTER `is_suspended`,
	ADD COLUMN `suspension_end` DATETIME NULL AFTER `suspension_start`,
	ADD COLUMN `suspension_lifted` DATETIME NULL AFTER `suspension_end`;
         */

    }

}