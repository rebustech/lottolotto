<?php

/**
 * Handle all Game Engine API interactions.
 *
 * @author    Lewis Theobald
 * @author    Wez Pyke
 * @version	1.0.1
 */
class GamesApi extends \LL\AjaxController {

	/**
	 * index: Default list of games
	 *
	 */
	public function index() {
		$sSql = "SELECT id, name, url, image, summary_content
		FROM game_engines
		WHERE active = 1
		ORDER BY name ASC";

		// Run the query
		$gameEngineChildren = DAL::executeQuery($sSql);

		// Clean our response into array(data, status, messages)
		$response = array(
			'status' => 200
		);

		// Clean into JSON formatting
		$games = array_values($gameEngineChildren);
		foreach($games as &$game) {
			$game['id'] = intval($game['id']);

            // Next draw date
            $sSql = "SELECT drawdate, jackpot FROM r_lottery_dates WHERE fk_lottery_id = {$game['id']} AND drawdate > NOW() LIMIT 1";
            $nextDraw = DAL::executeGetRow($sSql);
            $game['next'] = $nextDraw;

            // Previous result
            $sSql = "SELECT numbers, jackpot, fk_lotterydate FROM lottery_draws WHERE fk_lottery_id = {$game['id']} AND fk_lotterydate < NOW() ORDER BY fk_lotterydate DESC LIMIT 1 ";
            $previousDraw = DAL::executeGetRow($sSql);
            $game['previous'] = $previousDraw;
		}

		// Add the games
		$response['games'] = $games;

		// Pass over to output method
		$this->output($response);
	}

    /**
	 * Get full details on a game
	 *
	 * @param int ID - Game we want to fetch
	 */
	public function details($parameters = array()) {
		# Read the first item
		if(empty($parameters) || empty($parameters[0])) {
			$this->error(['error' => 'Missing or invalid arguments.']);
		}

		$sSql = "SELECT id, name, url, image, summary_content
		FROM game_engines
		WHERE active = 1 AND id = '{$parameters[0]}'
		ORDER BY name ASC";

		// Run the query
		$gameEngine = DAL::executeGetRow($sSql);

        // Next draw date
        $sSql = "SELECT drawdate, jackpot FROM r_lottery_dates WHERE fk_lottery_id = {$parameters[0]} AND drawdate > NOW() LIMIT 1";
        $nextDraw = DAL::executeQuery($sSql);

        // Previous result
        $sSql = "SELECT numbers, jackpot, fk_lotterydate FROM lottery_draws WHERE fk_lottery_id = {$parameters[0]} AND fk_lotterydate < NOW() ORDER BY fk_lotterydate DESC LIMIT 1 ";
        $previousDraw = DAL::executeQuery($sSql);

		// Clean our response into array(data, status, messages)
		$response = array(
			'status' => 200
		);

		// Clean into JSON formatting
		if(empty($gameEngine)) {
			$this->error(['error' => 'Invalid game engine ID.'], 404);
		}

		// Format as integer
		$gameEngine['id'] = intval($gameEngine['id']);

		// Add the games
		$response['data'] = $gameEngine;

        // Display the next draw date and jackpot
        $response['data']['next'] = $nextDraw;
        $response['data']['previous'] = $previousDraw;

		// Pass over to output method
		$this->output($response);
	}


    /**
     * Return a specific result data, and its prize breakdown, jackpot, etc
     * @param $id
     * @return array $lotteryResults
     */
    protected function getResults($id = null) {
        // Get the draws from the cache
        $lotteryResults = LLCache::get('GE-lotteryResults');

        // Not in cache, query it
        if(empty($lotteryResults)) {

            if (is_null($id)) {

                // Get the most-coming draw
                $sSQL = "SELECT
                        ld.numbers,
                        ld.fk_lotterydate AS lotterydate,
                        ld.jackpot,
                        ld.draw,
                        ld.id,
                        lc.lottery_id,
                        ld.winnings
                    FROM lottery_draws ld
                    INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
                    INNER JOIN (SELECT fk_lottery_id,MAX(fk_lotterydate) AS fk_lotterydate FROM lottery_draws WHERE winnings != 'b:0;'
                        AND winnings IS NOT NULL
                        AND fk_lotterydate <= NOW() GROUP BY fk_lottery_id) AS pp ON pp.fk_lottery_id=ld.fk_lottery_id AND pp.fk_lotterydate=ld.fk_lotterydate
                    GROUP BY lottery_id
                    ORDER BY ld.fk_lotterydate DESC";
            } else {

                // Get the most-coming draw
                $sSQL = "SELECT
                        ld.numbers,
                        ld.fk_lotterydate AS lotterydate,
                        ld.jackpot,
                        ld.draw,
                        ld.id,
                        lc.lottery_id,
                        ld.winnings
                    FROM lottery_draws ld
                    INNER JOIN lotteries_cmn lc ON lc.lottery_id = {$id}
                    INNER JOIN (SELECT fk_lottery_id,MAX(fk_lotterydate) AS fk_lotterydate FROM lottery_draws WHERE winnings != 'b:0;'
                        AND winnings IS NOT NULL
                        AND fk_lotterydate <= NOW() GROUP BY fk_lottery_id) AS pp ON pp.fk_lottery_id=ld.fk_lottery_id AND pp.fk_lotterydate=ld.fk_lotterydate
                    GROUP BY lottery_id
                    ORDER BY ld.fk_lotterydate DESC";
            }

            $lotteryResultsRaw = DAL::executeQuery($sSQL);

            $lotteryResults = array();
            foreach($lotteryResultsRaw as $resultDraw) {
                $lotteryResults[$resultDraw['lottery_id']] = $resultDraw;
                $lotteryResults[$resultDraw['lottery_id']]['numbers'] = explode("|", $resultDraw['numbers']);
                $lotteryResults[$resultDraw['lottery_id']]['winnings'] = unserialize($resultDraw['winnings']);
            }

            // Whack into LLCache
            LLCache::add('GE-lotteryResults', $lotteryResults, false, 900);
        }

        return $lotteryResults;
    }

    /**
     * HTTP GET
     * Return an overview of the game results
     * @param $parameters
     */
    public function results($parameters = array()) {

        if (empty($parameters)) {

            // Return overview of game results if no ID is provided
            $output = $this->getResults();
        } else {

            // Return specific result data if ID is provided
            $output = $this->getResults($parameters[0]);
        }

        // Display the output
        $this->output($output);
    }


    /**
     * HTTP GET
     * Retrieve the jackpots
     */
    public function jackpots() {
        # Get the draws from the cache
        $lotteryDrawPrizes = LLCache::get('GE-lotteryDraws');

        // Not in cache, query it
        if(empty($lotteryDrawPrizes)) {
            // Get the most-coming draw
            $sSql = "SELECT ld.*, cc.* FROM r_lottery_dates ld
            INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id
            INNER JOIN currencies cc ON cc.currency_id = lc.fk_currency_id
            WHERE ld.drawdate > NOW()
            GROUP BY ld.fk_lottery_id
            ORDER BY ld.drawdate ASC";
            $lotteryDrawPrizesRaw = DAL::executeQuery($sSql);

            $lotteryDrawPrizes = array();
            if(!empty($lotteryDrawPrizesRaw)) {
                foreach($lotteryDrawPrizesRaw as $draw) {
                    $lotteryDrawPrizes[$draw['fk_lottery_id']] = $draw;
                }
            }

            // Whack into LLCache
            LLCache::add('GE-lotteryDraws', $lotteryDrawPrizes, false, 900);

        }
        $this->output($lotteryDrawPrizes);
    }


    /**
     * HTTP GET
     * Get the draw dates for each lottery a specific lottery
     * @param array $parameters
     */
    public function drawdates($parameters = array()) {
        $lotteryID = $parameters[0];

        if (empty($lotteryID)) {
            $sSql = "SELECT * FROM r_lottery_dates";
            $aLotteryDrawDates = DAL::executeQuery($sSql);
            return $this->output($aLotteryDrawDates);
        }

        $sSql = "SELECT * FROM r_lottery_dates WHERE fk_lottery_id = {$lotteryID}";
        $aLotteryDrawDates = DAL::executeQuery($sSql);

        // Return an error when no dates are found
        if (count($aLotteryDrawDates) === 0) {
            return $this->error(['error' => 'No lottery dates found for supplied lottery ID'], 404);
        }

        $this->output($aLotteryDrawDates);
    }

	/**
	 * Run the Game Engine checks. Not yet implemented.
	 *
	 * @throws LottoException
	 */
	public function run() {

		$this->error('Method not implemented');

	}

}
