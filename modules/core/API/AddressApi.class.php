<?php

/**
 * Class AddressApi
 *
 * @author Wez Pyke
 * @version 1.0.0
 */
class AddressApi extends \LL\AjaxController {

    private $postcode;
    private $oAddress;

    public function __construct() {
        $this->oAddress = new Address();
    }

    /**
     * Retrieve address for postcode
     * @param $params
     */
    public function retrieve($params) {

        // Return an error if no params are supplied
        if (empty($params)) {
            $this->error(["error" => "No postcode supplied"]);
        }

        $result = $this->oAddress->addressLookup($params[0], $params[1], $params[2], $params[3]);

        if (is_null($result->AddressLookupResult)) {
            return $this->error(["error" => "No addresses returned"]);
        }

        $this->output($result);
    }

}


