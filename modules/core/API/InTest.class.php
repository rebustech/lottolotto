<?php

/**
 * This class allows us to run various automated tests before peforming
 * nightly integration testing
 */
class InTest extends \LL\AjaxController{

    function Test(){
        $name=$_POST['name'];
        /**
         * Write a temporary file which will be called by the remote test server
         * these are then called by the test server. Test script is responsible
         * for removing itself, however a cron job on the server runs every day
         * at 7am to remove any remaining test scripts to just to be on the safe side
         */
        @mkdir(Config::$config->sServerPath.'\\tests\\');
        //Leave this readable and writeable only by nginx process to prevent other scripts/users accessing this
        $fp=fopen(Config::$config->sServerPath.'\\tests\\'.$name,'w');
        //Write each test to the file for calling
        foreach($_POST['lines'] as $line) fwrite($fp,$line);
        fclose($fp);
    }
}