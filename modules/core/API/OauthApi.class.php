<?php

/**
 * Handle all OAuth2 API interactions.
 *
 * @author    Lewis Theobald
 * @version	1.0.1
 */
class OauthApi extends \LL\AjaxController {

	/**
	 * @param Stores the OAuth2 instance
	 *
	 */
	public $server = NULL;
    private $oMember = NULL;
    private $bIsLoggedIn = false;
    private $memberDetails;
    private $memberID = NULL;

    public function __construct() {

        $oMember = unserialize($_SESSION['memberobject']);
        if (is_object($oMember) && is_a($oMember, "Member")) {
            $this->oMember = $oMember;
        } else {
            $this->oMember = new Member();
        }

        if($this->oMember->validateMember()) {
            // Assign the logged in user to the appropriate properties
            $this->memberDetails = $this->oMember->getMemberDetails();
            $this->bIsLoggedIn= true;
            $this->memberID = $this->memberDetails['member_id'];
        }
    }


	public function loadOAuth2Server() {

		require_once(dirname(__DIR__).'/OAuth2/Autoloader.php');
		\OAuth2\Autoloader::register();

		$storage = new \OAuth2\Storage\Pdo(array('dsn' => 'mysql:host='.DBSERVERADDRESS.';dbname='.DATABASENAME, 'username' => DBUSERNAME, 'password' => DBPASSWORD));
		$this->server = new \OAuth2\Server($storage);
		$this->server->addGrantType(new \OAuth2\GrantType\ClientCredentials($storage));
		$this->server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($storage));



	}

	/**
	 * authorize: Request third-party app authorization
	 *
	 */
	public function authorize() {

        // Return an error if the user does not have a MaxLotto session
        if (!$this->bIsLoggedIn) {
            $this->error(["error" => "MaxLotto session does not exist", "status" => 403], 403);
        }

		# Load OAuth2
		$this->loadOAuth2Server();

		# Read the request
		$request = OAuth2\Request::createFromGlobals();
		$response = new OAuth2\Response();

		# Validate the authorize request
		if(!$this->server->validateAuthorizeRequest($request, $response)) {
            var_dump("validate failed");
			$response->send();
			die;
		}

		$_POST['authorized'] = 'yes';

		# Display an authorization form
		if(empty($_POST)) {
		  exit('<form method="post">
				<label>Do You Authorize TestClient?</label><br />
				<input type="submit" name="authorized" value="yes">
				<input type="submit" name="authorized" value="no">
			</form>');
		}

		# Print the authorization code if the user has authorized your client
		$is_authorized = ($_POST['authorized'] === 'yes');


		# We're now authorized, create a code
		$this->server->handleAuthorizeRequest($request, $response, $is_authorized, $this->memberID);

		# This is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
		if($is_authorized) {
		  $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=') + 5, 40);
		}

		# Send the response
		$response->send();

	}

	/**
	 * token: Get the access token
	 *
	 */
	public function token() {

		$this->loadOAuth2Server();
		$this->server->handleTokenRequest(\OAuth2\Request::createFromGlobals())->send();

	}

	/**
	 * test: Only available if we have authorized and are accessing with a valid access token
	 *
	*/
	public function test() {

		# Fire up OAuth
		$this->loadOAuth2Server();

		# Handle a request for an OAuth2.0 Access Token and send the response to the client
		if(!$this->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
		    $this->server->getResponse()->send();
		    exit;
		}

		$this->output(array('status' => 200, 'response' => 'You accessed my APIs!'));

	}

}
