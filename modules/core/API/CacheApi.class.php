<?php

class CacheApi extends \LL\AjaxController {
    function flush(){
        error_reporting(E_ALL);
        LLCache::flush();
        die('Cache Flushed');
    }

    function stats(){
        error_reporting(E_ALL);
        if(!class_exists('Memcache')) die('Memcache Not Installed/Running');
        print_d(LLCache::stats());
    }
}