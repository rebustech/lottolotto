<?php

/**
 * Handle all Account API interactions.
 *
 * @author    Lewis Theobald
 * @version	1.0.1
 */
class AccountApi extends \LL\AjaxController {

    /**
     * Store the member object, details and
     * boolean of if they're logged in
     *
     */
    private $oMember = NULL;
    private $memberDetails = NULL;
    private $loggedIn = false;
    private $memberID = NULL;

    /**
     * Setup the member details, storing them
     * into the API call
     */
    private function _getMember() {

        # Need to re-open the session
        ini_set('session.use_only_cookies', false);
        ini_set('session.use_cookies', false);
        ini_set('session.use_trans_sid', false);
        ini_set('session.cache_limiter', null);

        # Open a session
        session_start();

        $oMember = unserialize($_SESSION['memberobject']);
        if (is_object($oMember) && is_a($oMember, "Member")) {
            $this->oMember = $oMember;
        } else {
            $this->oMember = new Member();
        }

        // oAuth
        $oAuth = new OauthApi();
        $oAuth->loadOAuth2Server();

        // Grab the user if token is valid
        if ($oAuth->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
            $this->oMember = new Member();

            $token = $oAuth->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
            $this->loggedIn = true;
            $this->memberID = $token['user_id'];
            $this->oMember->iMemberID = $this->memberID;
            $this->oMember->bIsLoggedIn = true;
        }

        if ($this->oMember->validateMember()) {
            $this->memberDetails = $this->oMember->getMemberDetails();
            $this->loggedIn = true;
            $this->memberID = $this->memberDetails['member_id'];
        }
    }

    /**
     * Check if the user is logged in
     */
    private function _requireLogin() {
        $this->_getMember();

        if (!$this->loggedIn) {
            $response['status'] = 400;
            $response['error'] = 'You need to be logged in to access this';

            # Pass over to output method
            $this->output($response);
            exit;
        }
    }

    /**
     * Change the email preferences for current user
     */
    public function emails() {

        $this->_getMember();

        # Clean our response into array(data, status, messages)
        $response = array(
            'status' => 200,
            'active' => $this->loggedIn
        );

        # Are we logged in or not?
        $this->_requireLogin();

        # Update one of them
        if (!empty($_POST['alert'])) {

            $_POST['alert'] = str_replace('results_', 'result_', $_POST['alert']);
            $_POST['alert'] = str_replace('jackpots_', 'jackpot_', $_POST['alert']);
            $_POST['value'] = ($_POST['value'] == 'true');

            # Single switch
            DAL::executeQuery("INSERT INTO members_alerts SET {$_POST['alert']} = '{$_POST['value']}', member_id = '{$this->memberDetails['member_id']}'
			ON DUPLICATE KEY UPDATE {$_POST['alert']} = '{$_POST['value']}'");

            # Force the user data update
            $this->oMember->forceMemberDetailUpdate();

            # Update SP record
            Silverpop::addModifyContactToMarketingDatabase($this->memberDetails['member_id'], true);

            # Re-save object
            $_SESSION['memberobject'] = serialize($this->oMember);

            # Save it
            session_write_close();
        }

        # Pass over to output method
        $this->output($response);
    }

    /**
     * Get or update the user details
     */
    public function index() {
        // Show an error if the user is not logged in
        $this->_requireLogin();

        $sRequestMethod = strtolower($_SERVER['REQUEST_METHOD']);

        // Update user details if request method is post
        if ($sRequestMethod === "post") {
            $aMemberDetails = $_POST;

            // Try to update the current user
            $updateMember = $this->oMember->saveMemberDetails($aMemberDetails);

            if (!$updateMember) {
                $this->error(array("error" => "Unable to update member"));
            }

            $this->output(array("message" => "User successfully updated"));
            return;
        }

        // Retrieve user details
        $this->output($this->memberDetails);
    }

    /**
     * Register a new user
     */
    public function register() {

        // Only allow post request
        $sRequestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        if ($sRequestMethod !== "post") {
            $this->error(array("error" => "Method not allowed"), 405);
        }

        $this->oMember = new Member();

        $aMemberDetails = $_POST;

        $bValid = true;

        // Check if email and password are valid
        if (!$this->oMember->checkUsernameAvailable($aMemberDetails['email'])) {
            $bValid = false;
            $this->error(array("error" => "E-Mail address is already in use."), 400);
        }
        if (strlen($aMemberDetails['password']) < 6) {
            $bValid = false;
            $this->error(array("error" => "Password too short. Needs to be 6 characters or more."), 400);
        }
        if (empty($aMemberDetails['email']) || !filter_var($aMemberDetails['email'], FILTER_VALIDATE_EMAIL)) {
            $bValid = false;
            $this->error(array("error" => "Email address is invalid."), 400);
            unset($aMemberDetails['email']);
        }

        // Attempt to register member
        $bRegister = $this->oMember->addMemberSlim($aMemberDetails['email'], $aMemberDetails['password']);

        // Assign boolean value from $bRegister to $bValid
        $bValid = $bRegister;

        if (!$bValid) {
            $this->error(array("error" => "An error occurred"));
        }

        http_response_code(201);
        $this->output(array("message" => "Registration successful"));
    }

    /**
     * Grab a list of bets or a single bet
     * @param array $params
     */
    public function bets($params = array()) {

        // Require the user to be logged in
        $this->_requireLogin();

        if (empty($params)) {

            // Get all bets for user
            $bets = $this->oMember->getBookingOrderItems(3, $iTotalTickets);
        } else {

            // Get specific bet for user
            $bets = $this->oMember->getBookingOrderItems(3, $iTotalTickets, $params[0]);
        }

        // Get a specific bet for user
        $this->output($bets);
    }

    /**
     * @param array $params
     */
    public function transactions($params = array()) {

        // Require the user to be logged in
        $this->_requireLogin();

        if (empty($params)) {

            // Get all transactions
            $transactions = $this->oMember->getTransactions($iTotalTransactions);
        } else {

            // Get specific transaction details
            $transactions = $this->oMember->getTransactions($iTotalTransactions, $params[0], 1);
        }

        $this->output($transactions);
    }

    /**
     * Run the Game Engine checks. Not yet implemented.
     *
     * @throws LottoException
     */
    public function run() {

        $this->error('Method not implemented');
    }

}
