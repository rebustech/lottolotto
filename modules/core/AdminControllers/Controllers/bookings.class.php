<?php
/**
 * Bookings admin controller. Provides output for the bookings page giving details of
 * any given booking along with any booking items.
 * @todo Add hedging
 * @todo Add winnings
 * @package LoveLotto
 * @subpackage AdminControllers
 */
class bookings extends AdminController{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        $aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

        if($iId>0){
            $tabs=new TabbedInterface();

            //Create a tab to show assigned users
            $oOrderItemsTab=new TabbedInterface(lang::get('booking_order_items'));
            $oOrderItemsTab->sIcon='fa-shopping-cart';
            //$oItemsTab->aItems[]=GenericTableAdmin::createGenericDataTable('vw_Items_with_role', 0, 'fk_access_role_id='.$iId, '', 100);
            $aOrderItemsData=DAL::executeQuery('SELECT * FROM booking_order_items WHERE fk_booking_id='.intval($iId));
            $oOrderItemsView=new OrderItemControl();
            $oOrderItemsView->data=$aOrderItemsData;
            $oOrderItemsTab->AddControl($oOrderItemsView);
            $tabs->aItems[]=$oOrderItemsTab;

            //Create a tab to show assigned users
            $oItemsTab=new TabbedInterface(lang::get('booking_items'));
            $oItemsTab->sIcon='fa-ticket';
            //$oItemsTab->aItems[]=GenericTableAdmin::createGenericDataTable('vw_Items_with_role', 0, 'fk_access_role_id='.$iId, '', 100);
            $aItemsData=DAL::executeQuery('SELECT * FROM booking_items WHERE fk_booking_id='.intval($iId));

            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('booking_items');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            //$oItemsTable->keepFields('admin_Items','username,firstname,lastname,email,isactive');
            $oItemsTab->AddControl($oItemsTable);
            $tabs->aItems['Items']=$oItemsTab;

            //Create a tab to show rights assigned to this role
            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';
            $tabs->aItems['rights']=$oLogTab;

            $aItems[]=$tabs->Output();
        }

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }
}