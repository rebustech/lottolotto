<?php
/**
 * Profiler admin controller. Used in development to check on the processes used
 * on any given pathway
 * @package LoveLotto
 * @subpackage DebuggingAndDocumentation
 */
class profiler extends AdminController{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){

        if($iId!=''){
            $tabs=new TabbedInterface();

            //Create a tab to show assigned users
            $oItemsTab=new TabbedInterface(lang::get('call_flow'));
            $oItemsTab->sIcon='fa-shopping-cart';

            $aItemsData=$this->ProcessFile(urldecode($iId));
            $oItemsTable=new TableControl($aItemsData);

            $oItemsTable->addField(new TableField('file','varchar'));
            $oItemsTable->addField(new TableField('called','varchar'));
            $oItemsTable->addField(new TableField('calls','int'));
            $oItemsTab->AddControl($oItemsTable);
            $tabs->aItems['Items']=$oItemsTab;

            $aItems['trace']=$tabs;
        }

        return $oItemsTable->Output();

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }

    /**
     * Displays a listing of all the profiler grind files
     * @param type $sTablename
     * @param type $iLangId
     * @param type $sFilters
     * @param type $sFileName
     * @param type $limit
     * @return string
     */
    function GenericListing($sTablename, $iLangId, $sFilters, $sFileName, $limit){
        $aData=$this->GetFiles();
        $sOut='<ul>';
        foreach($aData as $sFileName){
            $sOut.='<li><a href="/administration/generic-details.php?tablename=profiler&id='.urlencode($sFileName).'&pp=generic-listing.php">'.$sFileName.'</a></li>';
        }
        $sOut.='</ul>';
        return $sOut;
    }

    /**
     * Gets a list of all this files in the grind output path that can be examined
     * @param string $sPath Path to the output directory of xdebug
     * @return array
     */
    function GetFiles($sPath='c:/wamp/tmp/'){
        $rDir=opendir($sPath);
        $aData=array();
        while($sFileName=readdir($rDir)){
            if(strstr($sFileName,'.out')){
                $aData[]=$sFileName;
            }
        }
        return $aData;
    }

    /**
     * Processes a cachegrind file and shows us what methods were called along the
     * path to producing the final output. Ignores PHP internal calls
     * @param string $sFileName
     * @return array
     */
    function ProcessFile($sFileName){
        $sFileName='c:/wamp/tmp/'.$sFileName;
        $aData=file($sFileName);
        $aOut=array();
        $aPreviousParts=array('','');
        foreach($aData as $sLine){
            $aParts=explode('=',$sLine,2);
            if($aParts[0]=='cfn'){
                if(trim($aPreviousParts[1])!='php:internal' && trim($aParts[1])!='__autoload'){
                    $aItem=array();
                    $aItem['file']=$aPreviousParts[1];
                    $aItem['called']=$aParts[1];
                    if($aItem['file']==$aPreviousItem['file'] && $aItem['called']==$aPreviousItem['called']){
                        $aItem['calls']++;
                    }else{
                        $aItem['calls']=1;
                        $aOut[]=$aItem;
                        $aPreviousItem=$aItem;
                    }
                }
            }
            $aPreviousParts=$aParts;
        }

        return $aOut;
    }
}