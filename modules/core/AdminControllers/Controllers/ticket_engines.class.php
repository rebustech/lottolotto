<?php
/**
 * Ticket Engines admin controller. Generates the display for ticket engines including
 * custom configuration fields exposed by the provided ticket engine
 * @package LoveLotto
 * @subpackage AdminControllers
 */
class ticket_engines extends AdminController{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        
        $form=new SelfSubmittingForm();
        $form->submitToGeneric($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

        $tabs=new TabbedInterface();
        $form->AddControl($tabs);
        
        //Create a tab to show assigned users
        $oItemsTab=new TabbedInterface(lang::get('details'));
        $oItemsTab->sIcon='fa-magnet';
        $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);
        $tabs->aItems['Items']=$oItemsTab;

        if($iId>0){

            //Create a tab to show assigned users
            $oUsersTab=new TabbedInterface(lang::get('configuration'));
            $oUsersTab->sIcon='fa-cogs';

            $oTicketEngine=BaseTicketEngine::getById($iId);
            $oConfigControl=new ClassConfigurationControl($oTicketEngine);

            $oUsersTab->AddControl($oConfigControl);
            $tabs->aItems['config']=$oUsersTab;

            //Create a tab to show rights assigned to this role
            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';
            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'ticket_engines\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');
            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems['rights']=$oLogTab;
        }

            $aItems[]=$form->Output();

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }
    
    function afterGenericFormPost(){
        if(!is_numeric($_GET['id'])){
            throw new InvalidArgumentException('adminControllers.game_engines.afterGenericSave.IDNotNumeric');
        }
        
        $oTicketEngine=BaseTicketEngine::getById(intval($_GET['id']));
        $oTicketEngine->populateFromPost();
        $oTicketEngine->save();
    }
    
}