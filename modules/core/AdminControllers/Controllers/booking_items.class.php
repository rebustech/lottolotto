<?php
/**
 * Bookings admin controller. Provides output for the bookings page giving details of
 * any given booking along with any booking items.
 * @todo Add hedging
 * @todo Add winnings
 * @package LoveLotto
 * @subpackage AdminControllers
 */
class booking_items extends AdminController{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        if($iId>0){
            $tabs=new TabbedInterface();

            //Create a tab to show assigned users
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-shopping-cart';
            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);
            $tabs->aItems['Items']=$oItemsTab;

            //Create a tab to show rights assigned to this role
            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';
            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'booking_items_Model\' AND fk_id='.intval($iId));
            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,message');
            $oLogTab->AddControl($oItemsTable);


            $tabs->aItems['rights']=$oLogTab;

            $aItems[]=$tabs->Output();
        }

        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }
}