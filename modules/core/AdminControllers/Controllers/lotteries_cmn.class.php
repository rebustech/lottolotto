<?php
/**
 * members controller. Generates the display for an individual member inluding basic details
 * KYC, contact details, order history, contact history
 * @package LoveLotto
 * @subpackage AdminControllers
 * @author Jonathan Patchett
 */
class lotteries_cmn{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);

        $oLottery=  Lottery::getById($iId);

        $form=new SelfSubmittingForm();
        $form->submitToGeneric($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

        $tabs=new TabbedInterface();
        $form->AddControl($tabs);

        $oDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oDetailsTab->sIcon='fa-user';
        $oDetailsTab->aItems['main']=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);

        $tabs->aItems['details']=$oDetailsTab;

        /**
         * IMAGES TAB
         *
         * This contains just a bunch of file upload controls for each of the images
         * for a lottery
         */
        $oImagesTab=new TabbedInterface(lang::get('images'));
        $oImagesTab->sIcon='fa-image';

        $oResultsImageControl=new FileUploadControl('Results Page Image','images/products/results_'.$oLottery->lottery_id,'results');
        if($oLottery->image_results!='') $oResultsImageControl->sPreviewFile=$oLottery->image_results;
        $oImagesTab->AddControl($oResultsImageControl);

        $oSmallImageControl=new FileUploadControl('Small No Text','images/products/lottery_sm_'.$oLottery->lottery_id,'lottery_sm');
        if($oLottery->image_lottery_sm!='') $oSmallImageControl->sPreviewFile=$oLottery->image_lottery_sm;
        $oImagesTab->AddControl($oSmallImageControl);

        $oSmallTextImageControl=new FileUploadControl('Small With Text','images/products/lottery_sm_text_'.$oLottery->lottery_id,'lottery_sm_text');
        if($oLottery->image_lottery_sm_text!='') $oSmallTextImageControl->sPreviewFile=$oLottery->image_lottery_sm_text;
        $oImagesTab->AddControl($oSmallTextImageControl);

        $oLargerImageControl=new FileUploadControl('Larger No Text','images/products/lottery_lg_'.$oLottery->lottery_id,'lottery_lg');
        if($oLottery->image_lottery_lg!='') $oLargerImageControl->sPreviewFile=$oLottery->image_lottery_lg;
        $oImagesTab->AddControl($oLargerImageControl);

        $oLargerTextImageControl=new FileUploadControl('Larger With Text','images/products/lottery_lg_text_'.$oLottery->lottery_id,'lottery_lg_text');
        if($oLottery->image_lottery_lg_text!='') $oLargerTextImageControl->sPreviewFile=$oLottery->image_lottery_lg_text;
        $oImagesTab->AddControl($oLargerTextImageControl);


        $tabs->aItems['images']=$oImagesTab;


        /**
         * PRIZE DIVISIONS TAB
         */
        $oBookingsTab=new TabbedInterface(lang::get('prize_divisions'));
        $oBookingsTab->sIcon='fa-sort-amount-desc';
        $aBookingsData=DAL::executeQuery('SELECT * FROM lottery_divisions WHERE fk_lottery_id='.intval($iId));
        $oBookingsTable=new TableControl($aBookingsData);
        $oBookingsTable->getFieldsFromTable('lottery_divisions');
        $oBookingsTable->keepFields('lottery_divisions','name,odds,avg_payout,match_main,match_extra');
        $oBookingsTab->AddControl(new ToolbarButtonControl(lang::get('add_prize_division'),'/administration/generic-details.php?tablename=lottery_divisions&fk_lottery_id='.intval($iId)));
        $oBookingsTab->AddControl($oBookingsTable);

        $tabs->aItems['bookings']=$oBookingsTab;

        $oTransactionsTab=new TabbedInterface(lang::get('draw_dates'));
        $oTransactionsTab->sIcon='fa-calendar';
        $aTransactionsData=DAL::executeQuery('SELECT * FROM r_lottery_dates WHERE drawdate>now() AND fk_lottery_id='.intval($iId));
        $oTransactionsTable=new TableControl($aTransactionsData);
        $oTransactionsTable->getFieldsFromTable('r_lottery_dates');
        //$oTransactionsTable->keepFields('transactions','id,gatewayreference,bookingreference,amount,status,transaction_date,confirmed');
        $oTransactionsTab->AddControl(new ToolbarButtonControl(lang::get('add_draw'),'/administration/generic-details.php?tablename=lottery_draws&fk_lottery_id='.intval($iId)));
        $oTransactionsTab->AddControl($oTransactionsTable);
        $tabs->aItems['transactions']=$oTransactionsTab;

        $oResultsTab=new TabbedInterface(lang::get('past_results'));
        $oResultsTab->sIcon='fa-calendar-o';
        $aResultsData=DAL::executeQuery('SELECT * FROM lottery_draws WHERE fk_lotterydate<now() AND fk_lottery_id='.intval($iId));
        $oResultsTable=new TableControl($aResultsData);
        $oResultsTable->getFieldsFromTable('lottery_draws');
        //$oTransactionsTable->keepFields('transactions','id,gatewayreference,bookingreference,amount,status,transaction_date,confirmed');
        $oResultsTab->AddControl($oResultsTable);
        $tabs->aItems['results']=$oResultsTab;

        $oWinningsTab=new TabbedInterface(lang::get('winners'));
        $oWinningsTab->sIcon='fa-trophy';
        $aWinningsData=DAL::executeQuery('SELECT * FROM lottery_winnings WHERE fk_lottery_id='.intval($iId));
        $oWinningsTable=new TableControl($aWinningsData);
        $oWinningsTable->getFieldsFromTable('lottery_winnings');
        //$oTransactionsTable->keepFields('transactions','id,gatewayreference,bookingreference,amount,status,transaction_date,confirmed');
        $oWinningsTab->AddControl($oWinningsTable);
        $tabs->aItems['winnings']=$oWinningsTab;

        //Create a tab to show rights assigned to this role
        $oContactLogTab=new TabbedInterface(lang::get('risk_management'));
        $oContactLogTab->sIcon='fa-flask';
        $aContactLogData=DAL::executeQuery('SELECT * FROM ticket_engines WHERE fk_lottery_id='.intval($iId).' order by effective_date,threshold_max');
        $oContactLogTable=new TableControl($aContactLogData);
        $oContactLogTable->getFieldsFromTable('ticket_engines');
        $oContactLogTable->keepFields('ticket_engines', 'name,comments,handler,threshold_min,threshold_max,effective_date');
        $oContactLogTab->AddControl(new ToolbarButtonControl(lang::get('add_risk_model'),'/administration/generic-details.php?tablename=ticket_engines&fk_lottery_id='.intval($iId)));
        $oContactLogTab->AddControl($oContactLogTable);
        $tabs->aItems['contact']=$oContactLogTab;

        //Create a tab to show rights assigned to this role
        $oLogTab=new TabbedInterface(lang::get('audit_log'));
        $oLogTab->sIcon='fa-th-list';
        $tabs->aItems['rights']=$oLogTab;

        $aItems[]=$form->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }

    function afterGenericFormPost(){
        if(!is_numeric($_GET['id'])){
            throw new InvalidArgumentException('adminControllers.game_engines.afterGenericSave.IDNotNumeric');
        }

        $oLottery=Lottery::getById($_GET['id']);


        /**
         * Upload all the images to their relevant place
         * the FileUploadControl helps with this by providing an upload method.
         * By using a constructor with the same parameters as on the admin controller
         * display method you can make sure that everything will match up for
         * both the display and the save.
         *
         * After saving the filename of the uploaded file is assigned to the relevant
         * property of the lottery object, which is then saved.
         */
        $oResultsImageControl=new FileUploadControl('Results Page Image','images/products/results_'.$_GET['id'],'results');
        if($oResultsImageControl->doUpload($oLottery)){
            $oLottery->image_results=$oResultsImageControl->sStoredFilename;
            $aData=array('image_results'=>$oLottery->image_results);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oSmallImageControl=new FileUploadControl('Small No Text','images/products/lottery_sm_'.$_GET['id'],'lottery_sm');
        if($oSmallImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_sm=$oSmallImageControl->sStoredFilename;
            $aData=array('image_lottery_sm'=>$oLottery->image_lottery_sm);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oSmallTextImageControl=new FileUploadControl('Small With Text','images/products/lottery_sm_text_'.$_GET['id'],'lottery_sm_text');
        if($oSmallTextImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_sm_text=$oSmallTextImageControl->sStoredFilename;
            $aData=array('image_lottery_sm_text'=>$oLottery->image_lottery_sm_text);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oLargerImageControl=new FileUploadControl('Larger No Text','images/products/lottery_lg_'.$_GET['id'],'lottery_lg');
        if($oLargerImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_lg=$oLargerImageControl->sStoredFilename;
            $aData=array('image_lottery_lg'=>$oLottery->image_lottery_lg);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oLargerTextImageControl=new FileUploadControl('Larger With Text','images/products/lottery_lg_text_'.$_GET['id'],'lottery_lg_text');
        if($oLargerTextImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_lg_text=$oLargerTextImageControl->sStoredFilename;
            $aData=array('image_lottery_lg_text'=>$oLottery->image_lottery_lg_text);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

    }
}