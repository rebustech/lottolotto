<?php
/**
 * @package LoveLotto
 * @subpackage AdminControllers
 * @author Jonathan Patchett
 */
class transactions{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);
        $oTransaction=\LL\Transaction::getById($iId);
        $oTransaction->loadWinnerInformation();
        
        $tabs=new TabbedInterface();

        $oDetailsTab=new TabbedInterface(lang::get('member_details'));
        $oDetailsTab->sIcon='fa-money';
        $oDetailsTab->aItems['main']=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);

        $tabs->aItems['details']=$oDetailsTab;

        $oWinningInfoTab=new TabbedInterface(lang::get('winning'));
        $oWinningInfoTab->sIcon='fa-trophy';
        $oWinningInfoTab->aItems[]=GenericTableAdmin::createGenericDetailsForm('lottery_winnings', $oTransaction->fk_bookingitem_id , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,true);
        
        $tabs->aItems[]=$oWinningInfoTab;
        
        
        /**
         * BOOKING ITEM INFO
         */
        $oBookingInfoTab=new TabbedInterface(lang::get('booking_item'));
        $oBookingInfoTab->sIcon='fa-ticket';
        $oBookingInfoTab->aItems['main']=GenericTableAdmin::createGenericDetailsForm('booking_items', $oTransaction->fk_bookingitem_id , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);

        $tabs->aItems[]=$oBookingInfoTab;

        
        
        /**
         * ORDER INFO
         */
        $oBookingTab=new TabbedInterface(lang::get('order'));
        $oBookingTab->sIcon='fa-shopping-cart';
        $oBookingTab->aItems['main']=GenericTableAdmin::createGenericDetailsForm('bookings', $oTransaction->booking_id , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false);
                
        $tabs->aItems[]=$oBookingTab;        

        /**
         * CUSTOMER TAB
         */
        $oCustomerTab=new TabbedInterface(lang::get('customer'));
        $oCustomerTab->sIcon='fa-user';
        
        // We can now pass an array to the form creator to indicate whether to show fields
        // and make them readonly
        $aDisplayOptions = array('username' => true,
                                 'email'    => true,
                                 'balance'  => true,
                                 'firstname' => true,
                                 'lastname' => true,
                                 'contact'  => true,
                                 'address1' => true,
                                 'address2' => true,
                                 'city'     => true,
                                 'zip'      => true,
                                 'fk_country_id' => true,
                                 'verified' => true,
                                 'join_date' => true,
                                 'is_active' => true,
                                 'fk_affiliate_id' => true,
                                 'email_generaloffers' => true,
                                 'email_lotterydraws' => true,
                                 'dob'       => true,
                                 'title'     => true,
                                 'fk_source_advert_id' => true,
                                 'monthly_deposit_limit' => true,
                                 'is_suspended' => true,
                                 'suspension_start' => true,
                                 'suspension_end' => true,
                                 'suspension_lifted' => true);        

        $oCustomerTab->aItems['main']=GenericTableAdmin::createGenericDetailsForm('members', $oTransaction->fk_member_id , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID,false,$aDisplayOptions);


        $tabs->aItems['images']=$oCustomerTab;
        

        //Create a tab to show rights assigned to this role
        $oKYCTab=new TabbedInterface(lang::get('member_fraud'));
        $oKYCTab->sIcon='fa-gavel';
        $oKYCTab->aItems[]=GenericTableAdmin::createGenericDetailsForm('members_kyc', $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID, false);
        $oCustomerTab->aItems['kyc']=$oKYCTab;
        
        /**
         * CUSTOMER TRANSACTION HISTORY
         */
        $oBookingsTab=new TabbedInterface(lang::get('member_bookings'));
        $oBookingsTab->sIcon='fa-rocket';
        $aBookingsData=DAL::executeQuery('SELECT * FROM bookings WHERE fk_member_id='.$oTransaction->fk_member_id);
        $oBookingsTable=new TableControl($aBookingsData);
        $oBookingsTable->getFieldsFromTable('bookings');
        //$oTransactionsTable->keepFields('admin_users','username,firstname,lastname,email,isactive');
        $oBookingsTab->AddControl(new ToolbarButtonControl(lang::get('add_booking'),'#'));
        $oBookingsTab->AddControl($oBookingsTable);

        $tabs->aItems['bookings']=$oBookingsTab;
        
        

        $oTransactionsTab=new TabbedInterface(lang::get('member_transactions'));
        $oTransactionsTab->sIcon='fa-credit-card';
        $aTransactionsData=DAL::executeQuery('SELECT * FROM transactions WHERE fk_member_id='.$oTransaction->fk_member_id);
        $oTransactionsTable=new TableControl($aTransactionsData);
        $oTransactionsTable->getFieldsFromTable('transactions');
        $oTransactionsTable->keepFields('transactions','id,gatewayreference,bookingreference,amount,status,transaction_date,confirmed');
        $oTransactionsTab->AddControl(new ToolbarButtonControl(lang::get('add_transaction'),'#'));
        $oTransactionsTab->AddControl($oTransactionsTable);
        $tabs->aItems['transactions']=$oTransactionsTab;        
                

        //Create a tab to show rights assigned to this role
        $oLogTab=new TabbedInterface(lang::get('audit_log'));
        $oLogTab->sIcon='fa-th-list';
        $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE (model=\'transactions\' AND fk_id='.intval($iId).') OR (model=\'lottery_winnings\' AND fk_id='.$oTransaction->fk_bookingitem_id.') OR (model=\'booking_items\' AND fk_id='.$oTransaction->fk_bookingitem_id.')');
        $oItemsTable=new TableControl($aItemsData);
        $oItemsTable->getFieldsFromTable('audit_log');
        //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
        $oItemsTable->keepFields('audit_log','datetime,model,message');
        $oLogTab->AddControl($oItemsTable);

        $tabs->aItems[]=$oLogTab;        
        
        $aItems[]=$tabs->Output();


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }
        return $sOut;
    }

    function afterGenericFormPost(){
        if(!is_numeric($_GET['id'])){
            throw new InvalidArgumentException('adminControllers.game_engines.afterGenericSave.IDNotNumeric');
        }

        $oLottery=Lottery::getById($_GET['id']);


        /**
         * Upload all the images to their relevant place
         * the FileUploadControl helps with this by providing an upload method.
         * By using a constructor with the same parameters as on the admin controller
         * display method you can make sure that everything will match up for
         * both the display and the save.
         *
         * After saving the filename of the uploaded file is assigned to the relevant
         * property of the lottery object, which is then saved.
         */
        $oResultsImageControl=new FileUploadControl('Results Page Image','images/products/results_'.$_GET['id'],'results');
        if($oResultsImageControl->doUpload($oLottery)){
            $oLottery->image_results=$oResultsImageControl->sStoredFilename;
            $aData=array('image_results'=>$oLottery->image_results);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oSmallImageControl=new FileUploadControl('Small No Text','images/products/lottery_sm_'.$_GET['id'],'lottery_sm');
        if($oSmallImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_sm=$oSmallImageControl->sStoredFilename;
            $aData=array('image_lottery_sm'=>$oLottery->image_lottery_sm);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oSmallTextImageControl=new FileUploadControl('Small With Text','images/products/lottery_sm_text_'.$_GET['id'],'lottery_sm_text');
        if($oSmallTextImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_sm_text=$oSmallTextImageControl->sStoredFilename;
            $aData=array('image_lottery_sm_text'=>$oLottery->image_lottery_sm_text);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oLargerImageControl=new FileUploadControl('Larger No Text','images/products/lottery_lg_'.$_GET['id'],'lottery_lg');
        if($oLargerImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_lg=$oLargerImageControl->sStoredFilename;
            $aData=array('image_lottery_lg'=>$oLottery->image_lottery_lg);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

        $oLargerTextImageControl=new FileUploadControl('Larger With Text','images/products/lottery_lg_text_'.$_GET['id'],'lottery_lg_text');
        if($oLargerTextImageControl->doUpload($oLottery)){
            $oLottery->image_lottery_lg_text=$oLargerTextImageControl->sStoredFilename;
            $aData=array('image_lottery_lg_text'=>$oLottery->image_lottery_lg_text);
            \DAL::Update('lotteries_cmn', $aData, 'lottery_id='.$_GET['id']);
        }

    }
}