<?php

/**
 * Access roles controller. Generates the display for managing access roles along with tabs for
 * assigned users and privileges granted to any given role.
 * @package LoveLotto
 * @subpackage AdminControllers
 */
class access_roles extends AdminController implements IAdminController {

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL) {
        $sFileName = basename($_SERVER['PHP_SELF']);

        $aItems[] = GenericTableAdmin::createGenericDetailsForm($sTablename, $iId, $aDefaultData, $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

        if ($iId > 0) {
            $tabs = new TabbedInterface();

            //Create a tab to show assigned users
            $oUsersTab = new TabbedInterface(lang::get('assigned_users'));
            $oUsersTab->sIcon = 'fa-users';
            //$oUsersTab->aItems[]=GenericTableAdmin::createGenericDataTable('vw_users_with_role', 0, 'fk_access_role_id='.$iId, '', 100);
            $aUsersData = DAL::executeQuery('SELECT * FROM vw_users_with_role WHERE fk_access_role_id=' . intval($iId));

            $oUsersTable = new TableControl($aUsersData);
            $oUsersTable->getFieldsFromTable('admin_users');
            $oUsersTable->addField(new TableField('admintype', 'varchar', 'User Type', ''));
            $oUsersTable->keepFields('admin_users', 'username,firstname,lastname,email,isactive');
            $oUsersTab->AddControl($oUsersTable);
            $oUsersTab->AddControl(new ToolbarButtonControl(lang::get('add_user'), '#'));
            $tabs->aItems['users'] = $oUsersTab;

            //Create a tab to show rights assigned to this role
            $oRightsTab = new TabbedInterface(lang::get('access_rights'));
            $oRightsTab->sIcon = 'fa-key';
            $oData = DAL::executeQuery('SELECT * FROM vw_roles_rights WHERE role_id=' . $iId);
            $oRightsTab->aItems[] = new AccessControlControl($oData);
            $tabs->aItems['rights'] = $oRightsTab;

            $aItems[] = $tabs->Output();
        }

        $sOut = '';
        foreach ($aItems as $item) {
            $sOut.=$item;
        }
        return $sOut;
    }

}
