<form class="detailsform">
<?php

foreach($this->data as $oItem):

    $oGameEngine=GameEngine::getById($oItem['fk_game_engine_id']);

    $config=json_decode($oItem['config']);

?>

<h3 style="clear:left"><?=$oGameEngine->name?></h3>

<?php foreach($config as $k=>$v): ?>

<?php
    switch($k){

        case 'priceFormatted':
        case 'drawDate':
        case 'daysOfDraw':
        case 'gameType':
            echo '<label class="halfsize"><span>'.$k.'</span><div><input type="text" name="bookingreference" id="field_bookingreference" class="halfsize" readonly value="'.$v.'"></div></label>';
            break;
        case 'boards':
            echo '<label class="halfsize"><span>'.$k.'</span><div>';
            foreach($v as $board){
                echo '<input type="text" name="bookingreference" id="field_bookingreference" class="halfsize" readonly value="';
                echo implode(',',$board->numbers);
                echo '"><br/>';
            }
            echo '</div></label>';
            break;
        case 'extras':
            echo '<label class="halfsize"><span>'.$k.'</span><div>';
            echo '<input type="text" name="bookingreference" id="field_bookingreference" class="halfsize" readonly value="';
            echo implode(',',$v);
            echo '"><br/>';
            echo '</div></label>';
            break;
        case 'games':
            echo '<label class="halfsize"><span>'.$k.'</span><div>';
            echo '<input type="text" name="bookingreference" id="field_bookingreference" class="halfsize" readonly value="';
            echo implode(',',$v);
            echo '"><br/>';
            echo '</div></label>';
            break;
        case 'isSubscription':
        case 'protectJackpot':
        case 'jackpotOnly':
        case 'quickPlay':
            echo '<label class="halfsize"><span>'.$k.'</span><div>';
            echo '<input type="text" name="bookingreference" id="field_bookingreference" class="halfsize" readonly value="';
            echo ($v=='true')?'Yes':'No';
            echo '">';
            echo '</div></label>';
            break;
    }

?>
<?php endforeach; ?>

<?php endforeach; ?>
</form>
