<?php

class OrderItemControl extends Control{
    public $sView='orderitemcontrol';

    /**
     * Render the View
     * @return string
     */
    public function output(){
        $oView=new LLView();
        $oView->data=$this->data;

        return $oView->output($this->sView);
    }
}