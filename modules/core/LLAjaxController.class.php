<?php
/**
 * Base class for creating ajax controllers. Ajax calls via ajaxRouter will only work
 * if they are derived from ajax controller.
 *
 * At the moment this class is just for semantic purposes and also for performing
 * the above check, however we should look at adding logging, tracing and
 * security to this class at a later date
 *
 * @package LoveLotto
 * @subpackage Core
 * @author J.Patchett
 */

namespace LL;

class AjaxController extends Controller{
    const OUTPUT_METHOD_XML=1;
    const OUTPUT_METHOD_JSON=2;
    const OUTPUT_METHOD_PHP=3;

    function output($mData=null,$iMethod=null){
        if($mData===null) $mData=$this;
        if($iMethod===null) $iMethod=$this->determineOutputMethod();
        if(is_object($mData)){
            $mData->messages=\LL\API\Message::$aMessages;
        }
        if(is_array($mData)){
            $mData['messages']=\LL\API\Message::$aMessages;
        }

        switch($iMethod){
            case self::OUTPUT_METHOD_XML:
                header('Content-type: text/xml');
                die(\XMLSerializer::generateValidXml($mData));
                break;
            case self::OUTPUT_METHOD_PHP:
                header('Content-type: text');
                die(serialize($mData));
                break;
            default:
                //NOTE: NO HEADER HERE AS IT IS STILL IN ajaxRouter for backwards compatability
                die(json_encode($mData));
                break;
        }
    }

    public function error($aMessage, $sErrorCode = 500) {
        http_response_code($sErrorCode);
        die(json_encode($aMessage));
    }

    /**
     * Gets the correct output method from the querystring, or defaults to JSON
     * @return integer from self::OUTPUT_METHOD_XML, self::OUTPUT_METHOD_PHP or
     *                 self::OUTPUT_METHOD_JSON
     */
    protected function determineOutputMethod(){
        $iMethod=self::OUTPUT_METHOD_JSON;
        if($_GET['format']){
            switch (strtolower($_GET['format'])){
                case 'xml':
                    $iMethod=self::OUTPUT_METHOD_XML;
                    break;
                case 'php':
                    $iMethod=self::OUTPUT_METHOD_PHP;
                    break;
                default:
                    $iMethod=self::OUTPUT_METHOD_JSON;
                    break;
            }
        }
        return $iMethod;
    }

    protected function _getFiles($aFiles){
        $sData='';
        foreach($aFiles as $sFile){
            if(!file_exists($sFile)) die('File '.$sFile.' not found');
            $sData.=file_get_contents($sFile)."\r\n";
        }
        return $sData;
    }

}