<?php

namespace LL\Results;

class Installer{
    use \LL\Installer\TInstaller;

    static function getInstallTasks(){
        $aJobs[]=new \LL\Installer\Task('\LL\Results\Installer', 'installResultsModule', 'Install results entry module');
        return $aJobs;
    }

    static function installResultsModule(){
        $oTable=new \LL\Installer\Table('lottery_winnings');
        $oTable->addField(new \LL\Installer\Field('checked', 'tinyint',4));
        $oTable->addField(new \LL\Installer\Field('checked_by', 'int',11));
        $oTable->addField(new \LL\Installer\Field('notes', 'text'));
        $oTable->compile();
        
        $iAdminSectionId=\DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='Results'");
        //Get the ID of the admin section
        $aData=array('title'=>'MF Results',
                     'filename'=>'add-results-checker-all.php',
                     'parent_id'=>$iAdminSectionId,
                     'icon'=>'fa-edit');
        
        if(\DAL::Query("SELECT * FROM admin_pages WHERE filename='add-results-checker-all.php'")==0){
            \DAL::Insert('admin_pages', $aData);
        }else{
            \DAL::Update('admin_pages', $aData, "title='MF Results'"); 
        }

        $aData=array('title'=>'Winscan Processing',
                     'filename'=>'/administration/resultsManager/pendingWinnings',
                     'parent_id'=>$iAdminSectionId,
                     'icon'=>'fa-edit');
        
        if(\DAL::Query("SELECT * FROM admin_pages WHERE filename='/administration/resultsManager/pendingWinnings'")==0){
            \DAL::Insert('admin_pages', $aData);
        }else{
            \DAL::Update('admin_pages', $aData, "title='Winscan Processing'"); 
        }
        
        
        
        self::installModule('Multi factor results module');
    }
    
}