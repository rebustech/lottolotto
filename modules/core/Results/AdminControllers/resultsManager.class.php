<?php

class resultsManager extends AdminController{
    var $aDefaultLevels=array(600,1000,2000,5000,10000,20000,100000,1000000,9999999999999);
    
    
    function pendingWinnings(){
        $aLevels=$this->aDefaultLevels;
        
        $sSQL='SELECT 
                        b.fk_lottery_id,
                        lo.comment,
                        b.fk_lotterydate,';
        
        $iPreviousLevel=0;
        foreach($aLevels as $iLevel){            
            $sSQL.='COALESCE(SUM(t.amount>'.$iPreviousLevel.' AND t.amount<'.$iLevel.' AND checked=1),0) AS c'.$iPreviousLevel.',
                    COALESCE(SUM(t.amount>'.$iPreviousLevel.' AND t.amount<'.$iLevel.'),0) AS b'.$iPreviousLevel.',';


            $iPreviousLevel=$iLevel;
        }
        
        $sSQL.='1'; //This just ties off that final comma
        
        $sSQL.='
                        FROM transactions t
                        INNER JOIN lottery_winnings l ON t.transaction_id=l.fk_transaction_id
                        INNER JOIN booking_items b ON l.fk_bookingitem_id=b.bookingitem_id
                        INNER JOIN lotteries_cmn lo ON lo.lottery_id=b.fk_lottery_id
                        WHERE fk_gateway_id=7 AND confirmed=0
                        GROUP BY    b.fk_lottery_id,
                                    b.fk_lotterydate
                        ORDER BY    b.fk_lotterydate, 
                                    b.fk_lottery_id
                                    
                                    ';

                
        $aSummary=\DAL::executeQuery($sSQL);

        $oView=new LLView();
        
        $oView->aLevels=$aLevels;
        $oView->aSummary=$aSummary;

        return $oView->output('pending_winnings');
        
    }
  
    
}