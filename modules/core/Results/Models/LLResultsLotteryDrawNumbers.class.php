<?php

namespace LL\Results;

class LotteryDrawNumbers extends \LLModel{
    static $sTableName='lottery_draw_numbers';
    static $sKeyField='id';
    static $sHandlerField='';

    public $fk_lottery_draw_id;
    public $fk_ball_type_id;
    public $ball_number;
    public $ball_sequence;

    function save($aData=array()){
        $aData['fk_lottery_draw_id']=$this->fk_lottery_draw_id;
        $aData['fk_ball_type_id']=$this->fk_ball_type_id;
        $aData['ball_number']=$this->ball_number;
        $aData['ball_sequence']=$this->ball_sequence;
        parent::save($aData);
    }
}