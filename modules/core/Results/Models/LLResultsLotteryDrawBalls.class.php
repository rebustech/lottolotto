<?php

namespace LL\Results;

class LotteryDrawBalls extends \LLModel{
    public $aLotteryDrawResults=array();
    public $oLottery;
    public $iSequence=0;
    public $fk_lottery_draw_id;

    const BALL=1;
    const BONUS_BALL=2;

    function addBall($iNumber){
        $this->iSequence++;
        $iType=($this->iSequence>$this->oLottery->iNumberCount)?self::BONUS_BALL:self::BALL;

        $oLotteryDrawNumbers=new LotteryDrawNumbers();
        $oLotteryDrawNumbers->fk_ball_type_id=$iType;
        $oLotteryDrawNumbers->fk_lottery_draw_id=$this->fk_lottery_draw_id;
        $oLotteryDrawNumbers->ball_number=$iNumber;
        $oLotteryDrawNumbers->ball_sequence=$this->iSequence;

        $this->aLotteryDrawResults[]=$oLotteryDrawNumbers;
    }

    /**
     * This has to save the indepenant data and the cached data object against the draw
     */
    function prepareSave(){
        /**
         * Save the individual balls
         * Create a serialised version to save against the draw at the same time
         */
        $aBallNumbers=array();
        foreach($this->aLotteryDrawResults as $oLotteryDrawResult){
            $oLotteryDrawResult->save();
            $aBallNumbers[]=$oLotteryDrawResult->ball_number;
        }

        $sBallNumbers=implode('|',$aBallNumbers);

        return $sBallNumbers;
    }


    function save(){
        $sBallNumbers=$this->prepareSave();
        $sSQL="UPDATE lottery_draws SET numbers='$sBallNumbers' WHERE id=".$this->fk_lottery_draw_id;
        \DAL::executeQuery($sSQL);
    }

    function saveToResults($sLotteryDate){
        $sBallNumbers=$this->prepareSave();
        $sSQL="UPDATE lottery_results_checking SET numbers='$sBallNumbers' WHERE date(fk_lottery_date)=date('".$sLotteryDate."') AND checker_id=541 AND fk_lottery_id=".$this->oLottery->iLotteryID;
        \DAL::executeQuery($sSQL);
    }
}