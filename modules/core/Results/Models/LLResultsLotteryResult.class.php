<?php

namespace LL\Results;

class LotteryResult extends \LLModel{
    var $oDrawBalls;
    var $oDrawPrizes;
    var $bIsRollover;
    var $fJackpot;
    var $draw_id;
    var $sDrawNumber;
    var $sLotteryDate;

    var $oLottery;
    var $aTiers;

    function __construct($draw_id,$oLottery) {
        $this->draw_id=$draw_id;

        $this->oDrawBalls=new LotteryDrawBalls();
        $this->oDrawBalls->fk_lottery_draw_id=$draw_id;
        $this->oDrawBalls->oLottery=$oLottery;

        $this->oLottery=$oLottery;
        $this->aTiers=\LL\GameEngines\PrizeTier::getLotteryDivisons($oLottery->iLotteryID);

        $this->oDrawPrizes=new LotteryDrawPrizes();
        $this->oDrawPrizes->oLottery=$oLottery;
        $this->oDrawPrizes->aTiers=$this->aTiers;
        $this->oDrawPrizes->fk_lottery_draw_id=$draw_id;

    }

    function save(){
        $this->oDrawBalls->save();
    }

    function savePrizes(){
        $this->oDrawPrizes->save();
    }

    function saveResults(){
        $this->oDrawBalls->saveToResults($this->sLotteryDate);
        $this->oDrawPrizes->saveToResults($this->sLotteryDate);        
    }
}