<?php

namespace LL\Results;

class LotteryDrawPrizes extends \LLModel{
    public $aLotteryDrawPrizes=array();
    public $oLottery;
    public $fk_lottery_draw_id;
    public $fJackpot;

    var $aTiers;

    function addPrize($prize_tier_number,$total_winners,$prize_per_winner){

        $oLotteryDrawPrize=new LotteryDrawPrize();
        $oLotteryDrawPrize->fk_lottery_draw_id=$this->fk_lottery_draw_id;
        $oLotteryDrawPrize->prize_tier_number=$prize_tier_number;
        $oLotteryDrawPrize->total_winners=$total_winners;
        $oLotteryDrawPrize->prize_per_winner=$prize_per_winner;

        $this->aLotteryDrawPrizes[]=$oLotteryDrawPrize;
    }

    /**
     * This has to save the indepenant data and the cached data object against the draw
     */
    function prepareSave($iTierOffset=0){
        /**
         * Save the individual balls
         * Create a serialised version to save against the draw at the same time
         */
        $aPrizes=array();
        foreach($this->aLotteryDrawPrizes as $oLotteryDrawPrize){
            $oLotteryDrawPrize->save();

            /**
             * Prepare to save the old way this is tricky because the new way
             * just saves the tier number, but the old way also stores the number
             * of numbers/bonus balls matches, so we'll have to do a look up
             * against lottery_divisions table
             */
            $oTier=$this->aTiers[$oLotteryDrawPrize->prize_tier_number+$iTierOffset];

            $aPrize=array();
            $aPrize['prize']=$oLotteryDrawPrize->prize_per_winner;
            $aPrize['match']=$oTier->match_main;
            $aPrize['bonus']=$oTier->match_extra;
            $aPrize['winners']=$oLotteryDrawPrize->total_winners;

            $aPrizes[$oLotteryDrawPrize->prize_tier_number]=$aPrize;
        }

        $sPrizeDetails=serialize($aPrizes);

        $sBallPrizes=implode('|',$aBallPrize);

        return $sPrizeDetails;
    }

    function save(){
        $sPrizeDetails=$this->prepareSave();
        $sSQL="UPDATE lottery_draws SET winnings='$sPrizeDetails', jackpot='".$this->fJackpot."' WHERE id=".$this->fk_lottery_draw_id;
        echo $sSQL;
        \DAL::executeQuery($sSQL);
    }

    function saveToResults($sLotteryDate){
        $sPrizeDetails=$this->prepareSave();
        $fJackpot=$this->fJackpot;
        $bRollover=($this->aLotteryDrawPrizes[0]->total_winners==0)?1:0;
        $sSQL="UPDATE lottery_results_checking SET winnings='$sPrizeDetails', next_jackpot_amount='$fJackpot',next_jackpot_rollover='$bRollover' WHERE date(fk_lottery_date)=date('".$sLotteryDate."') AND checker_id=541 AND fk_lottery_id=".$this->oLottery->iLotteryID;
        \DAL::executeQuery($sSQL);
    }
}