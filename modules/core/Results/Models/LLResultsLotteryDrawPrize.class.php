<?php

namespace LL\Results;

class LotteryDrawPrize extends \LLModel{
    static $sTableName='lottery_draw_breakdowns';
    static $sKeyField='id';
    static $sHandlerField='';

    public $fk_lottery_draw_id;
    public $prize_tier_number;
    public $total_winners;
    public $prize_per_winner;

    function save($aData=array()){
        $aData['fk_lottery_draw_id']=$this->fk_lottery_draw_id;
        $aData['prize_tier_number']=$this->prize_tier_number;
        $aData['total_winners']=$this->total_winners;
        $aData['prize_per_winner']=$this->prize_per_winner;
        parent::save($aData);
    }
}