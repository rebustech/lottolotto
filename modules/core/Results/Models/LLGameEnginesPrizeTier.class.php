<?php

namespace LL\GameEngines;

class PrizeTier extends \LLModel{
    static $sTableName='lottery_divisions';
    static $sKeyField='id';
    static $sHandlerField='';


    var $fk_lottery_id;
    var $name;
    var $odds;
    var $avg_payout;
    var $match_main;
    var $match_extra;
    var $payout_handler;
    var $tier_number;

    static function getLotteryDivisons($iLotteryId){
        $aData=\DAL::executeQuery('SELECT * FROM `lottery_divisions` WHERE fk_lottery_id='.$iLotteryId);
        foreach($aData as $aRow){
            $oPrize=new PrizeTier();
            $oPrize->populateFromArray($aRow);
            $aOut[$aRow['tier_number']]=$oPrize;
        }
        return $aOut;
    }

    function save($aData=array()){
        $aData['fk_lottery_id']=$this->fk_lottery_id;
        $aData['name']=$this->name;
        $aData['odds']=$this->odds;
        $aData['avg_payout']=$this->avg_payout;
        $aData['match_main']=$this->match_main;
        $aData['match_extra']=$this->match_extra;
        $aData['payout_handler']=$this->payout_handler;
        $aData['tier_number']=$this->tier_number;
        parent::save($aData);
    }
}