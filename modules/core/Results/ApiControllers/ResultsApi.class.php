<?php

/**
 * Results API used to feed results out to white labels
 */
class ResultsApi extends \LL\AjaxController{

    /**
     * Returns the latest results from the system starting from the date provided
     * The date should be provided by the caller based on the last time it called
     * this API. The API will then return all results since that date
     */
    function getLatest(){
        $d=$_GET['date'];
        $sSQL='SELECT * FROM lottery_draws WHERE fk_lotterydate>\''.$d.'\'';
        $aData=\DAL::executeQuery($sSQL);

        die(json_encode($aData));
    }

    function pullLatest(){
        $s='dev.maxlotto.com';
        
        $sSQL='SELECT max(fk_lotterydate) FROM lottery_draws';
        $sDate=\DAL::executeGetOne($sSQL);

        $sURL='http://'.$s.'/administration/API/Results/getLatest?date='.urlencode($sDate);
        $sData=file_get_contents($sURL);

        $aData=json_decode($sData);

        foreach($aData as $oItem){
            $aItem=get_object_vars($oItem);
            unset($aItem['id']);
            echo \DAL::Insert('lottery_draws', $aItem,true);
        }
    }

}