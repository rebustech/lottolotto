<ul class="BreadcrumbsControl"><li><span class="fa-stack fa-2x"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-flag-checkered fa-stack-1x fa-inverse"></i></span><h3><a href="add-results-checker-all.php?tablename=1">Results</a></h3></li><li><a href="generic-listing.php?tablename=lottery_checkers_list">Pending Winnings Summary</a></li></ul>
<table class="datatable">
    <tr>
        <td></td>
        <td></td>
<?php

    $iPreviousLevel=0;
        foreach($this->aLevels as $iLevel):

?>
        <td>€<?=number_format($iPreviousLevel,2)?></td>
        <td></td>
<?php
$iPreviousLevel=$iLevel;
        endforeach;
?>
    </tr>
    <tr>
        <td>Lottery</td>
        <td>Draw</td>
        <?=str_repeat('<td>%</td><td>#</td>',sizeof($this->aLevels));?>
    </tr>
<?php
    foreach($this->aSummary as $aSummaryRow):

        ?>
    <tr>
        <td><img src="/administration/images/lotto-<?=$aSummaryRow['fk_lottery_id']?>.png" height="30"/>&nbsp;<?=$aSummaryRow['comment']?></td>
        <td><?=date('d/m/Y H:i',strtotime($aSummaryRow['fk_lotterydate']))?></td>

<?php
    $iPreviousLevel=0;
        foreach($this->aLevels as $iLevel):
?>
        
        
        <td><?php if($aSummaryRow['b'.$iPreviousLevel]>0):?><a href="/administration/pending-winnings.php?m=<?=$iPreviousLevel?>&amp;x=<?=$iLevel?>&amp;lottery=<?=$aSummaryRow['fk_lottery_id']?>&amp;d=<?=$aSummaryRow['fk_lotterydate']?>"><?=round(($aSummaryRow['c'.$iPreviousLevel]/$aSummaryRow['b'.$iPreviousLevel])*100,0)?>%</a><?php endif; ?></td>
        <td><?php if($aSummaryRow['b'.$iPreviousLevel]>0):?><a href="/administration/pending-winnings.php?m=<?=$iPreviousLevel?>&amp;x=<?=$iLevel?>&amp;lottery=<?=$aSummaryRow['fk_lottery_id']?>&amp;d=<?=$aSummaryRow['fk_lotterydate']?>"><?=$aSummaryRow['b'.$iPreviousLevel]?></a><?php endif; ?></td>

<?php
$iPreviousLevel=$iLevel;
        endforeach;
?>
        

    </tr>
<?php
    endforeach;
?>
</table>

<style>
    table a {text-decoration: none}
    table td:nth-child(4n+1),
    table td:nth-child(4n+2){
        background:#fafafa;
    }
    table td:nth-child(4n+0),
    table td:nth-child(4n+3){
        background:#f5f5f5;
    }
    table td:nth-child(4n+0),
    table td:nth-child(4n+2){
        border-right:dotted 1px #444;
    }
    table.datatable td,
    table.datatable td img{        
        vertical-align: middle;
    }
    table.datatable td img{ 
        margin-right:10px;
    }
    
</style>