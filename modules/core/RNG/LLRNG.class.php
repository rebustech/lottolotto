<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LL;

class RNG {

    static function random($numbers, $min, $max, $bonusnumbers, $bonusmin, $bonusmax, $is_for_cache = false) {
        if($is_for_cache===true){
            echo str_repeat(" \r\n",1024);
            flush();
        }

        # Response object
        $response = new \stdClass();

        $sCacheKey = "RNGCACHE:$numbers:$min:$max:$bonusnumbers:$bonusmin:$bonusmax";
        $oCacheObject = \LLCache::getObject($sCacheKey);

        $response->log[]='Cache Key is '.$sCacheKey;

        /**
         * If we are not generating for the cache then we are reading from it
         * attempt to get a result if not we'll drop back into the api call to the
         * RNG
         */
        if ($is_for_cache===false) {
            $response->log[]='Calling for numbers from cache';
            if (is_array($oCacheObject->items)) {
                if(sizeof($oCacheObject->items)>0){
                    $response->log[]='Found numbers in cache';

                    $response_object = array_pop($oCacheObject->items);
                    \LLCache::addObject($sCacheKey, $oCacheObject);

                    //Generate a replacement asyncronously
                    $response->log[]='Restocking Cache';
                    $sURL="http://jpatchett:claire76@dev.maxlotto.com/administration/API/engine/random?numbers=$numbers&min=$min&max=$max&bonusnumbers=$bonusnumbers&bonusmin=$bonusmin&bonusmax=$bonusmax&is_for_cache=1";
                    $response->log[]=$sURL;
                    self::curlTouch($sURL);

                    /**
                     * For a normal RNG request for a bunch of numbers for which there
                     * is a key in the cache this is the end of the request
                     * The forked process above will still be running to generate a replacement
                     */
                    $response->log[]='Returning Cached response';
                    return $response_object;
                }
            }
        }

        /**
         * We will get here if :
         * 1. The request being made is to generate a replacment set of numbers in the cache (this will be a forked process)
         * -- OR --
         * 2. The request being made has no cached values (this would be a main process)
         *
         * Check to see if we actually have any numebrs in the cache. If not generate some asyncronously
         */
        if($is_for_cache===false){
            if (sizeof($oCacheObject->items)==0) {
                /**
                 * There is nothing in the cache yet which means the above block will
                 * have failed if requesting a set of numbers. That means we need
                 * to create a cushion of 10 items
                 */
                $response->log[]='Cache is empty';
                $oCacheObject->items = array();
                \LLCache::addObject($sCacheKey, $oCacheObject);
                $response->log[]='Restocking Cache';
                $sURL="http://jpatchett:claire76@dev.maxlotto.com/administration/API/engine/random?numbers=$numbers&min=$min&max=$max&bonusnumbers=$bonusnumbers&bonusmin=$bonusmin&bonusmax=$bonusmax&is_for_cache=1";
                $response->log[]=$sURL;
                self::curlTouch($sURL);
            }
        }
        /**
         * Finally we can actually make the call. We will get here if:
         *
         * 1. The request being made is for a set of numbers and tehre was nothing
         * in the cache. We'll get down to here to just do a normal call to the RNG
         * -- OR --
         * 2. The request being made was for a new item for the cache - in this case
         * we need to make sure that the cache is topped up as much as we need to.
         * Also check the memcache key 'running' - if set then don't store because
         * another process is already dealing with this and it will be a waste of numbers
         * from the RNG
         */


        $iNumberOfRequests=($is_for_cache)?100-sizeof($oCacheObject->items):1;
        $bIsScanningAlready=\LLCache::get($sCacheKey.'_SCANNING');
        if($bIsScanningAlready) $iNumberOfRequests=1;

        set_time_limit(0);

        for($a=0;$a<$iNumberOfRequests;$a++){
            \LLCache::add($sCacheKey.'_SCANNING',true);

            # Create a client
            $client = new \JsonRPCClient('https://api.random.org/json-rpc/1/invoke', 5, true);

            # Test our limits
            $check = $client->execute('getUsage', array(
                "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f"
                    )
            );

            $outputversion = 2;

            # Passed?
            if ($check['requestsLeft'] > 0) {

                # Setup what we need
                $apiData = array(
                    "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f",
                    "n" => $numbers,
                    "min" => $min,
                    "max" => $max,
                    "replacement" => false
                );

                # Process the request
                if ($outputversion > 1) {
                    $response->main = $client->execute('generateIntegers', $apiData);
                } else {
                    $response = $client->execute('generateIntegers', $apiData);
                }

                if ($bonusmax > 0) {

                    # Setup what we need
                    $apiData = array(
                        "apiKey" => "596741ea-ef5f-4c24-a031-f349e0cfd41f",
                        "n" => $bonusnumbers,
                        "min" => $bonusmin,
                        "max" => $bonusmax,
                        "replacement" => false
                    );

                    # Process the request
                    $response->bonus = $client->execute('generateIntegers', $apiData);
                }
            } else {



                # Main numbers
                $response->main = array(
                    'random' => array(
                        'data' => self::randomNumberRange($min, $max, $numbers)
                    )
                );

                # Bonus numbers
                if ($bonusmax > 0) {
                    $response->bonus = array(
                        'random' => array(
                            'data' => self::randomNumberRange($bonusmin, $bonusmax, $bonusnumbers)
                        )
                    );
                }
            }

            if ($is_for_cache===true) {
                $response->log[]='Storing in Cache';
                if(!is_array($oCacheObject->items)) $oCacheObject->items=array();
                //Get the object back before we modify it just in case anything has changed
                $oCacheObject->items[]=$response;
                $oCacheObject->lastUpdate=date('Y-m-d h:i:s');
                \LLCache::addObject($sCacheKey, $oCacheObject);
            }
        }

        \LLCache::add($sCacheKey.'_SCANNING',false);
        $response->log[]='Returning new response';
        return $response;
    }

    static function randomNumberRange($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

    static function curlTouch($sURL){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $sURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS,200);
        $response = curl_exec($ch);
    }
}
