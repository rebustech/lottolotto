<?php

/**
 * Seriliases an object to XML
 * Pinched from http://stackoverflow.com/questions/137021/php-object-as-xml-document
 */
class XMLSerializer {

    // functions adopted from http://www.sean-barton.co.uk/2009/03/turning-an-array-or-object-into-xml-using-php/
    public static function generateValidXml($obj, $node_block='data', $node_name='node') {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        if(is_array($obj)){
            $xml.=self::generateValidXmlFromArray($obj,$node_block,$node_name);
        }
        if(is_object($obj)){
            $xml.=self::generateValidXmlFromObj($obj,$node_block,$node_name);
        }
        return $xml;
    }

    public static function generateValidXmlFromObj($obj, $node_block='data', $node_name='node') {
        $arr = get_object_vars($obj);
        return self::generateValidXmlFromArray($arr, $node_block, $node_name);
    }

    public static function generateValidXmlFromArray($array, $node_block='data', $node_name='node') {

        $xml .= '<' . $node_block . '>';
        $xml .= self::generateXmlFromArray($array, $node_name);
        $xml .= '</' . $node_block . '>';

        return $xml;
    }

    private static function generateXmlFromArray($array, $node_name) {
        $xml = '';

        if (is_array($array)){
            foreach ($array as $key=>$value) {
                if (is_numeric($key)) {
                    $key = $node_name;
                }

                $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>';
            }
        }elseif(is_object($array)) {
            $objectVars=get_object_vars($array);
            foreach ($objectVars as $key=>$value) {
                if($key!=''){
                    //$value=$array->$key;
                    if (is_numeric($key)) {
                        $key = $node_name;
                    }
                    if(is_object($value)){
                        $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>';
                    }elseif(is_array($value)){
                        $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>';
                    }else{
                        $xml .= '<' . $key . '>' . $value . '</' . $key . '>';
                    }
                }
            }
        } else {
            $xml = htmlspecialchars($array, ENT_QUOTES);
        }

        return $xml;
    }

}