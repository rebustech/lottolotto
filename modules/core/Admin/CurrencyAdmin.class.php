<?php

/**
 * CurrencyAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class CurrencyAdmin {

    public static function getBaseCurrencyID() {
        $sSQL = "SELECT c.currency_id
				FROM currencies c
				WHERE c.base_currency = 1";

        return DAL::executeGetOne($sSQL);
    }

    public static function getCurrencyByShortName($sCurrency) {
        $sql = "SELECT c.currency_id as id, c.xrate as xrate
		FROM currencies c
		WHERE c.shortname = '{$sCurrency}'";

        return DAL::executeGetRow($sql);
    }

    static public function editCurrencyXRate($xrate, $iCurrencyID) {

        $tablename = "currencies";

        $data = array(
            'xrate' => $xrate
        );

        $params = "currency_id = $iCurrencyID";

        DAL::Update($tablename, $data, $params);

        return true;
    }

//eof

    public function convertToBase($fAmount, $iCurrencyID, $fMargin = 0) {
        $xRate = self::getCurrencyXRate($iCurrencyID);
        if ($xRate != 1) {
            $xRate = $xRate + $fMargin;
        }
        return ($fAmount / $xRate );
    }

    private function getCurrencyXRate($id) {
        $sSQL = "SELECT c.xrate
				FROM currencies c
				WHERE c.currency_id = {$id}
				LIMIT 1";

        return DAL::executeGetOne($sSQL);
    }

}