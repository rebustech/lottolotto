<?php

/**
 * UsersAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class UsersAdmin {

    public static function getUserDetails($iUserID, $iUserTypeID = NULL) {
        if ($iUserTypeID == SecurityAdmin::AFFILIATE) {
            $sInnerJoin = " INNER JOIN affiliates af ON af.fk_user_id = au.user_id ";
        }
        $sSQL = "SELECT *
				FROM admin_users au
				{$sInnerJoin}
				WHERE au.user_id = '" . $iUserID . "'";
        if ($iUserTypeID) {
            $sSQL .= " AND au.fk_admintype_id = {$iUserTypeID} ";
        }
        return DAL::executeGetRow($sSQL);
    }

    public function editUserDetails($iUserID, $aDetails) {
        $bValid = false;
        if (is_array($aDetails)) {
            $tablename = "admin_users";
            $params = "user_id = '" . $iUserID . "'";
            $bValid = DAL::Update($tablename, $aDetails, $params);
        }
        return $bValid;
    }

    public function changePassword($iUserID, $sNewPassword) {
        $tablename = "admin_users";
        $data = array(
            'password' => Encryption::Encrypt($sNewPassword)
        );
        $params = "user_id = '" . $iUserID . "'";
        return DAL::Update($tablename, $data, $params);
    }

    public function changePin($iUserID, $sNewPin) {
        $tablename = "admin_users";
        $data = array(
            'pin' => Encryption::Encrypt($sNewPin)
        );
        $params = "user_id = '" . $iUserID . "'";
        $bValid = DAL::Update($tablename, $data, $params);

        return $bValid;
    }

    public function getAdminTypes() {
        $sSQL = "SELECT admintype_id AS id,
						comment AS adminName,
						path AS folder,
						is_active as is_active,
						check_blacklist as check_blacklist

				FROM admin_types
				";

        return DAL::executeQuery($sSQL);
    }

    public function checkPageBlackList($iUsedID, $iPageID) {
        $sSQL = "SELECT aupb.fk_page_id
				FROM admin_users_pages_blacklist aupb
				INNER JOIN admin_pages ap ON ap.id = aupb.fk_page_id
				WHERE aupb.fk_user_id = '" . $iUserID . "'
					AND aupb.fk_page_id = '" . $iPageID . "'";
        $aPageID = DAL::executeGetOne($sSQL);
        return ($aPageID) ? false : true;
    }

    public function blacklistPages($iUserID, $aPageIDs) {
        $sSQL = "INSERT INTO admin_users_pages_blacklist (fk_user_id, fk_page_id)
					VALUES ";
        foreach ($aPageIDs as $key => $iPageID) {
            $sSQL .= "({$iUserID}, {$iPageID})";
            if ($key != count($aPageIDs) - 1) {
                $sSQL .= ",";
            } else {
                $sSQL .= ";";
            }
        }
        DAL::executeQuery($sSQL);
    }

    public function clearBlacklist($iUserID) {
        $sSQL = "DELETE FROM admin_users_pages_blacklist WHERE fk_user_id = {$iUserID}";
        DAL::executeQuery($sSQL);
    }

    public function getUsersByType($iAdminTypeID = NULL) {
        $sWhere = "";
        if ($iAdminTypeID) {
            $sWhere = " WHERE at.admintype_id = {$iAdminTypeID} ";
        }
        $sSQL = "SELECT au.username,
					au.firstname,
					au.lastname,
					au.user_id as id,
					at.comment as adminType
				FROM admin_users au
				INNER JOIN admin_types at ON at.admintype_id = au.fk_admintype_id
				{$sWhere}
				ORDER BY au.lastname ASC, au.firstname ASC, au.username ASC";

        return DAL::executeQuery($sSQL);
    }

}