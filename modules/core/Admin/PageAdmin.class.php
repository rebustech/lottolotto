<?php

/**
 * PageAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class PageAdmin {

    public static function getPageByID($iPageID) {

        $sql = "SELECT pc.comment as comment,
				pc.url as url,
				pc.page_id as id,
				pc.fk_section_id as sectionid,
				pc.parent_id as parentid,
				pc.level as level
				FROM pages_cmn pc
				WHERE pc.page_id = '{$iPageID}'";

        return DAL::executeGetRow($sql);
    }

//eof

    public static function getPageCodeByID($iPageID) {

        $sql = "SELECT pc.url as url, sc.folder as folder
				FROM pages_cmn pc
				INNER JOIN sections_cmn sc
				ON sc.section_id = pc.fk_section_id
				WHERE pc.page_id = '{$iPageID}'";
        $url = DAL::executeGetRow($sql);

        $folder = $url[folder];
        $file = $url[url];
        return File::readFile(WEBSITEPATH, $folder, $file);
    }

//eof

    public static function getRootPage($iSectionID, $iLangID = 0) {

        $sql = "SELECT sc.comment as section,
				pc.parent_id as pid,
				pc.url as url,
				pc.is_active,
				pc.comment as comment,
				pc.rank as rank,
				pc.page_id as id,
				pc.left_node,
				pc.right_node,
				pc.fk_section_id as sectionid,
				pc.showinmenu as showinmenu,
				pc.showinsitemap as showinsitemap";
        if ($iLangID) {
            $sql .= ", pl.page_title as title, pl.fk_language_id as lid, pl.is_active as lang_active ";
        }
        $sql .= " FROM pages_cmn pc
				LEFT JOIN sections_cmn sc
				ON pc.fk_section_id = sc.section_id";
        if ($iLangID) {
            $sql .= " LEFT JOIN pages_lang pl ON pc.page_id = pl.fk_page_id AND pl.fk_language_id = $iLangID";
        }
        $sql .= " WHERE pc.parent_id = 0
				AND pc.fk_section_id = '{$iSectionID}'
				";
        $result = DAL::executeGetRow($sql);
        return $result;
    }

    public static function getPageChildren($iSectionID, $iLeftNode, $iRightNode, $iLangID = 0) {
        $aRightNodes = array();
        if($iLeftNode!='' && $iRightNode!=''){

        $sql = "SELECT sc.comment as section,
				pc.parent_id as pid,
				pc.url as url,
				pc.is_active,
				pc.comment as comment,
				pc.rank as rank,
				pc.page_id as id,
				pc.right_node,
				pc.fk_section_id as sectionid,
				pc.showinmenu as showinmenu,
				pc.showinsitemap as showinsitemap";
        if ($iLangID) {
            $sql .= ", pl.page_title as title, l.language_id as lid, pl.is_active as lang_active ";
        }
        $sql .= " FROM pages_cmn pc";
        if ($iLangID) {
            $sql .= " LEFT JOIN pages_lang pl ON pc.page_id = pl.fk_page_id AND pl.fk_language_id = {$iLangID}
							LEFT JOIN languages l ON l.language_id = {$iLangID}";
        }
        $sql .= " INNER JOIN sections_cmn sc
				ON pc.fk_section_id = sc.section_id
				AND pc.fk_section_id = '{$iSectionID}'
				AND pc.left_node BETWEEN {$iLeftNode} AND {$iRightNode}
				AND pc.parent_id != 0
				ORDER BY pc.left_node ASC";
            $aPages = DAL::executeQuery($sql);
        foreach ($aPages as $key => $Page) {
            if (count($aRightNodes) > 0) {
                while (count($aRightNodes) && $aRightNodes[count($aRightNodes) - 1] < $Page['right_node']) {
                    array_pop($aRightNodes);
                }
            }
            $aPages[$key]['level'] = count($aRightNodes);
            $aRightNodes[] = $Page['right_node'];
        }
        }
        return $aPages;
    }

    public static function updateParentPageAndRank($iPageID, $iParentID, $iRank, $iSectionID) {
        $tablename = "pages_cmn";
        $data = array(
            'parent_id' => $iParentID,
            'rank' => $iRank
        );
        $params = "page_id = '{$iPageID}' AND fk_section_id = '{$iSectionID}'";

        return DAL::Update($tablename, $data, $params);
    }

    public static function enablePage($iPageID) {

        $tablename = "pages_cmn";
        $data = array(
            'is_active' => 1,
        );
        $params = "page_id = '{$iPageID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function disablePage($iPageID) {

        $tablename = "pages_cmn";
        $data = array(
            'is_active' => 0,
        );
        $params = "page_id = '{$iPageID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function enablePageLanguage($iPageID, $iLangID) {

        $tablename = "pages_lang";
        $data = array(
            'is_active' => 1,
        );
        $params = "fk_page_id = '{$iPageID}' AND fk_language_id = '{$iLangID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function disablePageLanguage($iPageID, $iLangID) {

        $tablename = "pages_lang";
        $data = array(
            'is_active' => 0,
        );
        $params = "fk_page_id = '{$iPageID}' AND fk_language_id = '{$iLangID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function enableShowInMenu($iPageID) {

        $tablename = "pages_cmn";
        $data = array(
            'showinmenu' => 1,
        );
        $params = "page_id = '{$iPageID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function disableShowInMenu($iPageID) {

        $tablename = "pages_cmn";
        $data = array(
            'showinmenu' => 0,
        );
        $params = "page_id = '{$iPageID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function enableShowInSiteMap($iPageID) {

        $tablename = "pages_cmn";
        $data = array(
            'showinsitemap' => 1,
        );
        $params = "page_id = '{$iPageID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function disableShowInSiteMap($iPageID) {

        $tablename = "pages_cmn";
        $data = array(
            'showinsitemap' => 0,
        );
        $params = "page_id = '{$iPageID}'";
        DAL::Update($tablename, $data, $params);
    }

    public static function addPage($filename, $title, $content, $parentid, $iSectionID, $level) {

        $bReturn = false;

        $folder = Menu::getFolder($iSectionID);

        $sql = "SELECT MAX(rank)
			FROM pages_cmn
			WHERE parent_id = {$parentid}";
        $rank = DAL::executeGetOne($sql);
        $rank++;

        $tablename = "pages_cmn";
        $data = array(
            'fk_section_id' => $iSectionID,
            'url' => $filename,
            'rank' => $rank,
            'level' => $level,
            'parent_id' => $parentid,
            'showinmenu' => 1,
            'showinsitemap' => 1,
            'comment' => $title
        );

        DAL::Insert($tablename, $data);
        $sql = "SELECT last_insert_id()";
        $iPageID = DAL::executeGetOne($sql);
        $content = str_replace("%id%", $iPageID, $content);

        $folder = $url[folder];
        $file = $url[url];
        if (!File::uploadFile(WEBSITEPATH, $folder, $file, stripslashes($content))) {
            $bReturn = false;
        }

        return $bReturn;
    }

    public static function editPage($iPageID, $title, $filename, $content, $parentid, $sectionid, $level) {

        $bReturn = false;

        $result_old = Page::getPageDetails($iPageID);

        $newfolder = Menu::getFolder($sectionid);
        $oldfolder = Menu::getFolder($result_old[sectionid]);

        $sPath = Websites::getWebsitePathByID($iWebsiteID);

        $result = Language::getLanguageFolders($iWebsiteID);
        foreach ($result as $key => $value) {
            $bReturn = true;
            $rootfolder = $value[folder];
            if (!@File::deletePage($sPath, $rootfolder, $oldfolder, $result_old[url]))
                $bReturn = false;
        }
        $result = Language::getLanguageFolders($iWebsiteID);
        foreach ($result as $key => $value) {
            $bReturn = true;
            $rootfolder = $value[folder];
            if (!@File::uploadFile($sPath, $rootfolder, $newfolder, $filename, stripslashes($content)))
                $bReturn = false;
        }

        $tablename = "pages_cmn";

        $data = array(
            'url' => $filename,
            'comment' => $title
        );

        $params = "page_id = '{$iPageID}' AND fk_website_id = '{$iWebsiteID}'";

        DAL::Update($tablename, $data, $params);

        if ($result_old[parent_id] != $parentid && $filename != "index.php") {

            $sql = "SELECT COUNT(page_id)
				FROM pages_cmn
				WHERE level = {$result_old[level]}
				AND fk_section_id = {$result_old[sectionid]}
				AND parent_id = {$result_old[parent_id]}
				AND rank > {$result_old[rank]}
				AND fk_website_id = '{$iWebsiteID}'";
            $result = DAL::executeGetOne($sql);

            $sql = "SELECT MAX(rank)
			FROM pages_cmn
			WHERE `parent_id` = {$parentid}
			AND fk_website_id = '{$iWebsiteID}'";
            $newrank = DAL::executeGetOne($sql);
            $newrank++;


            $tablename = "pages_cmn";


            $data = array(
                'fk_section_id' => $sectionid,
                'rank' => $newrank,
                'level' => $level,
                'parent_id' => $parentid,
            );

            $params = "page_id = '{$iPageID}' AND fk_website_id = '{$iWebsiteID}'";

            DAL::Update($tablename, $data, $params);

            if ($result > 0) {

                $rank = $result_old[rank];
                $olderrank = $rank + 1;

                while ($result > 0) {

                    $tablename = "pages_cmn";

                    $data = array(
                        'rank' => $rank,
                    );

                    $params = "rank = $olderrank AND fk_website_id = '{$iWebsiteID}' AND parent_id = " . $result_old[parent_id];

                    DAL::Update($tablename, $data, $params);

                    $rank++;
                    $olderrank++;
                    $result--;
                }
            }
        }
        return $bReturn;
    }

    public static function deletePage($iPageID, $iWebsiteID) {

        $bReturn = false;

        $sPath = Websites::getWebsitePathByID($iWebsiteID);
        $pageresult = Page::getPageDetails($iPageID, $iWebsiteID);
        $folder = Menu::getFolder($pageresult[sectionid], $iWebsiteID);

        $result = Language::getLanguageFolders($iWebsiteID);
        foreach ($result as $key => $value) {
            $bReturn = true;
            $rootfolder = $value[folder];
            if (!@File::deletePage($sPath, $rootfolder, $folder, $pageresult[url]))
                $bReturn = false;
        }

        $sql = "SELECT COUNT(page_id)
			FROM pages_cmn
			WHERE level = {$pageresult[level]}
			AND fk_section_id = {$pageresult[sectionid]}
			AND parent_id = {$pageresult[parent_id]}
			AND rank > {$pageresult[rank]}
			AND fk_website_id = {$iWebsiteID}";
        $result = DAL::executeGetOne($sql);

        $tablename = "pages_cmn";
        $params = "page_id = '$iPageID' AND fk_website_id = {$iWebsiteID}";
        $sql = "DELETE FROM $tablename WHERE $params";
        DAL::Query($sql);

        if ($result > 0) {

            $rank = $pageresult[rank];
            $olderrank = $rank + 1;

            while ($result > 0) {

                $tablename = "pages_cmn";

                $data = array(
                    'rank' => $rank,
                );

                $params = "rank = $olderrank AND fk_website_id = {$iWebsiteID} AND parent_id = " . $pageresult[parent_id];

                DAL::Update($tablename, $data, $params);

                $rank++;
                $olderrank++;
                $result--;
            }
        }
        return $bReturn;
    }

    public static function editPageLanguage($id, $iLangID, $iWebsiteID, $title, $browsertitle, $keywords, $h1, $description, $content, $isactive) {

        $tablename = "pages_lang";

        $data = array(
            'title' => $title,
            'browsertitle' => $browsertitle,
            'keywords' => $keywords,
            'h1' => $h1,
            'description' => $description,
            'content' => $content,
            'isactive' => $isactive,
            'lastmodified' => date('YmdHis')
        );

        $params = "fk_page_id = $id AND fk_language_id = '{$iLangID}' AND fk_website_id = '{$iWebsiteID}'";

        DAL::Update($tablename, $data, $params);
    }

    public static function editPageLanguageContent($id, $iLangID, $iWebsiteID, $content, $isactive) {

        $tablename = "pages_lang";

        $data = array(
            'content' => $content,
            'isactive' => $isactive,
            'lastmodified' => date('YmdHis')
        );

        $params = "fk_page_id = $id AND fk_language_id = '{$iLangID}' AND fk_website_id = '{$iWebsiteID}'";

        DAL::Update($tablename, $data, $params);
    }

    public static function addPageLanguage($id, $iLangID, $title, $browsertitle, $keywords, $h1, $description, $content, $isactive) {

        $tablename = "pages_lang";

        $data = array(
            'title' => $title,
            'browsertitle' => $browsertitle,
            'keywords' => $keywords,
            'h1' => $h1,
            'description' => $description,
            'content' => $content,
            'isactive' => $isactive,
            'fk_page_id' => $id,
            'fk_language_id' => $iLangID
        );

        DAL::Insert($tablename, $data, $params);
    }

    public static function deletePageLanguage($iPageID, $iLangID) {

        $tablename = "pages_lang";

        $params = "fk_page_id = '{$iPageID}' AND fk_language_id = '{$iLangID}'";
        $sql = "DELETE FROM $tablename WHERE $params";
        DAL::Query($sql);
    }

    public static function checkMMPageExists($id, $mmid) {

        $sql = "SELECT COUNT(fk_page_id)
		FROM r_mm_pages
		WHERE fk_page_id  = '$id'
		AND fk_mm_id = '$mmid'";
        return DAL::executeGetOne($sql);
    }

//eof

    static public function addMMPage($pid, $id, $order) {

        $tablename = "r_mm_pages";

        $data = array(
            'fk_page_id' => $pid,
            'fk_mm_id' => $id,
            '`rank`' => $order
        );

        DAL::Insert($tablename, $data, $params);

        return true;
    }

//eof

    static public function deleteMMPage($pid, $id) {

        $tablename = "r_mm_pages";

        $params = "fk_page_id = '$pid' AND fk_mm_id = '$id'";

        $sql = "DELETE FROM $tablename WHERE $params";

        DAL::Query($sql);
    }

//eof

    public static function checkMMPage($pid) {

        $sql = "SELECT COUNT(rmp.fk_page_id)
		FROM r_mm_pages rmp
		WHERE rmp.fk_page_id = '$pid'";

        return 0; //DAL::executeGetOne($sql);
    }

//eof

    public static function addPageFile($filename, $content, $iSectionID, $iPageID) {

        $bReturn = true;

        $folder = PageAdmin::getSectionFolder($iSectionID);

        $content = str_replace("%id%", $iPageID, $content);

        if (!File::uploadFile(WEBSITEPATH, $folder, $filename, stripslashes($content)))
            $bReturn = false;
        return $bReturn;
    }

    public static function updatePageFile($filename, $content, $iPageID, $iSectionID) {
        $bReturn = true;

        $sOldFilename = PageAdmin::getPageFileName($iPageID);
        $newfolder = PageAdmin::getSectionFolder($iSectionID);

        $content = str_replace("%id%", $iPageID, $content);
        if (!@File::uploadFile(WEBSITEPATH, $newfolder, $filename, stripslashes($content)))
            $bReturn = false;
        return $bReturn;
    }

    public static function deletePageFile($iPageID) {

        $bReturn = true;

        $sFilename = PageAdmin::getPageFileName($iPageID);
        $sFolder = PageAdmin::getSectionFolder($iSectionID);

        if (!@File::deletePage(WEBSITEPATH, $sFolder, $sFilename))
            $bReturn = false;
        return $bReturn;
    }

    public static function getPageFileName($iPageID) {
        $sSQL = "SELECT url
                FROM pages_cmn
                WHERE page_id = {$iPageID}";

        return DAL::executeGetOne($sSQL);
    }

    public static function getSectionFolder($iSectionID) {
        $sSQL = "SELECT folder
                FROM sections_cmn
                WHERE section_id = {$iSectionID}";

        return DAL::executeGetOne($sSQL);
    }

    public static function getPagesForSitemap() {
        $sSQL = "SELECT
					l.code as langcode,
					l.default as default_language,
					l.language_id as language_id,
					pc.url as url,
					sc.folder as folder,
					pl.last_updated as lastmodified
				FROM pages_cmn pc
				INNER JOIN pages_lang pl ON pl.fk_page_id = pc.page_id
				INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id
				INNER JOIN languages l ON l.language_id = pl.fk_language_id

				WHERE pc.is_active = 1
					AND pl.is_active = 1
					AND pc.showinsitemap = 1
					AND sc.is_active = 1
					AND l.is_active = 1

				ORDER BY l.default DESC, sc.rank ASC, pc.rank ASC
				";

        return DAL::executeQuery($sSQL);
    }

    public function getLotteryDrawsURLs() {
        $sSQL = "SELECT
					l.code as langcode,
					l.default as default_language,
					l.language_id as language_id,
					CONCAT(pc.url, '?draw=', ld.draw) as url,
					sc.folder as folder,
					ld.fk_lotterydate as lastmodified
				FROM lottery_draws ld
				INNER JOIN lotteries_cmn ll ON ll.lottery_id = ld.fk_lottery_id
				INNER JOIN pages_cmn pc ON pc.page_id = ll.results_page_id
				INNER JOIN pages_lang pl ON pl.fk_page_id = pc.page_id
				INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id
				INNER JOIN languages l ON l.language_id = pl.fk_language_id
				WHERE pc.is_active = 1
					AND pl.is_active = 1
					AND pc.showinsitemap = 1
					AND sc.is_active = 1
					AND l.is_active = 1
					AND ll.is_active = 1
				ORDER BY l.default DESC, sc.rank ASC, pc.rank ASC, ld.fk_lotterydate DESC";

        return DAL::executeQuery($sSQL);
    }

}
