<?php

/**
 * MutlimediaAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class MultimediaAdmin {
    //VARS

    const INTERNALLINK = 1;
    const PDFDOCS = 2;
    const WORDDOCS = 3;
    const TOPLEFTPAGEIMAGES = 4;
    const TOPRIGHTPAGEIMAGES = 5;
    const EXTERNALLINKS = 6;
    const INTERNALIMAGES = 8;
    const GALLERYIMAGES = 9;
    const PRODUCTIMAGES = 10;
    const BANNERS = 12;
    const VIDEOS = 13;
    const HOMEADS = 15;
    const HOMELINKIMAGES = 14;
    const SKYSCRAPERBANNER = 16;
    const ISLANDBANNER = 17;
    const BF_SKYSCRAPERBANNER = 18;
    const BF_ISLANDBANNER = 19;
    const MMURL = "../system/media/";

    static public function getMMList($iWebsiteID, $iMMTypeID, $sQuery = '', $sSearchBy = 'comment', $iLangID = 0, $iLimit = 10) {
        if (!$iLimit) {
            $iLimit = 50;
        }
        $sSite = strtolower(Websites::getWebsiteTitleByID($iWebsiteID));

        if ($sQuery) {
            $sClause = " AND {$sSearchBy} LIKE  '%{$sQuery}%'";
        }

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				mmcmn.title as title";
        $sql .= " FROM multimedia_cmn mmcmn";
        $sql .= " WHERE mmcmn.fk_mmtype_id = $iMMTypeID
				AND file LIKE '$sSite%'
				{$sClause}
				ORDER BY mmcmn.mm_id DESC
				LIMIT {$iLimit}";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getMMTypeTypeByID($id) {

        $sql = "SELECT type
				FROM multimediatypes
				WHERE mmtype_id = {$id}";
        return DAL::executeGetOne($sql);
    }

//eof

    static public function getFolderbyID($id) {

        $sql = "SELECT folder
				FROM multimediatypes
				WHERE `mmtype_id` = {$id}";

        return DAL::executeGetOne($sql);
    }

//eof

    static public function getMMListForPage($iWebsiteID, $iPageID, $iTypeID) {

        if ($iTypeID) {

            $sql = "SELECT mmcmn.mm_id as id, mmcmn.fk_mmtype_id as mmtype_id, mmcmn.file as file, mmcmn.url as url,mmcmn.isactive as isactive, mmcmn.comment as comment, rmm.fromdate, rmm.todate
					FROM multimedia_cmn mmcmn
					INNER JOIN r_mm_pages rmm
					ON rmm.fk_page_id = $iPageID
					AND rmm.fk_mm_id = mmcmn.mm_id
					AND rmm.fk_website_id = {$iWebsiteID}
					WHERE mmcmn.isactive = 1
					AND mmcmn.fk_mmtype_id = {$iTypeID}
					ORDER BY rank ASC";

            return DAL::executeQuery($sql);
        }
    }

//eof

    static public function getMMListForListingCategory($iWebsiteID, $iListingCategoryID, $iTypeID) {

        if ($iTypeID) {

            $sql = "SELECT mmcmn.mm_id as id, mmcmn.fk_mmtype_id as mmtype_id, mmcmn.file as file, mmcmn.url as url,mmcmn.isactive as isactive, mmcmn.comment as comment, rmm.fromdate, rmm.todate
					FROM multimedia_cmn mmcmn
					INNER JOIN r_mm_listingcategories rmm
					ON rmm.fk_mm_id = mmcmn.mm_id
					AND rmm.fk_listingcategory_id = {$iListingCategoryID}
					AND rmm.fk_website_id = {$iWebsiteID}
					WHERE mmcmn.isactive = 1
					AND mmcmn.fk_mmtype_id = {$iTypeID}
					ORDER BY rank ASC";

            return DAL::executeQuery($sql);
        }
    }

//eof

    static public function getMMListForWebsite($iWebsiteID, $iTypeID) {

        if ($iTypeID) {

            $sql = "SELECT mmcmn.mm_id as id, mmcmn.fk_mmtype_id as mmtype_id, mmcmn.file as file, mmcmn.url as url,mmcmn.isactive as isactive, mmcmn.comment as comment, rmm.fromdate, rmm.todate
					FROM multimedia_cmn mmcmn
					INNER JOIN r_mm_websites rmm
					ON rmm.fk_mm_id = mmcmn.mm_id
					AND rmm.fk_website_id = {$iWebsiteID}
					WHERE mmcmn.isactive = 1
					AND mmcmn.fk_mmtype_id = {$iTypeID}
					ORDER BY rank ASC";

            return DAL::executeQuery($sql);
        }
    }

//eof

    static public function getMMPageListForSearch($sComment, $iTypeID, $iPageID, $iWebsiteID, $sPrefix = NULL, $searchby = "comment") {

        if ($sPrefix != NULL && $iTypeID != 1)
            $prefixclause = "AND mmcmn.file LIKE '{$sPrefix}-%'";
        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID  AND rmp.fk_mm_id IS NULL";

        switch ($searchby) {
            case "comment":
                $searchclause = " AND LOWER(mmcmn.comment) LIKE LOWER('%{$sComment}%')";
                break;
            case "filename":
                $searchclause = " AND mmcmn.file LIKE '{$sPrefix}-%{$sComment}%'";
                break;
        }

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmp.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_pages rmp
				ON rmp.fk_mm_id = mmcmn.mm_id
				AND rmp.fk_page_id = {$iPageID}
				AND rmp.fk_website_id = {$iWebsiteID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$searchclause
				$where
				ORDER BY mmcmn.mm_id DESC, mmcmn.fk_mmtype_id DESC";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getMMListingCategoriesListForSearch($sComment, $iTypeID, $iListingCategoryID, $iWebsiteID, $searchby = "comment") {

        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID  AND rmp.fk_mm_id IS NULL";

        switch ($searchby) {
            case "comment":
                $searchclause = " AND LOWER(mmcmn.comment) LIKE LOWER('%{$sComment}%')";
                break;
            case "filename":
                $searchclause = " AND mmcmn.file LIKE '{$sComment}%'";
                break;
        }

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmp.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_pages rmp
				ON rmp.fk_mm_id = mmcmn.mm_id
				AND rmp.fk_page_id = {$iListingCategoryID}
				AND rmp.fk_website_id = {$iWebsiteID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$searchclause
				$where
				ORDER BY mmcmn.mm_id DESC, mmcmn.fk_mmtype_id DESC";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getLatestMMListingCategoryListForSearch($iTypeID, $iListingCategoryID, $iWebsiteID, $iLimit = 10) {

        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID AND rmm.fk_mm_id IS NULL";

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmm.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_listingcategories rmm
				ON rmm.fk_mm_id = mmcmn.mm_id
				AND rmm.fk_listingcategory_id = {$iListingCategoryID}
				AND rmm.fk_website_id = {$iWebsiteID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$where
				ORDER BY mmcmn.mm_id DESC LIMIT $iLimit";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getLatestMMPageListForSearch($iTypeID, $iPageID, $iWebsiteID, $sPrefix = NULL, $iLimit = 10) {

        if ($sPrefix != NULL && $iTypeID != 1)
            $prefixclause = "AND mmcmn.file LIKE '{$sPrefix}-%'";
        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID AND rmp.fk_mm_id IS NULL";

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmp.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_pages rmp
				ON rmp.fk_mm_id = mmcmn.mm_id
				AND rmp.fk_page_id = {$iPageID}
				AND rmp.fk_website_id = {$iWebsiteID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$where
				ORDER BY mmcmn.mm_id DESC LIMIT $iLimit";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getLatestMMListForSearch($iTypeID, $iWebsiteID, $iLimit = 10) {

        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID AND rmm.fk_mm_id IS NULL";

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmm.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_websites rmm
				ON rmm.fk_mm_id = mmcmn.mm_id
				AND rmm.fk_website_id = {$iWebsiteID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$where
				ORDER BY mmcmn.mm_id DESC LIMIT $iLimit";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getLatestMMProductListForSearch($iTypeID, $iProductID, $iLimit = 10) {

        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID AND rmp.fk_mm_id IS NULL";

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmp.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_products rmp
				ON rmp.fk_mm_id = mmcmn.mm_id
				AND rmp.fk_product_id = {$iProductID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$where
				ORDER BY mmcmn.mm_id DESC LIMIT $iLimit";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getMMProductListForSearch($sComment, $iTypeID, $iProductID, $searchby = "comment") {

        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID";

        switch ($searchby) {
            case "comment":
                $searchclause = " AND LOWER(mmcmn.comment) LIKE LOWER('%{$sComment}%')";
                break;
            case "filename":
                $searchclause = " AND mmcmn.file LIKE '%{$sComment}%'";
                break;
        }

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmp.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				LEFT OUTER JOIN r_mm_products rmp
				ON rmp.fk_mm_id = mmcmn.mm_id
				AND rmp.fk_product_id = {$iProductID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$searchclause
				$where
				ORDER BY mmcmn.mm_id DESC, mmcmn.fk_mmtype_id DESC";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getMMProductListForManagerSearch($sComment, $iTypeID, $iProductID, $searchby = "comment") {

        if ($iTypeID != 0)
            $where = "AND mmcmn.fk_mmtype_id = $iTypeID";

        switch ($searchby) {
            case "comment":
                $searchclause = " AND LOWER(mmcmn.comment) LIKE LOWER('%{$sComment}%')";
                break;
            case "filename":
                $searchclause = " AND mmcmn.file LIKE '%{$sComment}%'";
                break;
        }

        $sql = "SELECT mmcmn.mm_id as id,
				mmcmn.fk_mmtype_id as mmtype_id,
				mmcmn.file as file,
				mmcmn.url as url,
				mmcmn.isactive as isactive,
				mmcmn.comment as comment,
				rmp.fk_mm_id as relationshipMM
				FROM multimedia_cmn mmcmn
				INNER JOIN r_mm_products rmp
				ON rmp.fk_mm_id = mmcmn.mm_id
				AND rmp.fk_product_id = {$iProductID}
				WHERE mmcmn.isactive = 1
				$prefixclause
				$searchclause
				$where
				ORDER BY mmcmn.mm_id DESC, mmcmn.fk_mmtype_id DESC";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getMMListForProduct($iProductID, $iTypeID) {

        if ($iTypeID) {
            $sql = "SELECT mmcmn.mm_id as id, mmcmn.fk_mmtype_id as mmtype_id, mmcmn.file as file, mmcmn.url as url, mmcmn.isactive as isactive, mmcmn.comment as comment, r_mm_products.isdefault as isdefault
				FROM multimedia_cmn mmcmn
				INNER JOIN r_mm_products
				ON fk_product_id = {$iProductID}
				AND fk_mm_id = mmcmn.mm_id
				WHERE mmcmn.isactive = 1
				AND mmcmn.fk_mmtype_id = {$iTypeID}
				ORDER BY `rank` ASC";

            return DAL::executeQuery($sql);
        }
    }

//eof

    static private function removeProductMMbyMMID($id, $pid = NULL) {
        $sql = "DELETE FROM r_mm_products WHERE fk_mm_id = {$id}";
        if ($pid) {
            $sql .= " AND fk_product_id = {$pid}";
        }
        DAL::executeQuery($sql);
    }

//eof

    static private function removePageMMbyMMID($id, $pageid = NULL) {
        $sql = "DELETE FROM r_mm_pages WHERE fk_mm_id = {$id}";
        if ($pageid) {
            $sql .= " AND fk_product_id = {$pageid}";
        }
        DAL::executeQuery($sql);
    }

//eof

    public static function removePageMMByMMTypeID($iTypeID, $iPageID, $iWebsiteID) {
        $sql = "SELECT mmc.mm_id
				FROM r_mm_pages rmm
				INNER JOIN multimedia_cmn mmc ON rmm.fk_mm_id = mmc.mm_id AND mmc.fk_mmtype_id = '{$iTypeID}'
				WHERE rmm.fk_page_id = '{$iPageID}' AND rmm.fk_website_id = '{$iWebsiteID}'";

        $aRelationIDs = DAL::executeQuery($sql);
        if (!empty($aRelationIDs)) {
            foreach ($aRelationIDs as $iMMRelationID) {
                $sRelationIDs[] = $iMMRelationID['mm_id'];
            }
            $sRelationIDs = implode(",", $sRelationIDs);
            $sql = "DELETE FROM r_mm_pages WHERE fk_mm_id IN ({$sRelationIDs}) AND fk_page_id = '{$iPageID}' AND fk_website_id = '{$iWebsiteID}'";
            DAL::executeQuery($sql);
        }
    }

//eof

    public static function removeWebsiteMMByMMTypeID($iTypeID, $iWebsiteID) {
        $sql = "SELECT mmc.mm_id
				FROM r_mm_websites rmm
				INNER JOIN multimedia_cmn mmc ON rmm.fk_mm_id = mmc.mm_id AND mmc.fk_mmtype_id = '{$iTypeID}'
				WHERE rmm.fk_website_id = '{$iWebsiteID}'";

        $aRelationIDs = DAL::executeQuery($sql);
        if (!empty($aRelationIDs)) {
            foreach ($aRelationIDs as $iMMRelationID) {
                $sRelationIDs[] = $iMMRelationID['mm_id'];
            }
            $sRelationIDs = implode(",", $sRelationIDs);
            $sql = "DELETE FROM r_mm_websites WHERE fk_mm_id IN ({$sRelationIDs}) AND fk_website_id = '{$iWebsiteID}'";
            DAL::executeQuery($sql);
        }
    }

//eof

    public static function removeListingCategoryIDMMByMMTypeID($iTypeID, $iListingCategoryID, $iWebsiteID) {
        $sql = "SELECT mmc.mm_id
				FROM r_mm_listingcategories rmm
				INNER JOIN multimedia_cmn mmc ON rmm.fk_mm_id = mmc.mm_id AND mmc.fk_mmtype_id = '{$iTypeID}'
				WHERE rmm.fk_website_id = '{$iWebsiteID}' AND rmm.fk_listingcategory_id = '{$iListingCategoryID}'";

        $aRelationIDs = DAL::executeQuery($sql);
        if (!empty($aRelationIDs)) {
            foreach ($aRelationIDs as $iMMRelationID) {
                $sRelationIDs[] = $iMMRelationID['mm_id'];
            }
            $sRelationIDs = implode(",", $sRelationIDs);
            $sql = "DELETE FROM r_mm_listingcategories WHERE fk_mm_id IN ({$sRelationIDs}) AND fk_website_id = '{$iWebsiteID}' AND fk_listingcategory_id = '{$iListingCategoryID}'";
            DAL::executeQuery($sql);
        }
    }

//eof

    public static function addMMWebsite($iWebsiteID, $id, $fromdate, $todate, $order) {

        $tablename = "r_mm_websites";

        $data = array(
            'fk_website_id' => $iWebsiteID,
            'fk_mm_id' => $id,
            'fromdate' => $fromdate,
            'todate' => $todate,
            '`rank`' => $order
        );

        DAL::Insert($tablename, $data);

        return true;
    }

//eof

    public static function removeProductMMByProductID($iProductID) {
        if ($iProductID && is_numeric($iProductID)) {
            $sql = "DELETE FROM r_mm_products WHERE fk_product_id = {$iProductID}";
            DAL::executeQuery($sql);
        } else {
            return false;
        }
    }

//eof

    static public function getMMTypeList() {

        $sql = "SELECT mmt.mmtype_id as id, mmt.comment as comment
				FROM multimediatypes mmt";

        return DAL::executeQuery($sql);
    }

//eof

    static public function getMMTypebyID($id) {

        $sql = "SELECT mt.mmtype_id as id, mt.name as name, mt.folder as folder, mt.iconfile as iconfile, mt.description as description, mt.type as type, mt.maxw as maxwidth, mt.maxh as maxheight
				FROM multimediatypes mt
				WHERE mt.mmtype_id = {$id}";
        return DAL::executeGetRow($sql);
    }

//eof

    static public function addMMLink($iWebsiteID, $type, $url, $target, $comment) {

        $sSite = strtolower(Websites::getWebsiteTitleByID($iWebsiteID));

        $tablename = "multimedia_cmn";
        $data = array(
            'file' => $sSite,
            'fk_mmtype_id' => $type,
            'url' => $url,
            'target' => $target,
            'comment' => $comment
        );
        DAL::Insert($tablename, $data);
    }

//eof

    static public function uploadImage($iWebsiteID, $filename, $comment, $tempname, $iMMTypeID, $userid, $pid = NULL, $url = NULL, $quality = 100) {
        $ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
        $sNewFilename = MultimediaAdmin::getNewFilename($iWebsiteID, $filename, $ext, $comment, $pid);
        if (MultimediaAdmin::saveImage($iWebsiteID, $sNewFilename, $tempname, $iMMTypeID, $quality)) {
            $author = "";
            return MultimediaAdmin::insertMMtoDB($iMMTypeID, $sNewFilename, $sNewFilename, $comment, $author, $url, $userid, $pid);
        } else {
            return false;
        }
    }

//eof

    static public function insertExternalImage($iMMTypeID, $sImageUrl, $sThumbnail = "", $comment, $userid, $author, $pid = NULL, $iLangID = 0, $iImageRankID = 0) {
        if ($sThumbnail == "") {
            $sThumbnail = str_replace(array("jpg", "png", "/org/"), array("gif", "gif", "/square60/"), $sImageUrl);
        }
        return MultimediaAdmin::insertMMtoDB($iMMTypeID, $sImageUrl, $sThumbnail, $comment, $author, $url, $userid, $pid, $iLangID, $iImageRankID);
    }

//eof

    static public function resizeImage($oImageResource, $ext, $iWidth, $iHeight) {
        $bCheck = true;
        if ($ext != "gif") {
            $oResizedImage = imagecreatetruecolor($iWidth, $iHeight) or $bCheck = false;
        } else {
            $oResizedImage = imagecreate($iWidth, $iHeight);
            $background = imagecolorallocate($oResizedImage, 218, 27, 39);
            $color = imagecolorallocate($oResizedImage, 255, 255, 255);
            imagefill($oResizedImage, 0, 0, $background);
            imagecolortransparent($oResizedImage, $background);
        }
        if (function_exists('imagecopyresampled')) {
            imagecopyresampled($oResizedImage, $oImageResource, 0, 0, 0, 0, $iWidth, $iHeight, imagesx($oImageResource), imagesy($oImageResource)) or $bCheck = false;
        } else {
            imagecopyresized($oResizedImage, $oImageResource, 0, 0, 0, 0, $iWidth, $iHeight, imagesx($oImageResource), imagesy($oImageResource)) or $bCheck = false;
        }


        if ($bCheck) {
            return $oResizedImage;
        } else {
            return $bCheck;
        }
    }

//eof

    static public function createThumbnail($oImageResource, $fullfolder, $sNewFilename, $iMMTypeID = 0, $iLangID = NULL) {
        $bCheck = true;

        $tempname = "thumbs/$sNewFilename";
        $tempPath = $_SERVER['DOCUMENT_ROOT'] . "/tempimages/$tempname";
        $thumbPath = $fullfolder . "/thumbs/";

        $ext = strtolower(substr($sNewFilename, strrpos($sNewFilename, '.') + 1));
        $img = MultimediaAdmin::resizeImage($oImageResource, $ext, 80, 60);
        if (MultimediaAdmin::saveImageResource($img, $tempPath, $ext)) {
            $bCheck = MultimediaAdmin::copyMMToLangs($tempPath, $thumbPath, $sNewFilename, $iLangID, $iMMTypeID);
        } else {
            $bCheck = false;
        }
        File::deleteTempImage($tempname);
        return $bCheck;
    }

//eof

    static public function saveImage($iWebsiteID, $sNewFilename, $tempname, $iMMTypeID, $iQuality = 100) {
        $bValid = true;
        $aMMtype = MultimediaAdmin::getMMTypebyID($iMMTypeID);
        $mmtype = $aMMtype[type];
        $folder = MultimediaAdmin::getFolderbyID($iMMTypeID);
        $fullfolder = "$mmtype/$folder";
        File::uploadTempImage($tempname, $sNewFilename);
        $sTempFilename = $_SERVER['DOCUMENT_ROOT'] . "/tempimages/" . $sNewFilename;
        $oImageResource = MultimediaAdmin::getImageResource($sTempFilename);

        $aResizeSizes = MultimediaAdmin::checkNeedsResize($sTempFilename, $iMMTypeID);

        if ($aResizeSizes) {
            $oImageResource = MultimediaAdmin::resizeImage($oImageResource, $ext, $aResizeSizes["width"], $aResizeSizes["height"]);
            MultimediaAdmin::saveImageResource($oImageResource, $sTempFilename, $ext, $iQuality);
        } else if ($iQuality < 100) {
            MultimediaAdmin::saveImageResource($oImageResource, $sTempFilename, $ext, $iQuality);
        }

        $folder = MultimediaAdmin::getFolderbyID($iMMTypeID);
        if (!MultimediaAdmin::copyMMToLangs($sTempFilename, $fullfolder, $sNewFilename, $iLangID, $iMMTypeID)) {
            $bValid = false;
        }
        if (($iMMTypeID == MultimediaAdmin::PRODUCTIMAGES) || ($iMMTypeID == MultimediaAdmin::GALLERYIMAGES)) {
            if (!MultimediaAdmin::createThumbnail($oImageResource, $fullfolder, $sNewFilename, $iMMTypeID, $iLangID)) {
                $bValid = false;
            }
        }
        File::deleteTempImage($sNewFilename);
        return $bValid;
    }

//eof

    static public function deleteImage($mmid, $pid = NULL) {
        if ($pid) {
            $totalcount = MultimediaAdmin::assignedCount($mmid);
            if ($totalcount >= 1) {
                MultimediaAdmin::deleteMMGlobally($mmid);
            } else {
                MultimediaAdmin::removeProductMMbyMMID($mmid, $pid);
                MultimediaAdmin::removePageMMbyMMID($mmid, $pid);
            }
        } else {
            MultimediaAdmin::deleteMMGlobally($mmid);
        }
    }

//eof

    static public function deleteMMGlobally($id) {
        $bValid = MultimediaAdmin::deleteMMfile($id);

        if ($bValid == true) {
            MultimediaAdmin::removeProductMMbyMMID($id);
            MultimediaAdmin::removePageMMbyMMID($id);
            MultimediaAdmin::deleteMultimedia($id);
        }
        return $bValid;
    }

//eof

    static public function copyMMToLangs($tempname, $folder, $filename, $iLangID = NULL, $iMMTypeID = 0) {
        $bCheck = true;
        if ($iMMTypeID == MultimediaAdmin::PRODUCTIMAGES) {
            $iLangID = 1;
        }

        if ($iLangID == NULL) {
            $languages = Language::getAllAdminLanguageList();

            foreach ($languages as $key => $value) {
                $rootfolder = $value[folder];
                $prod_img = MMPATH . "$rootfolder/$folder/$filename";
                if (!File::copyFile($tempname, $prod_img)) {
                    $bCheck = false;
                }
            }
        } else {
            $rootfolder = Language::getFolder($iLangID);
            $prod_img = MMPATH . "$rootfolder/$folder/$filename";
            if (!File::copyFile($tempname, $prod_img)) {
                $bCheck = false;
            }
        }
        return $bCheck;
    }

//eof

    static public function getNewFilename($iWebsiteID, $filename, $ext, $comment, $pid = 0) {
        if ($iWebsiteID != 0) {
            $sSite = Websites::getWebsiteTitleByID($iWebsiteID);
        } else {
            $sSite = "";
        }
        $iNextId = MultimediaAdmin::getLargestMMid() + 1;
        if ($pid) {
            $sProductName = File::FilterStringToSEO(Products::getProductTitle($pid));
            if ($sSite != "") {
                $sNewFilename = strtolower($sSite) . "-" . strtolower($sProductName) . "-$iNextId.$ext";
            } else {
                $sNewFilename .= strtolower($sProductName) . "-$iNextId.$ext";
            }
        } else {
            if ($sSite != "") {
                $comment = File::FilterStringToSEO($comment);
                $sNewFilename = strtolower($sSite) . "-" . strtolower($comment) . "-$iNextId.$ext";
            } else {
                $sNewFilename .= strtolower($comment) . "-$iNextId.$ext";
            }
        }
        return $sNewFilename;
    }

//eof

    static public function getImageResource($sTempFilename) {
        $ext = strtolower(substr($sTempFilename, strrpos($sTempFilename, '.') + 1));
        $bCheck = true;
        switch ($ext) {
            case "jpg":
                $oImageResource = imagecreatefromjpeg($sTempFilename) or $bCheck = false;
                break;
            case "jpeg":
                $oImageResource = imagecreatefromjpeg($sTempFilename) or $bCheck = false;
                break;
            case "gif":
                $oImageResource = imagecreatefromgif($sTempFilename) or $bCheck = false;
                break;
            case "png":
                $oImageResource = imagecreatefrompng($sTempFilename) or $bCheck = false;
                break;
            default:
                $oImageResource = imagecreatefromjpeg($sTempFilename) or $bCheck = false;
                break;
        }

        if ($bCheck) {
            return $oImageResource;
        } else {
            return $bCheck;
        }
    }

//eof

    static public function saveImageResource($oImageResource, $sFilename, $ext, $iImageQuality = 100) {
        $bCheck = true;
        switch ($ext) {
            case "jpg":
            case "jpeg":
                imagejpeg($oImageResource, $sFilename, $iImageQuality) or $bCheck = false;
                break;
            case "gif":
                imagegif($oImageResource, $sFilename) or $bCheck = false;
                break;
            case "png":
                imagepng($oImageResource, $sFilename, 0) or $bCheck = false;
                break;
            default:
                imagejpeg($oImageResource, $sFilename, $iImageQuality) or $bCheck = false;
                break;
        }
        return $bCheck;
    }

//eof

    static public function checkNeedsResize($sTempFilename, $iMMTypeID) {
        $aMMtype = MultimediaAdmin::getMMTypebyID($iMMTypeID);
        $aResourceSizes = getimagesize($sTempFilename);
        $iCurrentWidth = $aResourceSizes[0];
        $iCurrentHeight = $aResourceSizes[1];

        $iTypeWidth = $aMMtype["maxwidth"];
        $iTypeHeight = $aMMtype["maxheight"];

        $aNewSizes = NULL;
        if (!empty($iTypeWidth) || !empty($iTypeHeight)) {
            if (($iCurrentHeight > $iTypeHeight) || ($iCurrentWidth > $iTypeWidth)) {
                if (($iTypeHeight - $iCurrentWidth) > ($iTypeHeight - $iCurrentHeight)) {
                    $iRatio = $iTypeHeight / $iTypeHeight;
                } else { // Resize using width as ratio
                    $iRatio = $iTypeHeight / $iCurrentWidth;
                }
                $iNewWidth = round($iCurrentWidth * $iRatio);
                $iNewHeight = round($iCurrentHeight * $iRatio);

                $aNewSizes = array("width" => $iNewWidth,
                    "height" => $iNewHeight,
                    "ratio" => $iRatio);
            }
        }

        return $aNewSizes;
    }

//eof

    static public function insertMMtoDB($iMMTypeID, $filename, $sThumbnail, $comment, $author, $url, $userid, $pid = 0, $iLangID = 0, $iImageRankID = 0) {
        $mmid = MultimediaAdmin::addMMFile($iMMTypeID, $filename, $sThumbnail, $comment, $comment, $author, "", $url, NULL, NULL, $userid, true);

        if ($pid) {
            ProductsAdmin::addMMProduct($pid, $mmid, $iImageRankID);
        }

        return $mmid;
    }

//eof

    static public function uploadFile($iWebsiteID, $filename, $comment, $tempname, $iMMTypeID, $userid, $pid = NULL, $todate = NULL, $fromdate = NULL) {
        $bValid = true;
        $ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
        $sNewFilename = MultimediaAdmin::getNewFilename($iWebsiteID, $filename, $ext, $comment, $pid);
        $aMMtype = MultimediaAdmin::getMMTypebyID($iMMTypeID);
        $mmtype = $aMMtype[type];
        $folder = MultimediaAdmin::getFolderbyID($iMMTypeID);
        $fullfolder = "$mmtype/$folder";
        $sSite = Websites::getWebsiteTitleByID($iWebsiteID);
        $author = "Choose" . $sSite . ".com";

        if (!File::uploadTempImage($tempname, $sNewFilename)) {
            Error::addError("The '$image' could not be uploaded to the temp folder.");
            $bValid = false;
        }

        $sTempFilename = $_SERVER['DOCUMENT_ROOT'] . "/tempimages/" . $sNewFilename;

        if (!MultimediaAdmin::copyMMToLangs($sTempFilename, $fullfolder, $sNewFilename, $iLangID, $iMMTypeID)) {
            Error::addError("The '$image' could not be copied to the temp folder.");
            $bValid = false;
        }

        if ($bValid) {
            MultimediaAdmin::insertMMtoDB($iMMTypeID, $sNewFilename, $sNewFilename, $comment, $author, "", $userid, $pid);
        }

        File::deleteTempImage($sNewFilename);
        return $bValid;
    }

//eof

    static public function deleteMMfile($id) {
        $bValid = true;
        $currentresult = Multimedia::getMMItem($id);
        $mmtype = MultimediaAdmin::getMMTypeTypeByID($currentresult[mmtype_id]);
        if ($mmtype == "images" || $mmtype == "files") {
            $folder = MultimediaAdmin::getFolderByID($currentresult[mmtype_id]);
            $oldfilename = $currentresult[file];
            $thumbimg = "thumbs/" . $currentresult[file];
            $subfolder = ($mmtype == "images") ? "images" : "files";
            $folder = "$subfolder/$folder";


            $result = Language::getAllAdminLanguageList();
            foreach ($result as $key => $value) {
                $rootfolder = $value[folder];
                if (!File::checkFolderPermissions(MMPATH . "$rootfolder/$folder")) {
                    Error::addError("The '../$rootfolder/$folder' folder has incorrect permissions. Please CHMOD the folder to 777 using FTP.");
                    $bValid = false;
                }
                if (file_exists(MMPATH . "$rootfolder/$folder/$oldfilename") && ($bValid == true)) {
                    if (!unlink(MMPATH . "$rootfolder/$folder/$oldfilename")) {
                        Error::addError("The '$oldfilename' could not be deleted from the '$rootfolder/$folder/' folder.");
                        $bValid = false;
                    }
                }
                if (file_exists(MMPATH . "$rootfolder/$folder/$thumbimg") && ($bValid == true)) {
                    if (!unlink(MMPATH . "$rootfolder/$folder/$thumbimg")) {
                        Error::addError("The thumbnail '$oldfilename' could not be deleted from the '$rootfolder/$folder/thumbs/' folder.");
                        $redirect = "multimedia.php";
                        $bValid = false;
                    }
                }
            }
        }
        return $bValid;
    }

//eof

    static private function deleteMultimedia($id) {

        $sql = "DELETE FROM multimedia_cmn WHERE mm_id = {$id}";
        DAL::executeQuery($sql);
    }

//eof

    static private function getLargestMMid() {
        $sql = "SELECT MAX(mmc.mm_id) as id
				FROM multimedia_cmn mmc ";


        return DAL::executeGetOne($sql);
    }

//eof

    static public function renameMM($iWebsiteID, $id, $filename) {
        $bValid = true;
        $currentresult = Multimedia::getMMItem($id);
        $result = Language::getAdminLanguageList($iWebsiteID);

        $folder = MultimediaAdmin::getFolderByID($currentresult["mmtype_id"]);
        $sOldFilename = $currentresult[file];
        $aOldFilename = explode("-", $sOldFilename);
        $filename = File::convertProductTitleForURL($filename);
        $sNewFilename = $aOldFilename[0] . "-" . $filename . "-" . $aOldFilename[sizeof($aOldFilename) - 1];

        $type = MultimediaAdmin::getMMTypeTypeByID($currentresult["mmtype_id"]);
        if ($type == "images") {
            $subfolder = "images";
        } else {
            $subfolder = "files";
        }
        $folder = "$subfolder/$folder";
        $thumbfolder = "$folder/thumbs/";
        $sPath = SYSTEMPATH . "media/";
        foreach ($result as $key => $value) {
            //Check image folder and thumbnail folder permissions
            $rootfolder = $value["folder"];
            $bValid = File::checkFileExists($sPath, $rootfolder, $folder, $sOldFilename);
            if ($bValid) {
                $bValid = File::checkFolderPermissions($sPath . "$rootfolder/$folder");
                if ($bValid) {
                    $bValid = File::checkFileExists($sPath, $rootfolder, $thumbfolder, $sOldFilename);
                    if ($bValid) {
                        $bValid = File::checkFolderPermissions($sPath . "$rootfolder/$thumbfolder");
                    }
                }
            }
        }

        if ($bValid == true) {
            foreach ($result as $key => $value) {
                $rootfolder = $value["folder"];
                $bValid = File::renameFile($rootfolder, $folder, $sNewFilename, $sOldFilename);
                if ($bValid) {
                    $bValid = File::renameFile($rootfolder, $thumbfolder, $sNewFilename, $sOldFilename);
                }
            }
        }
        if ($bValid == true) {
            MultimediaAdmin::updateFileName($id, $sNewFilename, $sNewFilename);
        }

        return $bValid;
    }

//eof

    static public function changeMMFile($id, $tempname, $iMMTypeID, $filename, $quality = 100) {

        $bValid = true;
        $aMMtype = MultimediaAdmin::getMMTypebyID($iMMTypeID);
        $type = $aMMtype['type'];
        $currentresult = Multimedia::getMMItem($id);
        $iWebsiteID = Websites::getAdminWebsiteID();

        $folder = MultimediaAdmin::getFolderByID($currentresult['mmtype_id']);
        $oldfilename = $currentresult['file'];
        $filename .= substr($currentresult['file'], -4);

        $bValid = MultimediaAdmin::deleteMMfile($id);
        if ($bValid) {
            $bValid = MultimediaAdmin::saveImage($iWebsiteID, $oldfilename, $tempname, $iMMTypeID, $quality);
        }
        return $bValid;
    }

//eof

    static public function checkMMProductItemByID($iProductID, $iMMID) {
        $sql = "SELECT count(fk_mm_id)
				FROM r_mm_products rmp
				WHERE rmp.fk_product_id = {$iProductID}
				AND rmp.fk_mm_id = {$iMMID}";

        return DAL::executeGetOne($sql);
    }

//eof

    static public function updateMMComment($iMMID, $sComment) {

        $tablename = "multimedia_cmn";

        $data = array(
            'comment' => $sComment
        );
        $params = "mm_id = '$iMMID'";
        DAL::Update($tablename, $data, $params);
    }

//eof

    static public function updateMMTitle($iMMID, $sTitle) {

        $tablename = "multimedia_cmn";

        $data = array(
            'title' => $sTitle
        );
        $params = "mm_id = '$iMMID'";
        DAL::Update($tablename, $data, $params);
    }

//eof

    static public function updateFileName($id, $filename, $thumbnail) {

        $tablename = "multimedia_cmn";

        $data = array(
            'file' => $filename,
            'thumbnail' => $thumbnail
        );
        $params = "mm_id = '$id'";
        DAL::Update($tablename, $data, $params);
    }

//eof

    static public function addMMFile($type, $file, $sThumbnail, $comment, $title, $author, $desc, $url, $fromdate, $todate, $userid, $bReturn = false) {
        $tablename = "multimedia_cmn";
        $data = array(
            'fk_mmtype_id' => $type,
            'file' => $file,
            'thumbnail' => $sThumbnail,
            'comment' => $comment,
            'title' => $title,
            'author' => $author,
            'description' => $desc,
            'url' => $url,
            'fromdate' => $fromdate,
            'todate' => $todate,
            'fk_user_id' => $userid,
            'isactive' => 1
        );
        return DAL::Insert($tablename, $data, $bReturn);
    }

//eof

    static public function assignedCount($mmid) {
        return MultimediaAdmin::isMMAssignedToPages($mmid) + MultimediaAdmin::isMMAssignedToProducts($mmid);
    }

//eof

    static public function isMMTypeBeingUsedInMM($id) {

        $sql = "SELECT COUNT(fk_mmtype_id)
				FROM multimedia_cmn
				WHERE fk_mmtype_id = {$id}";
        return DAL::executeGetOne($sql);
    }

//eof

    static public function isMMAssignedToProducts($id) {

        $sql = "SELECT COUNT(fk_product_id)
				FROM r_mm_products
				WHERE fk_mm_id = {$id}";
        return DAL::executeGetOne($sql);
    }

//eof

    static public function isMMAssignedToPages($id) {

        $sql = "SELECT COUNT(fk_page_id)
				FROM r_mm_pages
				WHERE fk_mm_id = {$id}";
        return DAL::executeGetOne($sql);
    }

}