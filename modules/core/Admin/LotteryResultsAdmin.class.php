<?php

/**
 * LotteryResultsAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class LotteryResultsAdmin {

    // CLASS CONSTANTS ADDED BY NATHAN
    const CHECKER_CANCELLED_STATUS = 4; // Status id of a cancelled check
    const CHECKER_THRESHOLD = 2; // Number of check entries required before equality check made


    protected $iLotteryID;


    public function addResults($iLotteryID, $dDrawDate, $iDraw, $aNumbers, $fJackpot, $oWinnings) {
        $sNumbers = implode("|", $aNumbers);
        $sWinnings = serialize($oWinnings);
        $iWon = (int) $bWon;

        $tablename = "lottery_draws";
        $data = array(
            'fk_lottery_id' => $iLotteryID,
            'fk_lotterydate' => $dDrawDate,
            'draw' => $iDraw,
            'numbers' => $sNumbers,
            'jackpot' => $fJackpot,
            'winnings' => $sWinnings,
            'processed' => 0
        );
        return DAL::Insert($tablename, $data, true);
    }

    public function editResult($iLotteryID, $dDrawDate, $iDraw, $aNumbers, $fJackpot, $oWinnings) {
        $sNumbers = implode("|", $aNumbers);
        $sWinnings = serialize($oWinnings);

        /*
        $iProcessed = (int) 0;
        $tablename = "lottery_draws";
        $data = array(
            'numbers' => $sNumbers,
            'jackpot' => $fJackpot,
            'winnings' => $sWinnings,
            'processed' => $iProcessed
        );
        $params = 'fk_lottery_id = ' . $iLotteryID . " AND " . 'fk_lotterydate = "' . $dDrawDate . '" AND draw = ' . $iDraw;
        */

        // Had to change to raw SQL here as, for some reason,
        // the processed flag kept on getting set to null, instead of 0
        $sSQL="UPDATE lottery_draws " .
              "SET numbers='{$sNumbers}', " .
              "jackpot='{$fJackpot}', " .
              "winnings='{$sWinnings}', " .
              "processed = 0 " .
              "WHERE fk_lottery_id = {$iLotteryID} " .
              "AND fk_lotterydate = '{$dDrawDate}' " .
              "AND draw = {$iDraw}";

        return DAL::executeQuery($sSQL);

        //return DAL::Update($tablename, $data, $params);
    }

    public function getDrawDates($iLotteryID, $bPast = true, $bUnassigned = false) {
        $sWhere = "";
        if ($bPast) {
            $sWhere .= " AND DATE(rld.drawdate) <=CURDATE()";
        }
        if ($bUnassigned) {
            $sWhere .= " AND ld.numbers IS NULL ";
        }
        $sSQL = "SELECT	rld.drawdate,
						rld.price,
						rld.jackpot,
                        rld.rollover
				FROM r_lottery_dates rld
				LEFT JOIN lottery_draws ld ON ld.fk_lotterydate = rld.drawdate AND ld.fk_lottery_id = rld.fk_lottery_id
				WHERE rld.fk_lottery_id = {$iLotteryID}
				{$sWhere}
				ORDER BY rld.drawdate ASC";
        return DAL::executeQuery($sSQL);
    }

    public function getWinningsCombinations($iLotteryID) {

        $sClassName = Lottery::getLotteryClassName($iLotteryID, false);
        if ($sClassName) {
            $oLottery = new $sClassName(1);
        }


        return
                array(
                    "currency" => $oLottery->getCurrencyDetails(),
                    "matches" => $oLottery->getNumberCombinations(),
                    "bonus" => $oLottery->getDrawnBonusNumbersCount(),
                    "numbers" => $oLottery->getNumbersCount(),
                    "bonus_text" => $oLottery->getBonusText(),
                    "bonus_plural" => $oLottery->getBonusTextPlural()
        );
    }

    public function getWinningAmounts($iLotteryID) {
        $sClassName = Lottery::getLotteryClassName($iLotteryID, false);
        if ($sClassName) {
            $oLottery = new $sClassName(1);
            return $oLottery->getWinningAmounts();
        }
        return false;
    }

    /**
     * Match any winners for this lottery.
     *
     * Creates new lottery object and calls that object's matchWinners function,
     * finds winners, evaluates their winnings,
     * adds winners found to the lottery_winners table,
     * sets draw as processed and then deals with any losers.
     *
     * @param type $iLotteryID lottery id
     * @param type $iDraw draw id
     *
     * @return boolean
     */
    public function matchWinners($iLotteryID, $iDraw) {
        $sClassName = Lottery::getLotteryClassName($iLotteryID, false);
        if ($sClassName) {
            $oLottery = new $sClassName(1);
        }

        $aWinners = $oLottery->matchWinners(LotteryResultsAdmin::getLotteryDraw($iLotteryID, $iDraw), TicketsAdmin::getTicketsByDraw($iLotteryID, $iDraw), $iDraw);
        foreach ($aWinners as $aWinner) {
            $aWinner['transaction_amount'] = LotteryResultsAdmin::evaluateWinning($aWinner['amount'], $oLottery->getCurrencyID());
            LotteryResultsAdmin::addWinner($aWinner);
        }

        LotteryResultsAdmin::setProcessed($iLotteryID, $iDraw);

        if (LotteryResultsAdmin::getLosers($iLotteryID, $iDraw, 1, true)) {
            LotteryResultsAdmin::setLoosersEmailSent($iLotteryID, $iDraw);
        }

        return true;
    }

    /**
     * Evaluates a customer's winning amount
     *
     * @param float $fAmount amount to evaluate
     * @param integer $iCurrencyID currency id
     * @return float
     */
    protected function evaluateWinning($fAmount, $iCurrencyID) {
        return CurrencyAdmin::convertToBase($fAmount, $iCurrencyID, 0);
    }

    public function getLotteryDraws($iLotteryID, $iLimit = NULL) {
        $sSQL = "SELECT
					ld.draw,
					ld.fk_lotterydate as date,
					ld.numbers,
					ld.jackpot,
					ld.processed,
					lc.timezone,
					COUNT(lw.fk_bookingitem_id) as winners
				FROM lottery_draws ld
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = ld.fk_lottery_id AND lc.lottery_id = {$iLotteryID}
				LEFT JOIN lottery_winnings lw ON ld.fk_lottery_id = lw.fk_lottery_id AND ld.draw = lw.draw
				GROUP BY ld.fk_lottery_id, ld.draw
				ORDER BY ld.fk_lotterydate DESC";
        if ($iLimit) {
            $sSQL .= " LIMIT " . $iLimit;
        }
        return DAL::executeQuery($sSQL);
    }

    public function getLotteryDraw($iLotteryID, $iDraw) {
        $sSQL = "SELECT
				ld.draw,
				ld.fk_lotterydate as date,
				ld.numbers,
				ld.winnings,
				ld.jackpot,
				ld.processed
			FROM lottery_draws ld
			WHERE ld.fk_lottery_id = {$iLotteryID}
				AND ld.draw = {$iDraw} ";

        return DAL::executeGetRow($sSQL);
    }

    /**
     * Add a winner to the lottery_winnings table
     *
     * @param array $aData
     * @return boolean
     */
    public function addWinner($aData) {
        $sTablename = "lottery_winnings";
        return DAL::Insert($sTablename, $aData);
    }

    /**
     * Get losers for this lottery and send email if it's their first order
     *
     * @param type $iLotteryID
     * @param type $iDraw
     * @param type $iLangID
     * @param type $bProcess
     * @return boolean
     */
    protected function getLosers($iLotteryID, $iDraw, $iLangID = 1, $bProcess = false) {
       /*$sSQL = "SELECT
						m.email,
						m.firstname,
						m.lastname,
						bi.fk_lotterydate as lotterydate,
						ll.title as lottery

				FROM booking_items bi
				INNER JOIN lotteries_lang ll ON ll.fk_lottery_id = bi.fk_lottery_id AND ll.fk_language_id = {$iLangID}
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
				INNER JOIN members m ON m.member_id = b.fk_member_id
				INNER JOIN lottery_draws ld ON ld.fk_lottery_id = bi.fk_lottery_id AND ld.fk_lotterydate = bi.fk_lotterydate
				LEFT JOIN lottery_winnings lw ON lw.fk_lottery_id = ld.fk_lottery_id AND lw.draw = ld.draw AND bi.bookingitem_id = lw.fk_bookingitem_id
				WHERE ld.processed = 1
					AND ld.emailed = 0
					AND bi.fk_lottery_id = {$iLotteryID}
					AND ld.draw = {$iDraw}
				GROUP BY m.member_id
				HAVING COUNT(lw.draw) = 0";*/

        // Revised query
        $sSQL = "SELECT	m.email,
                        m.firstname,
                        m.lastname,
                        m.balance,
                        m.member_id,
                        DATE_FORMAT(bi.fk_lotterydate, '%W %D %M %Y') as lotterydate,
                        ll.title as lottery,
                        ld.numbers as drawn_numbers,
                        bi.numbers as purchased_numbers
                FROM booking_items bi
                INNER JOIN lotteries_lang ll ON ll.fk_lottery_id = bi.fk_lottery_id AND ll.fk_language_id = {$iLangID}
                INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
                INNER JOIN members m ON m.member_id = b.fk_member_id
                INNER JOIN lottery_draws ld ON ld.fk_lottery_id = bi.fk_lottery_id AND ld.fk_lotterydate = bi.fk_lotterydate
                LEFT JOIN lottery_winnings lw ON lw.fk_lottery_id = ld.fk_lottery_id AND lw.draw = ld.draw AND bi.bookingitem_id = lw.fk_bookingitem_id
                WHERE ld.processed = 1
                        AND ld.emailed = 0
                        AND bi.fk_lottery_id = {$iLotteryID}
                        AND ld.draw = {$iDraw}
                GROUP BY m.member_id
                HAVING COUNT(lw.draw) = 0";

        $aLosers = DAL::executeQuery($sSQL);
        if ($bProcess) {
            $bValid = true;
            foreach ($aLosers as $aLoser) {

                // Check if this is their first order,
                // and send losing email if so

                if (Member::isFirstOrder($aLoser['member_id']))
                {
                    /*if (!EmailAdmin::sendLotteryLostEmail($aLoser['firstname'], $aLoser['lastname'], $aLoser['email'], $aLoser['lottery'], $aLoser['lotterydate'])) {
                        $bValid = false;
                    }*/

                    // Return the balance value to 2 decimal places, rounded down
                    // This will then be further formatted to 2display as 2 decimal places
                    $aLoser['balance'] = floor($aLoser['balance'] * 100) / 100;

                    $aEmailData = array('username'          => $aLoser['email'],
                                        'current balance'   => number_format_locale($aLoser['balance'], 2, '.', ','),
                                        'firstname'         => $aLoser['firstname'],
                                        'lottery_name'      => $aLoser['lottery'],
                                        'drawdate'          => $aLoser['lotterydate'],
                                        'drawn_numbers'     => $aLoser['drawn_numbers'],
                                        'purchased_numbers' => $aLoser['purchased_numbers'],
                                        'member_id'         => $aLoser['member_id'],
                                        'email_ref'         => 'first_order_lost');

                    Email::sendFirstOrderLosing($aLoser['email'], $aEmailData);
                }
            }
            return $bValid;
        } else {
            return $aLosers;
        }
    }

    public function addWinTransaction($iMemberID, $iBookingItemID, $iLotteryID, $fTransactionAmount, $sTransactionReference) {

        $oPaymentGateway = new WinningsGateway();
        $oPaymentGateway->assignAmount($fTransactionAmount);
        $iTransactionID = $oPaymentGateway->addTransaction($iMemberID, CurrencyAdmin::getBaseCurrencyID());
        if (LotteryResultsAdmin::updateWinnerTransaction($iBookingItemID, $fTransactionAmount, $iTransactionID)) {
            if ($oPaymentGateway->ProcessPayment($sTransactionReference)) {
                Member::updateMemberBalance($iMemberID);
                return true;
            }
        }
        return false;
    }

    /**
     * Gets winners for this draw.
     *
     * Called from administration/lottery-results-check.php as part of the winscan
     * (or from match-winners.php)
     *
     * If draw is not yet processed, call the matchWinners function in this class
     *
     * If a summary is then required, get asummart of all winners in this draw,
     * else show all win information for this draw
     *
     * @param type $iLotteryID lottery ID
     * @param type $iDraw draw ID
     * @param type $bProcess whether to process or not
     * @param type $bSummary whether to return a summary or all information
     *
     * @return array
     */
    public function getWinners($iLotteryID, $iDraw, $bProcess = false, $bSummary = false) {

        $sSQL = "SELECT ld.processed
				FROM lottery_draws ld
				WHERE ld.fk_lottery_id = {$iLotteryID}
				AND ld.draw = {$iDraw}";
        $bProcessed = DAL::executeGetOne($sSQL)==1?true:false;
        if (!$bProcessed && !$bProcess) {
            return false;
        }
        if ($bProcess && !$bProcessed) {
            LotteryResultsAdmin::matchWinners($iLotteryID, $iDraw);
        }
        if ($bSummary) {
            $sGroupBy = " GROUP BY bi.purchased, lw.emailed, lw.paid, lw.type ";
            $sGroupSelect = "COUNT(*) as winners,
							SUM(lw.amount) as total_winnings,
							SUM(lw.transaction_amount) as total_transaction_amount,
							SUM(bi.purchased) as purchased_tickets, ";
        }
        $sSQL = "SELECT {$sGroupSelect}
						lw.*,
						m.firstname,
						m.lastname,
						m.email,
                                                m.balance,
                                                m.member_id,
						t.bookingreference as transaction,
						t.transaction_date,
						bi.purchased,
						bi.bookingitem_id as bookingitem_id,
						b.booking_id as booking_id
				FROM lottery_winnings lw
				INNER JOIN lottery_draws ld ON ld.fk_lottery_id = lw.fk_lottery_id AND ld.draw = lw.draw
				INNER JOIN booking_items bi ON bi.bookingitem_id = lw.fk_bookingitem_id
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
				INNER JOIN members m ON m.member_id = b.fk_member_id
				LEFT JOIN transactions t ON t.transaction_id = lw.fk_transaction_id
				WHERE lw.fk_lottery_id = {$iLotteryID}
					AND lw.draw = {$iDraw}
				{$sGroupBy}
				ORDER BY lw.amount DESC, lw.paid DESC";
        return DAL::executeQuery($sSQL);
    }

    public function updateWinnerTransaction($iBookingItemID, $fTransactionAmount, $iTransactionID) {
        $aData = array(
            'paid' => 1,
            'transaction_amount' => $fTransactionAmount,
            'fk_transaction_id' => $iTransactionID
        );
        $sTablename = "lottery_winnings";
        $sParams = "fk_bookingitem_id = {$iBookingItemID}";
        DAL::Update($sTablename, $aData, $sParams);

        $aData = array(
            'confirmed' => 1
        );
        $sTablename = "transactions";
        $sParams = "transaction_id = {$iTransactionID}";
        return DAL::Update($sTablename, $aData, $sParams);
    }

    public function setWinnerEmailSent($iBookingItemID) {
        $aData = array(
            'emailed' => 1
        );
        $sTablename = "lottery_winnings";
        $sParams = "fk_bookingitem_id = {$iBookingItemID}";
        return DAL::Update($sTablename, $aData, $sParams);
    }

    /**
     * Set loser's email as sent
     *
     * @param integer $iLotteryID lottery id
     * @param integer $iDraw draw id
     */
    public function setLoosersEmailSent($iLotteryID, $iDraw) {
        $aData = array(
            'emailed' => 1
        );
        $sTablename = "lottery_draws";
        $sParams = "fk_lottery_id = {$iLotteryID} AND draw = {$iDraw}";
        DAL::Update($sTablename, $aData, $sParams);
    }

    /**
     * Set a lottery draw as processed, once the winscan has finished
     * checking this particular lottery
     *
     * @param integer $iLotteryID lottery id
     * @param integer $iDraw draw id
     */
    public function setProcessed($iLotteryID, $iDraw) {
        $aData = array(
            'processed' => 1
        );
        $sTablename = "lottery_draws";
        $sParams = "fk_lottery_id = {$iLotteryID} AND draw = {$iDraw}";
        DAL::Update($sTablename, $aData, $sParams);
    }

    public function getLotteryResultPageURL($iLotteryID, $iDraw) {
        $sSQL = "SELECT
					CONCAT('http://','" . WEBSITEURL . "', l.code, '/', sc.folder, CONCAT(pc.url, '?draw=', ld.draw)) as url
				FROM lottery_draws ld
				INNER JOIN lotteries_cmn ll ON ll.lottery_id = ld.fk_lottery_id
				INNER JOIN pages_cmn pc ON pc.page_id = ll.results_page_id
				INNER JOIN pages_lang pl ON pl.fk_page_id = pc.page_id
				INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id
				INNER JOIN languages l ON l.language_id = pl.fk_language_id
				WHERE pc.is_active = 1
					AND pl.is_active = 1
					AND pc.showinsitemap = 1
					AND sc.is_active = 1
					AND l.is_active = 1
					AND ll.is_active = 1
					AND ll.lottery_id = {$iLotteryID}
					AND ld.draw = {$iDraw}
					AND l.default = 1
				";
        return DAL::executeGetOne($sSQL);
    }

    /************************************\
     |
     |
     | <NATHAN 10/03/2014>
     |
     | All code below this point has been added
     | in order to provide functionality for 3-phase results checking
     |
     |
     \**********************************/


    /**
     * Creates entries in lottery_results_checking table
     * ready to be populated with results once checked
     * @param integer $iLotteryID
     * @param date $dDrawDate
     * @param integer $iDraw
     * @param array $aCheckers
     * @return array
     */
    public function createCheckingRowsForLotteryDraw($iLotteryID, $dDrawDate, $iDraw, $aCheckers) {

        $tablename = "lottery_results_checking";
        $data = array(
            'fk_lottery_id' => $iLotteryID,
            'fk_lottery_date' => $dDrawDate,
            'draw_ref' => $iDraw,
            'security_hash' => '',
            'checker_id' => ''
        );

        // Need to run this query multiple times, to create entries required.
        // Each entry will have a different security hash
        // made up of draw_ref, fk_lottery_id and checker_ref
        foreach ($aCheckers as $iCheckerID=>$aCheckerInfo) {

            // Call internal function to create unique security hash
            $sSecurityHash = self::_createSecurityHash($iDraw, $iLotteryID, $iCheckerID);

            $data['security_hash'] = $sSecurityHash;
            $data['checker_id'] = $iCheckerID;

            // Insert entry
            DAL::Insert($tablename, $data);

            // Add the security hash for this checker to the checkers array
            $aCheckers[$iCheckerID]['security_hash'] = $sSecurityHash;
        }

        // Return the modified checker array
        return $aCheckers;
    }


    /**
     * Get a single row from the lottery checking table which matches the supplied
     * security hash, lottery id, checker id and draw
     * @param string $sSecurityHash
     * @param integer $iLotteryID
     * @param integer $iCheckerID
     * @param integer $iDraw
     * @return array
     */
    public function getLotteryRowFromCheckingTable($sSecurityHash, $iLotteryID, $iCheckerID, $iDraw)
    {

        $sSQL = "SELECT * FROM lottery_results_checking " .
                "WHERE fk_lottery_id = '$iLotteryID' " .
                "AND checker_id = '$iCheckerID' " .
                "AND draw_ref = '$iDraw' " .
                "AND security_hash = '$sSecurityHash'";

        return DAL::executeQuery($sSQL);
    }


    /**
     * Get rows from the checking table which match the supplied
     * draw number, lottery id and status/es (as array)
     *
     * @param integer $iDraw
     * @param integer $iLotteryID
     * @param array $aStatus
     * @return array
     */
    public function getLotteryRowsToBeCheckedFromCheckingTable($iDraw, $iLotteryID, $aStatus)
    {
        $sStatus = implode(",", $aStatus);

        $sSQL = "SELECT lrc.*, lc.* FROM lottery_results_checking lrc " .
                "LEFT JOIN lottery_checkers_list lc ON lrc.checker_id = lc.id " .
                "WHERE lrc.draw_ref = $iDraw " .
                "AND lrc.fk_lottery_id = $iLotteryID " .
                "AND lrc.status IN ($sStatus)";

        return DAL::executeQuery($sSQL);
    }


    /**
     * Get all previous lotteries which:
     * haven't yet been fully checked or have mismatches
     * @return array
     */
    public function getLotteriesWithOutstandingChecks()
    {
         $sSQL = "SELECT * FROM lottery_results_checking lrc
INNER JOIN lottery_checkers_list lcl ON lcl.id=lrc.checker_id
                 WHERE lrc.status IN (0,1,3)
                 ORDER BY lrc.fk_lottery_date DESC";

         return DAL::executeQuery($sSQL);
    }


    /**
     * Revised function to get all lotteries and their current check status
     * @return array
     */
    public function getLotteriesAndCheckStatusesForDisplay()
    {
         $sStartDate = date("Y-m-d H:i:s", strtotime("-8 days"));

         // Display the previous 8 days worth of checks, and their statuses
         $sSQL = "SELECT * FROM lottery_results_checking lrc
                  INNER JOIN lottery_checkers_list lcl ON lcl.id=lrc.checker_id
                  WHERE lrc.fk_lottery_date > '{$sStartDate}'
                  ORDER BY lrc.fk_lottery_date DESC";

         return DAL::executeQuery($sSQL);
    }


    /**
     * Check all checks for the specified lottery id and reference have been made
     * @param integer $iLotteryID
     * @param integer $iDraw
     * @return array
     */
    public function checkAllChecksMadeForLottery($iLotteryID, $iDraw)
    {
        $sSQL = "SELECT COUNT(*) as num_checks " .
                "FROM lottery_results_checking lrc " .
                "WHERE lrc.status IN (1,3) " .
                "AND lrc.draw_ref = $iDraw " .
                "AND lrc.fk_lottery_id = $iLotteryID";

         return DAL::executeGetOne($sSQL);
    }


    /**
     * Adds number and winnings data to an entry in the lottery_results_checking table
     * @param string $sSecurityHash
     * @param array $aNumbers
     * @param object $oWinnings
     * @return array
     */
     public function addResultsToCheckingTable($sSecurityHash, $aNumbers, $oWinnings, $iNextJackpotAmount, $bNextJackpotRollover, $bWinningsNull) {

        // Drawn numbers are stored as a concatenated string
        // with each number seperated by a pipe symbol,
        // so create that string here from the supplied numbers
        $sNumbers = implode("|", $aNumbers);

        // Winnings are stored as a serialised array,
        // so serialise the supplied array
        $sWinnings = serialize($oWinnings);


        if ($bWinningsNull == true)
        {
            $sWinnings = null;
        }

        // Create update code
        $tablename = "lottery_results_checking";
        $data = array(
            'numbers' => $sNumbers,
            'winnings' => $sWinnings,
            'next_jackpot_amount' => $iNextJackpotAmount,
            'next_jackpot_rollover' => $bNextJackpotRollover,
            'status' => 1
        );
        $params = "security_hash = '$sSecurityHash'";

        return DAL::Update($tablename, $data, $params);
    }

    /**
     * Create a security hash from the supplied data
     * (Appends 'LL' to the supplied variables, concatenates them
     * and returns the MD5 hash.)
     *
     * @param integer $iDraw
     * @param integer $iLotteryID
     * @param integer $iCheckerID
     * @return string
     */
    private function _createSecurityHash($iDraw, $iLotteryID, $iCheckerID) {
        return md5("LL" . $iDraw . $iLotteryID. $iCheckerID);
    }

    /**
     * Checks the supplied security hash against the supplied variables
     * @param string $sSecurityHash
     * @param integer $iDraw
     * @param integer $iLotteryID
     * @param integer $iCheckerID
     * @param boolean $debug
     * @return boolean
     */
    public function checkSecurityHash($sSecurityHash, $iDraw, $iLotteryID, $iCheckerID, $debug=false) {

        // If debug is requested, print out supplied variables
        if ($debug) {
            echo ("Draw:");
            var_dump($iDraw);
            echo ("<br/>Lottery:");
            var_dump($iLotteryID);
            echo ("<br/>Checker:");
            var_dump($iCheckerID);
            echo ("<br/>Security Hash:");
            var_dump($sSecurityHash);
            echo ("<br/><br/>Result from function:");
            var_dump(self::_createSecurityHash($iDraw, $iLotteryID, $iCheckerID));
        }

        // Pass the draw, lottery id and checker id to the internal create security hash function
        // and check the returned value matches with the supplied security hash
        return ($sSecurityHash === self::_createSecurityHash($iDraw, $iLotteryID, $iCheckerID));
    }


    /**
     * Checks the supplied array of drawn number strings for equality
     * @param array $aValues
     * @return boolean
     */
    public function checkNumbersEquality($aValues) {
        // Start a count
        $c = 1;

        // Loop through supplied array of values
        foreach ($aValues as $Value) {
          // Generate temporary variable variable
          $n = "val$c";

          // Assign value from array to temp variable
          $$n = $Value;

          // Incrememnt count
          $c++;
        }


        // Transitive relations
        // If a is equal to b, and b is equal to c,
        // it therefore follows that a is equal to c
        // so return an appropriate value
        //return (($val1 == $val2) && ($val2 == $val3)?true:false);

        // Only doing a 2-value comparison now
        return ($val1 == $val2?true:false);
    }


    /**
     * Same as checkNumbersEquality, but works on array of prizes/winners
     * @param array $aValues
     * @return boolean
     */
    public function checkPrizesEquality($aValues) {

        // Used to store checked variables
        $aChecked = array();

        // Loop through supplied array of values
        foreach ($aValues as $iChecker=>$aData)
        {
            // Push winners and prize info onto storage array
           $aChecked['winners'][$iChecker] = $aData['winners'];
           $aChecked['prize'][$iChecker] = $aData['prize'];
        }

        // Check equality of winners first
        $c = 1;
        foreach ($aChecked['winners'] as $Winner) {
          // Generate temporary variable variable
          $n = "val$c";

          // Assign value from array to temp variable
          $$n = $Winner;

          // Incrememnt count
          $c++;
        }

        // Transitive relations
        // If a is equal to b, and b is equal to c,
        // it therefore follows that a is equal to c

        // Check temporary winners values and return appropriate value
        //$bCheck1 = (($val1 == $val2) && ($val2 == $val3)? true:false);

        $bCheck1 = ($val1 == $val2 ? true:false);

        // Repeat the above process for each prize
        $c = 1;
        foreach ($aChecked['prize'] as $Prize) {
          $n = "val$c";
          $$n = $Prize;
          $c++;
        }

        //$bCheck2 = (($val1 == $val2) && ($val2 == $val3)? true:false);
        $bCheck2 = ($val1 == $val2 ? true:false);

        // Return whether both sets of checks have passed or not
        return ($bCheck1 && $bCheck2 ? true : false);
    }

    /**
     * Select up to 3 (depending on $iLimit) random 'checkers' who will be responsible
     * for entering the lottery results for the selected lottery
     * Can also pass in an array of checker ids to exclude
     *
     * @param integer $iLotteryID
     * @param integer $iLimit
     * @param array $aFilter
     * @return array
     */
    public function selectRandomCheckers($iLotteryID, $iLimit = 2, $aFilter=false)
    {
        // Store for returned data
        $aReturn = array();

        // Create SQL
        // @added 22/10/2014 - don't include the scraper for now
        $sSQL = "SELECT lcls.id, lcls.email_address, lcls.first_name " .
                "FROM lottery_checkers_list lcls " .
                "LEFT JOIN lottery_checkers_lotteries lclo ON lcls.id = lclo.fk_lottery_checker_id " .
                "WHERE lcls.active=1 AND lclo.fk_lottery_id = $iLotteryID";

        // If filter information has been supplied, add to query
        if ($aFilter)
        {
            $sFilter = implode(",", $aFilter);
            $sSQL .= " AND lcls.id NOT IN ($sFilter) ";
        }
        // Return a random selection of $iLimit results
        $sSQL .= "ORDER BY RAND() LIMIT $iLimit";

        echo '<!-- '.$sSQL.' -->';

        // Run query
        $aResults =  DAL::executeQuery($sSQL);

        // Loop through returned resultset
        foreach ($aResults as $aResult)
        {
            // Get checker id
            $iCheckerID = $aResult['id'];

            // Put other results on array keyed on checker id
            $aReturn[$iCheckerID]['email_address'] = $aResult['email_address'];
            $aReturn[$iCheckerID]['first_name'] = $aResult['first_name'];
        }

        // Return formatted array
        return $aReturn;
    }

    /**
     * Get the details of the checker from the supplied security hash
     *
     * @param string $sSecurityHash
     * @return array
     */
    public function getCheckerByHash($sSecurityHash)
    {
        $sSQL = "SELECT * FROM lottery_checkers_list lcl ".
                "RIGHT JOIN lottery_results_checking lrc ON lcl.id = lrc.checker_id " .
                "WHERE lrc.security_hash = '$sSecurityHash'";

        return DAL::executeGetRow($sSQL);
    }

    /**
     * Update the supplied checker's details as 'unable to check'
     * and return a new checker
     * @param string $sSecurityHash
     * @param integer $iDraw
     * @param integer $iLotteryID
     * @return array
     */
    public function updateCheckerAsUnable($sSecurityHash, $iDraw, $iLotteryID)
    {
        // Storage for new checker
        $arrCheckers = array();

        // COnstruct update statement and execute
        $tablename = "lottery_results_checking";
        $data = array('status' => 4);
        $params = "security_hash = '$sSecurityHash'";
        DAL::Update($tablename, $data, $params);

        // Now create SQL for selecting ALL checkers for this lottery and draw,
        // whether unable or not
        // It's this array which will be used to help select a new checker
        // (as obviously we don't want to reselect someone who's already selected
        $sSQL = "SELECT checker_id FROM lottery_results_checking " .
                "WHERE draw_ref = $iDraw AND fk_lottery_id = $iLotteryID";

        // Loop through returned result set
        foreach (DAL::executeQuery($sSQL) as $aRow)
        {
            // Push checker id onto return array
            $arrCheckers[] = $aRow['checker_id'];
        }

        // Return array
        return $arrCheckers;
    }

    /**
     * Update statuses of all checkers for a particular lottery/draw
     * @param integer $iLotteryID
     * @param integer $iDraw
     * @param integer $iStatus
     * @return array
     */
    public function updateAllCheckingStatusForLottery($iLotteryID, $iDraw, $iStatus)
    {
        // Create update statement
        // We only want to update results if they've been entered and not previously checked
        // OR if they've been previously checked and found to be mismatched
        $tablename = "lottery_results_checking";
        $data = array(
            'status' => $iStatus
        );
        $params = "fk_lottery_id = $iLotteryID AND draw_ref = $iDraw AND status IN (1,3)";

        return DAL::Update($tablename, $data, $params);
    }

    /**
     * Get the next draw date of the selected lottery
     * from the list of future draw dates
     * @param integer $iLotteryID
     * @return array
     */
    public function getNextLotteryDrawDate($iLotteryID)
    {
        $sSQL = "SELECT * FROM r_lottery_dates " .
                "WHERE drawdate > CURDATE() AND fk_lottery_id = $iLotteryID " .
                "LIMIT 1";

        return DAL::executeGetRow($sSQL);

    }

    /**
     * Returns draw info of the supplied lottery/draw in an array
     * containing the draw date as a DATETIME
     * @param integer $iDraw
     * @param integer $iLotteryID
     * @return array
     */
    public function getLotteryDrawDateFromDrawRef($iDraw, $iLotteryID, $bNext = false)
    {
        // Because the $iDraw is in format YYYYMMDD
        // we need to create a date range in DATETIME format
        // So firstly break the YYYMMDD into seperate parts
        $iYear = substr($iDraw, 0, 4);
        $iMonth = substr($iDraw, 4, 2);
        $iDate = substr($iDraw, 6);

        // Generate start and end times dfor this date
        $strDateStart = $iYear."-".$iMonth."-".$iDate." 00:00:00";
        $strDateEnd = $iYear."-".$iMonth."-".$iDate." 23:59:59";


        // Execute query
        $sSQL = "SELECT * FROM r_lottery_dates " .
                "WHERE fk_lottery_id = $iLotteryID ";

        if (!$bNext)
        {
            $sSQL .= "AND drawdate BETWEEN '$strDateStart' AND '$strDateEnd'";
        }
        else
        {
            $sSQL .= "AND drawdate > '$strDateEnd' LIMIT 1";
        }

        return DAL::executeGetRow($sSQL);
    }

    /**
     * Update the next jackkpot informatino for the forthcoming draw
     * @param date $dLotteryDate
     * @param integer $iLotteryID
     * @param integer $iCutoff
     * @param integer $iPrice
     * @param integer $iJackpot
     * @param boolean $bRollover
     * @return array
     */
    public function updateNextJackpotInformation($iDraw, $iLotteryID, $iCutoff, $iPrice, $iJackpot, $bRollover)
    {
        // Get the next lottery information from the database
        $aNextLotteryInformation = self::getLotteryDrawDateFromDrawRef($iDraw, $iLotteryID, true);

        //Find out which hedging model to assign
        $oTicketEngine=TicketBuyingController::selectEngineToUse($iLotteryID,$aNextLotteryInformation['drawdate'],$iJackpot);

        // Create update statement
        $tablename = "r_lottery_dates";
        $data = array(
            'price' => $iPrice,
            'cutoff' => $iCutoff,
            'jackpot' => $iJackpot,
            'rollover' => $bRollover,
            'fk_ticket_engine' => $oTicketEngine->id,
            'ticket_engine_assigned_at' => date('Y-m-d H:i:s')
        );


        $params = "fk_lottery_id = $iLotteryID " .
                  "AND drawdate = '{$aNextLotteryInformation['drawdate']}'";

        $bResult=DAL::Update($tablename, $data, $params);



        return $bResult;
    }

    /**
     * Get checker information from their ID
     * @param integer $iCheckerID
     * @param integer $iLotteryID
     * @param integer $iDraw
     * @return array
     */
    public function getCheckerInfoFromID($iCheckerID, $iLotteryID, $iDraw)
    {
        $sSQL = "SELECT * FROM lottery_checkers_list lcl " .
                "LEFT JOIN lottery_results_checking lrc on lcl.id=lrc.checker_id " .
                "WHERE lcl.id = $iCheckerID " .
                "AND lrc.draw_ref = $iDraw " .
                "AND lrc.fk_lottery_id = $iLotteryID";

        return DAL::executeGetRow($sSQL);
    }


    /**
     * Add an entry in the transactions table, but only set as pending
     *
     * @param integer $iMemberID
     * @param integer $iBookingItemID
     * @param integer $iLotteryID
     * @param float $fTransactionAmount
     * @param string $sTransactionReference
     * @return boolean
     */
    public function addWinTransactionPending($iMemberID, $fTransactionAmount, $sTransactionReference) {

        $oPaymentGateway = new WinningsGateway();
        $oPaymentGateway->assignAmount($fTransactionAmount);
        $iTransactionID = $oPaymentGateway->addTransaction($iMemberID, CurrencyAdmin::getBaseCurrencyID());

        if ($oPaymentGateway->ProcessPaymentPending($sTransactionReference)) {

            return $iTransactionID;
        }

        return false;
    }


    /**
     * Add an entry in the transactions table, and set as confirmed
     *
     * @param array $aTransaction
     * @return boolean
     */
    public function addWinTransactionConfirmed($aTransaction) {

        $oPaymentGateway = new WinningsGateway();

        if (LotteryResultsAdmin::updateWinnerTransaction($aTransaction['bookingitem_id'], $aTransaction['transaction_amount'], $aTransaction['transaction_id']))
        {
            // Need to pass transaction ID here as well
            if ($oPaymentGateway->ProcessPayment($aTransaction['transaction_ref'], $aTransaction['transaction_id'])) {
                Member::updateMemberBalance($aTransaction['member_id']);
                return true;
            }
        }
        return false;
    }


   /**
    * Get lottery draw ID from draw ref and lottery id
    * @param integer $iLotteryId lottery id
    * @param integer $iDrawRef draw 'reference'
    * @return array
    */
   public static function getLotteryDrawIdFromLotteryIdAndRef($iLotteryId, $iDrawRef)
   {
       $iLotteryId = (int) $iLotteryId;
       $iDrawRef = (int) $iDrawRef;

       $sSQL = "SELECT id FROM lottery_draws " .
                "WHERE fk_lottery_id = $iLotteryId " .
                "AND draw = $iDrawRef ";

       $aRow = DAL::executeGetRow($sSQL);

       return $aRow['id'];

   }

   /**
    * Check that we don't already have checkers in place for this draw
    * @param integer $iLotteryID Lottery ID
    * @param integer $dDrawDate Draw date
    * @param integer $iDraw draw reference
    *
    * @return boolean
    */
   public static function doCheckersAlreadyExistForThisDraw($iLotteryID, $dDrawDate, $iDraw)
   {
        $sSQL = "SELECT COUNT(*) as num_checkers " .
                "FROM lottery_results_checking " .
                "WHERE fk_lottery_id = '{$iLotteryID}' " .
                "AND draw_ref = '{$iDraw}' " .
                "AND fk_lottery_date = '{$dDrawDate}' " .
                "AND status IN (0,1,2,3)";

        $iNumCheckers = (int) DAL::executeGetOne($sSQL);

        // Do we have 3 checkers?
        if ($iNumCheckers == 3)
        {
            return true;
        }
        return false;
   }
}
