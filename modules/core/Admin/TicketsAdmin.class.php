<?php

/**
 * TicketsAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class TicketsAdmin {

    protected $iLangID = 1;

    public function __construct($iLangID = 1) {
        $this->iLangID = $iLangID;
    }

    public function getTicketsByDraw($iLotteryID, $iDraw) {
        $sSQL = "SELECT
					bi.numbers,
					bi.bookingitem_id,
					b.fk_member_id as member_id
				FROM booking_items bi
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id AND b.fk_status_id = 1
				INNER JOIN lottery_draws ld ON ld.fk_lottery_id = bi.fk_lottery_id AND ld.fk_lotterydate = bi.fk_lotterydate
				WHERE
					ld.draw = {$iDraw}
					AND
					ld.fk_lottery_id = {$iLotteryID}";
//echo($sSQL);
        return DAL::executeQuery($sSQL);
    }

    public function getBooking($iBookingID) {

    }

    public function getMemberBookings($iMemberID) {

    }

    public function getTicket($iBookingItemID) {
        $sSQL = "SELECT
					b.bookingreference,
					au.firstname as affiliate_firstname,
					au.lastname as affiliate_lastname,
					b.promo as promo_code,
					b.booking_date as booking_date,
					s.comment as status,
					lc.comment as lottery,
					ll.title as lottery_title,
					lc.lottery_id as lottery_id,
					bi.numbers as numbers,
					bi.cost as ticket_price,
					ld.fk_lotterydate as draw_date,
					bi.purchased as purchased,
					m.firstname,
					m.lastname,
					m.email,
					m.contact,
					m.city,
					cl.title as country,
					m.join_date,
					m.balance,
					m.member_id as member_id,
					lw.amount as winning_amount,
					lw.transaction_amount as transaction_amount,
					lw.type as win_title,
					ld.processed as draw_processed,
					ld.emailed as losers_emailed,
					lw.emailed as emailed,
					lw.paid as paid,
					ts.comment as winning_transaction,
					t.bookingreference as winning_reference,
					t.transaction_date as transaction_date,
					c.shortname as currency,
					c.currency_id as currency_id
				FROM booking_items bi
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id AND b.fk_status_id = 1
				LEFT JOIN admin_users au ON au.user_id = b.fk_affiliate_id
				INNER JOIN status s ON s.status_id = b.fk_status_id
				INNER JOIN members m ON m.member_id = b.fk_member_id
				INNER JOIN countries_lang cl ON cl.fk_country_id = m.fk_country_id AND cl.fk_language_id = 1
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = bi.fk_lottery_id
				INNER JOIN lotteries_lang ll ON lc.lottery_id = ll.fk_lottery_id AND ll.fk_language_id = 1
				INNER JOIN currencies c ON c.currency_id = lc.fk_currency_id
				INNER JOIN lottery_draws ld ON ld.fk_lottery_id = bi.fk_lottery_id AND ld.fk_lotterydate = bi.fk_lotterydate
				LEFT JOIN lottery_winnings lw ON lw.fk_bookingitem_id = bi.bookingitem_id
				LEFT JOIN transactions t ON t.transaction_id = lw.fk_transaction_id AND t.fk_member_id = m.member_id
				LEFT JOIN status ts ON ts.status_id = t.fk_status_id
				WHERE bi.bookingitem_id = {$iBookingItemID}";

        return DAL::executeGetRow($sSQL);
    }

}