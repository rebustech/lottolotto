<?php
/**
 * WebServices class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage PublicAPI
 */
class WebServices
{
	public function Authenticate($sUsername, $sPassword, $sPin, $bAffiliate = true)
	{
		$iAdminType = ($bAffiliate)?SecurityAdmin::AFFILIATE:SecurityAdmin::ADMIN;
		$bIsAuthenticated = false;
		if ( $sUsername && $sPassword && $sPin )
		{
			$sql = "SELECT 	au.user_id,
							au.password,
							au.pin,
							au.isactive,
							at.is_active as admin_active,
							at.encryptedlogin,
							CONCAT_WS(' ', au.firstname, au.lastname) AS fullname
					FROM admin_users au
					INNER JOIN admin_types at ON at.admintype_id = au.fk_admintype_id
					WHERE au.username='{$sUsername}'
					AND au.fk_admintype_id = '" . $iAdminType . "'";
			$aCredentials = DAL::executeGetRow($sql);
			if($aCredentials['isactive'] == 1 && $aCredentials['admin_active'] == 1 ){
				$sDBPassword = $aCredentials['password'];
				$sDBPin 	 = $aCredentials['pin'];

				$bPassword = Encryption::Validate($sPassword, $sDBPassword);
				$bPin = Encryption::Validate($sPin, $sDBPin);

				if($bPassword && $bPin){
					$bIsAuthenticated = true;
				}
			}
		}
		return $bIsAuthenticated;
	}

}
