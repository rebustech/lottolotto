<?php

/**
 * ScriptsAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class ScriptsAdmin {

    public static function sendEmail($sEmail, $sFirstName, $sLastName, $sSubject, $sBody) {
        if ($sEmail && $sFirstName && $sLastName && $sSubject && $sBody) {
            return EmailAdmin::sendMailshot($sEmail, $sFirstName, $sLastName, $sSubject, $sBody);
        }
        return false;
    }

    public static function sendGeneralEmail($sSubject, $sBody, $sMemberIDs) {
        set_time_limit(0);
        $aReturn = array();
        if ($sSubject && $sBody && $sMemberIDs) {
            $sSQL = "SELECT m.firstname,
							m.lastname,
							m.email
					FROM members m
					WHERE m.is_active = 1 AND m.email_generaloffers = 1";
            if ($sMemberIDs != "ALL") {
                $sSQL .= " AND m.member_id IN ( {$sMemberIDs} ) ";
            }
            $aMembers = DAL::executeQuery($sSQL);
            foreach ($aMembers as $aMember) {

                if (EmailAdmin::sendMailshot($aMember['email'], $aMember['firstname'], $aMember['lastname'], $sSubject, $sBody)) {
                    $aReturn[] = "Email to " . $aMember['firstname'] . " " . $aMember['lastname'] . " sucessful";
                } else {
                    $aReturn[] = "Email to " . $aMember['firstname'] . " " . $aMember['lastname'] . " failed";
                }
            }
        }
        return $aReturn;
    }

    public static function advertiseNextLottery($sMemberIDs = NULL) {
        $aReturn = array();
        set_time_limit(0);

        $sSQL = "SELECT m.firstname,
						m.lastname,
						m.email
				FROM members m
				WHERE m.is_active = 1 AND m.email_lotterydraws = 1";
        if ($sMemberIDs) {
            $sSQL .= " AND m.member_id IN ( {$sMemberIDs} ) ";
        }

        $aMembers = DAL::executeQuery($sSQL);
        if (!empty($aMembers)) {
            $aReturn = Email::advertiseNextLottery($aMembers);
        }

        return $aReturn;
    }

    public static function sendBookingsYesterdayEmail() {
        $aReport = new ReportsAdmin($oSecurityObject, "BookingsYesterday", NULL);
        $oEmail = new Mailer();
        $oEmail->shortHeader();
        $oEmail->setSubject(WEBSITETITLE . " - Bookings Yesterday");
        $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Reporting");
        $oEmail->addTo("reports@lovelotto.com", "LoveLotto.com Reporting");
        $oEmail->addTo("lincoln@lovelotto.com", "Lincoln Grixti");
        $oEmail->addTo("chris@lovelotto.com", "Chris Knights");
        $oEmail->addTo("johnfirth@live.com", "John Firth");
        $oEmail->sendEmail("<h2>Bookings " . date('l jS F Y', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))) . "</h2>" . $aReport->display("Email"));
    }

    public static function sendDaysLotteryUnpurchasedTickets() {
        $aReport = new ReportsAdmin($oSecurityObject, "UnpurchasedLotteryTickets", NULL);
        $oEmail = new Mailer();
        $oEmail->shortHeader();
        $oEmail->setSubject(WEBSITETITLE . " - Today's Unpurchased Lottery Tickets");
        $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Reporting");
        $oEmail->addTo("reports@lovelotto.com", "LoveLotto.com Reporting");
        $oEmail->addTo("lincoln@lovelotto.com", "Lincoln Grixti");
        $oEmail->addTo("chris@lovelotto.com", "Chris Knights");
        $oEmail->addTo("johnfirth@live.com", "John Firth");
        $oEmail->sendEmail("<h2>Unpurchased Tickets for " . date('l jS F Y') . " Lottery</h2>" . $aReport->display("Email"));
    }

    public static function checkFutureLotteryDates() {
        $sSQL = "SELECT
					COUNT(rld.drawdate) as futuredates,
					rld.drawdate as drawdate,
					lc.comment as lottery
				FROM lotteries_cmn lc
				LEFT JOIN r_lottery_dates rld ON rld.fk_lottery_id = lc.lottery_id
					AND rld.drawdate > '" . date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"))) . "' AND rld.jackpot > 0
				WHERE lc.is_active = 1
				GROUP BY lc.lottery_id ";
        $aLotteries = DAL::executeQuery($sSQL);

        foreach ($aLotteries as $aLottery) {
            if ($aLottery['futuredates'] == 0) {
                $sMailBody = $aLottery['lottery'] . " has " . $aLottery['futuredates'] . " future dates and jackpots configured. Please login on admin to rectify the problem.";
                $oEmail = new Mailer();
                $oEmail->setSubject(WEBSITETITLE . " - " . $aLottery['lottery'] . ": Missing Lottery Draw Dates/Jackpot");
                $oEmail->setFrom("webmaster@lovelotto.com", "LoveLotto.com Webmaster");
                $oEmail->addTo("webmaster@lovelotto.com");
                $oEmail->sendEmail($sMailBody);
            } else {
                echo $aLottery['lottery'] . " has " . $aLottery['futuredates'] . " future dates configured.<br/>";
            }
        }
    }

    public static function checkTime() {
        $sSQL = "SELECT UNIX_TIMESTAMP();";
        $dSQLTime = DAL::executeGetOne($sSQL);

        $dPHPTime = time();


        $dTimeDiff = abs($dSQLTime - $dPHPTime);

        if ($dTimeDiff > 3500) {
            $sMailBody = "Server and MySQL time is out of Sync.";
            $oEmail = new Mailer();
            $oEmail->setSubject(WEBSITETITLE . " - Server Time Out of Sync");
            $oEmail->setFrom("webmaster@lovelotto.com", "LoveLotto.com Webmaster");
            $oEmail->addTo("webmaster@lovelotto.com");
            $oEmail->sendEmail($sMailBody);
            echo $sMailBody;
        } else {
            echo "Time is within 60minutes difference";
        }
    }

    public static function updateCurrenciesRate() {
        $XMLContent = file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        //the file is updated daily between 2.15 p.m. and 3.00 p.m. CET

        foreach ($XMLContent as $line) {
            if (ereg("currency='([[:alpha:]]+)'", $line, $currencyCode)) {
                if (ereg("rate='([[:graph:]]+)'", $line, $rate)) {
                    //Output the value of 1 EUR for a currency code
                    $aCurrency = CurrencyAdmin::getCurrencyByShortName($currencyCode[1]);
                    if ($aCurrency['id']) {
                        CurrencyAdmin::editCurrencyXRate($rate[1], $aCurrency['id']);
                        echo 'Updated ' . $currencyCode[1] . ' to &euro;1 = ' . $rate[1] . ' ' . $currencyCode[1] . "\r\n";
                        if ($currencyCode[1] == "USD") {
                            $aCurrency = CurrencyAdmin::getCurrencyByShortName("AED");
                            if ($aCurrency['id']) {
                                CurrencyAdmin::editCurrencyXRate($rate[1] * 3.6725, $aCurrency['id']);
                                echo 'Updated AED to &euro;1 = ' . ($rate[1] * 3.6725) . " AED\r\n";
                            }
                            $aCurrency = CurrencyAdmin::getCurrencyByShortName("QAR");
                            if ($aCurrency['id']) {
                                CurrencyAdmin::editCurrencyXRate($rate[1] * 3.64, $aCurrency['id']);
                                echo 'Updated QAR to &euro;1 = ' . ($rate[1] * 3.64) . " QAR\r\n";
                            }
                            $aCurrency = CurrencyAdmin::getCurrencyByShortName("BHD");
                            if ($aCurrency['id']) {
                                CurrencyAdmin::editCurrencyXRate($rate[1] * 0.376, $aCurrency['id']);
                                echo 'Updated BHD to &euro;1 = ' . ($rate[1] * 0.376) . " BHD\r\n";
                            }
                        }
                    }
                }
            }
        }
    }

    public static function createRobotsTXT() {
        $sWebsitePath = WEBSITEPATH;
        $sWebsiteURL = WEBSITEURL;
        $sWebsiteTitle = WEBSITETITLE;

        $sFileName = $sWebsitePath . "robots.txt";
        echo "----------------------------------------<BR>";
        echo "Creating " . $sWebsiteURL . "robots.txt" . "<BR>";
        echo "----------------------------------------<p>";
        if (is_writable($sFileName)) {

            $handle = fopen($sFileName, 'w+');
            if ($handle == false) {
                echo "Cannot open file ($sFileName)<p>";
                exit;
            } else {
                $sRobotstxt = "User-agent: *
				Allow: /
				Allow: /system/media/
				Disallow: /administration/
				Disallow: /affiliates/";
                $sRobotstxt .= "
				Disallow: /*aid= #
				Sitemap: http://{$sWebsiteURL}sitemap.xml";
                fwrite($handle, $sRobotstxt);
            }
            echo "Success, wrote {$sFileName}<p>";
            fclose($handle);
        } else {
            echo "Unable to write to file: {$sFileName}<p>";
        }
    }

    public static function createSiteMaps() {
        ini_set("memory_limit", "80M");
        $sWebsitePath = WEBSITEPATH;
        $sWebsiteURL = WEBSITEURL;
        if (strstr($sWebsiteURL, ".com")) {
            $sWebsiteMobileURL = str_replace(array("www"), array("m"), $sWebsiteURL);
        } else {
            $sWebsiteMobileURL = "m." . $sWebsiteURL;
        }
        $BaseURL = "http://" . $sWebsiteURL;
        $sURLListFileName = $sWebsitePath . "urls.txt";
//		$sURLListMobileFileName = $sWebsitePath . "mobilesitemap.xml";
        echo "Creating URLs.txt for <strong>" . WEBSITETITLE . "</strong><BR>";
        if (is_writable($sURLListFileName)) {

            $handle = fopen($sURLListFileName, 'w+');
//			$mobilehandle = fopen($sURLListMobileFileName, 'w+');
            if ($handle == false) {
                echo "Cannot open file ($sURLListFileName)";
                exit;
            }
            /*
              fwrite($mobilehandle,  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
              xmlns:mobile=\"http://www.google.com/schemas/sitemap-mobile/1.0\">\n");
              $BaseMobileURL = "http://" . $sWebsiteMobileURL;
              echo $BaseMobileURL . "<BR>";
             */
            $aPages = PageAdmin::getPagesForSitemap();
            $aPages = array_merge($aPages, PageAdmin::getLotteryDrawsURLs());
            foreach ($aPages as $PageKey => $aPage) {
                $sPageName = $aPage['langcode'] . "/" . $aPage['folder'] . $aPage['url'];
                $priority = 1.0;
                $changefreq = "weekly";
                $tLastModified = date("Y-m-d", strtotime($aPage['lastmodified'])) . "T" . date("H:i:s", strtotime($aPage['lastmodified'])) . "+01:00";
                $GoogleSiteMapURL = $BaseURL . $sPageName . " changefreq=" . $changefreq . " priority=" . $priority . " lastmod=" . $tLastModified . "\n";
                //echo $BaseURL . $sPageName . " changefreq=" . $changefreq ." priority=" . $priority . " lastmod=" . $tLastModified ."<br>";
                fwrite($handle, $GoogleSiteMapURL);

                //MOBILE STUFF
                /* 		if($aPage['isactivemobile'] == 1){
                  fwrite($mobilehandle,  "<url>");
                  fwrite($mobilehandle,  "<loc>" . $GoogleSiteMapMobileURL = $BaseMobileURL . $sPageName . "</loc>\n");
                  fwrite($mobilehandle,  "<mobile:mobile/>");
                  fwrite($mobilehandle,  "<lastmod>" . $tLastModified . "</lastmod>");
                  fwrite($mobilehandle,  "</url>\n");
                  }
                 */
            }
            $aPages = NULL;

            echo "Success, wrote URLS to file {$sURLListFileName}<br/>";
            //	fwrite($mobilehandle,  "</urlset>\n");
            fclose($handle);
            //	fclose($mobilehandle);
        } else {
            echo "<em>Unable to write to file: {$sURLListFileName}</em><br/>";
        }
    }

    public static function rebuildPagesTree() {
        self::rebuild_tree(0, 0);
        return "Pages Tree Traversal Rebuilt";
    }

    public static function clearWebsiteCache() {
        $bFoundCache = false;
        $iSuccessCount = 0;
        $iFailCount = 0;
        $aCacheFiles = @glob(SMARTY_DIR . strtolower(WEBSITENAME) . "/cache/*");
        if (is_array($aCacheFiles)) {
            foreach ($aCacheFiles as $filename) {
                $bFoundCache = true;
                $do = @unlink($filename);
                if ($do == "1") {
                    $iSuccessCount++;
                    $bValid = true;
                } else {
                    $iFailCount++;
                    $bValid = false;
                }
            }

            if ($iSuccessCount)
                $sMessages .= "<li>Succesfully cleared cache - {$iSuccessCount} File/s</li>";
            if ($iFailCount)
                $sMessages .= "<li class=\"error\">Unable to clear cache - {$iFailCount} File/s</li>";
        }
        $aCacheFiles = @glob(SMARTY_DIR . strtolower(WEBSITENAME) . "/compile/*");
        if (is_array($aCacheFiles)) {
            foreach ($aCacheFiles as $filename) {
                $bFoundCache = true;
                $do = @unlink($filename);
                if ($do == "1") {
                    $iSuccessCount++;
                    $bValid = true;
                } else {
                    $iFailCount++;
                    $bValid = false;
                }
            }

            if ($iSuccessCount)
                $sMessages .= "<li>Succesfully cleared compile - {$iSuccessCount} File/s</li>";
            if ($iFailCount)
                $sMessages .= "<li class=\"error\">Unable to clear compile - {$iFailCount} File/s</li>";
        }
        if (!$bFoundCache) {
            $sMessages .= "<li class=\"error\">No cache found for " . WEBSITETITLE . "</li>";
        }

        if ($sMessages) {
            echo "<ul>{$sMessages}</ul>";
        }
    }

    public static function updateTwitter($sNewStatus) {
        $oTwitter = new Twitter('eCe7m6jRrIwP3ZwUixxg', 'ofW0cBNJbrSvx25bKNp80Kzsf83XELKsJvCjNl5D3AA', '199286783-gK68JAtgo17x3iHKK97tK0QCs0NFEGiYuN1Xtu50', 'Rn3y7ZrKYzoxqwOE1UbhaIoXkK36pXgbnt2iVSpc4');
        $oReply = $oTwitter->updateStatus($sNewStatus);
        if ($oReply->error) {
            return $oReply->error . "{messagetype}achtungFail{/messagetype}";
        } else {
            return "Tweet Succesful";
        }
    }

    //
    // Server Arguments Retrieval Function
    //

	public static function getServerArgs() {
        $argv = $_SERVER['argv'];
        $_ARG = array();
        foreach ($argv as $arg) {
            if (ereg('--[a-zA-Z0-9]*=.*', $arg)) {
                $str = split("=", $arg);
                $arg = '';
                $key = ereg_replace("--", '', $str[0]);
                for ($i = 1; $i < count($str); $i++) {
                    $arg .= $str[$i];
                }
                $_ARG[$key] = $arg;
            } elseif (ereg('-[a-zA-Z0-9]', $arg)) {
                $arg = ereg_replace("-", '', $arg);
                $_ARG[$arg] = 'true';
            }
        }
        return $_ARG;
    }

    //
    // Private Functions
    //
    private static function rebuild_tree($iParentID, $iLeftNode) {
        $iRightNode = $iLeftNode + 1;
        $sSQL = "SELECT pc.page_id FROM pages_cmn pc INNER JOIN sections_cmn sc ON sc.section_id = pc.fk_section_id WHERE pc.parent_id = " . $iParentID . " ORDER BY sc.rank ASC, pc.rank ASC";
        $aPages = DAL::executeQuery($sSQL);
        foreach ($aPages as $aPage) {
            $iRightNode = self::rebuild_tree($aPage['page_id'], $iRightNode);
        }
        $sSQL = "UPDATE pages_cmn
                SET left_node = " . $iLeftNode . ",
                right_node = " . $iRightNode . "
                WHERE page_id = " . $iParentID;
        DAL::executeQuery($sSQL);
        return $iRightNode + 1;
    }

    /* make a URL small */

    public function shortURL($sURL, $login = 'lovelotto', $appkey = 'R_ba76e7e0c15dc0a9ceb76f307147fa08', $format = 'json', $version = '2.0.1') {
        //create the URL
        $bitly = 'http://api.bit.ly/shorten?version=' . $version . '&longUrl=' . urlencode($sURL) . '&login=' . $login . '&apiKey=' . $appkey . '&format=' . $format;

        //get the url
        //could also use cURL here
        $response = file_get_contents($bitly);

        //parse depending on desired format
        if (strtolower($format) == 'json') {
            $json = @json_decode($response, true);
            return $json['results'][$sURL]['shortUrl'];
        } else { //xml
            $xml = simplexml_load_string($response);
            return 'http://bit.ly/' . $xml->results->nodeKeyVal->hash;
        }
    }

    static public function insertLotteryDates($iLotteryID, $sFrom, $sTo) {

        $dDrawDate = strtotime($sFrom);
        $dTo = strtotime($sTo);
        $aLottoDays = array("Saturday", "Wednesday");
        $sLottoTime = "";
        $sLottoCutOff = "";
        $sLottoPrice = "";

        $sClassName = Lottery::getLotteryClassName($iLotteryID, false);

        if ($sClassName) {
            $oLottery = new $sClassName(1);
            $aLottoDays = $oLottery->getLottoDays();

            while ($dDrawDate <= $dTo):

                if (in_array(date("l", $dDrawDate), $aLottoDays)) {
                    $oLottery->insertLotteryDate($dDrawDate);
                }

                $dDrawDate = strtotime(date("l d M Y", $dDrawDate) . " +1 days");

            endwhile;
        }
    }

    public static function createRSSFiles() {
        $sWebsitePath = WEBSITEPATH;
        $sWebsiteURL = WEBSITEURL;
        $sWebsiteTitle = WEBSITETITLE;

        $sFileName = $sWebsitePath . "elgordo.xml";
        if (is_writable($sFileName)) {
            $handle = fopen($sFileName, 'w+');
            if ($handle == false) {
                echo "Cannot open file ($sFileName)<p>";
                exit;
            } else {
                $sText = "";
                fwrite($handle, $sText);
            }
            echo "Success, wrote {$sFileName}<p>";
            fclose($handle);
        } else {
            echo "Unable to write to file: {$sFileName}<p>";
        }
    }

}