<?php

/**
 * GenericTableAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class GenericTableAdmin {

    public static function createDataTable($aTitleFields, $aValueFields, $aData) {
        $sDataTable = '';

        if ($aData && !empty($aData)) {
            $sDataTable .= '<table class="datatable"><tr>';
            foreach ($aTitleFields as $currentField) {
                $sTitle = $currentField;
                $sClass = '';
                $aCurrentField = explode('|', $currentField);
                foreach ($aCurrentField as $key => $sParam) {
                    if ($key == 0) {
                        $sTitle = $sParam;
                    } else {
                        if ($sClass != '') {
                            $sClass .= ' ';
                        }
                        $sClass .= $sParam;
                        if ($sParam == "icon") {
                            $sTitle = "";
                        }
                    }
                }
                $sClass = ' class="' . $sClass . '"';
                $sDataTable .= '<th' . $sClass . '>' . $sTitle . '</th>';
            }
            $sDataTable .= '</tr>';
            foreach ($aData as $currentRow => $currentItem) {
                if ($currentRow !== 'pp') {
                    if ($currentRow % 2 == 1) {
                        $sDataTable .= '<tr class="alternate">';
                    } else {
                        $sDataTable .= '<tr>';
                    }
                    foreach ($aValueFields as $currentField) {
                        $sValue = $currentItem[$currentField];
                        $sClass = '';
                        $sTitleAttribute = '';
                        $aCurrentField = explode('|', $currentField);
                        foreach ($aCurrentField as $key => $sParam) {
                            if ($key == 0) {
                                $sValue = $currentItem[$sParam];
                                if (strlen($sValue) > 70) {
                                    $sValue = substr(htmlentities($sValue), 0, 30) . '...';
                                }
                                $sTitleAttribute = $sParam;
                            } elseif (strstr($sParam, 'dateformat:')) {
                                $sDateFormat = str_replace('dateformat:', '', $sParam);
                                $sValue = date($sDateFormat, strtotime($sValue));
                            } elseif (strstr($sParam, 'image_link:')) {
                                $sTitleAttribute = '';
                                $sCurrentImageLink = str_replace('image_link:', '', $sParam);
                                $iLinkStartPos = strpos($sCurrentImageLink, '{%link%}');
                                if ($iLinkStartPos) {
                                    $sCurrentImage = substr($sCurrentImageLink, 0, $iLinkStartPos);
                                    $sCurrentLink = substr($sCurrentImageLink, $iLinkStartPos + 8, strlen($sCurrentImageLink) - ($iLinkStartPos + 8));
                                    $sValue = '<a href="' . $sCurrentLink . '&id=' . $sValue . '"><img width="12px" src="' . $sCurrentImage . '" /></a>';
                                } else {
                                    $iScriptStartPos = strpos($sCurrentImageLink, "{%script%}");
                                    $sCurrentImage = substr($sCurrentImageLink, 0, $iScriptStartPos);
                                    $sCurrentScript = substr($sCurrentImageLink, $iScriptStartPos + 10, strlen($sCurrentImageLink) - ($iScriptStartPos + 10));
                                    $sCurrentScript = str_replace("{%id%}", $sValue, $sCurrentScript);
                                    $sValue = '<a href="#" &id=' . $sValue . '"  onclick="' . $sCurrentScript . '"><img width="12px" src="' . $sCurrentImage . '" /></a>';
                                }
                            } elseif (strstr($sParam, 'link:')) {
                                $sTitleAttribute = '';
                                $sCurrentLink = str_replace('link:', '', $sParam);
                                if (strstr($sCurrentLink, '{%valuefield%}')) {
                                    $iValueStartPos = strpos($sCurrentLink, '{%valuefield%}');
                                    $sHref = substr($sCurrentLink, 0, $iValueStartPos);
                                    if ($aData["pp"] && $sHref) {
                                        if (strstr($sHref, '?'))
                                            $sHref .= "&pp={$aData['pp']}";
                                        else
                                            $sHref .= "?pp={$aData['pp']}";
                                    }
                                    $sValueField = substr($sCurrentLink, $iValueStartPos + 14, strlen($sCurrentLink) - ($iValueStartPos + 14));
                                    if ($currentItem[$sValueField]) {
                                        $sValue = '<a href="' . $sHref . '&id=' . $sValue . '">' . $currentItem[$sValueField] . '</a>';
                                    }
                                } else if (strstr($sCurrentLink, "edit")) {
                                    if ($aData["pp"] && $sCurrentLink) {
                                        if (strstr($sCurrentLink, "?"))
                                            $sCurrentLink .= "&pp={$aData['pp']}";
                                        else
                                            $sCurrentLink .= "?pp={$aData['pp']}";
                                    }
                                    $sValue = '<a href="' . $sCurrentLink . '&id=' . $sValue . '">Edit</a>';
                                }
                                elseif (strstr($sCurrentLink, "delete")) {
                                    if ($aData["pp"] && $sCurrentLink) {
                                        if (strstr($sCurrentLink, "?"))
                                            $sCurrentLink .= "&pp={$aData['pp']}";
                                        else
                                            $sCurrentLink .= "?pp={$aData['pp']}";
                                    }
                                    $sValue = '<a href="' . $sCurrentLink . '&id=' . $sValue . '" onclick="return confirm(\'Are you sure you want to delete?\');"><img src="images/delete.gif" /></a>';
                                }
                            }
                            elseif (strstr($sParam, 'imagepath:')) {
                                $sTitleAttribute = "Image";
                                $sCurrentPath = str_replace('imagepath:', '', $sParam);
                                if ($sValue)
                                    $sValue = '<img src="' . $sCurrentPath . $sValue . '" />';
                            }
                            elseif (strstr($sParam, 'empty:')) {
                                $sEmptyText = str_replace('empty:', '', $sParam);
                                if (!$sValue) {
                                    $sValue = $sEmptyText;
                                }
                            } elseif (strstr($sParam, 'ajaxbool:')) {
                                $iCurrentId = $sValue;
                                $sToggleField = str_replace('ajaxbool:', '', $sParam);
                                $sTitleAttribute = $sToggleField;
                                $bToggleValue = $currentItem[$sToggleField];
                                if (!$bToggleValue) {
                                    $sImage = "disabled.gif";
                                    $sAction = "enable";
                                } else {
                                    $sImage = "enabled.gif";
                                    $sAction = "disable";
                                }
                                $sValue = "<a href=\"#\" action=\"{$sAction}\" onclick=\"toggleBooleanField($(this), '{$aCurrentField[0]}', '{$iCurrentId}', '{$sToggleField}', $(this).attr('action'), '{$aData['pp']}'); return false;\"><img src=\"images/{$sImage}\" /></a>";
                            } elseif ($sParam == "bool") {
                                if ($sValue != "0") {
                                    $sValue = "True";
                                } else {
                                    $sValue = "False";
                                }
                            } else {
                                if ($sClass != '') {
                                    $sClass .= ' ';
                                }
                                $sClass .= $sParam;
                            }
                        }
                        $sValue = str_replace("{%field:id%}", $currentItem["id"], $sValue);
                        $sClass = ' class="' . $sClass . '"';
                        $sDataTable .= '<td title="' . $sTitleAttribute . '" ' . $sClass . '>' . $sValue . '</td>';
                    }
                    $sDataTable .= '</tr>';
                }
            }
            $sDataTable .= "</table>";
        }
        return $sDataTable;
    }

    public static function createGenericDataTable($sTablename, $iLangId = 0, $aFilters = NULL, $sPreviousPage = '', $iLimit = 10, $sDetailsPage = 'generic-details.php', $sActionsPage = "generic_actions.php", $aFilterFields = array()) {

        if (is_array($aFilters)) {
            $sFilters = self::createFilterStringForSql($aFilters, $sTablename);
        } else if ($aFilters) {
            $sFilters = $aFilters;
        }
        if ($iLangId) {
            $aData = self::getLanguageRecords($sTablename, $iLangId, $sFilters, '', $iLimit);
        } else {
            $sOrderBy = ($sTablename == 'ratings') ? 'approved ASC' : '';
            if ($sTablename == 'ratings') {
                $aFilterFields = array('rating', 'comment', 'approved', 'positive_comments', 'negative_comments', 'isactive');
            }
            $aData = self::getRecords($sTablename, $sFilters, $sOrderBy, $iLimit);
        }
        $aColumns = self::getTableColumns($sTablename);
        if ($iLangId) {
            array_push($aColumns, array("Field" => "comment", "Type" => "varchar"));
        }
        $aPrimaryKeys = self::getPrimaryKeys($sTablename);
        $sPrimaryKeys = '';
        foreach ($aPrimaryKeys as $aCurrentKey) {
            if ($sPrimaryKeys != '') {
                $sPrimaryKeys .= '|';
            }
            $sPrimaryKeys .= $aCurrentKey['Column_name'];
        }
        $sDataTable = '';
        if ($aData && !empty($aData)) {
            $sDataTable .= '<table class="datatable"><tr>';
            $sDataTable .= '<th class="tiny centeralign">Edit</th>';
            $iColumnCount = 0;
            foreach ($aColumns as $currentField) {
                if ($currentField['Field'] != 'password' && $currentField['Field'] != 'pin' && ( empty($aFilterFields) || array_search($currentField["Field"], $aFilterFields) !== FALSE )) {
                    $iColumnCount++;
                    if ($iColumnCount >= AdminPage::COLUMN_LIMIT && $currentField['Field'] != 'isactive') {

                    } else {
                        $sTitle = $currentField['Field'];
                        $sFieldCaption = lang::get($sTablename . '-' . $sTitle);
                        $sClass = '';
                        if (strstr($currentField['Type'], 'tinyint')) {
                            $sClass .= 'tiny centeralign';
                        } elseif ($currentField['Key'] == 'PRI') {
                            $sClass .= 'tiny centeralign';
                        } elseif ($currentField['Type'] == 'varchar(10)') {
                            $sClass .= 'tiny centeralign';
                        }

                        $sClass = ' class="' . $sClass . '"';
                        $sDataTable .= '<th' . $sClass . '>' . $sFieldCaption . '</th>';
                    }
                }
            }
            $sDataTable .= '<th class="tiny centeralign">Delete</th>';

            $sDataTable .= '</tr>';
            foreach ($aData as $currentRow => $currentItem) {
                $sCurrentId = '';
                foreach ($aPrimaryKeys as $currentKey) {
                    if ($sCurrentId != '') {
                        $sCurrentId .= '_';
                    }
                    $sCurrentId .= $currentItem[$currentKey['Column_name']];
                }
                if ($currentRow % 2 == 1) {
                    $sDataTable .= '<tr class="alternate">';
                } else {
                    $sDataTable .= '<tr>';
                }
                $iColumnCount = 0;
                if (!$sPreviousPage) {
                    $sDataTable .= '<td class="tiny centeralign"><a href="' . $sDetailsPage . '?tablename=' . $sTablename . '&id=' . $sCurrentId . '">Edit</a></td>';
                } else {
                    $sDataTable .= '<td class="tiny centeralign"><a href="' . $sDetailsPage . '?tablename=' . $sTablename . '&id=' . $sCurrentId . '&pp=' . $sPreviousPage . '">Edit</a></td>';
                }
                foreach ($aColumns as $currentField) {
                    if ($currentField['Field'] != 'password' && $currentField['Field'] != 'pin' && ( empty($aFilterFields) || array_search($currentField["Field"], $aFilterFields) !== FALSE )) {
                        $iColumnCount++;
                        if ($iColumnCount >= AdminPage::COLUMN_LIMIT && $currentField['Field'] != 'isactive') {

                        } else {
                            $sValue = $currentItem[$currentField['Field']];
                            if (strlen($sValue) > 70) {
                                $sValue = substr(htmlentities($sValue), 0, 70) . '...';
                            }
                            $sClass = '';
                            $bAjaxBool = false;
                            if (strstr($currentField['Type'], 'tinyint')) {
                                $sClass .= 'centeralign';
                                $bAjaxBool = true;
                            } elseif ($currentField['Key'] == 'PRI') {
                                $sClass .= "centeralign";
                            } elseif ($currentField['Type'] == 'varchar(10)') {
                                $sClass .= 'centeralign';
                            }
                            $sTitleAttribute = $currentField["Field"];
                            $sClass = ' class="' . $sClass . '"';
                            if (!$bAjaxBool) {
                                $sDataTable .= '<td title="' . $sTitleAttribute . '" ' . $sClass . '>' . $sValue . '</td>';
                            } else {
                                $bToggleValue = $currentItem[$currentField["Field"]];
                                if (!$bToggleValue) {
                                    $sImage = 'disabled.gif';
                                    $sAction = 'enable';
                                } else {
                                    $sImage = 'enabled.gif';
                                    $sAction = 'disable';
                                }
                                $sDataTable .= '<td title="' . $sTitleAttribute . '"' . $sClass . '"><a href="#" action="' . $sAction . '" onclick="toggleBooleanField($(this), \'' . $sPrimaryKeys . '\', \'' . $sCurrentId . '\', \'' . $currentField['Field'] . '\', $(this).attr(\'action\'), \'' . $sPreviousPage . '\'); return false;"><img src="images/' . $sImage . '" /></a></td>';
                            }
                        }
                    }
                }

                if (!$sPreviousPage) {
                    $sDataTable .= '<td class="tiny centeralign"><a href="actions/' . $sActionsPage . '?a=delete&tablename=' . $sTablename . '&id=' . $sCurrentId . '" onclick="return confirm(\'Are you sure you want to delete this record?\');">Delete</a></td>';
                } else {
                    $sDataTable .= '<td class="tiny centeralign"><a href="actions/' . $sActionsPage . '?a=delete&tablename=' . $sTablename . '&id=' . $sCurrentId . '&pp=' . $sPreviousPage . '" onclick="return confirm(\'Are you sure you want to delete this record?\');">Delete</a></td>';
                }


                $sDataTable .= '</tr>';
            }
            $sDataTable .= "</table>";
        } else {
            $sDataTable = "There is nothing to display.";
        }
        return $sDataTable;
    }

    public static function checkTableExists($sTablename) {

        $sql = "SHOW TABLES from " . DATABASENAME . " LIKE '{$sTablename}'";
        $result = DAL::executeQuery($sql);
        if (sizeof($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function getRelationTables() {
        $sql = "SHOW TABLES from " . DATABASENAME . " LIKE 'r\_%'";
        return DAL::executeQuery($sql);
    }

    public static function checkColumnExists($sColumn, $sTablename) {

        $sql = "SHOW COLUMNS from {$sTablename} WHERE Field LIKE '{$sColumn}'";
        $result = DAL::executeQuery($sql);
        if (sizeof($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkLocationRelation($sTablename) {
        return false;
    }

    public static function getTableColumns($sTablename) {

        $sql = "DESCRIBE {$sTablename}";
        return DAL::executeQuery($sql);
    }

    /**
     * Gets all available information about a table structure and returns
     * as an object containing
     * ->fields = array of fields
     * ->fields->name = name
     * ->fields->type = type
     * ->fields->comment = comment
     * ->fields->required = required (i.e. is null allowed?)
     * ->fields->default = default
     * ->constraints = array of constraints
     * ->constraints->references_table
     * ->constraints->references_field
     * @param type $sTableName
     */
    public static function getTableDetails($sTableName) {
        $sql = 'SHOW CREATE TABLE `' . $sTableName . '`';
        $result = DAL::executeQuery($sql);
        $return = $result[0]['Create Table'];

        //Get the fields
        $aReturnSplit = explode("\n", $return);
        //Ignore first item as it will be the create statement
        foreach ($aReturnSplit as $sLine) {
            $aLine = explode(' ', trim($sLine));
            $oItem = new stdClass();
            switch ($aLine[0]) {
                case 'CREATE':
                    break;
                case 'KEY':
                    break;
                case 'PRIMARY':
                    $sField = str_replace('(`', '', str_replace('`)', '', $aLine[2]));
                    $aFields[$sField]->bPrimaryKey = true;
                    break;
                case 'CONSTRAINT':
                    $oConstraint = new stdClass();
                    $sField = str_replace('(`', '', str_replace('`)', '', $aLine[4]));
                    $oConstraint->sTable = str_replace('`', '', $aLine[6]);
                    $oConstraint->sTableField = str_replace('(`', '', str_replace('`)', '', $aLine[7]));
                    $out->aFields[$sField]->oConstraint = $oConstraint;
                    break;
                case ')':
                    //All done - break out of the loop (although this should be the last time round anyway)

                    break;
                default:
                    //This will be a field
                    $oItem->sName = str_replace('`', '', $aLine[0]);
                    $aType = explode('(', $aLine[1]);
                    $oItem->sType = $aType[0];
                    $oItem->iLength = intval($aType[1]);
                    $oItem->bRequired = true;
                    $oItem->bPrimaryKey = false;
                    //Rest of the field will depend on what type
                    foreach ($aLine as $iLineItemNum => $sLineItem) {
                        switch ($sLineItem) {
                            case 'DEFAULT':
                                $oItem->sDefault = $aLine[$iLineItemNum + 1];
                                break;
                            case 'COMMENT':
                                $oItem->sComment = implode(' ', array_slice($aLine, $iLineItemNum + 1));
                                break;
                            case 'NOT':
                                if ($aLine[$iLineItemNum + 1] == 'NULL')
                                    $oItem->bRequired = false;
                                break;
                        }
                    }
                    $out->aFields[$oItem->sName] = $oItem;
            }
        }
        return $out;
    }

    public static function getRecordCount($sTablename) {
        $sql = "select count(*) from {$sTablename}";
        return DAL::executeGetOne($sql);
    }

    public static function getPrimaryKeys($sTablename) {
        if ($sTablename == "cities_cmn_unassigned") {
            $sTablename = "cities_cmn";
        }

        $sql = 'SHOW INDEX FROM ' . $sTablename;

        $aKeys = DAL::executeQuery($sql);
        $aOutKeys = array();

        foreach ($aKeys as $aCurrentKey) {
            if ($aCurrentKey['Key_name'] == 'PRIMARY')
                $aOutKeys[] = $aCurrentKey;
        }

        return $aOutKeys;
    }

    public static function getFieldComment($sFieldname, $sTablename) {
        if ($sTablename == "cities_cmn_unassigned") {
            $sTablename = "cities_cmn";
        }
        $sql = "SELECT COLUMN_COMMENT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='{$sTablename}' AND COLUMN_NAME = '{$sFieldname}' AND TABLE_SCHEMA = '" . DATABASENAME . "'";
        return DAL::executeGetOne($sql);
    }

    public static function getRecords($sTablename, $sFilterString = "", $sOrderBy = "", $iLimit = 10) {

        if (!$iLimit) {
            $iLimit = 50;
        }

        $sql = "SELECT ct.* from {$sTablename} ct";

        if ($sFilterString) {
            $sql .= " where {$sFilterString}";
        }
        if ($sOrderBy) {
            $sql .=" ORDER BY {$sOrderBy}";
        }
        if ($iLimit) {
            $sql .=" LIMIT {$iLimit}";
        }
        return DAL::executeQuery($sql);
    }

    public static function getLanguageTable($sTablename) {
        if ($sTablename == "cities_cmn_unassigned") {
            $sTablename = "cities_cmn";
        }
        $sBaseTable = $sTablename;
        if (strstr($sTablename, "_cmn")) {
            $sBaseTable = str_replace("_cmn", "", $sTablename);
        }
        $sLanguageTable = $sBaseTable . "_lang";
        return $sLanguageTable;
    }

    public static function getLanguageRecords($sCommonTable, $sLanguageTable, $iLangId, $aFilters = NULL, $sOrderBy = "", $iLimit = 10) {
        $sTitleField = "lt.title as title ";
        if (!$iLimit) {
            $iLimit = 50;
        }
        if (is_array($aFilters)) {
            $sFilterString = self::createFilterStringForSql($aFilters, $sCommonTable);
        } else if ($aFilters) {
            $sFilterString = $aFilters;
        }
        $aPrimaryKeys = self::getPrimaryKeys($sCommonTable);
        $sOnClause = '';
        $sSelectFields = '';
        foreach ($aPrimaryKeys as $sCurrentKey) {
            $sCurrentKey = $sCurrentKey['column_name'];
            if ($sOnClause != '') {
                $sOnClause .= ' AND';
            }
            if ($sSelectFields != '') {
                $sSelectFields .= ' ,  \'_\' , ';
            }
            $sSelectFields .= ' ct.' . $sCurrentKey;

            if (strstr($sCurrentKey, 'fk_') && $sCurrentKey != 'fk_website_id') {
                $sCurrentKey = substr($sCurrentKey, 3);
                $sOnClause .= ' ct.' . $sCurrentKey . ' = lt.fk_' . $sCurrentKey;
            } else if ($sCurrentKey != "fk_website_id") {
                $sOnClause .= ' ct.' . $sCurrentKey . ' = lt.fk_' . $sCurrentKey;
            } else {
                $sOnClause .= ' ct.' . $sCurrentKey . ' = lt.' . $sCurrentKey;
            }
        }

        if (sizeof($aPrimaryKeys) > 1) {
            $sSelectFields = 'CONCAT(' . $sSelectFields . ')';
        }


        if (self::checkColumnExists('is_active', $sLanguageTable)) {
            $isActiveField = ', lt.is_active as is_active';
        } else {
            $isActiveField = '';
        }
        if ($sLanguageTable == 'faq_lang') {
            $sTitleField = 'lt.question as title';
        }
        if (self::CheckTableExists($sCommonTable) && self::CheckTableExists($sLanguageTable)) {
            $sql = "SELECT {$sSelectFields} as id, ct.comment as comment, {$sTitleField} {$isActiveField} from {$sCommonTable} ct LEFT OUTER JOIN {$sLanguageTable} lt on {$sOnClause} AND lt.fk_language_id = {$iLangId}";

            if (strstr($sLanguageTable, "products")) {
                $sql .= " LEFT OUTER JOIN cities_cmn cc ON ct.fk_city_id = cc.city_id LEFT OUTER JOIN regions_cmn rc ON cc.fk_region_id = rc.region_id LEFT OUTER JOIN countries_cmn countries ON rc.fk_country_id = countries.country_id
			INNER JOIN websites w ON ( w.country_id = rc.fk_country_id AND w.region_id = rc.region_id AND w.city_id = cc.city_id ) OR ( w.country_id = rc.fk_country_id AND w.region_id = rc.region_id AND w.city_id = 0 ) OR ( w.country_id = rc.fk_country_id AND w.region_id = 0 AND w.city_id = 0 )";
            } elseif (strstr($sFilterString, "fk_country_id") || strstr($sFilterString, "fk_region_id")) {
                $sLocationJoinTable = self::checkLocationRelation($sCommonTable);
                if ($sLocationJoinTable == "regions_cmn") {
                    $sql .= " LEFT OUTER JOIN regions_cmn rc ON ct.fk_region_id = rc.region_id LEFT OUTER JOIN countries_cmn cc ON rc.fk_country_id = cc.country_id";
                } else if ($sLocationJoinTable == "countries_cmn") {
                    $sql .= " LEFT OUTER JOIN countries_cmn cc ON ct.fk_country_id = cc.country_id";
                } else if ($sLocationJoinTable == "cities_cmn") {
                    $sql .= " LEFT OUTER JOIN cities_cmn ctc ON ct.fk_city_id = ctc.city_id LEFT OUTER JOIN regions_cmn rc ON ctc.fk_region_id = rc.region_id LEFT OUTER JOIN countries_cmn cc ON rc.fk_country_id = cc.country_id";
                }
            }

            if ($sFilterString) {
                $sql .= " where {$sFilterString}";
            }
            if ($sOrderBy) {
                $sql .=" ORDER BY {$sOrderBy}";
            }
            if ($iLimit) {
                $sql .=" LIMIT {$iLimit}";
            }
            return DAL::executeQuery($sql);
        } else {
            return false;
        }
    }

    public static function getRecordById($sTablename, $aFieldnames, $sIds, $sFilterString = "") {
        if ($sTablename == "cities_cmn_unassigned") {
            $sTablename = "cities_cmn";
        }
        $aIds = explode("_", $sIds);
        $sWhereClause = '';
        foreach ($aFieldnames as $key => $value) {
            $value = $value['Column_name'];
            if ($sWhereClause != '') {
                $sWhereClause .= ' AND';
            }
            $sWhereClause .= " {$value} = ";
            if (is_int($aIds[$key])) {
                $sWhereClause .= $aIds[$key];
            } else {
                $sWhereClause .= "'" . $aIds[$key] . "'";
            }
        }
        $sql = "SELECT * from {$sTablename} WHERE {$sWhereClause}";
        if ($sFilterString) {
            $sql .= " AND {$sFilterString}";
        }
        return DAL::executeGetRow($sql);
    }

    public static function getLanguageRecordById($sIds, $aCommonPks, $sCommonTable, $sLanguageTable, $iLangId) {
        if ($sCommonTable == "cities_cmn_unassigned") {
            $sCommonTable = "cities_cmn";
        }
        $aIds = explode("_", $sIds);
        $sWhereClause = '';
        $sSelectFields = '';
        $sJoinFields = '';

        foreach ($aCommonPks as $key => $value) {
            $value = $value["column_name"];
            if ($sWhereClause != '') {
                $sWhereClause .= ' AND';
                $sSelectFields .= ' + "_" + ';
                $sJoinFields .= ' AND';
            }
            $sWhereClause .= " ct.{$value} = ";
            if (is_int($aIds[$key])) {
                $sWhereClause .= $aIds[$key];
            } else {
                $sWhereClause .= "'" . $aIds[$key] . "'";
            }
            $sSelectFields .= ' ct.' . $value;
            if (strstr($value, "fk_") && $value != "fk_website_id") {
                $value = substr($value, 3);
                $sJoinFields .= ' ct.' . $value . ' = lt.fk_' . $value;
            } else if ($value != "fk_website_id") {
                $sJoinFields .= ' ct.' . $value . ' = lt.fk_' . $value;
            } else {
                $sJoinFields .= ' ct.' . $value . ' = lt.' . $value;
            }
        }

        if (sizeof($aCommonPks) > 1) {
            $sSelectFields = "CONCAT(" . $sSelectFields . ")";
        }

        // ct.{$sCommonPK} as id,
        if (self::CheckTableExists($sCommonTable) && self::CheckTableExists($sLanguageTable)) {
            $sql = "SELECT {$sSelectFields} as id, lt.*, ct.comment as cmncomment from {$sCommonTable} ct LEFT OUTER JOIN {$sLanguageTable} lt on {$sJoinFields} AND lt.fk_language_id = {$iLangId}
			WHERE {$sWhereClause}";

            return DAL::executeGetRow($sql);
        } else {
            return false;
        }
    }

    public static function createGenericDetailsForm($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL,$bAddForm = true, $aDisplayOptions = NULL) {

        $sOutput = '';
        $actionpage = "/administration/actions/generic_actions.php?tablename=" . $sTablename;
        $aColumns = self::getTableColumns($sTablename);
        /**
         * Get the table details
         */
        $oTableDetails = self::getTableDetails($sTablename);

        if ($iId) {
            $aRecordData = self::getRecordById($sTablename, self::getPrimaryKeys($sTablename), $iId);
            if ($bDeleteButton) {
                $sDeletePage = $actionpage . "&a=delete&id={$iId}";
            }
            $actionpage .= "&a=edit&id={$iId}";
        } else {
            $actionpage .= "&a=add";
        }

        if ($iReturnID != NULL) {
            $actionpage .= "&returnid=" . $iReturnID;
        }

        if ($sPreviousPage) {
            $previousPageQueryString = "&pp=" . substr(urldecode($sPreviousPage), 0, strpos(urldecode($sPreviousPage), "?"));
            $actionpage .= $previousPageQueryString;
            if ($sDeletePage) {
                $sDeletePage .= $previousPageQueryString;
            }
        }

        if ($iWebsiteID) {
            $actionpage .= "&websiteid=" . $iWebsiteID;
        }

        if($bAddForm) {
            $sOutput .= '<form  id="'.$sTablename.'" action="' . $actionpage . '" method="post" enctype="multipart/form-data" name="genericform">';
        }

        if ($sTablename == "multimedia_cmn" && $aRecordData["file"]) {
            $sFolder = MultimediaAdmin::getFolderByID($aRecordData["fk_mmtype_id"]);
            $sFile = $aRecordData["file"];

            $sOutput .= "<fieldset class=\"detailsform\">";
            if (strrchr($sFile, '.') == ".swf") {
                $sOutput .= "<div><a href=\"/system/control/bannertest.php?mm={$iId}\" target=\"_blank\">Click here to test banner</a></div>";
            } else {
                $sOutput .= "<div><img src=\"../../system/media/en/images/{$sFolder}/{$sFile}\" alt=\"Image Not Found\"/></div>";
            }
            $sOutput .= "<label>
			<div>{$sFile}
                            <select name=\"fileselect\" id=\"fileselect\"  onChange=\"change(this.value)\">
                                <option value=\"1\">Leave File As Is</option>
                                <option value=\"2\">Change Filename</option>
                                <option value=\"3\">Change File</option>
                            </select>
                        </div>
                        <div id=\"newfilename_div\" style=\"display: none; margin-top: 5px;\">New filename:
                            <input name=\"filename\" type=\"text\" id=\"filename\" />
                        </div>
                        <div id=\"newfile_div\" style=\"display: none; margin-top: 5px;\">
                            New file:
                        <input name=\"imagefile\" type=\"file\" id=\"imagefile\"/></div> </label>";
            $sOutput .= "</fieldset>";
        }

        $sOutput .= '<fieldset class="detailsform">';
        foreach ($aColumns as $aCurrentCol) {

            if (is_null($aDisplayOptions)||(is_array($aDisplayOptions) && array_key_exists($aCurrentCol["Field"], $aDisplayOptions)))
            {

                $sReadonly = is_null($aDisplayOptions)?"":$aDisplayOptions[$aCurrentCol["Field"]]==true?" readonly='readonly'":"";

                $oFieldDetails = $oTableDetails->aFields[$aCurrentCol['Field']];
                $sFieldCaption = lang::get($sTablename . '-' . $aCurrentCol["Field"]);

                $sCustomHandler = 'FormField_' . $sTablename . '_' . $aCurrentCol["Field"];
                $sCustomTableHandler = 'FormField_' . $oFieldDetails->oConstraint->sTable;

                $sType = strtolower($aCurrentCol["Type"]);
                if ($aCurrentCol["Extra"] == "auto_increment") {
                } elseif (class_exists($sCustomHandler)){
                    $oHandler=new $sCustomHandler($aCurrentCol["Field"], $sFieldCaption,false);
                    $oHandler->mValue=$aRecordData[$aCurrentCol["Field"]];
                    $oHandler->sTableName=$sTablename;
                    $oHandler->iId=$iId;
                    $sOutput.=$oHandler->output();
                } elseif (class_exists($sCustomTableHandler)){
                    $oHandler=new $sCustomTableHandler($aCurrentCol["Field"], $sFieldCaption,false);
                    $oHandler->mValue=$aRecordData[$aCurrentCol["Field"]];
                    $oHandler->sTableName=$sTablename;
                    $oHandler->iId=$iId;
                    $sOutput.=$oHandler->output();
                } elseif (strstr($sType, "tinyint")) {
                    $sOutput .= '<label class="small">';
                    $sOutput .= '<span>' . $sFieldCaption . '</span>';
                    $sOutput .= '<div><input type="hidden" value="0" name="' . $aCurrentCol["Field"] . '"/><input type="checkbox" name="' . $aCurrentCol["Field"] . '" ';
                    if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                        if ($aDefaultData[$aCurrentCol["Field"]] == 1) {
                            $sOutput .= ' checked="true"';
                        }
                    } else if ($aRecordData && ($aRecordData[$aCurrentCol["Field"]] == 1)) {
                        $sOutput .= ' checked="true"';
                    } else if (!$aRecordData && $aCurrentCol["Default"]) {
                        $sOutput .= ' checked="true"';
                    }
                    $sOutput .= $sReadonly . '/></div></label>';
                } elseif (strstr($sType, "text")) {
                    $sOutput .= '<label class="long">';
                    $sOutput .= '<span>' . $sFieldCaption . '</span>';
                    ($aCurrentCol["Field"] == "content") ? $iRows = 22 : $iRows = 5;
                    $sOutput .= '<div><textarea rows="' . $iRows . '" name="' . $aCurrentCol["Field"] . '"' . $sReadonly .'>';
                    if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                        $sOutput .= $aDefaultData[$aCurrentCol["Field"]];
                    } else if ($aRecordData && $aRecordData[$aCurrentCol["Field"]]) {
                        $sOutput .= $aRecordData[$aCurrentCol["Field"]];
                    } else if ($aCurrentCol["Default"] != NULL) {
                        $sOutput .= $aCurrentCol["Default"];
                    }
                    $sOutput .= '</textarea></div></label>';
                } else {
                    $sClass = "long";
                    if (strstr($sType, "time")) {
                        $sClass = "halfsize datetimepicker";
                    } elseif (strstr($sType, "date")) {
                        $sClass = "halfsize datepicker";
                    }
                    if (strstr($sType, "int") && !$oFieldDetails->oConstraint) {
                        $sClass = 'small number';
                    }
                    if (strstr($sType, "int") && $oFieldDetails->oConstraint) {
                        $sClass = 'halfsize number';
                    }
                    $sOutput .= '<label class="' . $sClass . '">';
                    $sOutput .= '<span>' . $sFieldCaption . '</span><div>';
                    if ($oFieldDetails->oConstraint) {
                        $sOutput .= '<input type="hidden" name="' . (($aCurrentCol["Field"] == "tablename") ? "tablename%ignoreme%" : $aCurrentCol["Field"]) . '" id="field_' . $aCurrentCol["Field"] . '" class="' . $sClass . '" value="';
                        if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                            $sOutput .= $aDefaultData[$aCurrentCol["Field"]];
                        } else if ($aRecordData && $aRecordData[$aCurrentCol["Field"]]) {
                            $sOutput .= $aRecordData[$aCurrentCol["Field"]];
                        } else if ($aCurrentCol["Default"] != NULL) {
                            if ($aCurrentCol["Default"] === "CURRENT_TIMESTAMP") {
                                $aCurrentCol["Default"] = date("Y-m-d h:i:s");
                            }
                            $sOutput .= $aCurrentCol["Default"];
                        }
                        $sComments = self::getFieldComment($aCurrentCol["Field"], $sTablename);
                        $sOutput .= '" />';
                        $sLookupFieldValue = '';
                        if ($aRecordData[$aCurrentCol["Field"]] != '') {
                            $sLookupSql = 'SELECT * FROM `' . $oFieldDetails->oConstraint->sTable . '` WHERE `' . $oFieldDetails->oConstraint->sTableField . '`=' . $aRecordData[$aCurrentCol["Field"]];
                            $aLookupFieldValueFields = DAL::executeGetRow($sLookupSql);
                            foreach ($aLookupFieldValueFields as $sLookupFieldName => $sLookupFieldValueItem) {
                                if (!is_numeric($sLookupFieldValueItem) && $sLookupFieldValue == '') {
                                    $sLookupFieldValue.=$sLookupFieldValueItem;
                                }
                            }
                        }
                        if ($sLookupFieldValue == '')
                            $sLookupFieldValue = 'None';
                        $sOutput .= ' <div><input type="text" readonly="readonly" id="field_lookup_' . $aCurrentCol["Field"] . '" value="' . $sLookupFieldValue . '"/></div>';
                        $sOutput .= ' <a href="tableview.php?tablename=' . $oFieldDetails->oConstraint->sTable . '&fk=' . $oFieldDetails->oConstraint->sTableField . '" class="simpledialog" field="field_' . $aCurrentCol["Field"] . '"><i class="fa fa-chevron-circle-down"></i></a>';
                    }else {
                        $sOutput .= '<input type="text" name="' . (($aCurrentCol["Field"] == "tablename") ? "tablename%ignoreme%" : $aCurrentCol["Field"]) . '" id="field_' . $aCurrentCol["Field"] . '" class="' . $sClass . '" value="';
                        if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                            $sOutput .= $aDefaultData[$aCurrentCol["Field"]];
                        } else if ($aRecordData && $aRecordData[$aCurrentCol["Field"]]) {
                            $sOutput .= $aRecordData[$aCurrentCol["Field"]];
                        } else if ($aCurrentCol["Default"] != NULL) {
                            if ($aCurrentCol["Default"] === "CURRENT_TIMESTAMP") {
                                $aCurrentCol["Default"] = date("Y-m-d h:i:s");
                            }
                            $sOutput .= $aCurrentCol["Default"];
                        }
                        $sComments = self::getFieldComment($aCurrentCol["Field"], $sTablename);
                        $sOutput .= '"' . $sReadonly . '/>';
                    }
                    if ($sComments) {
                        $sOutput .= ' <a href="tableview.php?tablename=' . self::getFieldComment($aCurrentCol["Field"], $sTablename) . '" class="simpledialog" field="field_' . $aCurrentCol["Field"] . '"><i class="fa fa-chevron-circle-down"></i></a>';
                    }
                    $sOutput .= '</div></label>';
                }
            }
        }
        /*
        if ($sTablename == "pages_cmn") {
            $sOutput .= '<tr><th align="right" class="small">';
            $sOutput .= "File Content";
            $sOutput .= '</th><td><small>Use %id% for page id.</small><p /><textarea rows="22" name="filecontent">';
            $iPageID = $iId;
            if ($iPageID) {
                $sOutput .= PageAdmin::getPageCodeByID($iPageID);
            }
            $sOutput .= '</textarea></td></tr>';
        }
*/

        $sOutput .= '</fieldset>';
        if($bAddForm)
        {

            $sOutput .= '<fieldset>';
            if ($bDeleteButton) {
                $sOutput .= ' <input type="button" value="Delete" style="float: left;" onclick="if(confirm(\'Are you sure you want to delete?\')){ document.location = \'' . $sDeletePage . '\'; }" />';
            }

            $sOutput .= '<input type="Submit" value="Save" />';
            $sOutput .= '<input type="button" value="Cancel" onclick="document.location = \'';
            if ($sPreviousPage) {
                $sOutput .= urldecode($sPreviousPage);
            } else {
                $sOutput .= 'generic-listing.php?tablename=' . $sTablename;
            }
            $sOutput .= '\'"/>';
            $sOutput .= '</fieldset>';

            $sOutput .= "</form>";
        }

        return $sOutput;
    }

    public static function createGenericLanguageDetailsForm($iId, $sCommonTable, $sLanguageTable, $iLangId, $aDefaultData = NULL, $sPreviousPage = '', $iReturnID = NULL) {
        $aIds = explode("_", $iId);
        $sOutput = '';
        $actionpage = "actions/generic_actions.php?tablename=" . $sCommonTable;

        $aCommonPks = self::getPrimaryKeys($sCommonTable);
        $aColumns = self::getTableColumns($sLanguageTable);

        $aRecordData = self::getLanguageRecordById($iId, $aCommonPks, $sCommonTable, $sLanguageTable, $iLangId);

        $actionpage .= "&a=edit&id=" . $iId . "&lid=" . $iLangId . "&languagetable=" . $sLanguageTable;
        if ($iReturnID != NULL) {
            $actionpage .= "&returnid=" . $iReturnID;
        }
        if (!$aRecordData["translationid"]) {
            $bValidKeys = true;
            $sTranslationID = "";
            foreach ($aCommonPks as $CommonPrimaryKeys) {
                if ($aRecordData['fk_' . $CommonPrimaryKeys['column_name']]) {
                    $sTranslationID .= $aRecordData['fk_' . $CommonPrimaryKeys['column_name']] . "-";
                } else {
                    $bValidKeys = false;
                }
            }
            if ($bValidKeys) {
                $aRecordData["translationid"] = rtrim($sTranslationID, "-");
            }
        }
        if ($sPreviousPage) {
            $actionpage .= "&pp=" . substr(urldecode($sPreviousPage), 0, strpos(urldecode($sPreviousPage), "?"));
        }

        $sOutput .= '<form action="' . $actionpage . '" method="post" enctype="multipart/form-data" name="genericform">';
        $sOutput .= '<table class="detailsform">';

        $sOutput .= '<tr><th align="right" class="small">Comment</th><td>' . $aRecordData['cmncomment'] . '</td></tr>';
        if ($aRecordData["translationid"]) {
            $sOutput .= '<tr><th align="right" class="small">Translation Id</th><td><input name="translationid" type="text" readonly="true" value="' . $aRecordData['translationid'] . '"/></td></tr>';
        }
        foreach ($aColumns as $aCurrentCol) {
            $sType = strtolower($aCurrentCol["Type"]);
            if (( $aCurrentCol["Extra"] == "auto_increment" || $aCurrentCol["Key"] == "PRI") && !strstr($aCurrentCol["Field"], "fk_")) {

            } elseif (strstr($aCurrentCol["Field"], "fk_")) {
                if (($aCurrentCol["Field"] == "fk_language_id") && (!$aRecordData[$aCurrentCol["Field"]]) && $iLangId) {
                    $aRecordData[$aCurrentCol["Field"]] = $iLangId;
                } else {
                    foreach ($aCommonPks as $iPkCounter => $aCurrentPk) {
                        $sCurrentPk = $aCurrentPk["column_name"];
                        if (!strstr($sCurrentPk, "fk_")) {
                            $sCurrentPk = "fk_" . $sCurrentPk;
                        }
                        if (($aCurrentCol["Field"] == $sCurrentPk) && (!$aRecordData[$aCurrentCol["Field"]]) && $aIds[$iPkCounter]) {
                            $aRecordData[$aCurrentCol["Field"]] = $aIds[$iPkCounter];
                            break;
                        }
                    }
                }

                if ($aCurrentCol["Key"] == "PRI") {
                    $sOutput .= '<input type="hidden" name="' . $aCurrentCol["Field"] . '" value="';
                } else {
                    $sOutput .= '<tr><th align="right" class="small">';
                    $sOutput .= $aCurrentCol["Field"];
                    $sOutput .= '</td><td><input type="text" name="' . $aCurrentCol["Field"] . '" value="';
                }
                if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                    $sOutput .= $aDefaultData[$aCurrentCol["Field"]];
                } else if ($aRecordData && $aRecordData[$aCurrentCol["Field"]]) {
                    $sOutput .= $aRecordData[$aCurrentCol["Field"]];
                }
                $sOutput .= '"';
                $sComments = self::getFieldComment($aCurrentCol["Field"], $sLanguageTable);
                $sOutput .= ' />';
                if ($aCurrentCol["Key"] != "PRI") {
                    if ($sComments) {
                        $sOutput .= ' <a href="tableview.php?tablename=' . self::getFieldComment($aCurrentCol["Field"], $sTablename) . '"  class="simpledialog">View related data</a>';
                    }
                    $sOutput .= '</td></tr>';
                }
            } elseif (strstr($sType, "tinyint")) {
                $sOutput .= '<tr><th align="right" class="small">';
                $sOutput .= $aCurrentCol["Field"];
                $sOutput .= '</td><td><input type="checkbox" name="' . $aCurrentCol["Field"] . '" ';
                if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                    if ($aDefaultData[$aCurrentCol["Field"]] == 1) {
                        $sOutput .= ' checked="true"';
                    }
                } else if ($aRecordData && ($aRecordData[$aCurrentCol["Field"]] == 1)) {
                    $sOutput .= ' checked="true"';
                } else if ($aCurrentCol["Default"]) {
                    $sOutput .= ' checked="true"';
                }
                $sOutput .= '" /></td></tr>';
            } elseif (strstr($sType, "text")) {
                $sOutput .= '<tr><th align="right" class="small">';
                $sOutput .= $aCurrentCol["Field"];
                ($aCurrentCol["Field"] == "content") ? $iRows = 22 : $iRows = 5;
                $sOutput .= '</td><td><textarea rows="' . $iRows . '" name="' . $aCurrentCol["Field"] . '">';
                if ($aDefaultData && $aDefaultData[$aCurrentCol["Field"]]) {
                    $sOutput .= $aDefaultData[$aCurrentCol["Field"]];
                } else if ($aRecordData && $aRecordData[$aCurrentCol["Field"]]) {
                    $sOutput .= $aRecordData[$aCurrentCol["Field"]];
                } else if ($aCurrentCol["Default"] != NULL) {
                    $sOutput .= $aCurrentCol["Default"];
                }
                $sOutput .= '</textarea></td></tr>';
            } else {
                /* if(($aCurrentCol["Field"] == ("fk_" . $sCommonPK)) && (!$aRecordData[$aCurrentCol["Field"]])){
                  $aRecordData[$aCurrentCol["Field"]] = $iId;
                  }
                  else */
                $sOutput .= '<tr><th align="right" class="small">';
                $sOutput .= $aCurrentCol["Field"];
                $sOutput .= '</td><td><input type="text" name="' . $aCurrentCol["Field"] . '" class="long" value="';
                if ($aDefaultData && isset($aDefaultData[$aCurrentCol["Field"]])) {
                    $sOutput .= $aDefaultData[$aCurrentCol["Field"]];
                } else if ($aRecordData && $aRecordData[$aCurrentCol["Field"]]) {
                    $sOutput .= $aRecordData[$aCurrentCol["Field"]];
                } else if ($aCurrentCol["Default"] != NULL) {
                    if ($aCurrentCol["Default"] === "CURRENT_TIMESTAMP") {
                        $aCurrentCol["Default"] = date("Y-m-d h:i:s");
                    }
                    $sOutput .= $aCurrentCol["Default"];
                }
                $sOutput .= '" /></td></tr>';
            }
        }

        $sOutput .= '<tr><th colspan="2" align="right"><input type="Submit" value="Save" /> <input type="button" value="Cancel" onclick="document.location = \'';
        if ($sPreviousPage) {
            $sOutput .= urldecode($sPreviousPage);
        } else {
            $sOutput .= 'generic-listing.php?tablename=' . $sTablename;
        }
        $sOutput .= '\'"/>		</th></tr>
    </table>';

        $sOutput .= "</form>";
        return $sOutput;
    }

    public static function doGenericInsert($sTablename, $aData) {
        return DAL::Insert($sTablename, $aData, true);
    }

    /**
     * Updates data in the database
     * @param type $sTablename
     * @param type $aData
     * @param type $sParams
     * @return type
     */
    public static function doGenericUpdate($sTablename, $aData, $sParams) {
        /**
         * Set up an event and a hook before update happens
         */
        $sModelName=$sTablename.'_Model';
        $aDataBeforeSave=DAL::executeGetRow('SELECT * FROM '.$sTablename.' WHERE '.$sParams);
        if(class_exists($sModelName)){
            $oModel=new $sModelName();
            $oModel->populateFromArray($aDataBeforeSave);
            $oModel->beforeGenericUpdate($aData);
        }

        /**
         * Update the database with the generic DAL method
         */
        $return=DAL::Update($sTablename, $aData, $sParams);

        /**
         * Log all changes
         */
        global $oSecurityObject;
        $fk_id_parts=explode('=',$sParams);
        $fk_id=intval(preg_replace('/[^0-9]*/','',$fk_id_parts[1]));
        foreach($aData as $k=>$v){
            if($v!=$aDataBeforeSave[$k]){
                AuditLog::LogItem($oSecurityObject->getsFullName().' changed '.$k.' from ['.$aDataBeforeSave[$k].'] to ['.$v.']', 'RECORD_CHANGE', $sTablename, $fk_id);
            }
        }

        /**
         * Call a hook on any models that can be loaded
         */
        if(class_exists($sModelName)){
            $oModel=new $sModelName();
            $aData=DAL::executeGetRow('SELECT * FROM '.$sTablename.' WHERE '.$sParams);
            $oModel->populateFromArray($aData);
            $oModel->afterGenericUpdate($aDataBeforeSave);
        }

        return $return;
    }

    public static function doGenericDelete($iId, $sTablename) {
        $aIds = explode("_", $iId);
        $aPrimaryKeys = self::getPrimaryKeys($sTablename);
        $sParams = '';
        foreach ($aPrimaryKeys as $counter => $sCurrentKey) {
            $sCurrentKey = ($sCurrentKey['Column_name'])?$sCurrentKey['Column_name']:$sCurrentKey['column_name'];
            if ($sParams != '') {
                $sParams .= ' AND ';
            }
            $sParams .= "{$sCurrentKey} = ";
            if (is_int($aIds[$counter])) {
                $sParams .= $aIds[$counter];
            } else {
                $sParams .= "'" . $aIds[$counter] . "'";
            }
        }
        $sql = "DELETE FROM $sTablename WHERE $sParams";
        return DAL::Query($sql);
    }

    public static function deleteGenericRelations($sTablename, $sPrimaryKey, $sID) {
        if ($sTablename && $sPrimaryKey && $sID) {
            $sSQL = "DELETE FROM {$sTablename} WHERE {$sPrimaryKey} = '{$sID}'";
            return DAL::executeQuery($sSQL);
        }
        return false;
    }

    public static function getFilterField($iFilterId) {
        switch ($iFilterId) {
            case AdminPage::LANGUAGE:
                $sReturn = "fk_language_id";
                break;
            case AdminPage::AUTHOR:
                $sReturn = "fk_admin_user_id";
                break;
            case AdminPage::LOTTERY:
                $sReturn = "fk_lottery_id";
                break;
            case AdminPage::BOOKINGSTATUS:
                $sReturn = "fk_bookingstatus_id";
                break;
            case AdminPage::BOOKINGDATE:
                $sReturn = "bookingdate";
                break;
            case AdminPage::ARRIVALDATE:
                $sReturn = "arrivaldate";
                break;
            case AdminPage::MULTIMEDIATYPE:
                $sReturn = "fk_multimedia_id";
                break;
            case AdminPage::USERTYPE:
                $sReturn = "fk_admintype_id";
                break;
            case AdminPage::USER:
                $sReturn = "user_id";
                break;
        }
        return $sReturn;
    }

    public static function createFilterStringForSql($aFilters, $sTablename) {
        $sFilterSql = "";

        if (!empty($aFilters)) {
            foreach ($aFilters as $iFilterId => $aCurrentFilter) {
                if (!is_null($iFilterId)) {
                    $sCurrentFilter = "";
                    $sCurrentFilterField = self::getFilterField($iFilterId);
                    $sPrefix = "ct.";
                    if ((self::checkColumnExists($sCurrentFilterField, $sTablename))) {

                        if (is_array($aCurrentFilter) && !empty($aCurrentFilter)) {
                            $sCurrentFilter .= $sPrefix;
                            $sCurrentFilter .= $sCurrentFilterField . " IN (";
                            $sCurrentFilter .= implode(", ", $aCurrentFilter);
                            $sCurrentFilter .= ")";
                        }
                        if ($sFilterSql != "" && $sCurrentFilter != "") {
                            $sFilterSql .= " AND ";
                        }
                        $sFilterSql .= $sCurrentFilter;
                    }
                }
            }
        }
        return $sFilterSql;
    }

    public static function appendConditionToFilterString($sFilterString, $sCondition) {
        if ($sFilterString != "") {
            $sFilterString .= " AND ";
        }
        $sFilterString .= $sCondition;
        return $sFilterString;
    }

    function displayRootPage($rootpage) {
        $iMMCount = PageAdmin::checkMMPage($rootpage["websiteid"], $rootpage['id']);
        $sOutput = "<ul class=\"page-list\"  style=\"margin-left: 0;\"><li class=\"clear-element\">";
        $sOutput .= "<table class='pages-table'>";
        $sOutput .= "<tr>";
        $sOutput .= "<td class='handle'><div class=\"sort-handle\"></div></td>";
        $sOutput .= "<td class='tiny'>" . $rootpage["id"] . "</td>";
        $sOutput .= "<td class='long'><a href='generic-details.php?tablename=pages_cmn&id=" . $rootpage["id"] . "&pp=pages.php'>" . $rootpage["comment"] . "</a></td>";
        $sOutput .= "<td class='medium'>" . $rootpage["url"] . "</td>";
        $sOutput .= "<td class='tiny imagecount'><a href=\"pagemm.php?id=" . $rootpage["id"] . "&tablename=r_mm_pages\" onclick=\"return checkSorting();\"><img src='images/assignmm.gif' alt='Assign MM' title='Assign page multimedia'/><span>{$iMMCount}</span></a></td>";
        $sOutput .= "<td class='tiny'><a href=\"#\" action=\"clearcache\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/clearcache.gif' alt='clearcache' title='Clear cache'/></td>";
        $sOutput .= "<td class='tiny'>";
        if ($rootpage['is_active']) {
            $sOutput .= "<a href='#' action=\"disable\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/enabled.gif' alt='Click to disable' title='Click to disable' /></a>";
        } else {
            $sOutput .= "<a href='#' action=\"enable\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/disabled.gif' alt='Click to enable' title='Click to enable' /></a>";
        }
        $sOutput .= "</td>";
        $sOutput .= "<td class='tiny'>";
        if ($rootpage['showinmenu']) {
            $sOutput .= "<a href='#' action=\"donotshowinmenu\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/showinmenu.gif' alt='Remove from menu' title='Click to hide from menu' /></a>";
        } else {
            $sOutput .= "<a href='#' action=\"showinmenu\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/donotshowinmenu.gif' alt='Show in menu' title='Click to show in menu' /></a>";
        }
        $sOutput .= "<td class='tiny'>";
        if ($rootpage['showinsitemap']) {
            $sOutput .= "<a href='#' action=\"donotshowinsitemap\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/enabled.gif' alt='Remove from sitemap' title='Click to hide from sitemap' /></a>";
        } else {
            $sOutput .= "<a href='#' action=\"showinsitemap\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/disabled.gif' alt='Show in sitemap' title='Click to show in sitemap' /></a>";
        }
        $sOutput .= "</td>";
        $sOutput .= "</td>";
        $sOutput .= "<td class='tiny'></td>";
        $sOutput .= "</tr></table>";
        $sOutput .= " </tr>";
        $sOutput .= "</table></li></ul>";
        return $sOutput;
    }

    function displayChildPages($aChildPages) {
        $sOutput = "";
        $iLevel = 0;
        foreach ($aChildPages as $key => $aCurrentPage) {
            $iMMCount = PageAdmin::checkMMPage($aCurrentPage['id']);
            if ($iLevel < $aCurrentPage['level']) {
                $sOutput .= "<ul class='page-list'>";
            } elseif ($iLevel > $aCurrentPage['level']) {
                $sOutput .= "</ul>";
            }

            $sOutput .= "<li id='{$aCurrentPage['id']}' class='clear-element page-item left'>";
            $sOutput .= "<table class='pages-table'><tr>";
            $sOutput .= "<td class='handle'><div class='sort-handle'><img src='images/handle.gif' alt='&equiv;' /></div></td>";
            $sOutput .= "<td class='tiny'>{$aCurrentPage['id']}</td>";
            $sOutput .= "<td class='long'><a href=\"generic-details.php?tablename=pages_cmn&id=" . $aCurrentPage['id'] . "&pp=pages.php\">{$aCurrentPage['comment']}</a></td>";
            $sOutput .= "<td class='medium'>{$aCurrentPage['url']}</td>";
            $sOutput .= "<td class='tiny imagecount'><a href=\"pagemm.php?id=" . $aCurrentPage["id"] . "&tablename=r_mm_pages\" onclick=\"return checkSorting();\"><img src='images/assignmm.gif' alt='Assign MM' title='Assign page multimedia'/><span>{$iMMCount}</span></a></td>";
            $sOutput .= "<td class='tiny'><a href=\"#\" action=\"clearcache\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/clearcache.gif' alt='clearcache'/></td>";
            $sOutput .= "<td class='tiny'>";
            if ($aCurrentPage['is_active']) {
                $sOutput .= "<a href='#' action=\"disable\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/enabled.gif' alt='Click to disable' title='Click to disable' /></a>";
            } else {
                $sOutput .= "<a href='#' action=\"enable\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/disabled.gif' alt='Click to enable' title='Click to enable' /></a>";
            }
            $sOutput .= "</td>";
            $sOutput .= "<td class='tiny'>";
            if ($aCurrentPage['showinmenu']) {
                $sOutput .= "<a href='#' action=\"donotshowinmenu\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/showinmenu.gif' alt='Remove from menu' title='Click to hide from menu' /></a>";
            } else {
                $sOutput .= "<a href='#' action=\"showinmenu\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/donotshowinmenu.gif' alt='Show in menu' title='Click to show in menu' /></a>";
            }
            $sOutput .= "</td>";
            $sOutput .= "<td class='tiny'>";
            if ($aCurrentPage['showinsitemap']) {
                $sOutput .= "<a href='#' action=\"donotshowinsitemap\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/enabled.gif' alt='Remove from sitemap' title='Click to hide from sitemap' /></a>";
            } else {
                $sOutput .= "<a href='#' action=\"showinsitemap\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/disabled.gif' alt='Show in sitemap' title='Click to show in sitemap' /></a>";
            }
            $sOutput .= "</td>";
            $sOutput .= "<td class='tiny'><a href='#' action=\"deleteglobal\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/delete.gif' alt='del' title='Delete'/></a></td>";
            $sOutput .= "</tr></table>";
            $sOutput .= "</li>";

            $iLevel = $aCurrentPage['level'];
        }
        return $sOutput;
    }

    function displayRootPageLang($rootpage) {
        $iMMCount = PageAdmin::checkMMPage($rootpage["websiteid"], $rootpage['id']);
        if (!$rootpage["title"]) {
            $rootpage["title"] = "Click here to translate";
            $sClass = " class=\"red\"";
        } else {
            $sClass = "";
        }
        $sOutput = "<ul class=\"page-list\" style=\"margin-left: 0;\"><li class=\"clear-element\">";
        $sOutput .= "<table class='pages-table'>";
        $sOutput .= "<tr>";
        $sOutput .= "<td class='handle'><div class=\"sort-handle\"></div></td>";
        $sOutput .= "<td class='tiny'>" . $rootpage["id"] . "</td>";
        $sOutput .= "<td class='long'><a href='generic-details.php?tablename=pages_cmn&languagetable=pages_lang&lid=" . $rootpage["lid"] . "&id=" . $rootpage["id"] . "&pp=pages.php' {$sClass}>" . $rootpage["title"] . "</a></td>";
        $sOutput .= "<td class='medium'>" . $rootpage["url"] . "</td>";
        $sOutput .= "<td class='tiny imagecount'><a href=\"pagemm.php?id=" . $rootpage["id"] . "&tablename=r_mm_pages\" onclick=\"return checkSorting();\"><img src='images/assignmm.gif' alt='Assign MM' title='Assign page multimedia'/><span>{$iMMCount}</span></a></td>";
        $sOutput .= "<td class='tiny'><a href=\"#\" action=\"clearcache\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/clearcache.gif' alt='clearcache' title='Clear cache'/></td>";
        $sOutput .= "<td class='tiny'>";
        if ($rootpage['lang_active']) {
            $sOutput .= "<a href='#' action=\"disablelanguage\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/enabled.gif' alt='Click to disable' title='Click to disable' /></a>";
        } else {
            $sOutput .= "<a href='#' action=\"enablelanguage\" onclick=\"doAction($(this).attr('action')," . $rootpage['id'] . ", $(this)); return false;\"><img src='images/disabled.gif' alt='Click to enable' title='Click to enable' /></a>";
        }
        $sOutput .= "</td>";
        $sOutput .= "<td class='tiny'></td>";
        $sOutput .= "</tr></table>";
        $sOutput .= " </tr>";
        $sOutput .= "</table></li></ul>";
        return $sOutput;
    }

    function displayChildPagesLang($aChildPages) {
        $sOutput = "";
        $iLevel = 0;
        foreach ($aChildPages as $key => $aCurrentPage) {
            $iMMCount = PageAdmin::checkMMPage($aCurrentPage['id']);
            if ($iLevel < $aCurrentPage['level']) {
                $sOutput .= "<ul class='page-list'>";
            } elseif ($iLevel > $aCurrentPage['level']) {
                $sOutput .= "</ul>";
            }
            if (!$aCurrentPage["title"]) {
                $aCurrentPage["title"] = "Click here to translate";
                $sClass = " class=\"red\"";
            } else {
                $sClass = "";
            }
            $iMMCount = PageAdmin::checkMMPage($aCurrentPage['id']);
            $sOutput .= "<li id='{$aCurrentPage['id']}' class='clear-element page-item left'>";
            $sOutput .= "<table class='pages-table'><tr>";
            $sOutput .= "<td class='handle'><div class='sort-handle'><img src='images/handle.gif' alt='&equiv;' /></div></td>";
            $sOutput .= "<td class='tiny'>{$aCurrentPage['id']}</td>";
            $sOutput .= "<td class='long'><a href=\"generic-details.php?tablename=pages_cmn&languagetable=pages_lang&lid=" . $aCurrentPage["lid"] . "&id=" . $aCurrentPage['id'] . "&pp=pages.php\" {$sClass}>{$aCurrentPage['title']}</a></td>";
            $sOutput .= "<td class='medium'>{$aCurrentPage['url']}</td>";
            $sOutput .= "<td class='tiny imagecount'><a href=\"pagemm.php?id=" . $aCurrentPage["id"] . "&tablename=r_mm_pages\" onclick=\"return checkSorting();\"><img src='images/assignmm.gif' alt='Assign MM' title='Assign page multimedia'/><span>{$iMMCount}</span></a></td>";
            $sOutput .= "<td class='tiny'><a href=\"#\" action=\"clearcache\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/clearcache.gif' alt='clearcache'/></td>";
            $sOutput .= "<td class='tiny'>";
            if ($aCurrentPage['lang_active']) {
                $sOutput .= "<a href='#' action=\"disablelanguage\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/enabled.gif' alt='Click to disable' title='Click to disable' /></a>";
            } else {
                $sOutput .= "<a href='#' action=\"enablelanguage\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/disabled.gif' alt='Click to enable' title='Click to enable' /></a>";
            }
            $sOutput .= "<td class='tiny'><a href='#' action=\"deletelang\" onclick=\"doAction($(this).attr('action')," . $aCurrentPage['id'] . ", $(this)); return false;\"><img src='images/delete.gif' alt='del' title='Delete Translation'/></a></td>";
            $sOutput .= "</tr></table>";
            $sOutput .= "</li>";

            $iLevel = $aCurrentPage['level'];
        }
        return $sOutput;
    }

    public static function createGenericTableView($sTablename, $iLangId = 0, $aFilters = NULL, $iLimit = 10) {
        if (is_array($aFilters)) {
            $sFilters = self::createFilterStringForSql($aFilters, $sTablename);
        } else if ($aFilters) {
            $sFilters = $aFilters;
        }
        if ($iLangId) {
            $aData = self::getLanguageRecords($sTablename, $iLangId, $sFilters, "", $iLimit);
        } else {
            $sOrderBy = ($sTablename == "ratings") ? "approved ASC" : "";
            $aData = self::getRecords($sTablename, $sFilters, $sOrderBy, $iLimit);
        }
        $aColumns = self::getTableColumns($sTablename);

        if ($iLangId) {
            array_push($aColumns, array("Field" => "comment", "Type" => "varchar"));
        }

        $aPrimaryKeys = self::getPrimaryKeys($sTablename);
        $sPrimaryKeys = "";
        foreach ($aPrimaryKeys as $aCurrentKey) {
            if ($sPrimaryKeys != "") {
                $sPrimaryKeys .= "|";
            }
            $sPrimaryKeys .= $aCurrentKey["Column_name"];
        }

        $sDataTable = '';
        if ($aData && !empty($aData)) {
            $sDataTable .= '<table class="datatable"><tr>';
            $iColumnCount = 0;
            foreach ($aColumns as $currentField) {
                if ($currentField["Field"] != "password" && $currentField["Field"] != "pin") {
                    $iColumnCount++;
                    if ($iColumnCount >= AdminPage::COLUMN_LIMIT && $currentField["Field"] != "isactive" && $currentField["Field"] != "password" && $currentField["Field"] != "pin") {

                    } else {
                        $sTitle = $currentField["Field"];
                        $sFieldCaption = lang::get($sTablename . '-' . $sTitle);
                        $sClass = '';
                        if ($currentField["Key"] == "PRI") {
                            $sClass .= "tiny centeralign";
                        } elseif ($currentField["Type"] == "varchar(10)") {
                            $sClass .= "tiny centeralign";
                        }

                        $sClass = ' class="' . $sClass . '"';
                        if (!strstr($currentField["Type"], "tinyint")) {
                            $sDataTable .= '<th' . $sClass . '>' . $sFieldCaption . '</th>';
                        }
                    }
                }
            }
            $sDataTable .= "<th></th>";

            $sDataTable .= '</tr>';
            foreach ($aData as $currentRow => $currentItem) {


                $sReturnDisplay = '';
                if ($_GET['fk']) {
                    $sCurrentId = $currentItem[$_GET['fk']];
                    $currentKey['Column_name'] = $_GET['fk'];
                } else {
                    $sCurrentId = '';
                    foreach ($aPrimaryKeys as $currentKey) {
                        if ($sCurrentId != '') {
                            $sCurrentId .= '_';
                        }
                        $sCurrentId .= $currentItem[$currentKey['Column_name']];
                    }
                }
                if ($currentRow % 2 == 1) {
                    $sDataTable .= '<tr class="alternate">';
                } else {
                    $sDataTable .= '<tr>';
                }
                $iColumnCount = 0;
                foreach ($aColumns as $currentField) {
                    $sButtonDisabled='';
                    $sButtonStyle='';
                    $sButtonLabel='Select';
                    if($currentField["Field"]=='is_active' && $currentItem[$currentField["Field"]]=="0"){

                            $sButtonStyle=' style="background-color:#ccc" ';

                            $sButtonDisabled="disabled";

                            $sButtonLabel='INACTIVE';

                    }
                    //var_dump($xdisabled);
                    if ($currentField["Field"] != "password" && $currentField["Field"] != "pin") {
                        $iColumnCount++;
                        if ($iColumnCount >= AdminPage::COLUMN_LIMIT && $currentField["Field"] != "isactive") {

                        } else {
                            $sValue = $currentItem[$currentField["Field"]];
                            if (strlen($sValue) > 70) {
                                $sValue = substr(htmlentities($sValue), 0, 70) . "...";
                            }
                            if ($sReturnDisplay == '' && $currentField["Field"] != $currentKey['Column_name']) {
                                $sReturnDisplay = $sValue;
                            }

                            $sClass = '';
                            $bShow = true;
                            if (strstr($currentField["Type"], "tinyint")) {
                                $bShow = false;
                            } elseif ($currentField["Key"] == "PRI") {
                                $sClass .= "centeralign";
                            } elseif ($currentField["Type"] == "varchar(10)") {
                                $sClass .= "centeralign";
                            }
                            $sTitleAttribute = $currentField["Field"];
                            $sClass = ' class="' . $sClass . '"';
                            if ($bShow) {
                                $sDataTable .= '<td title="' . $sTitleAttribute . '" ' . $sClass . '>' . $sValue . '</td>';
                            }
                        }
                    }


                }


                $sDataTable .= '<td title="Select Field"><input '.$sButtonDisabled.' type="button" '.$sButtonStyle.' recordid="' . $sCurrentId . '" data-return-text="' . $sReturnDisplay . '" value="'.$sButtonLabel.'" onclick="selectItem(this);"/></td>';
                $sDataTable .= '</tr>';
            }
            $sDataTable .= "</table>";
        } else {
            $sDataTable = "There is nothing to display.";
        }
        return $sDataTable;
    }

}
