<?php

/**
 * ReportsAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class ReportsAdmin {

    private $aDataSet;
    private $aColumnsToSum;
    private $aSums;
    private $sStartTable = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class='report'>";
    private $sEndTable = "</table>";
    private $sStartRow = "<tr>";
    private $sStartFooterRow = "<tr class='footer'>";
    private $sStartRowAlternate = "<tr class='alternate'>";
    private $sEndRow = "</tr>";
    private $sStartColumn = "<td>";
    private $sEndColumn = "</td>";
    private $sStartFooterColumn = "<td style=\"border-top: 1px solid\">";
    private $sType = "";
    private $oSecurityObject = NULL;
    private $aFilters = array();

    public function __construct(&$oSecurityObject, $sType, $aFilters, $sPassedThruSQL = NULL) {
        //Get Booking Type and call display();
        $this->oSecurityObject = $oSecurityObject;
        $this->sType = $sType;
        $this->aFilters = $aFilters;
        $bInvalid = false;
        switch ($sType) {
            case "TicketsNotPurchased":
                $sSQL = $this->getTicketsNotPurchased();
                $aSums = array("Price Paid");
                break;

            case "BookingsToday":
                $sSQL = $this->getBookingsToday();
                $aSums = array("Total", "Tickets");
                break;
            case "BookingsYesterday":
                $sSQL = $this->getBookingsYesterday();
                $aSums = array("Total", "Tickets");
                break;

            case "UnpurchasedLotteryTickets":
                $sSQL = $this->getUnpurchasedLotteryTickets();
                $aSums = array();
                break;

            case "StatisticsToday":
                $sSQL = $this->getStatisticsToday();
                $aSums = array("Count", "Total");
                break;
            case "StatisticsByMonth":
                $sSQL = $this->getStatisticsByMonth();
                $aSums = array("Count", "Total");
                break;

            case "Affiliates":
                $sSQL = $this->getAffiliateBookings();
                $aSums = array("Tickets Count", "Total", "Commission");
                break;
            case "AffiliatesByMonth":
                $sSQL = $this->getAffiliateBookingsByMonth();
                $aSums = array("Tickets Count", "Total", "Commission");
                break;
            case "TicketsByMembers":
                $sSQL = $this->getBookingsByMember();
                $aSums = array("Tickets Count", 'Tickets Cost');
                break;
            
            case "UnapprovedWinnings" :
                $sSQL = $this->getTransactionsPending(7);
                $aSums = array();
                //echo($sSQL);
                break;
            case "AdvertStatistics":
                $sSQL = $this->getAffiliateAdvertStastics();
                $aSums = array();
                break;
            
            default:
                if ($sPassedThruSQL != NULL) {
                    $sSQL = $sPassedThruSQL;
                    $aSums = array();
                } else {
                    echo "<div class='error' style='display:block;'>Report Not Found</div>";
                    return false;
                }
                break;
        }

        if ($bInvalid == false) {

            $this->aDataSet = FALSE;
            $this->aDataSet = DAL::executeQuery($sSQL);
            if (is_object($this->aDataSet)) {
                echo "<div class='error' style='display:block;'>Problem encountered when generating the report</div>";
                return false;
            }
            $this->aColumnsToSum = $aSums;
            $this->$aSums = array();
            $this->aSums = array_pad($this->$aSums, count($this->aDataSet[0]), 0);
        }
    }

    protected function getBookingsByMember() {
        if (!empty($this->aFilters[AdminPage::BOOKINGDATE])) {
            $sBookingDates = implode("','", $this->aFilters[AdminPage::BOOKINGDATE]);
            $sFilter = " AND DATE_FORMAT( b.booking_date, '%Y-%c-01' ) IN ('{$sBookingDates}')";
        }
        $sSQL = "SELECT
						YEAR(b.booking_date) as 'Year',
						MONTHNAME(b.booking_date) as 'Month',
						lc.comment as 'Lottery Name',
						CONCAT_WS(' ', m.firstname, m.lastname) as 'Member',
						COUNT(bi.bookingitem_id) as 'Tickets Count',
						SUM(bi.cost) as 'Tickets Cost'
					FROM booking_items bi
					INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
					INNER JOIN lotteries_cmn lc ON lc.lottery_id = bi.fk_lottery_id
					INNER JOIN members m ON m.member_id = b.fk_member_id
					WHERE 1
					{$sFilter}
					GROUP BY m.member_id, lc.lottery_id, YEAR(b.booking_date), MONTH(b.booking_date)
					ORDER BY  YEAR(b.booking_date) ASC, MONTH(b.booking_date) ASC, m.member_id ASC, lc.comment ASC";
        return $sSQL;
    }

    protected function getStatisticsToday() {
        $sSQL = "
				(
					SELECT
						'Members' as 'Title',
						COUNT(member_id) as 'Count',
						'' as 'Total'
					FROM members
					WHERE DATE(join_date) = CURDATE()
					GROUP BY DATE(join_date)
				)
				UNION
				(
					SELECT
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							DATE(t.transaction_date) = CURDATE()
						AND
							g.enabled = 0
						AND t.amount > 0
						AND t.confirmed = 1
					GROUP BY DATE(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							DATE(t.transaction_date) = CURDATE()
						AND
							g.enabled = 0
						AND t.amount < 0
						AND t.confirmed = 1
					GROUP BY DATE(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							DATE(t.transaction_date) = CURDATE()
						AND
							g.enabled = 1
						AND t.amount > 0
						AND t.confirmed = 1
					GROUP BY DATE(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							DATE(t.transaction_date) = CURDATE()
						AND
							g.enabled = 1
						AND t.amount < 0
						AND t.confirmed = 1
					GROUP BY DATE(t.transaction_date), g.gateway_id
				)
				ORDER BY Title ASC
		";
        return $sSQL;
    }

    protected function getStatisticsByMonth() {
        if (!empty($this->aFilters[AdminPage::BOOKINGDATE])) {
            $sBookingDates = implode("','", $this->aFilters[AdminPage::BOOKINGDATE]);
            $sMFilter = " AND DATE_FORMAT( join_date, '%Y-%c-01' ) IN ('{$sBookingDates}')";
            $sFilter = " AND DATE_FORMAT( t.transaction_date, '%Y-%c-01' ) IN ('{$sBookingDates}')";
        }
        $sSQL = "SELECT
				u.y as 'Year',
				u.monthname as 'Month',
				u.Title as 'Title',
				u.Count as 'Count',
				u.Total as 'Total'
				FROM
				( (
					SELECT
						MONTHNAME(join_date) as monthname,
						MONTH(join_date) as m,
						YEAR(join_date) as y,
						'Members' as 'Title',
						COUNT(member_id) as 'Count',
						'' as 'Total'
					FROM members
					WHERE 1
					{$sMFilter}
					GROUP BY YEAR(join_date), MONTH(join_date)
				)
				UNION
				(
					SELECT
						MONTHNAME(t.transaction_date) as monthname,
						MONTH(t.transaction_date) as m,
						YEAR(t.transaction_date) as y,
						'Active Members NEW' as 'Title',
						COUNT( DISTINCT m.member_id) as 'Count',
						'' as 'Total'
					FROM transactions t
					INNER JOIN members m ON m.member_id = t.fk_member_id
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE t.amount < 0
						AND t.confirmed = 1
						AND YEAR(m.join_date) = YEAR(t.transaction_date)
						AND MONTH(m.join_date) = MONTH(t.transaction_date)
						{$sFilter}
					GROUP BY YEAR(t.transaction_date), MONTH(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						MONTHNAME(t.transaction_date) as monthname,
						MONTH(t.transaction_date) as m,
						YEAR(t.transaction_date) as y,
						'Active Members' as 'Title',
						COUNT( DISTINCT m.member_id) as 'Count',
						'' as 'Total'
					FROM transactions t
					INNER JOIN members m ON m.member_id = t.fk_member_id
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE t.amount < 0
						AND t.confirmed = 1
						{$sFilter}
					GROUP BY YEAR(t.transaction_date), MONTH(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						MONTHNAME(t.transaction_date) as monthname,
						MONTH(t.transaction_date) as m,
						YEAR(t.transaction_date) as y,
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							g.enabled = 0
						AND t.amount > 0
						AND t.confirmed = 1
						{$sFilter}
					GROUP BY YEAR(t.transaction_date), MONTH(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						MONTHNAME(t.transaction_date) as monthname,
						MONTH(t.transaction_date) as m,
						YEAR(t.transaction_date) as y,
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							g.enabled = 0
						AND t.amount < 0
						AND t.confirmed = 1
						{$sFilter}
					GROUP BY YEAR(t.transaction_date), MONTH(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						MONTHNAME(t.transaction_date) as monthname,
						MONTH(t.transaction_date) as m,
						YEAR(t.transaction_date) as y,
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							g.enabled = 1
						AND t.amount > 0
						AND t.confirmed = 1
						{$sFilter}
					GROUP BY YEAR(t.transaction_date), MONTH(t.transaction_date), g.gateway_id
				)
				UNION
				(
					SELECT
						MONTHNAME(t.transaction_date) as monthname,
						MONTH(t.transaction_date) as m,
						YEAR(t.transaction_date) as y,
						g.comment as 'Title',
						COUNT(t.transaction_id) as 'Count',
						SUM(t.amount) as 'Total'
					FROM transactions t
					INNER JOIN gateways g ON g.gateway_id = t.fk_gateway_id
					WHERE
							g.enabled = 1
						AND t.amount < 0
						AND t.confirmed = 1
						{$sFilter}
					GROUP BY YEAR(t.transaction_date), MONTH(t.transaction_date), g.gateway_id
				) ) u
				ORDER BY u.y ASC, u.m ASC, u.Title ASC
		";
        return $sSQL;
    }

    protected function getTicketsNotPurchased() {
        $sSQL = "SELECT
					b.bookingreference as 'Booking Reference',
					lc.comment as 'Lottery Name',
					bi.fk_lotterydate as 'Lottery Date',
					REPLACE(SUBSTRING_INDEX(bi.numbers,'|',lc.number_count),'|', ' - ') as 'Numbers',
					REPLACE(SUBSTRING_INDEX(bi.numbers,'|',-lc.bonus_numbers),'|', ' - ') as 'Bonus',
					bi.cost as 'Price Paid'
				FROM booking_items bi
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = bi.fk_lottery_id
				INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
				WHERE bi.purchased = 0
				ORDER BY bi.fk_lotterydate ASC, lc.comment ASC, b.booking_date DESC";

        return $sSQL;
    }

    protected function getUnpurchasedLotteryTickets() {
        $sSQL = "SELECT
						lc.comment as 'Lottery Name',
						DATE_FORMAT(ct.fk_lotterydate, '%a %d %b %Y') as 'Draw Date',
						REPLACE(SUBSTRING_INDEX(ct.numbers,'|',lc.number_count),'|', ' - ') as 'Numbers',
						REPLACE(SUBSTRING_INDEX(ct.numbers,'|',-lc.bonus_numbers),'|', ' - ') as 'Bonus',
						IF(ct.purchased,'Purchased','Not Purchased') as 'Purchased'
					FROM booking_items ct
					INNER JOIN bookings b ON b.booking_id = ct.fk_booking_id
					INNER JOIN lotteries_cmn lc ON lc.lottery_id = ct.fk_lottery_id
					INNER JOIN r_lottery_dates rld ON rld.fk_lottery_id = lc.lottery_id AND rld.drawdate = ct.fk_lotterydate
					WHERE DATE(rld.drawdate) = CURDATE()
					ORDER BY ct.purchased ASC, ct.fk_lotterydate ASC, lc.comment ASC, ct.bookingitem_id ASC";
        return $sSQL;
    }

    protected function getBookingsYesterday() {
        $sSQL = "SELECT
						lc.comment as 'Lottery Name',
						DATE_FORMAT(ct.fk_lotterydate, '%a %d %b %Y') as 'Draw Date',
						REPLACE(ct.numbers,'|', ' - ') as 'Numbers',
						IF(ct.purchased,'Purchased','Not Purchased') as 'Purchased'
					FROM booking_items ct
					INNER JOIN bookings b ON b.booking_id = ct.fk_booking_id
					INNER JOIN lotteries_cmn lc ON lc.lottery_id = ct.fk_lottery_id
					WHERE DATE(b.booking_date) = ( CURDATE() - INTERVAL 1 DAY )
					ORDER BY ct.fk_lotterydate ASC, lc.comment ASC, ct.bookingitem_id ASC";
        return $sSQL;
    }

    protected function getBookingsToday() {
        $sSQL = "SELECT
						lc.comment as 'Lottery Name',
						DATE_FORMAT(ct.fk_lotterydate, '%a %d %b %Y') as 'Draw Date',
						REPLACE(ct.numbers,'|', ' - ') as 'Numbers',
						IF(ct.purchased,'Purchased','Not Purchased') as 'Purchased'
					FROM booking_items ct
					INNER JOIN bookings b ON b.booking_id = ct.fk_booking_id
					INNER JOIN lotteries_cmn lc ON lc.lottery_id = ct.fk_lottery_id
					WHERE DATE(b.booking_date) = CURDATE()
					ORDER BY ct.fk_lotterydate ASC, lc.comment ASC, ct.bookingitem_id ASC";
        return $sSQL;
    }

    protected function getAffiliateBookings() {
        $iAdminType = $this->oSecurityObject->getiAdminType();
        if ($iAdminType == SecurityAdmin::ADMIN) {
            if (!empty($this->aFilters[AdminPage::USER])) {
                $iAffiliateID = implode(",", $this->aFilters[AdminPage::USER]);
                $iAdminType = SecurityAdmin::AFFILIATE;
            }
        } else {
            $iAffiliateID = $this->oSecurityObject->getUserID();
        }
        $sSQL = "SELECT ";
        if (empty($iAffiliateID) || $iAdminType != SecurityAdmin::AFFILIATE) {
            $sSQL .= "CONCAT_WS(' ', au.firstname, au.lastname) AS Affiliate, af.web as URL, ";
        }
        $sSQL .= "
			DATE_FORMAT(b.booking_date, '%D %M %Y') as 'Purchase Date',
			lc.comment as Lottery,
			b.promo as 'Promo Code',
			DATE_FORMAT(bi.fk_lotterydate, '%D %M %Y') as 'Lottery Date',
			COUNT(bi.bookingitem_id) as 'Tickets Count',
			FORMAT(SUM(bi.cost),2) as Total,
			FORMAT(SUM(bi.cost)*af.commission,2) as 'Commission'
			FROM booking_items bi
			INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
			INNER JOIN admin_users au ON au.user_id = b.fk_affiliate_id
			INNER JOIN affiliates af ON af.fk_user_id = au.user_id
			INNER JOIN lotteries_cmn lc ON lc.lottery_id = bi.fk_lottery_id
			WHERE b.fk_affiliate_id <> 0 ";

        if (!empty($this->aFilters[AdminPage::BOOKINGDATE])) {
            $sBookingDates = implode("','", $this->aFilters[AdminPage::BOOKINGDATE]);
            $sSQL .= " AND DATE_FORMAT( b.booking_date, '%Y-%c-01' ) IN ('{$sBookingDates}')";
        }

        if (!empty($iAffiliateID) && $iAdminType == SecurityAdmin::AFFILIATE) {
            $sSQL .= " AND b.fk_affiliate_id IN ( {$iAffiliateID} ) ";
        }

        if (!empty($this->aFilters['date-from']) && !empty($this->aFilters['date-to'])) {
            $sSQL .= " AND b.booking_date BETWEEN '{$this->aFilters['date-from']}' AND '{$this->aFilters['date-to']}' ";
        }

        if (!is_null($this->aFilters['lottery_id']) && $this->aFilters['lottery_id'] !== "") {
            $sSQL .= " AND bi.fk_lottery_id = '{$this->aFilters['lottery_id']}'";
        }

        if (!is_null($this->aFilters['min-amount']) && $this->aFilters['min-amount'] !== "") {
            $sSQL .= " AND Total > {$this->aFilters['min-amount']} ";
        }

        if (!is_null($this->aFilters['max-amount']) && $this->aFilters['max-amount'] !== "") {
            $sSQL .= " AND Total < {$this->aFilters['max-amount']} ";
        }

        $sSQL .= "
				GROUP BY bi.fk_lotterydate, lc.lottery_id, DATE_FORMAT(b.booking_date, '%Y-%m-%d'), b.promo
				ORDER BY b.booking_date DESC, au.username ASC";
        return $sSQL;
    }

    protected function getAffiliateBookingsByMonth() {
        $iAdminType = $this->oSecurityObject->getiAdminType();
        if ($iAdminType == SecurityAdmin::ADMIN) {
            if (!empty($this->aFilters[AdminPage::USER])) {
                $iAffiliateID = implode(",", $this->aFilters[AdminPage::USER]);
                $iAdminType = SecurityAdmin::AFFILIATE;
            }
        } else {
            $iAffiliateID = $this->oSecurityObject->getUserID();
        }
        $sSQL = "SELECT ";
        if (empty($iAffiliateID) || $iAdminType != SecurityAdmin::AFFILIATE) {
            $sSQL .= "CONCAT_WS(' ', au.firstname, au.lastname) AS Affiliate, af.web as URL, ";
        }
        $sSQL .= "
			YEAR(b.booking_date) as 'Year',
			MONTHNAME(b.booking_date) as 'Month',
			lc.comment as Lottery,
			b.promo as 'Promo Code',
			COUNT(bi.bookingitem_id) as 'Tickets Count',
			FORMAT(SUM(bi.cost),2) as Total,
			FORMAT(SUM(bi.cost)*af.commission,2) as 'Commission'
			FROM booking_items bi
			INNER JOIN bookings b ON b.booking_id = bi.fk_booking_id
			INNER JOIN admin_users au ON au.user_id = b.fk_affiliate_id
			INNER JOIN affiliates af ON af.fk_user_id = au.user_id
			INNER JOIN lotteries_cmn lc ON lc.lottery_id = bi.fk_lottery_id
			WHERE b.fk_affiliate_id <> 0 ";

        if (!empty($this->aFilters[AdminPage::BOOKINGDATE])) {
            $sBookingDates = implode("','", $this->aFilters[AdminPage::BOOKINGDATE]);
            $sSQL .= " AND DATE_FORMAT( b.booking_date, '%Y-%c-01' ) IN ('{$sBookingDates}')";
        }

        if (!empty($iAffiliateID) && $iAdminType == SecurityAdmin::AFFILIATE) {
            $sSQL .= " AND b.fk_affiliate_id IN ( {$iAffiliateID} ) ";
        }

        if (!empty($this->aFilters['date-from']) && !empty($this->aFilters['date-to'])) {
            $sSQL .= "AND b.booking_date BETWEEN '{$this->aFilters['date-from']}' AND '{$this->aFilters['date-to']}'";
        }

        $sSQL .= "
				GROUP BY YEAR(b.booking_date), MONTH(b.booking_date), au.user_id, lc.lottery_id, b.promo
				ORDER BY b.booking_date DESC, au.username ASC, b.promo ASC, lc.comment ASC";
        return $sSQL;
    }
    
    
    protected function getTransactionsPending($iGatewayId, $iStatusId=2)
    {
        $iGatewayId = (int) $iGatewayId;
        $iStatusId = (int) $iStatusId;
        
        if($_GET['lottery']) $this->iLotteryId=$_GET['lottery'];
        if($_GET['d']) $this->dDrawDate=$_GET['d'];
        if($_GET['m']) $this->iMinAmount=$_GET['m'];
        if($_GET['x']) $this->iMaxAmount=$_GET['x'];
        
        $sSQL = "SELECT
                    t.*,
                    DATE_FORMAT(t.transaction_date, '%a %d %b %Y') as 'Transaction Date',
                    c.symbol, m.email";
        if($this->iLotteryId!==null || $this->dDrawDate){
            $sSQL.=' ,w.checked,w.ignore,b.bookingitem_id ';
        }
        $sSQL.= " FROM transactions t
                    INNER JOIN currencies c ON c.currency_id = t.fk_currency_id
                    INNER JOIN members m ON m.member_id=t.fk_member_id";

        if($this->iLotteryId!==null || $this->dDrawDate){
            $sSQL.=' INNER JOIN lottery_winnings w ON w.fk_transaction_id=t.transaction_id INNER JOIN booking_items b ON w.fk_bookingitem_id=b.bookingitem_id ';
        }
        
        
        $sSQL.="    WHERE t.fk_gateway_id = {$iGatewayId}
                        AND t.fk_status_id = {$iStatusId}";
                        
        if($this->iLotteryId!==null){
            $sSQL.=' AND w.fk_lottery_id='.$this->iLotteryId;
        }
        if($this->dDrawDate!==null){
            $sSQL.=' AND b.fk_lotterydate=\''.$this->dDrawDate."'";
        }
        if($this->iMinAmount!==null){
            $sSQL.=' AND t.amount >='.$this->iMinAmount;
        }
        if($this->iMaxAmount!==null){
            $sSQL.=' AND t.amount < '.$this->iMaxAmount;
        }
                        
        $sSQL.=" ORDER BY t.transaction_date ASC";
        return $sSQL;       
    }

    protected function getAffiliateAdvertStastics() {
        $AffiliateAdverts = new AffiliateAdverts();
        $advertClicksAndImpressions = $AffiliateAdverts->getClicksAndImpressions();

        return $advertClicksAndImpressions;
    }
    
    
    public function generateCSV() {
        $sFile = "";
        if (is_array($this->aDataSet)) {
            $sFile = implode(",", array_keys($this->aDataSet[0])) . "\r\n";
            foreach ($this->aDataSet as $RowKey => $Row) {
                $sFile .= implode(",", $Row) . "\r\n";
            }
        }
        return $sFile;
    }

    public function display($sMedia = NULL) {

        $sType = $this->sType;
        if (!empty($this->aDataSet) && is_array($this->aDataSet)) {
            $sOutput .= $this->sStartTable;
            $rowindex = 1;
            $i = 0;
            foreach ($this->aDataSet as $RowKey => $Row) {

                $i++;
                if (!$bShownHeader) {
                    //PRINT HEADER

                    $sOutput .= $this->sStartRow;
                    foreach ($Row as $Key => $Column) {
                        $sOutput .= "<th>";
                        $sOutput .= $Key;
                        $sOutput .= "</th>";
                    }
                    $sOutput .= $this->sEndRow;
                    $bShownHeader = 1;
                }

                //FILL ARRAY WITH DUMMY DATA
                $sOutput .= $this->sStartRow;

                //PRINT DATA
                foreach ($Row as $Key => $Column) {
                    $sOutput .= $this->sStartColumn;
                    $sOutput .= $Column;
                    $sOutput .= $this->sEndColumn;
                }
                $sOutput .= $this->sEndRow;

                $index = 0;
                foreach ($Row as $Key => $Column) {

                    //NOW ADD SUMS
                    foreach ($this->aColumnsToSum as $ColumnTitle) {
                        if ($Key == $ColumnTitle) {
                            $subtoal = str_replace(',', '', $Column);
                            $this->aSums[$index] += $subtoal;
                        }
                    }
                    $index++;
                }

                //PRINT FOOTER
                if (count($this->aDataSet) == $rowindex && !empty($this->aColumnsToSum)) {

                    $sOutput .= $this->sStartFooterRow;
                    $index = 0;
                    foreach ($this->aSums as $value) {
                        $sOutput .= $this->sStartFooterColumn;
                        if (!empty($this->aSums[$index])) {
                            if (gettype($this->aSums[$index]) == "double") {
                                $sOutput .= "<strong>" . number_format_locale($this->aSums[$index], 2) . "</strong>";
                            } else {
                                $sOutput .= "<strong>" . $this->aSums[$index] . "</strong>";
                            }
                        }
                        $sOutput .= $this->sEndColumn;
                        $index++;
                    }
                    $sOutput .= $this->sEndRow;
                }
                $rowindex++;
            }
            $sOutput .= $this->sEndTable;
        } else {
            $sOutput .= "<div class='reportempty'>No Data for Report</div>";
        }
        if ($sMedia == "Email") {
            return $sOutput;
        } else {
            echo $sOutput;
        }
    }
    
    public function getDataset()
    {
        return $this->aDataSet;
    }
// end of function
}
