<?php

/**
 * EmailAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class EmailAdmin extends Email {

    public static function sendMailshot($sEmail, $sFirstName, $sLastName, $sSubject, $sBody) {
        $oEmail = new Mailer();
        $oEmail->setSubject($sSubject);
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo($sEmail, $sFirstName . " " . $sLastName);
        return $oEmail->sendEmail(nl2br("Dear {$sFirstName} {$sLastName},

			{$sBody}"));
    }

    /*public static function sendLotteryLostEmail($sFirstName, $sLastName, $sEmail, $sLotteryTitle, $bDrawDate) {
        $aJackpotInfo = Email::getNextLotteryJackpot();
        $aJackpots = Email::getNextLottos(9);
        $sPlayText = "<table><tr><th style='text-align:left;padding-right:10px;' width='200px'>Lotto</th><th style='text-align:left;padding-right:10px;' width='170px'>Draw Date</th><th style='text-align:left;padding-right:30px;'>Jackpot</th><th style='text-align:left;padding-right:10px;'>Play</th></tr>";
        foreach ($aJackpots as $aJackpot) {
            $sPlayText .= "<tr><td>" . $aJackpot['title'] . "</td><td>" . date("D jS F", strtotime($aJackpot['date'])) . "</td><td>" . $aJackpot['jackpot'] . "</td><td><a href='http://www.maxlotto.com/play/'>PLAY NOW</a></td></tr>";
        }

        $sPlayText .= nl2br("</table><br/>");

        $sBody = nl2br("Dear {$sFirstName} {$sLastName},

		 <b>{$sLotteryTitle} Draw: " . date("l jS M Y", strtotime($bDrawDate)) . "</b>

			Thank you for purchasing your lottery tickets with LoveLotto.com. Unfortunately your numbers did not carry a winning combination. We wish you better luck next time.

			You can <a href='http://www.maxlotto.com/'>login to your account</a> to view your ticket details.

			<h4>What can I do now?</h4>Have you bought your tickets for this " . date("l", strtotime($aJackpotInfo['date'])) . "'s " . $aJackpotInfo['title'] . " Jackpot Draw? Don't miss out on " . (($aJackpotInfo['rollover']) ? "this massive " : "") . str_replace(".0m", "m", $aJackpotInfo['jackpot']) . " jackpot!

			Feeling lucky? Why not become a millionaire on one of our other lottos.

			{$sPlayText}");
        $oEmail = new Mailer();
        $oEmail->setSubject("Your {$sLotteryTitle} Order");
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo($sEmail, $sFirstName . " " . $sLastName);

        return $oEmail->sendEmail($sBody);
    }*/

    public static function sendLotteryWonEmail($sFirstName, $sLastName, $sEmail, $aNumbers, $fTotal, $dDrawDate, $sLotteryTitle, $sWinningTitle, $fCost, $sBookingReference) {
        $aJackpotInfo = Email::getNextLotteryJackpot();
        $aJackpots = Email::getNextLottos(9);
        $sPlayText = "<table><tr><th style='text-align:left;padding-right:10px;' width='200px'>Lotto</th><th style='text-align:left;padding-right:10px;' width='170px'>Draw Date</th><th style='text-align:left;padding-right:30px;'>Jackpot</th><th style='text-align:left;padding-right:10px;'>Play</th></tr>";
        foreach ($aJackpots as $aJackpot) {
            $sPlayText .= "<tr><td>" . $aJackpot['title'] . "</td><td>" . date("D jS F", strtotime($aJackpot['date'])) . "</td><td>" . str_replace(".0m", "m", $aJackpot['jackpot']) . "</td><td><a href='http://www.maxlotto.com/play/'>PLAY NOW</a></td></tr>";
        }

        $sPlayText .= nl2br("</table><br/>");

        $sBody = nl2br("Dear {$sFirstName} {$sLastName},

			Congratulations, you just won " . $fTotal . " by matching {$sWinningTitle} on the {$sLotteryTitle}!

			<h4>Winning Summary</h4><table cellpadding='5px'><tr><th style='text-align:left;padding-right:10px;'>Draw Date</th><th style='text-align:left;padding-right:10px;'>Numbers</th><th style='text-align:left;padding-right:10px;'>Winning</th><th style='text-align:left;padding-right:10px;'>Prize</th></tr>
			<tr><td>" . date("d/m/Y", strtotime($dDrawDate)) . "</td><td>" . str_replace("|", " - ", $aNumbers) . "</td><td>" . $sWinningTitle . "</td><td>" . $fTotal . "</td></tr></table>

			<h4>What happens next?</h4>Winnings under &euro;1000 will be transferred to your account automatically within 2 working days. If you have won over &euro;1000 a LoveLotto.com Customer Service Agent will be calling you to organise payment. It's a good idea to ensure your telephone number is up to date, please login and check your account details!

			<h4>What can I do now?</h4>Have you bought your tickets for this " . date("l", strtotime($aJackpotInfo['date'])) . "'s " . $aJackpotInfo['title'] . " Jackpot Draw? Don't miss out on " . (($aJackpotInfo['rollover']) ? "this massive " : "") . str_replace(".0m", "m", $aJackpotInfo['jackpot']) . " jackpot!

			Feeling lucky? Why not become a millionaire on one of our other lottos.

			{$sPlayText}");
        $oEmail = new Mailer();
        $oEmail->setSubject("Congratulations you won " . $fTotal);
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo($sEmail, $sFirstName . " " . $sLastName);
        $oEmail->addBCC("payments@loveraffles.com");
        return $oEmail->sendEmail($sBody);
    }

    public static function sendLotteryWonReceiptEmail($sFirstName, $sLastName, $sEmail, $aNumbers, $fTotal, $dDrawDate, $sLotteryTitle, $sWinningTitle, $fCost, $sBookingReference, $sTransactionReference, $dTransactionDate, $fAmountTransferred) {
        $aJackpotInfo = Email::getNextLotteryJackpot();
        $aJackpots = Email::getNextLottos(9);
        $sPlayText = "<table><tr><th style='text-align:left;padding-right:10px;' width='200px'>Lotto</th><th style='text-align:left;padding-right:10px;' width='170px'>Draw Date</th><th style='text-align:left;padding-right:30px;'>Jackpot</th><th style='text-align:left;padding-right:10px;'>Play</th></tr>";
        foreach ($aJackpots as $aJackpot) {
            $sPlayText .= "<tr><td>" . $aJackpot['title'] . "</td><td>" . date("D jS F", strtotime($aJackpot['date'])) . "</td><td>" . str_replace(".0m", "m", $aJackpot['jackpot']) . "</td><td><a href='http://www.maxlotto.com/play/'>PLAY NOW</a></td></tr>";
        }
        $sPlayText .= nl2br("</table><br/>");

        $sBody = nl2br("Dear {$sFirstName} {$sLastName},

			Congratulations, you just won " . $fTotal . " by matching {$sWinningTitle} on the {$sLotteryTitle}!

			<h4>Winning Summary</h4><table cellpadding='5px'><tr><th style='text-align:left;padding-right:10px;'>Draw Date</th><th style='text-align:left;padding-right:10px;'>Numbers</th><th style='text-align:left;padding-right:10px;'>Winning</th><th style='text-align:left;padding-right:10px;'>Prize</th></tr>
			<tr><td>" . date("d/m/Y", strtotime($dDrawDate)) . "</td><td>" . str_replace("|", " - ", $aNumbers) . "</td><td>" . $sWinningTitle . "</td><td>" . $fTotal . "</td></tr></table>

			<h4>Transaction Details</h4>Your winnings have been transferred to your account.
			<table cellpadding='5px'><tr><th style='text-align:left;padding-right:10px;'>Transaction Reference</th><th style='text-align:left;padding-right:10px;'>Transaction Date</th><th style='text-align:left;padding-right:10px;'>Transaction Amount</th></tr>
			<tr><td>" . $sTransactionReference . "</td><td>" . date("d/m/Y", strtotime($dTransactionDate)) . "</td><td>&euro;" . number_format_locale($fAmountTransferred, 2, '.', ',') . "</td></tr></table>

			<h4>What can I do now?</h4>Have you bought your tickets for this " . date("l", strtotime($aJackpotInfo['date'])) . "'s " . $aJackpotInfo['title'] . " Jackpot Draw? Don't miss out on " . (($aJackpotInfo['rollover']) ? "this massive " : "") . str_replace(".0m", "m", $aJackpotInfo['jackpot']) . " jackpot!

			Feeling lucky? Why not become a millionaire on one of our other lottos.

			{$sPlayText}");
        $oEmail = new Mailer();
        $oEmail->setSubject("Congratulations you won " . $fTotal);
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo($sEmail, $sFirstName . " " . $sLastName);
        $oEmail->addBCC("payments@loveraffles.com");
        return $oEmail->sendEmail($sBody);
    }

// =========================================================================== //
// All emails below this point are part of the checking system
// =========================================================================== //
    // Send email to checker
    public static function sendEmailToChecker($iCheckerID, $aCheckerInfo, $iLotteryID, $iDraw, $sDrawType, $dDrawDate, $aLiveDrawInformation)
    {

        $sSubject = "Hi {$aCheckerInfo['first_name']}, you're on checking duty for the next {$sDrawType} draw!";

        $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($dDrawDate));

        $sLink = "http://{$_SERVER['HTTP_HOST']}/administration/add-results-checker.php?" .
                 "sh={$aCheckerInfo['security_hash']}&l={$iLotteryID}&c={$iCheckerID}&d={$iDraw}";

        $sCancelLink = "http://{$_SERVER['HTTP_HOST']}/administration/lottery-checker-cancel.php?" .
                 "sh={$aCheckerInfo['security_hash']}&l={$iLotteryID}&c={$iCheckerID}&d={$iDraw}";

        $sBody = <<<EOT
Hi {$aCheckerInfo['first_name']},

You have been chosen to be one of the result checkers for the next draw of the {$sDrawType}.

This will be taking place on {$sDrawDateString}.

This will involve entering the drawn numbers and winning prizes/amounts for that particular draw once it has taken place.

At the bottom of this email you will find information on where to view the draw as it takes place and where to find prize breakdown information.

Your unique link to access the draw entry screen is here:
<a href="{$sLink}">{$sLink}</a>

Please ensure you only use this link to access the screen and not any other links you may have used in the past.

If you know you will be unable to perform this check, please click here <a href="{$sCancelLink}">{$sCancelLink}</a> to let us know.

Thanks!

=========================================================================

INFORMATION ON HOW/VIEW TO VIEW THIS LOTTERY DRAW:

- Official Website: {$aLiveDrawInformation['info_official_website']}
- Live draw time: {$aLiveDrawInformation['info_live_draw_times']}
- View the draw online LIVE at: {$aLiveDrawInformation['info_live_draw_location']}
- Prize breakdown information available from: {$aLiveDrawInformation['info_prize_breakdown_times']}
- View prize breakdown information at: {$aLiveDrawInformation['info_prize_breakdown_location']}


EOT;
        $sBody = nl2br($sBody);


        $oEmail = new Mailer();
        $oEmail->setSubject($sSubject);
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo($aCheckerInfo['email_address'], $aCheckerInfo['first_name']);
        $oEmail->sendEmail($sBody);

//        echo($sSubject . "<br/>");
//        echo($sBody);
    }

        // Send email to checker
    public static function sendEmailToCheckerAgain($iCheckerID, $aCheckerInfo, $iLotteryID, $iDraw, $sDrawType, $dDrawDate)
    {

        $sSubject = "Hi {$aCheckerInfo['first_name']}, you need to re-enter your results for the {$sDrawType} draw!";

        $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($dDrawDate));

        $sLink = "http://{$_SERVER['HTTP_HOST']}/administration/add-results-checker.php?" .
                 "sh={$aCheckerInfo['security_hash']}&l={$iLotteryID}&c={$iCheckerID}&d={$iDraw}";

        $sCancelLink = "http://{$_SERVER['HTTP_HOST']}/administration/lottery-checker-cancel.php?" .
                 "sh={$aCheckerInfo['security_hash']}&l={$iLotteryID}&c={$iCheckerID}&d={$iDraw}";

        $sBody = <<<EOT
Hi {$aCheckerInfo['first_name']},

Unfortunately there has been a problem with the last set of results you entered for the {$sDrawType} draw, on {$sDrawDateString}.

Please revisit the entry screen via the link below and re-enter the results.
<a href="{$sLink}">{$sLink}</a>

Please ensure you only use this link to access the screen and not any other links you may have used in the past.

If you know you will be unable to perform this check, please click here <a href="{$sCancelLink}">{$sCancelLink}</a> to let us know.

Thanks!

EOT;
        $sBody = nl2br($sBody);


        $oEmail = new Mailer();
        $oEmail->setSubject($sSubject);
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo($aCheckerInfo['email_address'], $aCheckerInfo['first_name']);
        $oEmail->sendEmail($sBody);

//        echo($sSubject . "<br/>");
//        echo($sBody);
    }

/****** EVERYTHING BELOW THIS LINE IS ADMINISTRATION EMAIL ********/

    public static function sendEmailToAdministrator($aEmailData, $sEmailToSend)
    {
        $sSubject = "";
        $sBody = "";

        switch ($sEmailToSend)
        {
            case 'checkers_generated' :
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Lottery checkers for next {$aEmailData['lottery_name']} draw chosen";
                $sBody = <<<EOT
This is to inform you that the lottery checkers for the next {$aEmailData['lottery_name']} draw on {$sDrawDateString} have been chosen:

EOT;
                $count = 1;
                foreach ($aEmailData['checkers'] as $aCheckerInfo) {
                    $sBody .= "Checker " . $count++ . ": " . $aCheckerInfo['first_name'] .
                                  " (email " .  $aCheckerInfo['email_address'] . ")\r\n";
                }
                $sBody .= <<<EOT
These people have been emailed with their unique link to access the lottery entry screen.

EOT;
                break;

            case "checker_cancelled" :
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "{$aEmailData['old_checker_name']} has cancelled lottery checker for next {$aEmailData['lottery_name']} draw";
                $sBody = <<<EOT
This is to inform you that {$aEmailData['old_checker_name']} has cancelled their check for the {$aEmailData['lottery_name']}  draw on {$sDrawDateString}.

A new checker has been chosen and they have been emailed their unique link to access the lottery entry screen.

The new checker is {$aEmailData['new_checker_name']} (email: {$aEmailData['new_checker_email']})

EOT;
                break;

            case "checker_results_entered" :

                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "{$aEmailData['data_entered']['first_name']} entered {$aEmailData['lottery_name']} data";
                $sBody = <<<EOT
This is to inform you that data for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has been entered by {$aEmailData['data_entered']['first_name']}.
The data has been entered as follows:

Draw numbers: {$aEmailData['data_entered']['numbers']}

Winnings:

EOT;
                $aWinnings = unserialize($aEmailData['data_entered']['winnings']);

                    // Loop through draw data here
                foreach ($aWinnings as $aData)
                {
                    // Create a prize level key based on match and bonus
                    $sPrizeLevel = $aData['match'] . "+" . $aData['bonus'];

                    //  Copy retrieved winners and prize for this level onto data check array
                    $iWinners = $aData['winners'];
                    $iPrize = $aData['prize'];

                    $sBody .= "Prize level: $sPrizeLevel"."\t".
                                  "Winners: $iWinners"."\t".
                                  "Prize: $iPrize" . "\r\n";
                }

                $sBody.= <<<EOT

Next jackpot {$aEmailData['data_entered']['next_jackpot_amount']}.
Rollover? {$aEmailData['data_entered']['next_jackpot_rollover']}

EOT;
                break;

            case "all_results_checked" :

                 $sLink = "http://{$_SERVER['HTTP_HOST']}/administration/add-results-checker-all.php?" .
                 "l={$aEmailData['lottery_id']}&d={$aEmailData['draw_ref']}";

                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Data for {$aEmailData['lottery_name']} on {$sDrawDateString} has been checked";

                if ($aEmailData['check_status']=='ok')
                {
                    $sDrawNumbers = implode("|", $aEmailData['data_entered']['numbers']);

                    $sSubject .= " - NO MISMATCHES";
                    $sBody = <<<EOT
This is to inform you that the supplied data for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has been checked.

NO mismatches have been found and the data has been stored in the main results tables.

You can view the checked data at the following URL:
{$sLink}

For your convenience, the data for this lottery is as follows:

Draw Numbers: {$sDrawNumbers}

Winnings:

EOT;
                    foreach ($aEmailData['data_entered']['winnings'] as $aData)
                    {
                        // Create a prize level key based on match and bonus
                        $sPrizeLevel = $aData['match'] . "+" . $aData['bonus'];

                        //  Copy retrieved winners and prize for this level onto data check array
                        $iWinners = $aData['winners'];
                        $iPrize = $aData['prize'];

                        $sBody .= "Prize level: $sPrizeLevel"."\t".
                                      "Winners: $iWinners"."\t".
                                      "Prize: $iPrize" . "\r\n";
                    }
                    $sBody.= <<<EOT

Next jackpot {$aEmailData['data_entered']['next_jackpot_amount']}.
Rollover? {$aEmailData['data_entered']['next_jackpot_rollover']}

EOT;
                }
                else
                {
                    $sSubject .= " - MISMATCHES FOUND";
                    $sBody = <<<EOT
This is to inform you that the supplied data for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has been checked.

Unfortunately, mismatches have been found so the data has not been stored.

You can view the current state of the data at the following URL:
{$sLink}

The three original checkers have been emailed again with requests to re-enter the results.

EOT;
                }
                break;

            case 'winscan_started' :
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Winscan started for {$aEmailData['lottery_name']}";

                $sBody = <<<EOT
This is to inform you that the Winscan for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has been started.
EOT;

            break;

            case 'winscan_completed_no_winners' :
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Winscan completed for {$aEmailData['lottery_name']}";

                $sBody = <<<EOT
This is to inform you that the Winscan for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has been completed.

No winners have been found for this draw.
EOT;

            break;

            case 'winscan_win_above_ind_threshold':
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Winscan for {$aEmailData['lottery_name']} - payout above individual threshold";

                $sBody = <<<EOT
This is to inform you that the Winscan for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has found
a payout on a winning ticket which exceeds the individual payout limit for this lottery.

Customer ID: {$aEmailData['customer_id']}
Booking Item ID: {$aEmailData['bookingitem_id']}
Winning combination: {$aEmailData['win_combination']}
Win amount: {$aEmailData['win_amount']}
Threshold: {$aEmailData['threshold']}

The payout transaction has been recorded in the database but has not yet been confirmed.
EOT;
            break;

            case 'winscan_win_above_draw_threshold':
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Winscan for {$aEmailData['lottery_name']} - total payouts above draw threshold";

                $sBody = <<<EOT
This is to inform you that the total payouts for the Winscan for the {$aEmailData['lottery_name']} draw on {$sDrawDateString}
exceeds the payout limit for this lottery.

Win amount: {$aEmailData['win_amount_total']}
Threshold: {$aEmailData['threshold']}

The payout transactions have been recorded in the database but have not yet been confirmed.
EOT;
            break;

            case 'winscan_completed_transactions_confirmed' :
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "Winscan completed for {$aEmailData['lottery_name']}";

                $sBody = <<<EOT
This is to inform you that the Winscan for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has been completed.

The transactions that were found have been confirmed.
EOT;

            break;

            case 'no_winscan' :
                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aEmailData['draw_date']));

                $sSubject = "No Winscan completed for {$aEmailData['lottery_name']}";

                $sBody = <<<EOT
This is to inform you that, because there is currently no winners data available,
the Winscan for the {$aEmailData['lottery_name']} draw on {$sDrawDateString} has not yet been done.

Once all winners data has been added and confirmed correct, the winscan will be performed

EOT;
        }

        $sBody = nl2br($sBody);

        $oEmail = new Mailer();
        $oEmail->setSubject($sSubject);
        $oEmail->setFrom("no-reply@maxlotto.com", WEBSITETITLE);
        $oEmail->addTo('jon@network649.com', 'Administrators');
        $oEmail->sendEmail($sBody);

//        echo($sSubject . "<br/>");
//        echo($sBody);
    }
}