<?php

/**
 * AdminPage DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class AdminPage {

    public $iID;
    public $sTitle;
    public $sFilename;
    public $bWebsiteFilter;
    public $bLanguageFilter;
    public $aFilters;
    public $sTablename;

    const COLUMN_LIMIT = 50;
    //Filters as per DB
    const WEBSITE = 0;
    const LANGUAGE = 1;
    const AUTHOR = 2;
    const LOTTERY = 5;
    const PRODUCTCATEGORY = 6;
    const PRODUCTRANKING = 7;
    const COUNTRY = 8;
    const REGION = 9;
    const BOOKINGSTATUS = 10;
    const BOOKINGDATE = 11;
    const ARRIVALDATE = 12;
    const FACILITYTYPE = 13;
    const EVENTCATEGORY = 14;
    const SINGLEWEBSITE = 16;
    const SECTION = 17;
    const MULTIMEDIATYPE = 18;
    const CITY = 19;
    const USERTYPE = 20;
    const USER = 21;
    const PRODUCT = 23;
    const BOOKINGREFERENCE = 24;
    const CLIENTNAME = 25;

    public function __construct($sFilename, $sTablename = "") {
        $this->setPageVars($sFilename, $sTablename);
    }

    public function __call($sMethod, $aArguments) {
        $prefix = substr($sMethod, 0, 3);
        $property = substr($sMethod, 3);
        if (empty($prefix) || empty($property)) {
            return false;
        }

        if ($prefix == "get" && isset($this->$property)) {
            return $this->$property;
        }
        if ($prefix == "set" && isset($this->$property)) {
            $this->$property = $aArguments[0];
            return true;
        }
        return false;
    }

    public function getPageID() {
        return $this->iID;
    }

    protected function setPageVars($sFileName, $sTablename = "") {
        $this->sFilename = $sFileName;
        $this->sTablename = $sTablename;
        $currentPage = AdminPage::getAdminPage($sFileName, $sTablename);
        if ($currentPage) {
            $this->iID = $currentPage["id"];
            $this->sTitle = $currentPage["title"];
            $this->bLanguageFilter = $currentPage["hasLanguageFilter"];
        }
    }

    private static function getAdminPage($sFileName, $sTablename) {
        $sql = "SELECT ap.id, ap.title, ap.filename, ap.tablename, ap.hasLanguageFilter
				FROM admin_pages ap
				WHERE LOWER(ap.filename) = LOWER('{$sFileName}')
				AND LOWER(ap.tablename) = LOWER('{$sTablename}')";
        return DAL::executeGetRow($sql);
    }

    public static function getAdminPages($iUserID = 0, $iParentID = 0) {
        $sWhere = " WHERE ap.parent_id = '{$iParentID}'";
        $sql = "SELECT ap.id, ap.title, ap.filename, ap.tablename, ap.hasLanguageFilter
				FROM admin_pages ap ";
        if ($iUserID) {
            $sql .= " LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '{$iUserID}' AND aupb.fk_user_id IS NULL ";
        }

        $sql .= $sWhere;
        $sql .= " ORDER BY ap.title asc";
        return DAL::executeQuery($sql);
    }

    public static function getAdminPagesForBlacklist($iUserID = 0, $iParentID = 0) {
        $sWhere = " WHERE ap.parent_id = '{$iParentID}'";
        $sql = "SELECT ap.id, ap.title, ap.filename, ap.tablename, ap.hasLanguageFilter, aupb.fk_user_id as is_blacklisted
				FROM admin_pages ap ";
        if ($iUserID) {
            $sql .= " LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '{$iUserID}' ";
        }

        $sql .= $sWhere;
        $sql .= " ORDER BY ap.title asc";
        return DAL::executeQuery($sql);
    }

    public function getFilters() {
        if (!$this->aFilters) {
            $this->aFilters = AdminPage::getPageFilters($this->iID);
        }
        return $this->aFilters;
    }

    public static function getPages($aPageIDs, $iUserID, $iLimit = NULL) {
        $aPages = array();
        if (is_array($aPageIDs) && !empty($aPageIDs)) {
            $sSQL = "SELECT ap.title, ap.filename, ap.tablename
					FROM admin_pages ap
					LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '" . $iUserID . "'
					WHERE aupb.fk_user_id IS NULL
						AND ap.id IN ( " . implode(", ", $aPageIDs) . " )
					ORDER BY FIELD(ap.id, " . implode(", ", $aPageIDs) . " )";
            if ($iLimit) {
                $sSQL .= " LIMIT {$iLimit}";
            }
            $aPages = DAL::executeQuery($sSQL);
        }
        return $aPages;
    }

    private static function getPageFilters($iPageID) {
        $sql = "SELECT af.id, af.title, af.filename
				FROM admin_r_pages_filters apf
				LEFT JOIN admin_pages ap ON apf.page_id = ap.id
				LEFT JOIN admin_filters af ON apf.filter_id = af.id
				WHERE ap.id = '{$iPageID}'
				ORDER BY af.rank ASC, af.id ASC
				";
        return DAL::executeQuery($sql);
    }

    public static function getAllPageFilters($iPageID) {
        $sql = "SELECT af.id, af.title, af.filename, apf.page_id as checked
				FROM admin_filters af
				LEFT JOIN admin_r_pages_filters apf ON apf.filter_id = af.id AND apf.page_id = '{$iPageID}'
				ORDER BY af.title ASC, af.rank ASC
				";
        return DAL::executeQuery($sql);
    }

}