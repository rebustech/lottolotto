<?php

/**
 * LotteryAdmin DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class LotteryAdmin {

    public function getLotteries($is_active = false) {

        $sWhere = "";
        if ($is_active == true) {
            $sWhere = " WHERE lc.is_active =1";
        }
        $sSQL = "SELECT lc.lottery_id,
						lc.comment,
						lc.classname,
						lc.is_active
				FROM lotteries_cmn lc
				{$sWhere}
				ORDER BY lc.lottery_id ASC ";

        return DAL::executeQuery($sSQL);
    }

    public function getLotteryDrawDates($iLotteryID, $bLimitPast = false) {
        if ($bLimitPast) {
            $sWhere = " AND rld.drawdate > 	DATE_ADD(NOW(), INTERVAL -3 MONTH ) ";
        }

        $sSQL = "SELECT
					rld.drawdate as drawdate,
					rld.fk_lottery_id as lottery_id
				FROM r_lottery_dates rld
				WHERE rld.fk_lottery_id = {$iLotteryID}
				{$sWhere}
				ORDER BY rld.drawdate ASC ";

        return DAL::executeQuery($sSQL);
    }

    public function getLotteryDetails($iLotteryID, $bGetLang = false) {
        if ($bGetLang) {
            $sSelect = ", ll.title as title, ll.bonus as bonus_text, ll.bonus_plural as bonus_text_plural";
            $sInnerJoin = " INNER JOIN lotteries_lang ll ON ll.fk_lottery_id = lc.lottery_id INNER JOIN languages l ON l.language_id = ll.fk_language_id AND l.default ";
        }

        $sSQL = "SELECT lc.lottery_id,
						lc.comment,
						lc.classname,
						lc.is_active,
						lc.number_count,
						lc.drawn_bonus_numbers,
						lc.results_page_id,
                                                lc.paythreshold_individual as ind_paythreshold,
                                                lc.paythreshold_draw as draw_paythreshold,
                                                lc.min_number,
                                                lc.max_number,
                                                lc.min_bonus,
                                                lc.max_bonus,
						c.symbol
						{$sSelect}
				FROM lotteries_cmn lc
				INNER JOIN currencies c ON c.currency_id = lc.fk_currency_id
				{$sInnerJoin}
				WHERE lc.lottery_id = {$iLotteryID}";
        if ($iLotteryID) {
            return DAL::executeGetRow($sSQL);
        }
        return false;
    }

    
    // Supplementarty function to retrieve informatino on where the draw/prize 
    // info can be accessed
    public function getLiveDrawInformation($iLotteryID)
    {
        $sSQL = "SELECT info_official_website, info_live_draw_times,
                        info_live_draw_location, info_prize_breakdown_times,
                        info_prize_breakdown_location
                 FROM lotteries_cmn
                 WHERE lottery_id = {$iLotteryID}";
       
        if ($iLotteryID) {
            return DAL::executeGetRow($sSQL);
        }
        return false;
    }
}