<?php

/**
 * Internal Payment gateway for handling winnings
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class WinningsGateway extends PaymentGateways {

    public function __construct() {
        parent::__construct(PaymentGateways::WINNINGS);
    }

    // Set the payment as completed
    public function ProcessPayment($sGatewayReference, $iTransactionID) {
        $this->sGatewayReference = $sGatewayReference;
        
        // Meed to set transaction ID here
        $this->iTransactionID = $iTransactionID;
        
        return $this->confirmTransaction();
    }

    // Set the payment as pending
    // (used for checking thresholds etc)
    public function ProcessPaymentPending($sGatewayReference) {
        $this->sGatewayReference = $sGatewayReference;
        return $this->confirmTransaction(false, 2);
    }
    
    protected function validateAmount($fAmount) {
        $bValid = false;
        if (is_numeric($fAmount) && $fAmount >= 0) {
            $bValid = true;
        }
        return $bValid;
    }

}