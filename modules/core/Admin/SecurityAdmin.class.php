<?php

/**
 * Security DAO and helper class
 * Part of the original LoveLotto package
 * @package LoveLotto
 * @subpackage Admin
 */
class SecurityAdmin {

    const ADMIN = 1;
    const AFFILIATE = 2;

    private $sEncryptionKey = NULL;
    private $sFolder = NULL;
    private $sAdminType = NULL;
    private $iAdminType = NULL;
    private $sAdminTitle = NULL;
    private $bIsActive = 0;
    private $bIsLoggedIn = NULL;
    private $iUserID = NULL;
    private $sUserName = NULL;
    private $sUserPassword = NULL;
    private $sUserPin = NULL;
    private $sFullName = NULL;
    private $dLoginDate = NULL;
    private $sIP = NULL;
    private $iHomepage = 0;

    public function SecurityAdmin($iAdminTypeID) {
        if (!$iAdminTypeID || !is_numeric($iAdminTypeID)) {
            $this->sEncryptionKey = NULL;
            $this->sFolder = NULL;
            $this->sAdminType = NULL;
            $this->sAdminTitle = NULL;
            $this->bIsActive = 0;
            $this->bIsLoggedIn = NULL;
            $this->iAdminType = NULL;
            $this->iUserID = NULL;
            $this->sUserName = NULL;
            $this->sUserPassword = NULL;
            $this->sUserPin = NULL;
            $this->sFullName = NULL;
            $this->dLoginDate = NULL;
            $this->sIP = NULL;
            return false;
        } else {
            $aAdminType = $this->getAdminTypeByID($iAdminTypeID);
            $this->iAdminType = $iAdminTypeID;
            $this->bIsActive = $aAdminType['is_active'];
            if (is_array($aAdminType) && $aAdminType['is_active'] == 1) {
                $this->sEncryptionKey = $aAdminType['encryptionkey'];
                $this->sFolder = $aAdminType['folder'];
                $this->sAdminType = $aAdminType['adminType'];
                $this->sAdminTitle = $aAdminType['adminName'];
                $this->bIsLoggedIn = 0;
                return true;
            } else {
                $this->sEncryptionKey = NULL;
                $this->sFolder = NULL;
                $this->sAdminType = NULL;
                $this->sAdminTitle = NULL;
                $this->bIsActive = 0;
                $this->bIsLoggedIn = NULL;
                $this->iAdminType = NULL;
                $this->iUserID = NULL;
                $this->sUserName = NULL;
                $this->sUserPassword = NULL;
                $this->sUserPin = NULL;
                $this->sFullName = NULL;
                $this->dLoginDate = NULL;
                $this->sIP = NULL;
                return false;
            }
        }
    }

    public function __sleep() {
        return array_keys(get_object_vars($this));
    }

    public function __call($method, $arguments) {
        $prefix = substr($method, 0, 3);
        $property = substr($method, 3);
        if (empty($prefix) || empty($property)) {
            return;
        }

        if ($prefix == "get" && isset($this->$property)) {
            return $this->$property;
        }
        if ($prefix == "set" && !empty($property)) {
            $this->$property = $arguments[0];
        }
    }

    public function getHomepage() {
        $sFilename = "index.php";
        if (!empty($this->iHomepage) && $this->iHomepage !== 0) {
            $sSQL = "SELECT ap.filename, ap.tablename
					FROM admin_pages ap
					LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '" . $this->getUserID() . "'
					WHERE aupb.fk_user_id IS NULL
					AND ap.id = {$this->iHomepage}";
            $aPageCredentials = DAL::executeGetRow($sSQL);
            if ($aPageCredentials && $aPageCredentials['filename'] && $aPageCredentials['tablename']) {
                $sFilename = implode("?tablename=", $aPageCredentials);
            }
        }
        return $sFilename;
    }

    public function getUserID() {
        return $this->iUserID;
    }

    public function getUsername() {
        return Encryption::decodeText($this->sUserName);
    }

    public function getPassword() {
        return Encryption::decodeText($this->sUserPassword);
    }

    public function getPin() {
        return Encryption::decodeText($this->sUserPin);
    }

    public function checkAuth($bNoRedirect = true, $bStripHTML = false) {
        try {
            $sql = "SELECT at.encryptedlogin as encryptionkey,
						   at.check_blacklist as check_blacklist,
						   at.is_active as adminactive,
						   au.isactive as useractive
						FROM admin_users au
						INNER JOIN admin_types at ON at.admintype_id = au.fk_admintype_id
						WHERE au.user_id ='" . $this->getUserID() . "'
						AND au.fk_admintype_id = '" . $this->iAdminType . "'";

            $aAdminType = DAL::executeGetRow($sql);
            $this->bIsActive = $aAdminType['adminactive'];
            if ($aAdminType && $aAdminType['encryptionkey'] == $this->sEncryptionKey && $aAdminType['adminactive'] == 1 && $aAdminType['useractive'] == 1 && $this->bIsLoggedIn == 1) {
                if ($aAdminType['check_blacklist']) {
                    $sFileName = ($_POST['post']) ? $_POST['pp'] : $_GET['pp'];
                    if (empty($sFileName)) {
                        $sPath = $_SERVER["PHP_SELF"];
                        $aPath = explode("/", $sPath);
                        $sFileName = $aPath[sizeof($aPath) - 1];

                        if($sFileName=='adminRouter.php'){
                            $sFileName = str_replace('/administration/','',$_SERVER["REQUEST_URI"]);
                        }

                    }

                    if ($sFileName != "index.php" && $sFileName != "logout.php") {
                        $sTableName = ($_POST['tablename']) ? $_POST['tablename'] : $_GET['tablename'];
                        if (!$this->checkPageBlackList($sTableName, $sFileName)) {
                            $oErrors = new Error($this->sAdminType);
                            $oErrors->addError("Unauthorised Access. Please contact an Administrator if the problem persists.");
                            if ($bNoRedirect == true) {
                                $sErrors = "<h2>Authentication Problem</h2>\r\n<div class='error' style='display:block'>";
                                $sErrors .= $oErrors->getErrorList();
                                $sErrors .= "</div>\r\n";
                                $oErrors->clearErrors();
                                if ($bStripHTML) {
                                    $sErrors = strip_tags($sErrors);
                                }
                                echo $sErrors;
                            } else {
                                header("location: /" . $this->sFolder . "index.php");
                            }
                            exit;
                        } else {
                            return true;
                        }
                    }
                    return true;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            //TODO: TRACE/LOGGING WITH STATIC LOGGING FUNCTION
            return false; //or standard error mgs. ie: we;re sorry we are experiencing problems right now blah
        }
    }

// eof

    public function getRememberCookie() {
        $file = CLASSPATH . "DATA/Encryption.class.php";
        if (file_exists($file))
            require_once("$file");
        $file = CLASSPATH . "DATA/Cookies.class.php";
        if (file_exists($file))
            require_once("$file");
        $aCredentials = unserialize(Encryption::decodeText(Cookies::getCookie($this->getsAdminType() . WEBSITENAME)));
        if (is_array($aCredentials)) {
            $aCredentials['username'] = Encryption::decodeText($aCredentials['username']);
            $aCredentials['password'] = Encryption::decodeText($aCredentials['password']);
            $aCredentials['pin'] = Encryption::decodeText($aCredentials['pin']);
        } else {
            $aCredentials = NULL;
            $this->deleteRememberCookie();
        }
        return $aCredentials;
    }

    public function setRememberCookie() {
        $file = CLASSPATH . "DATA/Encryption.class.php";
        if (file_exists($file))
            require_once("$file");
        $file = CLASSPATH . "DATA/Cookies.class.php";
        if (file_exists($file))
            require_once("$file");
        $aCredentials['username'] = $this->sUserName;
        $aCredentials['password'] = $this->sUserPassword;
        $aCredentials['pin'] = $this->sUserPin;
        $aCredentials = Encryption::encodeText(serialize($aCredentials));
        $bSetCookie = Cookies::setCookie($this->getsAdminType() . WEBSITENAME, $aCredentials, 24 * 30);
        return $bSetCookie;
    }

    public function deleteRememberCookie() {
        $file = CLASSPATH . "DATA/Cookies.class.php";
        if (file_exists($file))
            require_once("$file");
        $bSetCookie = Cookies::deleteCookie($this->getsAdminType() . WEBSITENAME);
        return $bSetCookie;
    }

    public function validateLogin($sUsername, $sPassword, $sPin) {
        try {
            $bIsAuthenticated = false;
            if ($sUsername && $sPassword && $sPin) {
                $file = CLASSPATH . "DATA/Encryption.class.php";
                if (file_exists($file))
                    require_once("$file");
                $sql = "SELECT 	au.user_id,
								au.password,
								au.pin,
								au.isactive,
								at.is_active as admin_active,
								at.encryptedlogin,
								at.comment,
								au.homepage,
								CONCAT_WS(' ', au.firstname, au.lastname) AS fullname
						FROM admin_users au
						INNER JOIN admin_types at ON at.admintype_id = au.fk_admintype_id
						WHERE au.username='{$sUsername}'
						AND au.fk_admintype_id = '" . $this->iAdminType . "'";
                $aCredentials = DAL::executeGetRow($sql);
                if ($aCredentials['isactive'] == 1 && $aCredentials['admin_active'] == 1) {
                    $sDBPassword = $aCredentials['password'];
                    $sDBPin = $aCredentials['pin'];

                    $bPassword = Encryption::Validate($sPassword, $sDBPassword);
                    $bPin = Encryption::Validate($sPin, $sDBPin);

                    if ($bPassword && $bPin) {
                        $bIsAuthenticated = true;
                        $this->sUserPin = Encryption::encodeText($sPin);
                        $this->sUserPassword = Encryption::encodeText($sPassword);
                        $this->sUserName = Encryption::encodeText($sUsername);
                        $this->iUserID = $aCredentials['user_id'];
                        $this->sEncryptionKey = $aCredentials['encryptedlogin'];
                        $this->sAdminTitle = $aCredentials['comment'];
                        $this->sFullName = $aCredentials['fullname'];
                        $this->dLoginDate = date(strtotime("now"));
                        $this->iHomepage = $aCredentials['homepage'];
                        $this->sIP = ($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                    }
                }
            }
            $this->bIsLoggedIn = $bIsAuthenticated;
        } catch (Exception $e) {
            //TODO: TRACE/LOGGING WITH STATIC LOGGING FUNCTION
            return $bIsAuthenticated; //or standard error mgs. ie: we;re sorry we are experiencing problems right now blah
        }
    }

// eof

    public function changePassword($sOldPassword, $sNewPassword) {
        $bValid = false;
        if ($sOldPassword && $sNewPassword) {
            $sql = "SELECT au.password
					FROM admin_users au
					WHERE au.user_id='" . $this->getUserID() . "'
					AND au.fk_admintype_id = '" . $this->iAdminType . "'";

            $sDBPassword = DAL::executeGetOne($sql);
            if ($sDBPassword) {

                $bPassword = Encryption::Validate($sOldPassword, $sDBPassword);
                if ($bPassword) {
                    $tablename = "admin_users";
                    $data = array(
                        'password' => Encryption::Encrypt($sNewPassword)
                    );
                    $params = "user_id = '" . $this->getUserID() . "'";
                    $bValid = DAL::Update($tablename, $data, $params);
                    if ($bValid) {
                        $this->sUserPassword = Encryption::encodeText($sNewPassword);
                    }
                }
            }
        }
        return $bValid;
    }

    public function changePin($sOldPin, $sNewPin) {
        $bValid = false;
        if ($sOldPin && $sNewPin) {
            $sql = "SELECT au.pin
					FROM admin_users au
					WHERE au.user_id='" . $this->getUserID() . "'
					AND au.fk_admintype_id = '" . $this->iAdminType . "'";

            $sDBPin = DAL::executeGetOne($sql);
            if ($sDBPin) {

                $bPin = Encryption::Validate($sOldPin, $sDBPin);
                if ($bPin) {
                    $tablename = "admin_users";
                    $data = array(
                        'pin' => Encryption::Encrypt($sNewPin)
                    );
                    $params = "user_id = '" . $this->getUserID() . "'";
                    $bValid = DAL::Update($tablename, $data, $params);
                    if ($bValid) {
                        $this->sUserPassword = Encryption::encodeText($sNewPin);
                    }
                }
            }
        }
        return $bValid;
    }

    public function validatePin($sPin) {
        $bValid = false;
        if ($sPin) {
            $sql = "SELECT au.pin
					FROM admin_users au
					WHERE au.user_id='" . $this->getUserID() . "'
					AND au.fk_admintype_id = '" . $this->iAdminType . "'";

            $sDBPin = DAL::executeGetOne($sql);
            if ($sDBPin) {

                $bPin = Encryption::Validate($sPin, $sDBPin);
                if ($bPin) {
                    $bValid = true;
                }
            }
        }
        return $bValid;
    }

    public function Logout() {
        $this->sEncryptionKey = NULL;
        $this->sAdminTitle = NULL;
        $this->bIsLoggedIn = 0;
        $this->iUserID = NULL;
        $this->sUserName = NULL;
        $this->sUserPassword = NULL;
        $this->sUserPin = NULL;
        $this->sFullName = NULL;
        $this->dLoginDate = NULL;
        $this->sIP = NULL;
    }

    public function getLoginTime() {
        return date("H:i:s D M Y", $this->dLoginDate);
    }

    private function getAdminTypeByID($iAdminTypeID) {
        $sSQL = "SELECT comment AS adminName,
						path AS folder,
						prefix AS adminType,
						encryptedlogin as encryptionkey,
						is_active as is_active,
						check_blacklist
				FROM admin_types
				WHERE admintype_id = '{$iAdminTypeID}'
				";
        return DAL::executeGetRow($sSQL);
    }

    public function getAdminTypes() {
        $sSQL = "SELECT admintype_id AS id,
						comment AS adminName,
						path AS folder,
						encryptedlogin as encryptionkey,
						is_active as is_active
				FROM admin_types
				WHERE is_active = 1
					AND showinmenu = 1
				";

        return DAL::executeQuery($sSQL);
    }

    private function checkPageBlackList($sTableName, $sPageFilename) {
        $sSQL = "SELECT ap.id
				FROM admin_pages ap
				WHERE
					ap.filename = '" . $sPageFilename . "'
					AND ap.tablename = '" . $sTableName . "'";
        $iPageID = DAL::executeGetOne($sSQL);
        if ($iPageID) {
            $sSQL = "SELECT aupb.fk_page_id
					FROM admin_users_pages_blacklist aupb
					INNER JOIN admin_pages ap ON ap.id = aupb.fk_page_id
					WHERE aupb.fk_user_id = '" . $this->iUserID . "'
						AND aupb.fk_page_id = '" . $iPageID . "'";
            $aPageID = DAL::executeGetOne($sSQL);
            return ($aPageID) ? false : true;
        } else {
            return true;
        }
        return true;
    }

    public function getNavigationPages($iParentID = 0) {
        $sSQL = "SELECT
						ap.id,
						ap.title,
						ap.filename,
						ap.tablename,
                                                ap.icon,
						IF(arup.fk_user_id IS NOT NULL, 1, 0) AS isfavourite
				FROM admin_pages ap
				LEFT JOIN admin_r_users_pages arup ON arup.fk_page_id = ap.id AND arup.fk_user_id = '" . $this->getUserID() . "'
				LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '" . $this->getUserID() . "'
				WHERE
						aupb.fk_user_id IS NULL
				 		AND ap.parent_id = '{$iParentID}'
				ORDER BY ap.sort_order,ap.title asc";
        return DAL::executeQuery($sSQL);
    }

    public function getNotifications() {
        $sSQL = "SELECT
					an.content as notification,
					an.priority as priority
				FROM admin_notifications an
				WHERE an.fk_admin_id = '{$this->iAdminType}'
					AND an.isactive = 1
					AND date_format(now(), '%Y-%m-%d') BETWEEN an.start_date AND an.end_date
				ORDER BY an.priority DESC, an.rank ASC, an.start_date ASC";

        return DAL::executeQuery($sSQL);
    }

    public function getBookmarks($bIsBookmarked = true) {
        if ($bIsBookmarked) {
            $sSQL = "SELECT ap.id,
							CONCAT('" . $this->sFolder . "',ap.filename) as filename,
							ap.tablename,
							ap.title
					FROM admin_pages ap
					INNER JOIN admin_r_users_pages arup ON arup.fk_page_id = ap.id AND arup.fk_user_id = '" . $this->getUserID() . "'
					LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '" . $this->getUserID() . "'
					WHERE aupb.fk_user_id IS NULL
					ORDER BY arup.rank ASC, ap.title ASC ";
        } else {
            $sSQL = "SELECT ap.id,
							CONCAT('" . $this->sFolder . "',ap.filename) as filename,
							ap.tablename,
							ap.title
					FROM admin_pages ap
					LEFT JOIN admin_r_users_pages arup ON arup.fk_page_id = ap.id AND arup.fk_user_id = '" . $this->getUserID() . "'
					LEFT JOIN admin_users_pages_blacklist aupb ON aupb.fk_page_id = ap.id AND aupb.fk_user_id = '" . $this->getUserID() . "'
					WHERE aupb.fk_user_id IS NULL
						AND arup.fk_user_id IS NULL
					ORDER BY arup.rank ASC, ap.title ASC ";
        }
        return DAL::executeQuery($sSQL);
    }

    public function addBookmark($iPageID, $iRank = 1) {
        $sSQL = "INSERT INTO admin_r_users_pages
				( fk_page_id, fk_user_id, rank )
				VALUES ( '{$iPageID}', '" . $this->getUserID() . "', '{$iRank}' )
				ON DUPLICATE KEY UPDATE rank = '{$iRank}'";

        return DAL::executeQuery($sSQL);
    }

    public function deleteBookmark($iPageID) {
        $sSQL = "DELETE FROM admin_r_users_pages
				WHERE fk_page_id = '{$iPageID}'
				AND fk_user_id = '" . $this->getUserID() . "' ";

        return DAL::executeQuery($sSQL);
    }

    public function getUserDetails() {
        if ($this->iAdminType == SecurityAdmin::AFFILIATE) {
            $sInnerJoin = " INNER JOIN affiliates af ON af.fk_user_id = au.user_id ";
        }
        $sSQL = "SELECT *
				FROM admin_users au
				{$sInnerJoin}
				WHERE au.user_id = '" . $this->getUserID() . "'";
        return DAL::executeGetRow($sSQL);
    }

    public function editUserDetails($aDetails) {
        $bValid = false;
        if (is_array($aDetails)) {
            $tablename = "admin_users";
            $params = "user_id = '" . $this->getUserID() . "'";
            $bValid = DAL::Update($tablename, $aDetails, $params);
        }
        return $bValid;
    }

}