<?php

class OrderScanCronApi extends \LL\AjaxController{

    /**
     * Cleans up order statuses - runs once an hour and also runs after a winscan
     * or draw details update
     */
    function pruneOrders(){
        # Check for tickets that won
        $sSQL='UPDATE booking_items b INNER JOIN lottery_winnings w ON b.bookingitem_id=w.fk_bookingitem_id SET fk_status_id=5';
        \DAL::executeQuery($sSQL);


        # Check for closed tickets
        $sSQL='UPDATE booking_items SET fk_status_id=2 WHERE fk_lotterydate < NOW() AND (fk_status_id=1 OR fk_status_id IS NULL)';
        \DAL::executeQuery($sSQL);

        # Update orders that only contain closed tickets
        /**
         * After running the above we need to find all orders that have now closed
         * and close them by running them through the original game engine
         */
        do{
            $sSQL='SELECT fk_order_item_id FROM booking_items GROUP BY fk_order_item_id HAVING MAX(fk_lotterydate)<NOW() LIMIT 0,100';
            $aClosedOrders=\DAL::executeQuery($sSQL);
            if(is_array($aClosedOrders)){
                foreach($aClosedOrders as $aClosedOrder){
                    $iID=$aClosedOrder['fk_order_item_id'];
                    //$oOrderItem=LL\OrderItem::getById($iID);
                    //$oOrderItem->closeOrder();
                    unset($oOrderItem);
                }
                unset($aClosedOrder);
                unset($aClosedOrders);
            }
        }while(sizeof($aClosedOrders)>0);

        # Check for tickets to void or cancel
    }

}