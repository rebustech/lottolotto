<?php
/**
 * Installs the New "Variants CMS"
 * @package LoveLotto
 * @subpackage VariantsCMS
 */

namespace LL\OrderScan;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        if(self::getModuleVersion('VariantsCMS')<1){
        }
        $aJobs[]=new \LL\Installer\Task('LL\VariantsCMS\Installer', 'installDatabase', 'Install Order Scanner Database');
        //$aJobs[]=new \LL\Installer\Task('LL\VariantsCMS\Installer', 'installLink', 'Add CRM link to nav');
        return $aJobs;
    }

    /**
     * Installs all the tables we need for the new CMS
     */
    static function installDatabase(\LL\Installer\Task $oTask){
        /**
         * Lookup table for the statuses
         */
        $oTable=new \LL\Installer\Table('booking_item_status');
        $oTable->addIdField();
        $oTable->addField(new \LL\Installer\Field('name', 'varchar', 20));
        $oTable->compile();

        /**
         * Add the initial values
         */
        \DAL::executeQuery("REPLACE INTO booking_item_status (`id`,`name`) values(1,'Active'),(2,'Closed'),(3,'Cancelled'),(4,'Void'),(5,'Won')");

        /**
         * Add the key field on the booking items table (this can take a long time to run)
         */
        $oTable=new \LL\Installer\Table('booking_items');
        $oTable->addField(new \LL\Installer\Field('fk_status_id', 'int',11));
        $oTable->compile();

        //NOTE : NO Constraint - too many already on that table!

        /**
         * Add the key field on the booking items table (this can take a long time to run)
         */
        $oTable=new \LL\Installer\Table('booking_order_items');
        $oTable->addField(new \LL\Installer\Field('fk_status_id', 'int',11));
        $oTable->compile();

        //NOTE : NO Constraint - too many already on that table!
    }

}
