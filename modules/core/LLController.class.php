<?php

/**
 * Base class for controllers
 * When creating controllers try and use AdminController, AjaxController or
 * FrontendController
 * This will be useful for adding useful methods that all controllers will need
 *
 * Currentlly empty, just for semantics
 *
 * @package LoveLotto
 * @subpackage Core
 * @author J.Patchett
 */

namespace LL;

class Controller{

}