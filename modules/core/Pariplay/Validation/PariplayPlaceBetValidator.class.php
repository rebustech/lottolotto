<?php 

class PariplayPlaceBetValidator extends \PariplayValidator implements \ValidableInterface {

    static $rules = [
        'TicketId'		=> 	'required|int',
        'IpAddress'		=>	'required|IP',
        'RequestId'		=>	'required|int',
        'TimeStamp'		=>	'required',
        'Token'		    =>	'required',
        'Login'         =>  'required',
        'Password'      =>  'required',
        'BetAmount'     =>  'required|float',
        'WinAmount'     =>  'required|float',
        'GameId'        =>  'required'

    ];

} 