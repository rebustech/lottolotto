<?php

abstract class PariplayValidator implements \ValidableInterface {

    protected $errors=[];

    /**
     * @todo Move this per validation class
     * @var string
     */
    protected $sDecimalSeparator='.';
    /**
     *
     * Take an array of fields and validate them against the defined rules
     * @param array $aAttributes
     * @return bool
     */
    public function passes(array $aAttributes)
    {
        foreach(static::$rules AS $key => $rule){
            $aRules=explode('|',$rule);

            // Validate required
            if(in_array('required',$aRules) && (!isset($aAttributes[$key]) || (isset($aAttributes[$key]) && empty($aAttributes[$key])))){

                $this->errors['ErrorCode']='XX';
                $this->errors['ErrorDescription']='Field '.$key.' is required';

                return false;
            }

            // Validate IP Address
            if(in_array('IP',$aRules) && !filter_var($aAttributes[$key], FILTER_VALIDATE_IP)){

                array_push($this->errors,'Field '.$key.' must be a valid IP address');
                return false;
            }

            // Validate Integer
            if(in_array('int',$aRules) && !filter_var($aAttributes[$key], FILTER_VALIDATE_INT)){

                array_push($this->errors,'Field '.$key.' must be an integer');
                return false;
            }

            // Validate Decimal
            if(in_array('float',$aRules) && !filter_var($aAttributes[$key], FILTER_VALIDATE_FLOAT,["options"=>["decimal" => $this->sDecimalSeparator]])){

                array_push($this->errors,'Field '.$key.' must be a decimal number with . as separator');
                return false;
            }
        }

        return true;
    }

    /**
     * Return the errors that occurred from validation
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }
}