<?php

interface ValidableInterface {
    public function passes(array $attributes);
    public function errors();
} 