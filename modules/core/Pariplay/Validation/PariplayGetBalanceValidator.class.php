<?php 

class PariplayGetBalanceValidator extends \PariplayValidator implements \ValidableInterface {

    static $rules = [
        'RequestId'		=>	'required|int',
        'TimeStamp'		=>	'required',
        'Token'		    =>	'required',
        'Login'         =>  'required',
        'Password'      =>  'required'
    ];
}