<?php 

class PariplayLoginValidator extends \PariplayValidator implements \ValidableInterface {

     static $rules = [
        'GameId'		=> 	'required',
        'IpAddress'		=>	'required|IP',
        'RequestId'		=>	'required|int',
        'TimeStamp'		=>	'required',
        'Token'		    =>	'required',
        'Login'         =>  'required',
        'Password'      =>  'required'
    ];
}