<?php

use LL\AjaxController;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PariplayApiClient {

    /**
     * Guzzle REST client instance
     * @var Object
     */
    protected $oRestClient;

    /**
     * The Pariplay API Url
     * @var string
     */
    public $sUrl;

    /**
     * The Pariplay Username
     * @var
     */
    public $sLogin;

    /**
     * The Pariplay password
     * @var
     */
    private $sPassword;

    /**
     * The Pariplay request id
     * @var
     */
    private $iRequestId;

    /**
     * The Pariplay Auth Token
     * @var
     */
    private $sToken;

    public function __construct(){

        //Instantiate new rest client
        $this->oRestClient=new Client();

        //Store the pariplay API Url
        $this->sUrl='http://www.thecollective.gr/json.php';

        //Register credentials for pariplay API login
        $this->sLogin=\Config::$config->sPariplayLogin;
        $this->sPassword=\Config::$config->sPariplayPassword;
    }

    /**
     * TESTTTTT
     * @return \GuzzleHttp\Stream\StreamInterface|null
     */
    public function play(){

        return $this->pariPlayPlaceBet(14,'0.20','0.40','Bomba',[

            'IpAddress'=>$_SERVER['REMOTE_ADDR'],
            'RequestId'=>$this->iRequestId,
            'TimeStamp'=>"Date(".time().")",
            'Token'=>$this->sToken,
            'Login'=>$this->sLogin,
            'Password'=>$this->sPassword
        ]);

    }


    /**
     * Fetch the user balance from PariPlay API
     * @param $aArgs
     * @return \GuzzleHttp\Stream\StreamInterface|null
     */
    public function pariPlayGetBalance($aArgs){

        return $this->makeApiCall('GetBalance',$aArgs);

    }

    /**
     * Place a bet in Pariplay
     * @param $iTicketId
     * @param $fBetAmount
     * @param $fWinAmount
     * @param $sGameId
     * @param $aArgs
     * @return \GuzzleHttp\Stream\StreamInterface|null
     */
    public function pariPlayPlaceBet($iTicketId,$fBetAmount,$fWinAmount,$sGameId,$aArgs){

        $aRequestParams=[
            'TicketId'=>$iTicketId,
            'BetAmount'=>$fBetAmount,
            'WinAmount'=>$fWinAmount,
            'GameId'=>$sGameId
        ];
        $aParams=array_merge($aRequestParams,$aArgs);

        return $this->makeApiCall('PlaceBet',$aParams);

    }

    /**
     * Mae the actual API HTTP call to Pariplay
     * @param $sMethodName
     * @param array $aArgs
     * @return \GuzzleHttp\Stream\StreamInterface|null
     * @throws LottoException
     */
    private  function makeApiCall($sMethodName,array $aArgs){

        $aParams=array_merge(['methodname'=>$sMethodName],$aArgs);

        try{
            $response=$this->oRestClient->post($this->sUrl,['body'=>$aParams]);
        }
        catch(RequestException $e){
            throw new \LottoException($e->getCode(),$e->getMessage());
        }

        try{
            $sCastToJson = $response->json();
        }
        catch (ParseException $e){
            throw new \LottoException($e->getCode(),$e->getMessage());
        }


        if($sCastToJson['ErrorCode']!==0){

            throw new \LottoException($sCastToJson['ErrorCode'],$sCastToJson['ErrorDescription']);
        }
        else{
            return $sCastToJson;
        }

    }
}