<?php

interface PariplayApiInterface {

    public function handleRequest();
    public function PariplayGetBalance();
    public function PariplayAuthenticate();
    public function PariplayPlaceBet();

} 