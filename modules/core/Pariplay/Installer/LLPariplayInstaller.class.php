<?php
/**
 * Installs the Pariplay Module & Handlers
 * @package LoveLotto
 * @subpackage Pariplay
 */

namespace LL\Pariplay;

class Installer{
    use \LL\Installer\TInstaller;


    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){

        $aJobs[]=new \LL\Installer\Task('\LL\Pariplay\Installer', 'installGameEnginePariplay', 'Install Pariplay game engine');
        $aJobs[]=new \LL\Installer\Task('\LL\Pariplay\Installer', 'installPariplayTokenTable', 'Install Pariplay game engine tokens table');
        return $aJobs;
    }


    static function installGameEnginePariplay(){
        self::installGameEngineCore('\LL\GameEngine\Pariplay', 'Pariplay Core');
        self::installGameEngineModule('Core Pariplay engine');
    }


    static function installGameEngineCore($sClass,$sName,$dVersion=1.2){
        self::installHandler($sClass, $sName, 'GameEngineCore',$dVersion);
    }

    static function installGameEngineModule($sModuleName){
        self::installModule($sModuleName, 2);
    }

    static function installPariplayTokenTable(){
        $oTable=new \LL\Installer\Table('pariplay_engine_tokens');
        $oTable->addIdField();
        $oTable->addField(new \LL\Installer\Field('fk_member_id', 'INT',11));
        $oTable->addField(new \LL\Installer\Field('token', 'VARCHAR',255));
        $oTable->addField(new \LL\Installer\Field('token_api', 'VARCHAR',255));
        $oTable->addField(new \LL\Installer\Field('created_at', 'DATETIME'));
        $oTable->addField(new \LL\Installer\Field('updated_at', 'DATETIME'));
        $oTable->addConstraint(new \LL\Installer\Constraint('fk_member_id','members','member_id'));
        
        $oTable->compile();
    }
}