<?php

/*
 * Request Params
 * requestid
 * timestamp
 * login
 * password
 * token
 *
 */


/*
 * Response Params
 *
 * timestamp, request id. token error code error description error command
 */

/**
 * Class PariplayApi
 * Receive requests from pariplay
 * and respond respectively
 */
class PariplayApi {

    public function handleRequest($sMethodname){

        //Switch to method name route to self methods
    }

    public function login($sGameId,$sIpAddress,$iRequestId,$sToken,$sLogin,$sPassword,$sTimestamp){

        //Validate token

        //Authenticate the player

        //Generate new token

        //Return new token,account id(max 50char unique),player name (optional),currency string ,iso country code
        //balance decimal eg 0.20, bonus balance decimal | 0, timestamp, iRequestId, error code,
        //error description, error command
    }

    public function getBalance($standardparameters){

        //Check if logged in

        //Retrieve Balance

        //Return all standard + balance decimal, bonus balance decimal | 0
    }

    public function placeBet($standard,$iTicketId,$sGameId,$dBetAmount,$dWinAmount,$sIpAddress){

        //Check if logged in

        // Add bet in transactions

        // Return standard + refTransactionId (string), balance (decimal),bonusbalance(decimal)
    }

} 