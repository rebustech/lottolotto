<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

use LL\AjaxController;
/**
 * Class PariplayApi  * Receive requests from pariplay  and respond respectively
 *
 * @author N.Poulias
 */
class PariplayTestApi extends AjaxController implements \PariplayApiInterface {

    /**
     * Each request from Pariplay MUST have the standard parameters
     * @var array
     */
    static $aStandardRequestParams=[
        'Login',
        'Password',
        'RequestId',
        'Token',
        'TimeStamp'
    ];

    static $aStandardResponseParams=[
        'RequestId'=>null,
        'Token'=>null,
        'TimeStamp'=>null,
        'ErrorCode'=>0,
        'ErrorDescription'=>null,
        'ErrorCommand'=>null
    ];

    /**
     * Object validator for login request
     * @var
     */
    protected $oLoginValidator;

    /**
     * Object validator for place bet request
     * @var
     */
    protected $oPlaceBetValidator;

    /**
     * Object validator for get balance request
     * @var
     */
    protected $ogetBalanceValidator;



    public function __construct(){

        $this->oLoginValidator=new \PariplayLoginValidator();
        $this->oPlaceBetValidator=new \PariplayPlaceBetValidator();
        $this->ogetBalanceValidator=new \PariplayGetBalanceValidator();
    }
    public function handleRequest(){


        $sMethodname=self::extractMethodname();

        if(!$sMethodname){
            return $this->Errors('406','Not acceptable. No method name defined');
        }

        if(!method_exists($this,$sMethodname)){

            return $this->Errors('404','Method not found');
        }


        call_user_func_array([$this,$sMethodname],[]);


    }


    public function PariplayAuthenticate(){

        if($this->oLoginValidator->passes($_POST)){
            echo "passed";
        }
        else{
            var_dump($this->oLoginValidator->errors());
            return;
        }
        //Validate token

        //Authenticate the player

        //Generate new token

        //Return new token,account id(max 50char unique),player name (optional),currency string ,iso country code
        //balance decimal eg 0.20, bonus balance decimal | 0, timestamp, iRequestId, error code,
        //error description, error command

    }

    public function PariplayGetBalance(){

        if($this->ogetBalanceValidator->passes($_POST)){
            echo "passed";
        }
        else{
            var_dump($this->ogetBalanceValidator->errors());
            return;
        }

        //Check if logged in

        //Validate Token

        //Retrieve Balance
        $aBalanceData=['Balance'=>'22.40','BonusBalance'=>0];

        //Return all standard + balance decimal, bonus balance decimal | 0
        $aStandardResponse=self::$aStandardResponseParams;
        $aStandardResponse['TimeStamp']='\/Date('.time().')\/';
        $aStandardResponse['RequestId']=$_POST['RequestId'];
        $aStandardResponse['Token']=$_POST['Token'];
        $data=array_merge($aBalanceData,$aStandardResponse);

        return self::output($data);
    }

    public function PariplayPlaceBet(){

        // Validate Request
        if($this->oPlaceBetValidator->passes($_POST)){
            echo "passed";
        }
        else{
            var_dump($this->oPlaceBetValidator->errors());
            return;
        }
        //Check if logged in

        // Add bet in transactions

        // Return standard + refTransactionId (string), balance (decimal),bonusbalance(decimal)
    }

    /**
     * Return formatted errors
     * @todo Response must be sent with header http code = $iErrorCode
     * @param $iErrorCode
     * @param $sErrorMessage
     */
    public function Errors($iErrorCode,$sErrorMessage){

        return self::output(['ErrorCode'=>$iErrorCode,'ErrorDescription'=>$sErrorMessage]);
    }



    /**
     * Check if there is a method name in the request and then
     * suffix and return accordingly
     * @return bool|string
     */
    private static function extractMethodname(){

        if(isset($_POST['methodname']) && !empty($_POST['methodname'])){
            $sMethodName=$_POST['methodname'];
            return "Pariplay{$sMethodName}";
        }

        return false;

    }
} 