<?php

namespace LL\GameEngine;

class Pariplay extends \GameEngine{

    /**
     * Used to decide what type of game to display on the frontend
     * @var string
     */
    var $sGameType='instant';

    /**
     * Used to decide what view file to render
     * @var string
     */
    var $sDisplayPod='pods/pariplay';

    /**
     * @var
     */
    static $oApiClient;

    public function __construct(){
        //Instantiate new rest client
        self::$oApiClient=new \PariplayApiClient() ;
    }

    /**
     * Play ...
     * @return string
     */
    public function playGame(){
    	$sToken=self::getToken();

    	$oGame=self::$oApiClient->play($sToken);

        return $sToken;
    }

    /**
     * Fetch token from Oauth2 generator
     * @return string
     */
    private function getToken(){
        return uniqid();
    }

}