<?php

/**
 * Payment gateway for Neteller
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class Neteller extends PaymentGateways {

    protected $sVersion = "4.1";

    public function __construct() {
        parent::__construct(PaymentGateways::NETELLER);
    }

    public function getCharges() {
        return ($this->fAmount * 0.045) + 0.29;
    }

    public function ProcessPayment(&$oPage, &$oMember, $aInfo) {
        $aResponse = $this->prepareTransaction($oMember->getMemberID(), $aInfo['net_account'], $aInfo['net_id']);
        if ($aResponse['approval'] == 'yes') {
            $this->sGatewayReference = $aResponse['trans_id'];
            $this->confirmTransaction();
            $this->sendAddFundsReceipt($oMember->getMemberID());
            $oPage->saveSessionObjects();
            header('location: /' . Navigation::getPageURL($this->iConfirmPageID));
            exit;
        } else {
            $aMerchantErrors[] = 1001;
            $aMerchantErrors[] = 1004;
            $aMerchantErrors[] = 1006;
            $aMerchantErrors[] = 1015;
            $aMerchantErrors[] = 1017;
            $aMerchantErrors[] = 1018;
            $aMerchantErrors[] = 1020;
            $aMerchantErrors[] = 1021;
            $aMerchantErrors[] = 1025;
            $aMerchantErrors[] = 5000;
            $aMerchantErrors[] = 6000;
            if (in_array($aResponse['error'], $aMerchantErrors)) {
                $oEmail = new Mailer();
                $oEmail->shortHeader();
                $oEmail->setSubject("Neteller Payment Failed");
                $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->addTo("lincoln@lovelotto.com", "LoveLotto.com Payments");
                $sBody = "<h2>Neteller Payment Failed.</h2>";
                $sBody .= "<h3>Member Details</h3>";
                $sBody .= "<pre>" . print_r($oMember->getMemberDetails(), true) . "</pre>";
                $sBody .= "<h3>Payment Details</h3>";
                $sBody .= "<strong>Topup Amount</strong>: &euro;" . $this->fAmount;
                $sBody .= "<h3>Gateway Response</h3>";
                $sBody .= "<pre>" . print_r($aResponse, true) . "</pre>";
                $oEmail->sendEmail($sBody);
                $oMember->addError("Your request could not be completed. Please try again or contact us should you still experience problems.");
            } else {
                $oMember->addError($aResponse['error_message']);
                if ($aResponse['url']) {
                    $oMember->addNotice($aResponse['url_message'] . "<a href='" . $aResponse['url'] . "' target='_blank'>Click Here</a>");
                }
                if ($aResponse['telephone_message']) {
                    $oMember->addNotice($aResponse['telephone_message']);
                }
            }
            $oPage->saveSessionObjects();
            header('location: /' . Navigation::getPageURL($this->iCancelPageID));
            exit;
        }
    }

    public function prepareTransaction($iMemberID, $sMember_Net_Account, $sMember_Net_ID) {
        $aParams = array();
        $aParams[] = "";
        $aParams[] = "merch_transid=" . $this->sTransactionReference;
        $aParams[] = "language=" . "EN";
        $aParams[] = "amount=" . $this->fAmount;
        $aParams[] = "currency=" . "EUR";
        $aParams[] = "secure_id=" . $sMember_Net_ID;
        $aParams[] = "net_account=" . $sMember_Net_Account;
        $aParams[] = "custom_1=" . $iMemberID;
        $aParams[] = "custom_2=" . $this->iTransactionID;
        if ($this->bTestGateway) {
            $aParams[] = "test=" . "1";
        }
        $resArray = $this->curl_call(implode("&", $aParams));

        return $resArray;
    }

    protected function curl_call($nvpStr) {
        $aAccountDetails = explode("|", $this->sAccount);
        $nvpreq = "version=" . $this->sVersion . "&merchant_id=" . urlencode($aAccountDetails[0]) . "&merch_key=" . urlencode($aAccountDetails[1]) . "&merch_name=" . urlencode(WEBSITETITLE) . $nvpStr;
        //setting the curl parameters.
        $sUserAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
        $oCurl = curl_init($this->sURL);
        curl_setopt($oCurl, CURLOPT_POST, 1);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $nvpreq);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($oCurl, CURLOPT_USERAGENT, $sUserAgent);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);  // this line makes it work under https
        //getting response from server
        $response = curl_exec($oCurl);
        //convrting NVPResponse to an Associative Array
        $nvpResArray = $this->deformatNVP($response);

        if (curl_errno($oCurl)) {
            print_d(curl_error($oCurl));
            //Execute the Error handling module to display errors.
        }
        curl_close($oCurl);
        return $nvpResArray;
    }

    protected function deformatNVP($sResponse) {
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($sResponse), $xml_values);
        xml_parser_free($parser);
        $aResponseArray = array();
        foreach ($xml_values as $xml_key) {
            if ($xml_key['type'] == 'complete') {
                $aResponseArray[$xml_key['tag']] = $xml_key['value'];
            }
        }
        return $aResponseArray;
    }

}