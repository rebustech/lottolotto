<?php

/**
 * Payment gateway for the lovelotto gateway - this is used for transacting manual top ups,
 * account corrections and winnings
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class MLWithdrawal extends PaymentGateways {

    public function __construct() {
        parent::__construct(PaymentGateways::WITHDRAWAL);
        $this->bSkipConfirmationPage=true;
    }

    public function ProcessPayment() {

        $this->sGatewayReference = TRANSACTIONREFERENCE . time();
        if($this->fAmount > -2500){
            $this->confirmTransaction();
        }else{
            $this->confirmTransaction(true,2);
        }
    }
}
