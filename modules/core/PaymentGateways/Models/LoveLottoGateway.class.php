<?php

/**
 * Payment gateway for the lovelotto gateway - this is used for transacting manual top ups,
 * account corrections and winnings
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class LoveLottoGateway extends PaymentGateways {

    public function __construct() {
        parent::__construct(PaymentGateways::LOVELOTTO);
    }

    public function ProcessPayment() {

        //$this->sGatewayReference = TRANSACTIONREFERENCE . time();
        $this->sGatewayReference = $this->sTransactionReference;
        if($this->fAmount < 1000){
            $this->confirmTransaction();
        }else{
            $this->confirmTransaction(true,2);
        }
    }

    protected function validateAmount($fAmount) {
        return true;
    }
}
