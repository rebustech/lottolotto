<?php

/**
 * Payment gateway for Transactium
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class Transactium extends PaymentGateways {

    protected $sJSURL = "https://psp.transactium.com/hpservices/site/js/startHPS.js";
    protected $sStagingJSURL = "https://psp.stg.transactium.com/hpservices/site/js/startHPS.js";
    protected $sProfileTag = "";
    protected $sLowTransProfileTag = "";
    protected $sReturnURL = "system/controls/paymentGateway.php?g=transactium";
    protected $oSOAP = NULL;
    protected $aGatewayErrors = array(
        'HPS1' => 'Indicates that an error occurred during the standard card transaction processing by the PSP.',
        'HPS2' => 'Indicates that an error occurred during the 3-D Secure enrolment check by the PSP.',
        'HPS3' => 'Indicates that the card is not enrolled to 3-D Secure and merchant accepts only 3-D Secure enrolled cards. Transaction halted.',
        'HPS4' => 'Indicates that an error occurred after a 3-D Secure authentication session was completed.',
        'HPS5' => 'Indicates that the user tried to submit transaction details before choosing a payment method (and hence a page was skipped).',
        'HPS6' => 'Indicates that the user tried to choose a payment method before a transaction ID and hosted payment profile were loaded.',
        'HPS7' => 'Indicates that the supplied transaction ID is not a valid hosted payment ID or is not in a valid state (ex. Already completed).',
        'HPS8' => 'Indicates that an invalid security code was encountered during initialization.',
        'HPS9' => 'Indicates that the hosted payment session could not ended since no transaction is loaded.',
        'HPS10' => 'Indicates that the user\'s client IP address does NOT match the IP address set in the Client IP Restriction.',
        'HPS11' => 'Indicates that the system could not load the transaction details of the particular payment.',
        'HPS12' => 'Indicates that the hosted payment was removed from being active while the session was still active. The hosted payment could not be found in the archived hosted payments. Hosted payment session timed out.',
        'HPS13' => 'Indicates that the hosted payment session could not be locked for financial transaction processing.',
        'HPS14' => 'Indicates that the financial transaction has been processed but the user tried to access a page shown in earlier stages of the payment session.',
        'HPS15' => 'Indicates that a hosted payment session state was skipped.',
        'HPS16' => 'Indicates that the particular page requires a certain payment method, however either no payment method was selected or an invalid payment method was selected.',
        'HPS17' => 'Indicates that the particular page requires the card payment method, however the selected payment method is not card payment.',
        'HPS18' => 'Indicates that the particular page requires a card configuration (for card payment method) to be selected.',
        'HPS19' => 'Indicates that the 3-D Secure end has been fired but no MD/PARES have been posted.',
        'HPS20' => 'Indicates that an error occurred during the 3-D Secure finalization process by the PSP.',
        'HPS21' => 'Indicates that no available payment methods are present for the hosted payment\'s profile.',
        'HPS22' => 'Indicates that the hosted payment session expired due to the maximum time limit being reached.',
        'HPS23' => 'Indicates that the start page was fired without a valid transaction.',
        'HPS24' => 'Indicates that there is no host account configured for the hosted payment session profile or that there is a restriction to a different application profile from the supplied card.',
        'HPS25' => 'Indicates that there are multiple secondary host accounts and no primary host accounts set for the hosted payment profile.'
    );

    public function __construct() {
        parent::__construct(PaymentGateways::TRANSACTIUM);
        $this->bSkipConfirmationPage = true;
    }

    public function getCharges() {
        return ($this->fAmount < 10) ? (($this->fAmount * 0.00) + 0.75) : ($this->fAmount * 0.00);
    }

    public function ProcessPayment(&$oPage, &$oMember, $aDetails) {
        $oResult = $this->prepareTransaction($oMember);
        if ($oResult) {
            $oPage->saveSessionObjects();
        }

        $aURLs['js'] = $this->sJSURL;
        $aURLs['redirect'] = $oResult->CreateHostedPaymentResult->RedirectURL;
        if ($this->bTestGateway) {
            $aURLs['js'] = $this->sStagingJSURL;
        }

        return $aURLs;
    }

    public function prepareTransaction(&$oMember) {
        $aParams = array();

        $aParams['Amount'] = ($this->fAmount + $this->getCharges()) * 100; ///payment amount in cents
        $aParams['Currency'] = "EUR"; ///payment currency
        $aParams['PaymentType'] = "Sale"; ///payment type
        $aParams['HPSProfileTag'] = ($this->fAmount < 10) ? $this->sLowTransProfileTag : $this->sProfileTag; ///payment profile
        $aParams['MaxTimeLimit'] = 0;  ///max session time limit
        $aParams['OrderReference'] = $this->sTransactionReference; ///order reference
        $aParams['ClientReference'] = $oMember->getMemberID(); ///client reference
        //$aParams['ClientIPRestriction'] = NULL; ///customer IP
        $aParams['ClientBillingCountry'] = $oMember->getMemberCountryCode(); ///customer billing country code
        $aParams['ClientEmail'] = $oMember->getUsername(); ///customer email address
        $aParams['SuccessURL'] = "https://" . WEBSITEURL . $this->sReturnURL; ///URL to redirect in case of success
        $aParams['FailURL'] = "https://" . WEBSITEURL . $this->sReturnURL; ///URL to redirect in case of error
        $aParams['LanguageCode'] = "en-GB"; ///hosted payment frame language code
        $aParams['PayInstId'] = ""; ///the encrypted payment instrument id
        $aParams['RefId'] = ""; ///the reference transaction id
        $aParams['ProcessWithRandomCode'] = false; ///the random code flag
        $oResponse = $this->CreateHostedPayment($aParams);
        $this->sGatewayReference = $oResponse->CreateHostedPaymentResult->HPSID;
        $this->updateGatewayReference();
        return $oResponse;
    }

    public function ConfirmPayment($sGatewayID) {
        $bValid = false;
        $aTransaction = $this->searchTransaction($sGatewayID);
        $aConfirmation = $this->GetHostedPayment($sGatewayID);
        $this->sGatewayReference = $sGatewayID;
        $this->iTransactionID = $aTransaction['transaction_id'];
        $this->fAmount = $aTransaction['amount'];
        $this->sTransactionReference = $aTransaction['bookingreference'];
        $oError = new Error();
        if ($aConfirmation->GetHostedPaymentResult->ProceedWithPurchase > 0 && $aTransaction) {
            $this->sGatewayReference = $aConfirmation->GetHostedPaymentResult->HPSID;
            if ($aConfirmation->GetHostedPaymentResult->Status == 'Completed' && $aTransaction['member_id'] == $aConfirmation->GetHostedPaymentResult->ClientReference && $aTransaction['bookingreference'] == $aConfirmation->GetHostedPaymentResult->OrderReference && (($this->getCharges() + $this->fAmount) * 100) == $aConfirmation->GetHostedPaymentResult->Amount) {
                $this->confirmTransaction();
                $this->updateMemberBalance($aTransaction['member_id']);
                $this->sendAddFundsReceipt($aTransaction['member_id']);
                $bValid = true;
            } else {
                $oEmail = new Mailer();
                $oEmail->shortHeader();
                $oEmail->setSubject("Transactium Payment Mismatch");
                $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->sendEmail("<h2>Transactium Payment Failed.</h2>Transactium Response<pre>" . print_r($aConfirmation, true) . "</pre><br/>Database Transaction<pre>" . print_r($aTransaction, true) . "</pre>");
            }
        } elseif ($aConfirmation->GetHostedPaymentResult->ProceedWithPurchase < 1 && $aConfirmation->GetHostedPaymentResult->Status == 'Cancelled') {
            $this->confirmTransaction(false, 4);
        } else {
            $this->confirmTransaction(false, 5);
            if ($aConfirmation->GetHostedPaymentResult->Transactions->Transaction->TransactionResultStatus == "Declined" || $aConfirmation->GetHostedPaymentResult->Transactions->Transaction->TransactionResultStatus == "Blocked") {
                if (in_array($aConfirmation->GetHostedPaymentResult->Transactions->Transaction->FSBlockReason, array("CountryGreylist", "CountryBlacklist"))) {
                    $oError->addError("Credit Card Blocked. Transaction coming from a blocked country.");
                } else {
                    $oError->addError("Credit Card Declined");
                }
            } elseif ($aConfirmation->GetHostedPaymentResult->Transactions->Transaction->TransactionResultStatus == "Error" || $aConfirmation->GetHostedPaymentResult->ErrorCode) {
                if (array_key_exists($aConfirmation->GetHostedPaymentResult->ErrorCode, $this->aGatewayErrors)) {
                    $oError->addError($this->aGatewayErrors[$aConfirmation->GetHostedPaymentResult->ErrorCode]);
                }
                $oEmail = new Mailer();
                $oEmail->shortHeader();
                $oEmail->setSubject("Transactium Payment Failed");
                $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->sendEmail("<h2>Transactium Payment Failed.</h2>Transactium Response<pre>" . print_r($aConfirmation, true) . "</pre><br/>Database Transaction<pre>" . print_r($aTransaction, true) . "</pre>");
            }
        }
        return $bValid;
    }

    protected function searchTransaction($sGatewayReference) {
        if ($sGatewayReference) {
            $sSQL = "SELECT t.amount as amount,
							t.fk_status_id as status_id,
							t.confirmed as confirmed,
							t.fk_member_id as member_id,
							t.transaction_id as transaction_id,
							t.bookingreference as bookingreference,
							t.gatewayreference as gatewayreference
					FROM transactions t
					WHERE
						t.gatewayreference LIKE '{$sGatewayReference}'
						AND
						t.fk_gateway_id = {$this->getGatewayID()}
						";
            return DAL::executeGetRow($sSQL);
        }
        return NULL;
    }

    protected function CTransactiumHPP() {
        $aAccountDetails = explode("|", $this->sAccount);
        $this->oSOAP = new SoapClient($this->sURL);
        $this->oSOAP->__setSoapHeaders(
                array(
                    new SoapHeader(
                            'http://www.transactium.com', 'HPSAuthHeader', array(
                        'Username' => $aAccountDetails[0],
                        'Password' => $aAccountDetails[1]
                            )
                    )
                )
        );
    }

    protected function GetHostedPayment($HPSID) {
        if ($HPSID) {
            $this->CTransactiumHPP();
            return $this->oSOAP->GetHostedPayment(array("HPSID" => $HPSID));
        }
        return NULL;
    }

    protected function CreateHostedPayment($paramarr) {
        $this->CTransactiumHPP();
        return $this->oSOAP->CreateHostedPayment(array("paymentDetails" => $paramarr));
    }

}