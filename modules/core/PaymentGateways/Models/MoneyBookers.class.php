<?php

/**
 * Payment gateway for MoneyBookers
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class MoneyBookers extends PaymentGateways {

    protected $sReturnURL = "https://www.lovelotto.com/system/controls/paymentGateway.php?g=moneybookers";
    protected $sSecret = '';
    protected $sSessionID = "";

    public function __construct() {
        parent::__construct(PaymentGateways::MONEYBOOKERS);
    }

    public function getCharges() {
        return ($this->fAmount * 0.00) + 0.00;
    }

    public function ProcessPayment(&$oPage, &$oMember, $aDetails) {
        $sPaymentMethod = NULL;
        if ($aDetails['pm']) {
            $sPaymentMethod = $aDetails['pm'];
        }
        if ($this->prepareTransaction($oMember, $sPaymentMethod)) {
            $oPage->saveSessionObjects();
            $this->RedirectToGatewayPage();
        }
        return false;
    }

    public function prepareTransaction(&$oMember, $sPaymentMethod) {
        $aMemberDetails = $oMember->getMemberDetails();
        $aParams = array();
        $aParams[] = "";
        $aParams[] = "return_url=http://" . WEBSITEURL . Navigation::getPageURL($this->iConfirmPageID);
        $aParams[] = "cancel_url=http://" . WEBSITEURL . Navigation::getPageURL($this->iCancelPageID);
        $aParams[] = "transaction_id=" . $this->sTransactionReference;
        $aParams[] = "return_url_text=" . "Return to " . WEBSITETITLE;
        $aParams[] = "status_url=" . $this->sReturnURL;
        $aParams[] = "status_url2=" . "payments@lovelotto.com";
        $aParams[] = "language=" . "EN";
        $aParams[] = "prepare_only=" . "1";
        $aParams[] = "rid=" . "18566475";
        $aParams[] = "amount=" . ($this->fAmount + $this->getCharges());
        $aParams[] = "currency=" . "EUR";
        $aParams[] = "merchant_fields=" . "mid,tid";
        $aParams[] = "mid=" . $oMember->getMemberID();
        $aParams[] = "tid=" . $this->iTransactionID;
        $aParams[] = "detail1_description=" . WEBSITETITLE . " Member Funds";
        $aParams[] = "detail1_text=" . $oMember->getFullName();
        $aParams[] = "detail2_description=" . "Member Account Topup";
        $aParams[] = "detail2_text=EUR " . number_format_locale($this->fAmount, 2, '.', ',');
        $aParams[] = "detail3_description=" . "Processing Fees";
        $aParams[] = "detail3_text=EUR " . number_format_locale($this->getCharges(), 2, '.', ',');
        $aParams[] = "pay_from_email=" . $oMember->getUsername();
        $aParams[] = "firstname=" . $aMemberDetails['firstname'];
        $aParams[] = "lastname=" . $aMemberDetails['lastname'];
        $aParams[] = "address=" . $aMemberDetails['address1'];
        $aParams[] = "address2=" . $aMemberDetails['address2'];
        $aParams[] = "phone_number=" . $aMemberDetails['contact'];
        $aParams[] = "postal_code=" . $aMemberDetails['zip'];
        $aParams[] = "city=" . $aMemberDetails['city'];
        if (!is_null($sPaymentMethod)) {
            $aParams[] = "logo_url=" . "https://www.lovelotto.com/system/media/images/web/logo-moneybookers-cc.png";
            $aParams[] = "hide_login=" . "1";
            $aParams[] = "payment_methods=" . $sPaymentMethod;
        } else {
            $aParams[] = "logo_url=" . "https://www.lovelotto.com/system/media/images/web/logo-moneybookers.png";
        }
        $resArray = $this->curl_call(implode("&", $aParams));

        if ($resArray[0]) {
            $this->sSessionID = urldecode($resArray[0]);
        }
        return $resArray;
    }

    public function ConfirmPayment($aDetails) {
        if ($this->validateResponse($aDetails['md5sig'], $aDetails['merchant_id'], $aDetails['transaction_id'], $aDetails['mb_amount'], $aDetails['mb_currency'], $aDetails['status'])) {
            $aTransaction = $this->searchTransaction($aDetails['mid'], $aDetails['tid'], $aDetails['transaction_id']);
            if ($aDetails["status"] >= 0) {
                $this->sGatewayReference = $aDetails["mb_transaction_id"];
                $this->iTransactionID = $aTransaction['transaction_id'];
                $this->fAmount = $aTransaction['amount'];
                $this->sTransactionReference = $aTransaction['bookingreference'];
                if ($aDetails['status'] == 2 && $aTransaction['confirmed'] == 0) {
                    $this->confirmTransaction();
                    $this->updateMemberBalance($aTransaction['member_id']);
                    $this->sendAddFundsReceipt($aTransaction['member_id']);
                } elseif ($aDetails['status'] == 0) {
                    $this->confirmTransaction(false, 2);
                }
            } else {
                $oEmail = new Mailer();
                $oEmail->shortHeader();
                $oEmail->setSubject("Moneybookers Payment Failed");
                $oEmail->setFrom("no-reply@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->addTo("payments@lovelotto.com", "LoveLotto.com Payments");
                $oEmail->sendEmail("<h2>Moneybookers Payment Failed.</h2><pre>" . print_r($aDetails, true) . "</pre>");
            }
        }
    }

    protected function validateResponse($sMD5, $sMerchantID, $sTransactionID, $fAmount, $sCurrency, $iStatus) {
        if ($sMD5 == strtoupper(md5($sMerchantID . $sTransactionID . strtoupper(md5(strtolower($this->sSecret))) . $fAmount . $sCurrency . $iStatus))) {
            return true;
        }
        return false;
    }

    protected function searchTransaction($iMemberID, $iTransactionID, $sTransactionReference) {
        if ($iMemberID && $iTransactionID && $sTransactionReference) {
            $sSQL = "SELECT t.amount as amount,
							t.fk_status_id as status_id,
							t.confirmed as confirmed,
							t.fk_member_id as member_id,
							t.transaction_id as transaction_id,
							t.bookingreference as bookingreference
					FROM transactions t
					WHERE t.transaction_id = {$iTransactionID}
						AND t.fk_member_id = {$iMemberID}
						AND t.bookingreference = '{$sTransactionReference}'
						";
            return DAL::executeGetRow($sSQL);
        }
        return NULL;
    }

    protected function RedirectToGatewayPage() {
        header("Location: " . $this->sURL . "?sid=" . $this->sSessionID);
    }

    protected function curl_call($nvpStr) {
        $nvpreq = "pay_to_email=" . urlencode($this->sAccount) . "&recipient_description=" . urlencode(WEBSITETITLE) . $nvpStr;
        //setting the curl parameters.
        $ch = curl_init($this->sURL);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


        $params = array('http' => array(
                'method' => 'POST',
                'content' => $nvpreq
        ));

        //getting response from server
        $response = curl_exec($ch);
        //convrting NVPResponse to an Associative Array
        $nvpResArray = $this->deformatNVP($response);

        if (curl_errno($ch)) {
            print_d(curl_error($ch));
            //Execute the Error handling module to display errors.
        }
        curl_close($ch);
        return $nvpResArray;
    }

    protected function deformatNVP($nvpstr) {
        $intial = 0;
        $nvpArray = array();
        if (strpos($nvpstr, '=')) {
            while (strlen($nvpstr)) {
                //postion of Key
                $keypos = strpos($nvpstr, '=');
                //position of value
                $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);

                /* getting the Key and Value values and storing in a Associative Array */
                $keyval = substr($nvpstr, $intial, $keypos);
                $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
                //decoding the respose
                $nvpArray[urldecode($keyval)] = urldecode($valval);
                $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
            }
        } else {
            $nvpArray[] = $nvpstr;
        }
        return $nvpArray;
    }

}
