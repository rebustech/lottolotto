<?php

/**
 * Base payment gateway class
 * @package LoveLotto
 * @subpackage PaymentGateways
 */
class PaymentGateways {

    const MONEYBOOKERS = 1;
    const LOVELOTTO = 2;
    const NETELLER = 4;
    const WINNINGS = 7;
    const TRANSACTIUM = 8;
    const PAYFREX = 10;
    const WITHDRAWAL = 11;
    const UKASH = 41;
    const DIRECTDEBIT = 441;
    
    // New gateways to handle withdrawals
    // These won't be called directly but will be used as part of return from Payfrex
    const CC_WITHDRAWAL = 141;
    const NETELLER_WITHDRAWAL = 241;
    const UKASH_WITHDRAWAL = 341;

    // Maximum/minimum topup amounts allowed
    const MINIMUMTOPUP = 1.50;
    //const MAXIMUMTOPUP = 1000;  
    const MAXIMUMTOPUP = 9999999999; // fix modal popping up on high deposits
    
    public $sURL = NULL;
    public $sAccount = NULL;
    public $iPaymentGatewayID = NULL;
    public $fAmount = 0.00;
    public $sGatewayName = NULL;
    public $sGatewayReference = NULL;
    public $sTransactionReference = NULL;
    public $iTransactionID = NULL;
    public $bTestGateway = NULL;
    public $bSkipConfirmationPage = false;
    public $iCancelPageID = 17;
    public $iConfirmPageID = 18;

    public function __construct($iPaymentGatewayID = NULL) {
        $this->assignPaymentGatewayID($iPaymentGatewayID);
    }

    public function assignPaymentGatewayID($iPaymentGatewayID) {
        if ($iPaymentGatewayID) {
            $this->getGatewayDetails($iPaymentGatewayID);
        }
    }

    public function getGatewayID() {
        return $this->iPaymentGatewayID;
    }

    protected function getGatewayDetails($iPaymentGatewayID) {
        $sSQL = "SELECT *
				FROM gateways
				WHERE gateway_id = {$iPaymentGatewayID}
					AND is_active = 1";

        $aGatewayDetails = DAL::executeGetRow($sSQL);
        if ($aGatewayDetails) {
            $this->bTestGateway = $aGatewayDetails['use_test'];
            $this->iPaymentGatewayID = $iPaymentGatewayID;
            $this->sGatewayName = $aGatewayDetails['comment'];
            if ($aGatewayDetails['use_test'] == 1) {
                $this->sURL = $aGatewayDetails['test_url'];
                $this->sAccount = $aGatewayDetails['test_account'];
            } else {
                $this->sURL = $aGatewayDetails['url'];
                $this->sAccount = $aGatewayDetails['account'];
            }
        }
    }

    public function getClassName($iPaymentGatewayID) {
        $sSQL = "SELECT classname
				FROM gateways
				WHERE gateway_id = {$iPaymentGatewayID}";

        return DAL::executeGetOne($sSQL);
    }

    public function getPaymentGateways($bActive = false) {
        $sSQL = "SELECT comment as title,
						gateway_id as gateway_id
				FROM gateways
				WHERE is_active = 1 AND enabled = 1  AND payment_gateway=1
				ORDER BY comment ASC";

        return DAL::executeQuery($sSQL);
    }

    public function getWithdrawalGateways($bActive = false) {
        $sSQL = "SELECT comment as title,
						gateway_id as gateway_id
				FROM gateways
				WHERE is_active = 1 AND enabled = 1 AND withdrawal_gateway=1
				ORDER BY comment ASC";

        return DAL::executeQuery($sSQL);
    }

    public function checkPaymentGateway($iPaymentGatewayID) {
        $sSQL = "SELECT enabled
				FROM gateways
				WHERE is_active = 1
				AND gateway_id = {$iPaymentGatewayID} ";
        return ($iPaymentGatewayID) ? DAL::executeGetOne($sSQL) : false;
    }

    public function getMinimumTopup() {
        return PaymentGateways::MINIMUMTOPUP;
    }

    public function getMaximumTopup() {
        return PaymentGateways::MAXIMUMTOPUP;
    }

    public function assignAmount($fAmount, $bNegative = false) {
        $this->fAmount=$fAmount;
        $bValid = false;
        if ($this->validateAmount($fAmount)) {
            // Extra formatting here to cope with a formatted withdrawal number
            $this->fAmount = ($bNegative) ? sprintf("%.2f", 0 - reverseNumberFormat($fAmount)) : $fAmount;
            $bValid = true;
        }
        return $bValid;
    }

    protected function validateAmount($fAmount) {
        
        // Remove all formatting from the amount before validating
        $fAmount = reverseNumberFormat($fAmount);
        // Raise a message in the audit log to say Payfrex has died.
        $sAuditLogMsg = "Trying to validate $fAmount";
        AuditLog::LogItem($sAuditLogMsg, 'VALIDATE_AMOUNT_ATTEMPT', 'transactions', 0);
        
        $bValid = false;
        if (is_numeric($fAmount) && $fAmount >= PaymentGateways::MINIMUMTOPUP && $fAmount <= PaymentGateways::MAXIMUMTOPUP) {
            $bValid = true;
            $sAuditLogMsg = "Number valid";
            AuditLog::LogItem($sAuditLogMsg, 'VALIDATE_AMOUNT_SUCCESS', 'transactions', 0);
        }
        return $bValid;
    }

    public function getTransactionReference() {
        return $this->sTransactionReference;
    }

    public function getAmount() {
        return $this->fAmount;
    }

    protected function generateTransactionReference() {
        $this->sTransactionReference = TRANSACTIONREFERENCE . time();
    }

    public function getGatewayName() {
        return $this->sGatewayName;
    }
    
    public function getGatewayReference() {
        return $this->sGatewayReference;
    }
    
    public function addTransaction($iMemberID, $iCurrencyID) {
        
        // Run $this->fAmount through the reverse formatter before adding transaction
        $this->fAmount = reverseNumberFormat($this->fAmount);

        // Do a str_replace on the db entered value as well,
        // replacing any commas with decimal points
        // (this should only affect non-UK withdrawals)
        $this->generateTransactionReference();
        $this->iMemberID = $iMemberID;
        $sTablename = "transactions";
        $aData = array(
            'confirmed' => 0,
            'fk_member_id' => $iMemberID,
            'fk_gateway_id' => $this->iPaymentGatewayID,
            'fk_currency_id' => $iCurrencyID,
            'bookingreference' => $this->sTransactionReference,
            'amount' => str_replace(",", ".", $this->fAmount),
            'transaction_date' => date("Y-m-d H:i:s"),
            'fk_status_id' => 1,
            'gatewayreference' => $this->getGatewayName()
        );

        $this->iTransactionID = DAL::Insert($sTablename, $aData, true);

        return $this->iTransactionID;
    }

    protected function updateGatewayReference() {
        $sTablename = "transactions";
        $aData = array(
            'gatewayreference' => $this->sGatewayReference
        );
        $sParams = "transaction_id = " . $this->iTransactionID;
        return DAL::Update($sTablename, $aData, $sParams);
    }

    protected function confirmTransaction($bConfirmed = true, $iStatus = 3) {
        $sTablename = "transactions";
        $bConfirmed = (int) $bConfirmed; // Need to cast to integer here
        $aData = array(
            'confirmed' => $bConfirmed,
            'fk_status_id' => $iStatus,
            'transaction_date' => date("Y-m-d H:i:s"),
            'gatewayreference' => $this->sGatewayReference,
            'fk_gateway_id' => $this->iPaymentGatewayID
        );
        $sParams = "transaction_id = " . $this->iTransactionID;

        $bDone=DAL::Update($sTablename, $aData, $sParams);

        //Update the members balance
        $this->updateMemberBalance($this->iMemberID);

        return $bDone;
    }

    protected function updateMemberBalance($iMemberID) {
        if($iMemberID==0) return;
        $sSQL = "SELECT
					SUM(t.amount)
				FROM transactions t
				WHERE t.fk_member_id = {$iMemberID}
					AND t.confirmed = 1";
        $fBalance = DAL::executeGetOne($sSQL);
        $sTablename = "members";
        $aData = array(
            'balance' => $fBalance
        );
        $sParams = "member_id = {$iMemberID}";

        return DAL::Update($sTablename, $aData, $sParams);
    }

    protected function getMemberDetails($iMemberID) {
        $sSQL = "SELECT
						m.firstname,
						m.lastname,
						m.email,
						m.balance
				FROM members m
				WHERE m.is_active = 1
					AND m.member_id = {$iMemberID} ";

        return DAL::executeGetRow($sSQL);
    }

    public function updateTransaction($iMemberID) {
        $sSQL = "SELECT t.fk_status_id as status_id,
						t.confirmed as confirmed,
						t.gatewayreference as gatewayreference
				FROM transactions t
				WHERE t.fk_member_id = {$iMemberID}
				AND t.transaction_id = " . $this->iTransactionID;

        #echo($sSQL);

        $aDetails = DAL::executeGetRow($sSQL);

        $this->sGatewayReference = $aDetails['gatewayreference'];
        return $aDetails['status_id'];
    }

    protected function sendAddFundsReceipt($iMemberID) {

        // To ensure that we get the most up-to-date information from the database
        // we'll pause for a couple of seconds here just to let everything catch up
        sleep(5);

        // Get the member details
        $aMemberDetails = $this->getMemberDetails($iMemberID);


        // Return the balance value to 2 decimal places, rounded down
        // This will then be further formatted to 2display as 2 decimal places
        $aMemberDetails['balance'] = floor($aMemberDetails['balance'] * 100) / 100;

        // Format the current balance
        $fCurrentBalance = number_format_locale($aMemberDetails['balance'], 2, '.', ',');

        // and the amount
        $fAmount = number_format_locale($this->fAmount, 2, '.', ',');

        // Create personalisation array for email
        
        // OLD ARRAY PREVIOUS TO SEP 12
        /*
        $aPersonalisationData = array('username'        => $aMemberDetails['email'],
                                      'current balance' => $fCurrentBalance,
                                      'total'           => $fAmount,
                                      'payment date'    => date("d/m/Y H:i:s"),
                                      'reference'       => $this->sTransactionReference,
                                      'firstname'       => $aMemberDetails['firstname'],
                                      'member_id'       => $iMemberID,
                                      'cc_ref'          => $this->sGatewayReference,
                                      'email_ref'       => 'add_funds');

        // NEW ARRAY FROM SEP 12
        $aPersonalisationData = array('username'        => $aMemberDetails['email'],
                                      'current balance' => $fCurrentBalance,
                                      'payment_amount'  => $fAmount,
                                      'payment_date'    => date("d/m/Y H:i:s"),
                                      'order_number'    => $this->sTransactionReference,
                                      'payment_method'  => $this->sGatewayName,
                                      'firstname'       => $aMemberDetails['firstname'],
                                      'member_id'       => $iMemberID,
                                      'cc_ref'          => $this->sGatewayReference,
                                      'email_ref'       => 'add_funds');
        */
        
        $aPersonalisationData = array('current balance' => $fCurrentBalance,
                                      'deposit_amount'  => $fAmount,
                                      'payment_date'    => date("d/m/Y H:i:s"),
                                      'transaction_id'  => $this->sTransactionReference,
                                      'payment_method'  => $this->sGatewayName,
                                      'firstname'       => $aMemberDetails['firstname'],
                                      'member_id'       => $iMemberID,
                                      'email_ref'       => 'add_funds');
 
 
        /*Email::addFundsReceipt($aMemberDetails['firstname'],
                               $aMemberDetails['lastname'],
                               $aMemberDetails['email'],
                               $this->sTransactionReference,
                               $this->sGatewayName,
                               date("Y-m-d H:i:s"),
                               $this->fAmount,
                               $aMemberDetails['balance']);*/

        Email::addFundsReceipt($iMemberID, $aMemberDetails['email'], $aPersonalisationData);
    }

    public function cancelTransaction() {
        if ($this->iTransactionID) {
            $this->confirmTransaction(false, 4);
        }
    }

    public function skipConfirmationPage() {
        return $this->bSkipConfirmationPage;
    }

    public function getCancelPage() {
        return Navigation::getPageURL($this->iCancelPageID);
    }

    public function getSuccessPage() {
        return Navigation::getPageURL($this->iConfirmPageID);
    }

    public function getCharges() {
        return 0;
    }
    
    
    /**
     * Insert new direct debit record for this member
     * @param array $aDirectDebitInformation information about the DD
     * @return integer
     */
    public function insertDirectDebitRecord($aDirectDebitInformation)
    {
        $dCreationDate = date ("Y-m-d H:i:s");

        $sTablename = "members_dd";
        $aData = array(
            'member_id'         => $aDirectDebitInformation['customerNumber'],
            'pf_transaction_id' => $aDirectDebitInformation['transactionId'],
            'creation_date'     => $dCreationDate,
            'original_transaction'  => $this->iTransactionID,           
            'amount' => $aDirectDebitInformation['amount'],
            'details' => $this->encryptDirectDebitDetails($aDirectDebitInformation, $this->iTransactionID)
        );
            
        return DAL::Insert($sTablename, $aData, true);
    }
    
    /**
     * Update the status of this direct debit
     * @param integer $iStatusID status
     * @param integer $iDirectDebitID direct debit to update
     * @return boolean
     */
    public function updateDirectDebitStatus($iStatusID, $iDirectDebitID)
    {
        // Possible statuses:
        //  0 - created
        //  1 - active
        //  2 - cancelled
        
        $sTablename = "members_dd";
        $iStatusID = (int) $iStatusID; // Need to cast to integer here
        
        $aData = array(
            'status' => (int) $iStatusID
        );
        $sParams = "dd_id = " . (int) $iDirectDebitID;

        $bDone=DAL::Update($sTablename, $aData, $sParams);       
        
        return $bDone;
        
    }
    
    
    
    /**
     * Get a direct debit record for this member and decrypt information stored
     * @param integer $iMemberID member ID
     * @param integer $iOriginalTransactionID original transactiion id
     * @return array
     */
    public function getDirectDebitRecord($iMemberID, $iOriginalTransactionID)
    {
        $sSQL = "SELECT * FROM members_dd " .
                "WHERE member_id = {$iMemberID} " .
                "AND original_transaction = {$iOriginalTransactionID} " .
                "ORDER BY creation_date DESC LIMIT 1";

            
        $aRow = DAL::executeGetRow($sSQL);      
        
        // Get encrypted details and decrypt
        $sEncrypted = $aRow['details'];
        $aRow['decrypted'] = decryptDirectDebitDetails($sEncrypted, $iOriginalTransactionID);
        
        return $aRow;
    }
    
    /**
     * Encode and encrypt returned direct debit details ready for storage
     * 
     * @param array $aDirectDebitDetails array of direct debit details
     */
    public function encryptDirectDebitDetails($aDirectDebitInformation, $sKey)
    {
        // Hash the key
        $sKey = hash('md5', $sKey);
        
        $aEncryptionArray = array(
            'bic'  => $aDirectDebitInformation['customerBic'],
            'iban'  => $aDirectDebitInformation['customerIban'],
            'bank_invoice_num'  => $aDirectDebitInformation['invoiceNumber'],
            'customer_name'  => $aDirectDebitInformation['customerName'],
            'bankcode'  => $aDirectDebitInformation['pspBankCode'],
            'account_number'  => $aDirectDebitInformation['pspAccountNumber'],
            'psp_name'  => $aDirectDebitInformation['pspName'],
            'merchant_information' => $aDirectDebitInformation['merchantInformation'],
            'mandate_reference'  => $aDirectDebitInformation['mandateReference'],
            'description'  => $aDirectDebitInformation 
         );
        
        
        $sStringToEncrypt = json_encode($aEncryptionArray);
        return Encryption::mpEncrypt($sStringToEncrypt, $sKey);
        
    }
    
    /**
     * Decrypt and decpde stored direct debit details and return as array
     * 
     * @param array $aDirectDebitDetails array of direct debit details
     */
    public function decryptDirectDebitDetails($sStringToDecrypt, $sKey)
    {
        // Hash the key
        $sKey = hash('md5', $sKey);        
        
        $sDecryptedString = Encryption::mpDecrypt($sStringToDecrypt, $sKey);
        
        $aDecryptedArray= json_decode($sDecryptedString);
        
        return $aDecryptedArray;
    }
}