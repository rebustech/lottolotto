<?php

/**
 * Uses the built in class listing to produce a dropdown choice of classes from the
 * folder named by $sClassesPath
 */
class FormField_gateways_classname extends FormFieldClassFileListing{
    var $sClassesPath='modules/PaymentGateways/models';
}