<?php

class VCMS{
    static function get($sKey,$aParameters=null,$sHandler=''){
        $oBlock=\LL\VariantsCMS\VariantBlock::getVariantByKey($sKey);

        if($aParameters!==null) $oBlock->setParameters($aParameters);

        if($_GET['showtags']==1){
            return $sKey;
        }

        if($_GET['liveedit']==1){
            return '<liveedit title="CMSKEY:'.$sKey.'" onClick="window.open(\'http://admin.dev.maxlotto.com/administration/generic-details.php?tablename=variants_blocks&id='.$oBlock->fk_block_id.'&pp=generic-listing.php\')">'.$oBlock->getValue().'</liveedit>';
        }


        if($_GET['highlightkey']==$sKey){
            return '<highlight style="outline:solid 6px yellow">'.$oBlock->getValue().'</highlight>';
        }

        $sValue=$oBlock->getValue();

        if($sValue==$sKey){
            $sValue=end(explode('.',$sKey));
        }

        return $sValue;
    }

    static function getWidgetPod($iWidgetId){
        if(empty($iWidgetId)) {
            return;
        }
        $aResult=\DAL::executeGetOne('SELECT view_name FROM cms_widgets WHERE id='.$iWidgetId);
        return 'pods/'.$aResult;
    }
}
