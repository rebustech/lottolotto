<?php



class translation_queue extends AdminController{
    /**
     * Decide what to show in a generic details view
     */
    function index(){
        $oView=new LLView();

        $sLanguageFilter=($_POST['languagefilter'])?'l.language_id='.$_POST['languagefilter']:'l.is_active=1';
        $sGroupFilter=($_POST['groupfilter'])?' AND b.group=\''.$_POST['groupfilter'].'\'':'';
        $sWordFilter=($_POST['wordfilter'])?' AND c.value like \'%'.str_replace(' ','%',$_POST['wordfilter']).'%\'':'';

        switch($_POST['statusfilter']){
            case '1':
                //Done
                $sStatusFilter=' AND (c.version=v.version)';
                break;
            case '2':
                //All
                $sStatusFilter='';
                break;
            case '3':
                //Missing
                $sStatusFilter=' AND (c.id IS NULL)';
                break;
            case '4':
                //Needs Translating
                $sStatusFilter=' AND (c.version<v.version)';
                break;
            default:
                //Needs Doing
                $sStatusFilter=' AND (c.version<v.version OR c.id IS NULL)';
                break;
        }


        $sSQL='SELECT b.id AS block_id,b.key,b.group,b.tag,b.first_seen,v.id AS variant_id,v.name AS variant_name,v.version AS variant_version,l.language_id,l.title as language,l.icon,c.id AS content_id,c.version AS content_version FROM variants_blocks b
   INNER JOIN variants_variants v ON b.id=v.fk_block_id
   INNER JOIN languages l ON '.$sLanguageFilter.'
    LEFT JOIN variants_variant_content c ON c.fk_language_id=l.language_id AND c.fk_variant_id=v.id
        WHERE 1=1 '.$sGroupFilter.$sWordFilter.$sStatusFilter.' ORDER BY b.id DESC
        LIMIT 0,20';

        $oView->sql=$sSQL;

        $oView->aData=\DAL::executeQuery($sSQL);
        $oView->aLanguages=\Language::getLanguages();
        $oView->aGroups=\DAL::executeQuery('select distinct(`group`) as `group` from variants_blocks');

        return $oView->output('translation_queue');
    }


    function summary(){

        $sSQL='SELECT `group`,
                COUNT(*) AS total_blocks, ';

        $aLangs=\Language::getLanguages();
        foreach($aLangs as $oLang){
            $sSQL.='SUM(c_'.$oLang['code'].'.value IS NULL) AS '.$oLang['code'].'_missing,';
            $sSQL.='SUM(c_'.$oLang['code'].'.value IS NOT NULL) AS '.$oLang['code'].'_done,';
            $sSQL.='SUM(c_'.$oLang['code'].'.value IS NOT NULL AND (COALESCE(c_'.$oLang['code'].'.version,0) < COALESCE(c_en.version,0))) AS '.$oLang['code'].'_needs_trans,';
        }

        $sSQL.='1 as dummyvalue
                FROM variants_blocks b
                INNER JOIN variants_variants v ON v.fk_block_id=b.id';

        foreach($aLangs as $oLang){
                $sSQL.=' LEFT JOIN variants_variant_content c_'.$oLang['code'].' ON c_'.$oLang['code'].'.fk_variant_id=v.id AND c_'.$oLang['code'].'.fk_language_id='.$oLang['language_id'];
        }


        $sSQL.=' WHERE `group` !=""
                GROUP BY `group`';

        $aSummary=\DAL::executeQuery($sSQL);

        $oView=new LLView();

        $oView->aLanguages=\Language::getLanguages();
        $oView->aSummary=$aSummary;

        return $oView->output('translation_summary');

    }
}