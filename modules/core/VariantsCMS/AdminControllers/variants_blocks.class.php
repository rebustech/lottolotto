<?php
/**
 * Game engines admin controller
 * @package LoveLotto
 * @subpackage AdminControllers
 */
class variants_blocks extends AdminController{

    /**
     * Decide what to show in a generic details view
     */
    function GenericDetails($sTablename, $iId = 0, $aDefaultData = NULL, $sPreviousPage = "", $iWebsiteID = 0, $bDeleteButton = false, $iReturnID = NULL){
        $sFileName = basename($_SERVER['PHP_SELF']);
        if($iId==0){
            $tabs=new TabbedInterface();

            //Create a tab to show assigned users
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-magnet';
            $oTitle=new HtmlTagControl('h1','Create Content Block');
            $aItems[]=$oTitle->Output();
            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

            $tabs->aItems['Items']=$oItemsTab;
            $aItems[]=$tabs->Output();

        }else{
            $tabs=new TabbedInterface();

            //Create a tab to show assigned users
            $oItemsTab=new TabbedInterface(lang::get('details'));
            $oItemsTab->sIcon='fa-magnet';
            $oTitle=new HtmlTagControl('h1','Edit Content Block');
            $aItems[]=$oTitle->Output();

            $oEditControl=new FormField_variants_blocks();
            $oVariantBlock=\LL\VariantsCMS\VariantBlock::getById($iId);
            $oEditControl->sKey=$oVariantBlock->key;
            $oItemsTab->AddControl($oEditControl);

            $oItemsTab->aItems[]=GenericTableAdmin::createGenericDetailsForm($sTablename, $iId , $aDefaultData , $sPreviousPage, $iWebsiteID, $bDeleteButton, $iReturnID);

            $tabs->aItems['Items']=$oItemsTab;

            //Create a tab to show audit log
            $oVariantsTab=new TabbedInterface(lang::get('variants'));
            $oVariantsTab->sIcon='fa-cubes';
            $aVariantsData=DAL::executeQuery('SELECT * FROM variants_variants WHERE fk_block_id='.intval($iId));
            $oVariantsTable=new TableControl($aVariantsData);
            $oVariantsTable->getFieldsFromTable('variants_variants');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oVariantsTable->keepFields('variants_variants','id,name,valid_from,valid_to');
            $oVariantsTab->AddControl($oVariantsTable);
            $tabs->aItems['variants']=$oVariantsTab;

            //Create a tab to show audit log
            $oLogTab=new TabbedInterface(lang::get('audit_log'));
            $oLogTab->sIcon='fa-th-list';
            $aItemsData=DAL::executeQuery('SELECT * FROM audit_log WHERE model=\'variants_blocks\' AND fk_id='.intval($iId).' ORDER BY `datetime` DESC LIMIT 0,50');
            $oItemsTable=new TableControl($aItemsData);
            $oItemsTable->getFieldsFromTable('audit_log');
            //$oItemsTable->addField(new TableField('admintype','varchar','User Type',''));
            $oItemsTable->keepFields('audit_log','datetime,code,message');
            $oLogTab->AddControl($oItemsTable);

            $tabs->aItems['rights']=$oLogTab;
            $aItems[]=$tabs->Output();
        }


        $sOut='';
        foreach($aItems as $item){
            $sOut.=$item;
        }

        return $sOut;

    }
}