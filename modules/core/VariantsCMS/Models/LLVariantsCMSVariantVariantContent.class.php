<?php
/**
 * A block is an individual block of content. Each block will examine environmental
 * conditions to determine which variant to use. This is faily intensive so will
 * need to make good use of memcached
 */

namespace LL\VariantsCMS;

class VariantVariantContent extends \LLModel{
    static $sTableName='variants_variant_content';
    var $id;

    var $fk_language_id=1;
    var $value;
    var $datechanged;
    var $fk_author_id;
    var $translation_required=0;
    var $fk_variant_id;
    var $version=0;

    function save($aData=array()){
        $this->datechanged=date('Y-m-d H:i:s');
        $this->version=intval($this->version)+1;

        $oVariant=$this->getVariantObject();

        if($this->version<$oVariant->version){
            $this->version=intval($oVariant->version);
        }

        $aData['fk_language_id']=$this->fk_language_id;
        $aData['value']=$this->value;
        $aData['datechanged']=$this->datechanged;
        $aData['fk_author_id']=$this->fk_author_id;
        $aData['version']=$this->version;
        $aData['fk_variant_id']=$this->fk_variant_id;

        parent::save($aData);

        //Update block and variant version to max version of any content. This will
        //Identify any translations that need checking
        if($this->translation_required==0){
            $sSQL='UPDATE variants_variants v SET v.version=(SELECT MAX(c.version) FROM variants_variant_content c WHERE v.id=c.fk_variant_id) WHERE id='.$this->fk_variant_id;
            \DAL::Query($sSQL);
            $sSQL='UPDATE variants_blocks v SET v.version=(SELECT MAX(c.version) FROM variants_variants c WHERE v.id=c.fk_block_id AND c.id='.$this->fk_variant_id.')';
            \DAL::Query($sSQL);
        }
    }

    function getVariantObject(){
        return \LL\VariantsCMS\VariantVariant::getById($this->fk_variant_id);
    }

}