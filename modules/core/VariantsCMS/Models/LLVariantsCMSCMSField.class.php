<?php

namespace LL\VariantsCMS;

class CMSField extends \ConfigurationEngineField{
    var $sView='VariantsCMS/Views/formField';
    var $sPopupView='VariantsCMS/Views/contentEditorPopup';

    /**
     * Used to track if the variants.js script has been added yet
     * @var bool
     */
    static $bScriptsAdded=false;

    function __construct($sName = '', $sCaption = '', $bIsRequired = true) {
        if(!\CMSPageBuilder::$bScriptsAdded){

            //Add the game builder javascript just before /body (and after the above JSON output)
            \LLResponse::$sPostBody.=\LLResponse::addScript('/administration/API/LLVariantsCMSAssets/pageBuilderScripts');
            \LLResponse::$sPostBody.='<link href="/administration/API/LLVariantsCMSAssets/CSS" rel="stylesheet" type="text/css">';

            \CMSPageBuilder::$bScriptsAdded=true;
        }

        if(!self::$bScriptsAdded){
            \LLResponse::$sPostBody.=\LLResponse::addScript('/administration/API/LLVariantsCMSAssets/variantsScript');


            $oPopupView=new \LLView();
            \LLResponse::$sPostBody.=$oPopupView->output($this->sPopupView);

            self::$bScriptsAdded=true;
        }

        parent::__construct($sName, $sCaption, $bIsRequired);
    }
}