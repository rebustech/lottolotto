<?php
/**
 * A block is an individual block of content. Each block will examine environmental
 * conditions to determine which variant to use. This is faily intensive so will
 * need to make good use of memcached
 */

namespace LL\VariantsCMS;

class VariantVariant extends \LLModel{
    static $sTableName='variants_variants';
    var $id;

    var $name='';
    var $fk_campaign_id=0;
    var $valid_from;
    var $valid_to;
    var $active=1;
    var $notloggedin=1;
    var $loggedin=1;
    var $fk_block_id;
    var $version=0;

    var $aParameters=array();
    var $sOutputHandler='';

    /**
     * Gets the variant. If no environment overrides are set just uses the
     * current environment as best it can
     */
    function getSections(){

    }

    function getContentTree(){

    }

    function getContent(){
        return VariantVariantContent::getAllWhere('`fk_variant_id`='.$this->id);
    }

    function save($aData=array()){
        $aData['name']=$this->name;
        $aData['fk_campaign_id']=$this->fk_campaign_id;
        $aData['valid_from']=date('Y-m-d',strtotime($this->valid_from));
        $aData['valid_to']=date('Y-m-d',strtotime($this->valid_to));
        $aData['active']=$this->active;
        $aData['notloggedin']=$this->notloggedin;
        $aData['loggedin']=$this->loggedin;
        $aData['fk_block_id']=$this->fk_block_id;
        $aData['version']=$this->version;

        parent::save($aData);
    }

    function getLanguageObject(){

    }

    function getAuthorObject(){

    }

    function getCampaignObject(){

    }

    function getBlockObject(){
        return \LL\VariantsCMS\VariantBlock::getById($this->fk_block_id);
    }

    function getValue(){
        $sOut=$this->value;
        foreach($this->aParameters as $sParameterName=>$sParameterValue){
            $sOut=str_replace('{'.$sParameterName.'}',$sParameterValue,$sOut);
        }
        return $sOut;
    }

    function setParameters($aParameters){
        $this->aParameters=array_merge($aParameters,$this->aParameters);
    }

}