<?php
/**
 * A block is an individual block of content. Each block will examine environmental
 * conditions to determine which variant to use. This is faily intensive so will
 * need to make good use of memcached
 */

namespace LL\VariantsCMS;

class VariantBlock extends \LLModel{
    static $sTableName='variants_blocks';
    var $id;

    var $key;
    var $comments;
    var $tag;
    var $group;
    var $fieldtype=1;
    var $first_seen='';
    var $version=0;

    /**
     * Gets a block by it's key
     * @param string $sKey
     * @return \LL\VariantsCMS\VariantBlock
     */
    static function getByKey($sKey){
        return self::getByField('key', $sKey);
    }

    /**
     * Shortcut to get a block then get the appropriate variant
     * @param string $sKey
     * @return \LL\VariantsCMS\VariantVariant
     */
    static function getVariantByKey($sKey,$sGroup=null,$sTag=null){
        $oBlock=self::getByKey($sKey);
        $aKeyParts=explode('.',$sKey);
        if($aKeyParts[0]=='CMSField') array_shift($aKeyParts);

        if($oBlock->id==0){
            $oBlock=new \LL\VariantsCMS\VariantBlock();
            $oBlock->key=$sKey;
            $oBlock->group=($sGroup===null)?$aKeyParts[0]:$sGroup;
            $oBlock->tag=($sTag===null)?$aKeyParts[1]:$sTag;
            $oBlock->save();
            $oBlock->createVariant('Default');
        }else{
            if(($oBlock->first_seen=='' || $oBlock->first_seen==null) && substr_count($_SERVER['REQUEST_URI'],'API')==0){
                $oBlock->first_seen=$_SERVER['REQUEST_URI'];
                $oBlock->save();
            }
        }

        $oVariant=$oBlock->getAppropriateVariant();

        if($oVariant->fk_block_id===null) $oVariant->fk_block_id=$oBlock->id;

        return $oVariant;
    }

    /**
     * Gets the variant. If no environment overrides are set just uses the
     * current environment as best it can
     */
    function getAppropriateVariant($aEnvironment=null){
        if($aEnvironment===null){
            $aEnvironment=array();
            $aEnvironment['c`.`fk_language_id']=\Config::$config->iLanguageId;
        }

        # Make some random key
        $sCacheKey = 'VCMS_variantBlock-'.$this->id;

        if(!empty($aEnvironment)) {
            $sCacheKey .= '_env';
            foreach($aEnvironment as $key => $value) {
                $sCacheKey .= '_'.str_replace('c`.`', '', $key).'-'.crc32($value);
            }
        }

        if(static::$bCacheLevel >= \Config::$config->bCacheLevel){
            $aRecord = \LLCache::getObject($sCacheKey);
            if(!empty($aRecord)) {
                $oVariant = new \LL\VariantsCMS\VariantVariant();
                $oVariant->populateFromArray($aRecord);
                return $oVariant;
            }
        }

        /**
         * Start by getting all active in-date-range variants in the current language
         */
        $sSQL='SELECT * FROM variants_variants v
                       INNER JOIN variants_variant_content c
                               ON c.fk_variant_id=v.id
                       WHERE ';

        foreach($aEnvironment as $sFilterKey=>$sFilterValue){
            $sSQL.="`{$sFilterKey}`='{$sFilterValue}' AND ";
        }
        $sSQL.="         (v.valid_from<'2000-01-01' OR v.valid_from>NOW())
                         AND (v.valid_to<'2000-01-01' OR v.valid_to<NOW())
                         AND v.active=1 AND v.fk_block_id=".$this->id;


        $aRecord=\DAL::executeGetRow($sSQL);
        if($aRecord==null){
            //Try fallback language
            if(sizeof($aEnvironment)>0){
                unset($aEnvironment['c`.`fk_language_id']);
                return $this->getAppropriateVariant($aEnvironment);
            }else{
                $oVariant=new \LL\VariantsCMS\VariantVariant();
                $oVariant->value=$this->key;
            }
        } else {
            $oVariant=new \LL\VariantsCMS\VariantVariant();
            $oVariant->populateFromArray($aRecord);

            # Cache length, some random on top
            # @todo, as the translation velocity slows, increase this
            $cacheTime = 60 + rand(15, 75);

            # Whack it in the cache
            if(static::$bCacheLevel >= \Config::$config->bCacheLevel){
                \LLCache::addObject($sCacheKey, $aRecord, $cacheTime);
            }
        }

        /**
         * Further filtering to come later
         */
        return $oVariant;
    }

    /**
     * Creats a new variant in this block and saves
     * @param string $sName
     */
    function createVariant($sName=''){
        $oVariant=new \LL\VariantsCMS\VariantVariant();
        $oVariant->name=$sName;
        $oVariant->fk_block_id=$this->id;
        $oVariant->save();
    }

    /**
     * Gets an array of all variants available for this block
     */
    function listAllVariants(){
        $sSQL='SELECT * from variants_variants WHERE fk_block_id='.intval($this->id);
        return \DAL::executeQuery($sSQL);
    }

    /**
     * Saves the Block to the database
     * @param array optional $aData For subclasses pass in an array of data to be saved along with this
     */
    function save($aData=array()){
        if(($this->first_seen=='' || $this->first_seen==null) && substr_count($_SERVER['REQUEST_URI'],'API')==0) $this->first_seen=$_SERVER['REQUEST_URI'];


        $aData['key']=$this->key;
        $aData['comments']=$this->comments;
        $aData['fieldtype']=$this->fieldtype;
        $aData['tag']=$this->tag;
        $aData['group']=$this->group;
        $aData['first_seen']=$this->first_seen;
        $aData['version']=$this->version;

        parent::save($aData);
    }
}
