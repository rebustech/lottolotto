<?php

//namespace LL\VariantsCMS;

class LLVariantsCMSAssetsApi extends \LL\AjaxController{

    function pageBuilderScripts(){
        header('Content-type: text/javascript');
        $aFiles=array(__DIR__.'/../../CMS/Scripts/froala/js/froala_editor.min.js',
                      __DIR__.'/../../CMS/Scripts/pageBuilder.js');

        die($this->_getFiles($aFiles));
    }

    function CSS(){
        header('Content-type: text/css');
        die($this->_getFiles(array(__DIR__.'/../../CMS/Scripts/froala/css/froala_editor.min.css')));
    }

    function variantsScript(){
        header('Content-type: text/javascript');
        die($this->_getFiles(array(__DIR__.'/../Scripts/variants.js')));
    }

}