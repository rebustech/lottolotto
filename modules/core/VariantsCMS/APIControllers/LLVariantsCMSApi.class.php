<?php
/**
 * API Controller for Variants CMS
 *
 * @package LoveLotto
 * @subpackge Variants CMS
 * @author J.Patchett
 */
class LLVariantsCMSApi extends \LL\AjaxController{

    /**
     * Gets details of the given block id. If no block id is provided a new
     * empty block and default variant will be created and returned instead
     */
    function GetBlock(){
        $sKey=$_POST['key'];
        try{
            $aKeyParts=explode('.',$sKey);
            if($aKeyParts[0]=='CMSField') array_shift($aKeyParts);

            $this->oBlock=\LL\VariantsCMS\VariantBlock::getByKey($sKey);
            if($this->oBlock->id==0){
                $this->oBlock=new \LL\VariantsCMS\VariantBlock();
                $this->oBlock->key=$sKey;
                $this->oBlock->group=$aKeyParts[0];
                $this->oBlock->tag=$aKeyParts[1];
                $this->oBlock->save();
                $this->oBlock->createVariant('Default');
                new \LL\API\Message('LL.Variants.CMS.API.CreatedNewBlockAndVariant');
            }
            $this->aVariants=$this->oBlock->listAllVariants();
        }catch(Exception $e){
            throw new \LL\API\Exception($e->getCode());
        }
        $this->output();
    }

    function GetVariant(){
        $iID=intval($_POST['iID']);
        try{
            $this->oVariant=\LL\VariantsCMS\VariantVariant::getById($iID);

            $this->oVariant->oContent=$this->oVariant->getContent();

            //Correct dates
            $valid_from=strtotime($this->oVariant->valid_from);
            $valid_to=strtotime($this->oVariant->valid_to);
            if($valid_from>0) $this->oVariant->valid_from=date('Y-m-d',$valid_from);
            if($valid_to>0) $this->oVariant->valid_to=date('Y-m-d',$valid_to);

        } catch (Exception $ex) {
            throw new \LL\API\Exception($e->getCode());
        }
        $this->output();
    }

    function Save(){
        $iID=intval($_POST['id']);
        try{


            if($iID>0){
                $this->oVariant=\LL\VariantsCMS\VariantVariant::getById($iID);
            }else{
                $this->oVariant=new \LL\VariantsCMS\VariantVariant();
            }
            $this->oVariant->populateFromArray($_POST);
            $this->oVariant->save();

            //Apply Customer Type

            //Apply Countries

            //Apply White Labels

            //Save changes to the block (only type at the moment)
            $oBlock=$this->oVariant->getBlockObject();
            if($oBlock->fieldtype!=$_POST['blockfieldtype']){
                $oBlock->fieldtype=$_POST['blockfieldtype'];
                $oBlock->save();
            }

            //Apply Content Blocks
            foreach($_POST['oContent'] as $aContent){
                if($aContent['id']>0){
                    $oContent= \LL\VariantsCMS\VariantVariantContent::getById($aContent['id']);
                }else{
                    $oContent=new \LL\VariantsCMS\VariantVariantContent();
                }

                if($oContent->is_locked==1 || $oBlock->is_locked==1){
                    new \LL\API\Message('LL.VariantsCMS.API.ContentLocked');                    
                }else{
                
                    //For some reason we get <p>?</p> from froala when there's no content
                    $aContent['value']=str_replace('<p></p>','',$aContent['value']);
                    $aContent['value']=str_replace('<p>​</p>','',$aContent['value']);
                    $aContent['value']=str_replace('<p>?</p>','',$aContent['value']);
                    $aContent['value']=str_replace('<p>null</p>','',$aContent['value']);

                    if($oContent->value!=$aContent['value']){
                        $oContent->populateFromArray($aContent);
                        if($aContent['id']>0 || ($aContent['id']==0 && trim($aContent['value'])!='')){
                            $oContent->save();
                        }
                    }
                }
            }



            new \LL\API\Message('LL.VariantsCMS.API.VariantSaved');
        } catch (Exception $ex) {
            throw new \LL\API\Exception($e->getCode());
        }
        $this->output();
    }

    function uploadImage(){
        // Allowed extensions.
        $aAllowedExtensions = (isset(Config::$config->allowedExtensions))?explode(',',Config::$config->allowedExtensions):array("gif", "jpeg", "jpg", "png");
        $aAllowedMimeTypes = (isset(Config::$config->allowedMimeTypes))?explode(',',Config::$config->allowedMimeTypes):array("image/gif", "image/jpeg", "image/jpg", "image/pjpeg","image/x-png","image/png");
        if(!isset(Config::$config->sDataPath)) throw new LottoException('VariantsCMSAPI.uploadImage.DataPathNotSet','Data path not set');

        $sPathName=Config::$config->sDataPath.'cmsimages/';

        $sCheckPathName=str_replace('/','\\',$sPathName);
        $aPathSegments=explode('\\',$sCheckPathName);
        $sCheckPath=  array_shift($aPathSegments);
        foreach($aPathSegments as $sPathSegment){
            $sCheckPath.='/'.$sPathSegment;
            @mkdir($sCheckPath);
            @chmod($sCheckPath,0777);
        }

        // Get filename.
        $temp = explode(".", $_FILES["file"]["name"]);

        // Get extension.
        $extension = end($temp);

        // An image check is being done in the editor but it is best to
        // check that again on the server side.
        if (in_array($_FILES["file"]["type"], $aAllowedMimeTypes) && in_array($extension, $aAllowedExtensions)) {
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            // Save file in the uploads folder.
            move_uploaded_file($_FILES["file"]["tmp_name"], $sPathName.$name);

            // Should really reduce file size here (maybe)

            // Generate response.
            $response = new StdClass;
            $response->link = '/cmsimages/'.$name;

            die(stripslashes(json_encode($response)));
        }
    }
}