var variantsCMS={
    bSetupDone:false,
    templates:{
        blockVariantList:null,
        blockVariantItem:null
    },
    currentEditor:{
        sFieldName:null,
        iActiveVariantId:null,
        sActiveBlockKey:null,
        oBlock:null,
        oVariant:null,
        oContent:null,
        gotolanguage:null,
        gotovariant:null
    },
    setup:function(){
        if(!variantsCMS.bSetupDone){
            variantsCMS.bSetupDone=true;
            $('.CMSField:not(.openEditorEventAttached)').addClass('openEditorEventAttached').click(variantsCMS.openEditor);
            variantsCMS.obtainTemplates();
            $('.contentEditorActionSave').click(variantsCMS.saveVariant);
            $('.contentEditorSelectVariantLanguage li').click(variantsCMS.showLanguage);

            $('.contentEditorPopup select[data-bind="block.fieldtype"]').change(variantsCMS.changeFieldType);
        }
    },
    changeFieldType:function(){
        variantsCMS.currentEditor.oBlock.fieldtype=$('.contentEditorPopup select[data-bind="block.fieldtype"]').val();
        variantsCMS.saveVariant();
    },
    createVariant:function(){
        variantsCMS.currentEditor.oVariant={
            id:null,
            name:'New Block',
            fk_language_id:1,
            loggedin:1,
            notloggedin:1,
            fk_block_id:variantsCMS.currentEditor.oBlock.id
        }
        $.ajax(
            {
                url:'/administration/API/LLVariantsCMS/Save',
                async:false,
                success:variantsCMS.afterSave,
                dataType:'JSON',
                type:"POST",
                data:variantsCMS.currentEditor.oVariant
            }
        );

    },
    saveVariant:function(){
        //Update current language with edited content
        if(variantsCMS.currentEditor.oContent!=null){
            switch(variantsCMS.currentEditor.oBlock.fieldtype){
                case "1":
                    variantsCMS.currentEditor.oContent.value=$('.contentEditorPopup .contentEditorText input').val();
                    break;
                case "2":
                    variantsCMS.currentEditor.oContent.value=$('.contentEditorPopup .froala-element').html();
                    break;
                case "3":
                    break;
                case "4":
                    variantsCMS.currentEditor.oContent.value=$('.contentEditorRaw textarea').val();
                    break;
            }
        }
        variantsCMS.currentEditor.oVariant.name=$('.contentEditorPopup input[data-bind="editvariant.name"]').val();
        variantsCMS.currentEditor.oVariant.loggedin=($('.contentEditorPopup input[data-bind="editvariant.loggedin"]').is(':checked'))?1:0;
        variantsCMS.currentEditor.oVariant.notloggedin=($('.contentEditorPopup input[data-bind="editvariant.notloggedin"]').is(':checked'))?1:0;
        variantsCMS.currentEditor.oVariant.valid_from=$('.contentEditorPopup input[data-bind="editvariant.valid_from"]').val();
        variantsCMS.currentEditor.oVariant.valid_to=$('.contentEditorPopup input[data-bind="editvariant.valid_to"]').val();
        variantsCMS.currentEditor.oVariant.active=($('.contentEditorPopup input[data-bind="editvariant.active"]').is(':checked'))?1:0;

        variantsCMS.currentEditor.oVariant.namedata=$('.contentEditorPopup input[data-bind="editvariant.name"]').val();

        variantsCMS.currentEditor.oVariant.blockfieldtype=$('.contentEditorPopup select[data-bind="block.fieldtype"]').val();

        $.ajax(
            {
                url:'/administration/API/LLVariantsCMS/Save',
                async:false,
                success:variantsCMS.afterSave,
                dataType:'JSON',
                type:"POST",
                data:variantsCMS.currentEditor.oVariant
            }
        );
    },
    afterSave:function(data){
        variantsCMS.reloadBlock();
        for(sMessageId in data.messages){
            alert(data.messages[sMessageId].sCode);
        }
    },
    /**
     * Finds the templates and populates them to this object
     */
    obtainTemplates:function(){
        variantsCMS.templates.blockVariantItem=$('.contentEditorPopup *[data-template="BlockVariantItem"]').clone();
        variantsCMS.templates.blockVariantList=$('.contentEditorPopup *[data-template="BlockVariantList"]').clone();

        //Remove all the templates from the current markup
        $('.contentEditorPopup *[data-template]').remove();
    },
    openEditor:function(){
        if($('.contentEditorPopup').hasClass('visible')) return;

        variantsCMS.currentEditor={
            sFieldName:null,
            iActiveVariantId:null,
            sActiveBlockKey:null,
            oBlock:null,
            oVariant:null,
            oContent:null,
            gotolanguage:null,
            gotovariant:null
        }

        var target=$(this);

        $('.contentEditorSelectVariantLanguage').each(function(){
            $(this)[0].oContent=null;
        });

        if(target.data('gotovariant')!='') variantsCMS.currentEditor.gotovariant=target.data('gotovariant');
        if(target.data('gotolanguage')!='') variantsCMS.currentEditor.gotolanguage=target.data('gotolanguage');

        variantsCMS.currentEditor.sActiveBlockKey=target.data('fieldname');
        variantsCMS.prepareEditor();

    },
    prepareEditor:function(){
        $('.contentEditorPopup').addClass('preparing').addClass('visible');
        $('.contentEditorPopup > h2').text('LOADING');
        setTimeout(function(){
            variantsCMS.loadBlock(variantsCMS.currentEditor.sActiveBlockKey);
            $('.contentEditorPopup').removeClass('preparing');
        },20);
    },
    reloadBlock:function(){
        variantsCMS.loadBlock(variantsCMS.currentEditor.oBlock.key);
    },
    loadBlock:function(sKey){
        $('.contentEditorSelectVariant *[data-template]').remove();
        $.ajax(
            {
                url:'/administration/API/LLVariantsCMS/GetBlock',
                async:false,
                success:variantsCMS.displayBlock,
                dataType:'JSON',
                type:"POST",
                data:{'key':sKey}
            }
        );
    },
    loadVariant:function(iID){
        $.ajax(
            {
                url:'/administration/API/LLVariantsCMS/GetVariant',
                async:false,
                success:variantsCMS.displayVariant,
                dataType:'JSON',
                type:"POST",
                data:{'iID':iID}
            }
        );
    },
    /**
     * Displays the data from a load block call
     * Block data is really only a list of variants
     * Also clears any existing variant data
     * @param {type} data
     */
    displayBlock:function(data){
        variantsCMS.currentEditor.oBlock=data.oBlock;

        var eVariantList=$(variantsCMS.templates.blockVariantList).clone();
        var eVariantListContain=$('.contentEditorPopup').find('*[data-bind="block.variants"]');

        //Remove existing list
        eVariantListContain.find('*[data-template="BlockVariantItem"]').remove();

        for(iVariantItemNum in data.aVariants){
            var oVariant=data.aVariants[iVariantItemNum];
            var eNewVariant=$(variantsCMS.templates.blockVariantItem).clone();

            eNewVariant.text(oVariant.name);
            eNewVariant.attr('data-id',oVariant.id);
            eNewVariant.click(variantsCMS.selectVariant);

            eVariantList.append(eNewVariant);
        }

        if(variantsCMS.currentEditor.oVariant==null){
            if(variantsCMS.currentEditor.gotovariant!=null){
                eVariantList.find('*[data-id='+variantsCMS.currentEditor.gotovariant+']').click();
            }else{
                eVariantList.find('*[data-id]:first').click();
            }
        }else{
            eVariantList.find('*[data-id='+variantsCMS.currentEditor.oVariant.id+']').click();
        }

        var eAddVariantButton=$('<li class="contentEditorAddVariant">Add New Variant</li>');
        eAddVariantButton.click(variantsCMS.createVariant);
        eVariantList.append(eAddVariantButton);

        eVariantListContain.append(eVariantList);

        $('.contentEditorPopup').find('select[data-bind="block.fieldtype"]').val(data.oBlock.fieldtype);

        //Show the correct editor
        $('.contentEditorEditor > div.contentEditorWidget').hide();
        $('.contentEditorEditor > div[data-editortype='+data.oBlock.fieldtype+']').show();

        $('.contentEditorPopup').addClass('visible');
    },
    selectVariant:function(){
        var iID=$(this).attr('data-id');
        variantsCMS.loadVariant(iID);
        $(this).addClass('active').siblings().removeClass('active');
    },
    displayVariant:function(data){
        variantsCMS.currentEditor.oVariant=data.oVariant;

        $('.contentEditorPopup').find('input[type=checkbox]').removeAttr('checked');

        $('.contentEditorPopup > h2').text('Edit '+variantsCMS.currentEditor.oBlock.key+' : '+variantsCMS.currentEditor.oVariant.name);


        for(var sKey in data.oVariant){
            var sItem=data.oVariant[sKey];
            $('.contentEditorPopup').find('input[data-bind="editvariant.'+sKey+'"]').val(sItem);
            $('.contentEditorPopup').find('select[data-bind="editvariant.'+sKey+'"]').val(sItem);
            if(sItem==1){
                $('.contentEditorPopup').find('input[type=checkbox][data-bind="editvariant.'+sKey+'"]').attr('checked','checked');
            }
        }
               
        //Go through each language and assign the content
        $('.contentEditorSelectVariantLanguage li').each(function(){
            $(this)[0].oContent=null;
        });
        $('.contentEditorSelectVariantLanguage li span i').attr('class','fa fa-exclamation-triangle');
        for(var sContentItem in data.oVariant.oContent){
            try{
                var oContent=data.oVariant.oContent[sContentItem];
                var iLanguage=oContent.fk_language_id;
                if(iLanguage!=null){
                    var eLanguage=$('.contentEditorSelectVariantLanguage li[data-languageid='+iLanguage+']');
                    if(oContent.value!=''){
                        if(oContent.is_locked || variantsCMS.currentEditor.oBlock.is_locked==1){
                            eLanguage.find('span i').attr('class','fa fa-lock');
                        }else{
                            if(oContent.version<variantsCMS.currentEditor.oVariant.version){
                                eLanguage.find('span i').attr('class','fa fa-flag');
                            }else{
                                eLanguage.find('span i').attr('class','fa fa-check');
                            }
                        }
                    }
                    eLanguage[0].oContent=oContent;
                }
            }catch(e){}
        }

        if(variantsCMS.currentEditor.oContent==null){
            if(variantsCMS.currentEditor.gotolanguage!=null){
                $('.contentEditorSelectVariantLanguage li[data-languageid='+variantsCMS.currentEditor.gotolanguage+']').click();
            }else{
                $('.contentEditorSelectVariantLanguage li:first').click();
            }
        }else{
            $('.contentEditorSelectVariantLanguage li[data-languageid='+variantsCMS.currentEditor.oContent.fk_language_id+']').click();
        }

    },
    showLanguage:function(){
        $(this).addClass('active').siblings().removeClass('active');
        //Update current language with edited content
        if(variantsCMS.currentEditor.oContent!=null){
            switch(variantsCMS.currentEditor.oBlock.fieldtype){
                case "1":
                    variantsCMS.currentEditor.oContent.value=$('.contentEditorPopup .contentEditorText input').val();
                    break;
                case "2":
                    variantsCMS.currentEditor.oContent.value=$('.contentEditorPopup .froala-element').html();
                    break;
                case "3":
                    break;
            }
        }

        var oContent=$(this)[0].oContent;
        if(oContent==null){
            var oContent={
                value:'',
                fk_variant_id:variantsCMS.currentEditor.oVariant.id,
                fk_language_id:$(this).attr('data-languageid'),
                translation_required:0
            }
            variantsCMS.currentEditor.oVariant.oContent.push(oContent);
            $(this)[0].oContent=oContent;
        }

        variantsCMS.currentEditor.oContent=oContent;
        /**
         * For HTML type editors we need to reset for each variant loaded so that
         * froala can reload
         */
        if(variantsCMS.currentEditor.oBlock.fieldtype==4){
            if(oContent.is_locked==1 || variantsCMS.currentEditor.oBlock.is_locked==1){
                $('.contentEditorRaw textarea').val(oContent.value).attr('readonly','readonly').attr('title','This item is locked, please content dev team if you need to unlock');
            }else{
                $('.contentEditorRaw textarea').val(oContent.value).removeAttr('readonly').removeAttr('title');
            }
        }
        if(variantsCMS.currentEditor.oBlock.fieldtype==2){
            if(oContent.is_locked==1 || variantsCMS.currentEditor.oBlock.is_locked==1){
                $('.contentEditorHTML').html('<div>This item is locked, please content dev team if you need to unlock</div>'+oContent.value);
            }else{
                var eNewEditor=$('<textarea data-bind="editvariant.value">'+oContent.value+'</textarea>');
                $('.contentEditorHTML').html(eNewEditor);
                eNewEditor.editable(
                    {
                        inlineMode: false,
                        inverseSkin: true,
                        // Set the image upload URL.
                        imageUploadURL: '/administration/API/LLVariantsCMS/uploadImage',
                        imageUploadParams: {id: "my_editor"},
                        defaultImageWidth: 'auto',
                        spellcheck: true
                    }
                );
            }
        }
        if(variantsCMS.currentEditor.oBlock.fieldtype==1){
            if(oContent.is_locked==1 || variantsCMS.currentEditor.oBlock.is_locked==1){
                $('.contentEditorText input').val(oContent.value).attr('readonly','readonly').attr('title','This item is locked, please content dev team if you need to unlock');
            }else{            
                $('.contentEditorText input').val(oContent.value).removeAttr('readonly').removeAttr('title');
            }
        }
    }
}

$(document).ready(function(){
    variantsCMS.setup();
})
