<?php
/**
 * Installs the New "Variants CMS"
 * @package LoveLotto
 * @subpackage VariantsCMS
 */

namespace LL\VariantsCMS;

class Installer{
    use \LL\Installer\TInstaller;

    /**
     * Gets a list of install jobs for the installer
     * @return array
     */
    function getInstallTasks(){
        /**
         * Add the menu items
         * Always do this, regardless of version
         */
        if(self::getModuleVersion('VariantsCMS')<1){
        }
        $aJobs[]=new \LL\Installer\Task('LL\VariantsCMS\Installer', 'installDatabase', 'Install Variants CMS Database');
        //$aJobs[]=new \LL\Installer\Task('LL\VariantsCMS\Installer', 'installLink', 'Add CRM link to nav');
        return $aJobs;
    }

    /**
     * Installs all the tables we need for the new CMS
     */
    static function installDatabase(\LL\Installer\Task $oTask){
        self::installVCMSModule();
        self::installVariantsBlocksTable();
        self::installVariantsVariantsTable();
        self::installVariantsContentTable();
        self::installVariantsCustomerStatusTable();
        self::installVariantsCountriesTable();
        self::installVariantsWhiteLabelsTable();
        self::installVariantsSectionsTable();
        self::installVariantsSectionRowsTable();
        self::installVariantsSectionColsTable();
        self::installHandlers();

        self::installSectionChanges();
        self::installCMSChanges();
        self::installGameEngineChanges();

        self::installLink();
    }

    /**
     * Only contains changes to the CMS table, not entire table
     */
    static function installCMSChanges(){

        $oTable=new \LL\Installer\Table('cms_widgets');
        $oTable->addIdField();
        $oTable->addField(new \LL\Installer\Field('widget_name', 'varchar',255));
        $oTable->addField(new \LL\Installer\Field('view_name', 'varchar',255));
        $oTable->compile();

        \DAL::Insert('cms_widgets', array('id'=>1,'widget_name'=>'Euromillions Classic CTA','view_name'=>'widgets/euromillions_classic_cta'));
        \DAL::Insert('cms_widgets', array('id'=>2,'widget_name'=>'Euromillions System CTA','view_name'=>'widgets/euromillions_system_cta'));
        \DAL::Insert('cms_widgets', array('id'=>3,'widget_name'=>'6Aus49 Classic CTA','view_name'=>'widgets/6aus49_classic_cta'));
        \DAL::Insert('cms_widgets', array('id'=>4,'widget_name'=>'6Aus49 System CTA','view_name'=>'widgets/6aus49_system_cta'));
        \DAL::Insert('cms_widgets', array('id'=>5,'widget_name'=>'Eurojackpot Classic CTA','view_name'=>'widgets/eurojackpot_classic_cta'));
        \DAL::Insert('cms_widgets', array('id'=>6,'widget_name'=>'Eurojackpot System CTA','view_name'=>'widgets/eurojackpot_system_cta'));
        \DAL::Insert('cms_widgets', array('id'=>7,'widget_name'=>'Megamillions Classic CTA','view_name'=>'widgets/megamillions_classic_cta'));
        \DAL::Insert('cms_widgets', array('id'=>8,'widget_name'=>'Megamillions System CTA','view_name'=>'widgets/megamillions_system_cta'));
        \DAL::Insert('cms_widgets', array('id'=>9,'widget_name'=>'Powerball Classic CTA','view_name'=>'widgets/powerball_classic_cta'));
        \DAL::Insert('cms_widgets', array('id'=>10,'widget_name'=>'Powerball System CTA','view_name'=>'widgets/powerball_system_cta'));
        \DAL::Insert('cms_widgets', array('id'=>11,'widget_name'=>'Livechat Box','view_name'=>'widgets/livechat_widget'));
        \DAL::Insert('cms_widgets', array('id'=>12,'widget_name'=>'Security Box','view_name'=>'widgets/security_widget'));
        //\DAL::Insert('cms_widgets', array('id'=>1,'widget_name'=>'Recent or Random Lotto CTA','view_name'=>'widgets/recent_lotto'));
        //\DAL::Insert('cms_widgets', array('id'=>1,'widget_name'=>'Random Lotto CTA','view_name'=>'widgets/random_lotto'));

        $oMCTable=new \LL\Installer\Table('pages_cmn');

        $oMCTable->addField(new \LL\Installer\Field('fk_content','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_content', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_page_title','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_page_title', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_meta_keywords','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_meta_keywords', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_widget1','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_widget1', 'cms_widgets', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_widget2','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_widget2', 'cms_widgets', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_widget3','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_widget3', 'cms_widgets', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_widget4','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_widget4', 'cms_widgets', 'id'));


        $oMCTable->compile();



    }


    /**
     * Modification to sections table
     */
    static function installSectionChanges(){
        $oMCSections=new \LL\Installer\Table('sections_cmn');
        $oMCSections->addField(new \LL\Installer\Field('fk_section_name', 'int', 11));
        $oMCSections->addConstraint(new \LL\Installer\Constraint('fk_section_name', 'variants_blocks', 'id'));
        $oMCSections->compile();
    }

    static function installGameEngineChanges(){
        $oMCTable=new \LL\Installer\Table('game_engines');

        $oMCTable->addField(new \LL\Installer\Field('main_content','text'))->drop();

        $oMCTable->addField(new \LL\Installer\Field('fk_main_content','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_main_content', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_faq_content','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_faq_content', 'faq_categories_cmn', 'faqcategory_id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_odds_content','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_odds_content', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_product_info','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_product_info', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_product_title','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_product_title', 'variants_blocks', 'id'));

        $oMCTable->addField(new \LL\Installer\Field('fk_header_title','int',11));
        $oMCTable->addConstraint(new \LL\Installer\Constraint('fk_header_title', 'variants_blocks', 'id'));


        $oMCTable->compile();
    }

    static function installVCMSModule(){
        self::installModule('VariantsCMS');
    }

    static function installVariantsBlocksTable(){
        /**
         * Variants Blocks table holds all of the content blocks that are available to the CMS
         */

        $oMCTable=new \LL\Installer\Table('variants_blocks');
        $oMCTable->addIdField();
        $oMCTable->addField(new \LL\Installer\Field('key','varchar',200));
        $oMCTable->addField(new \LL\Installer\Field('comments', 'varchar',255));
        $oMCTable->addField(new \LL\Installer\Field('foreign_table', 'varchar',50))->drop();
        $oMCTable->addField(new \LL\Installer\Field('foreign_id', 'int',11))->drop();
        $oMCTable->addField(new \LL\Installer\Field('fieldtype', 'int',1)); //2=html 1=text 3=designer
        $oMCTable->addField(new \LL\Installer\Field('group', 'varchar',50));
        $oMCTable->addField(new \LL\Installer\Field('tag', 'varchar',50));
        $oMCTable->addIndex(new \LL\Installer\Index('key', 'key'));
        $oMCTable->addField(new \LL\Installer\Field('first_seen', 'varchar',255));
        $oMCTable->addField(new \LL\Installer\Field('version', 'int',11));

        $oMCTable->compile();
    }
        /**
         * Variants variants are the different designs available to each content block
         */
    static function installVariantsVariantsTable(){
        $oMCLangTable=new \LL\Installer\Table('variants_variants');
        $oMCLangTable->addIdField();
        $oMCLangTable->addField(new \LL\Installer\Field('fk_language_id', 'int',11));
        $oMCLangTable->addField(new \LL\Installer\Field('name', 'varchar',255));
        $oMCLangTable->addField(new \LL\Installer\Field('fk_campaign_id', 'int',11));
        $oMCLangTable->addField(new \LL\Installer\Field('valid_from', 'datetime'));
        $oMCLangTable->addField(new \LL\Installer\Field('valid_to', 'datetime'));
        $oMCLangTable->addField(new \LL\Installer\Field('active', 'int',1));
        $oMCLangTable->addField(new \LL\Installer\Field('notloggedin', 'int',1));
        $oMCLangTable->addField(new \LL\Installer\Field('loggedin', 'int',1));
        $oMCLangTable->addField(new \LL\Installer\Field('value', 'mediumtext'));
        $oMCLangTable->addField(new \LL\Installer\Field('datechanged', 'datetime'));
        $oMCLangTable->addField(new \LL\Installer\Field('fk_author_id', 'int',11));
        $oMCLangTable->addField(new \LL\Installer\Field('translation_required', 'int',1));
        $oMCLangTable->addField(new \LL\Installer\Field('fk_block_id', 'int',11));
        $oMCLangTable->addField(new \LL\Installer\Field('version', 'int',11));

        $oMCLangTable->addField(new \LL\Installer\Field('fieldtype', 'int',1))->drop(); //1=html 2=text 3=designer


        //$oMCLangTable->addConstraint(new \LL\Installer\Constraint('fk_language_id', 'languages', 'language_id'));
        //$oMCLangTable->addConstraint(new \LL\Installer\Constraint('fk_campaign_id', 'campaigns', 'id'));
        $oMCLangTable->addConstraint(new \LL\Installer\Constraint('fk_author_id', 'admin_users', 'user_id'));
        $oMCLangTable->addConstraint(new \LL\Installer\Constraint('fk_block_id', 'variants_blocks', 'id'))->drop();

        $oMCLangTable->compile();
    }

    static function installVariantsContentTable(){
        $oTable=new \LL\Installer\Table('variants_variant_content');
        $oTable->addIdField();
        $oTable->addField(new \LL\Installer\Field('fk_language_id', 'int',11));
        $oTable->addField(new \LL\Installer\Field('fk_variant_id', 'int',11));
        $oTable->addField(new \LL\Installer\Field('value', 'text'));
        $oTable->addField(new \LL\Installer\Field('datechanged', 'datetime'));
        $oTable->addField(new \LL\Installer\Field('fk_author_id', 'int',11));
        $oTable->addField(new \LL\Installer\Field('translation_required', 'int',1))->drop();
        $oTable->addField(new \LL\Installer\Field('version', 'int',11));

        //$oTable->addConstraint(new \LL\Installer\Constraint('fk_language_id', 'languages', 'language_id'));
        //$oTable->addConstraint(new \LL\Installer\Constraint('fk_campaign_id', 'campaigns', 'id'));
        $oTable->addConstraint(new \LL\Installer\Constraint('fk_author_id', 'admin_users', 'user_id'));
        $oTable->addConstraint(new \LL\Installer\Constraint('fk_variant_id', 'variants_variants', 'id'));

        $oTable->compile();
    }

    static function installVariantsCustomerStatusTable(){
        /**
         * Lookup table to control which user status's apply
         */
        $oMCCustomerStatus=new \LL\Installer\Table('variants_customer_status');
        $oMCCustomerStatus->addField(new \LL\Installer\Field('fk_variant_id', 'int', 11));
        $oMCCustomerStatus->addField(new \LL\Installer\Field('fk_customer_status_id', 'int', 11));
        $oMCCustomerStatus->addConstraint(new \LL\Installer\Constraint('fk_variant_id', 'variants_variants', 'id'));
        //$oMCCustomerStatus->addConstraint(new \LL\Installer\Constraint('fk_customer_status_id', 'customer_status', 'id'));
        $oMCCustomerStatus->compile();
    }

    /**
     * Lookup table to control which countries apply
     */
    static function installVariantsCountriesTable(){
        $oMCCountries=new \LL\Installer\Table('variants_countries');
        $oMCCountries->addField(new \LL\Installer\Field('fk_variant_id', 'int', 11));
        $oMCCountries->addField(new \LL\Installer\Field('fk_country_id', 'int', 11));
        $oMCCountries->addConstraint(new \LL\Installer\Constraint('fk_variant_id', 'variants_variants', 'id'));
        $oMCCountries->addConstraint(new \LL\Installer\Constraint('fk_country_id', 'countries_cmn', 'country_id'));
        $oMCCountries->compile();
    }


    /**
     * Lookup table to control which countries apply
     */
    static function installVariantsWhiteLabelsTable(){
        $oMCWhiteLabels=new \LL\Installer\Table('variants_affiliate_groups');
        $oMCWhiteLabels->addField(new \LL\Installer\Field('fk_variant_id', 'int', 11));
        $oMCWhiteLabels->addField(new \LL\Installer\Field('fk_affiliate_group_id', 'int', 11));
        $oMCWhiteLabels->addConstraint(new \LL\Installer\Constraint('fk_variant_id', 'variants_variants', 'id'));
        $oMCWhiteLabels->addConstraint(new \LL\Installer\Constraint('fk_affiliate_group_id', 'affiliate_groups', 'id'));
        $oMCWhiteLabels->compile();
    }

    /**
     * Lookup table to house the sections for designer type variants
     */
    static function installVariantsSectionsTable(){
        $oMCSections=new \LL\Installer\Table('variants_sections');
        $oMCSections->addIdField();
        $oMCSections->addField(new \LL\Installer\Field('fk_variant_id', 'int', 11));
        $oMCSections->addField(new \LL\Installer\Field('sort_order', 'int', 11));
        $oMCSections->addConstraint(new \LL\Installer\Constraint('fk_variant_id', 'variants_variants', 'id'));
        $oMCSections->compile();
    }

    /**
     * Lookup table to house the rows for designer type variants
     */
    static function installVariantsSectionRowsTable(){
        $oMCRows=new \LL\Installer\Table('variants_section_rows');
        $oMCRows->addIdField();
        $oMCRows->addField(new \LL\Installer\Field('fk_section_id', 'int', 11));
        $oMCRows->addField(new \LL\Installer\Field('sort_order', 'int', 11));
        $oMCRows->addConstraint(new \LL\Installer\Constraint('fk_section_id', 'variants_sections', 'id'));
        $oMCRows->compile();
    }

    /**
     * Lookup table to house the rows for designer type variants
     */
    static function installVariantsSectionColsTable(){
        $oMCCols=new \LL\Installer\Table('variants_section_cols');
        $oMCCols->addIdField();
        $oMCCols->addField(new \LL\Installer\Field('fk_row_id', 'int', 11));
        $oMCCols->addField(new \LL\Installer\Field('sort_order', 'int', 11));
        $oMCCols->addField(new \LL\Installer\Field('cols', 'int', 11));
        $oMCCols->addField(new \LL\Installer\Field('fk_module_id', 'int', 11));
        $oMCCols->addField(new \LL\Installer\Field('data', 'text'));
        $oMCCols->addConstraint(new \LL\Installer\Constraint('fk_row_id', 'variants_section_rows', 'id'));
        $oMCCols->addConstraint(new \LL\Installer\Constraint('fk_module_id', 'handlers', 'id'));
        $oMCCols->compile();
    }

    static function installHandlers(){
        self::installHandler('\LL\VariantsCMS\HTMLBlock', 'CMS HTML Block', 'VariantWidget');
        self::installHandler('\LL\VariantsCMS\ImageBlock', 'CMS Image Block', 'VariantWidget');
        self::installHandler('\LL\VariantsCMS\TextBlock', 'CMS Image Text', 'VariantWidget');
    }

    static function installLink(){
        $iAdminSectionId=\DAL::executeGetOne("SELECT id FROM admin_pages WHERE title='CMS'");
        //Get the ID of the admin section
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Content Blocks'")==0){
            \DAL::Insert('admin_pages', array('title'=>'Content Blocks','filename'=>'generic-listing.php','tablename'=>'variants_blocks','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }
        if(\DAL::Query("SELECT * FROM admin_pages WHERE filename='Translation Queue'")==0){
            \DAL::Query('DELETE FROM admin_pages WHERE filename=\'translation_queue/index\'');
            \DAL::Insert('admin_pages', array('title'=>'Translation Queue','filename'=>'/administration/translation_queue/index','tablename'=>'','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }
        if(\DAL::Query("SELECT * FROM admin_pages WHERE title='Misc Translations'")>0){
            \DAL::Query("DELETE FROM admin_pages WHERE title='Misc Translations'");
        }
        if(\DAL::Query("SELECT * FROM admin_pages WHERE filename='Translation Status'")==0){
            \DAL::Query('DELETE FROM admin_pages WHERE filename=\'/translation_queue/summary\'');
            \DAL::Insert('admin_pages', array('title'=>'Translation Status','filename'=>'/administration/translation_queue/summary','tablename'=>'','parent_id'=>$iAdminSectionId,'icon'=>'fa-files-o'));
        }



    }
}
