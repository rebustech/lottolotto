<div class="contentEditorPopup v2popup">
    <h2>Edit Content</h2>
    <div class="contentEditorSelectVariant">
        <div data-bind="block.variants">
            <h4>Variant</h4>
            <ul data-template="BlockVariantList">
                <li data-template="BlockVariantItem" data-bind="variant.name"></li>
            </ul>
        </div>
        <div class="contentEditorSelectVariantLanguage">
            <h4>Language</h4>
            <ul>
            <?php
                $aLanguages=Language::getLanguages();
                foreach($aLanguages as $aLanguage):
                ?>
                <li data-languageid="<?=$aLanguage['language_id']?>"><?=$aLanguage['title']?><span><i class="fa fa-thumbs-up"></i></span></li>
            <?php endforeach; ?>
            </ul>
        </div>

            <div class="contentEditorBlockProperties">
                <label>
                    <span>Editor Type:</span>
                    <select data-bind="block.fieldtype">
                        <option value="1">Text</option>
                        <option value="2">Content</option>
                        <option value="4">HTML</option>
                    </select>
                </label>
            </div>

    </div>
    <div class="contentEditorEditor">
        <div class="contentEditorText contentEditorWidget" data-editortype="1">
            <input type="text"/>
        </div>
        <div class="contentEditorHTML contentEditorWidget" data-editortype="2">
            <textarea data-bind="editvariant.value"></textarea>
        </div>
        <div class="contentEditorBuilder contentEditorWidget" data-editortype="3">
            Coming Soon...
        </div>
        <div class="contentEditorRaw contentEditorRaw" data-editortype="4" style="height:100%">
            <textarea data-bind="editvariant.value" style="height:100%"></textarea>
        </div>
    </div>
    <div class="contentEditorControls">
        <fieldset>
            <label>
                <span>Variant Name:</span>
                <div>
                    <input type="text" data-bind="editvariant.name"/>
                </div>
            </label>
            <div class="label">
                <span>Logged in status:</span>
                <div>
                    <input type="checkbox" value="1" data-bind="editvariant.loggedin">In
                    <input type="checkbox" value="1" data-bind="editvariant.notloggedin">Out
                </div>
            </div>
            <div class="label">
                <span>Customer Type:</span>
                <div class="scrollablelist">
                    <ul>
                        <li><input type="checkbox" value="1">New</li>
                        <li><input type="checkbox" value="1">Signup Non-Depositor</li>
                        <li><input type="checkbox" value="1">Abandonded Cart</li>
                        <li><input type="checkbox" value="1">VIP</li>
                    </ul>
                </div>
            </div>
            <label>
                <span>Campaign:</span>
                <select data-bind="editvariant.fk_campaign_id">
                    <option value="0">Any</option>
                </select>
            </label>
            <label>
                <span>Countries:</span>
                <div class="scrollablelist">
                    <ul>
                    <?php
                        $aCountries=LL\Core\Country::getAllActive();
                        foreach($aCountries as $oCountry):

                        ?>
                        <li><input type="checkbox" value="<?=$oCountry->country_id?>"><?=$oCountry->comment?></li>
                    <?php endforeach; ?>
                    </ul>
                </div>
            </label>
            <label>
                <span>White Labels:</span>
                <div class="scrollablelist">
                    <ul>
                        <?php
                            $aWhiteLabels=LL\Core\WhiteLabel::getAll();
                            foreach($aWhiteLabels as $oWhiteLabel):

                            ?>
                            <li><input type="checkbox" value="<?=$oWhiteLabel->id?>"><?=$oWhiteLabel->name?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </label>
            <label>
                <span>Live Date:</span>
                <div>
                    <input type="date" data-bind="editvariant.valid_from"/>
                </div>
            </label>
            <label>
                <span>End Date:</span>
                <div>
                    <input type="date" data-bind="editvariant.valid_to"/>
                </div>
            </label>
            <div class="label">
                <span>Active:</span>
                <div>
                    <input type="checkbox" value="1" data-bind="editvariant.active">
                </div>
            </div>
        </fieldset>
    </div>
    <div class="contentEditorActions">
        <input type="button" value="Clone Variant" class="contentEditorActionClone" disabled="disabled"/>
        <input type="button" value="Delete Variant" class="contentEditorActionDelete" disabled="disabled"/>
        <input type="button" value="Save Variant" class="contentEditorActionSave"/>
    </div>

</div>