<label id="field_<?=$this->sName?>" class="medium<?=($this->bIsRequired)?' required':''?>">
    <span><?=$this->sCaption?></span>
    <div><input
            type="button"
            class="CMSField"
            data-fieldname="<?=($this->sKey)?$this->sKey:'CMSField.'.$this->sTableName.'.'.$this->iId.'.'.$this->sName?>"
            data-fieldvalue=""
            data-gotovariant="<?=$this->gotoVariant?>"
            data-gotolanguage="<?=$this->gotoLanguage?>"
            value="Manage Content"
            /></div>
</label>