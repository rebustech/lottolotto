<?php
    $is_first_row=true;
    $last_variant_id=0;
?>



<h1>Translation Queue</h1>
    <span>Language:</span>
    <select id="languagefilter">
        <option value="">Any</option>
        <?php foreach($this->aLanguages as $aLanguage): ?>
        <option value="<?=$aLanguage['language_id']?>" <?=($_GET['lang']==$aLanguage['language_id'])?'selected="selected"':''?>><?=$aLanguage['title']?></option>
        <?php endforeach; ?>
    </select>
    <span>Group:</span>
        <select id="groupfilter">
            <option value="">Any</option>
            <?php foreach($this->aGroups as $aGroup): ?>
            <option value="<?=$aGroup['group']?>" <?=($_GET['group']==$aGroup['group'])?'selected="selected"':''?>><?=$aGroup['group']?></option>
            <?php endforeach; ?>
        </select>
    <span>Status:</span>
    <select id="statusfilter">
        <option value="0">Needs Doing</option>
        <option value="1"<?=($_GET['status']==1)?'selected="selected"':''?>>Done</option>
        <option value="2"<?=($_GET['status']==2)?'selected="selected"':''?>>All</option>
        <option value="3"<?=($_GET['status']==3)?'selected="selected"':''?>>Missing</option>
        <option value="4"<?=($_GET['status']==4)?'selected="selected"':''?>>Needs Translating</option>
    </select>

    <span>Word:</span>
    <input id="wordfilter" type="text"/>

<table class="datatable">
    <thead>
        <tr>
            <th>Key</th>
            <th>Group</th>
            <th>Variant</th>
            <th>Preview</th>
            <th>Language</th>
            <th>Page</th>
            <th>Status</th>
            <th/>
        </tr>
    </thead>
    <tbody>
        <?php foreach($this->aData as $aRow):  ?>
        <tr data-id="<?=$aRow['key'].':'.$aRow['variant_name'].':'.$aRow['language']?>">
            <td><?=$aRow['key']?></td>
            <td><?=$aRow['group']?></td>
            <td><?=$aRow['variant_name']?></td>
            <td>
                <?php if($aRow['first_seen'] != '' && substr_count($aRow['first_seen'],'API')==0): ?>
                    <a href="http://dev.maxlotto.com<?=$aRow['first_seen']?>?highlightkey=<?=$aRow['key']?>" target="_blank"><i class="fa fa-search"></i></a></td>
                <?php else: ?>
                    N/A
                <?php endif; ?>
            <td><img src="/flags/<?=$aRow['icon']?>"/> <?=$aRow['language']?></td>
            <?php if ($aRow['content_id']==null): ?>
                <td><i class="fa fa-exclamation-triangle"></i> Missing</td>
            <?php elseif($aRow['content_version'] < $aRow['variant_version']):?>
                <td><i class="fa fa-flag"></i> Needs Translation / Review</td>
            <?php else:?>
                <td><i class="fa fa-check"></i> Done</td>
            <?php endif;?>
            <td>
                    <?php
                        $eButton=new \LL\VariantsCMS\CMSField();
                        $eButton->sKey=$aRow['key'];
                        $eButton->gotoVariant=$aRow['variant_id'];
                        $eButton->gotoLanguage=$aRow['language_id'];
                        echo $eButton->output();
                    ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script>
    var translationQueue={
        timer:null,
        refresh:function(){
            $.ajax(
                {
                    url:'/administration/translation_queue/index',
                    async:false,
                    success:translationQueue.displayData,
                    type:"POST",
                    data:{languagefilter:$('#languagefilter').val(),
                          groupfilter:$('#groupfilter').val(),
                          statusfilter:$('#statusfilter').val(),
                          wordfilter:$('#wordfilter').val()}
                }
            );
            if(translationQueue.timer!=null) clearTimeout(translationQueue.timer);
            translationQueue.timer=setTimeout(translationQueue.refresh,10000);
        },
        displayData:function(data){
            var eData=$(data);
            //Remove any that need to go
            $('tr[data-id]').each(function(){
                var dataid=$(this).data('id');
                if(eData.find('tr[data-id="'+dataid+'"]').length==0){
                    $(this).css('overflow','hidden').slideUp('slow',function(){
                        $(this).remove();
                    })
                }
            })

            //Add any that are missing
            eData.find('tr[data-id]').each(function(){
                var dataid=$(this).data('id');
                if($('tr[data-id="'+dataid+'"]').length==0){
                    $('.datatable tbody').append($(this));
                }
            });

            //Update the status on all
            eData.find('tr[data-id]').each(function(){
                var dataid=$(this).data('id');
                $('tr[data-id="'+dataid+'"] td:eq(4)').html($(this).find('td:eq(4)').html());
            });

            $('.CMSField').click(variantsCMS.openEditor);
        }

    }
    $('#languagefilter').change(translationQueue.refresh);
    $('#groupfilter').change(translationQueue.refresh);
    $('#wordfilter').change(translationQueue.refresh);
    $('#statusfilter').change(translationQueue.refresh);

    translationQueue.timer=setTimeout(translationQueue.refresh,2000);
</script>