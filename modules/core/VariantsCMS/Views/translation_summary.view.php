<?php
        $aLangs=\Language::getLanguages();
?>

<h2>Progress Summary</h2>
<table class="datatable">
    <tr>
        <td rowspan="2">Block</td>
        <td rowspan="2">Total</td>
        <td colspan="<?=sizeof($aLangs)?>">% Complete</td>
        <td colspan="<?=sizeof($aLangs)?>">Missing</td>
        <td colspan="<?=sizeof($aLangs)?>">Done</td>
        <td colspan="<?=sizeof($aLangs)?>">Needs Translating</td>
    </tr>
    <tr>

<?php

    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
    </tr>
<?php
    foreach($this->aSummary as $aSummaryRow):

        $t=0;
        foreach($aLangs as $oLang){
            $t+=$aSummaryRow[$oLang['code'].'_missing']+$aSummaryRow[$oLang['code'].'_needs_trans'];
        }

        foreach($aLangs as $oLang){
            $varname=$oLang['code'].'_p';
            $$varname=intval((1-($aSummaryRow[$oLang['code'].'_missing']+$aSummaryRow[$oLang['code'].'_needs_trans'])/$aSummaryRow['total_blocks'])*100);
        }

        $totalBlocks+=$aSummaryRow['total_blocks'];

        ?>
    <tr>
        <td><?=$aSummaryRow['group']?></td>
        <td><?=$aSummaryRow['total_blocks']?></td>
        <?php
        foreach($aLangs as $oLang){
            $varname=$oLang['code'].'_p';
        ?>
        <td><?=$$varname?>%</td>
        <?php } ?>
<?php
    foreach($aLangs as $oLang):
        $missingTotal[$oLang['code']]+=$aSummaryRow[$oLang['code'].'_missing'];
?>
        <td><a href="index?lang=<?=$oLang['language_id']?>&amp;status=3&amp;group=<?=$aSummaryRow['group']?>"><?=$aSummaryRow[$oLang['code'].'_missing']?></a></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
        $doneTotal[$oLang['code']]+=$aSummaryRow[$oLang['code'].'_done'];
?>
        <td><a href="index?lang=<?=$oLang['language_id']?>&amp;status=1&amp;group=<?=$aSummaryRow['group']?>"><?=$aSummaryRow[$oLang['code'].'_done']?></a></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
        $transTotal[$oLang['code']]+=$aSummaryRow[$oLang['code'].'_needs_trans'];
?>
        <td><a href="index?lang=<?=$oLang['language_id']?>&amp;status=4&amp;group=<?=$aSummaryRow['group']?>"><?=$aSummaryRow[$oLang['code'].'_needs_trans']?></a></td>
<?php
    endforeach;
?>
    </tr>
<?php
    endforeach;
?>

    <tr class="totals">
        <td rowspan="2">Block</td>
        <td rowspan="2">Total</td>
        <td colspan="<?=sizeof($aLangs)?>">% Complete</td>
        <td colspan="<?=sizeof($aLangs)?>">Missing</td>
        <td colspan="<?=sizeof($aLangs)?>">Done</td>
        <td colspan="<?=sizeof($aLangs)?>">Needs Translating</td>
    </tr>
    <tr class="totals">

<?php

    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
<?php
    foreach($aLangs as $oLang):
?>
        <td><?=$oLang['code']?></td>
<?php
    endforeach;
?>
    </tr>

    <tr class="totals">
        <td>TOTAL</td>
        <td><?=$totalBlocks?></td>

        <?php
            foreach($aLangs as $oLang):
                $p=intval(($doneTotal[$oLang['code']]/$totalBlocks)*100);
        ?>
                <td><?=$p?>%</td>
        <?php
            endforeach;
        ?>

        <?php
            foreach($aLangs as $oLang):
        ?>
                <td><a href="index?lang=<?=$oLang['language_id']?>&amp;status=3"><?=$missingTotal[$oLang['code']]?></a></td>
        <?php
            endforeach;
        ?>
        <?php
            foreach($aLangs as $oLang):
        ?>
                <td><a href="index?lang=<?=$oLang['language_id']?>&amp;status=1?>"><?=$doneTotal[$oLang['code']]?></a></td>
        <?php
            endforeach;
        ?>
        <?php
            foreach($aLangs as $oLang):
        ?>
                <td><a href="index?lang=<?=$oLang['language_id']?>&amp;status=4?>"><?=$transTotal[$oLang['code']]?></a></td>
        <?php
            endforeach;
        ?>


    </tr>
</table>

<style>
    .totals td {
        font-weight:bold;
        font-size:13px;
    }
</style>