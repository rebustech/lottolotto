<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
    include("includes/notifications.php");
?>
<script type="text/javascript">
$(document).ready(function(){
	$("img.download").click(function(event){
		event.preventDefault();
		var params = '';
		if ( $(this).attr('filename') )
		{
			params =  '&filename=' + $(this).attr('filename');
		}
		window.location.href  = 'actions/download.php?file=' + this.src + params;
		return false;
	});
});
</script>
<h1>Implementation Guide</h1>
<h2>Text Links</h2>
<div class="pod implementation">
    <?php
    $aAffAdverts = new AffiliateAdverts();
    $aAffAdverts->iUserId = $oSecurityObject->getUserID();
    $textAdverts = $aAffAdverts->getTextAdverts();

    foreach ($textAdverts as $advert) {
        $sHashToken = urlencode(base64_encode($advert["fk_advert_id"]));
        $sHashToken = "?trackingkey={$sHashToken}";

        $campaignID = $value['fk_campaign_id'];

        $campaign = "";
        if (!is_null($campaignID)) {
            $campaign = "&cid={$campaignID}";
        }

        $sHtml = str_replace('{trackingkey}', $sHashToken, $advert["html"]);
        $sHtml = str_replace('{website}', WEBSITEMAINURL, $sHtml);
        ?>
        <div>
            <span><?php echo $advert["aaName"] ?></span>
            <br /><br />
            <?php echo $sHtml ?>
            <br />
            <textarea><?php echo $sHtml ?> <script src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $sHashToken ?>&impression=true<?php echo $campaign ?>"></script></textarea>
        </div>


        <?php
    }
    ?>
</div>
<?php include("includes/bottom.php"); ?>