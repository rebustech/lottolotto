<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
    include("includes/notifications.php");
?>
<script type="text/javascript">
$(document).ready(function(){
	$("img.download").click(function(event){
		event.preventDefault();
		var params = '';
		if ( $(this).attr('filename') )
		{
			params =  '&filename=' + $(this).attr('filename');
		}
		window.location.href  = 'actions/download.php?file=' + this.src + params;
		return false;
	});
});
</script>
<h1>Implementation Guide</h1>
Welcome to the <?=WEBSITETITLE?> Affiliate implementation guide. Getting started with the <?=WEBSITETITLE?> Affiliate program is extremely straight forward. The sections below explain how to track your sales, track specific promotions and include examples of text ads and banner ads which you can use to increase affiliate sales.
<br/><br/>
<h2>Tracking your Affiliate sales</h2>
<div class="pod">
    In order to track your affiliates sales, you will need to direct your visitors to a customized  URL which includes your affiliate ID (<?=$oSecurityObject->getUserID()?>):
    <br />
    <br />
    <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>" target="_blank">http://<?=WEBSITEHOST?>?aid=<?=$oSecurityObject->getUserID()?></a>
    <br />
    <br />
    The moment a user visits this URL, a cookie will be set which will store your affiliate ID for up to 7 days. If that users leaves the website and returns to purchase while the cookie is still active, your affiliate sale will still be tracked.
</div>

<h2>Creating Promotions</h2>
<div class="pod">
    If you are promoting LoveLotto using different techniques (eg: website, newsletter, social networking), you might want to track each one to know which method is proving to be most successful.
    <br />
    <br />
    To do so, you will simply need to add another parameter onto your URL. This parameter is called <strong>promo </strong>and should be implemented as follows:
    <br />
    <br />
    <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>&promo=promotionname" target="_blank">http://<?=WEBSITEHOST?>?aid=<?=$oSecurityObject->getUserID()?>&amp;promo=promotionname</a>
    <br />
    <br />
    The following are all valid examples of promotions:<br />
    <br />
    http://<?=WEBSITEHOST?>?aid=<?=$oSecurityObject->getUserID()?>&amp;promo=websitetopbanner<br />
    http://<?=WEBSITEHOST?>?aid=<?=$oSecurityObject->getUserID()?>&amp;promo=newsletter<br />
    http://<?=WEBSITEHOST?>?aid=<?=$oSecurityObject->getUserID()?>&amp;promo=advert
    <br /><br />Keep in mind you will still need to include your <strong>aid </strong>(Affiliate ID) for the tracking to work.
</div>

<h2>Text Links</h2>
<div class="pod implementation">
    The following are a few examples of text links which should help you get started. Just copy and paste the code from the text boxes below.
    <br />
    <br />
   Want to buy UK National Lottery tickets but don't live in the UK? Now you can. <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Play Lotto Now</a>
    <br />
    <textarea>Want to buy UK National Lottery tickets but don't live in the UK? Now you can. <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Play Lotto Now</a></textarea>
    <br />
    <br />
    What would you do with &pound;11 Million? <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Buy a ticket for the UK National Lotto today!</a>
    <br />
    <textarea>What would you do with &amp;pound;11 Million? <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Buy a ticket for the UK National Lotto today!</a></textarea>
    <br />
    <br />
    What would you do with &euro;15 Million? <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Buy a ticket for the Euromillions Lotto today!</a>
    <br />
    <textarea>What would you do with &amp;euro;15 Million? <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Buy a ticket for the Euromillions Lotto today!</a></textarea>
    <br />
    <br />
    Buy your dream home. <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Play the UK National Lotto &amp; Euromillions</a> and win up to &euro;15 Million!
    <br />
    <textarea>Buy your dream home. <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Play the UK National Lotto &amp; Euromillions</a> and win up to &amp;euro;15 Million!</textarea>
    <br />
    <br />
    Buy your dream car. <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Play the UK National Lotto &amp; Euromillions</a> and win up to &euro;15 Million!
    <br />
    <textarea>Buy your dream car. <a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>">Play the UK National Lotto &amp; Euromillions</a> and win up to &amp;euro;15 Million!</textarea>

</div>

<h2>Banner Ads</h2>
<div class="pod implementation">
    Below are a few examples of adverts which you are free to use on your own website.  We will be regularly adding more banners so keep checking here for the latest updates.
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner1.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner1.gif" title="Play UK Lottery" alt="Play UK Lottery" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner2.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner2.gif" title="Play EuroMillions Lottery" alt="Play EuroMillions Lottery" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner3.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner3.gif" title="Win with <?=WEBSITETITLE?>" alt="Win with <?=WEBSITETITLE?>" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner8.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner8.gif" title="Win with <?=WEBSITETITLE?>" alt="Win with <?=WEBSITETITLE?>" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner4.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner4.gif" title="Play UK Lottery" alt="Play UK Lottery" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner5.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner5.gif" title="Play UK Lottery" alt="Play UK Lottery" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner6.gif" width="468" height="60" />
    <br/>
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner6.gif" title="Play EuroMillions Lottery" alt="Play EuroMillions Lottery" width="468" height="60" /></a></textarea>
    <br />
    <br />
    <img class="download" src="/system/media/images/affiliates/banners/banner7.gif" width="468" height="60" />
    <br />
    <textarea><a href="http://<?=WEBSITEHOST?>index.php?aid=<?=$oSecurityObject->getUserID()?>"><img src="http://yourwebsite.com/banner7.gif" title="Play EuroMillions Lottery" alt="Play EuroMillions Lottery" width="468" height="60" /></a></textarea>
</div>
<h2>Web Logos</h2>
<div class="pod">
    	<img class="download" src="/system/media/images/affiliates/logos/logo1.gif" />
    <br />
</div>
<?php include("includes/bottom.php"); ?>