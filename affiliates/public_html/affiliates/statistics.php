<?php
/**
 * List of adverts with statistics overview
 */

require_once("../system/includes/autoload.php");
include("includes/top.php");
include("includes/notifications.php");

$aAffAdverts = new AffiliateAdverts();
$aAffAdverts->iUserId = $oSecurityObject->getUserID();

$adverts = $aAffAdverts->getAll();
?>
<h1>Statistics</h1>

<table class="report" width="100%">
<tr>
    <th>Name</th>
    <th>Clicks</th>
    <th>Impressions</th>
</tr>
<?php
foreach ($adverts as $advert) {
    ?>
    <tr>
        <td>
            <a href="statistics-single.php?advert_id=<?php echo $advert['fk_advert_id'] ?>"><?php echo $advert['name'] ?></a>
        </td>
        <td><?php echo ($aAffAdverts->getClicksByAdvertIDAndAffiliateID($advert['fk_advert_id'])); ?></td>
        <td><?php echo ($aAffAdverts->getImpressionsByAdvertIDAndAffiliateID($advert['fk_advert_id'])); ?></td>
    </tr>
    <?php
}
?>
</table>

<?php include("includes/bottom.php"); ?>