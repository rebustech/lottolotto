<?php
    require_once("../system/includes/autoload.php");
	include("security/checkauth.php");
	$sFolder = $oSecurityObject->getsFolder();
	$oSecurityObject->Logout();
	unset($oSecurityObject);
	include("includes/session_end.php");
	?>
	<script language="javascript">
	window.location="/<?=$sFolder?>login.php";
	</script>
	<?php
	exit;
?>