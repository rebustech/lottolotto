<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
    include("includes/notifications.php");

    // Grab dates to filter data between
    $dateFrom = filter_input(INPUT_GET, 'date-from');
    $dateTo   = filter_input(INPUT_GET, 'date-to');

    // Set default dates for placeholders if no dates are specified
    $lastMonth = (!$dateFrom) ? date('Y-m-d', strtotime('-1 month')) : $dateFrom;
    $today     = (!$dateTo) ? date('Y-m-d') : $dateTo;
?>
<h1><?=$oSecurityObject->getsAdminTitle();?> Monthly Report</h1>
    <form>
        Date from <input type="text" name="date-from" placeholder="<?php echo $lastMonth ?>"> to <input type="text" name="date-to" placeholder="<?php echo $today ?>">
        <input type="submit" value="Filter">
    </form>
<?php


    $aFilters = array('date-from' => $dateFrom, 'date-to' => $dateTo);

	$oReports = new ReportsAdmin($oSecurityObject, "AffiliatesByMonth", $aFilters);
	$oReports->display();
?>
<?php include("includes/bottom.php"); ?>