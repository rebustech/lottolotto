<div id="main-navigation" class="sliding-menu">
    <img src="/administration/images/llmax.png" style="margin:0px"/>
    <ul>
        <li>
            <a href="index.php" <?php if($sCurrentFilename == "index.php"){ ?>class="selected"<?php } ?>>
                <i class="fa fa-shopping-cart fa-2x fa-fw"></i>Orders
            </a>
        </li>
        <li>
            <a href="customers.php" <?php if($sCurrentFilename == "customers.php"){ ?>class="selected"<?php } ?>>
                <i class="fa fa-group fa-2x fa-fw"></i>Customers
            </a>
        </li>
        <li><a href="monthly-report.php" <?php if($sCurrentFilename == "monthly-report.php"){ ?>class="selected"<?php } ?>><i class="fa fa-bar-chart-o fa-2x fa-fw"></i>Reports</a></li>
        <li><a href="statistics.php" <?php if($sCurrentFilename == "statistics.php"){ ?>class="selected"<?php } ?>><i class="fa fa-bar-chart-o fa-2x fa-fw"></i>Statistics</a></li>
        <li><a href="implementation.php" <?php if($sCurrentFilename == "implementation.php"){ ?>class="selected"<?php } ?>><i class="fa fa-tags fa-2x fa-fw"></i>Implementation</a></li>
        <li><a href="implementation_links.php" <?php if($sCurrentFilename == "implementation_links.php"){ ?>class="selected"<?php } ?>><i class="fa fa-chain fa-2x fa-fw"></i>Text Links</a></li>
        <li><a href="adverts.php" <?php if($sCurrentFilename == "implementation_adverts.php"){ ?>class="selected"<?php } ?>><i class="fa fa-magic fa-2x fa-fw"></i>Adverts</a></li>
        <li><a href="products.php" <?php if($sCurrentFilename == "products.php"){ ?>class="selected"<?php } ?>><i class="fa fa-rocket fa-2x fa-fw"></i>Products</a></li>
        <li><a href="faqs.php" <?php if($sCurrentFilename == "faqs.php"){ ?>class="selected"<?php } ?>><i class="fa fa-question-circle fa-2x fa-fw"></i>FAQs</a></li>
        <li style="padding-left: 6px; margin-top: 10px;"><strong>Settings</strong></li>
        <li><a href="termsandconditions.php" <?php if($sCurrentFilename == "termsandconditions.php"){ ?>class="selected"<?php } ?>>Terms and Conditions</a></li>
        <li><a href="myaccount.php" <?php if($sCurrentFilename == "myaccount.php"){ ?>class="selected"<?php } ?>>My Account</a></li>
        <li><a href="logout.php">Logout</a></li>
    </ul>
    <div>Logged in as <strong><?=$oSecurityObject->getsFullName()?></strong> from IP <?=$oSecurityObject->getsIP()?></div>

</div>