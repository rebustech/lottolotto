<?php
    include("security/checkauth.php");

    $iUserID = $oSecurityObject->getUserID();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?=WEBSITETITLE?> <?=$oSecurityObject->getsAdminTitle();?> <?php if($oPage->sTitle){ ?> - <?=$oPage->sTitle?><?php } ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="/system/media/images/web/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/system/media/images/web/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/administration/styles/admin.css" media="screen"/>
    <link rel="stylesheet" href="/administration/styles/admin-affiliates.css" media="screen"/>
    <link rel="stylesheet" href="/administration/styles/jquery/ui.achtung-min.css" media="screen" />
    <link rel="stylesheet" href="/administration/styles/jquery/jquery.simpledialog.css" media="screen" />
	<script type="text/javascript" src="/administration/scripts/jquery/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="/administration/scripts/jquery/jquery.form.js"></script>
    <script type="text/javascript" src="/administration/scripts/jquery/interface.js"></script>
    <script type="text/javascript" src="/administration/scripts/jquery/ui/ui.core.js"></script>
    <script type="text/javascript" src="/administration/scripts/jquery/ui/ui.datepicker.js"></script>
 	<script type="text/javascript" src="/administration/scripts/jquery/ui/ui.achtung-min.js"></script>
 	<script type="text/javascript" src="/administration/scripts/jquery/achtung-ajax.js"></script>
  	<script type="text/javascript" src="/administration/scripts/jquery/jquery.simpledialog.min.js"></script>
    <script type="text/javascript" src="/administration/scripts/jquery/jquery.validate.min.js"></script>
 	<script type="text/javascript" src="/administration/scripts/urlencode.js"></script>
   	<script type="text/javascript" src="/administration/scripts/jquery/jquery.jqprint.js"></script>

<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"/>

    <script type="text/javascript">
	$(document).ready(onDocumentReady);
	function onDocumentReady(){
		loadControls();
		$('.simpledialog').simpleDialog({duration: 100, opacity: 0.5, open: loadControls});
	}

	var achtungFormOptions = {
			beforeSubmit:  beforeFormSubmit,  // pre-submit callback
			success:       showAchtungResponse  // post-submit callback
		};

	function beforeFormSubmit(){
		if( $('.ajaxform').valid() ){
			startAchtungAjax();
		}
	}

	function loadControls(){
		$(".ajaxform").validate();
		$(".validateform").validate();
		$(".datepicker").datepicker({dateFormat: "dd/mm/yy"});
		$(".ajaxform").ajaxForm(achtungFormOptions);
	}
	</script>
</head>
<body>
    <div id="pseudoBody">
        <div id="contentarea">
