<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
	include("includes/notifications.php");
	if ( $_POST['agree'] == 1 )
	{
		$aUserData = $oSecurityObject->getUserDetails();
		$sContract = Affiliates::getNewContract();
		$oNumber = new Numbers();
		$sContract = str_replace(
				array
				(
					"%day%",
					"%month%",
					"%year%",
					"%textyear%",
					"%email%",
					"%web%",
					"%firstname%",
					"%lastname%",
					"%company%",
					"%vatno%",
					"%address1%",
					"%address2%",
					"%city%",
					"%region%",
					"%country%",
					"%zipcode%",
					"%commission%",
					"%commissiontext%"
				),
				array
				(
					date("l j"),
					date("F"),
					date("Y"),
					$oNumber->GetText(date("Y")),
					$aUserData['email'],
					$aUserData['web'],
					$aUserData['firstname'],
					$aUserData['lastname'],
					($aUserData['company'])?$aUserData['company']:'n/a',
					($aUserData['vatno'])?$aUserData['vatno']:'n/a',
					$aUserData['address1'],
					$aUserData['address2'],
					$aUserData['city'],
					$aUserData['region'],
					$aUserData['country'],
					$aUserData['zipcode'],
					$aUserData['commission'],
					$oNumber->GetText($aUserData['commission'])
				),
			$sContract
		);
		Affiliates::setContract($oSecurityObject->getUserID(), nl2br($sContract));
	}
	$sContract = Affiliates::getContract($oSecurityObject->getUserID());
if ( $sContract )
{
	?>
	<h1>Terms and Conditions</h1>
	<div class="termsconditions pod noborder">
		<?=$sContract?>
	</div>
    <div class="accepttc">
        <table width="100%">
        	<tr>
            	<td align="right">
    				<input type="button" value="Print" onclick='$(".termsconditions").jqprint({importCSS: true, operaSupport: false, printContainer: false, debug: false});' />
				</td>
			</tr>
	</table>
    </div>
<?php
}
else
{
	$aUserData = $oSecurityObject->getUserDetails();
	$sContract = Affiliates::getNewContract();
	$oNumber = new Numbers();
	$sContract = str_replace(
			array
			(
				"%day%",
				"%month%",
				"%year%",
				"%textyear%",
				"%email%",
				"%web%",
				"%firstname%",
				"%lastname%",
				"%company%",
				"%vatno%",
				"%address1%",
				"%address2%",
				"%city%",
				"%region%",
				"%country%",
				"%zipcode%",
				"%commission%",
				"%commissiontext%"
			),
			array
			(
				date("l j"),
				date("F"),
				date("Y"),
				$oNumber->GetText(date("Y")),
				$aUserData['email'],
				$aUserData['web'],
				$aUserData['firstname'],
				$aUserData['lastname'],
				($aUserData['company'])?$aUserData['company']:'n/a',
				($aUserData['vatno'])?$aUserData['vatno']:'n/a',
				$aUserData['address1'],
				$aUserData['address2'],
				$aUserData['city'],
				$aUserData['region'],
				$aUserData['country'],
				$aUserData['zipcode'],
				$aUserData['commission'],
				$oNumber->GetText($aUserData['commission'])
			),
		$sContract
	);
?>
	<h1>Affiliates Terms And Conditions</h1>
	<div class="termsconditions">
	<h3>Terms And Conditions</h3>
		<?=nl2br($sContract)?>
	</div>
    <div class="accepttc">
    	By clicking 'Accept' you agree to the Affiliate Terms and Conditions.
        <table width="100%">
        	<tr>
            	<td align="right">
                    <form method="post">
                        <input type="hidden" name="agree" value="1" />
                        <input type="submit" value="Accept" />
                    </form>
				</td>
			</tr>
	</table>
    </div>
<?php
}
?>
<?php include("includes/bottom.php"); ?>