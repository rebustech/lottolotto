<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
    include("includes/notifications.php");
?>
<h1>Frequently Asked Questions</h1>
<h4>Q: What lotteries can I promote?</h4>
A: With <?=WEBSITETITLE?>, you can promote the UK National Lotto and the Euromillions lottery.
<br/>
<h4>Q: How much commission will I earn?</h4>
A: Unlike other lotto affiliate programs which often start with very low commission rates and increase as you generate more sales, <?=WEBSITETITLE?> offers you a 25% commission on gross sales for the life of the customer, regardless of how many users you have referred.
<br/>
<h4>Q: How easy is it to get started?</h4>
A: You can browse our Implementation Guide which will explain what links to use. This page also offers a number of banners and text links you can use on your own website/email/promotion.
<br/><br/>
You are, of course, free to use your marketing material as long as it adheres to our Terms and Conditions. Once you've implemented <?=WEBSITETITLE?>'s affiliate links, you will be able to start tracking sales in real time.
<br/>
<h4>Q: How and when will I be paid out?</h4>
A: You will be paid monthly as long as you have a minimum commission balance of 100 Euro.  Commissions will be paid to by either, Bank Transfer, Paypal or MoneyBookers Accounts.
<br/>
<h4>Q: How long will I continue to receive commission from my referred customers?</h4>
A: As long as you're an active member of our affiliate program, you will keep receiving commissions from your referred customers each time they make a new purchase.
<br/>
<h4>Q: What is the minimum amount of commission I need to earn before I can get paid out?</h4>
A minimum balance of 100 Euro is required before we make payment.
<br/>
<h4>Q: Are the affiliates reports updated in real time?</h4>
A: Yes, all stats, including commissions and sales, are updated in real time on the Affiliate Bookings page inside your Affiliate Centre. At the end of each month, you will also be able to view the overall monthly statistics in the Affiliate Monthly Report page.
<br/><br/>
Have more questions? Simply contact us at affiliate [at] lovelotto [dot] com and we'll be happy to answer any questions.<br/><br/>
<?php include("includes/bottom.php"); ?>