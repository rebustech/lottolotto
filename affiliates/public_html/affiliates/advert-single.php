<?php
require_once("../system/includes/autoload.php");
include("includes/top.php");
include("includes/notifications.php");

// Get current advert id
$advertID = filter_input(INPUT_GET, 'advert_id');

// Get data for an advert
$AffiliateAdverts = new AffiliateAdverts();
$AffiliateAdverts->iUserId = $oSecurityObject->getUserID();



$advert = $AffiliateAdverts->getAffiliateAdvertByIdWithUserId($advertID);
$advertType = $AffiliateAdverts->getAdvertTypeInfo($advertID);

// Retrieve if ad is banner, widget or text
$advertFormatType = $advertType['name'];
$advertFormatType = strtolower($advertFormatType);

if ($advertFormatType !== "text" && $advertFormatType !== "widget") {
    $isBanner = true;
}

// Get width and height
$width = $advertType['width'];
$height = $advertType['height'];



// No advert was returned for provided user id
if (!$advert) {
    header("Location: /affiliates/adverts.php");
    return;
}


$trackingkey = urlencode(base64_encode($advertID));
$trackingkey = "?trackingkey={$trackingkey}";

if(!empty($advert['fk_campaign_id'])) {
    $trackingkey .= '&cid='.$advert['fk_campaign_id'];
}

if(!empty($AffiliateAdverts->iUserId)) {
    $trackingkey .= '&aid='.$AffiliateAdverts->iUserId;
}

// If html contains {trackingkey} then replace it
$advert['html'] = str_replace('{trackingkey}', $trackingkey, $advert['html']);

?>

<h1><?php echo $advert['name'] ?></h1>
<div class="pod implementation">

    <?php if ($advertFormatType === "banner") { ?>

        <img src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $trackingkey ?>&banner=true&do-not-track=true" width="<?php echo $width ?>" height="<?php echo $height ?>">
        <textarea><a href="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $trackingkey ?>"><img src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $trackingkey ?>&banner=true" width="<?php echo $width ?>" height="<?php echo $height ?>"></a></textarea>

    <?php } elseif ($advertFormatType === "text") { ?>

        <?php echo $advert['html'] ?>
        <textarea><a href="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $trackingkey ?>"><?php echo $advert['html'] ?></a></textarea>

    <?php } else { ?>

        <?php
        if (!is_null($advert['width'])) {
            $width = ' width="' . $advert['width'] . '" ';
        }

        if (!is_null($advert['height'])) {
            $height = ' height="' . $advert['height'] . '" ';
        }

        // Get the widget type
        $sSql = "SELECT * FROM game_engines WHERE id = {$advert['fk_game_engine_id']}";
        $widgetType = \DAL::executeGetRow($sSql);
        $widgetType = $widgetType["url"];

        // Add Google Analytics if Tracking ID is present
        $GATrackingID = null;
        if (!is_null($advert['google_analytics_tracking_id']) && $advert['google_analytics_tracking_id'] !== "") {
            $GATrackingID = "<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '{$advert['google_analytics_tracking_id']}', 'auto');
            ga('send', 'pageview');

        </script>";
        }
        ?>
        <iframe src="<?php echo WEBSITEMAINURL ?>/playlotto/<?php echo $widgetType ?>?iframe=1"<?php echo $width ?><?php echo $height ?>></iframe><?= $advert['html'] ?>
        <textarea><iframe src="<?php echo WEBSITEMAINURL ?>/playlotto/<?php echo $widgetType ?>?iframe=1"<?php echo $width ?><?php echo $height ?>></iframe><?= $advert['html'] ?><script src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $trackingkey ?>&impression=true<?php echo $campaign ?>"></script><?php echo $GATrackingID ?></textarea>

    <?php } ?>

</div>

<?php include("includes/bottom.php"); ?>
