<?php
    error_reporting(E_ERROR);
    require_once("../system/includes/autoload.php");


	$file = CLASSPATH . "DATA/DAL.class.php";
	if (file_exists($file)) require_once("$file");
	$file = CLASSPATH . "Admin/SecurityAdmin.class.php";
	if (file_exists($file)) require_once("$file");

	require_once ("includes/session_start.php");

	if ( is_object($oSecurityObject) )
	{
		if ( $oSecurityObject->getbIsActive() == 0 )
		{
			showMaintenancePage($oSecurityObject);
			unset($oSecurityObject);
		}
		else
		{
			if ( $oSecurityObject->getsFolder() && $oSecurityObject->getbIsLoggedIn() )
			{
				include("includes/session_end.php");
				?>
				<script language="javascript">
				window.location="/<?=$oSecurityObject->getsFolder()?>index.php";
				</script>
				<?php
				exit;
			}
			else
			{
				showLogin($oSecurityObject);
				unset($oSecurityObject);
			}
		}
	}
	else
	{

		if(file_exists("__config.php")) include("__config.php");
		else if(file_exists("../__config.php"))  include("../__config.php");
		$oSecurityObject = new SecurityAdmin($iAdminTypeID);
		if ( $oSecurityObject->getbIsActive() == 0 )
		{
			showMaintenancePage($oSecurityObject);
			unset($oSecurityObject);
		}
		else
		{
			showLogin($oSecurityObject);
			unset($oSecurityObject);
		}
	}

function loginTop(&$oSecurityObject)
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title> <?=WEBSITETITLE?> - <?=$oSecurityObject->getsAdminTitle();?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="/system/media/images/web/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/system/media/images/web/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/administration/styles/admin.css" media="screen"/>
</head>
<body class="loginForm">
    <div id="pseudoBody">
        <div id="contentarea" class="<?=strtolower(str_replace(" ","",$oSecurityObject->getsAdminTitle()))?>">

<?php
}

function loginBottom(&$oSecurityObject)
{
	$aAdminTypes = $oSecurityObject->getAdminTypes();
	?>
               </div> <!-- //#contentarea -->
            </div>
</body>
</html>
<?php
}

function processLogin(&$oSecurityObject)
{
	$aLogin = $_POST['action'];
	if ( $aLogin == "login" )
	{
		$sUsername = $_POST['username'];
		$sPassword = $_POST['password'];
		$sPin = $_POST['pin'];
		$oSecurityObject->validateLogin($sUsername, $sPassword, $sPin);
		if ( $oSecurityObject->getbIsLoggedIn() == 1 )
		{
			include("includes/session_end.php");
			?>
				<script language="javascript">
                window.location="/<?=$oSecurityObject->getsFolder()?>index.php";
                </script>
			<?php
			exit;
		}
		return "Error: Login Failed.";
	}
	else
	{
		return false;
	}
}
function showLogin(&$oSecurityObject)
{
	$sError = processLogin($oSecurityObject);
	loginTop($oSecurityObject);
?>
    <div>
        <div class="img"><img src="/administration/images/llmax.png" style="margin:0px"/></div>
    <h1><?=$oSecurityObject->getsAdminTitle()?> Login</h1>
    <p style="margin-left: 10px; margin-top: 3px;">Please use your account credentials to log in.</p>
    <div class="error" <?php if(isset($sError) && $sError != ""){?>style="display: block;" <?php } ?>><?=$sError?></div>
	<form action="login.php" name="login"  method="post" style="margin:0px">
            <fieldset class="detailsform">
		<label class="long">
		<span>Username:</span>
                <div><input type="text" value="" width="200" name="username"/></div>
		</label>
		<label class="long">
		<span>Password: </span>
                <div><input type="password" value=""  width="200" name="password"/></div>
		</label>
		<label class="long">
		<span>PIN: </span>
                <div><input type="password" value=""  width="200" name="pin"/></div>
		</label>
		<label>
		<span>Remember Me</span>
                <div><input type="checkbox" name="remember" class="bool"/></div>
		</label>
            <p>Your IP address <?=($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']?> will be logged for security measures.</p>

                <input type="submit" name="submit" value="Login"/>
		<input type="hidden" name="action" value="login"/>
            </fieldset>
		</form>

	   </div>
<?php
	loginBottom($oSecurityObject);
}


function showMaintenancePage(&$oSecurityObject)
{
	loginTop($oSecurityObject);
?>
	<strong>This admin is currently disabled.</strong>
<?php
	loginBottom($oSecurityObject);
}

include("includes/session_end.php");

?>