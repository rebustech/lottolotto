<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php"); ?>
	<h1>My Account</h1>
    <?php include("includes/errors.php");
	$aUserData = $oSecurityObject->getUserDetails();
	 ?>
     <form name="accountdetailsform" id="accountdetailsform" action="actions/myaccount_ajax.php?a=saveDetails&pp=<?=$sCurrentFilename?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Modify Account Details</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">First name</th>
            <td><input type="text" name="firstname" class="required"  value="<?=$aUserData["firstname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Last name</th>
            <td><input type="text" name="lastname" class="required"  value="<?=$aUserData["lastname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Email</th>
            <td><input type="text" name="email" class="required email medium"  value="<?=$aUserData["email"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Address</th>
            <td>
            	<input type="text" name="address1" class="required medium"  value="<?=$aUserData["address1"]?>"/><p />
                <input type="text" name="address2" class="medium" value="<?=$aUserData["address2"]?>"/>
            </td>
       </tr>
    	<tr>
        	<th align="right" class="small">ZIP Code</th>
            <td>
            	<input type="text" name="zip" class="required"  value="<?=$aUserData["zipcode"]?>"/>
            </td>
       </tr>
    	<tr>
        	<th align="right" class="small">City</th>
            <td>
            	<input type="text" name="city" class="required"  value="<?=$aUserData["city"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Country</th>
            <td>
            	<input type="text" name="country" class="required"  value="<?=$aUserData["country"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Telephone</th>
            <td>
            	<input type="text" name="telephone1" class="required"  value="<?=$aUserData["tel1"]?>"/><p />
            	<input type="text" name="telephone2"  value="<?=$aUserData["tel2"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Fax</th>
            <td>
            	<input type="text" name="fax" value="<?=$aUserData["fax"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Website</th>
            <td><input type="text" name="website" class="required url medium"  value="<?=$aUserData["web"];?>"/><br /><small>http://www.example.com</small></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Company</th>
            <td>
            	<input type="text" name="company" value="<?=$aUserData["company"];?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">VAT Number</th>
            <td>
            	<input type="text" name="vat" value="<?=$aUserData["vatno"];?>"/>
           </td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Save Details" />
            </th>
        </tr>
    </table>
  	</form>
    <p />
<form name="changepasswordform" action="actions/myaccount_ajax.php?a=changePassword&pp=<?=$sCurrentFilename?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change Password</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">Old password</th>
            <td><input type="password" name="oldpassword" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">New password</th>
            <td><input type="password" name="password" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm password</th>
            <td><input type="password" name="password2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change Password" />
            </th>
       </tr>
    </table>
  	</form>
    <p />
<form name="changepinform" action="actions/myaccount_ajax.php?a=changePin&pp=<?=$sCurrentFilename?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change PIN Number</strong>
            </td>
        </tr>
        <tr>
        	<th align="right" class="small">Old PIN</th>
            <td><input type="password" name="oldpin" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">New PIN</th>
            <td><input type="password" name="pin" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm PIN</th>
            <td><input type="password" name="pin2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change PIN" />
            </th>
       </tr>
    </table>
  	</form>
<?php include("includes/bottom.php"); ?>