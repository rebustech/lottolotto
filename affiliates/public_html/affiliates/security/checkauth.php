<?php
	$sIncludePath = ($_SERVER['DOCUMENT_ROOT'])?$_SERVER['DOCUMENT_ROOT']:'../';

	function __autoload($class_name) {
		$file = CLASSPATH . "BIZ/" . $class_name . '.class.php';
		if (file_exists($file)) require_once("$file");
		$file = CLASSPATH . "DATA/" . $class_name . '.class.php';
		if (file_exists($file)) require_once("$file");
		$file = CLASSPATH . "Admin/" . $class_name . '.class.php';
		if (file_exists($file)) require_once("$file");

	}

	if(file_exists("../includes/session_start.php")) require_once("../includes/session_start.php");
	else require_once("includes/session_start.php");

	if ( is_object($oSecurityObject) && $oSecurityObject->getbIsLoggedIn() == 1 )
	{
		$oSecurityObject->checkAuth();
	}
	else
	{
		if(file_exists("../includes/session_end.php")) require_once("../includes/session_end.php");
		else require_once("includes/session_end.php");
		?>
		<script language="javascript">
		window.location="login.php";
		</script>
		<?php
		exit;

	}
	$oErrors = new Error($oSecurityObject->getsAdminType());
	if($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"] == "") $_SESSION[$oSecurityObject->getsAdminType() ."iLangID"] = 0;


	$sPath = $_SERVER["PHP_SELF"];
	$aPath = explode("/", $sPath);
	$sCurrentFilename = $aPath[sizeof($aPath) - 1];

	if(file_exists("../security/contractsigned.php")) require_once("../security/contractsigned.php");
	else require_once("security/contractsigned.php");
?>