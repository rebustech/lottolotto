<?php
	$iUserID = $oSecurityObject->getUserID();
	if($_GET["productid"]){
		$iProductID = $_GET["productid"];
	}
	else{
		$iProductID = $_GET["id"];
	}
	$aProduct = false;
	if($iProductID && is_numeric($iProductID)){
		$aProduct = Products::getProduct($iProductID);
		if(empty($aProduct) || $iUserID != $aProduct["user"]){
			$iProductID = false;
			$aProduct = false;
			echo "<script type=\"text/javascript\">$(document).ready(function (){ document.location = 'index.php';});</script>";
		}
	}
	else{
		$iProductID = false;
		echo "<script type=\"text/javascript\">$(document).ready(function (){ document.location = 'index.php';});</script>";
	}
?>