<?php
include("../security/checkauth.php");
switch($_GET["a"]){
	case "saveDetails":
		$aUserData = array();
		$aAffiliateData = array();
		$bValid = true;
		$aUserData["firstname"] = $_POST["firstname"];
		if(!$aUserData["firstname"]){
			$oErrors->addError("First name is required");
			$bValid = false;
		}
		
		$aUserData["lastname"] = $_POST["lastname"];
		if(!$aUserData["lastname"]){
			$oErrors->addError("Last name is required");
			$bValid = false;
		}
		
		$aUserData["email"] = $_POST["email"];
		if(!$aUserData["email"]){
			$oErrors->addError("Email address is required");
			$bValid = false;
		}
		
		$aAffiliateData["address1"] = $_POST["address1"];
		if(!$aAffiliateData["address1"]){
			$oErrors->addError("Address is required");
			$bValid = false;
		}
		
		$aAffiliateData["address2"] = $_POST["address2"];

		$aAffiliateData["zipcode"] = $_POST["zip"];
		if(!$aAffiliateData["zipcode"]){
			$oErrors->addError("ZIP Code is required");
			$bValid = false;
		}
		
		$aAffiliateData["city"] = $_POST["city"];
		if(!$aAffiliateData["city"]){
			$oErrors->addError("City is required");
			$bValid = false;
		}
		
		$aAffiliateData["country"] = $_POST["country"];
		if(!$aAffiliateData["country"]){
			$oErrors->addError("Country is required");
			$bValid = false;
		}
		
		$aUserData["tel1"] = $_POST["telephone1"];
		if(!$aUserData["tel1"]){
			$oErrors->addError("Telephone is required");
			$bValid = false;
		}
		
		$aUserData["tel2"] = $_POST["telephone2"];

		$aAffiliateData["fax"] = $_POST["fax"];
		
		$aAffiliateData["company"] = $_POST["company"];
		
		$aAffiliateData["web"] = $_POST["website"];
		
		if(!$aAffiliateData["web"]){
			$oErrors->addError("Website is required.");
			$bValid = false;
		}
		
		$aAffiliateData["vatno"] = $_POST["vat"];
		
		if($bValid){
			if($oSecurityObject->editUserDetails($aUserData)){
				if(Affiliates::editUserDetails($aAffiliateData, $oSecurityObject->getUserID() )){
					echo "User details saved.";
				}
			}
			else{
				echo "Problem encountered on saving. {messagetype}achtungFail{/messagetype}";
			}
		}
		else{
			echo $oErrors->getErrorString() . "{messagetype}achtungFail{/messagetype}";
		}
		$oErrors->clearMessages();
		
	break;
	case "changePassword":
		if($_POST["password"] != $_POST["password2"]){
			echo "Passwords do not match.{messagetype}achtungFail{/messagetype}";
		}
		else{
			if($oSecurityObject->changePassword($_POST["oldpassword"], $_POST["password"])){
				echo "Password changed.";
			}
			else{
				echo "Old password incorrect. {messagetype}achtungFail{/messagetype}";
			}
		}	
	break;		
	case "changePin":
		if($_POST["pin"] != $_POST["pin2"]){
			echo "PINs do not match.{messagetype}achtungFail{/messagetype}";
		}
		else{
			if($oSecurityObject->changePin($_POST["oldpin"], $_POST["pin"])){
				echo "PIN changed.";
			}
			else{
				echo "Old PIN incorrect. {messagetype}achtungFail{/messagetype}";
			}
		}	
	break;
}
?>
