<?php
include("../security/checkauth.php");
$sourceFile = $_GET['file'];
$outputFile = ($_GET['filename'])?$_GET['filename']:$sourceFile; // the name they save as can be different to the existing file
// required for IE, otherwise Content-disposition is ignored
if ( $sourceFile && strpos($sourceFile, "https://" . WEBSITEURL . "system/media/images/affiliates/") === 0  )
{
	if(ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');
	
	switch( $fileExtension)
	{
		case "gif": $ctype="image/gif"; break;
		case "png": $ctype="image/png"; break;
		case "jpeg":
		case "jpg": $ctype="image/jpg"; break;
		default: $ctype="application/force-download";
	}
	
	session_cache_limiter("");
	
	header("Pragma: public"); // required
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); // required for certain browsers
	header("Content-Type: $ctype");
	header("Content-Disposition: attachment; filename=".basename($outputFile).";" );
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ". @filesize($sourceFile));
	
	@readfile("$sourceFile") ;  // This is the bit that prompts for the download, supress errors for niceness
}
else
{
	echo "<h1>500: Unauthorised Access</h1>";
}
exit();
?>
