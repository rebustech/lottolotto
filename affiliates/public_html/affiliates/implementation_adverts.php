<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
    include("includes/notifications.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
	$("img.download").click(function(event){
		event.preventDefault();
		var params = '';
		if ( $(this).attr('filename') )
		{
			params =  '&filename=' + $(this).attr('filename');
		}
		window.location.href  = 'actions/download.php?file=' + this.src + params;
		return false;
	});
});
</script>
<h1>Implementation Guide</h1>
<h2>Adverts</h2>
<div class="pod implementation">
    Below are a few examples of adverts which you are free to use on your own website.  We will be regularly adding more banners so keep checking here for the latest updates.

    <?php
        $aAffAdverts = new AffiliateAdverts();
        $aAffAdverts->iUserId = $oSecurityObject->getUserID();

        $bannerAdverts = $aAffAdverts->getBanners();
        foreach ($bannerAdverts as $key => $value)
        {
            $lotteryID = $value['fk_lottery_id'];

            $lotteryDate = null;
            $jackpot = null;

            if (!is_null($lotteryID)) {
                $sSql = "SELECT * FROM lottery_draws
                         JOIN lotteries_cmn ON lottery_draws.fk_lottery_id = lotteries_cmn.lottery_id
                         WHERE lottery_draws.fk_lottery_id = {$lotteryID}
                         ORDER BY lottery_draws.fk_lotterydate DESC";

                $result = \DAL::executeGetRow($sSql);
                $lotteryDate = strtotime($result['fk_lotterydate']);
                $lotteryDate = date('D', $lotteryDate);
                $lotteryDate = strtolower($lotteryDate);
                $jackpot = $result['jackpot'];
                $lotteryName = $result['comment'];
            }

            $aArraySrc = array();
            $aArrayHgt = array();
            $aArrayWdt = array();
            preg_match( '/< *img[^>]*src *= *["\']?([^"\']*)/i', urldecode($value["html"]), $aArraySrc );

            $advertID = $value["fk_advert_id"];
            $sHashToken = urlencode(base64_encode($advertID));
            $sHashToken = "?trackingkey={$sHashToken}";

            $campaignID = $value['fk_campaign_id'];

            $campaign = "";
            if (!is_null($campaignID)) {
                $campaign = "&cid={$campaignID}";
            }

            $sHtml = str_replace('{trackingkey}', $sHashToken, $value["html"]);
            $sHtml = str_replace('{website}', 'maxlotto.com', $sHtml);

            $sHtml = str_replace('{lottery}', $lotteryName, $sHtml);
            $sHtml = str_replace('{jackpotvalue}', $jackpot, $sHtml);
            $sHtml = str_replace('{drawdate}', $lotteryDate, $sHtml);
            $sHtml = str_replace('{width}', $value['width'], $sHtml);
            $sHtml = str_replace('{height}', $value['height'], $sHtml);

            ?>
            <br />
            <br />
            <span><?echo $value["name"]?> - Asset size: <?echo $value["width"].'x'.$value["height"].'px'?></span>

            <br />
            <img src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $sHashToken ?>&banner=true&do-not-track=true" width="<?echo $value["width"];?>" height="<?echo $value["height"];?>">
            <br />
            <textarea><a href="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $sHashToken ?>&aid=<?php echo $aAffAdverts->iUserId ?><?php echo $campaign ?>"><img src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $sHashToken ?>&banner=true<?php echo $campaign ?>" width="<?php echo $value['width'] ?>" height="<?php echo $value['height'] ?>"></a></textarea>
            <?
        } ?>

        <?php
        $widgets = $aAffAdverts->getWidgets();
        if (count($widgets) > 0) {
            ?>
            <h3>Widgets</h3>
            <?php
        }

        foreach ($widgets as $key => $value) {
            $aArraySrc = array();
            $aArrayHgt = array();
            $aArrayWdt = array();
            preg_match( '/< *img[^>]*src *= *["\']?([^"\']*)/i', urldecode($value["html"]), $aArraySrc );

            $sHashToken = urlencode(base64_encode($value["fk_advert_id"]));
            $sHashToken = "?trackingkey={$sHashToken}";

            $campaignID = $value['fk_campaign_id'];

            $campaign = "";
            if (!is_null($campaignID)) {
                $campaign = "&cid={$campaignID}";
            }

            $sHtml = str_replace('{trackingkey}', $sHashToken, $value["html"]);
            $sHtml = str_replace('{website}', 'maxlotto.com', $sHtml);

            // Get the widget type
            $sSql = "SELECT * FROM game_engines WHERE id = {$value['fk_game_engine_id']}";
            $widgetType = \DAL::executeGetRow($sSql);
            $widgetType = $widgetType["url"];


            // Get the widget width and height
            $width = null;
            $height = null;

            if (!is_null($value['width'])) {
                $width = ' width="' . $value['width'] . '" ';
            }

            if (!is_null($value['height'])) {
                $height = ' height="' . $value['height'] . '" ';
            }

            // Add Google Analytics if Tracking ID is present
            $GATrackingID = null;
            if (!is_null($value['google_analytics_tracking_id']) && $value['google_analytics_tracking_id'] !== "") {
                $GATrackingID = "<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '{$value['google_analytics_tracking_id']}', 'auto');
  ga('send', 'pageview');

</script>";
            }

            ?>
            <br />
            <br />
            <span><?echo $value["name"]?> - Asset size: <?echo $value["width"].'x'.$value["height"].'px'?></span>
            <br />
            <img class="download" src="<?print_r($aArraySrc[1]);?>" width="<?echo $value["width"];?>" height="<?echo $value["height"];?>" />
            <br />
            <textarea><iframe src="<?php echo WEBSITEMAINURL ?>/playlotto/<?php echo $widgetType ?>?iframe=1"<?php echo $width ?><?php echo $height ?>></iframe><?=$sHtml?><script src="<?php echo WEBSITEMAINURL ?>/tracker.php<?php echo $sHashToken ?>&impression=true<?php echo $campaign ?>"></script><?php echo $GATrackingID ?></textarea>
            <?php
        }
        ?>

</div>
<?php include("includes/bottom.php"); ?>