<?php
/**
 * Statistics for a single advert
 */

require_once("../system/includes/autoload.php");
include("includes/top.php");
include("includes/notifications.php");

// Sanitize the input
$advertID = filter_input(INPUT_GET, 'advert_id', FILTER_SANITIZE_SPECIAL_CHARS);

$aAffAdverts = new AffiliateAdverts();
$AffiliatesAdvertTracker = new AffiliatesAdvertTracker($advertID);

// Pass in user ID
$aAffAdverts->iUserId = $oSecurityObject->getUserID();

// Assign current affiliate ID
$AffiliatesAdvertTracker->affiliateID = $oSecurityObject->getUserID();

// Retrieve the advert
$advert = $aAffAdverts->getAdvertById($advertID);
$advert = $advert[0];

// Get current date to display as a placeholder for date from and date to
$dateFrom = date('Y-m-d', strtotime('-1 month'));
$dateTo = date('Y-m-d');

// Check if statistics are being displayed for specific dates
$dateFromQueryString = $_GET['date-from'];
$dateToQueryString = $_GET['date-to'];
$bShowForSpecificDates = false;


// If date from and to are set by the user then reassign variables
if (isset($dateFromQueryString) && isset($dateToQueryString)) {
    $bShowForSpecificDates = true;
    $dateFrom = $dateFromQueryString;
    $dateTo = $dateToQueryString;
    $AffiliatesAdvertTracker->dateFrom = $dateFrom;
    $AffiliatesAdvertTracker->dateTo = $dateTo;
}

// Get clicks and impressions
$aClicks = $AffiliatesAdvertTracker->getClicks(5);
$aImpressions = $AffiliatesAdvertTracker->getImpressions(5);
$clicksAndImpressions = $AffiliatesAdvertTracker->getClicksAndImpressions();

// Get number of clicks and impressions
$numberOfClicks = $AffiliatesAdvertTracker->getClicksCount();
$numberOfImpressions = $AffiliatesAdvertTracker->getImpressionsCount();

?>
<script>
    // Define array
    var maxlotto = {};
    maxlotto.clicksAndImpressions = [];
<?php
$dates = [];
foreach ($clicksAndImpressions as $value) {
    $currentDate = substr($value['datetime'], 0, 10);
    $isNewDate = true;

    // Check if the date is already in the array
    foreach ($dates as $key => $val) {

        if ($key === $currentDate) {
            $isNewDate = false;
        }
    }


    // Create a new key in array set to $currentDate
    if ($isNewDate) {
        $data = array("clicks" => 0, "impressions" => 0);
        $dates[$currentDate] = $data;
    }

    // Get the clicks and impressions for $currentDate
    $clicks = $dates[$currentDate]["clicks"];
    $impressions = $dates[$currentDate]["impressions"];

    if ($value['type'] === "click") {
        $clicks = (int)$clicks + 1;
    }

    if ($value['type'] === "impression") {
        $impressions = (int)$impressions + 1;
    }

    $dates[$currentDate] = array("clicks" => $clicks, "impressions" => $impressions);

}

// Reverse the array so that the chart display the data correctly
$dates = array_reverse($dates);
foreach ($dates as $key => $val) {
    ?>
    // Push to array
    maxlotto.clicksAndImpressions.push({"date": "<?php echo $key ?>", "clicks": <?php echo $val['clicks'] ?>, "impressions": <?php echo $val['impressions'] ?>});
    <?php
}
?>

</script>

<script src="/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript" src="/amcharts/amcharts/pie.js"></script>


<h1>Statistics for <?php echo $advert['aaName'] ?></h1>
<form method="get">
    <input type="hidden" name="advert_id" value="<?php echo $advertID ?>">
    Display statistics from <input type="text" name="date-from" placeholder="<?php echo $dateFrom ?>"> to <input type="text" name="date-to" placeholder="<?php echo $dateTo ?>"> <input type="submit" name="submit" value="Filter">
</form>

<form method="get" action="/affiliates/statistics-compare.php">
    <input type="hidden" name="advert_id" value="<?php echo $advertID ?>">
    <select name="advert_id2">
    <?php
    $listOfAdverts = $aAffAdverts->getAll();
    foreach ($listOfAdverts as $currentAdvert) {
        ?>
        <option value="<?php echo $currentAdvert['fk_advert_id'] ?>"><?php echo $currentAdvert['aaName']?></option>
        <?php
    }
    ?>
    </select>
    <input type="submit" value="Compare">
</form>

<div>
    <div id="chartdiv" style="width:100%;height:300px;" data-clicks="<?php echo $numberOfClicks ?>" data-impressions="<?php echo $numberOfImpressions ?>"></div>
    <div id="linechartdiv" style="width:100%;height:300px;" data-clicks="<?php echo $numberOfClicks ?>" data-impressions="<?php echo $numberOfImpressions ?>"></div>
    <p>Clicks: <?php echo $numberOfClicks ?></p>
    <p>Impressions: <?php echo $numberOfImpressions; ?></p>
</div>

<div>
    <h3>Most recent clicks and impressions</h3>
    <h4>Clicks</h4>
    <table class="report" width="100%">
        <tr>
            <th>User Agent</th>
            <th>Date/Time</th>
        </tr>
    <?php
    foreach ($aClicks as $click) {
        ?>
        <tr>
            <td>
                <?php echo $click['user_agent']; ?>
            </td>
            <td>
                <?php echo $click['datetime']; ?>
            </td>
        </tr>
        <?php
    }
    ?>
    </table>

    <h4>Impressions</h4>
    <table class="report" width="100%">
        <tr>
            <th>User Agent</th>
            <th>Date/Time</th>
        </tr>
    <?php
    foreach ($aImpressions as $impression) {
        ?>
        <tr>
            <td>
                <?php echo $impression['user_agent']; ?>
            </td>
            <td>
                <?php echo $impression['datetime']; ?>
            </td>
        </tr>
    <?php
    }
    ?>
    </table>

</div>

<script type="text/javascript" src="/administration/scripts/min/charts.min.js"></script>


<?php include("includes/bottom.php"); ?>