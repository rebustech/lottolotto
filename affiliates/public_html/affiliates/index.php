<?php
    require_once("../system/includes/autoload.php");
    include("includes/top.php");
    include("includes/notifications.php");

    $dateFrom  = filter_input(INPUT_GET, 'date-from');
    $dateTo    = filter_input(INPUT_GET, 'date-to');
    $lotteryID = filter_input(INPUT_GET, 'lottery');
    $minAmount = filter_input(INPUT_GET, 'min-amount');
    $maxAmount = filter_input(INPUT_GET, 'max-amount');

    // Set default dates for placeholders if no dates are specified
    $lastMonth = (!$dateFrom) ? date('Y-m-d', strtotime('-1 month')) : $dateFrom;
    $today     = (!$dateTo) ? date('Y-m-d') : $dateTo;

    $minAmount = (!$minAmount) ? "0.00" : $minAmount;
    $maxAmount = (!$maxAmount) ? "1000" : $maxAmount;

    // Retreive lotteries
    $lotteries = Lottery::getEnabledLotteries();
?>
<h1><?=$oSecurityObject->getsAdminTitle();?> Bookings</h1>

<form>
    Date from <input type="text" name="date-from" placeholder="<?php echo $lastMonth ?>"> to <input type="text" name="date-to" placeholder="<?php echo $today ?>">

     with
    <select name="lottery">
        <option value="">All lotteries</option>
        <?php foreach ($lotteries as $lottery) : ?>
            <option value="<?php echo $lottery['lottery_id'] ?>" <?php echo ($lotteryID === $lottery['lottery_id']) ? "selected" : ""?>><?php echo $lottery['comment'] ?></option>
        <?php endforeach; ?>
    </select>

    between <input type="text" name="min-amount" placeholder="<?php echo $minAmount ?>"> and <input type="text" name="max-amount" placeholder="<?php echo $maxAmount ?>">

    <input type="submit" value="Filter">
</form>
<?php

    $aFilters = array(
        'date-from' => $dateFrom,
        'date-to' => $dateTo,
        'lottery_id' => $lotteryID,
        'min-amount' => $minAmount,
        'max-amount' => $maxAmount
    );

	$oReports = new ReportsAdmin($oSecurityObject, "Affiliates", $aFilters);
	$oReports->display();
?>
<?php include("includes/bottom.php"); ?>