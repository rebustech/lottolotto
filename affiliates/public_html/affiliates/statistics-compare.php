<?php
/**
 * Statistics comparison of two adverts
 */

require_once("../system/includes/autoload.php");
include("includes/top.php");
include("includes/notifications.php");



// Sanitize the input
$advertID = filter_input(INPUT_GET, 'advert_id', FILTER_SANITIZE_SPECIAL_CHARS);
$advertID2 = filter_input(INPUT_GET, 'advert_id2', FILTER_SANITIZE_SPECIAL_CHARS);

$aAffAdverts = new AffiliateAdverts();
$AffiliatesAdvertTracker = new AffiliatesAdvertTracker($advertID);
$AffiliatesAdvertTracker2 = new AffiliatesAdvertTracker($advertID2);

// Pass in user ID
$aAffAdverts->iUserId = $oSecurityObject->getUserID();

// Retrieve the advert
$advert = $aAffAdverts->getAdvertById($advertID);
$advert = $advert[0];

$advert2 = $aAffAdverts->getAdvertById($advertID2);
$advert2 = $advert2[0];

// Get clicks and impressions
$aClicks = $AffiliatesAdvertTracker->getClicks(5);
$aImpressions = $AffiliatesAdvertTracker->getImpressions(5);
$clicksAndImpressions = $AffiliatesAdvertTracker->getClicksAndImpressions();

$aClicks2 = $AffiliatesAdvertTracker2->getClicks(5);
$aImpressions2 = $AffiliatesAdvertTracker2->getImpressions(5);
$clicksAndImpressions2 = $AffiliatesAdvertTracker2->getClicksAndImpressions();

?>

<script>
    // Define array
    var maxlotto = {};
    maxlotto.clicksAndImpressions = [];
    <?php
    $dates = [];
    foreach ($clicksAndImpressions as $value) {
        $currentDate = substr($value['datetime'], 0, 10);
        $isNewDate = true;

        // Check if the date is already in the array
        foreach ($dates as $key => $val) {

            if ($key === $currentDate) {
                $isNewDate = false;
            }
        }


        // Create a new key in array set to $currentDate
        if ($isNewDate) {
            $data = array("clicks" => 0, "impressions" => 0);
            $dates[$currentDate] = $data;
        }

        // Get the clicks and impressions for $currentDate
        $clicks = $dates[$currentDate]["clicks"];
        $impressions = $dates[$currentDate]["impressions"];

        if ($value['type'] === "click") {
            $clicks = (int)$clicks + 1;
        }

        if ($value['type'] === "impression") {
            $impressions = (int)$impressions + 1;
        }

        $dates[$currentDate] = array("clicks" => $clicks, "impressions" => $impressions);

    }

    // Reverse the array so that the chart display the data correctly
    $dates = array_reverse($dates);
    foreach ($dates as $key => $val) {
        ?>
    // Push to array
    maxlotto.clicksAndImpressions.push({"date": "<?php echo $key ?>", "clicks": <?php echo $val['clicks'] ?>, "impressions": <?php echo $val['impressions'] ?>});
    <?php
}
?>
</script>

<script>
    // Define array
    maxlotto.clicksAndImpressions2 = [];
    <?php
    $dates2 = [];
    foreach ($clicksAndImpressions2 as $value) {
        $currentDate = substr($value['datetime'], 0, 10);
        $isNewDate = true;

        // Check if the date is already in the array
        foreach ($dates2 as $key => $val) {

            if ($key === $currentDate) {
                $isNewDate = false;
            }
        }


        // Create a new key in array set to $currentDate
        if ($isNewDate) {
            $data = array("clicks" => 0, "impressions" => 0);
            $dates2[$currentDate] = $data;
        }

        // Get the clicks and impressions for $currentDate
        $clicks = $dates2[$currentDate]["clicks"];
        $impressions = $dates2[$currentDate]["impressions"];

        if ($value['type'] === "click") {
            $clicks = (int)$clicks + 1;
        }

        if ($value['type'] === "impression") {
            $impressions = (int)$impressions + 1;
        }

        $dates2[$currentDate] = array("clicks" => $clicks, "impressions" => $impressions);

    }

    // Reverse the array so that the chart display the data correctly
    $dates2 = array_reverse($dates2);
    foreach ($dates2 as $key => $val) {
        ?>
    // Push to array
    maxlotto.clicksAndImpressions2.push({"date": "<?php echo $key ?>", "clicks": <?php echo $val['clicks'] ?>, "impressions": <?php echo $val['impressions'] ?>});
    <?php
}
?>

</script>

<script src="/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript" src="/amcharts/amcharts/pie.js"></script>

    <h1>Statistics for <?php echo $advert['aaName'] ?> and <?php echo $advert2['aaName'] ?></h1>

    <form method="get" action="/affiliates/statistics-compare.php">
        <input type="hidden" name="advert_id" value="<?php echo $advertID ?>">
        <select name="advert_id2">
            <?php
            $listOfAdverts = $aAffAdverts->getAll();
            foreach ($listOfAdverts as $currentAdvert) {
                ?>
                <option value="<?php echo $currentAdvert['fk_advert_id'] ?>"><?php echo $currentAdvert['aaName']?></option>
            <?php
            }
            ?>
        </select>
        <input type="submit" value="Compare">
    </form>

    <div>
        <div class="charts">
            <h3><?php echo $advert['aaName'] ?></h3>
            <div id="chartdiv" class="compare-stats" style="height:300px; width: 100%;" data-clicks="<?php echo $advert['clicks'] ?>" data-impressions="<?php echo $advert['impressions'] ?>"></div>
            <div id="linechartdiv" class="compare-stats" style="height:300px; width: 100%;" data-clicks="<?php echo $advert['clicks'] ?>" data-impressions="<?php echo $advert['impressions'] ?>"></div>
        </div><!--/.charts1-->

        <div class="charts">
            <h3><?php echo $advert2['aaName'] ?></h3>
            <div id="chartdiv2" class="compare-stats" style="height:300px; width: 100%;" data-clicks="<?php echo $advert2['clicks'] ?>" data-impressions="<?php echo $advert2['impressions'] ?>"></div>
            <div id="linechartdiv2" class="compare-stats" style="height:300px; width: 100%;" data-clicks="<?php echo $advert2['clicks'] ?>" data-impressions="<?php echo $advert2['impressions'] ?>"></div>
        </div><!--/.charts2-->
    </div>

    <div class="clicks">
        <h3>Most recent clicks and impressions</h3>
        <h4>Clicks</h4>
            <div class="compare-stats">
                <?php echo $advert['aaName'] ?> - Clicks: <?php echo $advert['clicks'] ?>
                <?php if (count($aClicks) === 0) {?>
                    <div>No clicks for this advert</div>
                <?php } else { ?>
                    <table class="report">
                        <tr>
                            <th>User Agent</th>
                            <th>Date/Time</th>
                        </tr>
                        <?php
                        foreach ($aClicks as $click) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $click['user_agent']; ?>
                                </td>
                                <td>
                                    <?php echo $click['datetime']; ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php } ?>
            </div><!--/.compare-stats-->
            <div class="compare-stats">
                <?php echo $advert2['aaName'] ?> - Clicks: <?php echo $advert2['clicks'] ?>
                <?php if (count($aClicks2) === 0) {?>
                    <div>No clicks for this advert</div>
                <?php } else { ?>
                    <table class="report" width="100%" height="100%">
                        <tr>
                            <th>User Agent</th>
                            <th>Date/Time</th>
                        </tr>
                        <?php
                        foreach ($aClicks2 as $click2) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $click2['user_agent']; ?>
                                </td>
                                <td>
                                    <?php echo $click2['datetime']; ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php } ?>
            </div><!--/.compare-stats-->
        </div><!--/.clicks-->

        <div class="impressions">
            <h4>Impressions</h4>
            <div class="compare-stats">
                <?php echo $advert['aaName'] ?> - Impressions: <?php echo $advert['impressions'] ?>
                <?php if (count($aImpressions) === 0) { ?>
                    <div>No impressions for this advert</div>
                <?php } else { ?>
                    <table class="report" width="100%">
                        <tr>
                            <th>User Agent</th>
                            <th>Date/Time</th>
                        </tr>
                        <?php
                        foreach ($aImpressions as $impression) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $impression['user_agent']; ?>
                                </td>
                                <td>
                                    <?php echo $impression['datetime']; ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php } ?>
            </div><!--/.compare-stats-->

            <div class="compare-stats">
                <?php echo $advert2['aaName'] ?> - Impressions: <?php echo $advert2['impressions'] ?>
                <?php if (count($aImpressions2) === 0) { ?>
                    <div>No impressions for this advert</div>
                <?php } else { ?>
                    <table class="report" width="100%">
                        <tr>
                            <th>User Agent</th>
                            <th>Date/Time</th>
                        </tr>
                        <?php
                        foreach ($aImpressions2 as $impression) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $impression['user_agent']; ?>
                                </td>
                                <td>
                                    <?php echo $impression['datetime']; ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php } ?>
            </div> <!--/.compare-stats-->
        </div><!--/.impressions-->

    <script type="text/javascript" src="/administration/scripts/min/charts.min.js"></script>


<?php include("includes/bottom.php"); ?>