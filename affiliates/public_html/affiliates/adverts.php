<?php
require_once("../system/includes/autoload.php");
include("includes/top.php");
include("includes/notifications.php");

// Get data for all adverts for user
$AffiliateAdverts = new AffiliateAdverts();
$AffiliateAdverts->iUserId = $oSecurityObject->getUserID();

$adverts = $AffiliateAdverts->getAll();
?>

<h1>Adverts</h1>
<table class="report" width="100%">
    <tr>
        <th>Name</th>
        <th>Width</th>
        <th>Height</th>
        <th>Lottery</th>
        <th>Product</th>
    </tr>
    <?php
    foreach ($adverts as $advert) {
        ?>
        <tr>
            <td>
                <a href="advert-single.php?advert_id=<?php echo $advert['aaID'] ?>"><?php echo $advert['aaName'] ?></a>
            </td>
            <td><?php echo $advert['width'] ?></td>
            <td><?php echo $advert['height'] ?></td>
            <td><?php echo $advert['comment'] ?></td>
            <td><?=$advert['ge_name']; ?></td>
        </tr>
    <?php
    }
    ?>
</table>

<?php include("includes/bottom.php"); ?>
