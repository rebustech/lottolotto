$(document).ready(function () {

    // Get clicks and impressions from data attributes
    var clicks      = $("#chartdiv").attr('data-clicks');
    var impressions = $("#chartdiv").attr('data-impressions');

    // Generate chart
    var chart = AmCharts.makeChart("chartdiv",{
        "type"		: "pie",
        "titleField"	: "category",
        "valueField"	: "column-1",
        "dataProvider"	: [
            {
                "category": "Clicks",
                "column-1": clicks
            },
            {
                "category": "Impressions",
                "column-1": impressions
            }
        ]
    });


    var clicks2      = $("#chartdiv2").attr('data-clicks');
    var impressions2 = $("#chartdiv2").attr('data-impressions');

    // Generate chart
    var chart2 = AmCharts.makeChart("chartdiv2",{
        "type"		: "pie",
        "titleField"	: "category",
        "valueField"	: "column-1",
        "dataProvider"	: [
            {
                "category": "Clicks",
                "column-1": clicks2
            },
            {
                "category": "Impressions",
                "column-1": impressions2
            }
        ]
    });

    var statisticsData = maxlotto.clicksAndImpressions;
    var statisticsData2 = maxlotto.clicksAndImpressions2;

    var lineChart = AmCharts.makeChart("linechartdiv", {
        "type": "serial",
        "dataProvider": statisticsData,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id":"v1",
            "axisAlpha": 0,
            "position": "left"
        }],
        "graphs": [{
            "balloonText": "[[value]] clicks",
            "valueField": "clicks"
        }, {
            "balloonText": "[[value]] impressions",
            "valueField": "impressions"
        }
        ],

        "chartCursor": {
            "cursorPosition": "mouse",
            "pan": true,
            "valueLineEnabled":true,
            "valueLineBalloonEnabled":true
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true,
            "position": "top"
        }
    });

    var lineChart2 = AmCharts.makeChart("linechartdiv2", {
        "type": "serial",
        "dataProvider": statisticsData2,
        "dataDateFormat": "YYYY-MM-DD",
        "valueAxes": [{
            "id":"v1",
            "axisAlpha": 0,
            "position": "left"
        }],
        "graphs": [{
            "balloonText": "[[value]] clicks",
            "valueField": "clicks"
        }, {
            "balloonText": "[[value]] impressions",
            "valueField": "impressions"
        }
        ],

        "chartCursor": {
            "cursorPosition": "mouse",
            "pan": true,
            "valueLineEnabled":true,
            "valueLineBalloonEnabled":true
        },
        "categoryField": "date",
        "categoryAxis": {
            "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true,
            "position": "top"
        }
    });
});