var ll={}

ll.TabbedInterface={
    setup:function(){
        $('.TabbedInterface > ul > li').click(function(){
            var iSelectedTab=$(this).parent().find('li').index(this);
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().parent().find('> .TabbedInterfaceItems > .TabbedInterfaceContent').eq(iSelectedTab).addClass('active').siblings().removeClass('active');
        });
        $('.TabbedInterface').each(function(){
            $(this).find('> ul > li').eq(0).click();
        });
    }
}

ll.Popups={
    setup:function(){
        $('.v2popup').append('<button class="closeButton">X</button>');
        $('.v2popup').append('<div class="popupContent detailsform"></div>');
        $('.v2popup .closeButton').click(function(){
            $(this).parent().removeClass('visible');

            var eActiveComponent=$('.game_engine_holder li > div.active');
            if(eActiveComponent.length>0){
                var eActiveForm=eActiveComponent[0].eForm;
                $('.game_engine_data').append(eActiveForm);
            }


            return false;
        })

    }
}

$(document).ready(function(){
    ll.TabbedInterface.setup();
    ll.Popups.setup();
})