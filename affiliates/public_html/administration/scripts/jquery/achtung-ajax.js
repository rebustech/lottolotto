var notificationBox = null;
var achtungBox = null;
function startAchtungAjax(){
	achtungBox = $.achtung({
		className: 'achtungWait',
		icon: 'wait-icon',
		disableClose: true,
		message: 'Please wait.. '
	});
}

function doAchtungAjax(sUrl, sData, sOnCompleteFunction){
	 $.ajax({
	   type: "GET",
	   url: sUrl,
	   data: sData,
	   dataType: "html",
	   success: function(msg){
		    showAchtungResponse(msg, '');
			if(sOnCompleteFunction){
				eval(sOnCompleteFunction + "()");
			}
	   },
	  error: function(msg){
		 achtungBox.achtung('update', {message: "Problem encountered. " + msg, timeout:3, className: "achtungFail", icon: "ui-icon-close" });
	  }
	 });
}

function showAchtungResponse(msg, statusText){
	var achtungClass = "achtungSuccess";
	if(msg.indexOf("{messagetype}") > 0){
		var iStartPos = msg.indexOf("{messagetype}");
		var msgStart = iStartPos + 13;
		var msgEnd = msg.indexOf("{/messagetype}");
		achtungClass = msg.substring(msgStart, msgEnd);
		var sMsgInit = msg.substring(0, iStartPos);
		var sMsgEnd = msg.substring(msgEnd + 14, msg.length);
		msg = sMsgInit + sMsgEnd;
	}
	
	if(achtungClass == "achtungSuccess"){
		var iconClass = "ui-icon-check";	
		var iTimeout = 3;
	}
	else{
		var iconClass = "ui-icon-close";
		var iTimeout = 6;
	}
	
	if(achtungBox != null){
		achtungBox.achtung('update', {message: msg, timeout:iTimeout, className: achtungClass, icon: iconClass });
	}
	else{
		achtungBox = $.achtung({
			className: achtungClass,
			icon: iconClass,
			timeout:iTimeout,
			message: msg
		});
	}
}