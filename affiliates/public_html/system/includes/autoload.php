<?php

	$sIncludePath = findDocumentRoot(realpath('.'));
        $_SERVER['DOCUMENT_ROOT']=$sIncludePath;
        /**
         * Function to find public_html. Because our server setup
         * has dynamic virtual hosts we can't rely on DOCUMENT_ROOT
         * @param type $path
         */
        function findDocumentRoot($path){
            //Correct the path for windows sytems
            $path=  str_replace('\\', '/', $path);

            $aParts=explode('/',$path);
            $bFound=false;
            $sPath='';
            foreach($aParts as $part){
                if($bFound) $sPath.='../';
                if($part=='public_html') $bFound=true;
            }
            return substr($sPath,0,-1);
        }

        function includePath($path){
            $sNewIncludePath=PATH_SEPARATOR.$path;
            $d=opendir($path);
            while($f=readdir($d)){
                $sNewPath=$path.'/'.$f;
                if(is_dir($sNewPath) && $f!='.' && $f!='..'){
                    $sNewIncludePath.=includePath($sNewPath);
                }
            }
            closedir($d);
            return $sNewIncludePath;
        }
        $sNewIncludePath=includePath(realpath($sIncludePath).'/system');
        $sModulesIncludePath=includePath(realpath($sIncludePath).'/../../modules');
        set_include_path(get_include_path().PATH_SEPARATOR.$sNewIncludePath.PATH_SEPARATOR.$sModulesIncludePath);

        /**
         * This autoloader just tries to include the file that matches the class name
         * The code above here will have already set the include path to include all the
         * subfolders in the project so PHP will handle locating the file natively
         * @param type $class_name
         */
        spl_autoload_register(function($aClassName) {
            $sClassFile=str_replace('\\','',$aClassName).'.class.php';
            @include($sClassFile);
	});

        function number_format_locale($number,$decimals=2) {
            $locale = localeconv();
            return number_format($number,$decimals,
                       $locale['decimal_point'],
                       $locale['thousands_sep']);
        }

        /**
         * Load the constants file, which includes database config and start
         * the session
         */
	require_once("includes/constants.php");
        @session_start();
        session_regenerate_id();
