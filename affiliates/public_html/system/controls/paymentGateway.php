<?php
include("../includes/autoload.php");
switch ( $_GET['g'] )
{
    case 'moneybookers':
        $oPaymentGateway = new MoneyBookers();
        $oPaymentGateway->ConfirmPayment($_POST);
    break;
    
    case 'transactium' :
        $oPaymentGateway = new Transactium();
        if ( $oPaymentGateway->ConfirmPayment($_GET['hpsid']) )
        {
            header("location: http://" . WEBSITEURL . $oPaymentGateway->getSuccessPage() );
        }
        else
        {
            header("location: http://" . WEBSITEURL . $oPaymentGateway->getCancelPage() );
        }
        exit;
    break;
    
    case 'payfrex' :
        $oPaymentGateway = new PayFrex();
        
        // Read the incoming input stream
        $sXML = file_get_contents("php://input");
		
		// Debug file
		file_put_contents(dirname(__FILE__).'/test.xml', $sXML);
        
		/*
        $sTablename = "payfrexresponse";
        $aData = array(
            'entry_time' => date('Y-m-d H:i:s'),
            'data' => 'hello world'
        );
        DAL::Insert($sTablename, $aData);
		*/
        
        $oPaymentGateway->ConfirmPayment($sXML);				
    break;

    default:
        header("location: /403.html" );
        exit;
    break;
}
?>