<?php
// include NUSOAP library
require_once('../autoload.php');

// Create the server instance
$server = new soap_server;

$server->configureWSDL('wsdl', 'urn:wsdl');

// Include Structs/Register/Function 
foreach ( glob("structs/*.php") as $functionFilename )
{
	require_once($functionFilename);
}

// Use the request to invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

if ($HTTP_RAW_POST_DATA) {
	$sMessage = str_replace(">",">\r\n",$HTTP_RAW_POST_DATA);
}
	
$server->service($HTTP_RAW_POST_DATA);
?>