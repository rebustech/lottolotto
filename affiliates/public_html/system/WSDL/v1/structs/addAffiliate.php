<?php

if (!is_object($server)) {
	header("Location:../wsdl.php?wsdl");
	exit;
}

/*-----------------------------------------------------------*/
/* getRoomIDs Structures                 
/*-----------------------------------------------------------*/

$server->wsdl->addComplexType(
    'addAffiliate_Input',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' 	=> array('name' => 'username', 'type' => 'xsd:string'),
        'password' 	=> array('name' => 'password', 'type' => 'xsd:string'),
		'pin' 		=> array('name' => 'pin', 'type' => 'xsd:string'),
		'websiteid'	=> array('name' => 'websiteid', 'type' => 'xsd:int'),
		'visitorid'	=> array('name' => 'visitorid', 'type' => 'xsd:int'),
		'pageid'	=> array('name' => 'pageid', 'type' => 'xsd:int'),
		'languageid'=> array('name' => 'languageid', 'type' => 'xsd:int'),
		'sectionid'	=> array('name' => 'sectionid', 'type' => 'xsd:int'),
		'productid'	=> array('name' => 'productid', 'type' => 'xsd:int'),
		'querystring'=> array('name' => 'querystring', 'type' => 'xsd:string')
    )
);

/*-----------------------------------------------------------*/
/* getRoomIDs Register              
/*-----------------------------------------------------------*/

$server->register( 
	'addAffiliate',                           					 	// method name
    array('addAffiliate_Input' => 'tns:addAffiliate_Input'),  	// input parameters
    array('return' => 'xsd:int'),					    		  	// output parameters
    'uri:wsdl',                   							 	 	// namespace
    'uri:wsdl/addAffiliate',             				   			// SOAPAction
    'rpc',                              							// style
    'encoded',                           							// use
	'addAffiliate Function : Adds a new navigational page for a visitor for a website with profile type 1'			// documentation
	);
	
/*-----------------------------------------------------------*/
/* getRoomIDs Function                   
/*-----------------------------------------------------------*/

function addAffiliate($addAffiliate_Input) {

	extract($addAffiliate_Input, EXTR_OVERWRITE);
	$iUserID = WebServices::Authenticate($username, $password, $pin);
	if ( $iUserID ) {
		if ( empty($websiteid) || Security::ValidateAccessToWebsite($iUserID, $websiteid) )
		{
			return ProfileType1::addAffiliatePage($websiteid, $visitorid, $languageid, $pageid, $sectionid, $productid, $querystring);
		}
		else
		{
			return new soap_fault('Client', '', 'No access to the given Website ID');
		}
	}
	else 
	{
		return new soap_fault('Client', '', 'Incorrect Authentication Details');
	}
	
}

$aDescription['addAffiliate'] = "This function adds a new navigational page with the given parameters for users with profile type 1";

$aInput['addAffiliate'] =  array(
        'username' => array('type' => 'string','doc' => 'Username of Profiler Manager.'),
        'password' => array('type' => 'string','doc' => 'Password of Profiler Manager.'),
		'pin' => array('type' => 'string','doc' => 'Pin of Profiler Manager.'),
		'websiteid' => array('type' => 'int','doc' => 'Website ID'),
		'visitorid' => array('type' => 'int','doc' => 'Visitor ID'),
		'sectionid' => array('type' => 'int','doc' => 'Section ID'),
		'languageid' => array('type' => 'int','doc' => 'Language ID'),
		'pageid' => array('type' => 'int','doc' => 'Page ID'),
		'productid' => array('type' => 'int','doc' => 'Optional: Product ID'),
		'querystring' => array('type' => 'int','doc' => 'Optional: Query String'),
	);
	
$aOutput['addAffiliate'] =  array(
		'return' => array('type' => 'int', 'doc' => 'Flag: 1 on Successful, 0 on Fail')
	);

?>