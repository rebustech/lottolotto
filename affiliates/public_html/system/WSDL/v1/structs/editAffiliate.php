<?php

if (!is_object($server)) {
	header("Location:../wsdl.php?wsdl");
	exit;
}

/*-----------------------------------------------------------*/
/* getRoomIDs Structures                 
/*-----------------------------------------------------------*/

$server->wsdl->addComplexType(
    'getMostPopularProducts_1_Input',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' 	=> array('name' => 'username', 	'type' => 'xsd:string'),
        'password' 	=> array('name' => 'password', 	'type' => 'xsd:string'),
		'pin' 		=> array('name' => 'pin', 		'type' => 'xsd:string'),
		'websiteid' => array('name' => 'websiteid', 'type' => 'xsd:int'),
		'languageid'=> array('name' => 'languageid','type' => 'xsd:int'),
		'sectionid' => array('name' => 'sectionid',	'type' => 'xsd:int'),
		'limit'		=> array('name' => 'limit',		'type' => 'xsd:int', 'default' => 4)
		
    )
);

$server->wsdl->addComplexType(
    'getMostPopularProducts_1_ArrayOutput',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:getMostPopularProducts_1_Output[]')
    ),
    'tns:getMostPopularProducts_1_Output'
);
$server->wsdl->addComplexType(
    'getMostPopularProducts_1_Output',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'product_id' => array('name' => 'product_id', 'type' => 'xsd:int'),
        'views' 	=> array('name' => 'views', 'type' => 'xsd:int')
    )
);

/*-----------------------------------------------------------*/
/* getRoomIDs Register              
/*-----------------------------------------------------------*/

$server->register( 
	'getMostPopularProducts_1',                           					 	// method name
    array('getMostPopularProducts_1_Input' => 'tns:getMostPopularProducts_1_Input'),     		// input parameters
    array('return' => 'tns:getMostPopularProducts_1_ArrayOutput'),    		  	// output parameters
    'uri:wsdl',                   							 	 	// namespace
    'uri:wsdl/getMostPopularProducts_1',             				   			// SOAPAction
    'rpc',                              							// style
    'encoded',                           							// use
	'getMostPopularProducts_1 Function : Returns back an array of Products IDs recently viewed by the visitor'			// documentation
	);
	
/*-----------------------------------------------------------*/
/* getRoomIDs Function                   
/*-----------------------------------------------------------*/

function getMostPopularProducts_1($getMostPopularProducts_1_Input) {

	extract($getMostPopularProducts_1_Input, EXTR_OVERWRITE);
	$iUserID = Security::Authenticate($username, $password, $pin);
	if ( $iUserID ) {
		if ( Security::ValidateAccessToWebsite($iUserID, $websiteid) )
		{
			return ProfileType1::getMostViewedProducts($websiteid, $languageid, $sectionid, $limit);
		}
		else
		{
			return new soap_fault('Client', '', 'No access to the given Website ID');
		}
	}
	else 
	{
		return new soap_fault('Client', '', 'Incorrect Authentication Details');
	}
	
}

$aDescription['getMostPopularProducts_1'] = "This function returns the the most popular products in a section.";

$aInput['getMostPopularProducts_1'] =  array(
        'username' 	=> array('type' => 'string','doc' => 'Username of Profiler Manager.'),
        'password' 	=> array('type' => 'string','doc' => 'Password of Profiler Manager.'),
		'pin' 		=> array('type' => 'string','doc' => 'Pin of Profiler Manager.'),
		'websiteid' => array('type' => 'int', 	'doc' => 'Website ID'),
		'languageid'=> array('type' => 'int',	'doc' => 'Language ID'),
		'sectionid' => array('type' => 'int',	'doc' => 'Section ID to filter on'),
		'limit'		=> array('type' => 'int',	'doc' => "Optional: Number of items to return back, default is 4")

	);

$aOutput['getMostPopularProducts_1'] =  array(
		'product_id' => array('type' => 'int', 'doc' => 'An Array of Product IDs'),
		'views' => array('type' => 'int', 'doc' => 'Number of Views for the Product')
	);

?>