<?php

if (!is_object($server)) {
	header("Location:../wsdl.php?wsdl");
	exit;
}

/*-----------------------------------------------------------*/
/* getRoomIDs Structures                 
/*-----------------------------------------------------------*/

$server->wsdl->addComplexType(
    'getSimilarProducts_1_Input',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' 	=> array('name' => 'username', 	'type' => 'xsd:string'),
        'password' 	=> array('name' => 'password', 	'type' => 'xsd:string'),
		'pin' 		=> array('name' => 'pin', 		'type' => 'xsd:string'),
		'websiteid' => array('name' => 'websiteid', 'type' => 'xsd:int'),
		'visitorid' => array('name' => 'visitorid', 'type' => 'xsd:int'),
		'languageid'=> array('name' => 'languageid','type' => 'xsd:int'),
		'sectionid' => array('name' => 'sectionid',	'type' => 'xsd:int'),
		'productid' => array('name' => 'productid',	'type' => 'xsd:int'),
		'limit'		=> array('name' => 'limit',		'type' => 'xsd:int', 'default' => 3)
		
    )
);

$server->wsdl->addComplexType(
    'getSimilarProducts_1_ArrayOutput',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:getSimilarProducts_1_Output[]')
    ),
    'tns:getSimilarProducts_1_Output'
);
$server->wsdl->addComplexType(
    'getSimilarProducts_1_Output',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'product_id' => array('name' => 'product_id', 'type' => 'xsd:int'),
        'weight' 	=> array('name' => 'weight', 'type' => 'xsd:string')
    )
);

/*-----------------------------------------------------------*/
/* getRoomIDs Register              
/*-----------------------------------------------------------*/

$server->register( 
	'getSimilarProducts_1',                           					 	// method name
    array('getSimilarProducts_1_Input' => 'tns:getSimilarProducts_1_Input'),     		// input parameters
    array('return' => 'tns:getSimilarProducts_1_ArrayOutput'),    		  	// output parameters
    'uri:wsdl',                   							 	 	// namespace
    'uri:wsdl/getSimilarProducts_1',             				   			// SOAPAction
    'rpc',                              							// style
    'encoded',                           							// use
	'getSimilarProducts_1 Function : Returns back an array of Products IDs recently viewed by the visitor'			// documentation
	);
	
/*-----------------------------------------------------------*/
/* getRoomIDs Function                   
/*-----------------------------------------------------------*/

function getSimilarProducts_1($getSimilarProducts_1_Input) {

	extract($getSimilarProducts_1_Input, EXTR_OVERWRITE);
	$iUserID = Security::Authenticate($username, $password, $pin);
	if ( $iUserID ) {
		if ( Security::ValidateAccessToWebsite($iUserID, $websiteid) )
		{
			return ProfileType1::getSimilarProducts($websiteid, $visitorid, $languageid, $sectionid, $productid, $limit);
		}
		else
		{
			return new soap_fault('Client', '', 'No access to the given Website ID');
		}
	}
	else 
	{
		return new soap_fault('Client', '', 'Incorrect Authentication Details');
	}
	
}

$aDescription['getSimilarProducts_1'] = "This function returns the recently viewed products by a particular user.";

$aInput['getSimilarProducts_1'] =  array(
        'username' 	=> array('type' => 'string','doc' => 'Username of Profiler Manager.'),
        'password' 	=> array('type' => 'string','doc' => 'Password of Profiler Manager.'),
		'pin' 		=> array('type' => 'string','doc' => 'Pin of Profiler Manager.'),
		'websiteid' => array('type' => 'int', 	'doc' => 'Website ID'),
		'visitorid' => array('type' => 'int',	'doc' => 'Visitor ID'),
		'languageid'=> array('type' => 'int',	'doc' => 'Language ID'),
		'sectionid' => array('type' => 'int',	'doc' => 'Section ID to filter on'),
		'productid'	=> array('type' => 'int',	'doc' => "Product ID to exclude from the response"),
		'limit'		=> array('type' => 'int',	'doc' => "Optional: Number of items to return back")

	);

$aOutput['getSimilarProducts_1'] =  array(
		'product_id' => array('type' => 'int', 'doc' => 'An Array of Product IDs'),
		'weight' => array('type' => 'string', 'doc' => 'Weight of Product, returned as a string representing a long int')
	);

?>