<?php

# Disable local Op caching
if(function_exists('opcache_reset')) {
    opcache_reset();
}

# Hide PHP 5.5.10 strict warnings
error_reporting(0);

/**
 * Config for Wez's laptop
 */
Config::$config->bSendEmails=true;
Config::$config->sDatabaseUser='root';
Config::$config->sDatabasePassword='';
Config::$config->sDatabaseName = 'maxlotto';

# Lazy switch between my OS's
$agent = $_SERVER['HTTP_USER_AGENT'];

if(preg_match('/Linux/',$agent)) $os = 'Linux';
elseif(preg_match('/Win/',$agent)) $os = 'Windows';
elseif(preg_match('/Mac/',$agent)) $os = 'Mac';
else $os = 'UnKnown';

Config::$config->sServerPath='/Users/Wez/Sites/fftrading/web_maxlotto.com/public_html';
Config::$config->sWebsiteUrl = 'http://affiliates.wez.local.maxlotto.com';

// Location of main maxlotto website
Config::$config->sWebsiteMainUrl = 'http://wez.local.maxlotto.com';

Config::$config->iLanguageId=1;
//setlocale(LC_ALL, 'de_DE', 'de', 'german');
Config::$config->sZopimLanguageKey='de';
