<?php

class AuditLog extends LLModel{
    static $sTableName='audit_log';

    var $id;
    var $datetime;
    var $ip_address;
    var $code;
    var $message;
    var $model;
    var $fk_id;
    var $user_id;

    static function LogItem($message,$code,$model,$fk_id){
        $oLogItem=new AuditLog();
        $oLogItem->message=$message;
        $oLogItem->code=$code;
        $oLogItem->model=(is_object($model))?get_class($model):$model;
        $oLogItem->fk_id=$fk_id;
        $oLogItem->ip_address=$_SERVER['REMOTE_ADDR'];
        $oLogItem->save();
    }

    function save($aData=array()){
        global $oSecurityObject;
        $aData['datetime']=date('Y-m-d H:i:s');
        $aData['message']=$this->message;
        $aData['code']=$this->code;
        $aData['model']=$this->model;
        $aData['fk_id']=$this->fk_id;
        $aData['ip_address']=$this->ip_address;
        $aData['user_id']=$oSecurityObject->getiUserID();
        parent::save($aData);
    }
}