<?php

/**
 * Base class for all models. Provides useful methods for manipulating and populating
 * models
 *
 * @package LoveLotto
 */
class LLModel{
    static $sTableName='';
    static $sKeyField='id';
    static $sHandlerField='';

    /**
     * Copies all the object variables from $oObject to $this.
     * If $bOnlyObjectProperties is set to true then is populates all the object
     * variables in $this with the equivalent value in $oObject
     * @param type $oObject
     * @param type $bOnlyObjectProperties
     */
    function populateFromObject($oObject,$bOnlyObjectProperties=false){
        if($bOnlyObjectProperties){
            $aKeys=get_object_vars($this);
            foreach($aKeys as $k=>$v){
                $this->$k=$oObject->$k;
            }
        }else{
            $aKeys=get_object_vars($oObject);
            foreach($aKeys as $k=>$v){
                $this->$k=$v;
            }
        }
    }

    /**
     * Copies all the values from $aArray to $this, mapping the key to the property
     * name and the value to the property value.
     * If $bOnlyObjectProperties is set to true then is populates all the object
     * variables in $this with the value of the matching key in $aArray
     * @param type $oObject
     * @param type $bOnlyObjectProperties
     */
    function populateFromArray($aArray,$bOnlyObjectProperties=false){
        if($bOnlyObjectProperties){
            $aKeys=get_object_vars($this);
            foreach($aKeys as $k=>$v){
                $this->$k=$aArray[$k];
            }
        }else{
            foreach($aArray as $k=>$v){
                $this->$k=$v;
            }
        }
    }


    function beforeDALUpdate($aFields, $sWhereQuery){

    }

    function afterDALUpdate($aFields, $sWhereQuery){

    }

    function beforeGenericUpdate($aFields){

    }

    function afterGenericUpdate($aFieldsBeforeSave=null){

    }

    /**
     * Much better save method for saving a model to the database
     * Pass in an array of data to be saved. The model should override $sTableName
     * and $sKeyField
     * @param array $aData
     */
    function save($aData){
        $sKeyName=static::$sKeyField;
        $mKeyValue=$this->$sKeyName;
        if($mKeyValue=='' || $mKeyValue=='null'){
            $mNewKey=DAL::Insert(static::$sTableName, $aData,true);
            $this->$sKeyName=$mNewKey;
        }else{
            DAL::Update(static::$sTableName, $aData, "$sKeyName='$mKeyValue'");
        }
    }

    static function getById($iId){
        $sWhere='`'.static::$sKeyField.'`='.intval($iId);
        return self::getByWhere($sWhere);
    }

    static function getByField($sField,$sValue){
        $sWhere='`'.$sField."`='".mysql_escape_string($sValue)."'";
        return self::getByWhere($sWhere);
    }

    static function getByWhere($sWhere){
        /**
         * Try the cache first.
         */
        $sCacheKey=static::$sTableName.'['.str_replace(' ','_',$sWhere).']';
        //$oModel=LLCache::getObject($sCacheKey);

        /**
         * If it's not there get from the database, then pop in the cache
         */
        if(!is_object($oModel)){
            $sSql='SELECT * FROM `'.static::$sTableName.'` WHERE '.$sWhere;
            $aParentData=DAL::executeGetRow($sSql);

            if($aParentData===false) return null;

            /**
             * If we have a handler field then this is a dynamic object where the
             * specific class to use has been set in the database. Otherwise instatiate
             * itself. the syntax new static() is from php.net/lsb
             */
            if(static::$sHandlerField!=''){
                $sHandler=$aParentData[static::$sHandlerField];
                if($sHandler!=''){
                    $oModel=new $sHandler;
                }else{
                    /**
                     * Undesirable path - should log anything going through here
                     */
                    $oModel=new static();
                }
            }else{
                $oModel=new static();
            }
            $oModel->populateFromArray($aParentData);

            LLCache::addObject($sCacheKey,$oModel);

        }

        return $oModel;
    }

    static function getAll(){
        return self::getAllWhere('1=1');
    }

    static function getAllActive(){
        return self::getAllWhere('`isactive`=1');
    }

    static function getAllWhere($sWhere){
        /**
         * Try the cache first.
         */
        $sCacheKey=static::$sTableName.'_ALL_['.str_replace(' ','_',$sWhere).']';
        //$oModel=LLCache::getObject($sCacheKey);

        /**
         * If it's not there get from the database, then pop in the cache
         */
        if(!is_object($oModel)){
            $sSql='SELECT * FROM `'.static::$sTableName.'` WHERE '.$sWhere;
            $aParentItems=DAL::executeQuery($sSql);

            if($aParentItems===false) return array();

            $aOut=array();

            /**
             * If we have a handler field then this is a dynamic object where the
             * specific class to use has been set in the database. Otherwise instatiate
             * itself. the syntax new static() is from php.net/lsb
             */
            foreach($aParentItems as $aParentData){
                if(static::$sHandlerField!=''){
                    $sHandler=$aParentData[static::$sHandlerField];
                    if($sHandler!=''){
                        $oModel=new $sHandler;
                    }else{
                        /**
                         * Undesirable path - should log anything going through here
                         */
                        $oModel=new static();
                    }
                }else{
                    $oModel=new static();
                }
                $oModel->populateFromArray($aParentData);
                $aOut[]=$oModel;
            }
            LLCache::addObject($sCacheKey,$aOut);

        }

        return $aOut;
    }


    function log($code,$message){
        $sKeyName=static::$sKeyField;
        $mKeyValue=$this->$sKeyName;
        AuditLog::LogItem($message, $code, $this, $mKeyValue);
    }
}
