<?php

	$bStopOutput = false;
	if ( $_SERVER['SCRIPT_NAME'] == 'scripts.php' )
	{
		$aServerArgs = Scripts::getServerArgs();
		if ( $aServerArgs )
		{
			$oSecurityObject->validateLogin($aServerArgs['username'], $aServerArgs['password'], $aServerArgs['pin']);
		}
		$_GET['tablename'] = $aServerArgs['tablename'];
		$_GET['execute'] = $aServerArgs['execute'];
		$_POST = $aServerArgs;
		$bStopOutput = true;
		$bNoRedirect = true;
	}

	if ( is_object($oSecurityObject) && $oSecurityObject->getbIsLoggedIn() == 1 )
	{
		if ( $oSecurityObject->checkAuth($bNoRedirect, $bStopOutput) == false )
		{
			$oSecurityObject->Logout();
			if(file_exists("system/includes/session_end.php")) require_once("system/includes/session_end.php");
			else require_once("includes/session_end.php");
			?>
			<script language="javascript">
			window.location="/<?=$oSecurityObject->getsFolder()?>login.php";
			</script>
			<?php
			exit;
		}
	}
	elseif ( is_object($oSecurityObject) )
	{
		$oSecurityObject->Logout();
		if(file_exists("system/includes/session_end.php")) require_once("system/includes/session_end.php");
		else require_once("includes/session_end.php");
		if ( $bNoRedirect == true )
		{
			$sErrors = "<h2>Authentication Problem</h2>\r\n<div class='error' style='display:block'>";
			$sErrors .= "<ul><li>Not Logged In</li></ul>";
			$sErrors .= "</div>\r\n";
			if ( $bStopOutput )
			{
				$sErrors = strip_tags($sErrors);
			}
			echo $sErrors;
		}
		else
		{
			?>
			<script language="javascript">
			window.location="/<?=$oSecurityObject->getsFolder()?>login.php";
			</script>
			<?php
		}
		exit;
	}
	else
	{
		unset($oSecurityObject);
		if(file_exists("system/includes/session_end.php")) require_once("system/includes/session_end.php");
		else require_once("includes/session_end.php");
		if ( $bNoRedirect == true )
		{
			$sErrors = "<h2>Authentication Problem</h2>\r\n<div class='error' style='display:block'>";
			$sErrors .= "<ul><li>Not Logged In</li></ul>";
			$sErrors .= "</div>\r\n";
			if ( $bStopOutput )
			{
				$sErrors = strip_tags($sErrors);
			}
			echo $sErrors;
		}
		else
		{
			?>
			<script language="javascript">
			window.location="login.php";
			</script>
			<?php
		}
		exit;
	}

	$oErrors = new Error($oSecurityObject->getsAdminType());
	if($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"] == "") $_SESSION[$oSecurityObject->getsAdminType() ."iLangID"] = 0;

?>