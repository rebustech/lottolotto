# README #

I've just sent you part of the system here so you can get a flavour of it. The main body of what I've sent you is the entirety of the game engine module. This includes the game engine cores and game engine administration, management and API.

I've also included part of the entry point for the admin system. The way that this works is that the modules folder is installed outside of the web root. You can then setup multiple websites to use the same core. Each website decides how to handle it's routing. The routing system I've included here is able to route to either administration controllers or API controllers. There is also a router for frontend controllers which are used to build the website.

I have also included the payment gateways so you can see how these are implemented.

As noted on our phone call the DB Layer needs redoing, preferably migrating to PDO. I don't think the task of doing that will be too taxing, just a bit of donkey work for maybe 10-14 days. At the moment you'll see serveral sections of inline SQL, so we'd look to getting those replaced before launching. This was a task we had planned to carry out as you'll see in some of the comments. To my mind this is the main task that needs completing to get the system good to go along with any integration work and plenty of testing.

If you need to see any more please let me know and I'll sort it out for you.

FYI. This is a complete list of the modules:

* API
* APICore
* Admin
* AdminControllers
* Affiliates
* BIZ
* CMS
* CRM
* Campaigns
* ConfigurationEngines
* Controls
* DATA
* DashboardWidgets
* Exceptions
* FAQ/Installer
* FeMessaging
* GBGroup/PostcodeLookup
* GameEngines
* Guzzle
* Helper
* Installer
* Kraken
* MobileDetect
* OAuth2
* PHPMailer-master
* Pariplay
* PaymentGateways
* PaymentProcessing
* ProcessControllers
* RNG
* Results
* SOAP
* Scraper
* Silverpop
* Syndicates
* TicketEngines
* UI
* VariantsCMS
* Withdrawals
* Models
* Digidis
* NetLotto
* Payfrex
* PimsInsurance
* BluefinInsurance