<?php

require 'system/includes/autoload.php';

$sql='SELECT c.value,`key` FROM variants_variant_content c INNER JOIN variants_variants v ON v.id=c.fk_variant_id INNER JOIN variants_blocks b ON b.id=v.fk_block_id WHERE c.fk_language_id=1';
$aData=\DAL::executeQuery($sql);
$iWords=0;
foreach($aData as $aRow){
    $sLine=strip_tags($aRow['value']);
    for($a=0;$a<1000;$a++) $sLine=str_replace('  ',' ',$sLine);
    $iDWords=substr_count($sLine, ' ');
    if($iDWords>100) echo "<li>".$aRow['key'].' : '.$iDWords;
    $iWords+=$iDWords;
    $iBlocks++;
}

echo "<li>TOTAL:".$iWords.' in '.$iBlocks;