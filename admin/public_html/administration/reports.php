<?php
include ("includes/top.php");
//SHORTCUT - alt+5 clears report cache
?>
<h1><?=$oPage->sTitle?></h1>
<?php

if ( $oPage->sTablename )
{
	if ( is_array($oPage->aFilters) )
	foreach ( $oPage->aFilters as $aPageFilter)
	{
		if ( array_key_exists($aPageFilter['id'], $aFilterData ) )
		{
			$aFilters[$aPageFilter['id']] = $aFilterData[$aPageFilter['id']];
		}
	}

	$aSelectedUserTypes[] = "4";
	?>
    <ul class="subnav">
    	<li>
    		<a href="<?=$oPage->sFilename?>" class="largebutton">Back to Reports</a>
        </li>
    	<li>
    		<a href="" class="largebutton" onclick="printPage(); return false;">Print Report</a>
        </li>
    </ul>
	<div id="reports">
    <?php

	//Code to get filters here
	$oReports = new ReportsAdmin($oSecurityObject, $oPage->sTablename, $aFilters);
	if ( $bIgnoreCache )
	{
			$sURL = $_SERVER['PHP_SELF'] . "?tablename=" . $_GET['tablename'];
			unset($oReports);
			?>
            <script type="text/javascript">
				window.location = "<?=$sURL?>";
			</script>
            <?php
			exit;
	}
	if ( $_GET['view'] == "chart" )
	{
		?>
        <div class='reportchart'>
			<img id="report-chart" title="<?=$oPage->sTitle . " - " . $_GET['display']?>" alt="<?=$oPage->sTitle . " - " . $_GET['display']?>" src='actions/reports-charts.php?tablename=<?=$oPage->sTablename?>&pp=<?=$oPage->sFilename?>&display=<?=$_GET['display']?>&options=<?=serialize($aFilters)?>'/>
		</div>
        <?php
	}
	else
	{
		$oReports->display();
	}
	?>
    </div>
    <?php
	unset($oReports);
}
else
{
	$aReports = $oSecurityObject->getNavigationPages($oPage->iID);
	?>
    <div id="reports">
    <ul>
    <?php
	foreach ($aReports as $aReport )
	{
	?>
            <li>
        	<a href="<?=$aReport["filename"];?><?php if($aReport["tablename"]){ ?>?tablename=<?=$aReport["tablename"]; }?>"><?=$aReport["title"];?></a>
            </li>
    <?php
	}
	?>
	</ul>
    </div>
    <?php
}
include ("includes/bottom.php");
?>