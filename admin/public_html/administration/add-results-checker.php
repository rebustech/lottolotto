<?php
/**
 * This is the page where a nominated 'checker' can fill in lottery numbers and draw information
 */

$bypass_login = true;
    include ("includes/top.php");
    include ("includes/errors.php");
?>
<h1><?=$oPage->sTitle?></h1>
<?php 
if ( is_array($oPage->aFilters) )
foreach ( $oPage->aFilters as $aPageFilter)
{
    if ( array_key_exists($aPageFilter['id'], $aFilterData ) )
    {
        $aFilters[$aPageFilter['id']] = $aFilterData[$aPageFilter['id']];
    }
}

// Get query string parameters into variables
// (can sanitise them further if required)
$sSecurityHash = $_GET['sh'];
$iLotteryID = $_GET['l'];
$iDraw = $_GET['d'];
$iCheckerID = $_GET['c'];
?>

<form name="addResultCheck" action="actions/lottery_actions.php?tablename=<?=$oPage->sTablename?>&pp=<?=$oPage->sFilename?>&a=addResultCheck" method="post">
<input type="hidden" name="lotteryid" value="<?=$iLotteryID?>" />
<div id="results">
   
<?php
  
    // Get details of who was supposed to be doing this check
    $aCheckerInfo = LotteryResultsAdmin::getCheckerByHash($sSecurityHash);

    // Get further lottery information
    $aLotteryInformation = LotteryAdmin::getLotteryDetails($iLotteryID, false);
    
    //echo("Lottery details<br/>");
    //print_d($aLotteryInformation);
    
    // Get Live Draw information
    $aLiveDrawInformation = LotteryAdmin::getLiveDrawInformation($iLotteryID);
    
    //echo("Live draw info<br/>");
    //print_d($aLiveDrawInformation);
    
    // Get lottery date
    $aNextDrawDetails = LotteryResultsAdmin::getLotteryDrawDateFromDrawRef($iDraw, $iLotteryID);
    
    //echo("Next draw details<br/>");
    //print_d($aNextDrawDetails);
    
    // Check if the supplied security hash matches with the other supplied details
    if (!LotteryResultsAdmin::checkSecurityHash($sSecurityHash, $iDraw, $iLotteryID, $iCheckerID))
    {
        // TODO: Handle this better than a simple 'die' statement
        die("Sorry, there has been a problem with the link you clicked on");
    }
    
    // Check also if this has been previously cancelled
    // (using class constant)
    if ($aCheckerInfo['status'] == LotteryResultsAdmin::CHECKER_CANCELLED_STATUS)
    {
        // TODO: Handle this better
        die("Sorry, this checker link has been cancelled");
    }
    
    // Write audit log message
    $sAuditLogMsg = "{$aCheckerInfo['first_name']} clicked add results link for {$aLotteryInformation['comment']} draw " .
                    "on {$aNextDrawDetails['drawdate']}";

    AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_LINK_CLICKED', 'lottery_results_checking', $iCheckerID);

    // Form data from session
    $aFormData = $_SESSION[$oSecurityObject->getsAdminType() . "formdata"];
    
    //echo ("Here now");
    
    // Get winning prize combinations
    $aLotteryMatches = LotteryResultsAdmin::getWinningsCombinations($iLotteryID);
    
    //echo("Lottery matches<br/>");
    //print_d($aLotteryMatches);
    
    // Get draw information from the matching row in the chekcing table
    // This will also contain any pre-entered information if data has already been
    // entered but it is found to be mismatching.
    $aDrawInfo = LotteryResultsAdmin::getLotteryRowFromCheckingTable($sSecurityHash, $iLotteryID, $iCheckerID, $iDraw);
    
    //echo("Lottery row<br/>");
    //print_d($aDrawInfo);
    
    // Get any 'fixed' winning amounts 
    $aLotteryWinningAmounts = LotteryResultsAdmin::getWinningAmounts($iLotteryID);
    
    // Arrays for holding the previously retrieved data
    $aPrevNumbers = array();
    $aPrevWinnings = array();
    $iPrevNextJackpot = "";
    $sPrevRollover = "";
    
    // If there has been a previous attempt at entering data,
    // the session object will contain previous data
    if ($_SESSION[$sSecurityHash . "formdata"])
    {

        $aPrevNumbers     = explode("|",$_SESSION[$sSecurityHash . "formdata"]['number']);
        $aPrevWinnings    = unserialize($_SESSION[$sSecurityHash . "formdata"]['winnings']);
        $iPrevNextJackpot = $_SESSION[$sSecurityHash . "formdata"]['next_jackpot_amount'];
        $sPrevRollover    = $_SESSION[$sSecurityHash . "formdata"]['next_jackpot_rollover'];
    }
    else 
    {
        // If numbers have been previously entered and stored, explode and copy into array
        if ($aDrawInfo[0]['numbers'] != "")
        {        
            $aPrevNumbers = explode("|", $aDrawInfo[0]['numbers']);
        }

        // If prize winning information has been previously entered, unserialize and copy into array
        if ($aDrawInfo[0]['winnings'] != "")
        {
            $aPrevWinnings = unserialize($aDrawInfo[0]['winnings']);
        }

        if ($aDrawInfo[0]['next_jackpot_amount'] != "") 
        {
            $iPrevNextJackpot = $aDrawInfo[0]['next_jackpot_amount'];
        }

        if ($aDrawInfo[0]['next_jackpot_rollover'] != "") 
        {
            $sPrevRollover = $aDrawInfo[0]['next_jackpot_rollover']==1?"checked='checked'":"";
        }
    }
    
    // Create a 'normal' date string from the date of the draw.
    $sDateString = date("D d M Y H:i:s", strtotime($aDrawInfo[0]['fk_lottery_date']));
    
    // Create a draw reference from the YMD of the date of the draw.
    $draw = date("Ymd", strtotime($aDrawInfo[0]['fk_lottery_date']));
 ?>
    <h3>Lottery results checker entry screen for <?=$aCheckerInfo['first_name'];?></h3>
    <h3><?=$aLotteryInformation['comment'];?> lottery draw on <?=$sDateString;?> (draw ref: <?=$draw?>)</h3>
    <input type='hidden' name='drawdate' value='<?=$aDrawInfo[0]['fk_lottery_date']?>' />
    <input type='hidden' name='draw' value='<?=$draw?>' title='draw' autocomplete='off' />
    <h4>Please ensure you have ALL draw information to hand before entering.</h4>
    <h5>INFORMATION ON HOW AND WHERE TO VIEW THIS LOTTERY DRAW</h5>
    <p>
        &bull; Official Website: <b><?=nl2br($aLiveDrawInformation['info_official_website']);?></b><br/>
        &bull; Live draw time: <b><?=nl2br($aLiveDrawInformation['info_live_draw_times']);?></b><br/>
        &bull; View the draw online LIVE at: <b><?=nl2br($aLiveDrawInformation['info_live_draw_location']);?></b><br/>
        &bull; Prize breakdown information available from: <b><?=nl2br($aLiveDrawInformation['info_prize_breakdown_times']);?></b><br/>
        &bull; View prize breakdown information at: <b><?=nl2br($aLiveDrawInformation['info_prize_breakdown_location']);?></b><br/>
    </p>
    
<?php
    // Show errors
    if ($_SESSION['Errors'])
    {
?>
    
        <p style="font-weight: bold;">
            Please fix the following errors:<br/>
<?php
        foreach ($_SESSION['Errors'] as $sErrorText)
        {
?>
           <span style="color: red;">&bull; <?=$sErrorText;?></span><br/>
<?php } ?>
        </p>
<?php } ?>
    
    <hr/>
    <strong>Drawn Numbers:</strong>
 <?php
    // Generate boxes for entering draw numbers
    // and populate with existing information if supplied
    $x = 0;
    for ( $i = $aLotteryMatches['numbers']; $i > 0; $i-- )
    {
        // Assign number to temp variable if present, else blank
        $iPrevNum = !empty($aPrevNumbers)?sprintf("%1d",$aPrevNumbers[$x]):""; 
        if ($iPrevNum == 0)
        {
            $iPrevNum = '';
        }
        echo '<input type="number" name="number[]" value="' . $iPrevNum . '" title="number" maxlength="2"  size="10"  min="' . $aLotteryInformation['min_number'] . '" max="' . $aLotteryInformation['max_number'] . '" />';			
        $x++;
    }

    // Only display the bonus ball option if there is one
    // (i.e.if not Spiel77, Super6 or Glücksspirale)
    if ($aLotteryMatches['bonus'] > 0 )
    {
 ?>
    <br/><br/>
    <strong>Bonus Numbers:</strong>
 <?php
        // Generate boxes for entering bonus number(s)
        // and populate with existing information if supplied
        for ( $j = $aLotteryMatches['bonus']; $j > 0; $j-- )
        {
            // Bonus number is end of array of previous numbers
            $iBonus = array_pop($aPrevNumbers);
            
            if ($iBonus != '')
            {
                $iBonus = sprintf("%1d",$iBonus);
            }
            echo ' <input type="number" name="number[]" class="bonus" value="' . $iBonus . '" title="bonus number" maxlength="2" min="' . $aLotteryInformation['min_bonus'] . '" max="' . $aLotteryInformation['max_number'] . '" />';			
        }
    }
?>
    <br/>
    <br/>
    <strong>Winnings:</strong>
    <br/>
    - Please enter winners without any punctuation or formatting symbols (e.g. <b>245681</b>), 
    <br/>
    -- <b><span style="color:red;"> EXCEPT IN THE CASE OF GLÜCKSSPIRALE!</span></b> where you should use a decimal point to seperate the fractions 
    <br/><br/>
    - Please enter amount using a decimal point only to seperate the currency.<br/>-- Amounts need to be entered to TWO decimal places (e.g. <b>16594.00</b>).<br/><br/>
    <table cellspacing='0' cellpadding='0'>
        <tr>
            <th align='left'>Title</th>
            <th width='80px' align='left'>Winners</th>
            <th width='180px' align='left'>Amount</th>
        </tr>
 <?php
    $sCurrency = "";
    // Go through prize levels now
    foreach ( $aLotteryMatches['matches'] as $key=>$aLotteryMatch )
    {
        $sCurrency = $aLotteryMatches['currency']['shortname'];
        // If there is a previously entered value for the prize, assign this to the variable,
        // otherwise show the 'fixed' winning amount for this lottery
        $iPrize = (($aPrevWinnings[$key]['prize'])?$aPrevWinnings[$key]['prize']:($aLotteryWinningAmounts[$key]?$aLotteryWinningAmounts[$key]:''));
?>
        <tr>
            <td style="padding-right: 5px;">
                Match <?=$aLotteryMatch[0];?>
<?php
        if ( $aLotteryMatch[1] )
        {
            // Show if bonus ball(s) required for this prize level
?>
                &nbsp;plus&nbsp;<?=$aLotteryMatch[1]?>&nbsp;<?=(($aLotteryMatch[1] > 1)?$aLotteryMatches['bonus_plural']:$aLotteryMatches['bonus_text']);?>
<?php
        }
?>
            </td>
            <td>
                <input type="input" name="winners[]" value="<?=$aPrevWinnings[$key]['winners'];?>" title="Winners"  />
            </td>
            <td>
                <?=$aLotteryMatches['currency']['shortname']?>
                <input type="hidden" name="bonus[]" value="<?=$aLotteryMatch[1]?>" />
                <input type="hidden" name="match[]" value="<?=$aLotteryMatch[0]?>" /> 
                <input type="input" name="winnings[]" value="<?=$iPrize?>" title="Winnings" pattern="\d+(\.\d{2})?" />
            </td>
        </tr>
<?php
    }
?>
    </table>
    <br/><br/>
    <strong>Next Jackpot information</strong>
    <br/>
    Please enter next jackpot information below, without punctuation or formatting symbols (e.g. 2000000)
    <br/>
    Jackpot amount: <?=$sCurrency?> <input type="number" name="next_jackpot_amount" value="<?=$iPrevNextJackpot?>" style="width: 155px;"/><br/>
    Rollover? <input type="checkbox" name="next_jackpot_rollover" value="1" <?=$sPrevRollover?> />
</div>
<br/>
<input type="hidden" name="security_hash" value="<?=$sSecurityHash?>" />
<input type="hidden" name="checkerid" value="<?=$iCheckerID?>" />
<input type="submit" value="Add Result" />
</form>
<?php
include ("includes/bottom.php");