<?php
/**
 * This script manually creates a lottery syndicate
 */
include ("includes/top.php");
include ("includes/errors.php");

// Only want to show a subset of possible lotteries at this stage
$aLotteriesToShow = array(1,2,11);

// Display all available lotteries
$aLotteries = LotteryAdmin::getLotteries(true);

// Create new syndicate object
//$oSyndicate = new Syndicate();
?>
<ul class="subnav">
    <li>
        <a href="syndicates.php" class="largebutton">Back to Syndicate List</a>
    </li>
</ul>
<h2>Create Syndicate</h2>
<?php 
if ($_POST['select_lotteries']) 
{
?>
<form method="post" action="syndicate-create.php">
Please enter requirements for each lottery in the syndicate
    <table>
        <tr>
            <th>Lottery Name</th>
            <th>Number of lines<br/>this lottery to have in syndicate</th>
            <th>Amount of random numbers to use<br/>for generating lines</th>
        </tr>
<?php
    foreach ($_POST['l'] as $iLotteryId)
    {
        $aLottery = LotteryAdmin::getLotteryDetails($iLotteryId);
?>
        <tr>
            <td><?=$aLottery['comment'];?><input type="hidden" name="l[]" value="<?=$iLotteryId;?>"/></td>
            <td style="text-align:center;"><input type="text" name="nl[<?=$iLotteryId;?>]" size="6" /></td>
            <td style="text-align:center;"><input type="text" name="n[<?=$iLotteryId;?>]" size="6" /></td>
        </tr>
<?php
    }
?>
    </table>
    <br/>    

    <br/>
    Enter number of shares required to close syndicate
    <input type="text" name="ms" size="5" maxlength="5" />
    <br/>
    Enter price per share
    &euro;<input type="text" name="pps" size="5" maxlength="5" />
    <br/><br/>
    <input type="submit" name="syndicate_create" class="largebutton" value="Proceed" />
    </form>
<?php
}
else if ($_POST['confirm_create'])
{
?>
<div id="results">
<?php
    
    $bSyndicateCreated = false;
    
    $aSyndicateDetails = unserialize($_POST['syndicate_details']);
    
    
    foreach ($aSyndicateDetails as $iLotteryId => $aLotteryData)
    {
    // Random numbers from previous stage
        $aNumbers = explode("-", $aLotteryData['numbers']);
    
        $aData['iLotteryId'] = $iLotteryId;
        $aData['sOriginalNumbers'] = $aLotteryData['numbers'];
        $aData['sBonusNumbers'] = $aLotteryData['bonus_numbers'];
        $aData['iNumLines'] = (int) $aLotteryData['num_lines'];
        $aData['iMaxShares'] = (int) $_POST['max_shares'];
        $aData['fPricePerShare'] =  number_format($_POST['price_per_share'], 2, ".", 0);
        $aData['iNumRandomNumbers'] = (int) $aLotteryData['num_random_numbers'];
        $aData['iNumInEachCombination'] = (int) $aLotteryData['num_in_each_combination'];
        $aData['iOriginalNumLinesGenerated'] = (int) $aLotteryData['num_generated'];
    
        print_d($aData);
        print_d($aNumbers);

        // Because syndicate code is a "trait" which is used in a lottery game component,
        // we generate a new lottery game class here
        // and then both syndicate and lottery functionality will be available
        $sClassName = Lottery::getLotteryClassName($aData['iLotteryId']);
        if ( $sClassName )
        {
            $oSyndicate = new $sClassName(1);
            
            // Create entry in syndicate table at this stage, if not already created
            if ($bSyndicateCreated === false)
            {
                $iSyndicateId = $oSyndicate->createSyndicate($aData);
                echo("Syndicate id {$iSyndicateId} created<br/>");
                $bSyndicateCreated = true;

            }
            
            // Add entry to syndicate_lotteries table
            $oSyndicate->addLotteryToSyndicate($aData, $iSyndicateId);
            

            // Get combinations of numbers according to specifications
            $aCombinations = $oSyndicate->getCombinations($aNumbers, $aData['iNumInEachCombination']);

            // Array to hold "formatted" lines
            // which will be passed to line storing function
            $iTotalLines = 0;
            $aAllLines = array();

            foreach ($aCombinations as $aCombination)
            {
                // Using a 1-indexed array for storing, so increase count here
                $iTotalLines++;

                // Numbers are in array format here, so implode onto string
                $sCombination = implode("|", $aCombination);

                // Add to holding array
                $aAllLines[$iTotalLines] = $sCombination;

            }

            echo("{$iTotalLines} lines created<br/>");
            print_d($aAllLines);

            // All possible combinations have been created at this stage.

            // If the requested number of lines is LESS than the number of generated lines,
            // we need to remove lines from the master generated lines array until we have the right number   
            if ($aData['iNumLines'] < $iTotalLines)
            {
                // Num of lines to remove
                $iLinesToRemove = $iTotalLines - $aData['iNumLines'];

                echo("Need to remove {$iLinesToRemove} lines - these numbers will be removed<br/>");

                // Because the master array is 1-indexes, we can call the random number generator a second time
                // to generate a random selection of n numbers from x numbers
                // where n = number of lines to remove and x = number of lines in master array.
                // Once we have the numbers (returned as an array), we can flip the array
                // so that the values returned become the keys
                $aRemoveNumbers = array_flip($oSyndicate->getRandomNumbersFromJSON($iLinesToRemove,$iTotalLines));

                print_d($aRemoveNumbers);

                // Now, use array_diff to compute the 'difference' between the two arrays
                // (e.g. all entries in $aAllLines which are not present in $aRemoveNumbers)
                // Using the keys of the array as the inputs
                $aSyndicateLineKeys = array_diff ( array_keys($aAllLines) , array_keys($aRemoveNumbers) );

                $aLinesToStore = array();

                // We now have the required keys of the lines to select from the master array
                foreach ($aSyndicateLineKeys as $iSyndicateLineKey)
                {
                    $aLinesToStore[] = $aAllLines[$iSyndicateLineKey];
                    echo ("Line $iSyndicateLineKey chosen: $aAllLines[$iSyndicateLineKey] <br/>");
                }

                // Insert syndicate lines
                $oSyndicate->insertLines($iSyndicateId, $iLotteryId, $aLinesToStore, $aData['sBonusNumbers']);

                echo (count($aLinesToStore) . " lines stored");
            }
            else
            {
                // If the requested number of lines is greater than, or equal to, the number
                // of generated combinations, then just store them.
                $oSyndicate->insertLines($iSyndicateId, $iLotteryId, $aAllLines, $aData['sBonusNumbers']);

                echo (count($aAllLines) . " lines stored");
            }
        }
    }
?>
</div>
<?php
}
else if ($_POST['syndicate_create'])
{

    $iMaxShares = (int) $_POST['ms'];
    $fPricePerShare = number_format($_POST['pps'], 2, '.', '');
    
    // Possible that more than one lottery has been specified
    $aLotteries = $_POST['l'];
    $aNumLines = $_POST['nl'];
    $aRandomNumToUse = $_POST['n'];

    // Because syndicate code is a "trait" which is used in a lottery game component,
    // we generate a new lottery game class here
    // and then both syndicate and lottery functionality will be available

    $aSyndicateToCreate = array();
    
    foreach ($aLotteries as $iLotteryId)
    {
        $sClassName = Lottery::getLotteryClassName($iLotteryId);
        if ( $sClassName )
        {
            
            $oLottery = new $sClassName(1);


            // These variables come from the instantiated lottery class
            $iNumInEachCombination = (int) $oLottery->getNumbersCount();
            $iMaxNumbers = (int) $oLottery->getMaxNumber();
            
            $iNumBonusNumbers = $oLottery->getBonusNumbers();
            
            $iNumRandomNumbers = (int) $aRandomNumToUse[$iLotteryId];
             
            $iNumLines = $aNumLines[$iLotteryId];
            
            $aNumbers = $oLottery->getRandomNumbersFromJSON($iNumRandomNumbers,$iMaxNumbers);

            $sNumbers = implode("-", $aNumbers);

            $iNumLinesGenerated = $oLottery->nCr($iNumRandomNumbers,$iNumInEachCombination);
            
            // Post onto array
            $aSyndicateToCreate[$iLotteryId] = array('num_generated' => $iNumLinesGenerated,
                                                     'numbers' => $sNumbers,
                                                     'num_lines' => $iNumLines,
                                                     'num_in_each_combination' => $iNumInEachCombination,
                                                     'num_random_numbers' => $iNumRandomNumbers,
                                                     'max_numbers' => $iMaxNumbers,
                                                     'lottery_name' => $oLottery->getComment());
            
            if ($iNumBonusNumbers > 0)
            {
                $iMinBonusNum = $oLottery->getMinBonusNumber();
                $iMaxBonusNum = $oLottery->getMaxBonusNumber();
                $aBonusNumbers = $oLottery->getRandomNumbersFromJSON($iNumBonusNumbers,$iMaxBonusNum,$iMinBonusNum);
                $sBonusNumbers = implode("-", $aBonusNumbers);
                $aSyndicateToCreate[$iLotteryId]['bonus_numbers'] = $sBonusNumbers;
            }
            else
            {
               $aSyndicateToCreate[$iLotteryId]['bonus_numbers'] = ""; 
            }
        }        
        
    }
    
    
?>
    Here are your final syndicate details
    <table>
        <tr>
            <th>Lottery<br/>Name</th>
            <th>Max<br/>numbers</th>
            <th>Amount of<br/>random numbers</th>
            <th>Random number<br/>selection generated</th>
            <th>Bonus numbers (if required)</th>
            <th>Number of<br/>lines required</th>
            <th>Num lines which<br/>will be generated</th>
            <th>Num in<br/>each combination</th>
        </tr>

<?php
    foreach ($aSyndicateToCreate as $iLotteryId => $aSyndicateInfo) 
    {
?>        
        <tr>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['lottery_name'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['max_numbers'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['num_random_numbers'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['numbers'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['bonus_numbers'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['num_lines'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['num_generated'];?>
            </td>
            <td style="text-align:center;">
                <?=$aSyndicateInfo['num_in_each_combination'];?>
            </td>
        </tr>
<?php
    }
    
    $oSerializedSyndicate = serialize($aSyndicateToCreate);
?>

    </table>
    <br/>
    You requested a limit of <?=$iMaxShares;?> shares in the syndicate before closing and to set a price of &euro;<?=$fPricePerShare;?> per share.
    
    <form method="POST" action="syndicate-create.php" name="confirmsyndicatecreate">
        <input type="hidden" name="max_shares" value="<?=$iMaxShares;?>"/>
        <input type="hidden" name="price_per_share" value="<?=$fPricePerShare;?>"/>
        <input type="hidden" name="syndicate_details" value='<?=$oSerializedSyndicate;?>' />
        <input type="submit" name="confirm_create" value="Confirm requirements and create syndicate and lines"/>
    </form>
    <br/>
    <ul class="subnav">
        <li>
            <a href="syndicate-create.php" class="largebutton">Start Over</a>
        </li>
</ul>
<?php

} else {
?>
<div id="reports">
    <form name="syndicatecreate1" action="syndicate-create.php" method="post">
    Select your required lottery:<br/>
<?php 
    foreach ($aLotteries as $aLottery ) { 
        if (in_array($aLottery["lottery_id"], $aLotteriesToShow))
        {
            //$sSelected = ($aLottery["lottery_id"]==$iLotteryId?" selected='selected'":"");
?>
            <input type ="checkbox"  name="l[]" value="<?=$aLottery["lottery_id"]?>" /><?=$aLottery["comment"];?>
            <br/>
<?php } 
    }
?>
         <input type="submit" name="select_lotteries" value="Select these lotteries"/>
    </form>
</div>    
    
<?php
}
include ("includes/bottom.php");
