<?php 
include("includes/top.php");
$sTablename = $_GET["tablename"];
$bExecute = $_GET['execute'];
if ( !$bStopOutput ) {
?>
	<h1><?=$oPage->sTitle;?></h1>
    <?php 
	}
	if ($sTablename && $sTablename != "null" && $bExecute == 1)
	{
		
		$myRef = new ReflectionMethod('ScriptsAdmin', $sTablename);
		$aParameters = $myRef->getParameters();
		$aParams = array();
		foreach ( $aParameters as $aParameter )
		{
			$aParams[] = $_POST[$aParameter->name];
		}
		$callback = 'ScriptsAdmin::' . $sTablename;
		if ( !$bStopOutput ) {
	?>
            <ul class="subnav">
                <li><a href="scripts.php" class="largebutton">Back to Scripts List</a></li>
                <li><a href="scripts.php?tablename=<?=$sTablename?>" class="largebutton" style="width:300px;">Back to <?=$oPage->sTitle;?> Details</a></li>
            </ul>
            <h2>Function Response</h2>
            <pre>
            <?php
                print_r(call_user_func_array($callback,	$aParams) );
            ?>
            </pre>
            <?php
		}
		else
		{
			echo "\r\n";
			print_r(call_user_func_array($callback,	$aParams) );
			echo "\r\n";
			echo "\r\n";
		}
	}
	elseif($sTablename && $sTablename != "null"){ 
		$bValidScriptName = true;
		$myRef = new ReflectionMethod('ScriptsAdmin', $sTablename); 
		$aParameters = $myRef->getParameters();
	?>
        <p />
        <ul class="subnav">
            <li><a href="scripts.php" class="largebutton">Back to Scripts List</a></li>
        </ul>
        <form method="post" action="?tablename=<?=$sTablename?>&execute=1">
            <strong>Script Name</strong>: <?=$sTablename?><br/>
        <?php
            if ( count($aParameters) )
            {
            ?>
                <strong>Parameters</strong>:
                <ul>
            <?php
                foreach ( $aParameters as $aParameter )
                {
                    ?>
                        <li style="padding-bottom:10px; list-style-type:none;"><div style='float:left; width:70px;'><?=$aParameter->name?></div><input type='text' name='<?=$aParameter->name?>' style='width:200px;'/></li>
                    <?php
                }
            ?>
                </ul>
            <?php
            }
        ?>
            <input type='submit' value='Execute'/>
        </form>
    <?php
	} 
	else
	{
		$aTables = $oSecurityObject->getNavigationPages($oPage->iID);
	?>
    <div>
        <ol>
        <?php
        foreach ($aTables as $aTable )
        {
        ?>
            <li>
                <a href="<?=$aTable["filename"];?><?php if($aTable["tablename"]){ ?>?tablename=<?=$aTable["tablename"]; }?>"><?=$aTable["title"];?></a>
            </li>
        <?php    	
        }
        ?>
        </ol>
    </div>
    <?php
	 }
	 
include("includes/bottom.php");

?>