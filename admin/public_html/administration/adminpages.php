<?php include("includes/top.php"); 

	$sFileName = basename($_SERVER["PHP_SELF"]);

	if($_GET["clear"] == "languagefilter"){
		unset($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"]);	
	}
	$iLangId = $_SESSION[$oSecurityObject->getsAdminType() ."iLangID"];
	if(!$iLangId){
		$iLangId = 0;
	}
	else{
		$currentLanguage = Language::getLanguage($iLangId);
	}

	$sTablename = $_GET["tablename"];
	$bValidTable = GenericTableAdmin::checkTableExists($sTablename);
	if($bValidTable && $iLangId){
		$sLanguageTablename = GenericTableAdmin::getLanguageTable($sTablename);
		if(GenericTableAdmin::checkTableExists($sLanguageTablename)){
			$aLanguageData = GenericTableAdmin::getLanguageRecords($sTablename, $sLanguageTablename, $iLangId, $newFilters, "", $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);	
		}
		else{
			$bValidTable = false;
			echo "<span class='error'>Languages not applicable. <a href=\"adminpages.php?tablename={$sTablename}&clear=languagefilter\">Click here</a> to clear the language filter.</span>";
		}
		
	}
	if($bValidTable){
?>
<script type="text/javascript" language="javascript">
	var btnToSwitch = null;
	function toggleBooleanField(obj, sIdField, iId, sField, sValue, sFilename){
		var ajaxUrl = "actions/generic_ajax.php?a=toggleBooleanField";
		var ajaxData = "<?php if($iLangId){ echo "&lid=" . $iLangId . "&languagetable=" . $sLanguageTable; } ?>&tablename=<?=$sTablename?>&idfield=" + sIdField + "&id=" + iId + "&field=" + sField + "&value=" + sValue + "&pp=" + sFilename;
		btnToSwitch = obj;
		startAchtungAjax();
		doAchtungAjax(ajaxUrl, ajaxData, "toggleButton");	
	}
	
	
	function toggleButton(){
		var sCurrentAction = btnToSwitch.attr("action");
		if(sCurrentAction == 'enable'){ 
			btnToSwitch.attr("action", 'disable');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disable'){ 
			btnToSwitch.attr("action", 'enable');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
	}
	
	var typingTimer;
	
	function searchRecord(sQuery, sField){
			clearTimeout(typingTimer);
			if(sQuery != ""){
				$("#clearbtn").css("visibility", "visible");
			}
			else{
				$("#clearbtn").css("visibility", "hidden");
				$("#searchText").val("");
			}
			$("#dialog_content").html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
			$("#dialog_content").load('actions/generic_ajax.php?a=genericTableSearch&tablename=<?=$sTablename?>&languagetable=<?=$sLanguageTablename?>&pp=adminpages.php&lid=<?=$iLangId?>&ep=adminpages-details.php&filterstring=<?=urlencode($sFilters);?>&searchfield=' + sField + '&query=' + urlencode(sQuery), onDocumentReady);
	}
	
	function typingSearch(sQuery, sField){
		clearTimeout(typingTimer);
		typingTimer = setTimeout("searchRecord('" + sQuery + "', '" + sField + "')", 550);		   		return false;
	}
	
</script>
	<h1><?=$oPage->sTitle?><?php if ( $oPage->bLanguageFilter == 1 && $iLangId) { echo " - " . $currentLanguage; } ?></h1>
    <?php include("includes/errors.php"); ?>
    <?php if($bFiltersCheck){ ?>
    <ul class="subnav">
    	<li><a href="adminpages.php?tablename=<?=$sTablename?>" class="largebutton selected">Listing<?php if($currentLanguage){ ?> (<?=$currentLanguage;?>)<?php } ?></a></li><li><a href="generic-details.php?tablename=<?=$sTablename?>&pp=adminpages.php" class="largebutton">Insert New Record</a></li>
    </ul>
    <div style="float: right; position: absolute; top: 40px; right: 20px;"
    	<a href="#" rel="dialog_content" class="simpledialog">Fullscreen View</a>
    </div>
		<?php if ($oErrors->getErrorCount() > 0) { ?>
        <table width="100%" class="detailsform"  border="0" cellspacing="0" cellpadding="5">
        <tr>
        <td bgcolor="#FF0000" class="white"><?=$oErrors->getErrorString();?></td>
        </tr>
        </table><?php 
        $oErrors->clearErrors();
        }?>
    <div class="searchBox">
    <table width="100%">
        <tr>
   			<td align="center">
                Filter by 
                <?php 
				$aColumns = GenericTableAdmin::getTableColumns($sTablename); ?>
                <select id="searchField">
                <?php foreach($aColumns as $currentColumn){ 
				if(!strstr($currentColumn["Type"], "tinyint")){
				?>
                <option value="<?=$currentColumn["Field"];?>"><?=$currentColumn["Field"];?></option>
                <?php
				}
				 } ?>
                </select> 
                <input id="searchText" type="text" onKeyUp="typingSearch($(this).val(), $('#searchField').val());" /><a class="clearbtn" <?php if($sReferenceNumber == ""){ ?>style="visibility: hidden;"<?php }?> onclick="return typingSearch('', $('#searchField').val());" href="#" id="clearbtn">x</a>
        </td>
        </tr>
        </table>
    </div>
     <div id="dialog_content" style="overflow: auto; position: relative;">
    <?php 
	if(!$iLangId){
		echo GenericTableAdmin::createGenericDataTable($sTablename, $iLangId, $newFilters, $sFileName, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"], "adminpages-details.php"); 
	}
	else{
		$aLanguageData["pp"] = $sFileName;
		echo GenericTableAdmin::createDataTable(
		array("ID|tiny|centeralign", "Comment", "Translated Title", "Edit|tiny|centeralign", "Delete|tiny|centeralign"), 
		array("id|centeralign", "comment", "title|empty:<span style=\"color: #f00\">No translation</span>", "id|link:generic-details.php?pp=" . $sFileName . "&a=edit&lid=" . $iLangId . "&tablename=" . $sTablename . "&languagetable=" . $sLanguageTablename . "|centeralign",
			  "translationid|link:actions/generic_actions.php?pp=" . $sFileName . "&a=delete&lid=" . $iLangId . "&tablename=" . $sTablename . "&languagetable=" . $sLanguageTablename . "|centeralign"), 
		$aLanguageData, $newFilters); 
	}
	?>
    </div>
<? 
	}
	else{ ?>
    <ul class="subnav">
    	<li><a href="generic-details.php?pp=<?=$sFileName?>&tablename=<?=$sTablename?>" class="largebutton">Insert New Record</a></li>
    </ul>
	<?php }
}
include("includes/bottom.php"); ?>