<?php
include("includes/top.php");
$sTablename = ($_GET['pt'])?$_GET['pt']:$_GET["tablename"];
$iLotteryID = (int)$_GET["tablename"];
$aLottery = LotteryAdmin::getLotteryDetails($iLotteryID);
$dDrawDate = $_GET["dd"];
?>
<script type="text/javascript" language="javascript">
	var btnToSwitch = null;
	function toggleBooleanField(obj, sIdField, iId, sField, sValue, sFilename){
		var ajaxUrl = "actions/generic_ajax.php?a=toggleBooleanField";
		var ajaxData = "&tablename=<?=$sTablename?>&idfield=" + sIdField + "&id=" + iId + "&field=" + sField + "&value=" + sValue + "&pp=" + sFilename;
		btnToSwitch = obj;
		startAchtungAjax();
		doAchtungAjax(ajaxUrl, ajaxData, "toggleButton");
	}
	function toggleButton(){
		var sCurrentAction = btnToSwitch.attr("action");
		if(sCurrentAction == 'enable'){
			btnToSwitch.attr("action", 'disable');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disable'){
			btnToSwitch.attr("action", 'enable');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
	}
</script>
	<h1><?=$oPage->sTitle;?></h1>
    <?php
		if ( $iLotteryID && $aLottery && $dDrawDate )
		{
	?>
    <ul class="subnav">
        <li><a href="<?=$oPage->sFilename?>?tablename=<?=$sTablename?>" class="largebutton"><?=$aLottery['comment']?></a></li><li><a href="<?=$oPage->sFilename?>?pt=<?=$sTablename?>&tablename=<?=$iLotteryID?>" class="largebutton">Change Draw Date</a></li><li><a href="<?=$oPage->sFilename?>?pt=<?=$sTablename?>&tablename=<?=$iLotteryID?>&dd=<?=$dDrawDate?>" class="largebutton selected"><?=date("l d F Y", strtotime($dDrawDate))?> Draw</a></li>
    </ul>
    <div id="dialog_content" style="overflow: auto; position: relative;">
    <?php
    	$sFilters = GenericTableAdmin::createFilterStringForSql($newFilters, $sTablename);
		if($aQuickFilters){
			$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct." . $aQuickFilters['field'] . " LIKE '%" . $aQuickFilters['query'] . "%'");
		}
		$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "DATE_FORMAT(ct.fk_lotterydate,'%Y-%m-%d') LIKE '" . Date::dateToMySQL($dDrawDate) . "'");
		$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct.fk_lottery_id = " . $iLotteryID);
		$aTitles = array("ID|tiny|centeralign", "Lottery Name", "Draw Date", "Numbers", "Bonus",  "Purchased|tiny|centeralign");
		$aFields = array("bookingitem_id|centeralign", "comment", "lotterydate", "numbers", "bonus", "bookingitem_id|ajaxbool:purchased|centeralign");
		$sSQL = "SELECT
					ct.bookingitem_id as bookingitem_id,
					DATE_FORMAT(ct.fk_lotterydate, '%W %d %M %Y') as lotterydate,
					REPLACE(SUBSTRING_INDEX(ct.numbers,'|',lc.number_count),'|', ' - ') as numbers,
					REPLACE(SUBSTRING_INDEX(ct.numbers,'|',-lc.bonus_numbers),'|', ' - ') as bonus,
					ct.purchased as purchased,
					lc.comment as comment
				FROM booking_items ct
				INNER JOIN lotteries_cmn lc ON lc.lottery_id = ct.fk_lottery_id
				WHERE
				{$sFilters}
				ORDER BY ct.purchased ASC, ct.fk_lotterydate ASC, lc.comment ASC, ct.bookingitem_id ASC";
		if ( $iLimit )
		{
			$sSQL .= " LIMIT {$iLimit}";
		}
		$aData = DAL::executeQuery($sSQL);
		if ( $aData )
		{
			$aData["pp"] = $oPage->sFilename;
			echo "<h5>Lottery Tickets for " . $aLottery['comment'] . " drawn on " . date("l d F Y", strtotime($dDrawDate)) . "</h5>";
			echo GenericTableAdmin::createDataTable( $aTitles, $aFields, $aData, $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]);
		}
		else
		{
			echo "<h4>No Tickets Found for Selected Lottery and Date</h4>";
		}
	?>
     </div>
        <?php
	}
	elseif ( $iLotteryID && $aLottery )
	{
		$aDrawDates = LotteryAdmin::getLotteryDrawDates($iLotteryID, true);
		?>
		<ul class="subnav">
			<li><a href="<?=$oPage->sFilename?>?tablename=<?=$sTablename?>" class="largebutton">Change Lottery<br/><?=$aLottery['comment']?></a></li>
		</ul>
		<h3>Step 2: Select The Draw Date</h3>
        <div id="dialog_content" style="overflow: auto; position: relative;">
        <div style="padding:20px;">
        <form action="" method="get">
        <input type="hidden" name="pt" value="<?=$sTablename?>" />
		<input type="hidden" name="tablename" value="<?=$iLotteryID?>" />
        <strong>Draw Date: </strong><select name="dd" style="width:200px;">
		<?php
			$bSelected = false;
			foreach ($aDrawDates as $aDrawDate )
			{
                echo "<option value='" . date("Y-m-d", strtotime($aDrawDate['drawdate'])) . "'" . (( $aDrawDate['drawdate'] > date("Y-m-d H:i:s") && $bSelected == false )?" selected style='color:#F00;'":"") . ">" . date("l d F Y", strtotime( $aDrawDate['drawdate'])) . "</option>";
				if ( $aDrawDate['drawdate'] > date("Y-m-d H:i:s") )
				{
					$bSelected = true;
				}
			}
		?>
        </select>
        	<input type="submit" value="View" style="width:100px;font-weight:bold;"/>
        </form>
        </div>
    	<?php
			$sFilters = GenericTableAdmin::createFilterStringForSql($newFilters, $sTablename);
			if($aQuickFilters){
				$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct." . $aQuickFilters['field'] . " LIKE '%" . $aQuickFilters['query'] . "%'");
			}
			$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct.purchased = 0");
			$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct.fk_lottery_id = " . $iLotteryID);
			$aTitles = array("ID|tiny|centeralign", "Lottery Name", "Draw Date", "Numbers", "Bonus",  "Purchased|tiny|centeralign");
			$aFields = array("bookingitem_id|centeralign", "comment", "lotterydate", "numbers", "bonus", "bookingitem_id|ajaxbool:purchased|centeralign");

			$sSQL = "SELECT
					ct.bookingitem_id as bookingitem_id,
					DATE_FORMAT(ct.fk_lotterydate, '%W %d %M %Y') as lotterydate,
					REPLACE(SUBSTRING_INDEX(ct.numbers,'|',lc.number_count),'|', ' - ') as numbers,
					REPLACE(SUBSTRING_INDEX(ct.numbers,'|',-lc.bonus_numbers),'|', ' - ') as bonus,
					ct.purchased as purchased,
					lc.comment as comment
					FROM booking_items ct
					INNER JOIN lotteries_cmn lc ON lc.lottery_id = ct.fk_lottery_id
					WHERE
					{$sFilters}
					ORDER BY ct.fk_lotterydate ASC, lc.comment ASC, ct.bookingitem_id ASC";
			if ( $iLimit )
			{
				$sSQL .= " LIMIT {$iLimit}";
			}
			$aData = DAL::executeQuery($sSQL);
			if ( $aData )
			{
				$aData["pp"] = $oPage->sFilename;
				echo "<h5>Unpurchased Lottery Tickets for " . $aLottery['comment'] . "</h5>";
				echo GenericTableAdmin::createDataTable( $aTitles, $aFields, $aData, $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]);
			}
			else
			{
				echo "<h4>No unpurchased tickets found for the selected lottery. Please select a date to view all.</h4>";
			}
			 ?>
		 </div>
        <?php
	 }
	 else
	 {
		$aPages = $oSecurityObject->getNavigationPages($oPage->iID);
		?>
		<div id="reports">
		<ul>
		<?php
		foreach ($aPages as $aPage )
		{
		?>
			<li>
                            <a href="<?=$aPage["filename"];?><?php if($aPage["tablename"]){ ?>?tablename=<?=$aPage["tablename"]; }?>&pt=<?=$sTablename?>"><img src='images/lotto-<?=$aPage["tablename"]?>.png' width="70px" />
				<br/>
				<?=$aPage["title"];?>
                            </a>
			</li>
		<?php
		}
		?>
		</ul>
		</div>
    <?php
	}
include("includes/bottom.php");
?>