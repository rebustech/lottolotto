var ll={}

ll.TabbedInterface={
    setup:function(){
        $('.TabbedInterface > ul > li').click(function(){
            var iSelectedTab=$(this).parent().find('li').index(this);
            window.location.hash = iSelectedTab;
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().parent().find('> .TabbedInterfaceItems > .TabbedInterfaceContent').eq(iSelectedTab).addClass('active').siblings().removeClass('active');
        });
        $('.TabbedInterface').each(function(){
            var iIndex=0;
            if(window.location.hash) {

                var iItemsInContainer=$(this).find('> ul > li').length;
                var hash = window.location.hash.substring(1);
                iIndex=hash>iItemsInContainer || hash < 0 || !isNumeric(hash) ? 0 : hash;
            }

            $(this).find('> ul > li').eq(iIndex).click();
        });
    }
}

ll.Popups={
    setup:function(){
        $('.v2popup').append('<button class="closeButton">X</button>');
        $('.v2popup').append('<div class="popupContent detailsform"></div>');
        $('.v2popup .closeButton').click(function(){
            $(this).parent().removeClass('visible');

            var eActiveComponent=$('.game_engine_holder li > div.active');
            if(eActiveComponent.length>0){
                var eActiveForm=eActiveComponent[0].eForm;
                $('.game_engine_data').append(eActiveForm);
            }


            return false;
        })

    }
}

$(document).ready(function(){
    ll.TabbedInterface.setup();
    ll.Popups.setup();
});
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Daft little bit of script by JP to put a loader on action buttons when clicked
 */
$(document).ready(function(){
    $('.actionbutton button').click(function(){
        $(this).find('.fa')[0].className='fa fa-circle-o-notch fa-spin';
        $(this).parents('form').css('opacity',0.5);
    })
})
