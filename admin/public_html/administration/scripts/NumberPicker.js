/**
 * NumberPicker
 *
 * Handle interactions with the boards, setting up the UI and handling the
 * selected numbers
 *
 * @version alpha 0.1
 * @package MaxLotto
 */
function NumberPickerObject() {
	'use strict';

	this.debug = false;
	this.selectorElement = '#';

	/**
	 * Environment details about the current game
	 * @var int
	 */
	this.numberCount = 0;
	this.extraCount = 0;
	this.mobileCutoff = 4;
	this.boardCount = 0;
	this.boardsPerRow = 6;
	this.numberCountClickable = 0;
	this.extraCountClickable = 0;

	/**
	 * Used to communicate with the Random.org API
	 * @var object
	 */
	this.rpcClient = [];

	/**
	 *  Store whether we've opened the modal or not
	 * @var boolean
	 */
	this.firstGameChangeClick = false;

	/**
	 * Check whether the board has been protected
	 * @var boolean
	 */
	this.isProtectedJackpot = false;

	/**
	 * Store the JSON of all boards
	 * @var array
	 */
	this.boards = [];

	/**
	 * Setup any extra numbers we can pick at the end
	 * @var array
	 */
	this.extraPicks = [];

	/**
	 * Date we want to run, default to both
	 * @var array
	 */
	this.playDate;
	this.playDays = 1;

	/**
	 * Any extra games we may want to play, click to activate
	 * @var array
	 */
	this.extraGames = {
		'spiel77': false,
		'super6': false,
		'glucks': false,
		'megaplier': false,
		'powerplay': false
	};

	/**
	 * Whether or not we forced the state, don't shuffle and select when
	 * coming back inbound
	 */
	this.forceGameState = {
		'spiel77': false,
		'super6': false,
		'glucks': false,
		'megaplier': false,
		'powerplay': false
	};

	this.forceSystemState = {};

	/**
	 * If we are on mobile, keep tabs of what we are on
	 * @var int
	 */
	this.activeBoardIndex = -1;

	/**
	 * What is the selected play dates.
	 * @var int
	 */
	this.playDuration = 1;

	/**
	 * Is the subscription option enabled.
	 * @var bool
	 */
	this.isSubscription = false;

	/**
	 * Are we editing a past item?
	*/
	this.editingBetslipItem = false;
	this.editingBetslipIndex = 0;

	/**
	 * Are we putting a quick deal in the basket?
	 * @var bool
	*/
	this.isQuickDeal = false;

	/**
	 * assignParentLinks: Link the GE parent to the GE itself
	*/
	this.assignParentLinks = function() {

		// Setup parent link
		this.GameEngine.parent = this;

	};

	this.setGameData = function(data, editGameIndex) {

		// Not on the same game
		if(data.gameID != window.gameID) {
			return;
		}

		// We are editing the betslip
		this.editingBetslipItem = true;
		this.editingBetslipIndex = editGameIndex;

		// Go over the boards
		for(var board = 0; board < data.boards.length; board++) {
			for(var index = 0; index < data.boards[board].numbers.length; index++) {
				var number = data.boards[board].numbers[index];
				this.boards[board].selectNumber(number);
				$('#board-numbers-' + board).find('strong[data-number="' + number + '"]').addClass('current');
			}
		}

		// Pick the subscription
		if(data.isSubscription) {
			$('#isSubscription').prop('checked', 'checked');
		} else {
			$('#isSubscription').prop('checked', false);
		}

		// Set the draw date
		$('#playDateHolder [data-date="' + data.day + '"]').trigger('click');
		$('.playAreaExtras select[name="duration"]').val(data.playDuration).trigger('change');

		// Set the extra pickers
		for(index = 0; index < data.extras.length; index++) {
			this.extraPicks[index].setCurrentValue(data.extras[index]);
			this.extraPicks[index].updateUIValue();
		}

		// Set the extra games
		for(index = 0; index < data.games.length; index++) {
			this.toggleExtraGames(data.games[index], true, true);
		}

		this.GameEngine.checkStatus();

	};

	/**
	 * setPlayDuration: Update the length of the play duration
	 *
	 * @access public
	 * @param $element - The select list
	 */
	this.setPlayDuration = function($element) {
		this.playDuration = $element.val();
	};

	/**
	 * getPlayDate: Determine the selected day (should be one!)
	 *
	 * @return string in NumberPicker#playDate
	 */
	this.getPlayDate = function() {
		return this.playDate;
	};

	/**
	 * getPlayDate: Determine the selected day (should be one!)
	 *
	 * @return string in NumberPicker#playDate
	 */
	this.getExtraGames = function() {

		var gameKeys = [];

		// Go over every game, get those active
		for(var key in this.extraGames) {
			if(this.extraGames[key]) {
				gameKeys.push(key);
			}
		}

		// Return the JSON object
		return gameKeys;

	};

	/**
	 * showGameTip: Trigger the tooltip for each of the games when they're turned on
	 *
	 */
	this.showGameTip = function($element) {

		var hasGameActive = false;

		// Go over each game
		for(var key in this.extraGames) {
			if(this.extraGames[key]) {
				hasGameActive = true;
				break;
			}
		};

		// Read the game
		var tip = $element.data('tip');
		var game = $element.data('game');

		// Toggle the game on
		this.toggleExtraGames(game);

		// At least one active, show the generic message
		if(hasGameActive) {
			$('.dipperNumbers .pickerText-superzahl').show();
		}

		// Double check - we may want to always show
		if(tip == 'spiel77' && (this.extraGames['glucks'] || this.extraGames['spiel77'])) {

			$('.dipperNumbers .pickerText-' + tip).show();

		} else {

			// Enabled or not?
			if(this.extraGames[game]) {
				$('.dipperNumbers .pickerText-' + tip).show();
			} else {
				$('.dipperNumbers .pickerText-' + tip).hide();
			}

		}

		// Reset var
		var activeGameCount = 0;

		// Check all games active
		for(var key in this.extraGames) {
			if(this.extraGames[key]) {
				if($('#' + key + 'ExtraGameModal').length > 0) {
					$('#' + key + 'ExtraGameModal').prop('checked', true).attr('checked', 'checked');
				}
				activeGameCount++;
			} else {
				if($('#' + key + 'ExtraGameModal').length > 0) {
					$('#' + key + 'ExtraGameModal').prop('checked', false).removeAttr('checked');
				}
			}
		};

		// At least one game wasn't checked (means we unchecked one)
		if(activeGameCount < 3) {
			if(!this.firstGameChangeClick) {
				this.firstGameChangeClick = true;
				$('#extraGames').foundation('reveal', 'open');
			}
		}

	};

	/**
	 * togglePlayDate: Change the day we want to play the numbers
	 * and update our JSON pattern.
	 *
	 * @param date string - Either Mi, Sa or Mi/Sa (Wed, Sat or Both)
	 * @returns void
	 */
	this.togglePlayDate = function($element) {

		// Set the new as true
		this.playDate = $element.data('date');

		// Remove the on-states
		var $buttons = $('#playDateHolder .button');

		// Count the number of days
		var days = $element.data('days');

		// Different to what we currently have?
		if(days != this.playDays) {

			var $selectMenu = $('.playAreaExtras').find('select[name="duration"]');
			var $options = $selectMenu.find('option');

			// Go over the list
			for(var i = 0; i < $options.length; i++) {

				var weeks = $options.eq(i).val();

				// Multiple weeks?
				if(weeks > 1) {
					var weekText = $selectMenu.data('weekplural');
				} else {
					var weekText = $selectMenu.data('week');
				}

				// How many draws this week
				var draws = weeks * days;

				// Multiple draws or single?
				if(draws > 1) {
					var drawText = $selectMenu.data('drawplural');
				} else {
					var drawText = $selectMenu.data('draw');
				}

				// Set the text
				$options.eq(i).text(weeks + ' ' + weekText + ' (' + draws + ' ' + drawText + ')');

			}

			// Update the counter
			this.playDays = days;

		}

		// Reset buttons
		$buttons.removeClass('current');

		// Find our one
		$element.addClass('current');

	};

	/**
	 * toggleSubscription: Gets the isSubscription status to match the tickbox
	 *
	 * @param date string - Either Mi, Sa or Mi/Sa (Wed, Sat or Both)
	 * @returns void
	 */
	this.toggleSubscription = function($element) {
		// Set the new as true
		this.isSubscription = $element.is(':checked');
	};

	/**
	 * toggleExtraGames: Activate or remove the current picker from being
	 * played in any of the additional games.
	 *
	 * Only accepts values from #extraGames
	 *
	 * @requires extraGames
	 * @param game string - Accepted values in #extraGames
	 * @returns void
	 */
	this.toggleExtraGames = function(game, forceState, desiredState) {

		if(typeof forceState === 'undefined') {
			forceState = false;
		}

		// Not a valid object
		if(!this.extraGames.hasOwnProperty(game)) {
			return;
		}

		// Just inverse the state
		if(forceState) {
			this.forceGameState[game] = true;
			if(desiredState) {
				this.extraGames[game] = true;
				$('.button[data-game="' + game + '"],.gameButton[data-game="' + game + '"]').addClass('current').removeClass('disabled');
			} else {
				this.extraGames[game] = false;
				$('.button[data-game="' + game + '"],.gameButton[data-game="' + game + '"]').addClass('disabled').removeClass('current');
			}
		} else {
			if(this.extraGames[game]) {
				this.extraGames[game] = false;
			} else {
				this.extraGames[game] = true;
			}

			// Find the game
			$('.button[data-game="' + game + '"],.gameButton[data-game="' + game + '"]').toggleClass('disabled current');
		}

	};

	/**
	 * toggleProtectJackpot: If we've already protected them, remove it
	 * adjust the price and then the UI of the boards
	 *
	 * @void
	 */
	this.toggleProtectJackpot = function() {

		$('#protectJackpot').toggleClass('current');

        // Do we want to protect them?
		if(this.isProtectedJackpot==true) {
			this.isProtectedJackpot = false;
			this.afterProtectJackpotOff();
		} else {
			this.isProtectedJackpot = true;
			this.afterProtectJackpotOn();
		}

		// Loop through every board, only apply to filled out
		for(var i = 0; i < this.boards.length; i++) {
            var board = this.boards[i];
            board.isProtectedJackpot = (($('#board-container-' + i).hasClass('protect')) && $('#board-container-' + i).hasClass('active'));
		}


		// Fire to GE
		this.GameEngine.checkStatus();
	};

	/**
	 * afterProtectJackpotOn: Fired when we enable the jackpot proteciton option
	 *
	 * @void
	*/
	this.afterProtectJackpotOn = function() {

		// Loop through every board, only apply to filled out
		for(var i = 0; i < this.boards.length; i++) {
			if($('#board-container-' + i).hasClass('active')) {
				$('#board-container-' + i).addClass('protect');
			}
		};

	};

	/**
	 * afterProtectJackpotOff: Fired when we disable the jackpot protection option
	 *
	 * @void
	*/
	this.afterProtectJackpotOff = function() {

		// Loop through every board, only apply to filled out
		$('.protect').removeClass('protect');

	};

	/**
	 * shuffleBoard: Create a random matrix of numbers on a board
	 *
	 * @param board int - The board (as an index, starts from 0)
	 */
	this.shuffleBoard = function(board) {

		var self = this;

		// Attempt to select the next board that's got spaces
		if(typeof board === 'undefined' || arguments.length == 0) {

			// Loop through every board until the first valid
			for(var i = 0; i < this.boards.length; i++) {

				// Check the queue
				var queue = "GameEngine-Board-" + i;

				if((this.boards[i].checkBoardFree()) && !$.ajaxq.isRunning(queue)) {

					// Toggle loading icon
					self.toggleLoadingButtons(i, 1);

					// Callback function
					this.boards[i].shuffleBoard(function(boardIndex) {

						// Update interface
						self.updateInlineSelection(boardIndex);

						// Fire to GE
						self.GameEngine.checkStatus();

						// Toggle loading icon
						self.toggleLoadingButtons(boardIndex, 0);

					});

					board = i;
					break;
				}

			}

		} else {

			// Toggle loading icon
			this.toggleLoadingButtons(board, 1);

			// Callback on shuffle
			this.boards[board].shuffleBoard(function(boardIndex) {

				// Update interface
				self.updateInlineSelection(boardIndex);

				// Fire to GE
				self.GameEngine.checkStatus();

				// Toggle loading icon
				self.toggleLoadingButtons(boardIndex, 0);

			});

		}

	};

	this.toggleLoadingButtons = function(board, state) {

		// By default, enable
		if(typeof state === 'undefined') {
			state = 1;
		}

		var $boardContainer = $('#board-container-' + board);
		var $boardNumbers = $('#board-numbers-' + board);

		// Get all the buttons
		var $loadingButtons = $.merge($boardContainer.find('.loadingButton'), $boardNumbers.find('.loadingButton'));

		// Enable it
		if(state) {

			// Show the button
			$boardContainer.find('header aside').css('opacity', 1);

			// Show loadnig icons
			$loadingButtons.find('i').hide();
			$loadingButtons.find('.loading').show();

			// Button colours
			$loadingButtons.css({
				color: '#EFEFEF',
				background: '#000041'
			});

		} else {

			// Reset boards
			$loadingButtons.find('i').show();
			$loadingButtons.find('.loading').hide();

			// Remove colours
			$loadingButtons.removeAttr('style');

			// Reset hidden
			$boardContainer.find('header aside').removeAttr('style');

		}

	}

	/**
	 * shuffleAllBoards: Loop over each of the boards, give them a shuffle
	 * and then fire off the events to select the numbers, delegate with the UI
	 * and store the new board data
	 *
	 * @uses Board:shuffleBoard()
	 * @uses Board:emptyNumbers()
	 */
	this.shuffleAllBoards = function() {

		var passedRequests = 0;
		var self = this;

		// Loop through every board until the first valid
		for(var i = 0; i < this.boards.length; i++) {

			// Toggle loading icon
			this.toggleLoadingButtons(i, 1);

			// Callback when complete
			this.boards[i].shuffleBoard(function(boardIndex) {

				// Update numbers
				self.updateInlineSelection(boardIndex);

				// Toggle loading icon
				self.toggleLoadingButtons(boardIndex, 0);

				// We are done
				passedRequests++;

				// Finally, fire away
				if(passedRequests == self.boards.length) {
					self.GameEngine.checkStatus();
				}

			});

		}

	};

	/**
	 * quickShuffle: Shuffle the next board that either has a free slot
	 * or hasn't been selected at all
	 *
	 * @uses :shuffleBoard()
	 */
	this.quickShuffle = function() {
		this.shuffleBoard();
	};

	/**
	 * clearBoard: Clear a given board
	 *
         * @param board int - Index to access the Board instance
	 */
	this.clearBoard = function(board) {

		var self = this;

		// Delegate task
		this.boards[board].clearBoard(function() {

			// Fire to GE
			self.GameEngine.checkStatus();

		});

	};

	/**
	 * clearAllBoards: Work through each board and de-select everything
	 * then reset its data to the default state
	 */
	this.clearAllBoards = function(check) {

		// @todo, replace this
		var check = typeof check === 'undefined' ? confirm($('#playGamesLanguage').data('clearboards')) : check ;

		// Loop through every board until the first valid
		if(check) {
			for(var i = 0; i < this.boards.length; i++) {
				this.boards[i].clearBoard();
			}
		}

		// Fire to GE
		this.GameEngine.checkStatus();

	};

	/**
	 * selectNumber: Given the number index and a board, toggle the data
	 * and store it as selected
	 */
	this.selectNumber = function(onBoard, number, isStarNumber) {

		// Default board, though we should specify it
		if(typeof onBoard === 'undefined') {
			onBoard = 0;
		}

		// Get the board
		var board = this.boards[onBoard];

		// Fire off the hook
		board.selectNumber(number, isStarNumber);

		// Read our JSON data
		var boardData = board.selectedAsJSON();

		// Fire to GE
		if(boardData.numbers.length == this.numberCountClickable) {
			this.GameEngine.checkStatus();
		}

	};

	/**
	 * incrementPicker: Take the index of an extra picker, then
	 * attempt to fire the increment method to the instance
	 *
	 * @uses Picker::increment
	 * @param index int - The index (starts from 0)
	 */
	this.incrementPicker = function(index) {

		// Validate the instance - debug
		if(this.debug) {
			console.log('Picker instance? ' + (this.extraPicks[index] instanceof Picker));
		}

		// Can the instance respond to the method?
		if(this.extraPicks[index] instanceof Picker) {
			if(this.debug) {
				console.log('Calling increment on: ' + index);
			}
			this.extraPicks[index].increment();
		}

		// Reload the pricing on syndicate shares
		if(window.gameType == 'syndicate') {
			this.GameEngine.checkStatus();
		}

	};

	/**
	 * decrementPicker: Take the index of an extra picker, then
	 * attempt to fire the decrement method to the instance
	 *
	 * @uses Picker::decrement
	 * @param index int - The index (starts from 0)
	 */
	this.decrementPicker = function(index) {

		// Validate the instance - debug
		if(this.debug) {
			console.log('Picker instance? ' + (this.extraPicks[index] instanceof Picker));
		}

		// Can the instance respond to the method?
		if(this.extraPicks[index] instanceof Picker) {
			if(this.debug) {
				console.log('Calling decrement on: ' + index);
			}
			this.extraPicks[index].decrement();
		}

		// Reload the pricing on syndicate shares
		if(window.gameType == 'syndicate') {
			this.GameEngine.checkStatus();
		}

	};

	/**
	 * shuffleAllPickers: Go through all the extra pickers and shuffle them all up
	 *
	 * @uses Picker::shufflePicker
	 * @void
	 */
	this.shuffleAllPickers = function(firstLoad) {

		if(typeof firstLoad === 'undefined') {
			firstLoad = false;
		}

		// Loop over the potential extras
		for(var i = 0; i < this.extraPicks.length; i++) {

			// Make sure they respond to the Picker methods
			if(this.extraPicks[i] instanceof Picker) {
				this.extraPicks[i].shufflePicker();
			}

		}

		// 6aus49 Classic or System
		if(firstLoad) {
			if(window.gameID == '3' || window.gameID == '8') {
				if(!this.forceGameState['super6']) {
					this.toggleExtraGames('super6');
				}
				if(!this.forceGameState['spiel77']) {
					this.toggleExtraGames('spiel77');
				}
			} else if(window.gameID == '6' || window.gameID == '14') {
				if(!this.forceGameState['powerplay']) {
					this.toggleExtraGames('powerplay');
				}
			} else if(window.gameID == '7' || window.gameID == '13') {
				if(!this.forceGameState['megapiler']) {
					this.toggleExtraGames('megapiler');
				}
			}
		}

		// Reload pricing
		this.GameEngine.checkStatus();

		// System only
		if(firstLoad) {
			if(window.gameType == 'system') {
				var index = 0;
				$('#gameContainer').find('li.board select[name="systemType"]').each(function() {
					if(!this.forceSystemState[index]) {
						this.SystemPlay.changeType($(this));
					}
					index++;
				});
			}
		}

	};

	this.overrideSystemType = function(boards, type) {

		// Get all of the boards
		var $boards = $('#gameContainer').find('li.board');

		for(var i = 0; i < boards; i++) {
			var $element = $boards.eq(i).find('select[name="systemType"]');
			$element.val(type).trigger('change');
			this.forceSystemState[i] = true;
		}

	};

	this.overrideSystemGame = function(boards, game) {

		// Get all of the boards
		var $boards = $('#gameContainer').find('li.board');

		for(var i = 0; i < boards; i++) {
			var $element = $boards.eq(i).find('select[name="gameSystem"]');
			$element.val(game).trigger('change');
			this.forceSystemState[i] = true;
		}

	};

	/**
	 * closeChooseNumbers: Close the modal overlay
	 *
	 * @access public
	 */
	this.closeChooseNumbers = function() {

		// Don't save, reset board
		this.clearBoard(this.activeBoardIndex);

		// Change index back
		this.activeBoardIndex = -1;

		// Whatever is open, try and shut it
		$.fancybox.close();

	};

	/**
	 * saveChooseNumbers: Send the save handler, let it do its magic
	 * then trigger the close delegate.
	 *
	 * @access public
	 */
	this.saveChooseNumbers = function() {

		// Update the active numbers
		this.updateInlineSelection(this.activeBoardIndex);

		// Whatever is open, try and shut it
		$.fancybox.close();

	};

	/**
	 * updateInlineSelection: Given a board, get the selected elements and then
	 * format out the numbers we've selected. Update the list.
	 *
	 * @param board int - Index to the board
	 */
	this.updateInlineSelection = function(board) {

		var selectedItemsHTML = '';

		// Get the selected numbers
		var selectedNumbers = this.boards[board].selectedAsJSON();

		// Debug
		console.log(selectedNumbers.numbers);

		// Loop through them
		for(var i = 0; i < selectedNumbers.numbers.length; i++) {

			selectedItemsHTML += '<li> \
				<a href="javascript:NumberPicker.changeNumbers(\'' + board + '\');" class="number"> \
					<span>' + selectedNumbers.numbers[i] + '</span> \
				</a> \
			</li>';

		}

		// Update contents
		$('#board-container-' + board + ' .activeNumbersOnBoard').html(selectedItemsHTML);

	};

	/**
	 * changeNumbers: On mobile displays, expand the current picker
	 *
	 * @param board int - Index for the board item in the array
	 */
	this.changeNumbers = function(board) {

		// Get the board
		var $currentBoard = $('#board-container-' + board + ' .boardNumbers');

		// Wrap a display element
		$currentBoard.wrap('<div style="display: none;"></div>').removeClass('hide-for-small').show();

		$.fancybox({
			width: 300,
			height: 'auto',
			autoSize: false,
			closeBtn: false,
			padding: 20,
			helpers: {
				overlay: {
					closeClick: false,
					locked: false
				}
			},
			href: '#board-container-' + board + ' .boardNumbers',
			beforeClose: function(e) {
				$currentBoard.addClass('hide-for-small');
			},
			afterClose: function() {
				$currentBoard.unwrap();
			}
		});

		this.activeBoardIndex = board;

	};

	/**
	 * removeLines: Remove a line / multiple depending on browser
	 *
	 */
	this.removeLines = function() {

		// Is Modernizr running?
		if(Modernizr) {

			// Must be a mobile size
			if(Modernizr.mq('only all and (max-width: 540px)')) {

				this.removeSingleLine(false);

			} else {

				// Quick message
				if(this.boards.length <= 6) {
					alert($('#playGamesLanguage').data('noremove'));
					return;
				}

				// Dry run responses
				var responses = [];

				// We have to put in a variable, when we splice, it'll decrement
				var boardLength = this.boards.length;

				// Add 6 to the desktop
				for(var i = boardLength - 1; i >= boardLength - 6; i--) {
					responses.push(this.removeSingleLine(true, i));
				}

				// One wasn't OK
				for(var i = 0; i < responses.length; i++) {
					if(!responses[i]) {
						return;
					}
				}

				// No dry-run, go again, remove them
				for(var i = boardLength - 1; i >= boardLength - 6; i--) {
					responses.push(this.removeSingleLine(false, i));
				}

				if(this.debug) {
					console.log(this.boards);
				}

			}

		} else {

			// Everything else fallsback
			this.removeSingleLine(false);

		}

		// Refresh event handlers
		this.setupGameEvents();

	};

	/**
	 * addSingleLine: Handles create all the elements we actually need
	 * and pulling everything together
	 *
	 * @access publi
	 */
	this.removeSingleLine = function(dryRun, index) {

		// Last board instance
		if(typeof index === undefined) {
			index = this.boards.length - 1;
		}

		// Get the single instance
		var board = this.boards[index];

		// Check they want to remove
		if(dryRun) {
			if(board.checkBoardActive()) {
				return confirm($('#playGamesLanguage').data('confirmdelete'));
			} else {
				return true;
			}
		} else {
			$('#board-container-' + board.boardIndex).remove();

			// Wipe the board
			// this.boards[index] = null;
			board = null;

			// Delete from array and window
			// delete this.boards[index];
			this.boards.remove(index);
		}

	};

	/**
	 * addMoreLines: Increase the board by 6 on desktop, 1 on mobile
	 *
	 */
	this.addMoreLines = function() {

		if(this.debug) {
			console.log(this.boards);
		}

		// Is Modernizr running?
		if(Modernizr) {

			// Must be a mobile size
			if(Modernizr.mq('only all and (max-width: 540px)')) {

				this.addSingleLine();

			} else {

				// Add 6 to the desktop
				for(var i = 0; i < 6; i++) {
					this.addSingleLine();
				}

			}

		} else {

			// Everything else fallsback
			this.addSingleLine();

		}

		// Refresh event handlers
		this.setupGameEvents();

	};

	/**
	 * addSingleLine: Handles create all the elements we actually need
	 * and pulling everything together
	 *
	 * @access publi
	 */
	this.addSingleLine = function() {

		var $lastActiveBoard = $('#gameContainer > li:last-of-type').clone();
		var oldBoardIndex = ($lastActiveBoard.attr('id')).replace('board-container-', '');
		var newBoardIndex = this.boards.length;

		// Debugging
		if(this.debug) {
			console.log(oldBoardIndex + ' => ' + newBoardIndex);
		}

		// Change the ID
		$lastActiveBoard.attr('id', 'board-container-' + newBoardIndex);

		// Replace all active
		$lastActiveBoard.find('strong.number').removeClass('current');

		// Find remaining ID
		$lastActiveBoard.find('#remaining-board-' + oldBoardIndex).each(function() {
			$(this).attr('id', 'remaining-board-' + newBoardIndex);
		});

		// Board numbers holder
		$lastActiveBoard.find('#board-numbers-' + oldBoardIndex).each(function() {
			$(this).attr('id', 'board-numbers-' + newBoardIndex);
		});

		// What to match
		var regexMatchLinks = new RegExp('\\(' +oldBoardIndex + '\\)', 'g');

		// Find all links
		$lastActiveBoard.find('a').each(function() {
			var currentValue = $(this).attr('href');
			currentValue = currentValue.replace(regexMatchLinks, '(' + newBoardIndex + ')');
			$(this).attr('href', currentValue);
		});

		// Replace all board indexes
		$lastActiveBoard.find('[data-board]').each(function() {
			$(this).data('board', newBoardIndex).attr('data-board', newBoardIndex);
		});

		// Titles
		$lastActiveBoard.find('.mobileTitle').each(function() {
			$(this).text(newBoardIndex + 1);
		});
		$lastActiveBoard.find('.bigNumber').each(function() {
			$(this).text(newBoardIndex + 1);
		});
		$lastActiveBoard.find('.desktopTitle').each(function() {
			var currentValue = $(this).html();
			currentValue = currentValue.replace('Line ' + newBoardIndex, 'Line ' + (newBoardIndex + 1));
			$(this).html(currentValue);
		});

		// Setup the new board
		this.createNewBoard(newBoardIndex);

		// Add into container
		 $('#gameContainer').append($lastActiveBoard);

	};

	/**
	 * createNewBoard: Setup a new Board instance using the
	 * environment settings
	 *
	 * @requires #numberCount, #extraCount
	 * @param newBoardIndex int - The new index to assign
	 */
	this.createNewBoard = function(newBoardIndex) {

		// Next index
		if(typeof newBoardIndex === 'undefined') {
			newBoardIndex = this.boards.length;
		}

		// Create a new instance
		this.boards[newBoardIndex] = new Board();

		/**
		 * @todo - Need to pull in the game logic at this point,
		 * numbers allowed, boards, bonus numbers
		 */

		// Setup each board
		this.boards[newBoardIndex].setup(this.numberCount, this.extraCount);

		// Assign index
		this.boards[newBoardIndex].boardIndex = newBoardIndex;

		// Minimum or maximum
		if(this.numberCountClickable > 0) {
			this.boards[newBoardIndex].setMinimumNumbers(this.numberCountClickable, true);
		}

		if(this.extraCountClickable > 0) {
			this.boards[newBoardIndex].setMinimumExtraNumbers(this.extraCountClickable, true);
		}

	};

	/**
	 * setClickableNumbers: Pass over the minimum
	 *
	 */
	this.setClickableNumbers = function(numbers, extraNumbers) {

		this.numberCountClickable = numbers;
		this.extraCountClickable = extraNumbers;

		// Loop over the boards
		for(var i = 0; i < this.boards.length; i++) {
			this.boards[i].setMinimumNumbers(numbers, true);
		}

		// Loop over the boards
		for(var i = 0; i < this.boards.length; i++) {
			this.boards[i].setMinimumExtraNumbers(extraNumbers, true);
		}

	};

	/**
	 * initialise: Fire up the UI and create all board instances
	 *
	 * @param boards int - Number of boards to manage
	 */
	this.setup = function(boardCount, numberCount, extraCount, mobileCutoff) {

		// Is Modernizr running?
		if(Modernizr) {

			// Must be a mobile size
			if(Modernizr.mq('only all and (max-width: 540px)')) {
				boardCount = mobileCutoff;
			}

		}

		// Store setup environment
		this.numberCount = numberCount;
		this.extraCount = extraCount;
		this.mobileCutoff = mobileCutoff;
		this.boardCount = boardCount;

		// Try the play date
		if($('#playDateHolder .button:last').length > 0) {
			this.playDate = $('#playDateHolder .button:last').addClass('current').data('date').toString();
			this.playDays = $('#playDateHolder .button:last').data('days');
		} else {
			if($('#drawDayOfWeek').length > 0) {
				this.playDate = $('#drawDayOfWeek').data('date').toString();
				this.playDays = $('#drawDayOfWeek').data('days');
			} else {
				this.playDate = 5;
				this.playDays = 1;
			}
		}

		// Create the board
		for(var i = 0; i < boardCount; i++) {

			// Create a new instance
			this.createNewBoard(i);

		}

		// Setup parent link
		this.GameEngine.parent = this;

		// Store self for events
		var self = this;

		// Handle clicking
		$(document).on('click', '.boardNumbers .number', function(event) {

			// Cache element
			var $element = $(this);

			// What type are we?
			var isStarNumber = false;
			if($element.is('[data-star]')) {
				var isStarNumber = true;
			}
			// Read the data
			var board = $element.data('board');

			// Read the number
			if(isStarNumber) {
				var number = $element.data('star');
			} else {
				var number = $element.data('number');
			}

			// Determine if we can select another
			if(isStarNumber) {
				var canSelectNumber = self.boards[board].checkBoardFreeStars();
			} else {
				var canSelectNumber = self.boards[board].checkBoardFree();
			}

			// Check if this is selected or not
			if(self.boards[board].isChecked(number, isStarNumber)) {

				// Select field
				$element.removeClass('current');

				// Debug
				if(self.debug) {
					if(isStarNumber) {
						console.log('De-selecting board: ' + board + ', star number: ' + number);
					} else {
						console.log('De-selecting board: ' + board + ', number: ' + number);
					}
				}

				// Toggle boolean state
				canSelectNumber = true;

			} else if(canSelectNumber) {

				// Select field
				$element.addClass('current');

				// Debug
				if(self.debug) {
					if(isStarNumber) {
						console.log('Selecting board: ' + board + ', star number: ' + number);
					} else {
						console.log('Selecting board: ' + board + ', number: ' + number);
					}
				}

			}

			// Check the number
			if(canSelectNumber) {

				self.selectNumber(board, number, isStarNumber);

			} else {

				/**
				 * @todo - Make a bit more visual, not atm
				 *
				 */
				alert($('#playGamesLanguage').data('fullboard'));

			}

		});

		// All JSON RPC calls
		this.rpcClient = new $.JsonRpcClient({
			ajaxUrl: 'https://api.random.org/json-rpc/1/invoke',
			headers: null
		});

		// Check megaplier by default
		if($('#megapiler').length > 0) {
			if($('#megapiler').is(':checked')) {
				this.extraGames['megaplier'] = true;
			}
		}

	};

	this.setupSyndicatePickers = function(count) {

		// Create the board
		var i = 0;

		// Create a new instance
		this.extraPicks[i] = new Picker();

		// Find the correct picker
		var $syndicatePicker = $(this.selectorElement + 'picker');

		// Data ranges
		var rangeFrom = $syndicatePicker.data('range-from');
		var rangeTo = $syndicatePicker.data('range-to');

		/**
		* @todo - Need to pull in the game logic at this point,
		* numbers allowed, boards, bonus numbers
		*/

		// Setup each board
		this.extraPicks[i].setup(rangeFrom, rangeTo);

		// Assign index
		this.extraPicks[i].pickerIndex = i;
		this.extraPicks[i].syndicateHolder = this.selectorElement;

		this.setupGameEvents();

	};

	/**
	 * setupExtras: Called to trigger when we need to be able to select
	 * additional bonus numbers at the end of the boards.
	 *
	 * @param count - The number to be able to select
	 * @parma type - Can be either pickers (up/down controls) or star picks
	 */
	this.setupExtras = function(count, rangeFrom, rangeTo, type) {

		// Fail-safe
		if(typeof type === 'undefined') {
			type = 'picker';
		}

		// Create the board
		for(var i = 0; i < count; i++) {

			// Create a new instance
			if(type === 'picker') {
				this.extraPicks[i] = new Picker();
			}

			/**
			 * @todo - Need to pull in the game logic at this point,
			 * numbers allowed, boards, bonus numbers
			 */

			// Setup each board
			this.extraPicks[i].setup(rangeFrom, rangeTo);

			// Assign index
			this.extraPicks[i].pickerIndex = i;

		}

		this.setupGameEvents();

	};

	/**
	 * setupGameEvents: Handle all DOM and document
	 * event firebacks
	 */
	this.setupGameEvents = function() {

		var self = this;

		// Catch when they click the number
		$(document).on('click', '.picker[data-picker] .number, .picker[data-picker] .shareNumber', function(e) {

			// Get the pickerIndex key
			var pickerIndex = $(this).parent('.picker').data('picker');

			// We are on syndicate
			if($(this).hasClass('shareNumber')) {

				// Trigger the event
				var className = $(this).data('object');

				// Get the object
				var classObject = eval(className);
				classObject.incrementPicker(pickerIndex);

			} else {

				// Increment the counter
				self.incrementPicker(pickerIndex);

				// Fire to GE
				self.GameEngine.checkStatus();

			}

		});

		// What are checked
		$('#playGamesHolder .button').each(function() {
			if($(this).hasClass('current')) {
				var game = $(this).data('game');
				self.extraGames[game] = true;
			}
		});

		$('#gameContainer li.board').hover(function() {
			$(this).stop(true, true).addClass('hover');
		}, function() {
			$(this).stop(true, true).removeClass('hover');
		});

	};

	/**
	 * setBoardsPerRow: Setup the environment for how many boards
	 * we should have on a single row.
	 *
	 * @param number int - The quantity
	 */
	this.setBoardsPerRow = function(number) {
		this.boardsPerRow = number;
	};

	/**
	 * SystemPlay: Handle all of the rules regarding the
	 * system play possibilities and combinations.
	 *
	 * @package NumberPicker
	 * @version 1.0.0
	 */
	this.SystemPlay = new function() {

		this.gameTypeRules = {
			"part": {
				"9/12": "12",
				"10/15": "15",
				"12/22": "22",
				"10/30": "30",
				"11/66": "66",
				"22/77": "77",
				"26/130": "130",
				"12/132": "132"
			},
			"full": {
				"7": "7",
				"8": "28",
				"9": "84",
				"10": "210",
				"11": "462",
				"12": "924",
				"13": "1716",
				"14": "3003",
				"15": "5005",
				"16": "8008",
				"17": "12376",
				"18": "18564",
				"19": "27132",
				"20": "38760"
			}
		};

		/**
		 * calculateLineCombinations: Determine the maximum possible
		 * situations for a given system.
		 *
		 * @return int
		 */
		this.calculateLineCombinations = function(system) {

			function factorial(x) {

				// Validating the input
				x = parseInt(x, 10);

				// Can't use this number
				if(isNaN(x)) {
					return 1;
				}

				// If x below 0, return 1
				if (x <= 0) {
					return 1;
				}

				// If x above 170, return infinity
				if (x > 170) {
					return Infinity;
				}

				// Calculating the factorial
				var y = 1;
				for (var i = x; i>0; i--){
					y *= i;
				}

				// Return the denominator
				return y;

			}

			function choose(n, k){

				// Validate n
				n = parseInt(n, 10);
				if(isNaN(n)) n = 0;
				if(n < 0) n = 0;

				// Validate k
				k = parseInt(k, 10);
				if(isNaN(k)) k = 0;
				if(k < 0) k = 0;
				if(k > n) k = n;

				// Grab the factorials
				return (factorial(n)) / (factorial(k) * factorial(n - k));

			}

			// We are calling x choose y
			var x = system;
			var y = x - 6;

			// Run the call
			var answer = choose(x, y);
			return answer;

		};

		/**
		 * changeBoardGame: If the user changes the select dropdown for 'Part' or 'Full',
		 * fire off to the active board the new rules and logic to follow
		 *
		 * @param board int - Index of the Board instance
		 * @param type string - Either part or full
		 * @param system string - The logic or rule to follow (from gameTypeRules above)
		 */
		this.changeBoardGame = function($element) {

			// Get the board index
			var board = $element.data('board');
			var basePrice = $element.data('base');
			var system = $element.val();

			// Read the board index
			var aBoard = this.boards[board];

			// Board is active, prompt to change
			if(aBoard.checkBoardActive()) {
				var check = confirm($('#playGamesLanguage').data('activeboard'));
				if(!check) {
					$element.val($element.data('current'));
           			return false;
				} else {
					aBoard.clearBoard();
				}
			}

			// Update data
			$element.data('current', $element.val());

			// Change of type, re-populate the options
			var type = aBoard.gameType;

			// Split up the type for the part
			if(type == 'part') {

				// Split up, first part = the system number, second part = how many lines to play
				var systemSplit = system.split('/');

				// String of what it means
				var stringText = '<strong>Part System ' + system + '</strong><br />' + systemSplit[1] + ' lines - &euro;' + number_format(systemSplit[1] * basePrice, 2, '.', ',') + ' / draw';

				// Actual string to display
				var stringLine = 'Part System ' + system + '<br /><span style="font-weight: 400;">Pick ' + systemSplit[0] + ' Numbers<br />Play ' + systemSplit[1] + ' lines<br />&euro;' + number_format(systemSplit[1] * basePrice, 2, '.', ',') + ' / draw</span>';

				// Update numbers allowed
				aBoard.setMinimumNumbers(systemSplit[0], true);

				// We can calculate the number of combinations
				var combinations = this.SystemPlay.calculateLineCombinations(systemSplit[0]);

			} else {

				// We can calculate the number of combinations
				var combinations = this.SystemPlay.calculateLineCombinations(system);

				// Again, what it means
				var stringText = '<strong>Full System ' + system + '</strong><br />' + this.gameTypeRules[type][system] + ' lines - &euro;' + number_format(this.gameTypeRules[type][system] * basePrice, 2, '.', ',') + ' / draw';

				// Actual string to display
				var stringLine = 'Full System ' + system + '<br /><span style="font-weight: 400;">Pick ' + system + ' Numbers<br />Play ' + this.gameTypeRules[type][system] + ' lines<br />&euro;' + number_format(this.gameTypeRules[type][system] * basePrice, 2, '.', ',') + ' / draw</span>';

				// Update numbers allowed
				aBoard.setMinimumNumbers(system, true);

			}

			// Update HTML
			$('#board-container-' + board).find('h5.systemLineCount').html(stringLine);
			$('#board-container-' + board).find('.mobileIfActive p.statusText').html(stringText);

			// Set the game type
			aBoard.setSystemType(type);
			aBoard.setSystemRule(system, stringText);

		};

		/**
		 * changeType: Fired from the bind event on the first select list.
		 *
		 * @uses $element
		 */
		this.changeType = function($element, reloadPricing) {

			if(typeof reloadPricing === 'undefined') {
				reloadPricing = true;
			}

			if(!$.isFunction($element.data)) {
				return;
			}

			// Get the board index
			var board = $element.data('board');
			var type = $element.val();

			// Changed value
			if(true || this.boards[board].gameType != type) {

				// Change the next element
				var selectHTML = '';

				// Cache the element
				var $optionList = $('#board-container-' + board + ' .hide-for-small').find('select[name="gameSystem"]');

				// Add each option
				for(var key in this.SystemPlay.gameTypeRules[type]) {

					selectHTML += '<option value="' + key + '">System ' + key + '</option>';

				}

				// Save the list
				$optionList.html(selectHTML);

			}

			// Get the Board instance
			this.boards[board].setSystemType(type);

			// Update HTML
			this.SystemPlay.changeBoardGame($optionList);

		};

	};

	/**
	 * Syndicate:
	 *
	 * @package Syndicate
	 * @version 1.0.0
	 */
	this.Syndicate = new function() {

		/**
		 * @var Store the syndicate ID we want to play
		 */
		this.syndicateId = [];

		/**
		 * toggleSyndicate: Change the selected syndicate ID
		 * and re-calcuate pricing
		 */
		this.toggleSyndicate = function($element) {

			var ID = $element.val();

			if(typeof this.syndicateId[ID] == 'undefined') {
				this.syndicateId[ID] = true;
			} else if(this.syndicateId[ID]) {
				this.syndicateId[ID] = false;
			} else {
				this.syndicateId[ID] = true;
			}

		};

	};

	/**
	 * QuickPlay: Handle the quick-play interactions to the GameEngine and API.
	 *
	 * @package QuickPlay
	 * @version 1.0.0
	 */
	this.QuickPlay = new function() {

		this.loadedReveal = false;

		/**
		 * Store the quick play countdown timer
		 * @object CountdownClockObject
		 */
		this.quickCountdown = null;

		/**
		 * Store the freshslider instance
		 *
		 */
		this.slider = null;

		this.placeQuickOrder = function($element) {

			// Link back to GE
			var self = this;

			// Once added, submit to checkout
			this.GameEngine.addToBasket(function(json) {

				// Update the cart / price
				self.GameEngine.createBasketContents(json.cart, false);

				// What to do?
				if($element.data('active')) {
					$('#quickPlayForm').submit();
				} else {
					$('#cartLoginLink').trigger('click');
				}
			});
		}

		/**
		 * openQuickModal: Pull in the game details, show the appropriate
		 * modal
		 */
		this.openQuickModal = function($element) {

			var self = this;

			// Only on first initialise
			if(!this.QuickPlay.loadedReveal) {

				// When closing Foundation
				$(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
					$('#quickPlayLoaded').hide();
					$('#quickPlayLoading').show();
					clearInterval(self.QuickPlay.quickCountdown.timer);
				});

				// Toggle boolean
				this.QuickPlay.loadedReveal = true;

				// Create the new countdown
				this.QuickPlay.quickCountdown = new CountdownClockObject();
				this.QuickPlay.quickCountdown.parentElement = '#countdownQuick ';

				// Setup the slider
				this.QuickPlay.slider = $('.unranged-value').freshslider({
					step: 1,
					value: 4,
					min: 1,
					max: 12,
					onchange: function(value) {
						self.GameEngine.checkStatus();
					}
				});
			}

			// Loading modal
			$('#quickPlay').foundation('reveal', 'open');

			// AJAX request
			this.GameEngine.getGameDetails($element.data('game'), function(json) {

				// Store the game type
				window.gameType = json.engine.gameType;
				window.gameID = json.engine.id;

				// Find the page modal
				var $quickPlayLoaded = $('#quickPlayLoaded');
				$quickPlayLoaded.attr('class', json.engine.className + ' generic');

				// Header, game jackpot / countdown
				var $gameHeader = $quickPlayLoaded.find('.gameHeader');
				$gameHeader.find('h1').text(json.draw.text);
				$gameHeader.find('h2').text(json.draw.jackpot);
				$gameHeader.find('#countdownQuick').html(json.html.countdown);

				// Draws / weeks
				$quickPlayLoaded.find('#quickPlayDuration').attr('data-status', '0').html(json.html.weeks).removeAttr('data-status');
				$quickPlayLoaded.find('#quickPlayDays').data('date', json.draw.days).trigger('click');

				// Hide loading, show main area
				$('#quickPlayLoading').hide();
				$quickPlayLoaded.slideDown(500);

				self.QuickPlay.quickCountdown.timer = setInterval(function() {
					self.QuickPlay.quickCountdown.tick();
				}, 1000);

				// Reset value
				self.QuickPlay.slider.setValue(4);

				// Trigger a change to update the pricing
				$quickPlayLoaded.find('#quickPlayDuration').trigger('change');

				// Rollover!
				if(json.draw.rollover == "1") {
					$('#rolloverQuick').fadeIn();
				} else {
					$('#rolloverQuick').hide();
				}

			});

		};

	};

	/**
	 * GameEngine: Responsible for connecting with the API
	 * and handling any queued requests or delegates.
	 *
	 * @package NumberPicker
	 * @version 1.0.0
	 */
	this.GameEngine = new function() {

		this.parent = null;
		this.basketMovement = 3;
		this.basketMovementHeight = 89;

		/**
		 * handleBasketMovement: Trigger the basket up or down
		 *
		*/
		this.handleBasketMovement = function($element, event) {

			// Stop button clicking
			event.preventDefault();

			// Global basket container
			var $basketItems = $('#basketItems');
			var totalItems = $basketItems.find('.orderLine').length;

			// Index to array numbering
			if(totalItems > 0) {
				totalItems--;
			}

			// What direction to move?
			var direction = $element.data('direction');
			var nextPosition = -1;
			var currentPosition = $basketItems.data('position');

			// Move it up?
			if(direction == 'up') {

				// Make sure we're not already at the top
				if(currentPosition != 0) {

					// Next is current - the distance to move
					nextPosition = currentPosition - this.GameEngine.basketMovement;

					// Exceeds past the top
					if(nextPosition < 0) {
						nextPosition = 0;
					}

				}

			} else {

				// Not already at the end?
				if(currentPosition != totalItems) {

					// Next is current + the distance to move
					nextPosition = currentPosition + this.GameEngine.basketMovement;

					// Exceeds the bottom?
					if(nextPosition > totalItems) {
						nextPosition = totalItems;
					}

				}

			}

			// Always want 3 in-view
			if((nextPosition + this.GameEngine.basketMovement) > totalItems) {
				nextPosition = (totalItems + 1) - this.GameEngine.basketMovement;
			}

			// Calculate the actual height of the elements
			var movementHeightNeeded = 0;

			$basketItems.find('.orderLine:lt(' + nextPosition + ')').each(function() {
			   movementHeightNeeded += $(this).outerHeight();
			});

			// Work out where to move
			if(nextPosition != -1) {

				$basketItems.find('.basketItemsPusher')
					.first()
					.animate({
						'top': '-' + movementHeightNeeded + 'px'
					}, 500);

				$basketItems.data('position', nextPosition);

			}

		};

		this.addQuickDeal = function(gameID) {

			// We need to store this one
			var currentGameID = window.gameID;
			var currentGameType = window.gameType;

			// Setup details to pass in
			window.gameID = gameID;
			window.gameType = 'quickDeal';
			this.isQuickDeal = true;

			// Store GE
			var self = this;

			// Add into basket
			this.addToBasket(function(json) {

				// Get the HTML
				self.createBasketContents(json.cart);

				// Show the login modal
				if($('#topScrollFixed').data('user') == 'false' || !$('#topScrollFixed').data('user')) {
					triggerLoginModal();
				}

			});

			// Show the cart
			if(!$('#basket').hasClass('open')) {
				$('#topScrollFixed .fixedTopNav [data-dropdown="basket"]').trigger('click');
				$('body').addClass('activeBasket');
			}

			// Finally, update back to normal
			window.gameID = currentGameID;
			window.gameType = currentGameType;
			this.isQuickDeal = false;

		}

		this.getGameState = function() {

			// No game set
			if(typeof window.gameID === 'undefined') {
				window.gameID = 0;
			}

			// No game type
			if(typeof window.gameType === 'undefined') {
				window.gameType = 'classic';
			}

			// Create data array
			var checkData = {
				"gameID": window.gameID,
				"gameType": window.gameType,
				"boards": [],
				"extras": [],
				"syndicates": [],
				"day": this.parent.getPlayDate(),
				"games": this.parent.getExtraGames(),
				"playDuration": this.parent.playDuration,
				"isSubscription": this.parent.isSubscription,
				"protectJackpot": this.parent.isProtectedJackpot,
				"quickPlay": this.parent.QuickPlay.loadedReveal,
				"quickDeal": this.parent.isQuickDeal,
				"editingBetslipItem": this.parent.editingBetslipItem,
				"editingBetslipIndex": this.parent.editingBetslipIndex
			};

			// Syndicate games
			if(window.gameType == 'syndicate') {

				var picker = 0;

				// Go over each syndicate ticked
				for(var ID in this.parent.Syndicate.syndicateId) {
					if(typeof this.parent.Syndicate.syndicateId[ID] === 'function') {
						continue;
					}
					if(this.parent.Syndicate.syndicateId[ID]) {
						checkData['syndicates'].push({
							"syndicateId": ID,
							"shares": this.parent.extraPicks[picker].getCurrentValue(),
							"playDuration": this.parent.playDuration
						});
					}
				}

				/*
				// Store the syndicate data
				checkData['syndicates'] = [{
					"syndicateId": this.parent.syndicateId,
					"shares": this.parent.extraPicks[picker].getCurrentValue(),
					"playDuration": this.parent.playDuration
				}];
				*/

			// Quick play, pass in null stub
			} else if(checkData['quickPlay']) {

				checkData['boards'] = this.parent.QuickPlay.slider.getValue();

			} else {

				// Only add the active boards, get their current options
				for(var board = 0; board < this.parent.boards.length; board++) {
					if(this.parent.boards[board].checkBoardActive()) {
						checkData['boards'][board] = this.parent.boards[board].selectedAsJSON();
						checkData['boards'][board]['type'] = this.parent.boards[board].gameTypeRule;
						checkData['boards'][board]['isProtectedJackpot'] = this.parent.boards[board].isProtectedJackpot;
						checkData['boards'][board]['minimumNumbers'] = this.parent.boards[board].minimumNumbers;
						checkData['boards'][board]['minimumStarNumbers'] = this.parent.boards[board].minimumStarNumbers;
					}
				};

				// Only add the add active pickers
				for(var picker = 0; picker < this.parent.extraPicks.length; picker++) {
					checkData['extras'][picker] = this.parent.extraPicks[picker].getCurrentValue();
				}
			}

			// Reset boolean
			this.parent.isQuickDeal = false;

			console.log(checkData);


			// Return JSON object
			return checkData;

		};

		this.createBasketContents = function(cart, showBasket) {

			// Update the header
			var $basketItems = $('#basketItems');

			// Basket HTML contents
			// var basketHTML = '<div class="row">' + $basketItems.find('.row').first().html() + '</div><hr />';
			var basketHTML = '<div class="basketItemsPusher">';

			if(cart.items > 3) {
				$('#basket').find('.basketPagination').removeClass('hidden');
			} else {
				$('#basket').find('.basketPagination').addClass('hidden');
			}

			// Loop over each
			for(var i = 0; i < cart.items; i++) {

				if((typeof cart[i].data.cartLinesText !== 'undefined') && (cart[i].data.gameType != 'quickDeal')) {
					var text = cart[i].data.cartLinesText + ' - ';
				} else if((typeof cart[i].data.gameType !== 'undefined') && (cart[i].data.gameType != 'quickDeal')) {
					var text = capitaliseFirstLetter(cart[i].data.gameType) + ' play ' + ((typeof cart[i].data.boards == 'null' || cart[i].data.boards == null) ? 1 : cart[i].data.boards.length) + ' line' + (typeof cart[i].data.boards != 'null' && cart[i].data.boards != null && cart[i].data.boards.length != 1 ? 's' : '') + ' - ';
				} else {
					var text = '';
				}

				// <a href="#">Edit</a> &nbsp;
				basketHTML += '<div class="row orderLine border" id="cartItem-' + i + '"> \
					<div class="small-2 columns"> \
						<img src="/static/img/game-icons-ticket/' + (cart[i].data.cssLogoSmall).toLowerCase() + '.png" alt="' + cart[i].data.gameTitle + '" /> \
					</div> \
					<div class="small-7 columns"> \
						<h2>' + cart[i].data.gameTitle + '</h2> \
						<p>' + text + cart[i].data.daysOfDraw + ' ' + $basketItems.data('draws') + ' \
						</p> \
						<p>' + $basketItems.data('firstdraw') + ' ' + cart[i].data.drawDate + '</p> \
						<p> \
							<a class="removeCartLink" href="javascript:NumberPicker.GameEngine.removeFromBasket(' + i + ');">' + $basketItems.data('remove') + '</a> \
							' + (cart[i].data.gameType != 'quickDeal' ? '<a class="removeCartLink" href="/?editBoard=' + i + '&gameId=' + cart[i].data.gameID + '">' + $basketItems.data('edit') + '</a>': '') + ' \
						</p> \
					</div> \
					<div class="small-3 columns"> \
						<h2>' + cart[i].data.priceFormatted + '</h2> \
					</div> \
				</div>
				<hr />';

			}

			// Close .basketItemsPusher wrapper
			basketHTML += '</div>';

			// Update HTML
			$basketItems.html(basketHTML);

			// Update tally
			$('#cartTotal').html(cart.items + ' ' + (cart.items == 1 ? $('#cartTotal').data('single') : $('#cartTotal').data('plural')));
			$('#cartDropTotal').html('&euro;' + cart['totalFormatted']);

			// Show the dropdown
			if(typeof showBasket === 'undefined' || (typeof showBasket !== 'undefined' && showBasket)) {
				if(!$('#basket').hasClass('open') || (parseInt(($('#basket').css('left')).replace('px', ''), 10) < 0)) {
					$('#topScrollFixed .fixedTopNav [data-dropdown="basket"]').trigger('click');
					$('body').addClass('activeBasket');
				}
			}

		};

		/**
		 * checkStatus: Send a generic 'ping' to the game engine, check everything
		 * with the current session is okay. Keep tabs on the user events and.
		 * current products added.
		 *
		 * @void
		 */
		this.checkStatus = function() {

			// Create what to send
			var gameState = this.getGameState();
			var selectorElement = this.parent.selectorElement;

			// No game, skip
			if(gameState.gameState == 0) {
				return;
			}

            $(selectorElement + 'subTotalPricing, ' + selectorElement + 'price1, ' + selectorElement + 'price2, ' + selectorElement + 'td_line1, ' + selectorElement + 'td_line2, ' + selectorElement + 'td_line3, ' + selectorElement + 'pr_line1, ' + selectorElement + 'pr_line2').fadeOut(10).addClass('checking');
            $(selectorElement + 'td_line1, ' + selectorElement + 'subTotalPricing').html('<i class="fa fa-refresh fa-spin"></i>');
            $(selectorElement + 'price1, ' + selectorElement + 'price2, ' + selectorElement + 'td_line2, ' + selectorElement + 'td_line3, ' + selectorElement + 'pr_line1, ' + selectorElement + 'pr_line2, ' + selectorElement + 'subTotalTitle').html('&nbsp;');

			// Send the data
			$.ajaxq("GameEngine", {
				clearQueue: true,
				url: apiURL + 'engine/status',
				type: 'POST',
				data: gameState,
				dataType: 'json',
				complete: function(xhr) {

					var json = xhr.responseJSON;

					// Bad request?
					if(json.status == 400) {
						return;
					}

					// Grand total!
					$(selectorElement + 'subTotalPricing').html(json.engine.priceFormatted).hide().fadeIn(1400);

					// Need a full breakdown?
					if($(selectorElement + 'price1').length > 0) {
						$(selectorElement + 'price1').html(json.engine.pricinginfo.price1Formatted).hide().fadeIn(1000);
						$(selectorElement + 'price2').html(json.engine.pricinginfo.price2Formatted).hide().fadeIn(600);
						$(selectorElement + 'td_line1').html(json.engine.pricinginfo.td_line1).hide().fadeIn(200);
						$(selectorElement + 'td_line2').html(json.engine.pricinginfo.td_line2).hide().fadeIn(400);
						$(selectorElement + 'td_line3').html(json.engine.pricinginfo.td_line3).hide().fadeIn(600);
						$(selectorElement + 'pr_line1').html(json.engine.pricinginfo.pr_line1).hide().fadeIn(800);
						$(selectorElement + 'pr_line2').html(json.engine.pricinginfo.pr_line2).hide().fadeIn(1000);
						$(selectorElement + 'subTotalTitle').html(json.engine.pricinginfo.total_line2).hide().fadeIn(1000);
					}

				},
				error: function(error) {

					// Log to Google Analytics
					if(typeof ga !== 'undefined') {
						ga('send', 'event', 'errorCheckStatus', 'error', error);
					}

				}
			});

		};

		/**
		 * getGameDetails: Given the ID, get the game engine details (formatted)
		 *
		 * @array
		 */
		this.getGameDetails = function(ID, callback) {

			// Store in GE queue
			$.ajaxq("GameEngine", {
				url: apiURL + 'engine/details',
				type: 'GET',
				data: {
					'game': ID
				},
				dataType: 'json',
				priority: 1,
				complete: function(xhr) {
					var json = xhr.responseJSON;
					if(typeof callback !== 'undefined') {
						callback(json);
					}
				}
			});

		};

		/**
		 * addToBasket: Add the current state to the basket.
		 *
		 * @void
		 */
		this.addToBasket = function(callback, initiatingButton) {

			// Create what to send
			var gameState = this.getGameState();

			// Validate the basket board
			if(window.gameType != 'quickDeal') {

				for(var boardIndex in gameState.boards) {

					if(typeof gameState.boards[boardIndex] !== 'function') {

						if(gameState.boards[boardIndex].numbers.length != gameState.boards[boardIndex].minimumNumbers) {
							// You have not picked enough numbers on board 1
							alert($('#playGamesLanguage').data('missingnumbers') + ' ' + (parseInt(boardIndex) + 1));
							return;
						}

						if(gameState.boards[boardIndex].extras.length != gameState.boards[boardIndex].minimumStarNumbers) {
							// You have not picked enough bonus numbers on board 1
							alert($('#playGamesLanguage').data('missingextranumbers') + ' ' + (parseInt(boardIndex) + 1));
							return;
						}

					}
				}

			}

			// Internal action
			gameState['action'] = 'addToCart';

			// Store GE
			var self = this;

			// Send the data
			$.ajaxq("GameEngine", {
				url: apiURL + 'engine/basket',
				type: 'POST',
				data: gameState,
				dataType: 'json',
				priority: 1,
				complete: function(xhr) {

					var json = xhr.responseJSON;

					// Dump the contents for now
					if(self.debug) {
						if((typeof console !== 'undefined') && (typeof console.table === 'function')) {
							console.log(json);
						}
					}

					self.editingBetslipItem = false;

					// Custom callback or default?
					if(typeof callback !== 'undefined' && callback) {
						callback(json);
					} else {
						// Get the HTML
						self.GameEngine.createBasketContents(json.cart);
					}

					if(typeof initiatingButton !== 'undefined') {
	                    initiatingButton.removeAttr('disabled');

						// Clear the boards
	                    self.clearAllBoards(true);
					}

                    // Show the cart
					if(!$('#basket').hasClass('open')) {
						$('#topScrollFixed .fixedTopNav [data-dropdown="basket"]').trigger('click');
						$('body').addClass('activeBasket');
					}
				},
				error: function(error) {

					// Log to Google Analytics
					if(typeof ga !== 'undefined') {
						ga('send', 'event', 'errorAddToBasket', 'error', error);
					}

				}
			});

		};

		/**
		 * removeFromBasket: Remove the ticket index {x} from the cart array
		 *
		 * @void
		 */
		this.removeFromBasket = function(index) {

			// Loading icon
			$('#cartItem-' + index).find('.removeCartLink').html('<i class="fa fa-spinner fa-lg fa-spin"></i>');

			// On bets page
			if($('#cartItemSitu-' + index).length > 0) {
				$('#cartItemSitu-' + index).find('.removeCartLink').html('<i class="fa fa-spinner fa-lg fa-spin"></i>');
			}

			// Create what to send
			var gameState = this.getGameState();

			// Internal action
			gameState['action'] = 'remove';
			gameState['item'] = index;

			// Store GE
			var self = this;

			// Send the data
			$.ajaxq("GameEngine", {
				url: apiURL + 'engine/basket',
				type: 'POST',
				data: gameState,
				dataType: 'json',
				priority: 1,
				complete: function(xhr) {

					var json = xhr.responseJSON;

					// Slide and remove
					$('#cartItem-' + index).slideUp(function() {
						$(this).next('hr').remove();
						$(this).remove();
					});

					// On bets page
					if($('#cartItemSitu-' + index).length > 0) {
						$('#cartItemSitu-' + index).slideUp(function() {
							$(this).next('hr').remove();
							$(this).remove();
						})
					}

					// Update tally
					$('#cartTotal').html(json.cart['items'] + ' ' + (json.cart['items'] != 1 ? $('#cartTotal').data('plural') : $('#cartTotal').data('single')));
					$('#cartDropTotal, #cartSituTotal').html('&euro;' + json.cart['totalFormatted']);

					// Get the HTML
					self.GameEngine.createBasketContents(json.cart);

				},
				error: function(error) {

					// Log to Google Analytics
					if(typeof ga !== 'undefined') {
						ga('send', 'event', 'errorRemoveFromBasket', 'error', error);
					}

				}
			});

		};

		/**
		 * calculatePrice: Send a price calculation request to the GE.
		 * Take the current game session and all options, pass down the line.
		 *
		 * @void
		 */
		this.calculatePrice = function() {

			// Create what to send
			var gameState = this.getGameState();

			// Store GE
			var self = this;

			// Send the data
			$.ajaxq("GameEngine", {
				url: apiURL + 'engine/price',
				type: 'POST',
				data: gameState,
				dataType: 'json',
				priority: 1,
				complete: function(xhr) {

					var json = xhr.responseJSON;

					// Dump the contents for now
					if(self.debug) {
						if((typeof console !== 'undefined') && (typeof console.table === 'function')) {
							console.log(json);
						}
					}

				},
				error: function(error) {

					// Log to Google Analytics
					if(typeof ga !== 'undefined') {
						ga('send', 'event', 'errorCalculatePrice', 'error', error);
					}

				}
			});

		};

	};

};

var NumberPicker = new NumberPickerObject();
NumberPicker.assignParentLinks();
