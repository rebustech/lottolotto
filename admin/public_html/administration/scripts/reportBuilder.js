var reportBuilder={
    setup:function(){
        $('div[data-for-datatype]').hide();
        $('select').change(reportBuilder.showFilterOptions);
    },
    showFilterOptions:function(){
        var sDataType=$(this).find(':selected').attr('data-frontype');
        if(sDataType=='') return;
        $(this).parents('tr').find('div[data-for-datatype='+sDataType+']').show().siblings().hide();
    }

}

reportBuilder.setup();