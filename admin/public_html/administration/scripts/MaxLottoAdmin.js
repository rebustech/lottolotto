;var MLCheckout = new function() {
    'use strict';

    this.checkPayment = function() {

        $.getJSON('/administration/API/PaymentAjax/status', function(data){

            if(data.status == null || data.aSteps == null) {
                $('.payment-progress-popup').foundation('reveal', 'close');
                return;
            }

            /**
             * Update the messages in the popup
             */
            $('.payment-progress-popup .processSteps li:eq(0) span').text(data.aSteps[0]);
            $('.payment-progress-popup .processSteps li:eq(1) span').text(data.aSteps[1]);
            $('.payment-progress-popup .processSteps li:eq(2) span').text(data.aSteps[2]);
            $('.payment-progress-popup .processSteps li:eq(3) span').text(data.aSteps[3]);


            if(data.status=='MyAccount'){
                //document.location='/myaccount';
            }

            if(data.status=='Processing' || data.status=='MyAccount'){
                //Show buying popup
                $('.payment-progress-popup').foundation('reveal', 'open');
                if(data.iStep==0) $('.payment-progress-popup .processSteps li:eq(0)').addClass('animation');
                if(data.iStep==1) $('.payment-progress-popup .processSteps li:eq(1)').addClass('animation');
                if(data.iStep==2) $('.payment-progress-popup .processSteps li:eq(2)').addClass('animation');
                if(data.iStep==3) $('.payment-progress-popup .processSteps li:eq(3)').addClass('animation');
            }
        });


        // Commented out for the moment
        //setTimeout(MLCheckout.checkPayment, 5000);
        
    }

    this.initialise = function() {

        // Fire off the checks
        MLCheckout.checkPayment();

        // Handle input changes
        $('form input, form select').change(function(){
            $('iframe').attr('src','');
            if(parseFloat(($('input[name=amount]').val()).replace(',', '')) > 0){
                $('#paymentform').css('display','block');
                $('#formSubmit').click();
            }
        })

        $('input[name=amount]').keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $('#formSubmit').click();
            }
        });

        $('#deposit_buttons button').click(function(){
            $('input[name=amount]').val($(this).val()).change();
            return false;
        });

        $('#formSubmit').click();
        $('#paymentform').css('display','block');
    }
}();

/*
A simple jQuery function that can add listeners on attribute change.
http://meetselva.github.io/attrchange/

About License:
Copyright (C) 2013 Selvakumar Arumugam
You may use attrchange plugin under the terms of the MIT Licese.
https://github.com/meetselva/attrchange/blob/master/MIT-License.txt
*/
(function($) {
   function isDOMAttrModifiedSupported() {
		var p = document.createElement('p');
		var flag = false;

		if (p.addEventListener) p.addEventListener('DOMAttrModified', function() {
			flag = true
		}, false);
		else if (p.attachEvent) p.attachEvent('onDOMAttrModified', function() {
			flag = true
		});
		else return false;
		p.setAttribute('id', 'target');
		return flag;
   }

   function checkAttributes(chkAttr, e) {
		if (chkAttr) {
			var attributes = this.data('attr-old-value');

			if (e.attributeName.indexOf('style') >= 0) {
				if (!attributes['style']) attributes['style'] = {}; //initialize
				var keys = e.attributeName.split('.');
				e.attributeName = keys[0];
				e.oldValue = attributes['style'][keys[1]]; //old value
				e.newValue = keys[1] + ':' + this.prop("style")[$.camelCase(keys[1])]; //new value
				attributes['style'][keys[1]] = e.newValue;
			} else {
				e.oldValue = attributes[e.attributeName];
				e.newValue = this.attr(e.attributeName);
				attributes[e.attributeName] = e.newValue;
			}

			this.data('attr-old-value', attributes); //update the old value object
		}
   }

   //initialize Mutation Observer
   var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

   $.fn.attrchange = function(a, b) {
	   if (typeof a == 'object') {//core
		   return attrchangeFx._core.call(this, a);
	   } else if (typeof a == 'string') { //extensions/options
		   return attrchangeFx._ext.call(this, a, b);
	   }
   }

   var attrchangeFx = {
	   _core: function (o) {
		   var cfg = {
				trackValues: false,
				callback: $.noop
			};

			//backward compatibility
			if (typeof o === "function" ) {	cfg.callback = o; } else { 	$.extend(cfg, o); }

		    if (cfg.trackValues) { //get attributes old value
		    	this.each(function (i, el) {
		    		var attributes = {};
		    		for (var attr, i=0, attrs=el.attributes, l=attrs.length; i<l; i++){
		    		    attr = attrs.item(i);
		    		    attributes[attr.nodeName] = attr.value;
		    		}
		    		$(this).data('attr-old-value', attributes);
		    	});
		    }

			if (MutationObserver) { //Modern Browsers supporting MutationObserver
				var mOptions = {
					subtree: false,
					attributes: true,
					attributeOldValue: cfg.trackValues
				};
				var observer = new MutationObserver(function(mutations) {
					mutations.forEach(function(e) {
						var _this = e.target;
						//get new value if trackValues is true
						if (cfg.trackValues) {
							/**
							 * @KNOWN_ISSUE: The new value is buggy for STYLE attribute as we don't have
							 * any additional information on which style is getting updated.
							 * */
							e.newValue = $(_this).attr(e.attributeName);
						}
						cfg.callback.call(_this, e);
					});
				});

				return this.data('attrchange-method', 'Mutation Observer')
						.data('attrchange-obs', observer)
						.each(function() {
					observer.observe(this, mOptions);
				});
			} else if (isDOMAttrModifiedSupported()) { //Opera
				//Good old Mutation Events
				return this.data('attrchange-method', 'DOMAttrModified').on('DOMAttrModified', function(event) {
					if (event.originalEvent) event = event.originalEvent; //jQuery normalization is not required
					event.attributeName = event.attrName; //property names to be consistent with MutationObserver
					event.oldValue = event.prevValue; //property names to be consistent with MutationObserver
					cfg.callback.call(this, event);
				});
			} else if ('onpropertychange' in document.body) { //works only in IE
				return this.data('attrchange-method', 'propertychange').on('propertychange', function(e) {
					e.attributeName = window.event.propertyName;
					//to set the attr old value
					checkAttributes.call($(this), cfg.trackValues , e);
					cfg.callback.call(this, e);
				});
			}

			return this;
	   },
	   _ext: function (s, o) { /*attrchange option/extension*/
		   switch (s) {
		   case 'disconnect':
			   return this.each (function () {
				   var attrchangeMethod = $(this).data('attrchange-method');
				   if (attrchangeMethod == 'propertychange' || attrchangeMethod == 'DOMAttrModified') {
					   $(this).off(attrchangeMethod);
				   } else if (attrchangeMethod == 'Mutation Observer'){
					   $(this).data('attrchange-obs').disconnect();
				   }
			   }).removeData('attrchange-method');
		   }
	   }
   }

})(jQuery);

/**
 * Update URL parameters on the fly
 * http://stackoverflow.com/questions/1090948/change-url-parameters
 */
function updateURLParameter(url, param, paramVal) {
    var TheAnchor = null;
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";

    if (additionalURL) {
        var tmpAnchor = additionalURL.split("#");
        var TheParams = tmpAnchor[0];
            TheAnchor = tmpAnchor[1];
        if(TheAnchor)
            additionalURL = TheParams;

        tempArray = additionalURL.split("&");

        for (i=0; i<tempArray.length; i++)
        {
            if(tempArray[i].split('=')[0] != param)
            {
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    } else {
        var tmpAnchor = baseURL.split("#");
        var TheParams = tmpAnchor[0];
            TheAnchor  = tmpAnchor[1];

        if(TheParams)
            baseURL = TheParams;
    }

    if(TheAnchor)
        paramVal += "#" + TheAnchor;

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
	var rest = this.slice((to || from) + 1 || this.length);
	this.length = from < 0 ? this.length + from : from;
	return this.push.apply(this, rest);
};

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function(){

	// Side menu's
    /*$('#left-menu').sidr({
      name: 'sidr-left',
      side: 'left'
    });

    $('#right-menu').sidr({
      name: 'sidr-right',
      side: 'right'
    });

	// Load MixItUp if in DOM
	$mixContainer = $("#mixContainer");

	// Fire it off
	if($mixContainer.length > 0) {
        var defaultFilter = $mixContainer.data('default-filter');
        if(typeof defaultFilter === 'undefined') {
            defaultFilter = '';
        }
		$mixContainer.mixItUp({
            load: {
                filter: defaultFilter
            }
        });
	}

	// Content tabs
    if($('.slidingDiv').length > 0) {
    	$('.slidingDiv').hide();
    	$('.show_hide').show();

    	// Slide down
    	$('.show_hide').click(function(e){
    		e.preventDefault();
    		$('.slidingDiv').slideToggle();
    		return false;
    	});
    }

	// qTip2 init
	$('a[title].has-tip').qtip({
	    position: {
	        my: 'right bottom',
	        at: 'top left'
	    }
	});

    $('#basket').attrchange({
        trackValues: false,
        callback: function(e) {
            if(e.attributeName == 'class') {
                if($(this).hasClass('open')) {
                    $('body').addClass('activeBasket');
                } else {
                    $('body').removeClass('activeBasket');
                }
            }
        }
    });

    $( ".moreInfo dl.tabs dd" ).bind( "click", function() {
          var a_href = $(this).find("a").attr("href");
          if($(a_href).hasClass("active")){
              $(a_href).toggleClass("hide_tab_content");
         } else {
             $(a_href).removeClass("hide_tab_content");
         }
    });*/
});
