<?php 
include("includes/top.php");
$sTablename = $_GET["tablename"]; 
$aQuickFilters = ($_SESSION[$oSecurityObject->getsAdminType() . "aQuickFilters"][$sTablename])?$_SESSION[$oSecurityObject->getsAdminType() . "aQuickFilters"][$sTablename]:array();
$sFilters = GenericTableAdmin::createFilterStringForSql($newFilters, $sTablename);
?>
<script type="text/javascript" language="javascript">
	var btnToSwitch = null;
	function toggleBooleanField(obj, sIdField, iId, sField, sValue, sFilename){
		var ajaxUrl = "actions/generic_ajax.php?a=toggleBooleanField";
		var ajaxData = "<?php if($iLangId){ echo "&lid=" . $iLangId . "&languagetable=" . $sLanguageTable; } ?>&tablename=<?=$sTablename?>&idfield=" + sIdField + "&id=" + iId + "&field=" + sField + "&value=" + sValue + "&pp=" + sFilename;
		btnToSwitch = obj;
		startAchtungAjax();
		doAchtungAjax(ajaxUrl, ajaxData, "toggleButton");	
	}
	
	
	function toggleButton(){
		var sCurrentAction = btnToSwitch.attr("action");
		if(sCurrentAction == 'enable'){ 
			btnToSwitch.attr("action", 'disable');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disable'){ 
			btnToSwitch.attr("action", 'enable');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
	}
	
	var typingTimer;
	
	function searchRecord(sQuery, sField){
			clearTimeout(typingTimer);
			if(sQuery != ""){
				$("#clearbtn").css("visibility", "visible");
			}
			else{
				$("#clearbtn").css("visibility", "hidden");
				$("#searchText").val("");
			}
			$("#dialog_content").html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
			$("#dialog_content").load('actions/generic_ajax.php?a=genericTableSearch&tablename=<?=$sTablename?>&languagetable=<?=$sLanguageTablename?>&pp=relationtables.php&lid=<?=$iLangId?>&filterstring=<?=urlencode($sFilters);?>&searchfield=' + sField + '&query=' + urlencode(sQuery), onDocumentReady);
	}
	
	function typingSearch(sQuery, sField){
		clearTimeout(typingTimer);
		typingTimer = setTimeout("searchRecord('" + sQuery + "', '" + sField + "')", 550);		   		return false;
	}
	
</script>
	<h1><?=$oPage->sTitle;?></h1>
    <?php 
		if($sTablename && $sTablename != "null"){ 
		$bValidTable = true;
	?>
    <p />
	<ul class="subnav">
    	<li><a href="relationtables.php" class="largebutton">Back to Table List</a></li><li><a href="relationtables.php?tablename=<?=$sTablename?>" class="largebutton selected">Listing<?php if($currentLanguage){ ?> (<?=$currentLanguage;?>)<?php } ?></a></li><li><a href="generic-details.php?tablename=<?=$sTablename?>&pp=relationtables.php" class="largebutton">Insert New Record</a></li>
    </ul>
 <div class="searchBox">
    <table width="100%">
        <tr>
   			<td align="center">
                Filter by 
                <?php 
				$aColumns = GenericTableAdmin::getTableColumns($sTablename); ?>
                <select id="searchField">
                <?php foreach($aColumns as $currentColumn){ 
				if(!strstr($currentColumn["Type"], "tinyint")){
				?>
                <option value="<?=$currentColumn["Field"];?>" <?php if ($aQuickFilters['field'] == $currentColumn["Field"]) { ?> selected="selected" <?php } ?> ><?=$currentColumn["Field"];?></option>
                <?php
				}
				 } ?>
                </select> 
                <input id="searchText" type="text" onKeyUp="typingSearch($(this).val(), $('#searchField').val());" <?php if ($aQuickFilters['query'] ) { ?> value="<?=$aQuickFilters['query']?>" <?php } ?>/><a class="clearbtn" <?php if($sReferenceNumber == "" && $aQuickFilters['query'] == ""){ ?>style="visibility: hidden;"<?php }?> onclick="return typingSearch('', $('#searchField').val());" href="#" id="clearbtn">x</a>
        </td>
        </tr>
        </table>
    </div>
     <div id="dialog_content" style="overflow: auto; position: relative;">
    <?php	
		if($aQuickFilters){
			$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct." . $aQuickFilters['field'] . " LIKE '%" . $aQuickFilters['query'] . "%'");
		}
    	echo GenericTableAdmin::createGenericDataTable($sTablename, $iLangId,  $sFilters, "relationtables.php", $iLimit);  ?>
     </div>  
        <?php
     } 
	 else
	 {
		$aTables = $oSecurityObject->getNavigationPages($oPage->iID);
			?>
            <div>
                <ol>
                <?php
                foreach ($aTables as $aTable )
                {
                ?>
                    <li>
                        <a href="<?=$aTable["filename"];?><?php if($aTable["tablename"]){ ?>?tablename=<?=$aTable["tablename"]; }?>"><?=$aTable["title"];?></a>
                    </li>
                <?php    	
                }
                ?>
                </ol>
            </div>
    <?php
	 }
	 
include("includes/bottom.php");

?>