<?php
include ("includes/top.php");
//SHORTCUT - alt+5 clears report cache
?>
<h1><?=$oPage->sTitle?></h1>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).bind('keydown', 'alt+5', clearCache);
	});
	
	function clearCache(){
		document.location = "?tablename=<?=$oPage->sTablename?>&action=clearcache";
	}
</script>
<?php 

if ( $oPage->sTablename )
{
	if ( $_GET['draw'] )
	{
		$bSummary = ($_GET['view'] == 'brief')?true:false;
		?>
        <ul class="subnav">
			<li>
				<a href="<?=$oPage->sFilename?>?tablename=<?=$oPage->sTablename?>" class="largebutton">Back to Results</a>
			</li>
			<li>
				<a href="<?=$oPage->sFilename?>?tablename=<?=$oPage->sTablename?>&draw=<?=$_GET['draw']?><?=($bSummary)?'':'&view=brief'?>" class="largebutton"><?=($bSummary)?'Show All':'Show Summary'?></a>
			</li>
			<li>
				<a href="" class="largebutton" onclick="printPage(); return false;">Print Winners</a>
			</li>
		</ul>
        <?php       
		$aResults = LotteryResultsAdmin::getWinners($oPage->sTablename, $_GET['draw'], true, $bSummary);
		if ( $aResults )
		{
			if ( $bSummary )
			{
				echo "<h2>Showing Winners Summary for Draw " . $_GET['draw'] . "</h2>";
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class='report'>";
				echo "<tr>";
				echo "<th>Match Type</th>";
				echo "<th>Winners</th>";
				echo "<th>Total Payment Amount</th>";
				echo "<th>Total Winning Amount</th>";
				echo "<th>Amount</th>";
				echo "<th>Tickets Purchased</th>";
				echo "<th>Paid</th>";
				echo "<th>Emailed</th>";
				echo "<th></th>";
				echo "</tr>";
				foreach ( $aResults as $key=>$aResult )
				{
                                    
					$sClass = '';
					if ( $aResult['prize'] > 1 && $aResult['winners'] > 0 )
					{
						$sClass = " priority";
					}
					if ( $key % 2 )
					{
						echo "<tr class='alternate{$sClass}'>";
					}
					else
					{
						echo "<tr class='{$sClass}'>";
					}
					echo "<td>" . $aResult['type'] . "</td>";
					echo "<td>" . $aResult['winners'] . "</td>";
					echo "<td>" . number_format($aResult['total_transaction_amount'],2,".",",") . "</td>";
					echo "<td>" . number_format($aResult['total_winnings'],2,".",",") . "</td>";
					echo "<td>" . number_format($aResult['amount'],2,".",",") . "</td>";
					echo "<td>" . $aResult['purchased_tickets'] . "</td>";
					echo "<td>" . (($aResult['paid'])?'Yes':'<span class="alert">No</span>') . "</td>";
					echo "<td>" . (($aResult['emailed'])?'Yes':'<span class="alert">No</span>') . "</td>";
					echo "<td><a href=''>Process</a></td>";
					echo "</tr>";
				}
			}
			else
			{
				echo "<h2>Showing Winners Breakdown for Draw " . $_GET['draw'] . "</h2>";
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class='report'>";
				echo "<tr>";
				echo "<th>Match Type</th>";
				echo "<th>Client Name</th>";
				echo "<th>Payment Amount</th>";
				echo "<th>Winning Amount</th>";
				echo "<th>Purchased</th>";
				echo "<th>Paid</th>";
				echo "<th>Emailed</th>";
				echo "<th>Transaction</th>";
				echo "<th></th>";
				echo "</tr>";
				foreach ( $aResults as $key=>$aResult )
				{
                                    
					$sClass = '';
					if ( $aResult['prize'] > 1 && $aResult['winners'] > 0 )
					{
						$sClass = " priority";
					}
					if ( $key % 2 )
					{
						echo "<tr class='alternate{$sClass}'>";
					}
					else
					{
						echo "<tr class='{$sClass}'>";
					}
					echo "<td>" . $aResult['type'] . "</td>";
					echo "<td>" . $aResult['firstname'] . " " . $aResult['lastname'] . "<br/><small>" . $aResult['email'] . "</small></td>";
					echo "<td>" . number_format($aResult['transaction_amount'],2,".",",") . "</td>";
					echo "<td>" . number_format($aResult['amount'],2,".",",") . "</td>";
					echo "<td>" . (($aResult['purchased'])?'Yes':'<span class="alert">No</span>') . "</td>";
					echo "<td>" . (($aResult['paid'])?'Yes':'<span class="alert">No</span>') . "</td>";
					echo "<td>" . (($aResult['emailed'])?'Yes':'<span class="alert">No</span>') . "</td>";
					echo "<td>" . (($aResult['transaction'])?"<small>" . $aResult['transaction'] . "<br/>" . $aResult['transaction_date'] . "</small>":(($aResult['paid'])?'Manual Payment':'Not Paid')) . "</td>";
					echo "<td><a href=\"actions/reservations_ajax.php?a=getTicketSummary&id={$aResult['bookingitem_id']}&pp={$oPage->sFilename}&tablename={$oPage->sTablename}\" class=\"simpledialog\" >Process</a></td>";
					echo "</tr>";
				}
				
			}
			echo "</table>";
		}
		else
		{
			echo "<h2>Showing Winners for Draw " . $_GET['draw'] . "</h2>";
			echo "<div class='reportempty'>No Winners for this Draw</div>";
		}
			
		
	}
	else
	{
		?>
		<ul class="subnav">
			<li>
				<a href="<?=$oPage->sFilename?>" class="largebutton">Change Lottery</a>
			</li>
			<li>
				<a href="" class="largebutton" onclick="printPage(); return false;">Print Draws</a>
			</li>
		</ul>
		<div id="reports">
		<?php
			$aResults = LotteryResultsAdmin::getLotteryDraws($oPage->sTablename);
			if ( $aResults )
			{
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class='report'>";
				echo "<tr><th>Draw Number</th><th>Draw Date</th><th>Numbers</th><th>Jackpot</th><th>Processed</th><th></th>";
				foreach ( $aResults as $key=>$aResult )
				{
					if ( $key % 2 )
					{
						echo "<tr class='alternate'>";
					}
					else
					{
						echo "<tr>";
					}
					echo "<td>" . $aResult['draw'] . "</td>";
					echo "<td>" . date("l j F Y", strtotime($aResult['date'])) . "</td>";
					echo "<td>" . str_replace("|", "&nbsp;&nbsp;", $aResult['numbers']) . "</td>";
					echo "<td>" . number_format($aResult['jackpot'],0,".",",") . "</td>";
					echo "<td>" . (($aResult['processed'])?$aResult['winners'] . ' Winners':'<span class="alert">No</span>') . "</td>";
					echo "<td><a href='" . $oPage->sFilename . "?tablename=" . $oPage->sTablename ."&draw=" . $aResult['draw'] . "'>" . (($aResult['processed'])?'View Winners':'Process Draw') . "</a></td>";
					echo "</tr>";
				}
				echo "</table>";
			}
			else
			{
				echo "<div class='reportempty'>No Lottery Results Entered Yet</div>";
			}
		?>
		</div>
		<?php
		unset($aResults);
	}
}
else
{
	$aPages = $oSecurityObject->getNavigationPages($oPage->iID);
	?>
    <div id="reports">
    <ul>
    <?php
	foreach ($aPages as $aPage )
	{
	?>
		<li>
        	<a href="<?=$aPage["filename"];?><?php if($aPage["tablename"]){ ?>?tablename=<?=$aPage["tablename"]; }?>"><img src='images/lotto-<?=$aPage["tablename"]?>.png' width="70px" />
            <br/>
        	<?=$aPage["title"];?></a>
        </li>
    <?php    	
	}
	?>
	</ul>
    </div>
    <?php
}
include ("includes/bottom.php");
?>