<?php
    require("../system/includes/autoload.php");
    include("security/checkauth.php");
    $sFolder = $oSecurityObject->getsFolder();
    $oSecurityObject->deleteRememberCookie();
    $oSecurityObject->Logout();
    unset($oSecurityObject);
    include("includes/session_end.php");
    header('Location: /'.$sFolder.'login.php');
    die();