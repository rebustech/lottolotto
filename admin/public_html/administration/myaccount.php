<?php
	include("includes/top.php"); 
	$iLotteryID = 1;
	$dDrawDate = "2010-07-14 18:00:00";
	$aNumbers = array(43,21,5,12,14,2);
	
	//LotteryResultsAdmin::addResults($iLotteryID, $dDrawDate, $aNumbers, 13124.21, NULL, 2, false );
	
?>
	<h1>My Account</h1>
    <?php include("includes/errors.php");
	$aUserData = $oSecurityObject->getUserDetails();
	 ?>
     <form name="accountdetailsform" id="accountdetailsform" action="actions/myaccount_ajax.php?a=saveDetails&pp=<?=$sCurrentFilename?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Modify Account Details</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">First name</th>
            <td><input type="text" name="firstname" class="required"  value="<?=$aUserData["firstname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Last name</th>
            <td><input type="text" name="lastname" class="required"  value="<?=$aUserData["lastname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Email</th>
            <td><input type="text" name="email" class="required medium"  value="<?=$aUserData["email"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Telephone</th>
            <td>
            	<input type="text" name="telephone1" class="required"  value="<?=$aUserData["tel1"]?>"/><br />
            	<input type="text" name="telephone2"  value="<?=$aUserData["tel2"]?>"/>
           </td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Save Details" />
            </th>
        </tr>
    </table>
  	</form>
    <br />
<form name="changepasswordform" action="actions/myaccount_ajax.php?a=changePassword&pp=<?=$sCurrentFilename?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change Password</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">Old password</th>
            <td><input type="password" name="oldpassword" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">New password</th>
            <td><input type="password" name="password" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm password</th>
            <td><input type="password" name="password2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change Password" />
            </th>
       </tr>
    </table>
  	</form>
   <br />
<form name="changepinform" action="actions/myaccount_ajax.php?a=changePin&pp=<?=$sCurrentFilename?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change PIN Number</strong>
            </td>
        </tr>
        <tr>
        	<th align="right" class="small">Old PIN</th>
            <td><input type="password" name="oldpin" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">New PIN</th>
            <td><input type="password" name="pin" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm PIN</th>
            <td><input type="password" name="pin2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change PIN" />
            </th>
       </tr>
    </table>
  	</form>
<?php include("includes/bottom.php"); ?>