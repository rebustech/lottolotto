<?php
include ("includes/top.php");
$sTablename = $_GET["tablename"]; 
$iLimit = $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"];
$sFileName = basename($_SERVER["PHP_SELF"]);

?>
<script type="text/javascript" language="javascript">
	var btnToSwitch = null;
	function toggleBooleanField(obj, sIdField, iId, sField, sValue, sFilename){
		var ajaxUrl = "actions/users_ajax.php?a=toggleBooleanField";
		var ajaxData = "&tablename=<?=$sTablename?>&idfield=" + sIdField + "&id=" + iId + "&field=" + sField + "&value=" + sValue + "&pp=<?="users.php"?>";
		btnToSwitch = obj;
		startAchtungAjax();
		doAchtungAjax(ajaxUrl, ajaxData, "toggleButton");	
	}
	
	
	function toggleButton(){
		var sCurrentAction = btnToSwitch.attr("action");
		if(sCurrentAction == 'enable'){ 
			btnToSwitch.attr("action", 'disable');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disable'){ 
			btnToSwitch.attr("action", 'enable');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
	}
	
	var typingTimer;
	
	function searchRecord(sQuery, sField){
			clearTimeout(typingTimer);
			if(sQuery != ""){
				$("#clearbtn").css("visibility", "visible");
			}
			else{
				$("#clearbtn").css("visibility", "hidden");
				$("#searchText").val("");
			}
			$("#dialog_content").html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
			$("#dialog_content").load('actions/users_ajax.php?a=usersTableSearch&tablename=<?=$sTablename?>&pp=users.php&filterstring=<?=urlencode($sFilters);?>&searchfield=' + sField + '&query=' + urlencode(sQuery), onDocumentReady);
	}
	
	function typingSearch(sQuery, sField){
		clearTimeout(typingTimer);
		typingTimer = setTimeout("searchRecord('" + sQuery + "', '" + sField + "')", 550);		   		return false;
	}
	
</script>
	<h1><?php echo ucwords($sTablename); ?></h1>
    <?php include("includes/errors.php"); ?>
    <?php if($bFiltersCheck){ ?>
    <ul class="subnav">
    	<li><a href="users.php?tablename=<?=$sTablename?>" class="largebutton selected">Listing</a></li><li><a href="users-details.php?tablename=<?=$sTablename?>&pp=users.php" class="largebutton">Insert New User</a></li>
    </ul>
    <div style="float: right; position: absolute; top: 40px; right: 20px;"
    	<a href="#" rel="dialog_content" class="simpledialog">Fullscreen View</a>
    </div>
		<?php if ($oErrors->getErrorCount() > 0) { ?>
        <table width="100%" class="detailsform"  border="0" cellspacing="0" cellpadding="5">
        <tr>
        <td bgcolor="#FF0000" class="white"><?=$oErrors->getErrorString();?></td>
        </tr>
        </table><?php 
        $oErrors->clearErrors();
        }?>
    <div class="searchBox">
    <table width="100%">
        <tr>
   			<td align="center">
                Filter by 
                <?php 
				$aColumns = GenericTableAdmin::getTableColumns($sTablename); ?>
                <select id="searchField">
                <?php foreach($aColumns as $currentColumn){ 
				if(!strstr($currentColumn["Type"], "tinyint") && $currentColumn["Field"] != "password" && $currentColumn["Field"] != "pin"){
				?>
                <option value="<?=$currentColumn["Field"];?>"><?=$currentColumn["Field"];?></option>
                <?php
				}
				 } ?>
                </select> 
                <input id="searchText" type="text" onKeyUp="typingSearch($(this).val(), $('#searchField').val());" /><a class="clearbtn" <?php if($sReferenceNumber == ""){ ?>style="visibility: hidden;"<?php }?> onclick="return typingSearch('', $('#searchField').val());" href="#" id="clearbtn">x</a>
        </td>
        </tr>
        </table>
    </div>
     <div id="dialog_content" style="overflow: auto; position: relative;">
    <?php 
		$aData = GenericTableAdmin::getRecords($sTablename, GenericTableAdmin::createFilterStringForSql($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"], $sTablename), "", $iLimit);
		$aData["pp"] = $sFileName;
		echo GenericTableAdmin::createDataTable(
				array("ID|tiny|centeralign", "Username", "First name", "Last name", "Role", "isactive","Edit|tiny|centeralign", "Delete|tiny|centeralign"), 
				array("user_id|centeralign", "username", "firstname", "lastname", "fk_admintype_id", "user_id|ajaxbool:isactive|centeralign", "user_id|link:users-details.php?pp=" . $sFileName . "&a=edit&tablename=" . $sTablename . "|centeralign",
					  "user_id|link:actions/generic_actions.php?pp=" . $sFileName . "&a=delete&tablename=" . $sTablename . "|centeralign"), 
					  $aData, $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]);
	?>
    </div>
<? 
	}
	else{ ?>
    <ul class="subnav">
    	<li><a href="users-details.php?tablename=<?=$sTablename?>" class="largebutton">Insert New Record</a></li>
    </ul>
	<?php 
	}
include("includes/bottom.php"); ?>