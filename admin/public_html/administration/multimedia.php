<? include("includes/top.php"); 
if($_GET["id"]){
$iMMID = $_GET["id"];
}
?>
<link rel="stylesheet" href="styles/admin-multimedia.css" />
<h1>Multimedia</h1>
    <p />
    <ul class="subnav">
    	<li><a href="#" class="largebutton selected">Listing<?php if($currentLanguage){ ?> (<?=$currentLanguage;?>)<?php } ?></a></li><li><a href="addmultimedia.php?tablename=<?=$_GET["tablename"];?>" class="largebutton">Add New Multimedia Item</a></li>
    </ul>
<?php if($newFilters[18]){ 
$iMMTypeID = $newFilters[18][0];
$iLangID = $_SESSION[$oSecurityObject->getsAdminType() ."iLangID"];
$_SESSION[$oSecurityObject->getsAdminType() . "mmtypeid"] = $iMMTypeID;
$aMMType = MultimediaAdmin::getMMTypeTypeByID($iMMTypeID);
$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);


$aMMItems = MultimediaAdmin::getMMList($iWebsiteID,$iMMTypeID,'','', $iLangID, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);	
if ($iMMTypeID == 10 || $iMMTypeID == 9){ 
	$sThumbFile = "../system/media/en/images/{$sFolder}/thumbs/"; 
}
elseif($iMMTypeID == MultimediaAdmin::PDFDOCS){
	$sThumbFile = "images/multimedia_icons/pdf.jpg?f=";
}
elseif($iMMTypeID == MultimediaAdmin::WORDDOCS){
	$sThumbFile = "images/multimedia_icons/word.jpg?f=";
}
elseif($iMMTypeID == MultimediaAdmin::INTERNALLINK || $iMMTypeID == MultimediaAdmin::EXTERNALLINKS){
	$sThumbFile = "images/multimedia_icons/link.jpg?f=";
}
elseif($iMMTypeID == MultimediaAdmin::VIDEOS){
	$sThumbFile = "images/multimedia_icons/video.jpg?f=";
}
else{ 
	$sThumbFile = "thumbnail.php?maxw=100&maxh=100&file=../system/media/en/images/{$sFolder}/";
}

$swfImage = "images/multimedia_icons/swf.jpg";
?>
<script type="text/javascript" language="javascript">
	var typingTimer;
	
	function searchMM(sQuery, sField){
			clearTimeout(typingTimer);
			$("#librarymmDIV").html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
			switch(sField){
				case "comment":
					$("#librarymmDIV").load('actions/multimedia_ajax.php?a=searchMMByComment&tablename=multimedia_cmn&pp=multimedia.php<?php if($iLangID){?>&lid=<?=$iLangID?><?php } ?>&typeid=<?=$iMMTypeID?>&query=' + urlencode(sQuery));
				break;
				case "filename":
					$("#librarymmDIV").load('actions/multimedia_ajax.php?a=searchMMByFilename&tablename=multimedia_cmn&pp=multimedia.php<?php if($iLangID){?>&lid=<?=$iLangID?><?php } ?>&typeid=<?=$iMMTypeID?>&query=' + urlencode(sQuery));
				break;
			}
	}
	
	function typingSearch(sQuery, sField){
		clearTimeout(typingTimer);
		typingTimer = setTimeout("searchMM('" + sQuery + "', '" + sField + "')", 550);		   
	}
	
	
</script>
  <div class="searchBox">
  	<table width="100%">
    	<tr>
        <td width="50%" align="center">
            	<label for="txtComment">Comment</label>
                <input id="txtComment" type="text" onKeyUp="typingSearch($(this).val(), 'comment');" onClick="$(this).removeClass('disabledTextField'); $('#txtFilename').addClass('disabledTextField');" />
        </td><td>
               <label for="txtFilename" >Filename</label>
               <input id="txtFilename" type="text" onKeyUp="typingSearch($(this).val(), 'filename');" onClick="$(this).removeClass('disabledTextField'); $('#txtComment').addClass('disabledTextField');" class="disabledTextField" />
        </td>
        </tr>
        </table>
    </div>
	<div id="librarymmDIV" class="MMListing">
    	<?php foreach($aMMItems as $iKey => $aCurrentItem){
			if ( strrchr($aCurrentItem["file"],'.') == ".swf" ){
				$sCurrentSrc = $swfImage;
			}
			else{
				$sCurrentSrc = $sThumbFile . $aCurrentItem["file"];
			}
			?>
            <a href="generic-details.php?tablename=multimedia_cmn<?php if($iLangID){?>&lid=<?=$iLangID?>&languagetable=multimedia_lang<?php } ?>&pp=multimedia.php&id=<?=$aCurrentItem["id"];?>" title="<?=$aCurrentItem["comment"];?>">
            <div class="mm_plc">
                <div class="mm_image">
                    <img src="<?=$sCurrentSrc?>" alt="Not found"/>
                </div>
                <div class="mm_caption">
                	<?php if($iLangID && !$aCurrentItem["title"]){ 
						echo "<span class=\"red bold\">No Translation</span>"; 
					}
					else if($aCurrentItem["title"]){
						echo $aCurrentItem["title"]; 
					}
					else{
						echo $aCurrentItem["comment"]; 
					}?>
                </div>
            </div>
            </a>
        <?php } ?>
    </div>
<? 
}else{
?>
    <div style="clear: both;"><br /><br />You must select both Website and Multimedia Type.</div>
    <?php	
}
include("includes/bottom.php"); ?>