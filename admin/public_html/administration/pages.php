<?php
include("includes/top.php"); 
if($_GET["clear"] == "languagefilter"){
	unset($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"]);	
}
$iLangId = $_SESSION[$oSecurityObject->getsAdminType() ."iLangID"];

$sTablename = $_GET["tablename"];
?>
<link rel="stylesheet" href="styles/admin-pagelist.css" />
	<h1>Pages</h1>
<?php 
if($newFilters[17]){
$rootpage = PageAdmin::getRootPage($newFilters[17][0], $iLangId); 
$aChildPages = PageAdmin::getPageChildren($rootpage["sectionid"], $rootpage['left_node'], $rootpage['right_node'], $iLangId);
?>
<script language="javascript">
	var serializedData = "";
	
	$(document).ready(initSortable);

	function initSortable(){
		serializedData = "";
		$('#aPageData').NestedSortable(
			{
				accept: 'page-item',
				noNestingClass: "no-nesting",
				opacity: .8,
				helperclass: 'helper',
				autoScroll: true,
				handle: '.sort-handle',
				currentNestingClass: 'current-element',
				onChange : function(serialized) { serializedData = serialized; $(".saveButtons").attr("disabled", ""); }
			}
		);
		$(".saveButtons").attr("disabled", "disabled");
	}

	function saveSortingChanges(){
		if(serializedData != ""){
			 $(".saveButtons").attr("disabled", "disabled");
			$("#srtContainer").html("<p /><center><img src='images/loadingclock.gif' alt='Loading..' /></center>");
			var sortingAjaxUrl = "actions/pages_ajax.php?a=sorting<?php if($iLangId){ echo "&lid=" . $iLangId; } ?>&sectionid=<?=$rootpage["sectionid"];?>&rootpageid=<?=$rootpage["id"]?>&tablename=<?=$sTablename?>&pp=<?=$sCurrentFilename?>&" + serializedData[0].hash;
			$("#srtContainer").load(sortingAjaxUrl, initSortable);
		}
	}
	
	function checkSorting(){
		var bProceed = true;
		if(serializedData != ""){
			bProceed = confirm("You have not yet saved your sorting changes. Are you sure you want to proceed?");
		}
		return bProceed;
	}
	
	var btnToSwitch;
	function doAction(action, iPageID, obj){
		var bProceed = true;
		if(serializedData != ""){
			bProceed = confirm("You have not yet saved your sorting changes. Are you sure you want to proceed?");
		}
		if(bProceed){
			if(action == "deleteglobal"){
				bProceed = confirm("You are about to permanently delete this page. Are you sure you want to proceed?");
			}
			else if(action == "deletelang"){
				bProceed = confirm("You are about to permanently delete this language translation. Are you sure you want to proceed?");
			}
			if(bProceed){
				var ajaxUrl = "actions/pages_ajax.php?a=" + action + "&tablename=<?=$sTablename?>&pp=<?=$sCurrentFilename?>";
				var ajaxData = "<?php if($iLangId){ echo "&lid=" . $iLangId; } ?>&sectionid=<?=$rootpage["sectionid"];?>&id=" + iPageID;
				if(action == "deleteglobal" || action == "deletelang"){
					$("#srtContainer").html("<p /><center><img src='images/loadingclock.gif' alt='Loading..' /></center>");
					ajaxUrl += ajaxData;
					$("#srtContainer").load(ajaxUrl, initSortable);
				}
				else{
					btnToSwitch = obj
					startAchtungAjax();
					doAchtungAjax(ajaxUrl, ajaxData, "toggleButton");	
				}
			}
		}
	}
	
	
	function toggleButton(){
		var sCurrentAction = btnToSwitch.attr("action");
		
		if(sCurrentAction == 'enable'){ 
			btnToSwitch.attr("action", 'disable');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disable'){ 
			btnToSwitch.attr("action", 'enable');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
		else if(sCurrentAction == 'disablemobile'){ 
			btnToSwitch.attr("action", 'enablemobile');
			btnToSwitch.children(":first").attr("src", "images/notmobile.gif");
		}
		else if(sCurrentAction == 'enablemobile'){ 
			btnToSwitch.attr("action", 'disablemobile');
			btnToSwitch.children(":first").attr("src", "images/mobile.gif");
		}
		else if(sCurrentAction == 'showinmenu'){ 
			btnToSwitch.attr("action", 'donotshowinmenu');
			btnToSwitch.children(":first").attr("src", "images/showinmenu.gif");
		}
		else if(sCurrentAction == 'donotshowinmenu'){ 
			btnToSwitch.attr("action", 'showinmenu');
			btnToSwitch.children(":first").attr("src", "images/donotshowinmenu.gif");
		}
		else if(sCurrentAction == 'showinsitemap'){ 
			btnToSwitch.attr("action", 'donotshowinsitemap');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'donotshowinsitemap'){ 
			btnToSwitch.attr("action", 'showinsitemap');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
		else if(sCurrentAction == 'enablelanguage'){ 
			btnToSwitch.attr("action", 'disablelanguage');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disablelanguage'){ 
			btnToSwitch.attr("action", 'enablelanguage');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
	}
</script>
    <ul class="subnav">
    	<li><a href="generic-listing.php" class="largebutton selected">Listing<?php if($currentLanguage){ ?> (<?=$currentLanguage;?>)<?php } ?></a></li><li><a href="generic-details.php?tablename=pages_cmn&pp=pages.php" class="largebutton">Add New Page</a></li>
    </ul>
    
<div style="text-align: right; padding-right: 20px;"><small>Click <input type="button" class="saveButtons" disabled="disabled" value="Save" onclick="saveSortingChanges();"/> for sorting changes to take place.</small></div>
<p />
<div class="nestedsortable" id="srtContainer">
	<?php if($iLangId){ echo GenericTableAdmin::displayRootPageLang($rootpage); }else{ echo GenericTableAdmin::displayRootPage($rootpage); } ?>
    <ul id="aPageData" class="page-list"> 
    	<?php if($iLangId){ echo GenericTableAdmin::displayChildPagesLang($aChildPages); }else{ echo GenericTableAdmin::displayChildPages($aChildPages); } ?>
    </ul> 
</div> 
<p />
<div style="text-align: right; padding-right: 20px;"><small>Click <input type="button" class="saveButtons" disabled="disabled" value="Save" onclick="saveSortingChanges();"/> for sorting changes to take place.</small></div>
<?php 
}else{
	?>
    You must select Section.
    <?php	
} ?>
<? include("includes/bottom.php"); ?>