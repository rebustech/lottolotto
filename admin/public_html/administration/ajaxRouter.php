<?php

/**
 * Routes all calls to /API in the relevant code
 * URLS are in the format /API/Class/Method
 * Classes that are available via the API should extend \LL\AjaxController
 * Classes made available for the API should ONLY contain methods available via the API
 *
 *
 * @todo put in a custom error handler so we can report errors back on all ajax calls
 * @todo need to put some authentication in place for API calls
 * @todo maybe we should consider making this available via XML too?
 *
 * List of currently available API calls:
 *
 * EngineAPI
 * InstallerAjax
 */
error_reporting(0);
if ($_GET['format'] == 'json' || !isset($_GET['format'])) {
    header('Content-type: application/json');
}

$bNoRedirect = true;
require_once("../system/includes/autoload.php");

# Credentials check - shouldn't be here?
# include("security/checkauth.php");
session_write_close();

$aUrlParts = explode('?', $_SERVER['REQUEST_URI']);
$sParts = explode('/', $aUrlParts[0]);
$sClassName = $sParts[3];
$sMethodName = $sParts[4];

if ($sMethodName == '')
    $sMethodName = 'index';

# Check API class
if (strpos($sClassName, 'Ajax') === false) {
    $sClassName = ucfirst($sClassName) . 'Api';
}
$oClass = new $sClassName;

if ($oClass == '') {
    include 'index.php';
    die();
}

if ($oClass instanceof \LL\AjaxController) {
    if (method_exists($oClass, $sMethodName)) {
        echo $oClass->$sMethodName();
    } else {
        echo 'Method not found';
    }
} else {
    echo '404';
}