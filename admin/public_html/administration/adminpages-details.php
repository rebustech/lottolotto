<?php 
$_GET['tablename'] = "admin_pages";
include("includes/top.php"); 
$iId = $_GET["id"];
$iLangId = $_GET["lid"];
if(!$iLangId){
	$iLangId = 0;
}
else{
	$currentLanguage = Language::getLanguage($iLangId);
}

$bValidTable = false;

$sTablename = $_GET["tablename"];
$sLanguageTablename = $_GET["languagetable"];
$sPreviousPage = $_GET["pp"] . "?tablename=" . $sTablename;
if($sOldQueryString){
	if($_GET["pp"] && strstr($sOldQueryString, "&pp=")){
		$iPPStartPos = strpos($sOldQueryString, "&pp=");
		$sPPqs = substr($sOldQueryString, strpos(sOldQueryString, "&pp="));
		$sPPqsTrimmed = substr($sPPqs, 1);
		$iPPEndPos = strpos($sPPqsTrimmed, "&");
		$sPPqs = substr($sPPqs, 0, $iPPEndPos + 1);
		$sOldQueryString = str_replace($sPPqs, "", $sOldQueryString);
	}
	if($sOldQueryString){
		$sPreviousPage .= "&qs=" . $sOldQueryString;
	}
}
$iWebsiteID = $_GET["websiteid"];
$bValidTable = GenericTableAdmin::checkTableExists($sTablename);
if($bValidTable && $iLangId && $sLanguageTablename){
	if(!GenericTableAdmin::checkTableExists($sLanguageTablename)){
		$bValidTable = false;
		echo "<span class='error'>Languages not applicable. Please set language filter to \'All\'.</span>";
	}	
}
if($bValidTable && $iId ){
?>
<script type="text/javascript" language="javascript">
function change(id) {
	if (id == '1') {
		$("#newfilename_div").css("display", "none");
		$("#newfile_div").css("display", "none");			
	}
	else if (id == '2') {
		$("#newfilename_div").css("display", "");
		$("#newfile_div").css("display", "none");							
	}
	else if (id == '3') {
		$("#newfilename_div").css("display", "none");
		$("#newfile_div").css("display", "");				
	}
}
</script>
	<h1>Admin Pages - Edit</h1>    
    <ul class="subnav">
    	<li><a href="<?php if($sPreviousPage && $sPreviousPage != "generic-listing.php"){ echo $sPreviousPage;  }else{ ?>generic-listing.php?tablename=<?=$sTablename?><?php }?>" class="largebutton">Listing</a></li>
        <li><a href="#" class="largebutton selected">Details View</a></li>
    </ul>
		<?php if ($oErrors->getErrorCount() > 0) { ?>
            <table width="100%" class="detailsform"  border="0" cellspacing="0" cellpadding="5">
                <tr>
	                <td bgcolor="#FF0000" class="white"><?=$oErrors->getErrorString()?></td>
                </tr>
            </table>
            <?php 
            $oErrors->clearErrors();
        }
		echo GenericTableAdmin::createGenericDetailsForm($sTablename, $iId, $_SESSION[$oSecurityObject->getsAdminType() . "formdata"], urlencode($sPreviousPage), $iWebsiteID);
		?>
        <br/>
        <form name="filtersform" action="actions/generic_ajax.php?a=saveRelationshipList&id=<?=$_GET["id"];?>&pp=<?=$_GET["pp"]?>&tablename=<?=$sTablename?>&reltable=admin_r_pages_filters&primary=page_id&secondary=filter_id" class="ajaxform" method="post">
       <table class="detailsform">
        <tr>
        	<th colspan="2">
            	<strong>Pages Filters</strong>
            </th>
        </tr>
  <?php $aFilters = AdminPage::getAllPageFilters($iId);	
		  foreach ($aFilters as $iFilterKey => $aPageFilter) 
  	 		{
			 if ($iFilterKey == 0) echo "<tr><td width='50%'>";
			 else if ($iFilterKey % 2 == 0) echo "<tr><td width='50%'>";
			 else if ($iFilterKey % 2 == 1) echo "<td width='50%'>";
			 ?>
				<input type="checkbox" name="fields[]" value="<?=$aPageFilter["id"]?>" <?php if ($aPageFilter['checked']) echo "checked"; ?>> <?=$aPageFilter["title"]?>
		<?php 
		     if ($iFilterKey % 2 == 0 && $iFilterKey != count($aFilters)) echo "</td>";
			 else if ($iFilterKey % 2 == 1 && $iFilterKey != count($aFilters)) echo "</td></tr>";
			 else if ($iFilterKey == count($aFilters)) echo "</td></tr>";
		} ?>
  </table>
          <table class="detailsform"><tr><th align="right"><input type="submit" value="Save Changes"/></th></tr></table>
        </form>
        <?php
}
unset($_SESSION[$oSecurityObject->getsAdminType() . "formdata"]);
include("includes/bottom.php"); ?>