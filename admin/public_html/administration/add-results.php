<?php
include ("includes/top.php");
?>
<h1><?=$oPage->sTitle?></h1>
<?php 
include ("includes/errors.php");
if ( $oPage->sTablename )
{
	if ( is_array($oPage->aFilters) )
	foreach ( $oPage->aFilters as $aPageFilter)
	{
		if ( array_key_exists($aPageFilter['id'], $aFilterData ) )
		{
			$aFilters[$aPageFilter['id']] = $aFilterData[$aPageFilter['id']];
		}
	}
	?>
    <ul class="subnav">
    	<li>
    		<a href="<?=$oPage->sFilename?>" class="largebutton">Back to Lotteries</a>
        </li>
    </ul>
    <form name="addResult" action="actions/lottery_actions.php?tablename=<?=$oPage->sTablename?>&pp=<?=$oPage->sFilename?>&a=addResult" method="post">
        <input type="hidden" name="lotteryid" value="<?=$oPage->sTablename?>" />
        <div id="results">
    <?php
		$aFormData = $_SESSION[$oSecurityObject->getsAdminType() . "formdata"];
		$aLotteryMatches = LotteryResultsAdmin::getWinningsCombinations($oPage->sTablename);
		$aDrawDates = LotteryResultsAdmin::getDrawDates($oPage->sTablename, true, true);
		$aLotteryWinningAmounts = LotteryResultsAdmin::getWinningAmounts($oPage->sTablename);
	
		echo "<strong>Draw Date:</strong><select name='drawdate'>";
		foreach ($aDrawDates as $DrawDate )
		{
			echo "<option value='" . $DrawDate['drawdate'] . "'>" . date("D d M Y H:i:s", strtotime($DrawDate['drawdate'])) . "</option>";
		}
		echo "</select><br/>";
		$draw = ($aFormData['draw'])?$aFormData['draw']:date("Ymd", strtotime($DrawDate['drawdate'])) ;
		echo "<strong>Draw:</strong> <input type='number' style='width:80px' name='draw' value='" . $draw . "' title='draw' autocomplete='off' /><br />";
		echo "<strong>Drawn Numbers:</strong> ";
		for ( $i = $aLotteryMatches['numbers']; $i > 0; $i-- )
		{
				echo '<input type="number" name="number[]" value="' . $aFormData['number'][$aLotteryMatches['numbers']-$i] . '" title="number" maxlength="2"  min="1" />';			
		}
		for ( $j = $aLotteryMatches['bonus']; $j > 0; $j-- )
		{
			echo '<input type="number" name="number[]" class="bonus" value="' . $aFormData['number'][($aLotteryMatches['numbers']+$aLotteryMatches['bonus'])-$j] . '" title="bonus number" maxlength="2" min="0" />';			
		}
		echo "<br/><strong>Winnings:</strong><Br/><br/>
		<table cellspacing='0' cellpadding='0'><tr><th width='160px' align='left'>Title</th><th width='80px' align='left'>Winners</th><th width='180px' align='left'>Amount</th></tr>";
		echo '<tr><td>Jackpot</td><td>&nbsp</td><td>' . $aLotteryMatches['currency']['shortname'] . ' <input type="number" name="jackpot" value="' . (($aFormData['jackpot'])?$aFormData['jackpot']:$aDrawDates[0]['jackpot']) . '" title="Jackpot" class="bonus" /></td></tr>';
		foreach ( $aLotteryMatches['matches'] as $key=>$aLotteryMatch )
		{
			if ( $key > 0 )
			{
				echo "<tr><td>Match " . $aLotteryMatch[0];
				if ( $aLotteryMatch[1] )
				{
					echo " plus " . $aLotteryMatch[1] . " " . (($aLotteryMatch[1] > 1)?$aLotteryMatches['bonus_plural']:$aLotteryMatches['bonus_text']);
				}
			}
			else
			{
				echo "<tr><td>Jackpot";
			}
			echo '</td><td><input type="number" name="winners[]" value="' . $aFormData['winners'][$key] . '" title="Winners"  /></td><td>' . $aLotteryMatches['currency']['shortname'] . '<input type="hidden" name="bonus[]" value="' . $aLotteryMatch[1]. '" /><input type="hidden" name="match[]" value="' . $aLotteryMatch[0] . '" /> <input type="input" name="winnings[]" value="' . (($aFormData['winnings'][$key])?$aFormData['winnings'][$key]:(($key == 0)?$aDrawDates[0]['jackpot']:$aLotteryWinningAmounts[$key])) . '" title="Winnings"  /></td></tr>';
			
		}
		echo "</table>";
	?>
    	</div>
        <input type="submit" value="Add Result" />
    </form>
<?php
}
else
{
	$aLotteries = LotteryAdmin::getLotteries(true);
	?>
    <div id="reports">
    <ul>
    <?php
	foreach ($aLotteries as $aLottery )
	{
	?>
            <li>
        	<a href="<?=$oPage->sFilename?>?tablename=<?=$aLottery["lottery_id"]?>"><img src='images/lotto-<?=$aLottery["lottery_id"]?>.png' width="70px" />
                <br/>
        	<?=$aLottery["comment"];?></a>
        </li>
    <?php    	
	}
	?>
	</ul>
    </div>
    <?php
}
include ("includes/bottom.php");
?>