<?php
	$bNoRedirect = true;
        require_once("../system/includes/autoload.php");
	include("security/checkauth.php");

	$sTablename = $_GET["tablename"];
	if(empty($sTablename)){
		header("location: index.php");
		exit;
	}

	$sPath = $_SERVER["PHP_SELF"];
	$aPath = explode("/", $sPath);
	$sCurrentFilename = $aPath[sizeof($aPath) - 1];
?>
	<script type="text/javascript" language="javascript">
        var typingTimer;

        function searchRecord(sQuery, sField){
                clearTimeout(typingTimer);
                if(sQuery != ""){
                    $("#clearbtn").css("visibility", "visible");
                }
                else{
                    $("#clearbtn").css("visibility", "hidden");
                    $("#searchText").val("");
                }
                $("#dialog_content").html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
                $("#dialog_content").load('actions/generic_ajax.php?a=tableviewSearch&tablename=<?=$sTablename?>&pp=<?=$sCurrentFilename?>&lid=<?=$iLangId?>&searchfield=' + sField + '&query=' + urlencode(sQuery), onDocumentReady);
        }

        function typingSearch(sQuery, sField){
            clearTimeout(typingTimer);
            typingTimer = setTimeout("searchRecord('" + sQuery + "', '" + sField + "')", 550);		   		return false;
        }

		function selectItem(obj){
			targetSelectField.val($(obj).attr("recordid"));
			targetSelectField.parent().find('div input').val($(obj).attr("data-return-text"));
			 $.simpleDialog.close();
		}

    </script>
	<div class="searchBox">
    	<table width="100%">
        	<tr>
   				<td align="center">
                Filter by
                <?php
				$aColumns = GenericTableAdmin::getTableColumns($sTablename); ?>
                <select id="searchField">
                <?php foreach($aColumns as $currentColumn){
				if(!strstr($currentColumn["Type"], "tinyint")){
				?>
                <option value="<?=$currentColumn["Field"];?>"><?=$currentColumn["Field"];?></option>
                <?php
				}
				 } ?>
                </select>
                <input id="searchText" type="text" onKeyUp="typingSearch($(this).val(), $('#searchField').val());" /><a class="clearbtn" <?php if($sReferenceNumber == ""){ ?>style="visibility: hidden;"<?php }?> onclick="return typingSearch('', $('#searchField').val());" href="#" id="clearbtn">x</a>
       		 </td>
        	</tr>
        </table>
    </div>
    <div style="text-align: left; height: 600px;" id="dialog_content">
        <?php echo GenericTableAdmin::createGenericTableView($sTablename, $iLangId, "", 20);  ?>
    </div>