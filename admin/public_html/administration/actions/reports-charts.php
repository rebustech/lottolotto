<?php
chdir('..');
include("security/checkauth.php");

	$sType = $_GET['tablename'];
	$sDisplay = $_GET['display'];

	$oFilters = unserialize($_GET['options']);
	if ( !is_null($sType) && !is_null($sDisplay) )
	{
		$oReports = new ReportsAdmin($oSecurityObject, $sType, $oFilters);
		if ($sType == "BookingsThisMonth"){
			$oReports->displayGraph($sDisplay, "bar");
		}
		else
		{
			$oReports->displayGraph($sDisplay);
		}
	}
?>