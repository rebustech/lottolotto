<?php
//error_reporting(E_ALL);
//include("../../../includes/autoload.php");

/**
 * Lottery functions for adding results
 */
include("../../system/includes/autoload.php");

// Get query string variables
$action = $_GET["a"];
$iId = $_GET["id"];
$sTablename = $_GET["tablename"];
$sPreviousPage = urldecode($_GET["pp"]);
$aLotteryData = array();

// Redirect page values
$redirect = "../" . $sPreviousPage . "?tablename=" . $sTablename;
$detailsredirect = "../" . $sPreviousPage . "?tablename=" . $sTablename;
 
// Flag to record whether the posted data is valid or not.
$bValid = true;

// Define new error object
$oErrors = new Error();
$oErrors->clearErrors();

switch($action) {
    case "addResult":
        $bValid = true;
        $aLotteryData["draw"] = $_POST["draw"];
        $aLotteryData["number"] = $_POST["number"];
        $aLotteryData["winnings"] = $_POST["winnings"];
        $aLotteryData["winners"] = $_POST["winners"];
        $aLotteryData["jackpot"] = $_POST["jackpot"];
        $aLotteryData["lotteryid"] = $_POST["lotteryid"];
        $aLotteryData["drawdate"] = $_POST["drawdate"];
        $aLotteryData["bonus"] = $_POST["bonus"];
        $aLotteryData["match"] = $_POST["match"];
        if(!$aLotteryData["drawdate"]){
            $oErrors->addError("Draw Date is required");
            $bValid = false;
        }
        if(!$aLotteryData["lotteryid"]){
            $oErrors->addError("Lottery ID is required");
            $bValid = false;
        }
        if(!$aLotteryData["draw"]){
            $oErrors->addError("Draw is required");
            $bValid = false;
        }
        if(!$aLotteryData["jackpot"]){
            $oErrors->addError("Jackpot is required");
            $bValid = false;
        }
        if(!$aLotteryData["winnings"]){
            $oErrors->addError("Winnings are required");
            $bValid = false;
        }
        if(!$aLotteryData["winners"]){
            $oErrors->addError("Winners are required");
            $bValid = false;
        }
        if(!$aLotteryData["number"]){
            $oErrors->addError("Lottery Numbers are required");
            $bValid = false;
        }
        foreach ( $aLotteryData['number'] as $key => $value )
        {
            if ( (int)$value < 0 )
            {
                $oErrors->addError("All Lottery numbers are required");
                $bValid = false;
            }
            $aLotteryData['number'][$key] = str_pad($value,2,"0", STR_PAD_LEFT);
        }
        if ( $bValid )
        {

            foreach ( $aLotteryData['winnings'] as $key=>$value )
            {
            $aWinnings[$key] = array('prize' => $value,
                                     'match' => $aLotteryData['match'][$key],
                                     'bonus' => $aLotteryData['bonus'][$key],
                                     'winners' => $aLotteryData['winners'][$key]);
            }
            $bValid = LotteryResultsAdmin::addResults($aLotteryData['lotteryid'], $aLotteryData['drawdate'], $aLotteryData['draw'], $aLotteryData['number'], $aLotteryData['jackpot'], $aWinnings);
            if ( $bValid )
            {
                $redirect = "../lottery-results.php?tablename=" . $aLotteryData['lotteryid'] . "&draw=" . $aLotteryData['draw'];
            }
        }
        break;
        
    case "editResult":
        $bValid = true;

        $aLotteryData["draw"] = $_POST["draw"];
        $aLotteryData["number"] = $_POST["number"];
        $aLotteryData["winnings"] = $_POST["winnings"];
        $aLotteryData["winners"] = $_POST["winners"];
        $aLotteryData["jackpot"] = $_POST["jackpot"];
        $aLotteryData["lotteryid"] = $_POST["lotteryid"];
        $aLotteryData["drawdate"] = $_POST["drawdate"];
        $aLotteryData["match"] = $_POST["match"];
        $aLotteryData["bonus"] = $_POST["bonus"];
        if(!$aLotteryData["drawdate"]){
            $oErrors->addError("Draw Date is required");
            $bValid = false;
        }
        if(!$aLotteryData["lotteryid"]){
            $oErrors->addError("Lottery ID is required");
            $bValid = false;
        }
        if(!$aLotteryData["draw"]){
            $oErrors->addError("Draw is required");
            $bValid = false;
        }
        if(!$aLotteryData["jackpot"]){
            $oErrors->addError("Jackpot is required");
            $bValid = false;
        }
        if(!$aLotteryData["winnings"]){
            $oErrors->addError("Winnings are required");
            $bValid = false;
        }
        if(!$aLotteryData["winners"]){
            $oErrors->addError("Winners are required");
            $bValid = false;
        }
        if(!$aLotteryData["number"]){
            $oErrors->addError("Lottery Numbers are required");
            $bValid = false;
        }
        foreach ( $aLotteryData['number'] as $key => $value )
        {
            if ( (int)$value <= 0 )
            {
                $oErrors->addError("All Lottery numbers are required");
                $bValid = false;
            }
            $aLotteryData['number'][$key] = str_pad($value,2,"0", STR_PAD_LEFT);
        }
        if ( $bValid )
        {
            foreach ( $aLotteryData['winnings'] as $key=>$value )
            {
                $aWinnings[$key] = array('prize' => $value,
                                         'match' => $aLotteryData['match'][$key],
                                         'bonus' => $aLotteryData['bonus'][$key],
                                         'winners' => $aLotteryData['winners'][$key]);
            }
            $bValid = LotteryResultsAdmin::editResult($aLotteryData['lotteryid'], $aLotteryData['drawdate'], $aLotteryData['draw'], $aLotteryData['number'], $aLotteryData['jackpot'], $aWinnings);
            if ( $bValid )
            {
                $redirect = "../lottery-results.php?tablename=" . $aLotteryData['lotteryid'] . "&draw=" . $aLotteryData['draw'];
            }
        }
        break;
     
    // ------ CODE FROM THIS PART HAS BEEN ADDED BY NATHAN
    case "addResultCheck":
        
        // Set valid flag as true here
        // as it'll only be modified if there's a problem
        $bValid = true;
        
        // A secondary flag to check on numbers only
        $bNumError = false;
        
        // An extra flag here, to ensure that we can omit winnings
        // if required (mainly for 6aus49 etc)
        $bWinningsNull = false;
                
        // Post data
        $aLotteryData["draw"] = $_POST["draw"];
        $aLotteryData["number"] = $_POST["number"];
        $aLotteryData["winnings"] = $_POST["winnings"];
        $aLotteryData["winners"] = $_POST["winners"];
        $aLotteryData["lotteryid"] = $_POST["lotteryid"];
        $aLotteryData["drawdate"] = $_POST["drawdate"];
        $aLotteryData["bonus"] = $_POST["bonus"];
        $aLotteryData["match"] = $_POST["match"];                              
        $aLotteryData['security_hash'] = $_POST["security_hash"];
        $aLotteryData['checkerid'] = $_POST["checkerid"];
        $aLotteryData['next_jackpot_amount'] = $_POST['next_jackpot_amount'];
        $aLotteryData['next_jackpot_rollover'] = $_POST['next_jackpot_rollover']==1?1:0;
        
        // Used for redirecting if errors in submitted data
        $detailsredirect = "../add-results-checker.php?sh={$aLotteryData['security_hash']}" .
                           "&l={$aLotteryData['lotteryid']}" .
                           "&c={$aLotteryData['checkerid']}" .
                           "&d={$aLotteryData['draw']}";

        if(!$aLotteryData["drawdate"]){
            $oErrors->addError("Draw Date is required");
            $bValid = false;
        }
        if(!$aLotteryData["lotteryid"]){
            $oErrors->addError("Lottery ID is required");
            $bValid = false;
        }
        if(!$aLotteryData["draw"]){
            $oErrors->addError("Draw is required");
            $bValid = false;
        }


        // Number of winnings and winners required for a full completed set of prize breakdowns
        $aCountRequired = array('w'=>count($aLotteryData["winners"]),'n'=>count($aLotteryData["winnings"])); 
        
        // Keep a tally of entered infomation
        $aCountEntered = array('w'=>0,'n'=>0);
        
        // Because we may want to skip entering winners for certain lotteries
        // until we have all the data (lotto 6aus49)
        // we don't want to stop the process here, just flag that no info
        // has been added
        foreach ($aLotteryData["winners"] as $iWinners)
        {
            if($iWinners == ""){
                // Flag no info
                $bWinningsNull = true;
            }
            else 
            {
                // Count as entered
                $aCountEntered['w']++;
            }
        }
        
        // Same for the winnings
        foreach ($aLotteryData["winnings"] as $iWinNum)
        {
            if($iWinNum == ""){
                // Flag no info
                $bWinningsNull = true;
            }
            else 
            {
                // Count as entered
                $aCountEntered['n']++;
            }
        }
        
        // We now check the prize breakdowns that have been entered
        // We only raise an error if at least one piece of information has been entered
        // and only if not all entries have been made
        if (($aCountEntered['w'] > 0 && $aCountEntered['w'] != $aCountRequired['w']) 
         || ($aCountEntered['w'] > 0 &&$aCountEntered['n'] != $aCountRequired['n']))
        {
            $oErrors->addError("All prize breakdown information is required");
            $bValid = false;
        }
        

        
        if(!$aLotteryData["next_jackpot_amount"]){
            $oErrors->addError("Next jackpot amount is required");
            $bValid = false;           
        }
        if(!$aLotteryData["checkerid"]){
            $oErrors->addError("Checker id is required");
            $bValid = false;
        }
        if(!$aLotteryData["security_hash"]){
            $oErrors->addError("Security hash is required");
            $bValid = false;
        }
        
        // Check that the supplied security hash matches the hash made by the other details supplied
        if (!LotteryResultsAdmin::checkSecurityHash($aLotteryData["security_hash"], $aLotteryData["draw"], $aLotteryData["lotteryid"], $aLotteryData['checkerid'], false)){
            $oErrors->addError("Security hash does not match with supplied data");
            $bValid = false;                                   
        }
        
        // Check drawn numbers
        foreach ( $aLotteryData['number'] as $key => $value )
        {
            // Temporaily cast to integer
            $iIntVal = (int) $value;
            
            // If casted value is less than zero, or blank
            if ( $iIntVal < 0 || $value == '' )
            {
                $bNumError = true;
                $bValid = false;
            }

            switch ($aLotteryData["lotteryid"]) {
                case 26 :
                case 27 :
                case 28 :
                  // Just store the uncasted value without any zero padding 
                  // if it's super 6, spiel 77 or Clucks
                  $aLotteryData['number'][$key] = $value;
                break;
            
                default :
                  $aLotteryData['number'][$key] = str_pad($iIntVal,2,"0", STR_PAD_LEFT);
            }
            
        }
        
        if ($bNumError == true)
        {
            $oErrors->addError("All lottery numbers are required");
        }
        
        $aWinnings = array();
        
        // Get all entered information into an array
        foreach ( $aLotteryData['winnings'] as $key=>$value )
        {
            $aWinnings[$key] = array(
                                    'prize' => $value,
                                    'match' => $aLotteryData['match'][$key],
                                    'bonus' => $aLotteryData['bonus'][$key],
                                    'winners' => $aLotteryData['winners'][$key]
                                    );
        }
        
        if ( $bValid )
        {

            
            // At this stage, all supplied lottery results should be OK, so add to the row of the checker
            $bValid = LotteryResultsAdmin::addResultsToCheckingTable($aLotteryData["security_hash"], 
                                                                     $aLotteryData['number'], 
                                                                     $aWinnings,
                                                                     $aLotteryData['next_jackpot_amount'],
                                                                     $aLotteryData['next_jackpot_rollover'],
                                                                     $bWinningsNull);

            // If there's no problem with the data...
            if ( $bValid )
            {
                // Redirect to the screen to check validity
                $redirect = "../lottery-results-check.php?sh=" . $aLotteryData["security_hash"] . "&l=" . $aLotteryData["lotteryid"] ."&c=" . $aLotteryData["checkerid"] ."&d=" . $aLotteryData['draw'] . "&tablename=" . $sTablename;
            }
            else
            {
                // Redirect back to the entry screen via a seperate security object
                // to the original one
                
                $sSecurityHash = $aLotteryData["security_hash"];
                
                // Need to serialize the winnings data and implode the numbers
                // before storing in session
                $aLotteryData['number'] = implode("|", $aLotteryData['number']);
                
                $aWinnings = serialize($aWinnings);              
                $aLotteryData['winnings'] = $aWinnings;
                
                
                $_SESSION[$sSecurityHash . "formdata"] = $aLotteryData;
                $_SESSION["blablaformdata"] = $aLotteryData;
                header("Location: $detailsredirect");
                exit;
            }
        }
        else
        {
            // Redirect back to the entry screen via a seperate security object
            // to the original one

            $sSecurityHash = $aLotteryData["security_hash"];

            // Need to serialize the winnings data and implode the numbers
            // before storing in session
            $aLotteryData['number'] = implode("|", $aLotteryData['number']);

            $aWinnings = serialize($aWinnings);              
            $aLotteryData['winnings'] = $aWinnings;


            $_SESSION[$sSecurityHash . "formdata"] = $aLotteryData;
            $_SESSION["blablaformdata"] = $aLotteryData;
            header("Location: $detailsredirect");
            exit;
        }
        break;
    }
    
    //echo ("BValid is " . var_export($bValid));
    
    //print_d($oErrors);
        
    //die();
    
    if(!$bValid){

        header("Location: $detailsredirect");
        exit;
    }
    else{
        header("Location: $redirect");
        exit;
    }