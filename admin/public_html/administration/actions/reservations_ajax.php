<?php
chdir('..');
        include("../system/includes/autoload.php");
	include("security/checkauth.php");
$iBookingItemID = $_GET['id'];
$sPreviousPage = $_GET['pp'];
$sTablename = $_GET['tablename'];
switch($_GET["a"]){
	case "sendWinnerEmail":
			$aTicket = TicketsAdmin::getTicket($iBookingItemID);
			if ( $aTicket['draw_processed'] && $aTicket['win_title'] )
			{
				$bValid = false;
				if ( $aTicket['paid'] )
				{
					$bValid = EmailAdmin::sendLotteryWonReceiptEmail($aTicket['firstname'], $aTicket['lastname'], $aTicket['email'], $aTicket['numbers'], $aTicket['currency'] . " " . number_format($aTicket['winning_amount'],2) , $aTicket['draw_date'], $aTicket['lottery_title'], $aTicket['win_title'], $aTicket['ticket_price'], $aTicket['bookingreference'], $aTicket['winning_reference'], $aTicket['transaction_date'], $aTicket['transaction_amount'] );
				}
				else
				{
					$bValid = EmailAdmin::sendLotteryWonEmail($aTicket['firstname'], $aTicket['lastname'], $aTicket['email'], $aTicket['numbers'], $aTicket['currency'] . " " . number_format($aTicket['winning_amount'],2), $aTicket['draw_date'], $aTicket['lottery_title'], $aTicket['win_title'], $aTicket['ticket_price'], $aTicket['bookingreference']);
				}
				if ( $bValid )
				{
					if ( LotteryResultsAdmin::setWinnerEmailSent($iBookingItemID) )
					{
						echo "Email Sent Succesfuly.{messagetype}achtungSuccess{/messagetype}";
					}
					else
					{
						echo "Email Sent. Ticket updating failed.{messagetype}achtungFail{/messagetype}";
					}
				}
				else
				{
					echo "Email Delivery Failed.{messagetype}achtungFail{/messagetype}";
				}
			}
			else
			{
				echo "Problem encountered while trying to send winner email. Some fields are invalid.{messagetype}achtungFail{/messagetype}";
			}
		break;
	case "processTransaction":
			$aTicket = TicketsAdmin::getTicket($iBookingItemID);
			$fTransactionAmount = $_POST['amount'];
			if ( $aTicket['win_title'] && is_numeric($fTransactionAmount) && $fTransactionAmount > 0 )
			{
				if ( LotteryResultsAdmin::addWinTransaction($aTicket['member_id'], $iBookingItemID, $aTicket['lottery_id'], $fTransactionAmount, $aTicket['lottery'] . " - " . $aTicket['win_title'] ) )
				{
					echo "Transaction Succesful.{messagetype}achtungSuccess{/messagetype}";
				}
				else
				{
					echo "Transaction Failed.{messagetype}achtungFail{/messagetype}";
				}
			}
			else
			{
				echo "Problem encountered while trying to process transaction. Some Fields are invalid.{messagetype}achtungFail{/messagetype}";
			}
			return true;
		break;
	case "getEmail":
			$aTicket = TicketsAdmin::getTicket($iBookingItemID);
			echo getBookingMenu($iBookingItemID, $sTablename, $sPreviousPage);
			$sOutput = "
					<form name='transaction' action='actions/reservations_ajax.php?a=sendWinnerEmail&id={$iBookingItemID}&tablename={$sTablename}&pp={$sPreviousPage}' method='post' class='ajaxform'>
						<table class='detailsform right'>
							<tr>
								<th colspan='2' style='font-weight:bold;font-size:10pt; text-align:left; padding-left:20px;'>Transaction Details</th>
							</tr>";
			if ( date("Ymd", strtotime($aTicket['draw_date'])) <= date("Ymd") )
			{
				$sOutput .=
				"
					<tr>
						<th style='width:120px;'>Draw Processed</th>
						<td>";
					$sOutput .= (($aTicket['draw_processed'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
					</tr>
					<tr>
						<th style='width:120px;'>Client Emailed</th>
						<td>";
					$sOutput .= (($aTicket['emailed'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
					</tr> ";
				if ( $aTicket['draw_processed'] && $aTicket['win_title'] )
				{
					$sOutput .= "
						<tr>
							<th>Won</th>
							<td>{$aTicket['win_title']}</td>
						</tr>
						<tr>
							<th>Amount Won</th>
							<td>{$aTicket['currency']} " . number_format($aTicket['winning_amount'],2) . "</td>
						</tr>
						<tr>
							<th>Transaction Amount</th>
							<td>EUR " . number_format($aTicket['transaction_amount'],2) . "</td>
						</tr>";
					if ($aTicket['paid'])
					{
						$sOutput .= "
						<tr>
							<th>Transaction</th>
							<td>{$aTicket['winning_transaction']}</td>
						</tr>
						<tr>
							<th>Transaction Reference</th>
							<td>{$aTicket['winning_reference']}</td>
						</tr>
						<tr>
							<th>Transaction Date</th>
							<td>{$aTicket['transaction_date']}</td>
						</tr>";
					}
					$sOutput .= "<tr>
							<th>Send Winning Email</th>
							<td><input type='submit' value='";
					$sOutput .= (($aTicket['emailed'])?'Re-Send':'Send');
					$sOutput .=" Email to Winner'></td></tr>";
				}
				else
				{
					$sOutput .= "
						<tr>
							<th>Won</th>
							<td>{$aTicket['draw_processed']}</td>
						</tr>
						<tr>
							<th>Emailed</th>
							<td>{$aTicket['losers_emailed']}</td>
						</tr>
						";
				}
			}
			else
			{
				$sOutput .= "<tr>
								<th style='width:120px;'>Status</th>
								<td>Not yet drawn</td>
							</tr>";
			}
			$sOutput .= "</table>
					</form>";
			displayOutput($sOutput);
		break;
	case "getTransaction":
			$aTicket = TicketsAdmin::getTicket($iBookingItemID);
			echo getBookingMenu($iBookingItemID, $sTablename, $sPreviousPage);
			$sCurrentConversionRate = CurrencyAdmin::convertToBase($aTicket['winning_amount'], $aTicket['currency_id']);
			$sOutput = "
					<form name='transaction' action='actions/reservations_ajax.php?a=processTransaction&id={$iBookingItemID}&tablename={$sTablename}&pp={$sPreviousPage}' method='post' class='ajaxform'>
						<table class='detailsform right'>
							<tr>
								<th colspan='2' style='font-weight:bold;font-size:10pt; text-align:left; padding-left:20px;'>Transaction Details</th>
							</tr>";
			if ( date("Ymd", strtotime($aTicket['draw_date'])) <= date("Ymd") )
			{
				$sOutput .=
				"
					<tr>
						<th style='width:120px;'>Status</th>
						<td>";
					$sOutput .= (($aTicket['draw_processed'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
					</tr> ";
				if ( $aTicket['draw_processed'] && $aTicket['win_title'] )
				{
					$sOutput .= "
						<tr>
							<th>Won</th>
							<td>{$aTicket['win_title']}</td>
						</tr>
						<tr>
							<th>Amount Won</th>
							<td>{$aTicket['currency']} " . number_format($aTicket['winning_amount'],2) . "</td>
						</tr>
						<tr>
							<th>Transaction Amount</th>";
					if ($aTicket['paid'])
					{

						$sOutput .= "	<td>EUR " . number_format($aTicket['transaction_amount'],2) . "</td>
						</tr>
						<tr>
							<th>Transaction</th>
							<td>{$aTicket['winning_transaction']}</td>
						</tr>
						<tr>
							<th>Transaction Reference</th>
							<td>{$aTicket['winning_reference']}</td>
						</tr>
						<tr>
							<th>Transaction Date</th>
							<td>{$aTicket['transaction_date']}</td>
						</tr>";
					}
					else
					{
						$sOutput .= "<td>EUR <input type='text' name='amount' value='" . number_format($aTicket['transaction_amount'],2) . "'></td></tr>
						<tr>
							<th>Current Echange Rate</th>
							<td>EUR " . number_format($sCurrentConversionRate,2) . "</td>
						</tr>
						<tr>
							<th>Winning Paid</th>
							<td><input type='submit' value='Process Transaction'></td></tr>";
					}
				}
				else
				{
					$sOutput .= "
						<tr>
							<th>Winner</th>
							<td>";
					$sOutput .= (($aTicket['win_title'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
						</tr>
						<tr>
							<th>Emailed</th>
							<td>{$aTicket['losers_emailed']}</td>
						</tr>
						";
				}
			}
			else
			{
				$sOutput .= "<tr>
								<th style='width:120px;'>Status</th>
								<td>Not yet drawn</td>
							</tr>";
			}
			$sOutput .= "</table>
					</form>";
			displayOutput($sOutput);
		break;
	case "getTicketSummary":
			$aTicket = TicketsAdmin::getTicket($iBookingItemID);
			echo getBookingMenu($iBookingItemID, $sTablename, $sPreviousPage);
			$sOutput = "<table class='detailsform right'>
							<tr>
								<th colspan='2' style='font-weight:bold;font-size:10pt; text-align:left; padding-left:20px;'>Ticket Details</th>
							</tr>
							<tr>
								<th style='width:120px;'>Booking Reference</th>
								<td>{$aTicket['bookingreference']}</td>
							</tr>
							<tr>
								<th>Booking Date</th>
								<td>{$aTicket['booking_date']}</td>
							</tr>
							<tr>
								<th>Status</th>
								<td>{$aTicket['status']}</td>
							</tr>
							<tr>
								<th>Lottery</th>
								<td>{$aTicket['lottery']}</td>
							</tr>
							<tr>
								<th>Draw Date</th>
								<td>{$aTicket['draw_date']}</td>
							</tr>
							<tr>
								<th>Numbers</th>
								<td>{$aTicket['numbers']}</td>
							</tr>
							<tr>
								<th>Paid Ticket Price</th>
								<td>&euro;{$aTicket['ticket_price']}</td>
							</tr>
							<tr>
								<th>Ticket Purchased</th>
								<td>{$aTicket['purchased']}</td>
							</tr>
							<tr>
								<th>Affiliate</th>
								<td>{$aTicket['affiliate_firstname']} {$aTicket['affiliate_lastname']}</td>
							</tr>
							<tr>
								<th>Promo Code</th>
								<td>{$aTicket['promo_code']}</td>
							</tr>
						</table>
						<Br/>
						<table class='detailsform right'>
							<tr>
								<th colspan='2' style='font-weight:bold;font-size:10pt; text-align:left; padding-left:20px;'>Member Details</th>
							</tr>
							<tr>
								<th style='width:120px;'>First Name</th>
								<td>{$aTicket['firstname']}</td>
							</tr>
							<tr>
								<th>Last Name</th>
								<td>{$aTicket['lastname']}</td>
							</tr>
							<tr>
								<th>E-Mail</th>
								<td>{$aTicket['email']}</td>
							</tr>
							<tr>
								<th>Contact #</th>
								<td>{$aTicket['contact']}</td>
							</tr>
							<tr>
								<th>City</th>
								<td>{$aTicket['city']}</td>
							</tr>
							<tr>
								<th>Country</th>
								<td>{$aTicket['country']}</td>
							</tr>
							<tr>
								<th>Join Date</th>
								<td>{$aTicket['join_date']}</td>
							</tr>
							<tr>
								<th>Account Balance</th>
								<td>&euro;{$aTicket['balance']}</td>
							</tr>
						</table>
						<br/>
						<table class='detailsform right'>
							<tr>
								<th colspan='2' style='font-weight:bold;font-size:10pt; text-align:left; padding-left:20px;'>Draw Details</th>
							</tr>";
			if ( date("Ymd", strtotime($aTicket['draw_date'])) <= date("Ymd") )
			{
				$sOutput .=
				"
					<tr>
						<th style='width:120px;'>Status</th>
						<td>";
					$sOutput .= (($aTicket['draw_processed'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
					</tr> ";
				if ( $aTicket['draw_processed'] && $aTicket['win_title'] )
				{
					$sOutput .= "
						<tr>
							<th>Won</th>
							<td>{$aTicket['win_title']}</td>
						</tr>
						<tr>
							<th>Amount Won</th>
							<td>{$aTicket['currency']} " . number_format($aTicket['winning_amount'],2) . "</td>
						</tr>
						<tr>
							<th>Transaction Amount</th>
							<td>EUR " . number_format($aTicket['transaction_amount'],2) . "</td>
						</tr>
						<tr>
							<th>Email Notification</th>
							<td>";
					$sOutput .= (($aTicket['emailed'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
						</tr>
						<tr>
							<th>Winning Paid</th>
							<td>";
					$sOutput .= (($aTicket['paid'])?'Yes':'<strong>No</strong>');
					$sOutput .= "</td>
						</tr>
						<tr>
							<th>Transaction</th>
							<td>{$aTicket['winning_transaction']}</td>
						</tr>
						<tr>
							<th>Transaction Reference</th>
							<td>{$aTicket['winning_reference']}</td>
						</tr>
						<tr>
							<th>Transaction Date</th>
							<td>{$aTicket['transaction_date']}</td>
						</tr>
					";
				}
				else
				{
					$sOutput .= "
						<tr>
							<th>Won</th>
							<td>{$aTicket['draw_processed']}</td>
						</tr>
						<tr>
							<th>Emailed</th>
							<td>{$aTicket['losers_emailed']}</td>
						</tr>
						";
				}
			}
			else
			{
				$sOutput .= "<tr>
								<th style='width:120px;'>Status</th>
								<td>Not yet drawn</td>
							</tr>";
			}
			$sOutput .= "</table>";
			displayOutput($sOutput);
		break;
	default:
			echo getBookingMenu($iBookingItemID, $sTablename, $sPreviousPage);
			$sOutput = "<div class='error' style='display:block;'><ul class='error'><li>Function not found</li></ul></div>";
			displayOutput($sOutput);

		break;
}


function displayOutput($sContent)
{
	?>
    <div id="ajaxContentDiv" style="width: 800px; height: 550px; padding-top: 20px; overflow: auto; text-align: left;">
		<?=$sContent?>
	</div>
    <?php
}

function getBookingMenu($iBookingItemID, $sTablename, $sPreviousPage){
		$sSelected = $_GET["a"];
		$aTicket = TicketsAdmin::getTicket($iBookingItemID);
		$sSelectedStyle = " style=\"border-bottom: 2px solid #000;\"";
		$sUnselectedStyle = " style=\"border-bottom: 2px solid #fff;\"";
		$sLoadingImg = "<img src=\'images/loadingclock.gif\' alt=\'Loading..\' style=\'margin-top: 150px\' />";
		$sDivToUpdate = '.sd_content';
		//Ticket Information
		if($sSelected == "getTicketSummary"){
			$sStyle = $sSelectedStyle;
		}
		else{
			$sStyle = $sUnselectedStyle;
		}
		$sOutput = "<a onclick=\"$('{$sDivToUpdate}').html('{$sLoadingImg}'); $('{$sDivToUpdate}').load('actions/reservations_ajax.php?a=getTicketSummary&id={$iBookingItemID}&tablename={$sTablename}&pp={$sPreviousPage}', loadControls); return false;\" ><img src=\"images/reservation_icons/details.gif\" alt=\"Booking Details\" {$sStyle}/></a> ";
		if ( $aTicket['win_title'] )
		{
			//Transaction Details
			if($sSelected == "getTransaction"){
				$sStyle = $sSelectedStyle;
			}
			else{
				$sStyle = $sUnselectedStyle;
			}
			$sOutput .="<a href=\"#\" onclick=\"$('{$sDivToUpdate}').html('{$sLoadingImg}'); $('{$sDivToUpdate}').load('actions/reservations_ajax.php?a=getTransaction&id={$iBookingItemID}&tablename={$sTablename}&pp={$sPreviousPage}', loadControls); return false;\" >";
			if(!$aTicket["paid"]){
				$sOutput .= "<img src=\"images/reservation_icons/preauth_off.gif\" alt=\"PreAuth\" {$sStyle}/>";
			}
			else{
				$sOutput .= "<img src=\"images/reservation_icons/preauth_on.gif\" alt=\"PreAuth\" {$sStyle}/>";
			}
			$sOutput .= "</a>";

			//Email Notification
			if($sSelected == "getEmail"){
				$sStyle = $sSelectedStyle;
			}
			else{
				$sStyle = $sUnselectedStyle;
			}
			$sOutput .="<a href=\"#\" onclick=\"$('{$sDivToUpdate}').html('{$sLoadingImg}'); $('{$sDivToUpdate}').load('actions/reservations_ajax.php?a=getEmail&id={$iBookingItemID}&tablename={$sTablename}&pp={$sPreviousPage}', loadControls); return false;\" >";
			if($aTicket["emailed"]){
				$sOutput .= "<img src=\"images/reservation_icons/voucher_off.gif\" alt=\"Voucher\" {$sStyle}/>";
			}
			else{
				$sOutput .= "<img src=\"images/reservation_icons/voucher_on.gif\" alt=\"Voucher\" {$sStyle}/>";
			}
			$sOutput .= "</a>";
		}
		return $sOutput;
	}
?>
