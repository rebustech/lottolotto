<?php
    chdir('..');
        include("../system/includes/autoload.php");
	include("security/checkauth.php");
	$action = $_GET["a"];
	$iId = $_GET["id"];
	$sTablename = $_GET["tablename"];
	$sLanguageTable = $_GET["languagetable"];
	$iLangId = $_GET["lid"];
	$iTranslationId = $_POST["translationid"];
	$sPreviousPage = urldecode($_GET["pp"]);

        $sHandler=$_GET['tablename'];
        if(class_exists($sHandler)) $oHandler=new $sHandler;


	if($_GET["oldqs"]){
		$sOldQueryString = unserialize(urldecode($_GET["oldqs"]));
	}
	if($sOldQueryString){
		$sPreviousPage .= "?qs=" . $sOldQueryString;
	}

	if($sPreviousPage){
		$redirect = "../" . $sPreviousPage . "?tablename=" . $sTablename;
		$detailsredirect = "../generic-details.php?tablename=$sTablename&id=$iId&a=edit&pp=" . $sPreviousPage;
	}
	else{
		$redirect = "../generic-listing.php?tablename=$sTablename";
		$detailsredirect = "../generic-details.php?tablename=$sTablename&id=$iId&a=edit";
	}

	$bValid = true;

	switch($action) {
		case "add":
			$aColumns = GenericTableAdmin::getTableColumns($sTablename);
			$aData = array();
			foreach($aColumns as $aCurrentCol){
					$sType = str_replace(")", "", $aCurrentCol["Type"]);
					$aType = explode("(", $sType);
					$sType = strtolower($aType[0]);
					$iTypeLength = $aType[1];
					$aCurrentCol['Field'] = (($aCurrentCol["Field"] == "tablename")?"tablename%ignoreme%":$aCurrentCol["Field"]);

					if($aCurrentCol["Extra"] != "auto_increment"){
						if($aCurrentCol["Null"] == "NO" && !isset($_POST[$aCurrentCol['Field']])){
							if($sType == "tinyint" && $iTypeLength <= 4){
								$aData[$aCurrentCol['Field']] = '0';
							}
							else{
								$oErrors->addError("You cannot leave '" . $aCurrentCol["Field"] . "' empty.");
								$bValid = false;
							}
						}
						else if(isset($_POST[$aCurrentCol['Field']])){
							if($sType == "tinyint"){
								if($iTypeLength <= 4){
									if(strtolower($_POST[$aCurrentCol['Field']]) == "on"){
										$aData[$aCurrentCol['Field']] = '1';
									}
									else{
										if(is_numeric($_POST[$aCurrentCol['Field']])){
											$aData[$aCurrentCol['Field']] = $_POST[$aCurrentCol['Field']];
										}
										else{
											$oErrors->addError("Numeric value required for '" . $aCurrentCol["Field"] . "' .");
											$bValid = false;
										}
									}
								}
							}
							elseif($sType == "int"){
								if(is_numeric($_POST[$aCurrentCol['Field']])){
									$aData[$aCurrentCol['Field']] = $_POST[$aCurrentCol['Field']];
								}
								elseif ( $aCurrentCol['Null'] == "NO" ){
									$oErrors->addError("Numeric value required for '" . $aCurrentCol["Field"] . "' .");
									$bValid = false;
								}
							}
							else{
								$aData[str_replace("%ignoreme%", "",$aCurrentCol['Field'])] = $_POST[$aCurrentCol['Field']];
							}
						}
					}
			}
			if($bValid){
				if($sTablename == "sections_cmn2"){
					if($_POST["isactive"]){
						$bIsActive = 1;
					}
					else{
						$bIsActive = 0;
					}
					SectionsAdmin::addSection($_POST["comment"], $_POST["url"], $bIsActive, $_POST["rank"]);
				}
				else{
					$iPageID = GenericTableAdmin::doGenericInsert($sTablename, $aData);
					if($iPageID && $sTablename == "pages_cmn"){
						//PageAdmin::addPageFile($_POST["url"],$_POST["filecontent"],$_POST["fk_section_id"], $iPageID);
						ScriptsAdmin::rebuildPagesTree();
					}
                                        $detailsredirect = "../generic-details.php?tablename=$sTablename&id=$iPageID&a=edit";
				}
			}
			break;
		case "delete":
			if($sLanguageTable){
				if (!empty($iId)) $bResult = GenericTableAdmin::doGenericDelete($iId, $sLanguageTable);
			}
			else{
				if($sTablename == "sections_cmn"){
					$sFolder = Menu::getSectionFolder($iId);
					SectionsAdmin::deleteSection($iId,$sFolder);
				}
				else if($sTablename == "multimedia_cmn"){
					MultimediaAdmin::deleteImage($iId);
				}
				else{
					if (!empty($iId)) $bResult = GenericTableAdmin::doGenericDelete($iId, $sTablename);
					if($bResult && $sTablename == "pages_cmn"){
						ScriptsAdmin::rebuildPagesTree();
						PageAdmin::deletePageFile($iId);
					}
				}
			}
                        $detailsredirect = "../generic-listing.php?tablename=$sTablename";
			break;
		case "edit":
			if($sLanguageTable && $iLangId){
				$detailsredirect .= "&languagetable={$sLanguageTable}&lid={$iLangId}";
				$aColumns = GenericTableAdmin::getTableColumns($sLanguageTable);
			}
			else{
				$aColumns = GenericTableAdmin::getTableColumns($sTablename);
			}
			$aData = array();
			foreach($aColumns as $aCurrentCol){
					$aCurrentCol['Field'] = (($aCurrentCol["Field"] == "tablename")?"tablename%ignoreme%":$aCurrentCol["Field"]);
					$sType = str_replace(")", "", $aCurrentCol["Type"]);
					$aType = explode("(", $sType);
					$sType = strtolower($aType[0]);
					$iTypeLength = $aType[1];

					if($aCurrentCol["Extra"] != "auto_increment"){
						if($aCurrentCol["Null"] == "NO" && !isset($_POST[$aCurrentCol['Field']]) ){
							if($sType == "tinyint" && $iTypeLength <= 4){
								$aData[$aCurrentCol['Field']] = '0';
							}
							else{
								$oErrors->addError("You cannot leave '" . $aCurrentCol["Field"] . "' empty.");
								$bValid = false;
							}
						}
						elseif(isset($_POST[$aCurrentCol['Field']])){
							if($sType == "tinyint"){
								if($iTypeLength <= 4){
									if(strtolower($_POST[$aCurrentCol['Field']]) == "on"){
										$aData[$aCurrentCol['Field']] = '1';
									}
									else{
										if(is_numeric($_POST[$aCurrentCol['Field']])){
											$aData[$aCurrentCol['Field']] = $_POST[$aCurrentCol['Field']];
										}
								        elseif ( $aCurrentCol['Null'] == "NO" ){
											$oErrors->addError("Numeric value required for '" . $aCurrentCol["Field"] . "' .");
											$bValid = false;
										}
									}
								}
							}
							elseif($sType == "int"){
								if(is_numeric($_POST[$aCurrentCol['Field']])){
									$aData[$aCurrentCol['Field']] = $_POST[$aCurrentCol['Field']];
								}
								elseif ( $aCurrentCol['Null'] == "NO" ){
									$oErrors->addError("Numeric value required for '" . $aCurrentCol["Field"] . "' .");
									$bValid = false;
								}
							}
							else{
								if (Validation::validateHTML($_POST[$aCurrentCol['Field']],$aErrors)){
									$aData[str_replace("%ignoreme%", "",$aCurrentCol['Field'])] = $_POST[$aCurrentCol['Field']];
								}
								else
								{
									$oErrors->addError("Invalid input found for '" . $aCurrentCol["Field"] . "'. " . print_r($aErrors,true));
									$bValid = false;
								}
							}
						}
					}
			}
			if($sTablename == "multimedia_cmn"){
				$fileselect = $_POST["fileselect"];
				switch ($fileselect) {
					//Update Filename
					case 2:
						$filename = $_POST["filename"];
						if (empty($filename)) {
							$oErrors->addError("You cannot leave the filename empty.");
							$bValid = false;
						}
					break;
					//Change File
					case 3:
						$filename = $_FILES['imagefile']['name'];
						if (empty($filename)) {
							$oErrors->addError("No file selected.");
							$bValid = false;
						}
					break;
				}
			}
			if($bValid){
				if($sLanguageTable && $iLangId){
					if($iTranslationId){
						$aPrimaryKeys = GenericTableAdmin::getPrimaryKeys($sLanguageTable);
						$sPrimaryKey = $aPrimaryKeys[0]['Column_name'];
						$sParams .= "$sPrimaryKey = $iTranslationId";
						GenericTableAdmin::doGenericUpdate($sLanguageTable, $aData, $sParams);
					}
					else{
						GenericTableAdmin::doGenericInsert($sLanguageTable, $aData);
					}
				}
				else{
					$aIds = explode("_", $iId);
					if($sTablename == "sections_cmn2"){
						$sCurrentFolder = Menu::getSectionFolder($iSectionID);
						if($_POST["isactive"]){
							$bIsActive = 1;
						}
						else{
							$bIsActive = 0;
						}
						SectionsAdmin::editSection($iSectionID,$_POST["comment"],$sPath,$_POST["url"], $sCurrentFolder, $bIsActive, $_POST["rank"]);
					}
					else{
						$aPrimaryKeys = GenericTableAdmin::getPrimaryKeys($sTablename);
						$sParams = '';
						foreach($aPrimaryKeys as $counter => $sCurrentKey){
							$sCurrentKey =  $sCurrentKey['Column_name'];
							if($sParams != ''){
								$sParams .= ' AND ';
							}
							$sParams .= "{$sCurrentKey} = ";
							if ( is_int($aIds[$counter]) )
							{
								$sParams .= $aIds[$counter];
							}
							else
							{
								$sParams .= "'" . $aIds[$counter] . "'";
							}
						}


						$bResult = GenericTableAdmin::doGenericUpdate($sTablename, $aData, $sParams);
						if($bResult && $sTablename == "pages_cmn"){
							//PageAdmin::updatePageFile($_POST["url"], $_POST["filecontent"], $iId, $_POST["fk_section_id"]);
							ScriptsAdmin::rebuildPagesTree();
						}
						else if($bResult && $sTablename == "multimedia_cmn"){
							switch ($fileselect) {
								//Update Filename
								case 2:
									$bValid = MultimediaAdmin::renameMM($iId, $filename);
								break;
								//Change File
								case 3:
									$bValid = MultimediaAdmin::changeMMFile($iId, $_FILES['imagefile']['tmp_name'], $_POST['fk_mmtype_id'], $filename);
								break;
							}
						}
					}
				}
			}
                        //Hook in the new stuff

                        if(method_exists($oHandler,'afterGenericFormPost')){
                            echo $oHandler->afterGenericFormPost();
                        }
			break;

	}
	if(!$bValid){
		$_SESSION[$oSecurityObject->getsAdminType() . "formdata"] = $aData;
		header("Location: $detailsredirect");
		exit;
	}
	else{
		header("Location: $detailsredirect");
		exit;
	}

?>