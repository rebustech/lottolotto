<?php
if($_GET["tablename"] != "admin_users"){
	echo "Unauthorised Access. Please contact an Administrator if the problem persists.";
	exit;
}
    chdir('..');
        include("../system/includes/autoload.php");
	include("security/checkauth.php");
?>
<?php
	$action = $_GET["a"];

	$iId = $_GET["id"];
	$iWebsiteID = $_GET["websiteid"];
	$sTablename = $_GET["tablename"];
	$sLanguageTable = $_GET["languagetable"];
	$iLangId = $_GET["lid"];
	$iTranslationId = $_POST["translationid"];
	$sPreviousPage = urldecode($_GET["pp"]);
	$aUserData = array();

	$redirect = "../users.php?tablename=admin_users";
	$detailsredirect = "../users-details.php?tablename=admin_users&pp=users.php";

	$bValid = true;

	switch($action) {
		case "add":
				$bValid = true;

				$aUserData["username"] = $_POST["username"];
				if(!$aUserData["username"]){
					$oErrors->addError("Username is required");
					$bValid = false;
				}
				$aUserData["password"] = $_POST["passwd"];
				$aUserData["password2"] = $_POST["passwd2"];
				if(!$aUserData["password"]){
					$oErrors->addError("Password is required");
					$bValid = false;
				}
				else if($aUserData["password"] != $aUserData["password2"]){
					$oErrors->addError("Passwords do not match!");
					$bValid = false;
				}
				else{
					unset($aUserData["password2"]);
				}

				$aUserData["pin"] = $_POST["pin"];
				$aUserData["pin2"] = $_POST["pin2"];
				if(!$aUserData["pin"]){
					$oErrors->addError("PIN is required");
					$bValid = false;
				}
				else if($aUserData["pin"] != $aUserData["pin2"]){
					$oErrors->addError("PINs do not match!");
					$bValid = false;
				}
				else{
					unset($aUserData["pin2"]);
				}

				$aUserData["firstname"] = $_POST["firstname"];
				if(!$aUserData["firstname"]){
					$oErrors->addError("First name is required");
					$bValid = false;
				}

				$aUserData["lastname"] = $_POST["lastname"];
				if(!$aUserData["lastname"]){
					$oErrors->addError("Last name is required");
					$bValid = false;
				}

				$aUserData["email"] = $_POST["email"];
				if(!$aUserData["email"]){
					$oErrors->addError("Email address is required");
					$bValid = false;
				}

				$aUserData["tel1"] = $_POST["telephone1"];
				if(!$aUserData["tel1"]){
					$oErrors->addError("Telephone is required");
					$bValid = false;
				}

				$aUserData["tel2"] = $_POST["telephone2"];

				$aUserData["fk_admintype_id"] = $_POST["fk_admintype_id"];
				if ( $aUserData["fk_admintype_id"] == SecurityAdmin::AFFILIATE )
				{
					$detailsredirect = "../affiliate-details.php?tablename=admin_users&pp=affiliates.php";
					$aAffiliateData = array();

					$aAffiliateData["address1"] = $_POST["address1"];
					if(!$aAffiliateData["address1"]){
						$oErrors->addError("Address is required");
						$bValid = false;
					}
					$aAffiliateData["address2"] = $_POST["address2"];

					$aAffiliateData["zipcode"] = $_POST["zip"];
					if(!$aAffiliateData["zipcode"]){
						$oErrors->addError("ZIP Code is required");
						$bValid = false;
					}

					$aAffiliateData["city"] = $_POST["city"];
					if(!$aAffiliateData["city"]){
						$oErrors->addError("City is required");
						$bValid = false;
					}

					$aAffiliateData["country"] = $_POST["country"];
					if(!$aAffiliateData["country"]){
						$oErrors->addError("Country is required");
						$bValid = false;
					}
					$aAffiliateData["fax"] = $_POST["fax"];
					$aAffiliateData["company"] = $_POST["company"];
					$aAffiliateData["vatno"] = $_POST["vat"];
					$aAffiliateData["web"] = $_POST["web"];
					if(!$aAffiliateData["web"]){
						$oErrors->addError("Website is required.");
						$bValid = false;
					}
					$aAffiliateData["commission"] = $_POST["commission"];
					if(!$aAffiliateData["commission"]){
						$oErrors->addError("Commission Rate is required.");
						$bValid = false;
					}

				}
				$aUserData["isactive"] = 0;

			if($bValid){
				$aUserData["password"] = Encryption::Encrypt($aUserData["password"]);
				$aUserData["pin"] = Encryption::Encrypt($aUserData["pin"]);
				$bResult = GenericTableAdmin::doGenericInsert($sTablename, $aUserData);
				if ( $bResult && $aUserData["fk_admintype_id"] == SecurityAdmin::AFFILIATE )
				{
					$aAffiliateData['fk_user_id'] = $bResult;
					$bResult = GenericTableAdmin::doGenericInsert("affiliates", $aAffiliateData);
				}
			}
			break;
		case "delete":
				if (!empty($iId)) $bResult = GenericTableAdmin::doGenericDelete($iId, $sTablename);
			break;
	}
	if(!$bValid){
		$_SESSION[$oSecurityObject->getsAdminType() . "formdata"] = ($aUserData["fk_admintype_id"] == SecurityAdmin::AFFILIATE)?array_merge($aUserData,$aAffiliateData):$aUserData;
		header("Location: $detailsredirect");
		exit;
	}
	else{
		header("Location: $redirect");
		exit;
	}

?>