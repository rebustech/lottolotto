<?php

chdir('..');
include("../system/includes/autoload.php");
include("security/checkauth.php");
$iId = $_GET["id"];
$sIdField = $_GET["idfield"];
$iWebsiteID = $_GET["websiteid"];
$sTablename = $_GET["tablename"];
$sLanguageTable = $_GET["languagetable"];
$iLangID = $_GET["lid"];

switch($_GET["a"]){
	case "toggleBooleanField":
			$sField = $_GET["field"];
			$bValue = $_GET["value"];
			if($bValue == "enable"){
				$sValue = "True";
				$bToggleValue = 1;
			}
			elseif($bValue == "disable"){
				$sValue = "False";
				$bToggleValue = 0;
			}
			$aData = array($sField => $bToggleValue);
			if($iLangID && $sLanguageTable){
				if($sIdField == "translationid"){
					$sIdField = "id";
					$sParams = "$sIdField = '$iId'";
				}
				elseif(strstr($sIdField, "|")){
					$aIdFields = explode('|', $sIdField);
					$aIds = explode('_', $iId);
					$sParams = "";
					foreach($aIdFields as $iKey => $sCurrentIdField){
						if($sParams .= ""){
							$sParams .= " AND ";
						}
						$sParams .= "$sCurrentIdField = '" . $aIds[$iKey] . "' ";
					}
				}
				else{
					$sParams = "$sIdField = '$iId'";
				}
				$bResponse = GenericTableAdmin::doGenericUpdate($sLanguageTable, $aData, $sParams);
				$iId .= "(translationid)";
			}
			else{
				if(strstr($sIdField, "|")){
					$aIdFields = explode('|', $sIdField);
					$aIds = explode('_', $iId);
					$sParams = "";
					foreach($aIdFields as $iKey => $sCurrentIdField){
						if($sParams .= ""){
							$sParams .= " AND ";
						}
						$sParams .= "$sCurrentIdField = '" . $aIds[$iKey] . "' ";
					}
				}else{
					$sParams = "$sIdField = '$iId'";
				}
				$bResponse = GenericTableAdmin::doGenericUpdate($sTablename, $aData, $sParams);
			}

			if($bResponse == 1){
				echo "$sField #{$iId} set to {$sValue}.";
			}
			else{
				echo "Problem encountered. {messagetype}achtungFail{/messagetype}";
			}
		break;
	case "genericTableSearch":
			$sQuery = $_GET["query"];
			$sSearchField = $_GET["searchfield"];
			$sFilters = urldecode($_GET["filterstring"]);
			$sDetailsPage = ($_GET["ep"])?$_GET["ep"]:"generic-details.php";
			$sActionsPage = ($_GET["ap"])?$_GET["ap"]:"generic_actions.php";
			$sPreviousPage = ($_GET["pp"])?$_GET["pp"]:"generic-listing.php";
			if($sQuery){
				$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct.{$sSearchField} LIKE '%{$sQuery}%'");
			}
			$_SESSION[$oSecurityObject->getsAdminType() . "aQuickFilters"][$sTablename] = array("field"=> $sSearchField, "query" => $sQuery);
			$iLangId = $iLangID;
			$sLanguageTablename = $sLanguageTable;

			if(!$iLangId){
				echo GenericTableAdmin::createGenericDataTable($sTablename, $iLangId, $sFilters, $sPreviousPage, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"], $sDetailsPage, $sActionsPage);
			}
			else{
				$aLanguageData = GenericTableAdmin::getLanguageRecords($sTablename, $sLanguageTablename, $iLangId, $sFilters, "", $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);
				$aLanguageData["pp"] = "generic-listing.php";
				echo GenericTableAdmin::createDataTable(
				array("ID|tiny|centeralign", "Comment", "Translated Title", "Edit|tiny|centeralign", "Delete|tiny|centeralign"),
				array("id|centeralign", "comment", "title|empty:<span style=\"color: #f00\">No translation</span>", "id|link:generic-details.php?pp=" . $sFileName . "&a=edit&lid=" . $iLangId . "&tablename=" . $sTablename . "&languagetable=" . $sLanguageTable . "|centeralign",
					  "translationid|link:actions/generic_actions.php?pp=" . $sFileName . "&a=delete&lid=" . $iLangId . "&tablename=" . $sTablename . "&languagetable=" . $sLanguageTable . "|centeralign"),
				$aLanguageData, $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]);
			}
		break;
		case "tableviewSearch":
			$sQuery = $_GET["query"];
			$sSearchField = $_GET["searchfield"];
			$sFilters = "";
			if($sQuery){
				$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct.{$sSearchField} LIKE '%{$sQuery}%'");
			}
			echo GenericTableAdmin::createGenericTableView($sTablename, $iLangID, $sFilters);

		break;
		case "saveRelationshipList":
			$iID = $_GET["id"];
			$sRelTablename = $_GET['reltable'];
			$sPrimaryKey = $_GET['primary'];
			$sField = $_GET['secondary'];
			if( !( ( ( $_GET["tablename"] == "admin_pages" ) || ( $_GET["tablename"] == "admin_filters") ) && $sRelTablename == "admin_r_pages_filters" ) ) {
				echo "Unauthorised Access. Please contact an Administrator if the problem persists.";
				exit;
			}
			GenericTableAdmin::deleteGenericRelations($sRelTablename, $sPrimaryKey, $iID);
			$aFields = $_POST["fields"];
			if (is_array($aFields)) {
				foreach ($aFields as $iFieldID)
				{
					$aData = array();
					$aData[$sPrimaryKey] = $iID;
					$aData[$sField] = $iFieldID;
					GenericTableAdmin::doGenericInsert($sRelTablename,$aData);
				}
			}
			echo "Relationship saved ok.";
		break;
	}
?>
