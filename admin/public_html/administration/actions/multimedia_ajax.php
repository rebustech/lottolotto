<?php
include("../security/checkauth.php");
$iWebsiteID = $_GET["websiteid"];
$iSectionID = $_GET["sectionid"];
$iPageID = $_GET["pid"];
$iProductID = $_GET["pid"];
$iListingCategoryID = $_GET["lcid"];

$iLangID = $_GET["lid"];
switch($_GET["a"]){
	//PAGE MM
	case "searchPageMMByComment":
			if($_GET["tablename"] != "r_mm_pages"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];
			$sWebsite = strtolower(str_replace(" ","",Websites::getWebsiteTitleByID($iWebsiteID)));
			if(!$sQuery){
				$aLibraryMM = MultimediaAdmin::getLatestMMPageListForSearch($iMMTypeID, $iPageID, $iWebsiteID, $sWebsite);
			}
			else{
				$aLibraryMM = MultimediaAdmin::getMMPageListForSearch($sQuery,$iMMTypeID, $iPageID, $iWebsiteID, $sWebsite, "comment");	
			}
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;
	case "searchPageMMByFilename":
			if($_GET["tablename"] != "r_mm_pages"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];
			$sWebsite = strtolower(str_replace(" ","",Websites::getWebsiteTitleByID($iWebsiteID)));
			if(!$sQuery){
				$aLibraryMM = MultimediaAdmin::getLatestMMPageListForSearch($iMMTypeID, $iPageID, $iWebsiteID, $sWebsite);
			}
			else{
				$aLibraryMM = MultimediaAdmin::getMMPageListForSearch($sQuery,$iMMTypeID, $iPageID, $iWebsiteID, $sWebsite, "filename");
			}
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;
	case "savePageMM":
		if($_GET["tablename"] != "r_mm_pages"){
			echo "Insufficient pemissions";
			exit;
		}
		$aPageMM = $_GET["mm"];
		$dFromDate = $_GET["fromdate"];
		$dToDate = $_GET["todate"];
		$iMMTypeID = $_GET["mmtypeid"];
		MultimediaAdmin::removePageMMByMMTypeID($iMMTypeID, $iPageID, $iWebsiteID);
		if($aPageMM){
			foreach($aPageMM as $iRank => $iCurrentMMID){
				PageAdmin::addMMPage($iWebsiteID, $iPageID,$iCurrentMMID, $dFromDate[$iRank], $dToDate[$iRank], $iRank);
			}
		}
		echo "Page multimedia saved succesfully. {messagetype}achtungSuccess{/messagetype}";
		break;
		//PRODUCT MM
		case "searchProductMMByComment":
			if($_GET["tablename"] != "r_mm_products"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];;
			if(!$sQuery){
				$aLibraryMM = MultimediaAdmin::getLatestMMProductListForSearch($iMMTypeID, $iProductID);
			}
			else{
				$aLibraryMM = MultimediaAdmin::getMMProductListForSearch($sQuery,$iMMTypeID, $iProductID, "comment");	
			}
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;
	case "searchProductMMByFilename":
			if($_GET["tablename"] != "r_mm_products"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];;
			if(!$sQuery){
				$aLibraryMM = MultimediaAdmin::getLatestMMProductListForSearch($iMMTypeID, $iProductID);
			}
			else{
				$aLibraryMM = MultimediaAdmin::getMMProductListForSearch($sQuery,$iMMTypeID, $iProductID, "filename");	
			}
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;
	case "saveProductMM":
		if($_GET["tablename"] != "r_mm_products"){
			echo "Insufficient pemissions";
			exit;
		}
		$aProductMM = $_GET["mm"];
		MultimediaAdmin::removeProductMMByProductID($iProductID);
		if($aProductMM){
			foreach($aProductMM as $iRank => $iCurrentMMID){ 
				ProductsAdmin::addMMProduct($iProductID,$iCurrentMMID,$iRank);
			}
		}
		echo "Product multimedia saved succesfully. {messagetype}achtungSuccess{/messagetype}";
		break;
	case "searchMMByComment":
			if($_GET["tablename"] != "multimedia_cmn"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];
			$aLibraryMM = MultimediaAdmin::getMMList($iWebsiteID, $iMMTypeID, $sQuery, 'comment', $iLangID, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMListHtml($aLibraryMM, $sFolder, $iMMTypeID, $iLangID);
		break;
	case "searchMMByFilename":
			if($_GET["tablename"] != "multimedia_cmn"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];
			$aLibraryMM = MultimediaAdmin::getMMList($iWebsiteID, $iMMTypeID, $sQuery, 'file', $iLangID, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMListHtml($aLibraryMM, $sFolder, $iMMTypeID, $iLangID);
		break;
	case "saveWebsiteMM":
		if($_GET["tablename"] != "r_mm_websites"){
			echo "Insufficient pemissions";
			exit;
		}
		$aPageMM = $_GET["mm"];
		$dFromDate = $_GET["fromdate"];
		$dToDate = $_GET["todate"];
		$iMMTypeID = $_GET["mmtypeid"];
		MultimediaAdmin::removeWebsiteMMByMMTypeID($iMMTypeID, $iWebsiteID);
		if($aPageMM){
			foreach($aPageMM as $iRank => $iCurrentMMID){
				MultimediaAdmin::addMMWebsite($iWebsiteID, $iCurrentMMID, $dFromDate[$iRank], $dToDate[$iRank], $iRank);
			}
		}
		echo "Page multimedia saved succesfully. {messagetype}achtungSuccess{/messagetype}";
		break;		
	case "searchWebsiteMMByFilename":
			if($_GET["tablename"] != "r_mm_websites"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];;
			$aLibraryMM = MultimediaAdmin::getMMList($iWebsiteID, $iMMTypeID, $sQuery, 'file', $iLangID, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;	
	case "searchWebsiteMMByComment":
			if($_GET["tablename"] != "r_mm_websites"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];;
			$aLibraryMM = MultimediaAdmin::getMMList($iWebsiteID, $iMMTypeID, $sQuery, 'comment', $iLangID, $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;

	case "searchListingCategoryMMByComment":
			if($_GET["tablename"] != "r_mm_listingcategories"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];
			if(!$sQuery){
				$aLibraryMM = MultimediaAdmin::getLatestMMListingCategoryListForSearch($iMMTypeID, $iListingCategoryID, $iWebsiteID);
			}
			else{
				$aLibraryMM = MultimediaAdmin::getMMListingCategoriesListForSearch($sQuery,$iMMTypeID, $iListingCategoryID, $iWebsiteID, "comment");	
			}
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;
	case "searchListingCategoryMMByFilename":
			if($_GET["tablename"] != "r_mm_listingcategories"){
				echo "Insufficient pemissions";
				exit;
			}
			$iMMTypeID = $_GET["typeid"];
			$sQuery = $_GET["query"];
			$sWebsite = strtolower(str_replace(" ","",Websites::getWebsiteTitleByID($iWebsiteID)));
			if(!$sQuery){
				$aLibraryMM = MultimediaAdmin::getLatestMMListingCategoryListForSearch($iMMTypeID, $iListingCategoryID, $iWebsiteID);
			}
			else{
				$aLibraryMM = MultimediaAdmin::getMMListingCategoriesListForSearch($sQuery,$iMMTypeID, $iListingCategoryID, $iWebsiteID, "filename");
			}
			$sFolder = MultimediaAdmin::getFolderByID($iMMTypeID);
			echo buildMMSortableHtml($aLibraryMM, $sFolder, "orange");
		break;
	case "saveListingCategoryMM":
		if($_GET["tablename"] != "r_mm_listingcategories"){
			echo "Insufficient pemissions";
			exit;
		}
		$aListingCategoryMM = $_GET["mm"];
		$dFromDate = $_GET["fromdate"];
		$dToDate = $_GET["todate"];
		$iMMTypeID = $_GET["mmtypeid"];
		MultimediaAdmin::removeListingCategoryIDMMByMMTypeID($iMMTypeID, $iListingCategoryID, $iWebsiteID);
		if($aListingCategoryMM){
			foreach($aListingCategoryMM as $iRank => $iCurrentMMID){
				ListingsAdmin::addMMListingCategory($iWebsiteID, $iListingCategoryID,$iCurrentMMID, $dFromDate[$iRank], $dToDate[$iRank], $iRank);
			}
		}
		echo "Page multimedia saved succesfully. {messagetype}achtungSuccess{/messagetype}";
		break;

}

function buildMMListHtml($aData, $sFolder, $iMMTypeID, $iLangID = 0){
	$sOutput = "";
	if ($iMMTypeID == 10 || $iMMTypeID == 9){ 
		$sThumbFile = "../system/media/en/images/{$sFolder}/thumbs/"; 
	}
	elseif($iMMTypeID == MultimediaAdmin::PDFDOCS){
		$sThumbFile = "images/multimedia_icons/pdf.jpg?f=";
	}
	elseif($iMMTypeID == MultimediaAdmin::WORDDOCS){
		$sThumbFile = "images/multimedia_icons/word.jpg?f=";
	}
	elseif($iMMTypeID == MultimediaAdmin::INTERNALLINK || $iMMTypeID == MultimediaAdmin::EXTERNALLINKS){
		$sThumbFile = "images/multimedia_icons/link.jpg?f=";
	}
	elseif($iMMTypeID == MultimediaAdmin::VIDEOS){
		$sThumbFile = "images/multimedia_icons/video.jpg?f=";
	}
	else{ 
		$sThumbFile = "thumbnail.php?maxw=100&maxh=100&file=../system/media/en/images/{$sFolder}/";
	}
    
	if(empty($aData)){
		$sOutput .= "<center>No results match your query.</center>";
	 }
	foreach($aData as $iKey => $aCurrentItem){
			$sCurrentSrc = $sThumbFile . $aCurrentItem["file"];
			$sOutput .= "<a href=\"generic-details.php?tablename=multimedia_cmn";
			if($iLangID){ 
				$sOutput .= "&lid={$iLangID}&languagetable=multimedia_lang";
			}
			$sOutput .= "&pp=multimedia.php&id={$aCurrentItem['id']}\" title=\"{$aCurrentItem['comment']}\">";
			$sOutput .= "<div class=\"mm_plc\">";
				$sOutput .= "<div class=\"mm_image\">";
					$sOutput .= "<img src=\"{$sCurrentSrc}\" alt=\"Not found\"/>";
				$sOutput .= "</div>";
				$sOutput .= "<div class=\"mm_caption\">";
				 if($iLangID && !$aCurrentItem["title"]){ 
						$sOutput .= "<span class=\"red bold\">No Translation</span>"; 
					}
					else if($aCurrentItem["title"]){
						$sOutput .= $aCurrentItem["title"]; 
					}
					else{
						$sOutput .= $aCurrentItem["comment"]; 
					}
				$sOutput .= "</div>";
			$sOutput .= "</div>";
		   $sOutput .= " </a>";
	}
	return $sOutput;
}

function buildMMSortableHtml($aData, $sFolder, $sClass){
	$sOutput = '';
	if(sizeof($aData) > 15){
		$sOutput .= "<div class=\"pod\"><small>Showing 15 of " . sizeof($aData) . " results. Please refine your search further.</small></div>";
	}
	$sOutput .= '<ul id="librarymmSRT" class="connectedSortable sortable">';
	
	foreach($aData as $key => $currentMM){ 
        $sOutput .= '<li class="' . $sClass . ' mm_' . $currentMM["id"] . '" id="mm_' . $currentMM["id"] . '">';
        $sOutput .= '<img class="thumb" src="thumbnail.php?maxw=60&maxh=44&file=../system/media/en/images/' . $sFolder . '/' . $currentMM["file"] . '" alt="' . $currentMM["file"] . '" />';
        $sOutput .= ' <div class="info">';
		
		if(strlen($currentMM["comment"]) > 35){
			$sComment = substr($currentMM["comment"], 0,35) . "...";
		}else{
			$sComment = $currentMM["comment"];
		}
		
        $sOutput .= ' <strong>' . $sComment . '</strong>';
		
		if(strlen($currentMM["file"]) > 45){
			$sFile = substr($currentMM["file"], 0,45) . "...";
		}else{
			$sFile = $currentMM["file"];
		}
		
        $sOutput .= '<br /><small>' . $sFile . '</small>';
		$sOutput .= '<div class="dates" style="display:none;"><small>';
		$sOutput .= '<input type="text" class="datepicker" name="fromdate[]" style="width:60px; font-size:8pt;" title="From Date" onchange=\'$(".saveButtons").attr("disabled", "");\' />';
		$sOutput .= '<input type="text" class="datepicker" name="todate[]" style="width:60px; font-size:8pt;" title="To Date" onchange=\'$(".saveButtons").attr("disabled", "");\' />';
		$sOutput .= '</small></div>';
        $sOutput .= '</div>';
        $sOutput .= '</li>';
		if($key > 15){
			break;	
		}
     }
	 $sOutput .= "</ul>";
	 
	 return $sOutput;
}

?>
