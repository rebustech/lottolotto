<?php
chdir('..');
include("security/checkauth.php");
$iWebsiteID = $_GET["websiteid"];
$iSectionID = $_GET["sectionid"];
$iPageID = $_GET["id"];
$iLangID = $_GET["lid"];
switch($_GET["a"]){
	case "sorting":
		$iParentID = $_GET["rootpageid"];
		$aPageData = $_GET["aPageData"];

		parsePageData($aPageData, $iParentID, $iSectionID);
		ScriptsAdmin::rebuildPagesTree();
        showPageTree($iSectionID, $iLangID);
	break;
	case "clearcache":
		$bFoundCache = false;
		$iSuccessCount = 0;
		$iFailCount = 0;
		$aCacheFiles = @glob(SMARTY_DIR . strtolower(WEBSITENAME) . "/cache/*P{$iPageID}C*");
		if(is_array($aCacheFiles)){
			foreach($aCacheFiles as $filename) {
			$bFoundCache = true;
				$do = @unlink($filename);
				if($do=="1")
				{
					$iSuccessCount++;
					$bValid = true;
				}
				else
				{
					$iFailCount++;
					$bValid = false;
				}
			}

			if($iSuccessCount) $sMessages .= "<li>Succesfully cleared cache - {$iSuccessCount} File/s</li>";
			if($iFailCount)	 $sMessages .= "<li class=\"error\">Unable to clear cache - {$iFailCount} File/s{messagetype}achtungFail{/messagetype}</li>";
		}

		if(!$bFoundCache){
			$sMessages .= "<li class=\"error\">No cache found for PageID: {$iPageID}{messagetype}achtungFail{/messagetype}</li>";
		}

		if($sMessages){
			echo "<ul>{$sMessages}</ul>";
		}
	break;
	case "disable": PageAdmin::disablePage($iPageID);
		echo "Page <small>P:{$iPageID} </small> disabled.";
		break;
	case "enable": PageAdmin::enablePage($iPageID);
		echo "Page <small>P:{$iPageID} </small> enabled.";
		break;
	case "disablelanguage": PageAdmin::disablePageLanguage($iPageID, $iLangID);
		echo "Language Page <small>P:{$iPageID} </small> disabled.";
		break;
	case "enablelanguage": PageAdmin::enablePageLanguage($iPageID, $iLangID);
		echo "Language Page <small>P:{$iPageID} </small> enabled.";
		break;
	case "disablemobile": PageAdmin::disableMobilePage($iPageID);
		echo "Mobile page <small>P:{$iPageID} </small> disabled.";
		break;
	case "enablemobile": PageAdmin::enableMobilePage($iPageID);
		echo "Mobile page <small>P:{$iPageID} </small> enabled.";
		break;
	case "showinmenu": PageAdmin::enableShowInMenu($iPageID);
		echo "Page <small>P:{$iPageID} </small> set to show in menu.";
		break;
	case "donotshowinmenu": PageAdmin::disableShowInMenu($iPageID);
		echo "Page <small>P:{$iPageID} </small> removed from menu.";
		break;
	case "showinsitemap": PageAdmin::enableShowInSiteMap($iPageID);
		echo "Page <small>P:{$iPageID} </small> set to show in sitemap.";
		break;
	case "donotshowinsitemap": PageAdmin::disableShowInSiteMap($iPageID);
		echo "Page <small>P:{$iPageID} </small> removed from sitemap.";
		break;
	case "deleteglobal":
		$bValid = true;
		$folder = Menu::getFolder($iSectionID);
		$filename = Page::getFilenameByID($iPageID);
		$aChildren = PageAdmin::getPageChildren($iSectionID, $iPageID);
		if(count($aChildren)){
			$sMessages .= "<li class=\"error\">Unable to delete page <small>P:{$iPageID} </small>. Please move or delete sub-pages first.</li>";
		}
		else{
			$sPath = WEBSITEPATH;

			if (!File::checkFolderExists($sPath,$folder))
			{
				$sMessages .= "<li class=\"error\">The '$folder' folder does not exist in '$sPath'.</li>";
				$bValid = false;
			}

			if (Page::checkNoPageTranslations($iPageID)) {
				$sMessages .= "<li class=\"error\">Please delete all translations of the '$filename' page.</li>";
				$bValid = false;
			}
			if ($bValid == true) {
				if (!PageAdmin::deletePage($iPageID))
					$sMessages .= "<li class=\"error\">Unable to delete page <small>P:{$iPageID} </small>.</li>";
			}
		}
		ScriptsAdmin::rebuildPagesTree();
		if($sMessages)	echo "<ul class=\"ajaxMessages\">{$sMessages}</ul>";
		showPageTree($iWebsiteID, $iSectionID);

		break;
	case "deletelang": PageAdmin::deletePageLanguage($iPageID,$iLangID);
		showPageTree($iSectionID, $iLangID);
		break;

}

function showPageTree($iSectionID, $iLangID = 0){
	$rootpage = PageAdmin::getRootPage($iSectionID, $iLangID);
	$aChildPages = PageAdmin::getPageChildren($iSectionID, $rootpage['left_node'], $rootpage['right_node'], $iLangID);
    if($iLangID){
	echo GenericTableAdmin::displayRootPageLang($rootpage); ?>
	<ul id="aPageData" class="page-list">
		<?php echo GenericTableAdmin::displayChildPagesLang($aChildPages); ?>
	</ul>
	<?php
	}else{
	echo GenericTableAdmin::displayRootPage($rootpage); ?>
	<ul id="aPageData" class="page-list">
		<?php echo GenericTableAdmin::displayChildPages($aChildPages); ?>
	</ul>
	<?php
	}
}

function parsePageData($aPages, $iParentID, $iSectionID){
	foreach($aPages as $iKey => $aCurrentPage){
		$iRank = $iKey + 1;
		PageAdmin::updateParentPageAndRank($aCurrentPage["id"], $iParentID, $iRank, $iSectionID);
		if($aCurrentPage["children"]){
			$iSubLevel = $iLevel;
			$iSubLevel++;
			parsePageData($aCurrentPage["children"], $aCurrentPage["id"], $iSectionID);
		}
	}
}
?>
