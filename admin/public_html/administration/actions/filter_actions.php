<?php
chdir('..');
        include("../system/includes/autoload.php");
	include("security/checkauth.php");
$sPreviousPage = $_GET["pp"] . "?a=filters";
if($_GET["tablename"]){
	$sPreviousPage .= "&tablename=" . $_GET["tablename"];
}
if($_GET["oldqs"]){
	$sPreviousPage .= unserialize(urldecode($_GET["oldqs"]));
}
if($_GET["a"] == "clear"){
	unset($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]);
	unset($_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]);
	Cookies::deleteCookie($oSecurityObject->getsAdminType() . "filterdata-" . $oSecurityObject->getUserID());
	header("location: ../" . $sPreviousPage);
	exit;
}
else{
	if($_POST["txtLimit"] && is_numeric($_POST["txtLimit"]) && $_POST["txtLimit"] > 0){
		$_SESSION[$oSecurityObject->getsAdminType() . "iLimit"] = $_POST["txtLimit"];
	}

	$aFilters = array();
	foreach($_POST as $key => $value){
		$aKey = explode("_", $key);
		$iFilterId = $aKey[sizeof($aKey) - 1];
		if($iFilterId == 1){
			if($value[0] == 0){
				unset($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"]);
			}
			else{
				$_SESSION[$oSecurityObject->getsAdminType() ."iLangID"] = $value[0];
				$sPreviousPage .= "&lid=" . $value[0];
			}
		}
		else{
			$aFilters["$iFilterId"] = $value;
		}
	}
	$_SESSION[$oSecurityObject->getsAdminType() . "filterdata"] = $aFilters;
	Cookies::setCookie($oSecurityObject->getsAdminType() . "filterdata-" . $oSecurityObject->getUserID(), serialize($aFilters), 60*60*24*30);
	header("location: ../" . $sPreviousPage);
	exit;
}
?>
