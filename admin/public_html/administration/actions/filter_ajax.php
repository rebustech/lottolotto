<?php
chdir('..');
include("security/checkauth.php");
switch($_GET["a"]){
	case "getSections":
		unset($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::SECTION]);
		$aCurrentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$_GET["selectedFilter"]];
		$iSelectedWebsite = $_GET["selectedValue"];
		$aCurrentFilterData = array($_GET["selectedValue"]);
		$_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$_GET["selectedFilter"]] = $aCurrentFilterData;
		?>
		<table>
		<?php
            if(!empty($iSelectedWebsite)){
				$bSections = false;
				$iGlobalKey = 1;
				$aSections = Menu::getSectionsAndID($iSelectedWebsite);
				foreach ($aSections as $key => $value)
				{
					$bSections = true;
					$iGlobalKey++;
				?>
			<tr><td><input id="filter_<?=AdminPage::SECTION?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" type="radio" name="filter_<?=AdminPage::SECTION?>[]" value="<?=$value["id"];?>"/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["comment"]; ?></label></td></tr>
		<?php
				}
				if(!$bSections){
				?>
				<tr><td>No sections for selected website</td></tr>
				<?php
				}
			}else{
        ?>
			<tr><td>Select a website</td></tr>
		<?php } ?>
		</table>
		<?php
	break;
	case "getUsers":
		unset($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::USER]);
		$aCurrentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$_GET["selectedFilter"]];

		$iUserTypeID = $_GET["selectedValue"];

		if(is_array($aCurrentFilterData)){
			if(!in_array($iUserTypeID, $aCurrentFilterData)){;
				array_push($aCurrentFilterData, $iUserTypeID);
			}
			else{
				$aInvertFilterData = array_flip($aCurrentFilterData);
				unset($aCurrentFilterData[$aInvertFilterData[$iUserTypeID]]);
			}
		}
		else{
			$aCurrentFilterData = array($_GET["selectedValue"]);
		}

		$_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$_GET["selectedFilter"]] = $aCurrentFilterData;
		?>
		<table>
		<?php
			if(!empty($aCurrentFilterData)){
				$bUsers = false;
				$iGlobalKey = 1;
				foreach($aCurrentFilterData as $currentUserType){
					$aUsers = UsersAdmin::getUsersByType($currentUserType);
					foreach ($aUsers as $key => $value)
					{
						$iGlobalKey++;
						$bUsers = true;
					?>
						<tr><td><input id="filter_<?=AdminPage::USER?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" type="checkbox"  title="<?=$value["username"];?>" onclick="usertypeChanged(this.value);" name="filter_<?=AdminPage::USER?>[]" value="<?=$value["id"];?>"/><label title="<?=$value["username"];?>" for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["firstname"] . " " . $value["lastname"]; ?></label></td></tr>
					<?php
					}
				}
				if(!$bUsers){
				?>
				<tr><td>No users for selected user types</td></tr>
				<?php
				}
			}
			else{
				?>
				<tr><td>Select a user type</td></tr>
				<?php
			}
        ?>
		</table>
		<?php
	break;
}
?>
