<?php
chdir('..');
include("security/checkauth.php");
switch($_GET["a"]){
	case "tweetResults":
			$iMaxLength = 140;
			$sMessage = stripslashes($_POST['tweet']);
			if ( $_POST['draw'] ):
				$sURL = " ". LotteryResultsAdmin::getLotteryResultPageURL((int)$_GET['tablename'], (int)$_POST['draw'] );
				$iMaxLength = 119;
			endif;
			if ( strlen($sMessage) > $iMaxLength )
			{
				echo "Message is Longer than {$iMaxLength} Characters. " . strlen($sMessage) . ".{messagetype}achtungFail{/messagetype}";
			}
			elseif ( empty($sMessage) )
			{
				echo "Message is Required.{messagetype}achtungFail{/messagetype}";
			}
			else
			{
				print_d(ScriptsAdmin::updateTwitter($sMessage . $sURL));
			}
		break;
	case "createRSS":
		$aLotteryDetails = LotteryAdmin::getLotteryDetails((int)$_GET['tablename'], true);
		$sWebsitePath   = WEBSITEPATH;
		$sWebsiteURL  = WEBSITEURL;

		$sFileName = $sWebsitePath . strtolower(str_replace(" ", "", $aLotteryDetails['title']) . ".xml");
		print_d(strtolower(str_replace(" ", "", $aLotteryDetails['title']) . ".xml"));

		$aResults = LotteryResultsAdmin::getLotteryDraws((int)$_GET['tablename'], 15);

		if ( $aResults )
		{
			if(is_writable($sFileName)){
				$handle = fopen($sFileName, 'w+');
				if($handle == false ){
					print_d( "Cannot open file ($sFileName)");
				}
				else{
					$sClassName = Lottery::getLotteryClassName((int)$_GET['tablename']);
					if ( $sClassName )
					{
						$oLottery = new $sClassName(1);
					}
					$rRRS = new RSS();
					$rRRS->setTitle($aLotteryDetails['title'] . " Results");
					$rRRS->setLink("http://" . $sWebsiteURL . $oLottery->getResultsPage());
					$rRRS->setDescription($aLotteryDetails['title'] . " Lottery Results Delivered as they happen! Check your lottery tickets and see if you've won.  Don't forget to buy your lottery tickets online with LoveLotto.com");
					$rRRS->setLanguage("en-gb");
					//$rRRS->setImage("http://www.lovelotto.com/system/media/images/web/logo_purple.gif");
					//$rRRS->setImageH("70");
					//$rRRS->setImageW("265");
					$aItems = array();
					foreach ($aResults as $aResult){

						$aNumbers = explode("|", $aResult['numbers']);
						$aItem = array();
						$aItem['title'] = date("l jS M Y", strtotime($aResult['date']));
						$aItem['guid'] = "http://" . $sWebsiteURL . $oLottery->getResultsPage() . "?draw=" . $aResult['draw'];
						$aItem['link'] = "http://" . $sWebsiteURL . $oLottery->getResultsPage() . "?draw=" . $aResult['draw'];
						if('1' >= $aResult['timezone'] ){
							$sTimeZone = " +" . $aResult['timezone'] . "hours";
						}
						else{
							$sTimeZone = " " . $aResult['timezone'] . "hours";
						}
						$aItem['pubdate'] =  date("D, j M Y H:i:s O", strtotime($aResult['date'] . $sTimeZone ));
						$aItem['description'] = $aLotteryDetails['title'] . " Results for " . date("l jS M Y", strtotime($aResult['date']));

						$aItem['description'] .= ": ";
						$aItem['description'] .= implode("-", array_slice($aNumbers,0,$aLotteryDetails['number_count'])) . ". ";
						if ( $aLotteryDetails['drawn_bonus_numbers'] ) {
							$aItem['description'] .= ($aLotteryDetails['drawn_bonus_numbers'] > 1)?$aLotteryDetails['bonus_text_plural'] . ": ":$aLotteryDetails['bonus_text'] . ": ";
							$aItem['description'] .= implode("-", array_slice($aNumbers,$aLotteryDetails['number_count'],$aLotteryDetails['drawn_bonus_numbers'])) . ". ";

						}
						//$aItem['description'] .= "| Next Jackpot: ";
						array_push($aItems, $aItem);
					}
					$rRRS->setItems($aItems);
					fwrite($handle,  $rRRS->getFeed());
					print_d("Success, wrote {$sFileName}");
					unset($aItems);
				}
				fclose($handle);
			}
			else{
				print_d("Unable to write to file: {$sFileName}");
			}
		}
		break;
	case "shortenUrl":
			$sURL = $_POST['url'];
			if ( $sURL )
			{
				echo displayOutput(ScriptsAdmin::shortURL($sURL));
			}
			else
			{
				echo displayOutput("URL Is Empty");
			}
			break;
	default:

		break;
}

function displayOutput($sContent)
{
	?>
    <div id="ajaxContentDiv" style="width: 1350px; height: 150px; padding-top: 20px; overflow: auto; text-align: left;">
		<?=$sContent?>
	</div>
    <?php
}
?>