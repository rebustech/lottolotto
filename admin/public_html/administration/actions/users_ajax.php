<?php
    chdir('..');
        include("../system/includes/autoload.php");
	include("security/checkauth.php");
$iId = $_GET["id"];
$sIdField = $_GET["idfield"];
$iWebsiteID = $_GET["websiteid"];
$sTablename = $_GET["tablename"];
$sLanguageTable = $_GET["languagetable"];
$iLangID = $_GET["lid"];
$iLimit = $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"];


switch($_GET["a"]){
	case "toggleBooleanField":
		$sField = $_GET["field"];
		$bValue = $_GET["value"];
		if($bValue == "enable"){
			$sValue = "True";
			$bToggleValue = 1;
		}
		elseif($bValue == "disable"){
			$sValue = "False";
			$bToggleValue = 0;
		}
		$aData = array($sField => $bToggleValue);

		if(strstr($sIdField, "|")){
			$aIdFields = explode('|', $sIdField);
			$aIds = explode('_', $iId);
			$sParams = "";
			foreach($aIdFields as $iKey => $sCurrentIdField){
				if($sParams .= ""){
					$sParams .= " AND ";
				}
				$sParams .= "$sCurrentIdField = '" . $aIds[$iKey] . "' ";
			}
		}else{
			$sParams = "$sIdField = '$iId'";
		}
		$bResponse = GenericTableAdmin::doGenericUpdate($sTablename, $aData, $sParams);

		if($bResponse == 1){
			echo "$sField #{$iId} set to {$sValue}.";
		}
		else{
			echo "Problem encountered. {messagetype}achtungFail{/messagetype}";
		}
	break;
	case "usersTableSearch":
			$sQuery = $_GET["query"];
			$sSearchField = $_GET["searchfield"];
			$sFilters = urldecode($_GET["filterstring"]);

			if($sQuery){
				$sFilters = GenericTableAdmin::appendConditionToFilterString($sFilters, "ct.{$sSearchField} LIKE '%{$sQuery}%'");
			}
			$aData = GenericTableAdmin::getRecords($sTablename, $sFilters, "", $iLimit);
			$aData["pp"] = $_GET["pp"];
			echo GenericTableAdmin::createDataTable(
				array("ID|tiny|centeralign", "Username", "First name", "Last name",  "isactive","Edit|tiny|centeralign", "Delete|tiny|centeralign"),
				array("user_id|centeralign", "username", "firstname", "lastname", "user_id|ajaxbool:isactive|centeralign", "user_id|link:users-details.php?pp=" . $_GET["pp"] . "&a=edit&tablename=" . $sTablename . "|centeralign",
				"user_id|link:actions/generic_actions.php?pp=" . $_GET["pp"] . "&a=delete&tablename=" . $sTablename . "|centeralign"),
				$aData, $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]
			);

		break;
		case "saveDetails":
				$aUserData = array();
				$bValid = true;

				$aUserData["username"] = $_POST["username"];
				if(!$aUserData["username"]){
					$oErrors->addError("Username is required");
					$bValid = false;
				}

				$aUserData["firstname"] = $_POST["firstname"];
				if(!$aUserData["firstname"]){
					$oErrors->addError("First name is required");
					$bValid = false;
				}

				$aUserData["lastname"] = $_POST["lastname"];
				if(!$aUserData["lastname"]){
					$oErrors->addError("Last name is required");
					$bValid = false;
				}

				$aUserData["email"] = $_POST["email"];
				if(!$aUserData["email"]){
					$oErrors->addError("Email address is required");
					$bValid = false;
				}

				$aUserData["tel1"] = $_POST["telephone1"];
				if(!$aUserData["tel1"]){
					$oErrors->addError("Telephone is required");
					$bValid = false;
				}

				$aUserData["tel2"] = $_POST["telephone2"];
				if ( $_POST['at'] == SecurityAdmin::AFFILIATE )
				{
					$aAffiliateData["web"] = $_POST["web"];

					$aAffiliateData["address1"] = $_POST["address1"];
					if(!$aAffiliateData["address1"]){
						$oErrors->addError("Address is required");
						$bValid = false;
					}

					$aAffiliateData["address2"] = $_POST["address2"];

					$aAffiliateData["zipcode"] = $_POST["zip"];
					if(!$aAffiliateData["zipcode"]){
						$oErrors->addError("ZIP Code is required");
						$bValid = false;
					}

					$aAffiliateData["city"] = $_POST["city"];
					if(!$aAffiliateData["city"]){
						$oErrors->addError("City is required");
						$bValid = false;
					}

					$aAffiliateData["country"] = $_POST["country"];
					if(!$aAffiliateData["country"]){
						$oErrors->addError("Country is required");
						$bValid = false;
					}
					$aAffiliateData["fax"] = $_POST["fax"];


					$aAffiliateData["company"] = $_POST["company"];
					$aAffiliateData["web"] = $_POST["website"];
					if(!$aAffiliateData["web"]){
						$oErrors->addError("Website is required.");
						$bValid = false;
					}

					$aAffiliateData["vatno"] = $_POST["vat"];

					$aAffiliateData["commission"] = $_POST["commission"];
					if(!$aAffiliateData["commission"]){
						$oErrors->addError("Commission Rate is required.");
						$bValid = false;
					}
				}

				if($bValid){
					if(UsersAdmin::editUserDetails($iId, $aUserData)){
						if( ( $aAffiliateData && Affiliates::editUserDetails($aAffiliateData,$iId)) || empty($aAffiliateData) ) {
							echo "User details saved.";
						}
						else{
							echo "Problem encountered on saving. {messagetype}achtungFail{/messagetype}";
						}
					}
					else{
						echo "Problem encountered on saving. {messagetype}achtungFail{/messagetype}";
					}
				}
				else{
					echo $oErrors->getErrorString() . "{messagetype}achtungFail{/messagetype}";
				}
				$oErrors->clearMessages();

			break;
			case "changePassword":
				if($_POST["passwd"] != $_POST["passwd2"]){
					echo "Passwords do not match.{messagetype}achtungFail{/messagetype}";
				}
				else{
					if(UsersAdmin::changePassword($iId, $_POST["passwd"])){
						echo "Password changed.";
					}
					else{
						echo "Problem encountered while changing password. {messagetype}achtungFail{/messagetype}";
					}
				}
			break;
			case "changePin":
				if($_POST["pin"] != $_POST["pin2"]){
					echo "PINs do not match.{messagetype}achtungFail{/messagetype}";
				}
				else{
					if(UsersAdmin::changePin($iId, $_POST["pin"])){
						echo "PIN changed.";
					}
					else{
						echo "Problem encountered while changing PIN. {messagetype}achtungFail{/messagetype}";
					}
				}
			break;
			case "savePermissions":
				//to do save user type
				$aUserData = array();
				$aUserData["fk_admintype_id"] = $_POST["fk_admintype_id"];
				UsersAdmin::editUserDetails($iId, $aUserData);
				if($_POST["pages"]){
					UsersAdmin::clearBlacklist($iId);
					array_shift($_POST["pages"]);
					if(count($_POST["pages"]) > 0 ){
						UsersAdmin::blacklistPages($iId, $_POST["pages"]);
					}
				}
				if($_POST["products"]){
					UsersAdmin::removeAllProductFromUser($iId);
					array_shift($_POST["products"]);
					if(count($_POST["products"]) > 0 ){
						UsersAdmin::setProductsForUser($iId, $_POST["products"]);
					}
				}
				echo "Permissions saved ok.";
			break;
	}
?>
