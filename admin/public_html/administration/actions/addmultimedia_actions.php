<?php  
include("../security/checkauth.php");
if($_SESSION[$oSecurityObject->getsAdminType() . "pid"]){
	$redirect = "../image-library.php";
}
else{
	$redirect = "../multimedia.php?tablename=" . $_GET["tablename"];	
}



$iWebsiteID = $_GET["websiteid"];
$iMMTypeID = $_GET["typeid"];


$aMMtype = MultimediaAdmin::getMMTypebyID($iMMTypeID);
$mmtype = $aMMtype[type];

$bValid = true;
switch($mmtype){
	case "images":
	case "files":	
			$uploadedimages = Array();
			$i = 0;
			$maxwidth = $aMMtype[maxwidth];
			$maxheight = $aMMtype[maxheight];
			$folder = MultimediaAdmin::getFolderByID($iMMTypeID);
			
				foreach($_FILES['imagefile']['name'] as $key => $value){
					if(($_POST["{$i}_comment"] == "") && ($value != "")){
						$oErrors->addError("Comment cannot be left blank");
						$redirect = "../addmultimedia.php?tablename=" . $_GET["tablename"];
						$bValid = false;
					} 
					else if(($_POST["{$i}_comment"] != "") && ($value == "")){
						$oErrors->addError("Filepath required");
						$redirect = "../addmultimedia.php?tablename=" . $_GET["tablename"];
						$bValid = false;							
					}
					else if(($_POST["{$i}_comment"] != "") && ($value != "")){
						$imagedata = Array();
						
						if($bValid == true){
									$tempname =  $_FILES['imagefile']['tmp_name'][$key];
									$comment = $_POST["{$i}_comment"];
									$userid = $_POST["{$i}_user"];
									$url =  $_POST["{$i}_url"];
									$filename = $value;
									$type = $_FILES['imagefile']['type'][$key];
								if(strstr($type, "image")){
									if (!MultimediaAdmin::uploadImage($iWebsiteID,
										$value, $comment, $tempname, 
										$iMMTypeID, $userid, $_SESSION[$oSecurityObject->getsAdminType() . 'pid'], $url, $_POST["quality"])) {
										$oErrors->addError("Problem while uploading image.");	
										$bValid = false;
										$redirect = "../addmultimedia.php?tablename=" . $_GET["tablename"];
									}
								}
								else{
										MultimediaAdmin::uploadFile($iWebsiteID,
										$value, $comment, $tempname, 
										$iMMTypeID, $userid, $_SESSION[$oSecurityObject->getsAdminType() . 'pid'], $url);
									}	
								
						}
						else{
								header("Location: $redirect");				
							}
					}
					$i++;
				}		
				
					
					if ($bValid == true) {
						header("Location: $redirect");
					}
				

				header("Location: $redirect");
				exit;
	break;	
	case "links":
					$target = $_POST["0_target"];
				
					if ($_POST["0_comment"] == "") {
						$oErrors->ddError("You cannot leave the comment empty.");	
						$redirect = "../addmultimedia.php?tablename=" . $_GET["tablename"];
						$bValid = false;			
					}
						
					$url = $_POST["0_url"];
					if (empty($url)) {
						$oErrors->addError("You cannot leave the Link empty.");	
						$redirect = "../addmultimedia.php?tablename=" . $_GET["tablename"];
						$bValid = false;			
					}		
					
					if ($bValid ==  true) MultimediaAdmin::addMMLink($iWebsiteID,$iMMTypeID,$url,$target,$_POST["0_comment"]);
					
					header("Location: $redirect");
	break;
}					
					
?>