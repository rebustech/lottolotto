<?php
    require_once("../system/includes/autoload.php");


    //Create 5 new customers
    $i=rand(1,100);
    for($a=0;$a<$i;$a++){
    DAL::executeQuery('INSERT INTO members (firstname,lastname,contact,address1,city,zip,is_active,email)
SELECT (SELECT firstname FROM randomnames ORDER BY RAND() LIMIT 1),
(SELECT lastname FROM randomnames ORDER BY RAND() LIMIT 1),
(SELECT contact FROM randomnames ORDER BY RAND() LIMIT 1),
(SELECT address1 FROM randomnames ORDER BY RAND() LIMIT 1),
(SELECT city FROM randomnames ORDER BY RAND() LIMIT 1),
(SELECT zip FROM randomnames ORDER BY RAND() LIMIT 1),1,CONCAT(\'japatchett+\',NOW(),RAND(),\'@hotmail.com\')');
    }

    //Create a random order for a random user on a random date

    $i=rand(1,4);
    for($a=0;$a<$i;$a++){

        $iUserId=DAL::executeGetOne('SELECT member_id FROM members ORDER BY RAND() LIMIT 0,1');

        $oOrder=new Cart();
        //Create between 1 and 6 random order items
        $iNumberOfOrderItems=rand(1,6);
        for($iOrderItemNumber=1;$iOrderItemNumber<=$iNumberOfOrderItems;$iOrderItemNumber++){
            $iGameId=DAL::executeGetOne('SELECT id FROM game_engines WHERE engine_core=\'\\\\LL\\\\GameEngine\\\\StandardPlay\' ORDER BY RAND() LIMIT 0,1');
            $oOrderItem=new CartItem();
            $oOrderItem->setGameID($iGameId);

            //Simulate a random play
            $oGameEngine=GameEngine::getById($iGameId);
            if(method_exists($oGameEngine, 'simulatePlay')){
                $oGameData=$oGameEngine->simulatePlay();
                $oOrderItem->setData($oGameData);

                //Attach the standard payment processor for testing
                $oMember=new Member();
                $oMember->iMemberID=$iUserId;
                $oCurrency=new Currency(1); //EURO

                $oOrder->addToCart($oOrderItem);

                //Before we commit the order does this person have enough cash or do they need a topup?
                $oMember->updateBalance();
                if($oMember->getMemberBalance() < $oOrder->getCartTotal()){
                    $iTopUp=$oOrder->getCartTotal()-$oMember->getMemberBalance();
                    $iTopUp=round($iTopUp,-1)+(rand(0,100)*10);
                    if($iTopUp<100){
                        $iTopUp=round($iTopUp,-1);
                    }else{
                        $iTopUp=round($iTopUp,-2);
                    }
                    $oPayment=new LoveLottoGateway();
                    $oPayment->assignAmount($iTopUp);
                    $oPayment->addTransaction($iUserId, 1);
                    $oPayment->ProcessPayment();
                }
                $oOrder->commitPurchase($oMember,$oCurrency);
            }
        }
    }
    \DAL::executeQuery('DELETE FROM booking_items WHERE numbers=\'\'');
?>