<?php
    include("includes/top.php");
?>
    <h1>Dashboard</h1>

<?
    include("includes/errors.php");
    include("includes/notifications.php");
?>

<script src="/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript" src="/amcharts/amcharts/pie.js"></script>
<script>
    var llcharts={};
</script>

<?
    echo \LL\DashboardWidgets\Core::Headlines();
    echo \LL\DashboardWidgets\Core::TicketsAll();
    echo \LL\DashboardWidgets\Core::SalesByPromotion();
    echo \LL\DashboardWidgets\Core::SalesByProduct();

	if($_GET["a"] == "addbookmark" && is_numeric($_GET["p"]) ){
		$oSecurityObject->addBookmark($_GET["p"]);
	}
	else if($_GET["a"] == "deletebookmark" && is_numeric($_GET["p"]) ){
		$oSecurityObject->deleteBookmark($_GET["p"]);
	}

	$aPageBookmarks = $oSecurityObject->getBookmarks();
	$aNotBookmarks = $oSecurityObject->getBookmarks(false);
	$aRecentPageIDs = unserialize(Cookies::getCookie($oSecurityObject->getsAdminType() . "history-" . $oSecurityObject->getUserID()));
	if ( !is_array($aRecentPages) )
	{
		$aRecentPageIDs = array();
	}
	$aRecentPages = AdminPage::getPages($aRecentPageIDs, $oSecurityObject->getUserID(), 10);
	?>
  	<div style="float: left; width: 50%;">
        <h2>Bookmarks</h2>
        <ol>
        <?php
        foreach($aPageBookmarks as $aCurrentFav){
            ?>
                <li><a href="/<?=$aCurrentFav["filename"]?>?tablename=<?=$aCurrentFav["tablename"]?>"><?=$aCurrentFav["title"];?></a> <small><a href="?a=deletebookmark&p=<?=$aCurrentFav["id"];?>" style="color: #000; margin-left: 5px;">Remove</a></small> </li>
            <?php
        } ?>
        	<li style="list-style: none; margin-top: 5px;">
            	<select id="pageselect" onchange="if($('#pageselect').val() != 'null'){ $('#addbookmark').attr('disabled', ''); }else{ $('#addbookmark').attr('disabled', 'disabled'); }">
                	<option value="null" selected="selected">Select page</option>
                    <?php foreach($aNotBookmarks as $aCurrentNotfav){ ?>
                    <option value="<?=$aCurrentNotfav["id"];?>"><?=$aCurrentNotfav["title"];?></option>
                    <?php } ?>
                </select>
                <input id="addbookmark" type="button" value="Add" disabled="disabled" onclick="if($('#pageselect').val() != 'null'){ document.location = '?a=addbookmark&p=' + $('#pageselect').val(); }" />
                </li>
        </ol>
    </div>
    <div style="float: left; width: 50%;">
        <h2>Recent Pages</h2>
        <ol>
        <?php
        foreach($aRecentPages as $aCurrentFav){
            ?>
                <li><a href="<?=$aCurrentFav["filename"]?>?tablename=<?=$aCurrentFav["tablename"]?>"><?=$aCurrentFav["title"];?></a></li>
            <?php
        } ?>
        </ol>
    </div>
<?php include("includes/bottom.php"); ?>