<?php
include ("includes/top.php");
//SHORTCUT - alt+5 clears report cache
?>
<h1><?=$oPage->sTitle?></h1>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).bind('keydown', 'alt+5', clearCache);
	});

	function clearCache(){
		document.location = "?tablename=<?=$oPage->sTablename?>&action=clearcache";
	}
</script>
<?php

if ( $oPage->sTablename )
{
	if ( $_GET['draw'] )
	{
		$aResults = LotteryResultsAdmin::getLotteryDraw($oPage->sTablename, $_GET['draw'], true);
		if ( $aResults )
		{
			$aResults['winnings'] = unserialize($aResults['winnings']);
			if ( $_GET['action'] == 'edit' )
			{
			?>
            	<ul class='subnav'>
                    <li><a href='<?=$oPage->sFilename?>?tablename=<?=$oPage->sTablename?>' class='largebutton'>View All Draws</a></li>
                    <li><a href='<?=$oPage->sFilename?>?tablename=<?=$oPage->sTablename?>&draw=<?=$aResults['draw']?>' class='largebutton'>View Result</a></li>
                </ul>
				<form name="editResult" action="actions/lottery_actions.php?tablename=<?=$oPage->sTablename?>&pp=<?=$oPage->sFilename?>&a=editResult" method="post">
					<input type="hidden" name="lotteryid" value="<?=$oPage->sTablename?>" />
					<div id="results">
				<?php
					$aResults['number'] = explode("|", $aResults['numbers']);
					$aLotteryMatches = LotteryResultsAdmin::getWinningsCombinations($oPage->sTablename);
					//$aDrawDates = LotteryResultsAdmin::getDrawDates($oPage->sTablename, true, true);
					echo "<strong>Draw Date:</strong><input type='hidden' name='drawdate' value='" . $aResults['date'] . "' />{$aResults['date']}<br/>";
					echo "<strong>Draw:</strong> <input type='number' style='width:80px' name='draw' value='" . $aResults['draw'] . "' title='draw' autocomplete='off' /><br />";
					echo "<strong>Drawn Numbers:</strong> ";
					for ( $i = $aLotteryMatches['numbers']; $i > 0; $i-- )
					{
							echo '<input type="number" name="number[]" value="' . $aResults['number'][$aLotteryMatches['numbers']-$i] . '" title="number" maxlength="2"  min="1"/>';
					}
					for ( $j = $aLotteryMatches['bonus']; $j > 0; $j-- )
					{
						echo '<input type="number" name="number[]" class="bonus" value="' . $aResults['number'][($aLotteryMatches['numbers']+$aLotteryMatches['bonus'])-$j] . '" title="bonus number" maxlength="2" min="1"/>';
					}
					echo "<br/><strong>Winnings:</strong><Br/><br/>
					<table cellspacing='0' cellpadding='0'><tr><th width='160px' align='left'>Title</th><th width='80px' align='left'>Winners</th><th width='180px' align='left'>Amount</th></tr>";
					echo '<tr><td>Jackpot</td><td>&nbsp</td><td>' . $aLotteryMatches['currency']['shortname'] . ' <input type="number" name="jackpot" value="' . $aResults['jackpot'] . '" title="Jackpot" class="bonus" /></td></tr>';
					foreach ( $aLotteryMatches['matches'] as $key=>$aLotteryMatch )
					{
						echo "<tr><td>Match " . $aLotteryMatch[0];
						if ( $aLotteryMatch[1] )
						{
							echo " plus " . $aLotteryMatch[1] . " " . (($aLotteryMatch[1] > 1)?$aLotteryMatches['bonus_plural']:$aLotteryMatches['bonus_text']);
						}
						echo '</td><td><input type="number" name="winners[]" value="' . $aResults['winnings'][$key]['winners'] . '" title="Winners"  /></td><td>' . $aLotteryMatches['currency']['shortname'] . '<input type="hidden" name="bonus[]" value="' . $aLotteryMatch[1]. '" /><input type="hidden" name="match[]" value="' . $aLotteryMatch[0] . '" /> <input type="text" name="winnings[]" value="' . $aResults['winnings'][$key]['prize'] . '" title="Winnings"  /></td></tr>';

					}
					echo "</table>";
				?>
					</div>
					<input type="button" value="Cancel" onclick="location.href='<?=$oPage->sFilename?>?tablename=<?=$oPage->sTablename?>&draw=<?=$aResults['draw']?>'" />
					<input type="submit" value="Save Changes" />
				</form>
		<?php

			}
			else
			{
				echo "<ul class='subnav'>
						<li><a href='{$oPage->sFilename}?tablename={$oPage->sTablename}' class='largebutton'>View All Draws</a></li>
						<li><a href='{$oPage->sFilename}?tablename={$oPage->sTablename}&action=edit&draw={$aResults[draw]}' class='largebutton'>Edit Result</a></li>
						<li><a href='match-winners.php?tablename={$oPage->sTablename}&draw={$aResults[draw]}' class='largebutton'>Match Winners</a></li>
						<li><a href='addJackpot.php?tablename={$oPage->sTablename}' class='largebutton'>Add Jackpot</a></li>
					</ul>";
				echo "<h2>Lottery Draw Results</h2>";
				echo "<strong>Draw Date</strong>: " . $aResults['date'] . "<br/>";
				echo "<strong>Draw</strong>: " . $aResults['draw'] . "<br/>";
				echo "<strong>Jackpot</strong>: " . number_format($aResults['jackpot'],0,'.',',') . "<br/>";
				echo "<strong>Drawn Numbers</strong>: " . str_replace("|", " - ", $aResults['numbers']) . "<br/><br/>";
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class='report'>";
				echo "<tr><th width='60px'>Match</th><th>Bonus</th><th>Prize</th><th>Winners</th>";
				$key = 0;
				foreach ( $aResults['winnings'] as $aResult )
				{
					$key++;
					$sClass = '';
					if ( $key % 2 )
					{
						echo "<tr class='alternate{$sClass}'>";
					}
					else
					{
						echo "<tr class='{$sClass}'>";
					}
					echo "<td>" . $aResult['match'] . "</td>";
					echo "<td>" . $aResult['bonus'] . "</td>";
					echo "<td>" . number_format($aResult['prize'],0,'.',',') . "</td>";
					echo "<td>" . number_format($aResult['winners'],0,'.',',') . "</td>";
					echo "</tr>";
				}
				echo "</table>";
				$aNumbers = explode("|", $aResults['numbers']);
				$aLotteryDetails = LotteryAdmin::getLotteryDetails($oPage->sTablename, true);
				$sClassName = Lottery::getLotteryClassName($oPage->sTablename);
				if ( $sClassName )
				{
					$oLottery = new $sClassName(1);
				}
				$aDraws = $oLottery->getAvailableDates();
				$sNextJackpot = $aDraws[0]['jackpot'];

				?><br/><h2>Tweet Results</h2>
                <form name="tweetresults" action="actions/lottery_ajax.php?a=tweetResults&pp=<?=$oPage->sFilename?>&tablename=<?=$oPage->sTablename?>" class="ajaxform" method="post">
                	<input type="hidden" name="draw" value="<?=$aResults['draw']?>" />

                <table class="detailsform">
                    <tr>
                        <td colspan="2">
                            <strong>Tweet Results on Love_Lotto Twitter Account</strong>
                        </td>
                    </tr>
                    <tr>
                        <th align="right" class="medium">Message to Tweet</th>
                        <td><input type="text" name="tweet" class="tweet long required"  value="" /></td>
                   </tr>
                   <tr>
                   		<th align="right"><?=$aLotteryDetails['comment']?> Template</th>
                        <td><a onclick="$('.tweet').val($(this).text());">#<?= strtolower(
						str_ireplace(array("USA", " ","uknationallotto", "ukthunderball","mega-sena"), array("","","nationallottery", "thunderball", "megasena"), $aLotteryDetails['title']))?> Results for <?=date("D d M",strtotime($aResults['date']))?>: <?=implode("-", array_slice($aNumbers,0,$aLotteryDetails['number_count']))?>. <?php if ( $aLotteryDetails['drawn_bonus_numbers'] ) { ?><?=($aLotteryDetails['drawn_bonus_numbers'] > 1)?$aLotteryDetails['bonus_text_plural']:$aLotteryDetails['bonus_text']?>: <?=implode("-", array_slice($aNumbers,$aLotteryDetails['number_count'],$aLotteryDetails['drawn_bonus_numbers']))?>.<?php } ?> <?=( ($aResults['winnings'][0]['winners'])?$aResults['winnings'][0]['winners'] . " winner" . ( ($aResults['winnings'][0]['winners'] > 1)?"s take":" takes") . " home " . $aLotteryDetails['symbol'] . number_format(($aResults['winnings'][0]['prize']/1000000),0,".",",") . "M!":"No Jackpot Winners." )?> Next Jackpot: <?=$aLotteryDetails['symbol'] . str_replace(".0","",number_format(($sNextJackpot/1000000),0,".",",")) . "M"?></a></td>
                   </tr>
                   <tr>
                        <th colspan="2" align="right">
                            <input type="submit" value="Post Tweet" />
                        </th>
                   </tr>
                </table>
                </form>
                <br/><h2>Actions</h2>
                <table>
                <tr>
                <td>
                <form id="createRSS" name="createRSS" action="actions/lottery_ajax.php?a=createRSS&pp=<?=$oPage->sFilename?>&tablename=<?=$oPage->sTablename?>" class="ajaxform" method="post">
                <input type="submit" value="Update RSS" />
                </form>
                </td>
                <td>
                <form id="clearresultscache" name="clearresultscache" action="actions/pages_ajax.php?a=clearcache&pp=<?=$oPage->sFilename?>&tablename=<?=$oPage->sTablename?>" class="ajaxform" method="get">
                <input type="hidden" name="id" value="<?=$aLotteryDetails['results_page_id']?>" />
                <input type="submit" value="Clear Page Cache" />
                </form>
                </td>
                <td>
                <form id="clearcache" name="clearcache" action="actions/pages_ajax.php?a=clearcache&pp=<?=$oPage->sFilename?>&tablename=<?=$oPage->sTablename?>" class="ajaxform" method="get">
                <input type="hidden" name="id" value="20" />
                <input type="submit" value="Clear Results Page Cache" />
                </form>
                </td>
                <td>
                <input type="submit" value="Do It All" onclick='$("#clearcache").submit(); $("#clearresultscache").submit();  $("#createRSS").submit(); ' />
                </td>
                </tr>
                </table>
                <br/>
                <!--
                <form name="shortenurl" action="actions/lottery_ajax.php?a=shortenUrl&pp=<?=$oPage->sFilename?>&tablename=<?=$oPage->sTablename?>" class="simpledialog" method="post">
                <table class="detailsform">
                    <tr>
                        <td colspan="2">
                            <strong>Shorten URL on Bit.ly</strong>
                        </td>
                    </tr>
                    <tr>
                        <th align="right" class="medium">URL to Shorten</th>
                        <td><input type="url" name="url" class="url long required"  value="" /></td>
                   </tr>
                   <tr>
                        <th colspan="2" align="right">
                            <input type="submit" value="Shorten It!" />
                        </th>
                   </tr>
                </table>
                </form>
                -->
                <?php
			}
		}
		else
		{
			echo "<div class='reportempty'>Draw Not Found</div>";
		}
	}
	else
	{
		?>
		<ul class="subnav">
			<li>
				<a href="<?=$oPage->sFilename?>" class="largebutton">Back to Lottery Results</a>
			</li>
			<li>
				<a href="" class="largebutton" onclick="printPage(); return false;">Print Results</a>
			</li>
		</ul>
		<div id="reports">
		<?php
			$aResults = LotteryResultsAdmin::getLotteryDraws($oPage->sTablename);
			if ( $aResults )
			{
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\" class='report'>";
				echo "<tr><th>Draw Number</th><th>Draw Date</th><th>Numbers</th><th>Jackpot</th><th>Processed</th><th></th>";
				foreach ( $aResults as $key=>$aResult )
				{
					if ( $key % 2 )
					{
						echo "<tr class='alternate'>";
					}
					else
					{
						echo "<tr>";
					}
					echo "<td>" . $aResult['draw'] . "</td>";
					echo "<td>" . $aResult['date'] . "</td>";
					echo "<td>" . str_replace("|", "&nbsp;&nbsp;", $aResult['numbers']) . "</td>";
					echo "<td>" . $aResult['jackpot'] . "</td>";
					echo "<td>" . (($aResult['processed'])?'Yes':'<span class="alert">No</span>') . "</td>";
					echo "<td><a href='" . $oPage->sFilename . "?tablename=" . $oPage->sTablename ."&draw=" . $aResult['draw'] . "'>Show Details</a></td>";
					echo "</tr>";
				}
				echo "</table>";
			}
			else
			{
				echo "<div class='reportempty'>No Lottery Results Entered Yet</div>";
			}
		?>
		</div>
		<?php
		unset($aResults);
	}
}
else
{
	$aPages = $oSecurityObject->getNavigationPages($oPage->iID);
	?>
    <div id="reports">
    <ul>
    <?php
	foreach ($aPages as $aPage )
	{
	?>
		<li>
        	<a href="<?=$aPage["filename"];?><?php if($aPage["tablename"]){ ?>?tablename=<?=$aPage["tablename"]; }?>"><img src='images/lotto-<?=$aPage["tablename"]?>.png' width="70px" />
            <br/>
        	<?=$aPage["title"];?></a>
        </li>
    <?php
	}
	?>
	</ul>
    </div>
    <?php
}
include ("includes/bottom.php");
?>