<?php
/**
 * THis page is displayed after results entered by a checker have been stored in the database
 */


$bypass_login = true;
include ("includes/top.php");
//SHORTCUT - alt+5 clears report cache
?>
<h1><?=$oPage->sTitle?></h1>
<?php


// This script should have been accessed via a querystring with a security hash,
// so check for the existence of that
if ( $_GET['sh'] )
{
    // Assign querystring variables
    $sSecurityHash = $_GET['sh'];
    $iLotteryID = $_GET['l'];
    $iCheckerID = $_GET['c'];
    $iDraw = $_GET['d'];

    // TODO: security check on hash here

    // Get newly-stored result row
    $aResults = LotteryResultsAdmin::getLotteryRowFromCheckingTable($sSecurityHash, $iLotteryID, $iCheckerID, $iDraw);

    // Get general lottery details
    $aLotteryDetails = LotteryAdmin::getLotteryDetails($iLotteryID);

    // Check to ensure we do have the results from the database
    if ( $aResults )
    {
        
        
        // Unserialise the winnings (stored as serialised array)
        $aResults[0]['winnings'] = unserialize($aResults[0]['winnings']);

        // Display entered results and prize breakdowns
?>
    <h2>Lottery Draw Results entry confirmation</h2>
    <strong>Draw Date</strong>: <?=$aResults[0]['fk_lottery_date']?><br/>
    <strong>Draw</strong>: <?=$aResults[0]['draw_ref']?><br/>
    <strong>Lottery type</strong>: <?=$aLotteryDetails['comment']?><br/><br/>
    
    <h3>You entered the following information:</h3>
    <strong>Drawn Numbers</strong>: <?=str_replace("|", " - ", $aResults[0]['numbers'])?><br/><br/>
    <strong>Prize Breakdowns</strong>:<br/>
    
<?php if ($aResults[0]['winnings'] === false) { ?>
    No prize breakdown entered
    <br/><br/>
<?php } else { ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th width='120px'>Match</th>
            <th>Bonus</th>
            <th>Prize</th>
            <th>Winners</th>
        </tr>
<?php
        $key = 0;
        foreach ( $aResults[0]['winnings'] as $aResult )
        {
            $key++;
            $sClass = '';
            if ( $key % 2 )
            {
                $sClass = 'alternate';
            }
?>
        <tr class="<?=$sClass?>">
            <td><?=$aResult['match']?></td>
            <td><?=$aResult['bonus']?></td>
            <td><?=$aResult['prize']?></td>
            <td><?=$aResult['winners']?></td>
        </tr>
<?php
        }
 ?>
    </table>
<?php } ?> 
    <br/>
    <strong>Next draw information:</strong>
    <br/>
    <strong>Jackpot amount: </strong><?=$aResults[0]['next_jackpot_amount']?><br/>
    <strong>Rollover? </strong><?=$aResults[0]['next_jackpot_rollover']==1?"Yes":"No"?><br/>
    
    <?php if (array_key_exists('ws', $_GET) && $_GET['ws']==1) { ?>
    <p>
        Thank you for entering the above information, which has been checked along with the information from the other two checkers for this lottery.
        <br/>
        As all three sets of information have been entered, the winscan has now been started.
    </p>
    <?php
        }
        else
        {
    ?>
     <p>
        Thank you for entering the above information, which will be checked once all three sets of checker data have been entered.
        <br/>
        If all three sets of information match, the data will be accepted as correct and the information
        will be displayed on the website.
        <br/>
        If there is any discrepancy, the data may have to be entered again.
    </p>   
    <?php
        }
    }
    else
    {
            echo "<div class='reportempty'>Draw Not Found</div>";
    }
}
else
{            
    // TODO: handle this better
    die("Sorry, there was a problem accessing the results you have just entered");
}


include ("includes/bottom.php");
?>