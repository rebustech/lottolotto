<?php
include ("includes/top.php");
$sTablename = $_GET["tablename"]; 
$iLimit = $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"];
$sFileName = basename($_SERVER["PHP_SELF"]);

?>
<script type="text/javascript" language="javascript">
	var btnToSwitch = null;
	function toggleBooleanField(obj, sIdField, iId, sField, sValue, sFilename){
		var ajaxUrl = "actions/generic_ajax.php?a=toggleBooleanField";
		var ajaxData = "&tablename=<?=$sTablename?>&idfield=" + sIdField + "&id=" + iId + "&field=" + sField + "&value=" + sValue + "&pp=members.php";
		btnToSwitch = obj;
		startAchtungAjax();
		doAchtungAjax(ajaxUrl, ajaxData, "toggleButton");	
	}
	
	
	function toggleButton(){
		var sCurrentAction = btnToSwitch.attr("action");
		if(sCurrentAction == 'enable'){ 
			btnToSwitch.attr("action", 'disable');
			btnToSwitch.children(":first").attr("src", "images/enabled.gif");
		}
		else if(sCurrentAction == 'disable'){ 
			btnToSwitch.attr("action", 'enable');
			btnToSwitch.children(":first").attr("src", "images/disabled.gif");
		}
	}
	
	var typingTimer;
	
	function searchRecord(sQuery, sField){
			clearTimeout(typingTimer);
			if(sQuery != ""){
				$("#clearbtn").css("visibility", "visible");
			}
			else{
				$("#clearbtn").css("visibility", "hidden");
				$("#searchText").val("");
			}
			$("#dialog_content").html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
			$("#dialog_content").load('actions/generic_ajax.php?a=genericTableSearch&tablename=<?=$sTablename?>&pp=members.php&filterstring=<?=urlencode($sFilters);?>&searchfield=' + sField + '&query=' + urlencode(sQuery), onDocumentReady);
	}
	
	function typingSearch(sQuery, sField){
		clearTimeout(typingTimer);
		typingTimer = setTimeout("searchRecord('" + sQuery + "', '" + sField + "')", 550);		   		return false;
	}
	
</script>
	<h1>Members</h1>
    <?php include("includes/errors.php"); ?>
    <?php if($bFiltersCheck){ ?>
    <ul class="subnav">
    	<li><a href="members.php?tablename=<?=$sTablename?>" class="largebutton selected">Listing</a></li></li>
    </ul>
	<?php if ($oErrors->getErrorCount() > 0) { ?>
        <table width="100%" class="detailsform"  border="0" cellspacing="0" cellpadding="5">
            <tr>
            	<td bgcolor="#FF0000" class="white"><?=$oErrors->getErrorString();?></td>
            </tr>
        </table><?php 
        $oErrors->clearErrors();
    }?>
    <div class="searchBox">
    <table width="100%">
        <tr>
   			<td align="center">
                Filter by 
                <?php 
				$aColumns = GenericTableAdmin::getTableColumns($sTablename); ?>
                <select id="searchField">
                <?php foreach($aColumns as $currentColumn){ 
				if(!strstr($currentColumn["Type"], "tinyint") && $currentColumn["Field"] != "password" && $currentColumn["Field"] != "pin"){
				?>
                <option value="<?=$currentColumn["Field"];?>"><?=$currentColumn["Field"];?></option>
                <?php
				}
				 } ?>
                </select> 
                <input id="searchText" type="text" onKeyUp="typingSearch($(this).val(), $('#searchField').val());" /><a class="clearbtn" <?php if($sReferenceNumber == ""){ ?>style="visibility: hidden;"<?php }?> onclick="return typingSearch('', $('#searchField').val());" href="#" id="clearbtn">x</a>
        </td>
        </tr>
        </table>
    </div>
     <div id="dialog_content" style="overflow: auto; position: relative;">
    <?php 
		$aData = GenericTableAdmin::getRecords($sTablename, GenericTableAdmin::createFilterStringForSql($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"], $sTablename), "member_id DESC", $iLimit);
		$aData["pp"] = $sFileName;
		echo GenericTableAdmin::createDataTable(
				array("Member ID|tiny|centeralign", "Username", "Firstname", "Lastname", "Balance", "Join Date", "Affiliate|tiny", "Verified|tiny|centeralign", "Active|tiny|centeralign", "Edit|tiny|centeralign", "Delete|tiny|centeralign"), 
				array("member_id|centeralign", "username", "firstname", "lastname", "balance", "join_date", "fk_affiliate_id", "member_id|ajaxbool:verified|centeralign", "member_id|ajaxbool:is_active|centeralign", "member_id|link:generic-details.php?pp=" . $sFileName . "&a=edit&tablename=" . $sTablename . "|centeralign",
					  "member_id|link:actions/generic_actions.php?pp=" . $sFileName . "&a=delete&tablename=" . $sTablename . "|centeralign"), 
					  $aData, $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]);
	?>
    </div>
<? 
	}
	else{ ?>
    <ul class="subnav">
    	<li><a href="generic-details.php?tablename=<?=$sTablename?>" class="largebutton">Insert New Record</a></li>
    </ul>
	<?php 
	}
include("includes/bottom.php"); ?>