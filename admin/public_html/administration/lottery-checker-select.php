<?php

/**
 * This script picks the 3 chekcers who will be responsible for checking and entering
 * the details (draw/prize) of the required lottery.
 */
if ( $_GET['l'] ) $bypass_login = true;
    // At the moment this script is being generated from the web
    // so include page construction code here
    include ("includes/top.php");
    include ("includes/errors.php");
?>
<h2>Lottery checker generator</h2>
<?php
    // Has a lottery ID been passed on the query string
    if ( $_GET['l'] )
    {
        // Basic sanitisation (should be improved?)
        $iLotteryID = (int) $_GET['l'];

        // Get generic lottery information for the supplied lottery ID
        $aLotteryInformation = LotteryAdmin::getLotteryDetails($iLotteryID);

        // Get live draw information for this lottery
        $aLiveDrawInformation = LotteryAdmin::getLiveDrawInformation($iLotteryID);

        // Get the next future draw date of this lottery
        $aNextDrawDetails = LotteryResultsAdmin::getNextLotteryDrawDate($iLotteryID);

        // Generate the draw reference from the year, month and date
        $iDraw = date("Ymd", strtotime($aNextDrawDetails['drawdate']));

        // Select the 3 random lottery checkers for this lottery
        $aCheckers = LotteryResultsAdmin::selectRandomCheckers($iLotteryID, 2, array('541'));

        // Do we have next draw details?
        if (!$aNextDrawDetails)
        {
            // We don't, so quit out here
            // TODO: Improve notification
            die("Could not get next draw details for lottery $iLotteryID (" . $aLotteryInformation['comment'] . ")");
        }

        // DO we have 3 checkers?
        if (count($aCheckers) < 2)
        {
            // We don't so quit out here
            // TODO: Improve notification
            var_dump($selectRandomCheckers);
            die("Could not get 2 checkers for this lottery");
        }

        // Do we have checkers already in place for this draw?
        if (LotteryResultsAdmin::doCheckersAlreadyExistForThisDraw($iLotteryID, $aNextDrawDetails['drawdate'], $iDraw) === true)
        {
            die("All checkers already exist for this draw!");
        }

        // At this point, we should have our array of 3 checkers
        // Sort it in ascending order of checker id
        krsort($aCheckers);

        // Pass the array of checkers to the function which will generate the rows in the checker array
        // and return the array with checking links
        $aCheckers = LotteryResultsAdmin::createCheckingRowsForLotteryDraw($iLotteryID, $aNextDrawDetails['drawdate'], $iDraw, $aCheckers);

        // Send out email code is below

        // Apart from the email code, all code is dispay-specific and will probably be redundant
        // as I suspect this code will be called via command line through a CRONTAB
        // or as part of another process requiring no 'visual' feedback
?>

<div id="results">
    <h4>Lottery checkers for the <?=$aLotteryInformation['comment']?> draw on <?=$aNextDrawDetails['drawdate']?></h4>

<?php
    $count = 0;
    foreach ($aCheckers as $iCheckerID=>$aCheckerInfo)
    {
/*
        EmailAdmin::sendEmailToChecker($iCheckerID, $aCheckerInfo, $iLotteryID, $iDraw,
                                       $aLotteryInformation['comment'], $aNextDrawDetails['drawdate'],
                                       $aLiveDrawInformation);

        $sLink = "http://{$_SERVER['HTTP_HOST']}/administration/add-results-checker.php?" .
                 "sh={$aCheckerInfo['security_hash']}&l={$iLotteryID}&c={$iCheckerID}&d={$iDraw}";

        // Write audit log entry to record email sent to this person
        $sAuditLogMsg = "Checking email for {$aLotteryInformation['comment']} draw " .
                        "on {$aNextDrawDetails['drawdate']} sent to {$aCheckerInfo['first_name']} (id {$iCheckerID}) " .
                        "and row stored in lottery_results_checking";
        AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_EMAIL_SENT', 'lottery_results_checking', $iCheckerID);
 * */

?>
    Checker <?=++$count?> is <?=$aCheckerInfo['first_name']?> (email <?=$aCheckerInfo['email_address']?>)<br/>
    Link is <a href="<?=$sLink;?>" target="_blank"><?=$sLink;?></a>
    <br/><br/>
<?php } ?>
</div>

<?php
      $aEmailData = array('checkers'=>$aCheckers,
                          'lottery_id'=>$iLotteryID,
                          'draw'=>$iDraw,
                          'lottery_name'=>$aLotteryInformation['comment'],
                          'draw_date'=>$aNextDrawDetails['drawdate']);

      EmailAdmin::sendEmailToAdministrator($aEmailData, 'checkers_generated');
    }
    else
    {
        // No lottery ID passed on the query string
        // If this script has been called via a browser,
        // show a choice of lotteries
        $aLotteries = LotteryAdmin::getLotteries(true);
        $aOutstandingLotteries = LotteryResultsAdmin::getLotteriesWithOutstandingChecks();
?>
<h3>Please select the required lottery</h3>
<div id="reports">
    <ul>
<?php
        foreach ($aLotteries as $aLottery )
        {
?>
        <li>
            <a href="<?=$oPage->sFilename?>?l=<?=$aLottery["lottery_id"]?>"><img src='images/lotto-<?=$aLottery["lottery_id"]?>.png' width="70px" /></a>
            <br/>
            <a href="<?=$oPage->sFilename?>?l=<?=$aLottery["lottery_id"]?>"><?=$aLottery["comment"];?></a>
        </li>
<?php
        }
?>
    </ul>
</div>
<?php
    }
    // Inlucde bottom of page construction code
    include ("includes/bottom.php");
