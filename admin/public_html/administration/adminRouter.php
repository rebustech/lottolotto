<?php

/**
 * This is the main entry point for the admin area. This file will route
 * requests to an appropriately named admin controller (must be a subclass of 
 * AdminController) with the correct method.
 * 
 * This currently still makes use of top.php and bottom.php from the original system
 */

/**
 * Work out where to route to and check that we have everything we need in the url
 */
$sQueryStringParts = explode('?', str_replace('/administration/', '', $_SERVER['REQUEST_URI']));
$sParts = explode('/', $sQueryStringParts[0]);

//If we are calling the index method of the route we can leave that off so use the 
//url segments as the class name completely
$sIndexBasedClassName = '\\' . implode('\\', $sParts);

//Otherwise Last part is method name and the class is the rest
$sMethodName = array_pop($sParts);
$sClassName = '\\' . implode('\\', $sParts);

if ($sClassName == '' || $sIndexBasedClassName == '') {
    header('Location: /administration/index.php');
    die();
}

/**
 * Looks good so far so start the system autoloader and perform security checks
 */
require_once("../system/includes/autoload.php");
include("security/checkauth.php");
include('includes/top.php');

$bValidTable = GenericTableAdmin::checkTableExists($sTablename);
if ($bValidTable && $iLangId && $sLanguageTablename) {
    if (!GenericTableAdmin::checkTableExists($sLanguageTablename)) {
        $bValidTable = false;
        echo "<span class='error'>Languages not applicable. Please set language filter to \'All\'.</span>";
    }
}

session_write_close();

try {
    if (class_exists($sClassName)) {
        //If the class exists we can make our way into the new system
        $oClass = new $sClassName;
        if (!array_key_exists(class_implements($sClassName), 'IAdminController')) {
            throw new LottoException('Router.InvalidClassPath', 'Route does not end in a class that implements IAdminController');
        }
        if (!method_exists($oClass, $sMethodName)) {
            throw new LottoException('Router.InvalidRoute', 'Controller does not implement the requested method');
        }
        echo $oClass->$sMethodName();
    } else {
        if (!class_exists($sIndexBasedClassName)) {
            throw new LottoException('Router.InvalidRouteController', 'No controller found to handle the request');
        }

        $sMethodName = 'index';
        $oClass = new $sIndexBasedClassName;
        if (!array_key_exists(class_implements($sIndexBasedClassName), 'IAdminController')) {
            throw new LottoException('Router.InvalidClassPath', 'Route does not end in a class that implements IAdminController');
        }
        if (!method_exists($sIndexBasedClassName, $sMethodName)) {
            throw new LottoException('Router.InvalidRoute', 'Controller does not implement the requested method');
        }
        
        echo $oClass->$sMethodName();
    }
} catch (LottoException $ex) {
    echo 'Route not found';
} catch (Exception $ex) {
    echo 'Something went wrong';
}

include("includes/bottom.php");
