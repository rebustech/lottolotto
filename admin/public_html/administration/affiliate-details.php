<?php
	include("includes/top.php"); 
	$iUserID = $_GET["id"];
	$_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::USERTYPE] = array(SecurityAdmin::AFFILIATE);
	$aTemplates = array();

	?>
 	<script type="text/javascript">
		function toggleMultipleSelect(obj){
			obj = $(obj);
			if(obj.is(":checked")){
				$('#subpages_' + obj.val()).show();
				$('#subpages_' + obj.val() + " input").attr("checked", "checked");
			}
			else{
				$('#subpages_' + obj.val()).hide();
				$('#subpages_' + obj.val() + " input").attr("checked", "");
			}
			
		}
		
		function toggleParent(obj, iParentID){
			obj = $(obj);
			if(!obj.is(":checked")){
				$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "");
			}
			else if(obj.is(":checked")){
				var aUnChecked = $("#subpages_" + iParentID + " input:not(:checked)");
				if(aUnChecked.length == 0){
					$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "checked");
				}
			}
		}
		
		function switchAdminType(obj){
			obj = $(obj);
			if(obj.val() == 1){
				$("#blacklist_presets").show();
				$("#blacklist_pages").show();
			}
			else{
				$("#blacklist_presets").hide();
				$("#blacklist_pages").hide();
				$("div.blacklist_subpages").hide();
				
				$("#blacklist_parentpages input").attr('checked','');
				$("div.blacklist_subpages input").attr('checked', '');
			}
			
			if(obj.val() == 3){
				$("#products").show();
			}
			else{
				$("#products").hide();
			}
			
		}
		
		function selectAll(iParentID){
			$("#subpages_" + iParentID + " input").attr('checked', 'checked');
			$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "checked");
		}
		
		function deselectAll(iParentID){
			$("#subpages_" + iParentID + " input").attr('checked', '');
			$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "");
		}
		
		function selectTemplate(sPageIds){
			sPageIds = sPageIds.split(',');
			$("#blacklist_pages input").attr("checked", "");
			$("div.blacklist_subpages").hide();
			for(var i = 0; i < sPageIds.length; i++){
				$("#blacklist_pages input[value=" + sPageIds[i] + "]").attr("checked", "checked");
				var iParentID = $("#blacklist_pages input[value=" + sPageIds[i] + "]").attr("parent");
				if(iParentID){
					$("#subpages_" + iParentID).show();
				}
				else{
					$("#subpages_" + sPageIds[i]).show();
				}
			}
			return false;
		}
		
		function toggleProductChanged(obj){
			obj = $(obj);
			var parentDiv = $(obj).parent();
			if(!obj.is(':checked')){ 
				parentDiv.remove();
				$("#unassignedproducts").append(parentDiv);
			}
			else{
				parentDiv.remove();
				$("#assignedproducts").append(parentDiv);
			}
		}
	</script>
	<h1>Affiliate Details</h1>
     <ul class="subnav">
    	<li><a href="affiliates.php?tablename=admin_users" class="largebutton">Listing</a></li><li><a href="affiliate-details.php?tablename=admin_users&pp=products.php<?php if($iUserID){?>?id=<?=$iUserID;?><?php } ?>" class="largebutton selected"><?php if($iUserID){?>Edit User<?php }else{ ?>Add New User<?php } ?></a></li>
    </ul>
    <?php include("includes/errors.php");
	if($_GET["id"] && $aUserData = UsersAdmin::getUserDetails($_GET["id"], SecurityAdmin::AFFILIATE) ) {
	 ?>
     <form name="accountdetailsform" id="accountdetailsform" action="actions/users_ajax.php?a=saveDetails&pp=<?=$sCurrentFilename?>&id=<?=$_GET["id"]?>&pp=affiliates.php&tablename=admin_users" class="ajaxform" method="post">
     <input type="hidden" name="at" value="<?=SecurityAdmin::AFFILIATE?>" />
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Modify Affiliate Details</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">Username</th>
            <td><input type="text" name="username" class="required"  value="<?=$aUserData["username"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">First name</th>
            <td><input type="text" name="firstname" class="required"  value="<?=$aUserData["firstname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Last name</th>
            <td><input type="text" name="lastname" class="required"  value="<?=$aUserData["lastname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Email</th>
            <td><input type="text" name="email" class="required email medium"  value="<?=$aUserData["email"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Address</th>
            <td>
            	<input type="text" name="address1" class="required medium"  value="<?=$aUserData["address1"]?>"/><p />
                <input type="text" name="address2" class="medium" value="<?=$aUserData["address2"]?>"/>
            </td>
       </tr>
    	<tr>
        	<th align="right" class="small">ZIP Code</th>
            <td>
            	<input type="text" name="zip" class="required"  value="<?=$aUserData["zipcode"]?>"/>
            </td>
       </tr>
    	<tr>
        	<th align="right" class="small">City</th>
            <td>
            	<input type="text" name="city" class="required"  value="<?=$aUserData["city"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Country</th>
            <td>
            	<input type="text" name="country" class="required"  value="<?=$aUserData["country"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Telephone</th>
            <td>
            	<input type="text" name="telephone1" class="required"  value="<?=$aUserData["tel1"]?>"/><p />
            	<input type="text" name="telephone2"  value="<?=$aUserData["tel2"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Fax</th>
            <td>
            	<input type="text" name="fax"  value="<?=$aUserData["fax"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Website</th>
            <td><input type="text" name="website" class="url required medium"  value="<?=$aUserData["web"];?>"/><br /><small>http://www.example.com</small></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Company</th>
            <td>
            	<input type="text" name="company" value="<?=$aUserData["company"];?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Commission</th>
            <td><input type="text" name="commission" class="required number" value="<?=$aUserData["commission"];?>"/><br /><small>Example: Enter 0.25 for 25%</small></td>
       </tr>
    	<tr>
        	<th align="right" class="small">VAT Number</th>
            <td>
            	<input type="text" name="vat" value="<?=$aUserData["vatno"];?>"/>
           </td>
       </tr>
       <tr>
        	<th align="right" class="small">Contract Signed</th>
            <td>
            	<?=$aUserData['contract']?'<strong>Yes</strong>':'<strong>No</strong>'?>
           </td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Save Details" />
            </th>
        </tr>
    </table>
  	</form>
    <p />
<form name="changepasswordform" action="actions/users_ajax.php?a=changePassword&pp=affiliates.php&tablename=admin_users&id=<?=$iUserID;?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change Password</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">New password</th>
            <td><input type="password" name="passwd" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm password</th>
            <td><input type="password" name="passwd2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change Password" />
            </th>
       </tr>
    </table>
  	</form>
    <p />
<form name="changepinform" action="actions/users_ajax.php?a=changePin&pp=affiliates.php&tablename=admin_users&id=<?=$iUserID;?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change PIN Number</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">New PIN</th>
            <td><input type="password" name="pin" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm PIN</th>
            <td><input type="password" name="pin2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change PIN" />
            </th>
       </tr>
    </table>
    </form>
    <?php }else{ 
	$aData = $_SESSION[$oSecurityObject->getsAdminType() . "formdata"];
	?>
 <form name="accountdetailsform" id="accountdetailsform" action="actions/users_actions.php?a=add&pp=<?=$sCurrentFilename?>&id=<?=$_GET["id"]?>&pp=affiliates.php&tablename=admin_users" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Create new Affiliate</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">Username</th>
            <td><input type="text" name="username" class="required" value="<?=$aData["username"];?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Password</th>
            <td><input type="password" name="passwd" class="required" /></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm Password</th>
            <td><input type="password" name="passwd2" class="required" /></td>
       </tr>
    	<tr>
    	<tr>
        	<th align="right" class="small">PIN</th>
            <td><input type="password" name="pin" class="required" /></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm PIN</th>
            <td><input type="password" name="pin2" class="required" /></td>
       </tr>
        	<th align="right" class="small">First name</th>
            <td><input type="text" name="firstname" class="required"  value="<?=$aData["firstname"];?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Last name</th>
            <td><input type="text" name="lastname" class="required"  value="<?=$aData["lastname"];?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Email</th>
            <td><input type="text" name="email" class="required medium" value="<?=$aData["email"];?>" /></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Telephone</th>
            <td>
            	<input type="text" name="telephone1" class="required" value="<?=$aData["tel1"];?>"/><p />
            	<input type="text" name="telephone2" value="<?=$aData["tel2"];?>"/>
           </td>
       </tr>
       <tr>
        	<th align="right" class="small">Address</th>
            <td>
            	<input type="text" name="address1" class="required medium"  value="<?=$aData["address1"]?>"/><p />
                <input type="text" name="address2" class="medium" value="<?=$aData["address2"]?>"/>
            </td>
       </tr>
    	<tr>
        	<th align="right" class="small">ZIP Code</th>
            <td>
            	<input type="text" name="zip" class="required"  value="<?=$aData["zipcode"]?>"/>
            </td>
       </tr>
    	<tr>
        	<th align="right" class="small">City</th>
            <td>
            	<input type="text" name="city" class="required"  value="<?=$aData["city"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Country</th>
            <td>
            	<input type="text" name="country" class="required"  value="<?=$aData["country"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Fax</th>
            <td>
            	<input type="text" name="fax"  value="<?=$aData["fax"]?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Company</th>
            <td>
            	<input type="text" name="company"  class="required" value="<?=$aData["company"];?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">VAT Number</th>
            <td>
            	<input type="text" name="vat" class="required"  value="<?=$aData["vatno"];?>"/>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Website</th>
            <td>
            	<input type="text" name="web" class="required medium" value="<?=$aData["web"];?>"/><br /><small>http://www.example.com</small>
           </td>
       </tr>
    	<tr>
        	<th align="right" class="small">Commission</th>
            <td>
            	<input type="text" name="commission" class="required number" value="<?=($aData["commission"])?$aData["commission"]:Affiliates::getCommissionRate();?>"/><br /><small>Example: Enter 0.25 for 25%</small>
           </td>
       </tr>
       <tr>
       		<th align="right" class="small">User Type</th>
            <td><input type="hidden" name="fk_admintype_id" value="<?=SecurityAdmin::AFFILIATE?>" />Affiliate</td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Create User" /> 
                <input type="button" value="Cancel" onclick="document.location = 'affiliates.php?tablename=admin_users'"/>	
            </th>
        </tr>
    </table>
	</form>
	<?php }
unset($_SESSION[$oSecurityObject->getsAdminType() . "formdata"]);
include("includes/bottom.php"); ?>