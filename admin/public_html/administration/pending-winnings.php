<?php
/**
 * Pending winnings checks
 */
include ("includes/top.php");
include ("includes/errors.php");
?>
<h2>Pending Winnings</h2>
<div id="results">
<?php 

$aReport = new ReportsAdmin($oSecurityObject, "UnapprovedWinnings", NULL);
// Get the lottery rows that require checking
$aData = $aReport->getDataset();
if ($_POST['confirm_approve'])
{
    $success = 0;
    $aFailureArray = array();
    $aDataToApprove = unserialize($_POST['approve_data']);
    foreach ($aDataToApprove as $aTransactionInfo)
    {
        $aTransactionInfo['member_id'] = $aTransactionInfo['fk_member_id'];
        if (LotteryResultsAdmin::addWinTransactionConfirmed($aTransactionInfo))
        {
            $success++;
        }
        else
        {
            $aFailureArray[] = $aTransactionInfo;
        }
            
    }
    $failures = count($aFailureArray);
?>
<?=$success;?> transactions approved with  <?=$failures;?> failures.
<ul class="subnav">
    <li>
        <a href="/administration/resultsManager/pendingWinnings" class="largebutton">Return to winnings manager</a>
    </li>
</ul>
<?php
print_d($aFailureArray);
}
else if ($_POST['approve_winnings'])
{
    $fApprovalLimit = number_format($_POST['approve_value'],2);
    
    $aToApprove = array();
    $count = 0;    
    foreach ($aData as $key=>$aRow)
    {        
        if ($aRow['amount'] <= $_POST['approve_value'] && $aRow['ignore']!='1')
        {
            // Add some extra elements to the array here
            // to ensure the transaction is fully updated correctly
            $aRow['member_id'] = $aRow['fk_member_id'];
            $aRow['transaction_amount'] = $aRow['amount'];
            $aRow['transaction_ref'] = $aRow['gatewayreference'];
            
            $aToApprove[] = $aRow;
            $count++;
        }
    }
    $sDataToApprove = serialize($aToApprove);    
?>
<?=$count;?> pending transactions equal to or below <?=$fApprovalLimit;?> have been found. 
<?php if ($count > 0) { ?>
Approve transactions?
<br/>
<form method="post" action="">
    <input type="hidden" name="approve_value" value='<?=$fApprovalLimit;?>' />
    <input type="hidden" name="approve_data" value='<?=$sDataToApprove;?>' />
    <input type="submit" name="confirm_approve" value="Confirm"/>
</form>
<?php } ?>
<ul class="subnav">
    <li>
        <a href="<?=$_SERVER['REQUEST_URI']?>" class="largebutton">Return to transactions list</a>
    </li>
</ul>
<?php
}
else 
{
    
?>


<?php


    
    // Do we have rows?
    if (count($aData) == 0) {
        echo "<strong>No pending transactions found</strong>";
    }
    else
    {
        
?>
    <div>
        <form method="POST" action="" />
            Approve all pending winnings of value <b>BELOW</b> <input type="text" name="approve_value" size="6" maxlength="10" value="<?=$_GET['x']?>"/>
            <input type="submit" name="approve_winnings" value="Approve"/>
        </form>
    </div>
    <br/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th>Transaction Date</th>
            <th>Transaction Reference</th>
            <th>Booking Reference</th>
            <th>Prize Won</th>
            <th>Win Amount</th>
            <th>Winning Member</th>
            <th>Checked</th>
            <th>Pay?</th>
        </tr>
<?php           
            
        // Loop through the winnings
        foreach ( $aData as $key=>$aRow)
        {
            
            $sClass = '';
            // Output each prize level's winnings and winners
            // and whether there was a mismatch or not
            if ( $key % 2==0 )
            {
                $sClass = "alternate";
            }
?>
        <tr class='<?=$sClass?>'>
            <td><?=$aRow['Transaction Date'];?></td>
            <td><a href="/administration/generic-details.php?pp=transactions.php&a=edit&tablename=transactions&pp=transactions.php&id=<?=$aRow['transaction_id'];?>"><?=$aRow['transaction_id'];?></a></td>
            <td><?=$aRow['bookingreference'];?></td>
            <td><?=$aRow['gatewayreference'];?></td>
            <td><?=$aRow['symbol'];?><?=number_format($aRow['amount'],2,'.',',')?></tt></td>
            <td><?=$aRow['email'];?></td>
            <td><?=($aRow['checked'])?'<i class="fa fa-check"></i>':'';?></td>
            <td><?=($aRow['ignore'])?'<i class="fa fa-times"></i>&nbsp;&nbsp;Withold Payment':'<i class="fa fa-check"></i>&nbsp;&nbsp;Will be paid';?></td>
        </tr>
 <?php                   
        }
 ?>
    </table>
    
    
<?php
    }
    
}
?>
</div>
<br/>
<?php
include ("includes/bottom.php");
