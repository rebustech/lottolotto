<?php
/**
 * This script is executed when a checker clicks on the 'cancel' link in the email
 * they receieved whenn they are selected as one of the lottery checkers
 */

$bypass_login = true;
    include ("includes/top.php");
    include ("includes/errors.php");
    
    //sh={$aCheckerInfo['security_hash']}&l={$iLotteryID}&c={$iCheckerID}&d={$iDraw}
    
    // Get values from query string
    $sSecurityHash = $_GET['sh'];
    $iDraw = $_GET['d'];
    $iLotteryID = $_GET['l'];
    $iCheckerID = $_GET['c'];
     
    // Check the supplied security hash against the other supplied values
    $bHashCheck = LotteryResultsAdmin::checkSecurityHash($sSecurityHash, $iDraw, $iLotteryID, $iCheckerID);
    
?>
<h2>Lottery checker - cancel check</h2>
<?php 
    
    if ($bHashCheck)
    {
        // Hash check passed, so on with the motley!
         

        // Get details of who was supposed to be doing this check
        $aCurrentChecker = LotteryResultsAdmin::getCheckerByHash($sSecurityHash);
        
        // Get further lottery information
        $aLotteryInformation = LotteryAdmin::getLotteryDetails($iLotteryID);
        
        // Get live draw information for this lottery
        $aLiveDrawInformation = LotteryAdmin::getLiveDrawInformation($iLotteryID);
        
        // Get lottery date
        $aNextDrawDetails = LotteryResultsAdmin::getLotteryDrawDateFromDrawRef($iDraw, $iLotteryID);
        
        // Write audit log entry to record click of cancel link
        $sAuditLogMsg = "{$aCurrentChecker['first_name']} clicked cancelled check link " .
                        "for {$aLotteryInformation['comment']} draw " .
                        "on {$aNextDrawDetails['drawdate']}"; 
        AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_CANCEL', 'lottery_results_checking', $iCheckerID);
        
        
        // Set this checker as 'unable', and get list of all checkers assigned to this lottery
        $aExistingCheckers = LotteryResultsAdmin::updateCheckerAsUnable($sSecurityHash, $iDraw, $iLotteryID);
        
        // Get a new checker for this lottery
        $aNewChecker = LotteryResultsAdmin::selectRandomCheckers($iLotteryID, 1, $aExistingCheckers);
 
        // Get new checker's security hash
        $aNewChecker= LotteryResultsAdmin::createCheckingRowsForLotteryDraw($iLotteryID, $aNextDrawDetails['drawdate'], $iDraw, $aNewChecker);
        
        // Get checker ID
        $iCheckerID = key($aNewChecker);
        
        // Checker info is at the end of the array, so pick that up
        $aCheckerInfo = end($aNewChecker);
        reset($aNewChecker);
        
        // Write audit log message
        $sAuditLogMsg = "Checking email for {$aLotteryInformation['comment']} draw " .
                        "on {$aNextDrawDetails['drawdate']} sent to {$aCheckerInfo['first_name']} (id {$iCheckerID}) " .
                        "and row stored in lottery_results_checking";
                        
        AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_EMAIL_SENT_REPLACE', 'lottery_results_checking', $iCheckerID);
                
        
        // Send email to administratiors informing that a check has been cancelled
        $aEmailData = array('old_checker_name'=>$aCurrentChecker['first_name'],
                            'new_checker_name'=>$aCheckerInfo['first_name'],
                            'new_checker_email'=>$aCheckerInfo['email_address'],
                            'lottery_name'=>$aLotteryInformation['comment'],
                            'draw_date'=>$aNextDrawDetails['drawdate']);

        EmailAdmin::sendEmailToAdministrator($aEmailData, 'checker_cancelled');
        
        
?>

<div id="results">
    <h4><?=$aLotteryInformation['comment']?> draw on <?=$aNextDrawDetails['drawdate']?></h4>
    <p>Thank you, you have cancelled your check for the above mentioned lottery.  
</div>


<?php
    }
    else
    {
?>
<h3></h3>
<div id="reports">
<?php
    die("You are unauthorised to access this page");
?>
</div>
<?php
    }

    include ("includes/bottom.php");
