<?php
	include("includes/top.php"); 
	$iUserID = $_GET["id"];
	$aTemplates = array();
	$aTemplates["Site Administrator"] = "";
	
	$aTemplates["Lottery Management"] = "1,2";
	
	$aTemplates["Ticket Management"] = "1,2,3,4";
	
	?>
 	<script type="text/javascript">
		function toggleMultipleSelect(obj){
			obj = $(obj);
			if(obj.is(":checked")){
				$('#subpages_' + obj.val()).show();
				$('#subpages_' + obj.val() + " input").attr("checked", "checked");
			}
			else{
				$('#subpages_' + obj.val()).hide();
				$('#subpages_' + obj.val() + " input").attr("checked", "");
			}
			
		}
		
		function toggleParent(obj, iParentID){
			obj = $(obj);
			if(!obj.is(":checked")){
				$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "");
			}
			else if(obj.is(":checked")){
				var aUnChecked = $("#subpages_" + iParentID + " input:not(:checked)");
				if(aUnChecked.length == 0){
					$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "checked");
				}
			}
		}
		
		function switchAdminType(obj){
			obj = $(obj);
			if(obj.val() == 1){
				$("#blacklist_presets").show();
				$("#blacklist_pages").show();
			}
			else{
				$("#blacklist_presets").hide();
				$("#blacklist_pages").hide();
				$("div.blacklist_subpages").hide();
				
				$("#blacklist_parentpages input").attr('checked','');
				$("div.blacklist_subpages input").attr('checked', '');
			}
			
			if(obj.val() == 3){
				$("#products").show();
			}
			else{
				$("#products").hide();
			}
			
		}
		
		function selectAll(iParentID){
			$("#subpages_" + iParentID + " input").attr('checked', 'checked');
			$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "checked");
		}
		
		function deselectAll(iParentID){
			$("#subpages_" + iParentID + " input").attr('checked', '');
			$("#blacklist_parentpages input[value=" + iParentID + "]").attr("checked", "");
		}
		
		function selectTemplate(sPageIds){
			sPageIds = sPageIds.split(',');
			$("#blacklist_pages input").attr("checked", "");
			$("div.blacklist_subpages").hide();
			for(var i = 0; i < sPageIds.length; i++){
				$("#blacklist_pages input[value=" + sPageIds[i] + "]").attr("checked", "checked");
				var iParentID = $("#blacklist_pages input[value=" + sPageIds[i] + "]").attr("parent");
				if(iParentID){
					$("#subpages_" + iParentID).show();
				}
				else{
					$("#subpages_" + sPageIds[i]).show();
				}
			}
			return false;
		}
		
		function toggleProductChanged(obj){
			obj = $(obj);
			var parentDiv = $(obj).parent();
			if(!obj.is(':checked')){ 
				parentDiv.remove();
				$("#unassignedproducts").append(parentDiv);
			}
			else{
				parentDiv.remove();
				$("#assignedproducts").append(parentDiv);
			}
		}
	</script>
	<h1>User Details</h1>
     <ul class="subnav">
    	<li><a href="users.php?tablename=admin_users" class="largebutton">Listing</a></li><li><a href="users-details.php?tablename=admin_users&pp=products.php<?php if($iUserID){?>?id=<?=$iUserID;?><?php } ?>" class="largebutton selected"><?php if($iUserID){?>Edit User<?php }else{ ?>Add New User<?php } ?></a></li>
    </ul>
    <?php include("includes/errors.php");
	if($_GET["id"]){ $aUserData = UsersAdmin::getUserDetails($_GET["id"]);
	 ?>
     <form name="accountdetailsform" id="accountdetailsform" action="actions/users_ajax.php?a=saveDetails&pp=<?=$sCurrentFilename?>&id=<?=$_GET["id"]?>&pp=users.php&tablename=admin_users" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Modify User Details</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">Username</th>
            <td><input type="text" name="username" class="required"  value="<?=$aUserData["username"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">First name</th>
            <td><input type="text" name="firstname" class="required"  value="<?=$aUserData["firstname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Last name</th>
            <td><input type="text" name="lastname" class="required"  value="<?=$aUserData["lastname"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Email</th>
            <td><input type="text" name="email" class="required medium"  value="<?=$aUserData["email"]?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Telephone</th>
            <td>
            	<input type="text" name="telephone1" class="required"  value="<?=$aUserData["tel1"]?>"/><p />
            	<input type="text" name="telephone2"  value="<?=$aUserData["tel2"]?>"/>
           </td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Save Details" />
            </th>
        </tr>
    </table>
  	</form>
    <p />
    <form name="changepermissionsform" action="actions/users_ajax.php?a=savePermissions&pp=users.php&tablename=admin_users&id=<?=$iUserID;?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Admin Permissions</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">User type</th>
            <?php 
			$aAdminTypes = UsersAdmin::getAdminTypes(); 
			$aSelectedAdminType = NULL;
			?>
            <td><select name="fk_admintype_id" onchange="switchAdminType(this);">
            <?php foreach($aAdminTypes as $aCurrentAdminType){ ?>
            	<option value="<?=$aCurrentAdminType["id"]?>" <?php 
				if($aUserData["fk_admintype_id"] == $aCurrentAdminType["id"]){ $aSelectedAdminType = $aCurrentAdminType; ?> selected <?php } ?> <?php if($aCurrentAdminType["is_active"] == 0){ ?>style="color: #aaa;"<?php } ?>><?=$aCurrentAdminType["adminName"];?></option>
            <?php } ?>
            </select></td>
       </tr>
    	<tr id="blacklist_presets" <?php if($aSelectedAdminType["check_blacklist"] != 1){ ?>style="display: none;"<?php } ?>>
        	<th align="right" class="small">Blacklist Presets</th>
            <td>
            <?php
				foreach($aTemplates as $title => $currentTemplate){
			?>
            	<a href="#" onclick="return selectTemplate('<?=$currentTemplate?>');"><?=$title?></a>&nbsp;
            <?php } ?>
            </td>
       </tr>
    	<tr id="blacklist_pages" <?php if($aSelectedAdminType["check_blacklist"] != 1){ ?>style="display: none;"<?php } ?>>
        	<th align="right" class="small">Blacklist</th>
            <td>
            	<?php $aPages = AdminPage::getAdminPagesForBlacklist($iUserID,0); 
				$aSubPagesHtml = array();
				?>
                <div style="float: left; margin: 2px; position: relative; width: 30%; background-color: #eee; padding: 5px;" id="blacklist_parentpages">
                	<small>Admin Pages</small>
                    <div style="height: 200px; overflow-y: scroll; margin-top: 2px; background-color: #fff;">
                    <input type="hidden" name="pages[]" value="" />
                	<?php foreach($aPages as $aPage){ ?>
                        <input type="checkbox" name="pages[]" id="page_<?=$aPage['id']?>" value="<?=$aPage['id']?>" onclick="toggleMultipleSelect(this)" <?php if($aPage["is_blacklisted"]){ ?>checked="checked"<?php } ?>/><label for="page_<?=$aPage['id']?>"><?=$aPage["title"]?></label><br />
                        <?php 
						$tempSubPages = AdminPage::getAdminPagesForBlacklist($iUserID, $aPage["id"]);
						if(!empty($tempSubPages)){
							$bShowDiv = false;
							$childrenHtml = "";
							foreach($tempSubPages as $aSubPage){
								$childrenHtml .= "<input type=\"checkbox\" name=\"pages[]\" id=\"subpage_{$aPage['id']}_{$aSubPage['id']}\" parent=\"{$aPage['id']}\" value=\"{$aSubPage['id']}\" onclick=\"toggleParent(this,'{$aPage['id']}');\"";
								if($aSubPage["is_blacklisted"]){
									$childrenHtml .= " checked=\"checked\"";
									$bShowDiv = true;
								}
								
								$childrenHtml .= "/>";
								$childrenHtml .= "<label for=\"subpage_{$aPage['id']}_{$aSubPage['id']}\">" . $aSubPage["title"] . "</label>";
								$childrenHtml .= "<br />";
							}
						$tempHtml = "<div id=\"subpages_{$aPage['id']}\" style=\"float: left; margin: 2px; width: 30%; background-color: #eee; position: relative; padding: 5px;";
							if(!$bShowDiv){
								$tempHtml .= " display: none;";
                            }
							$tempHtml .= "\" class=\"blacklist_subpages\">";
							$tempHtml .= "<small>{$aPage['title']}</small>";
							$tempHtml .= "<div style='position: absolute; right: 5px; top: 3px; font-size: 8pt;'><a href=\"#\" onclick=\"selectAll({$aPage['id']}); return false;\">All</a> <a href=\"#\" onclick=\"deselectAll({$aPage['id']}); return false;\">None</a></div>";
							$tempHtml .= "<div style=\"height: 200px; overflow-y: scroll;  margin-top: 2px; background-color: #fff;\">";
							$tempHtml .= $childrenHtml . "</div></div>";
							array_push($aSubPagesHtml, $tempHtml);
						} ?>
                    <?php } ?>
                </select>
                	</div>
                </div>
				<?php foreach($aSubPagesHtml as $tempHtml){ ?>
					<?=$tempHtml;?>
				<?php } ?>
           	
            </td>
       </tr>
       <tr>
       		<th colspan="2" align="right" id="jsconsole">
            	<input type="submit" value="Save Permissions" />
            </th>
       </tr>
    </table>
  	</form>
    <p />
<form name="changepasswordform" action="actions/users_ajax.php?a=changePassword&pp=users.php&tablename=admin_users&id=<?=$iUserID;?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change Password</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">New password</th>
            <td><input type="password" name="passwd" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm password</th>
            <td><input type="password" name="passwd2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change Password" />
            </th>
       </tr>
    </table>
  	</form>
    <p />
<form name="changepinform" action="actions/users_ajax.php?a=changePin&pp=users.php&tablename=admin_users&id=<?=$iUserID;?>" class="ajaxform" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Change PIN Number</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">New PIN</th>
            <td><input type="password" name="pin" class="required"  value=""/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm PIN</th>
            <td><input type="password" name="pin2" class="required"  value=""/></td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Change PIN" />
            </th>
       </tr>
    </table>
    </form>
    <?php }else{ 
	$aData = $_SESSION[$oSecurityObject->getsAdminType() . "formdata"];
	?>
 <form name="accountdetailsform" id="accountdetailsform" action="actions/users_actions.php?a=add&pp=<?=$sCurrentFilename?>&id=<?=$_GET["id"]?>&pp=users.php&tablename=admin_users" method="post">
    <table class="detailsform">
    	<tr>
        	<td colspan="2">
            	<strong>Create new user</strong>
            </td>
        </tr>
    	<tr>
        	<th align="right" class="small">Username</th>
            <td><input type="text" name="username" class="required" value="<?=$aData["username"];?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Password</th>
            <td><input type="password" name="passwd" class="required" /></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm Password</th>
            <td><input type="password" name="passwd2" class="required" /></td>
       </tr>
    	<tr>
    	<tr>
        	<th align="right" class="small">PIN</th>
            <td><input type="password" name="pin" class="required" /></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Confirm PIN</th>
            <td><input type="password" name="pin2" class="required" /></td>
       </tr>
        	<th align="right" class="small">First name</th>
            <td><input type="text" name="firstname" class="required"  value="<?=$aData["firstname"];?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Last name</th>
            <td><input type="text" name="lastname" class="required"  value="<?=$aData["lastname"];?>"/></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Email</th>
            <td><input type="text" name="email" class="required" value="<?=$aData["email"];?>" /></td>
       </tr>
    	<tr>
        	<th align="right" class="small">Telephone</th>
            <td>
            	<input type="text" name="telephone1" class="required" value="<?=$aData["tel1"];?>"/><p />
            	<input type="text" name="telephone2" value="<?=$aData["tel2"];?>"/>
           </td>
       </tr>
       <tr>
       		<th align="right" class="small">User Type</th>
            <td><select name="fk_admintype_id">
            <?php 
			$aAdminTypes = UsersAdmin::getAdminTypes(); 
			foreach($aAdminTypes as $aCurrentAdminType){ ?>
            	<option value="<?=$aCurrentAdminType["id"];?>" <?php if($aCurrentAdminType["is_active"] == 0){ ?>style="color: #aaa;"<?php } ?>><?=$aCurrentAdminType["adminName"];?></option>
            <?php } ?>
            </select> Do not forget to set permissions before changing to active.
            </td>
       </tr>
       <tr>
       		<th colspan="2" align="right">
            	<input type="submit" value="Create User" /> 
                <input type="button" value="Cancel" onclick="document.location = 'users.php?tablename=admin_users'"/>	
            </th>
        </tr>
    </table>
	</form>
	<?php }
unset($_SESSION[$oSecurityObject->getsAdminType() . "formdata"]);
include("includes/bottom.php"); ?>