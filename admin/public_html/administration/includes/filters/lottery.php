<?php 
$sPath = $_SERVER["PHP_SELF"];
$aPath = explode("/", $sPath);
$sFileName = $aPath[sizeof($aPath) - 1];

$currentFilterId = $currentFilter["id"];
if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId])){
	$currentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId];
}
else{
	$currentFilterData = false;
}
$aSelectedTypes = array();
 ?> 
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilter["id"]; ?>_filtercontent');  return false;" <?php if($currentFilterData){ ?>class="selected"<?php } ?>><?=$currentFilter["title"]; ?> 
    <img id="<?=$currentFilter["id"]; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilter["id"]; ?>_filtercontent" >
   		<div id="<?=$currentFilter["id"]; ?>_html">
    	<table>
        <?php
            $aLotteries = LotteryAdmin::getLotteries();
            foreach ($aLotteries as $key => $value) 
            { ?>
                <tr><td><input type="checkbox" id="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["lottery_id"];?>" onmouseover="checkDragSelect(this.id)" name="filter_<?=$currentFilterId;?>[]" value="<?=$value["lottery_id"];?>" <?php if($currentFilterData && in_array($value["lottery_id"], $currentFilterData)){ array_push($aSelectedTypes, $value["lottery_id"]); echo " checked"; }?>/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["lottery_id"];?>"><?php echo $value["comment"]; ?></label></td></tr>
            <?php 
            }
        ?>
        </table>
        </div>
    </div>
</div>