<?php 
$sPath = $_SERVER["PHP_SELF"];
$aPath = explode("/", $sPath);
$sFileName = $aPath[sizeof($aPath) - 1];

$currentFilterId = $currentFilter["id"];
if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId])){
	$currentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId];
}
else{
	$currentFilterData = false;
}
$aSelectedTypes = array();
 ?> 
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilter["id"]; ?>_filtercontent');  return false;" <?php if($currentFilterData){ ?>class="selected"<?php } ?>><?=$currentFilter["title"]; ?> 
    <img id="<?=$currentFilter["id"]; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilter["id"]; ?>_filtercontent" >
   		<div id="<?=$currentFilter["id"]; ?>_html">
    	<table>
        <?php
            $aProductTypes = ProductTypes::getProductTypes();
            foreach ($aProductTypes as $key => $value) 
            { ?>
                <tr><td><input type="checkbox" id="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" onclick="typeChanged(this.value);" name="filter_<?=$currentFilterId;?>[]" value="<?=$value["id"];?>" <?php if($currentFilterData && in_array($value["id"], $currentFilterData)){ array_push($aSelectedTypes, $value["id"]); echo " checked"; }?>/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["title"]; ?></label></td></tr>
            <?php 
            }
        ?>
        </table>
        </div>
    </div>
</div>
<script type="text/javascript">
	function typeChanged(selectedType){
		if($("<?=AdminPage::PRODUCTCATEGORY?>_filtercontent")){
			$("#" + "<?=AdminPage::PRODUCTCATEGORY?>_html").load("actions/filter_ajax.php?a=getProductCategories&selectedFilter=<?=$currentFilter["id"]; ?>&selectedValue="  + selectedType + "&pp=<?=$sFileName?>&tablename=<?=$_GET['tablename']?>");
		}
		if($("<?=AdminPage::PRODUCTRANKING?>filtercontent")){
			$("#" + "<?=AdminPage::PRODUCTRANKING?>_html").load("actions/filter_ajax.php?a=getProductRankings&pp=<?=$sFileName?>&tablename=<?=$_GET['tablename']?>");
		}
		if($("<?=AdminPage::PRODUCT?>_filtercontent")){
			$("#" + "<?=AdminPage::PRODUCT?>_html").load("actions/filter_ajax.php?a=getProducts&selectedFilter=<?=$currentFilter["id"]; ?>&selectedValue="  + selectedType + "&pp=<?=$sFileName?>&tablename=<?=$_GET['tablename']?>");
		}
	}
</script>