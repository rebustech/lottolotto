<?php 
$currentFilterId = $currentFilter["id"];
if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId])){
	$currentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId];
}
else{
	$currentFilterData = false;
}
 ?> 
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilter["id"]; ?>_filtercontent');  return false;" <?php if($currentFilterData){ ?>class="selected"<?php } ?>><?=$currentFilter["title"]; ?> 
    <img id="<?=$currentFilter["id"]; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilter["id"]; ?>_filtercontent" >
	    <div id="<?=$currentFilter["id"]; ?>_html">
		<table>
        <?php
			if(!$aSelectedUserTypes){
				if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::USERTYPE]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::USERTYPE])){
				$aSelectedUserTypes = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::USERTYPE];
				}
			}
			if(!empty($aSelectedUserTypes)){
				$bUsers = false;
				$iGlobalKey = 1;
				foreach($aSelectedUserTypes as $currentUserType){
					$aUsers = UsersAdmin::getUsersByType($currentUserType);
					foreach ($aUsers as $key => $value) 
					{ 
						$iGlobalKey++;
						$bUsers = true;
					?>
						<tr><td><input id="filter_<?=AdminPage::USER?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" type="checkbox"  title="<?=$value["username"];?> onclick="usertypeChanged(this.value);" name="filter_<?=AdminPage::USER?>[]" value="<?=$value["id"];?>"/><label title="<?=$value["username"];?>" for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["firstname"] . " " . $value["lastname"]; ?></label></td></tr>
					<?php 
					}
				}
				if(!$bUsers){
				?>
               	<tr><td>No users for selected user types</td></tr>	
                <?php
				}
			}
			else{
				?>
                <tr><td>Select a user type</td></tr>	
                <?php
			}
        ?>
        </table>
        </div>
    </div>
</div>