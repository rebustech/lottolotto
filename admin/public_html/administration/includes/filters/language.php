<?php 

$currentFilterId = 1;
 ?> 
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilterId; ?>_filtercontent');  return false;" <?php if($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"]){ ?>class="selected"<?php } ?>>Language 
    <img id="<?=$currentFilterId; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilterId; ?>_filtercontent" <?php if(!$_SESSION[$oSecurityObject->getsAdminType() ."iLangID"]){ ?>style="display: none"<?php } ?>>
   		<div id="<?=$currentFilterId; ?>_html">
    	<table>
        	<tr><td><input id="filter_<?=$currentFilterId;?>_all_0" type="radio" name="filter_<?=$currentFilterId;?>[]" value="0" <?php if(!$_SESSION[$oSecurityObject->getsAdminType() . "languageid"]){ echo " checked"; }?>/><label for="filter_<?=$currentFilterId;?>_all_0">All</label></td></tr>
        <?php
				$aLanguages = Language::getAllAdminLanguageList();
				foreach ($aLanguages as $key => $value) 
				{ 
				?>
			<tr><td><input id="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>" type="radio" name="filter_<?=$currentFilterId;?>[]" value="<?=$value["id"];?>" <?php if($_SESSION[$oSecurityObject->getsAdminType() ."iLangID"] == $value["id"]){ echo " checked"; }?>/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["title"]; ?></label></td></tr>
		<?php 
				}
			?>
        </table>
        </div>
    </div>
</div>