<?php 
$aDates = array();
$loopYear = 2010;
$currentYear = date("Y");
$currentMonth = date("m");

while($loopYear <= $currentYear){
	if($loopYear == $currentYear){
		$loopMonth = 1;
		if($currentMonth % 2){
			array_push($aDates, array("value" => "blank", "comment" => "blank"));
		}
		while($loopMonth <= $currentMonth){
			$sDate = $loopYear . "-" . $loopMonth . "-01";
			$sMonth = date("M",strtotime($sDate));
			array_push($aDates, array("value" => $sDate, "comment" => $sMonth . " " . $loopYear));
			$loopMonth++;
		}
	}
	else{
		$loopMonth = 1;
		while($loopMonth <= 12){
			$sDate = $loopYear . "-" . $loopMonth . "-01";
			$sMonth = date("M",strtotime($sDate));
			array_push($aDates, array("value" => $sDate, "comment" => $sMonth . " " . $loopYear));
			$loopMonth++;
		}
	}
	array_push($aDates, array("value" => "break", "comment" => "break"));
	$loopYear++;
}

$currentFilterId = $currentFilter["id"];
if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId])){
	$currentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId];
}
else{
	$currentFilterData = false;
}
 ?>
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilter["id"]; ?>_filtercontent');  return false;" <?php if($currentFilterData){ ?>class="selected"<?php } ?>><?=$currentFilter["title"]; ?> 
    <img id="<?=$currentFilter["id"]; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilter["id"]; ?>_filtercontent" >
    	<div id="<?=$currentFilter["id"]; ?>_html">
    	<table><tr>
        <?php
			$subKey = 0;
            foreach (array_reverse($aDates) as $key => $value) 
            { 
				if($value["value"] == "break"){?>
                </tr></table><br /><table><tr>
                <?php
				}else{
				if($value["value"] == "blank"){?>
                	<td></td>
                <?php
				}else{ ?>
					<td><input id="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" type="checkbox" name="filter_<?=$currentFilterId;?>[]" value="<?=$value["value"];?>" <?php if($currentFilterData && in_array($value["value"], $currentFilterData)){ echo " checked"; }?>/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["comment"]; ?></label></td>
            <?php
					}
				if($subKey%2 == 1 && $subKey != 0){ ?>
					</tr><tr>
				<?php } 
				$subKey++; 
				}
            }
        ?>
        </tr></table>
        </div>
    </div>
</div>