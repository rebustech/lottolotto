<?php 
$sPath = $_SERVER["PHP_SELF"];
$aPath = explode("/", $sPath);
$sFileName = $aPath[sizeof($aPath) - 1];
						
$currentFilterId = $currentFilter["id"];
if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId])){
	$currentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId];
}
else{
	$currentFilterData = false;
}
$aSelectedCountries = array();
 ?> 
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilter["id"]; ?>_filtercontent');  return false;" <?php if($currentFilterData){ ?>class="selected"<?php } ?>><?=$currentFilter["title"]; ?> 
    <img id="<?=$currentFilter["id"]; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilter["id"]; ?>_filtercontent" >
   		<div id="<?=$currentFilter["id"]; ?>_html">
    	<table>
        <?php
            $aUserTypes = UsersAdmin::getAdminTypes();	
			$aSelectedUserTypes = array();
            foreach ($aUserTypes as $key => $value) 
            { ?>
                <tr><td><input id="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" type="checkbox" onclick="usertypeChanged(this.value);" name="filter_<?=$currentFilterId;?>[]" value="<?=$value["id"];?>" <?php if($currentFilterData && in_array($value["id"], $currentFilterData)){ array_push($aSelectedCountries, $value["id"]); echo " checked"; array_push($aSelectedUserTypes, $value["id"]); }?>/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["adminName"]; ?></label></td></tr>
            <?php 
            }
        ?>
        </table>
        </div>
    </div>
</div>
<script type="text/javascript">
	function usertypeChanged(selectedUser){
		if($("<?=AdminPage::USER?>_filtercontent")){
			$("#" + "<?=AdminPage::USER?>_html").load("actions/filter_ajax.php?a=getUsers&selectedFilter=<?=$currentFilter["id"]; ?>&selectedValue="  + selectedUser + "&pp=<?=$sFileName?>&tablename=<?=$_GET['tablename']?>");
		}
	}
</script>