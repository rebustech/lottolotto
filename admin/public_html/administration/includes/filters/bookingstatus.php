<?php 

$currentFilterId = $currentFilter["id"];
if(is_array($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId]) && !empty($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId])){
	$currentFilterData = $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][$currentFilterId];
}
else{
	$currentFilterData = false;
}
 ?>
<div class="filter">
	<a href="#" onclick="toggleFilterBox('<?=$currentFilter["id"]; ?>_filtercontent');  return false;" <?php if($currentFilterData){ ?>class="selected"<?php } ?>><?=$currentFilter["title"]; ?> 
    <img id="<?=$currentFilter["id"]; ?>_filtercontent_img" src="images/arrowdown.gif" /></a>
    <div class="content" style="display:none;" id="<?=$currentFilter["id"]; ?>_filtercontent" >
    	<div id="<?=$currentFilter["id"]; ?>_html">
    	<table>
        <?php
            $aStatuses = BookingsAdmin::getBookingStatusList();
            foreach ($aStatuses as $key => $value) 
            { ?>
                <tr><td><input id="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>" onmouseover="checkDragSelect(this.id)" type="checkbox" name="filter_<?=$currentFilterId;?>[]" value="<?=$value["id"];?>" <?php if($currentFilterData && in_array($value["id"], $currentFilterData)){ echo " checked"; }?>/><label for="filter_<?=$currentFilterId;?>_<?=$key?>_<?=$value["id"];?>"><?php echo $value["title"]; ?></label></td></tr>
            <?php 
            }
        ?>
        </table>
        </div>
    </div>
</div>