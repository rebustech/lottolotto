<?php
	$aMainNavigation = $oSecurityObject->getNavigationPages();
	$sPreviousPage = ($_GET['pp'])?$_GET['pp']:'';

        if($_GET['expand']) $_SESSION['expanded']=$_GET['expand'];
?>
<div id="main-navigation" class="sliding-menu">
    <img src="/administration/images/llmax.png" style="margin:0px"/>
	<ul>
            <li><a href="index.php" <?php if($oPage->sFilename == "index.php"){ ?>class="selected"<?php } ?>><i class="fa fa-dashboard fa-2x fa-fw"></i>Dashboard</a></li>
    	<?php
			foreach($aMainNavigation as $value){
				if ( $value["filename"] != "tableview.php" )
				{
					$bSelected = false;
					if( ( ($sPreviousPage == $value['filename']) || ($oPage->sFilename == $value['filename'] ) ) && ($oPage->sTablename == $value["tablename"]) && ($value["tablename"] != "") ) {
						$bSelected = true;
					}
					else{
						$bSelected = false;
					}
				?>
				<li>
                                    <a href="/administration/<?=$value["filename"]?>?expand=<?=$value['id']?><?php if($value["tablename"]){ ?>&tablename=<?=$value["tablename"]; }?>" <?php if($bSelected){ ?>class="selected"<?php } ?>><i class="fa <?=$value['icon']?> fa-2x fa-fw"></i><?=$value["title"];?></a>
                                    <?php
                                    if($_SESSION['expanded']==$value['id']){
                                        $aChildNavigation = $oSecurityObject->getNavigationPages($value['id']);
                                        if(is_array($aChildNavigation)){ ?>
                                    <ul>
                                        <?php foreach($aChildNavigation as $value){
                                                if ( $value["filename"] != "tableview.php" )
                                                {
                                                        $bSelected = false;
                                                        if( ( ($sPreviousPage == $value['filename']) || ($oPage->sFilename == $value['filename'] ) ) && ($oPage->sTablename == $value["tablename"]) && ($value["tablename"] != "") ) {
                                                                $bSelected = true;
                                                        }
                                                        else{
                                                                $bSelected = false;
                                                        }
                                                ?>
                                                <li <?php if ( $value['isfavourite'] ) { ?>class="favourite"<?php } ?>>
                                                    <a href="/administration/<?=$value["filename"];?><?php if($value["tablename"]){ ?>?tablename=<?=$value["tablename"]; }?>" <?php if($bSelected){ ?>class="selected"<?php } ?>><?=$value["title"];?></a>
                                                </li>
                                        <?php } } ?>
                                    </ul>
                                    <?php } } ?>
                                </li>
			<?php }
			} ?>
         <li style="padding-left: 6px; margin-top: 10px; margin-bottom: 5px;"><strong>Settings</strong></li>
        <li><a href="/administration/myaccount.php" <?php if($sCurrentFilename == "myaccount.php"){ ?>class="selected"<?php } ?>>My Account</a></li>
         <li><a href="/administration/logout.php">Logout</a></li>
   </ul>
            <div id="userdetails">Logged in as <strong><?=$oSecurityObject->getsFullName()?></strong><br />
        from IP <?=$oSecurityObject->getsIP()?></div>

</div>