<?php $aNotifications = $oSecurityObject->getNotifications(); 
	if ( count($aNotifications) > 0 )
	{
		?>
        	<div class='notifications'>
            	<h2>Notifications</h2>
        		<ul>
        <?php
			foreach ( $aNotifications as $Notification )
			{
				?>
					<li class='<?=($Notification['priority'])?'priority':''?>'><?=$Notification['notification']?></li>
				<?php
			}
		?>
        		</ul>
            </div>
        <?php
	}
?>