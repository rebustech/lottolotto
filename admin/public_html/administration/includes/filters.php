<?php if($oPage->sFilename != "generic-details.php" && $_GET["tablename"]){ ?>
<div id="filters">
<h3>My Filters</h3>
<form action="actions/filter_actions.php?pp=<?=$oPage->sFilename;?><?php if($_GET["tablename"]){?>&tablename=<?=$_GET["tablename"]?><?php } ?><?=$sCurrentQueryString?><?php if($_GET["id"]){ echo "&id={$_GET['id']}"; } ?>" method="post" onmouseout="stopDrag(event);" onmousedown="startDrag(event)" onmouseup="MouseUpStopDrag()">

  <div class="limitsdiv" style="margin: 10px; text-align: right;">
	<label for="txtLimit">Limit results:</label> <input type="textbox" id="txtLimit" name="txtLimit" style="width: 30px" value="<?php if($_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]){ echo $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"]; }else{ echo 150; } ?>" />
    <br />
            <div style="padding-left: 10px; text-align: right;">
            &nbsp;
    <?php if($bValidTable){ ?>
        <small><?php if($iLangId && $sLanguageTablename){ echo $sLanguageTablename; $sCountTable = $sLanguageTablename; }else{ echo $sTablename; $sCountTable = $sTablename; }?> has <?php echo GenericTableAdmin::getRecordCount($sCountTable); ?> records.</small>
    <?php } ?>
    	</div>
	</div>
  <p />
 <?php if($oPage->bLanguageFilter){
	include("includes/filters/language.php");
 } ?>
 <?php
 foreach($oPage->getFilters() as $currentFilter){
	include("includes/filters/" . strtolower($currentFilter["filename"]));
 } ?>
 <p />
<center><input type="submit" value="Apply filters" />
  <input type="button" value="Clear filters" onclick="document.location = 'actions/filter_actions.php?pp=<?=$oPage->sFilename;?><?php if($_GET["tablename"]){?>&tablename=<?=$_GET["tablename"]?><?php } ?>&a=clear';"/></center>
 </form>
 </div>
 <?php } ?>
 <script type="text/javascript">
 	var dragging = false;
	var checkMe = null;
	function startDrag(e){
		if (!e) var e = window.event;
		var tg = (window.event) ? e.srcElement : e.target;
		if (tg.nodeName != 'input'){
			checkMe = tg;
		}
		dragging = true;
	}
	function stopDrag(e){
		if (!e) var e = window.event;
		var tg = (window.event) ? e.srcElement : e.target;
		if (tg.nodeName != 'DIV') return;
		var reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement;
		while (reltg != tg && reltg.nodeName != 'BODY')
			reltg= reltg.parentNode
		if (reltg== tg) return;
		dragging = false;
	}
	function MouseUpStopDrag(){
		dragging = false;
	}
	function checkDragSelect(CheckID){

		if(dragging == true){
			if(checkMe){
				if(checkMe.checked == true){
					checkMe.checked = false;
				}
				else{
					checkMe.checked = true;
				}
				if(checkMe.onclick){
					checkMe.onclick();
				}
				checkMe = null;
			}
			if($('#' + CheckID).attr('onclick')){
				$('#' + CheckID).click();
			}
			else
			{
				if($("#" + CheckID).is(':checked') == false){
					$("#" + CheckID).attr('checked', 'true');
				}
				else{
					$("#" + CheckID).attr('checked', '');
				}
			}
		}
	}
 </script>
