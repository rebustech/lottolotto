<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


/**
 * Set some global variables that are used by the main views in the old system
 */
$iId = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$iLangId = filter_input(INPUT_GET, 'lid', FILTER_SANITIZE_NUMBER_INT);
if (!$iLangId) {
    $iLangId = 0;
} else {
    $currentLanguage = Language::getLanguageTitle($iLangId);
}

/**
 * Keep track of the previous page requested as this is used in various places for
 * back/cancel buttons and some security checks.
 */
$sTablename = filter_input(INPUT_GET, 'tablename', FILTER_SANITIZE_STRING);
$sLanguageTablename = filter_input(INPUT_GET, 'languagetable', FILTER_SANITIZE_STRING);
$sPreviousPage = filter_input(INPUT_GET, 'pp', FILTER_SANITIZE_NUMBER_INT) . "?tablename=" . $sTablename;
$iReturnID = filter_input(INPUT_GET, 'returnid', FILTER_SANITIZE_NUMBER_INT);
if ($iReturnID != null) {
    $sPreviousPage .= '&id=' . $iReturnID;
}
$sOldQueryString = unserialize(urldecode(filter_input(INPUT_GET, 'oldqs', FILTER_SANITIZE_STRING)));
$iPreviousPage = unserialize(urldecode(filter_input(INPUT_GET, 'pp', FILTER_SANITIZE_NUMBER_INT)));
if ($sOldQueryString) {
    if ($iPreviousPage && strstr($sOldQueryString, "&pp=")) {
        $iPPStartPos = strpos($sOldQueryString, "&pp=");
        $sPPqs = substr($sOldQueryString, strpos(sOldQueryString, "&pp="));
        $sPPqsTrimmed = substr($sPPqs, 1);
        $iPPEndPos = strpos($sPPqsTrimmed, "&");
        $sPPqs = substr($sPPqs, 0, $iPPEndPos + 1);
        $sOldQueryString = str_replace($sPPqs, "", $sOldQueryString);
    }
    if ($sOldQueryString) {
        $sPreviousPage .= "&qs=" . $sOldQueryString;
    }
}


$sAlert = '';



$sPath = $_SERVER["PHP_SELF"];
$aPath = explode("/", $sPath);
$sCurrentFilename = $aPath[sizeof($aPath) - 1];
$iLimit = $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"];

$oPage = new AdminPage($sCurrentFilename, $_GET["tablename"]);


$aRecentPages = unserialize(Cookies::getCookie($oSecurityObject->getsAdminType() . "history-" . $oSecurityObject->getUserID()));
if (!is_array($aRecentPages)) {
    $aRecentPages = array();
}
if ($oPage->getPageID()) {
    array_unshift($aRecentPages, $oPage->getPageID());
}
$bValid = Cookies::setCookie($oSecurityObject->getsAdminType() . "history-" . $oSecurityObject->getUserID(), serialize($aRecentPages), 24 * 30);
$sCurrentQueryString = "";
if ($_GET["pp"]) {
    $sCurrentQueryString .= "&pp=" . $_GET["pp"];
}
if ($_GET["id"]) {
    $sCurrentQueryString .= "&id=" . $_GET["id"];
}
if ($_GET["languagetable"]) {
    $sCurrentQueryString .= "&languagetable=" . $_GET["languagetable"];
}
$sCurrentQueryString = "&oldqs=" . urlencode(serialize($sCurrentQueryString));

if ($_GET["oldqs"]) {
    $sOldQueryString = unserialize(urldecode($_GET["oldqs"]));
}

$bFiltersOn = false;
$bFiltersCheck = true;
$bShowFiltersBar = false;
if (sizeof($oPage->getFilters()) > 0) {
    $bFiltersCheck = false;
    if ($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]) {
        $bFiltersOn = true;
    } elseif (Cookies::getCookie($oSecurityObject->getsAdminType() . "filterdata-" . $oSecurityObject->getUserID())) {
        $bFiltersOn = true;
        $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"] = unserialize(Cookies::getCookie($oSecurityObject->getsAdminType() . "filterdata-" . $oSecurityObject->getUserID()));
    }
    $aFilterData = ($_SESSION[$oSecurityObject->getsAdminType() . "filterdata"]) ? $_SESSION[$oSecurityObject->getsAdminType() . "filterdata"] : array();
    if ($bFiltersOn) {
        $newFilters = array();
        foreach ($oPage->getFilters() as $key => $value) {
            if (in_array($value["id"], array_keys($aFilterData))) {
                $newFilters[$value["id"]] = $aFilterData[$value["id"]];
            }
        }
        if ($oPage->bWebsiteFilter) {
            if (in_array("0", array_keys($aFilterData))) {
                $newFilters["0"] = $aFilterData["0"];
            }
        }
        if ($bUnAssignedCities == true) {
            $newFilters["9"] = array("0");
        }
        if (empty($newFilters)) {
            $bFiltersOn = false;
        } else {
            $bFiltersCheck = true;
        }
    }

    if (!$bFiltersOn && $oPage->sFilename != 'reports.php') {
        $bShowFiltersBar = true;
    } else {
        $bFiltersCheck = true;
    }
}

if (!$bStopOutput) {
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title><?= WEBSITETITLE ?> <?= $oSecurityObject->getsAdminTitle(); ?> <?php if ($oPage->sTitle) { ?> - <?= $oPage->sTitle ?><?php } ?></title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

            <link rel="stylesheet" type="text/css" href="/<?= $oSecurityObject->getsFolder() ?>styles/admin.css"/>
            <link rel="stylesheet" href="/<?= $oSecurityObject->getsFolder() ?>styles/jquery/ui.achtung-min.css" />
            <link rel="stylesheet" href="/<?= $oSecurityObject->getsFolder() ?>styles/jquery/jquery.simpledialog.css" />
            <link rel="stylesheet" href="/<?= $oSecurityObject->getsFolder() ?>styles/jquery/ui.theme.css" />
            <link rel="stylesheet" href="/<?= $oSecurityObject->getsFolder() ?>styles/jquery/ui.datepicker.css" />
            <link rel="stylesheet" type="text/css" href="/<?= $oSecurityObject->getsFolder() ?>styles/admin-print.css" media="print"/>
            <link rel="stylesheet" type="text/css" href="/<?= $oSecurityObject->getsFolder() ?>styles/admin-mobile.css" media="handheld, only screen and (max-device-width: 480px)"/>

            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery-1.7.2.min.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/mithril.min.js"></script>

            <?php
            /**
             * These two bits of javascript break file uploads!
             *
             */
            if (substr_count($_SERVER['REQUEST_URI'], 'tablename=game_engines') == 0 && substr_count($_SERVER['REQUEST_URI'], 'tablename=lotteries_cmn') == 0):
                ?>

                <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery.form.js"></script>
                <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/ui/ui.datepicker.js"></script>

                <?php
            endif;
            ?>

            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/interface.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/inestedsortable-1.0.1.pack.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/ui/jquery-ui-1.7.2.custom.min.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/ui/ui.core.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/ui/ui.draggable.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/ui/ui.sortable.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/ui/ui.achtung-min.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/achtung-ajax.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery.simpledialog.min.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery.validate.min.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery.hotkeys.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery.popupWindow.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/jquery/jquery.jqprint.js"></script>

            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/urlencode.js"></script>
            <script type="text/javascript" src="/<?= $oSecurityObject->getsFolder() ?>scripts/ll.js"></script>

            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"/>

            <script type="text/javascript">

                function printPage() {
                    $("#contentarea").jqprint({importCSS: true, operaSupport: false, printContainer: false, debug: true});
                }

                function toggleFilterBox(div_id) {
                    $("#" + div_id).toggle("fast", switchImage(div_id));
                }
                function switchImage(div_id) {
                    var imgObj = $("#" + div_id + "_img");
                    var currentSrc = imgObj.attr("src").indexOf("arrowup.gif");
                    if (currentSrc > 0) {
                        imgObj.attr("src", "images/arrowdown.gif");
                    } else {
                        imgObj.attr("src", "images/arrowup.gif");
                    }
                }

                var genericTypingTimer;

                function genericSearch(sQuery, sTablename, sField, sParams, divToUpdate) {
                    clearTimeout(genericTypingTimer);
                    if (sQuery != "") {
                        $("#clearbtn").css("visibility", "visible");
                    } else {
                        $("#clearbtn").css("visibility", "hidden");
                        $("#txtGenericSearch").val("");
                    }
                    $("#" + divToUpdate).html("<center><img src=\"images/loadingclock.gif\" alt=\"Loading..\"/></center>");
                    $("#" + divToUpdate).load('actions/generic_ajax.php?a=genericTableSearch&tablename=' + sTablename + '&searchfield=' + sField + '&query=' + urlencode(sQuery) + sParams);
                }

                function genericTypingSearch(sQuery, sTablename, sField, sParams, divToUpdate) {
                    clearTimeout(genericTypingTimer);
                    genericTypingTimer = setTimeout("genericSearch('" + sQuery + "', '" + sTablename + "',  '" + sField + "', '" + sParams + "', '" + divToUpdate + "')", 550);
                }

                $(document).ready(onDocumentReady);
                function onDocumentReady() {
                    loadControls();
                    $('.simpledialog').simpleDialog({duration: 100, opacity: 0.5, open: loadControls});
                }

                var achtungFormOptions = {
                    beforeSubmit: beforeFormSubmit, // pre-submit callback
                    success: showAchtungResponse  // post-submit callback
                };

                function beforeFormSubmit(a, form, options) {
                    var sFormSelector = "form[name='" + $(form).attr('name') + "']";

                    if ($(sFormSelector).valid()) {
                        startAchtungAjax();
                    } else {
                        return false;
                    }
                }

                function loadControls() {
                    //$(".ajaxform").validate();
                    $(".datetimepicker").datetimepicker({dateFormat: "yy-mm-dd", buttonImage: "images/calendar.gif"});
                    $(".datepicker").datepicker({dateFormat: "yy-mm-dd", buttonImage: "images/calendar.gif"});
                    //$(".ajaxform").ajaxForm(achtungFormOptions);
                    $('.ccpopup').popupWindow({
                        windowURL: "getCC.php",
                        windowName: "ccpopup",
                        height: 280,
                        width: 450
                    });
                    $('.ccpopup').attr("disabled", "");
                }

            </script>
            <?= LLResponse::$sHead ?>

            <script src="/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
            <script src="/amcharts/amcharts/serial.js" type="text/javascript"></script>
            <script type="text/javascript" src="/amcharts/amcharts/pie.js"></script>
            <script type="text/javascript" src="/amcharts/amcharts/themes/none.js"></script>
            <script>
                var llcharts = {};
            </script>

        </head>
        <?php
        if ($sAlert):
            ?>

            <body style="position:relative;top:50px;">
                <div style="padding:20px;background:#ccc;position: fixed;width: 100%;z-index: 100;">
                    <i class="fa fa-warning" style="color:#627;"></i>&nbsp;&nbsp;<?= $sAlert ?>
                </div>
                <?php
            else:
                ?>
                <body>
                <?php
                endif;
                ?>
                <?= LLResponse::$sPreBody ?>
                <div id="pseudoBody">
                    <div id="contentarea" class="<?= strtolower(str_replace(" ", "", $oSecurityObject->getsAdminTitle())) ?>">
                        <?php if ($bShowFiltersBar) { ?>
                            <div class="applyfiltersbar">&nbsp;</div>
                            <?php
                        }

                        $oBreadcrumbs = new BreadcrumbsControl();
                        echo $oBreadcrumbs->Output();
                    }
                    ?>
