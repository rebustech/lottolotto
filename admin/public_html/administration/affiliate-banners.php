<?php
include ("includes/top.php");
$sTablename = $_GET["tablename"];
$iLimit = $_SESSION[$oSecurityObject->getsAdminType() . "iLimit"];
$sFileName = basename($_SERVER["PHP_SELF"]);
$_SESSION[$oSecurityObject->getsAdminType() . "filterdata"][AdminPage::USERTYPE] = array(SecurityAdmin::AFFILIATE);

// Get affiliate ID
$sAffiliateID = filter_input(INPUT_GET, 'aid');
?>

<h1>Affiliates Management</h1>
<?php include("includes/errors.php"); ?>

<ul class="subnav">
    <li>
        <a href="affiliates.php?tablename=<?=$sTablename?>" class="largebutton">Listing</a>
    </li>
    <li>
        <a href="affiliate-details.php?tablename=<?=$sTablename?>&pp=affiliates.php" class="largebutton">Add Affiliate</a>
    </li>
    <li>
        <a href="affiliate-banners.php?tablename=<?=$sTablename?>" class="largebutton selected">Banners</a>
    </li>
</ul>

<?php

// Get the list of affiliates
$sSql = "SELECT DISTINCT affiliates_adverts_link.fk_affiliate_id, admin_users.* FROM affiliates_adverts_link
         INNER JOIN admin_users on admin_users.user_id = affiliates_adverts_link.fk_affiliate_id";
$aAffiliates = DAL::executeQuery($sSql);

?>
<script>
    var changeAffiliate = function (affiliate) {
        window.location.href = '/administration/affiliate-banners.php?aid=' + affiliate;
    };
</script>

<select onchange="javascript:changeAffiliate(this.value)">
    <option value="">Affiliates</option>
<?php
foreach ($aAffiliates as $affiliate) {
    ?>
    <option value="<?php echo $affiliate['user_id'] ?>" <?php if ($sAffiliateID === $affiliate['user_id']) { echo "selected"; } ?>><?php echo $affiliate['username']; ?></option>
    <?php
}
?>
</select>
<?php

$sSql = "SELECT affiliates_adverts_link.*, affiliate_adverts.name, admin_users.username FROM affiliates_adverts_link
         INNER JOIN affiliate_adverts ON affiliate_adverts.id = affiliates_adverts_link.fk_advert_id
         INNER JOIN admin_users ON admin_users.user_id = affiliates_adverts_link.fk_affiliate_id";


// List adverts for specific affiliate if sAffiliateID is set
if ($sAffiliateID) {
    $sSql .= " WHERE affiliates_adverts_link.fk_affiliate_id = {$sAffiliateID}";
}


$tabs=new TabbedInterface();

$oAffiliateAdvertsTab=new TabbedInterface(lang::get('affiliate_adverts'));
$aAffiliateAdverts=DAL::executeQuery($sSql);
//Get assigned countries
$oAffiliateAdvertsControl=new TableControl($aAffiliateAdverts);
$oAffiliateAdvertsControl->getFieldsFromTable('affiliate_adverts');
$oAffiliateAdvertsControl->getFieldsFromTable('admin_users');
$oAffiliateAdvertsControl->keepFields('affiliate_adverts', 'name');
$oAffiliateAdvertsControl->keepFields('admin_users', 'username');
$oAffiliateAdvertsControl->sName='affiliate_adverts';
$oAffiliateAdvertsControl->sCaption='Affiliate Adverts';
$oAffiliateAdvertsTab->AddControl($oAffiliateAdvertsControl);
$tabs->aItems['affiliates']=$oAffiliateAdvertsTab;

echo $tabs->Output();
?>

<?php include("includes/bottom.php"); ?>
