<?php
/**
 * Syndicate listing - also shows details of requested syndicate
 */
include ("includes/top.php");

if ( $_GET['i'] )
{
    $iSyndicateId = (int) $_GET['i'];
    $aSyndicateInfo = GenericSyndicate::getSyndicateInfo($iSyndicateId);

?>
<ul class="subnav">
    <li>
        <a href="syndicates.php" class="largebutton">Back to Syndicate List</a>
    </li>
    <li>
        <a href="syndicate-create.php" class="largebutton">Create New Syndicate</a>
    </li>
</ul>
<h2>Syndicate <?=$iSyndicateId;?></h2>
<?php
    $iNumLinesInSyndicate = count($aSyndicateInfo['lines']);
    $iNumMembersInSyndicate = count($aSyndicateInfo['members']);
    
 ?>
    <p>
        Current status: <?=$aSyndicateInfo['status_text'];?>
        <br/>
        Price per share: &euro;<?=number_format($aSyndicateInfo['price_per_share'], 2, ".", 0);?>
        <br/>       
        Current number of shares: <?=$aSyndicateInfo['current_shares'];?>
        <br/>
        Syndicate closes at <?=$aSyndicateInfo['max_shares'];?> shares
        <br/>
        Shares remaining: <?=$aSyndicateInfo['shares_left'];?>
    </p>
    <p>
    <h4>Lotteries in this syndicate</h4>
    <table width='100%' border='0' cellspacing='0' cellpadding='2' class='report'>
        <tr>
            <th>Lottery Name</th>
            <th>Number of<br/>random numbers</th>
            <th>Number of<br/>lines requested</th>
            <th>Number of<br/>lines generated</th>
            <th>Original random<br/>number selection</th>
            <th>Original bonus<br/>number selection</th>
            <th>Numbers in<br/>each line</th>
        </tr>
  <?php
     foreach ($aSyndicateInfo['lotteries'] as $count=>$aLottery)
     {
         $sClass = ($count % 2?" class='alternate'":"");

         if ($aLottery['bonus_numbers'] != "")
         {
            $iCountBonus = count(explode("|", $aLottery['bonus_numbers']));
            $sPlusBonus = "+{$iCountBonus}";
         }
         else
         {
             $sPlusBonus = "";
         }
  ?>
        <tr<?=$sClass;?>>
            <td><?=$aLottery['comment'];?></td>
            <td><?=$aLottery['numbers_playing_from'];?></td>
            <td><?=$aLottery['num_lines'];?></td>
            <td><?=$aLottery['num_generated'];?></td>
            <td><tt><?=$aLottery['original_numbers'];?></tt></td>
            <td><tt><?=$aLottery['bonus_numbers'];?></tt></td>
            <td><?=$aLottery['numbers_to_select'];?><?=$sPlusBonus;?></td>
        </tr>
  <?php
     }
  ?>
    </table>
    </p>
    <hr/>
    <div style="float:left; width: 45%;">
 <?php
    if ($iNumLinesInSyndicate > 0)
    {
?>

    <h3><?=$iNumLinesInSyndicate;?>  lines in this syndicate</h3>
    <table width='90%' border='0' cellspacing='0' cellpadding='2' class='report'>
        <tr>
            <th width='15%'>Line</th>
            <th>Lottery</th>
            <th>Numbers on this line</th>
        </tr>
 <?php
        $key = 0;
        foreach ( $aSyndicateInfo['lines'] as $aLine )
        {
            $key++;
            $sClass = ($key % 2?" class='alternate'":"");
?>
        <tr<?=$sClass;?>>
            <td><?=$key;?></td>
            <td><?=$aLine['comment'];?></td>
            <td><tt><?=$aLine['line_numbers'];?></tt></td>
        </tr>
<?php
        }
?>       
    </table>
<?php
    }
    else
    {
            echo "<div class='reportempty'>No lines in this syndicate!</div>";
    }
?>
    </div>
    <div style="float:right; width: 45%;">
 <?php
    if ($iNumMembersInSyndicate > 0)
    {
?>

    <h3><?=$iNumMembersInSyndicate;?>  members in this syndicate</h3>
    <table width='100%' border='0' cellspacing='0' cellpadding='3' class='report'>
        <tr>
            <th width='30%'>Member Name</th>
            <th>Member Email</th>
            <th>Number of shares</th>
        </tr>
 <?php
        $key = 0;
        foreach ( $aSyndicateInfo['members'] as $aMember )
        {
            $key++;
            $sClass = ($key % 2?" class='alternate'":"");
?>
        <tr<?=$sClass;?>>
            <td><?=$aMember['firstname'] . " " . $aMember['lastname'];?></td>
            <td><?=$aMember['email'];?></td>
            <td><tt><?=$aMember['num_shares'];?></tt></td>
        </tr>
<?php
        }
?>       
    </table>
<?php
    }
    else
    {
            echo "<div class='reportempty'>No members currently in this syndicate!</div>";
    }
?>
    </div>
<?php
}
else
{            
?>
<ul class="subnav">
    <li>
        <a href="syndicate-create.php" class="largebutton">Create New Syndicate</a>
    </li>
</ul>
<h2>Syndicates</h2>
<div id="reports">
<?php
    $aResults = GenericSyndicate::getSyndicates();
    if ( $aResults )
    {
        
?>
    <table width='100%' border='0' cellspacing='0' cellpadding='3' class='report'>
        <tr>
            <th style='text-align:center;'>Id</th>
            <th style='text-align:center;'>Start Date</th>
            <th style='text-align:center;'>Lottery Games</th>
            <th style='text-align:center;'>Price Per<br/>Share</th>
            <th style='text-align:center;'>Current<br/>Shares</th>
            <th style='text-align:center;'>Max<br/>Shares</th>
            <th style='text-align:center;'>Shares<br/>Remaining</th>
            <th style='text-align:center;'>Status</th>
            <th style='text-align:center;'>Booking<br/>ID</th>
            <th></th>
        </tr>
<?php
        foreach ( $aResults as $key=>$aResult )
        {
            $aLotteryNames = array();
            $sClass = ($key % 2?"":" class='alternate'");
            $iBookingID = is_null($aResult['booking_id'])?"-":$aResult['booking_id'];
            foreach ($aResult['lotteries'] as $i=>$aName)
            {
                $aLotteryNames[] = $aName['comment'];
            }
?>          
        <tr<?=$sClass;?>>
            <td style='text-align:center;'><?=$aResult['id'];?></td>
            <td style='text-align:center;'><?=(str_replace(" ", "<br/>", $aResult['start_date']));?></td>
            <td style='text-align:center;'><?=implode("<br/>", $aLotteryNames);?></td>
            <td style='text-align:center;'>&euro;<?=number_format($aResult['price_per_share'], 2, ".", 0);?></td>
            <td style='text-align:center;'><?=$aResult['current_shares'];?></td>
            <td style='text-align:center;'><?=$aResult['max_shares'];?></td>
            <td style='text-align:center;'><?=($aResult['shares_left']);?></td>
            <td style='text-align:center;'><?=$aResult['status'];?></td>
            <td style='text-align:center;'><?= $iBookingID;?></td>
            <td><a href='?i=<?=$aResult['id'];?>'>Details</a></td>
        </tr>
<?php
        }
?>
    </table>
<?php
    }
    else
    {
?>
    <div class='reportempty'>No Syndicates to display</div>
<?php
    }
?>
</div>
<?php
unset($aResults);
}

include ("includes/bottom.php");
?>