<?php
/**
 * This script displays the current 'checking' status for a particular lottery draw,
 * or, if no lottery draw is chosen, highlights lotteries which have outstanding
 * checks
 */
$bypass_login = true;

include ("includes/top.php");
include ("includes/errors.php");
?>
<h2>Check supplied lottery results for checking</h2>
<?php

// Check if lottery id has been supplied on query string
if ( $_GET['l'] )
{
    // Assing to variable (for further sanitising)
    $iLotteryID = (int)($_GET['l']);

    // Get general lottery details
    $aLotteryDetails = LotteryAdmin::getLotteryDetails($_GET['l']);

    // Has a lottery date been supplied?
    if (!$_REQUEST['d']) // Can pass in either on query string or via a POST command
    {
        // No draw date supplied, so get a list of possible draw dates for this array
        $aDrawDates = LotteryResultsAdmin::getDrawDates($_GET['l']);

        // Reverse this array so it's sorted in descending date order
        $aDrawDates = array_reverse($aDrawDates, true);
?>
    <div id="reports">
        <strong>Lottery Type:</strong>  <?=$aLotteryDetails['comment']?><br/>
        <form name="addResultCheckAllSelectDate" action="add-results-checker-all.php?l=<?=$_GET['l']?>" method="post">
            <h4>Please select the draw date</h4>
            <select name='d'>
<?php
        // Produce a select list of the draw dates
        foreach ($aDrawDates as $DrawDate )
        {
            echo "<option value='" . date("Ymd", strtotime($DrawDate['drawdate'])) . "'>" . date("D d M Y H:i:s", strtotime($DrawDate['drawdate'])) . "</option>";
        }
?>
            </select>
            <input type="submit" class="largebutton" value="Select Date" />
        </form>
        <br/>
    </div>

<?php
    }
    else
    {
        // Draw date has been supplied, so we can start the process.

    // Set flags which record the final checking status of numbers, winnings, next jackpot and rollover
    // and set all as true - they will only be changed if there's a mimatch
        $bCheckNumbersOK = true;
        $bCheckWinningsOK = true;
        $bCheckNJAmountOK = true;
        $bCheckNJRolloverOK = true;
?>

<div id="results">
<?php

    // Get query string parameters into variables
    $iLotteryID = $_GET['l'];
    $iDraw = $_REQUEST['d'];

    // Get the lottery rows that require checking
    $aRowsToBeChecked = LotteryResultsAdmin::getLotteryRowsToBeCheckedFromCheckingTable($iDraw, $iLotteryID, array(1,2,3));

    // Do we have rows?
    if (!$aRowsToBeChecked) {
        echo "<strong>No data to be checked for this lottery found - either this lottery has not yet been drawn or data has not yet been entered</strong>";
    }
    else
    {
        // Array for storing checking results
        $aDataCheck = array();

        // Loop through the returned rows
        foreach ($aRowsToBeChecked as $iCount=>$aData) {

            // ID of the person who checked
            $iChecker = $aData['checker_id'];

            // Unserialise the stored winnings information
            $aWinnings = unserialize($aData['winnings']);

            // Current prize level
            $sPrizeLevel = "";

            // Copy the stored numbers into the storing array
            $aDataCheck['numbers'][$iChecker] = $aData['numbers'];

            // Do the same for the next jackpot information
            $aDataCheck['nj_amount'][$iChecker] = $aData['next_jackpot_amount'];
            $aDataCheck['nj_rollover'][$iChecker] = $aData['next_jackpot_rollover'];

            // Go through the unserialized winnings
            foreach ($aWinnings as $iCount2=>$aData2)
            {
                // Create a key for each prize level based on match and bonus
                $sPrizeLevel = $aData2['match'] . "+" . $aData2['bonus'];

                // Copy the stored winners and prizes into the storage array
                $aDataCheck['winnings'][$sPrizeLevel][$iChecker]['winners'] = $aData2['winners'];
                $aDataCheck['winnings'][$sPrizeLevel][$iChecker]['prize'] = $aData2['prize'];
            }
        }

        // Create a date string from the draw date of the lottery
        $sDateString = date("D d M Y H:i:s", strtotime($aRowsToBeChecked[0]['fk_lottery_date']));
?>
    <strong>Lottery Type:</strong><?=$aLotteryDetails['comment']?><br/>
    <strong>Draw Date:</strong><?=$sDateString?><br/>
    <strong>Draw Reference: </strong><?=$iDraw?>

    <p style='color:red;'>
        <b>N.B.</b> Remember that TWO checks are now required for each set of numbers/prize breakdown information!
    </p>

    <h4>Draw Numbers:</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th width='120px'>Checker ID</th><th>Numbers</th><th>Check Status</th>
        </tr>

 <?php
            // Display the outcome of checking the numbers
            $key = 0;
            // Loop through the drawn numbers
            foreach ( $aDataCheck['numbers'] as $iChecker => $sNumbers)
            {
                // Keep a count of the number of checks already made
                $key++;
                $sStyle="";
                // Only check on the first time round the loop
                if ($key % 3 == 1)
                {
                    // Pass ALL numbers to be checked into the checking function
                    // Return the outcome
                    $bNumbersOK = LotteryResultsAdmin::checkNumbersEquality($aDataCheck['numbers']);
                    if ($bNumbersOK)
                    {
                        $sStatus = "OK";
                        $sStyle="color:green;font-weight:bold";
                    }
                    else
                    {
                        // If we reach here, there has been a mismatch
                        // We need to flag this up accordingly
                        $bCheckNumbersOK = false;
                        $sStatus = "Mismatch";
                        $sStyle="color:red;font-weight:bold";
                    }
                }
                else
                {
                    $sStatus="&nbsp;";
                }
?>
        <tr class='alternate'>
            <td><?=$iChecker?></td>
            <td><tt><?=$sNumbers?></tt></td>
            <td style="<?=$sStyle?>"><?=$sStatus?></td>
        </tr>
<?php
            }
?>
    </table>
    <br/>
    <h4>Prize Breakdowns:</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th width='60px'>Match</th>
            <th>Bonus</th>
            <th>Checker ID</th>
            <th>Prize</th>
            <th>Winners</th>
            <th>Check Status</th>
        </tr>
<?php
            // Now display the outcome of checking the prizes
            $key = 0;
            $key2 = 0;

            // Loop through the winnings
            foreach ( $aDataCheck['winnings'] as $sPrizeLevel => $aWinData )
            {
               $key++;
               $sStyle="color:green;font-weight:bold";

               // Check the equality of the prizes for this prize level
               $aWinDataChecks[$sPrizeLevel] = LotteryResultsAdmin::checkPrizesEquality($aWinData);

               // Set up variables for holding the previous match and bonus values
               $iPrevMatch = "";
               $iPrevBonus = "";

               // Loop through each prize level's winnings
               foreach ($aWinData as $iChecker => $aWinInfo)
               {
                    $key2++;

                    // Seperate the prize level into match and bonus
                    list($iMatch, $iBonus) = explode("+", $sPrizeLevel);

                    // If the match and bonus values do not equal the previous values
                    // then check match statuses here
                    if (($iMatch != $iPrevMatch) && ($iBonus != $iPrevBonus))
                    {
                        // If the equality check did not pass, then flag up accordingly
                        if (!$aWinDataChecks[$sPrizeLevel])
                        {
                           $bCheckWinningsOK = false;
                           $sStyle="color:red;font-weight:bold";
                        }

                        // Set mismatch text accordingly
                        $sCheckStatus = ($aWinDataChecks[$sPrizeLevel]?"OK":"Mismatch");
                        $iMatchText = $iMatch;
                        $iBonusText = $iBonus;
                    }
                    else
                    {
                        $sCheckStatus = "&nbsp";
                        $iMatchText = "&nbsp";
                        $iBonusText = "&nbsp";
                    }

                    $sClass = '';
                    // Output each prize level's winnings and winners
                    // and whether there was a mismatch or not
                    if ( $key % 2 )
                    {
                        $sClass = "alternate";
                    }
?>
        <tr class='<?=$sClass?>'>
            <td><?=$iMatchText?></td>
            <td><?=$iBonusText?></td>
            <td><?=$iChecker?></td>

            <td><tt><?=$aWinInfo['prize']?></tt></td>

            <td><tt><?=$aWinInfo['winners']?></tt></td>
            <td style="<?=$sStyle?>"><?=$sCheckStatus?></td>
        </tr>
 <?php
                    // Assign the current match and bonus values to the previous ones
                    $iPrevMatch = $iMatch;
                    $iPrevBonus = $iBonus;
                }
            }
 ?>
    </table>
<?php
        }

        // We now display the overall status of the check based on the overall flags set
?>

    <h4>Next jackpot information:</h4>
    <div style="float:left;">
    <table border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th width='90px'>Checker ID</th><th>Next Jackpot</th><th>Check Status</th>
        </tr>

 <?php
            // Display the outcome of checking the numbers
            $key = 0;
            // Loop through the drawn numbers
            foreach ( $aDataCheck['nj_amount'] as $iChecker => $iNextJackpot)
            {
                // Keep a count of the number of checks already made
                $key++;
                $sStyle = '';
                // Only check on the first time round the loop
                if ($key % 3 == 1)
                {
                    // Pass ALL numbers to be checked into the checking function
                    // Return the outcome
                    $bNJAmountOK = LotteryResultsAdmin::checkNumbersEquality($aDataCheck['nj_amount']);
                    if ($bNJAmountOK)
                    {
                        $sStatus = "OK";
                        $sStyle="color:green;font-weight:bold";
                    }
                    else
                    {
                        // If we reach here, there has been a mismatch
                        // We need to flag this up accordingly
                        $bCheckNJAmountOK = false;
                        $sStatus = "Mismatch";
                        $sStyle="color:red;font-weight:bold";
                    }
                }
                else
                {
                    $sStatus="&nbsp;";
                }
?>
        <tr class='alternate'>
            <td><?=$iChecker?></td>
            <td><tt><?=$iNextJackpot?></tt></td>
            <td style="<?=$sStyle?>"><?=$sStatus?></td>
        </tr>
<?php
            }
?>
    </table>
    </div>
    <div style="float:right;">
    <table border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th width='90px'>Checker ID</th><th>Rollover</th><th>Check Status</th>
        </tr>

 <?php
            // Display the outcome of checking the numbers
            $key = 0;
            // Loop through the drawn numbers
            foreach ( $aDataCheck['nj_rollover'] as $iChecker => $bRollover)
            {
                // Keep a count of the number of checks already made
                $key++;

                // Only check on the first time round the loop
                if ($key % 3 == 1)
                {
                    // Pass ALL numbers to be checked into the checking function
                    // Return the outcome
                    $bNJRolloverOK = LotteryResultsAdmin::checkNumbersEquality($aDataCheck['nj_rollover']);
                    if ($bNJRolloverOK)
                    {
                        $sStatus = "OK";
                        $sStyle="color:green;font-weight:bold";
                    }
                    else
                    {
                        // If we reach here, there has been a mismatch
                        // We need to flag this up accordingly
                        $bCheckNJRolloverOK = false;
                        $sStatus = "Mismatch";
                        $sStyle="color:red;font-weight:bold";
                    }
                }
                else
                {
                    $sStatus="&nbsp;";
                }
?>
        <tr class='alternate'>
            <td><?=$iChecker?></td>
            <td><?=$bRollover==1?"Yes":"No"?></td>
            <td style="<?=$sStyle?>"><?=$sStatus?></td>
        </tr>
<?php
            }
?>
    </table>
    </div>
    <div style="clear: both; padding-top: 10px;">
        Number check status: <b><?=($bCheckNumbersOK?"OK":"Mismatch")?></b><br/>
        Winners check status: <b><?=($bCheckWinningsOK?"OK":"Mismatch")?></b><br/>
        Next jackpot amount status: <b><?=($bCheckNJAmountOK?"OK":"Mismatch")?></b><br/>
        Next jackpot rollover status: <b><?=($bCheckNJRolloverOK?"OK":"Mismatch")?></b><br/><br/>
        Overall check status: <b><?=(($bCheckNumbersOK&&$bCheckWinningsOK&&$bCheckNJAmountOK&&$bCheckNJRolloverOK)?"OK":"Mismatch")?></b>
    </div>
</div>
<br/>
<?php
    }
}
else
{
    // Display all available lotteries
    $aLotteries = LotteryAdmin::getLotteries(true);

    // Get list of lotteries which still have some outstanding checks to be made
    //$aOutstandingLotteries = LotteryResultsAdmin::getLotteriesWithOutstandingChecks();

    // Get list of lotteries and their current statuses
    $aOutstandingLotteries = LotteryResultsAdmin:: getLotteriesAndCheckStatusesForDisplay();

?>
<h3>Please select the required lottery</h3>
<div id="reports">

        <ul>
<?php
    foreach ($aLotteries as $aLottery )
    {
    ?>
            <li>
                <a href="<?=$oPage->sFilename?>?l=<?=$aLottery["lottery_id"]?>"><img src='images/lotto-<?=$aLottery["lottery_id"]?>.png' width="70px" />
                <br/>
                <?=$aLottery["comment"];?></a>
            </li>
<?php
    }
    ?>
        </ul>

</div>
<div style="clear: both; padding-top: 10px;">
    <h3>Lotteries with outstanding results to be checked</h3>
    <table width="90%" border="0" cellspacing="0" cellpadding="3" class='report'>
        <tr>
            <th>Lottery Type</th>
            <th>Person</th>
            <th>Draw Date/Time</th>
            <th>Status</th>
            <th></th>
        </tr>
<?php
    $key = 0;

    // Display lotteries with outstanding checks
    foreach ($aOutstandingLotteries as $aOutstandingLotteryInfo)
    {
        $sClass = ($key++ % 2 ?"":"alternate");
        $sBackgroundColour=(strtotime($aOutstandingLotteryInfo['fk_lottery_date']) > time())?'green':'red';
        $aLotteryDetails = LotteryAdmin::getLotteryDetails($aOutstandingLotteryInfo['fk_lottery_id']);
        $sLotteryDate = date("D d M Y H:i:s", strtotime($aOutstandingLotteryInfo['fk_lottery_date']));
        $sLotteryDateCode = date("Ymd", strtotime($aOutstandingLotteryInfo['fk_lottery_date']));

        // Status
        switch ($aOutstandingLotteryInfo['status'])
        {
            case 0:
                $sStatus = "Checks not started";
                $sBackgroundColour = 'black';
            break;

            case 1:
                $sStatus = "Waiting on completion of all checks";
                $sBackgroundColour = 'orange';
            break;

            case 2:
                $sStatus = "Checks done - <b>NO ERRORS</b>";
                $sBackgroundColour = 'green';
            break;

            case 3:
                $sStatus = "Checks done - <b>ERRORS EXIST</b>";
                $sBackgroundColour = 'red';
            break;

            default:
                $sStatus = "Unknown";
        }
?>
        <tr class='<?=$sClass?>' style="color:<?=$sBackgroundColour?>">
            <td><?=$aLotteryDetails['comment']?></td>
            <td><?=$aOutstandingLotteryInfo['first_name']?></td>
            <td><?=$sLotteryDate?></td>
            <td><?=$sStatus;?></td>
            <td><a href="/administration/add-results-checker.php?sh=<?=$aOutstandingLotteryInfo['security_hash']?>&l=<?=$aOutstandingLotteryInfo['fk_lottery_id']?>&c=<?=$aOutstandingLotteryInfo['checker_id']?>&d=<?=$sLotteryDateCode?>">Manual Entry</a></td>
        </tr>
<?php
    }
?>
   </table>
</div>
<?php
}

include ("includes/bottom.php");
