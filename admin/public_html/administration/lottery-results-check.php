<?php

ob_start();

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

//echo ("!!!!!!!");
/*
 * This script is called from the form used by checkers to enter the lottery data.
 *
 * Functionality:
 * 1) Individual entry of results into chekcing table
 * 2) Comparison of entered results when checking threshold reached
 * 3) If comparison OK, execution of winscan
 *
 * In all cases, this script edirects to confirmation page after results entered
 */
$bypass_login = true;
include ("includes/top.php");

//echo ("!!!!!!!");

//error_reporting(E_ALL);
// Variables from querystring
$sSecurityHash = $_GET['sh'];
$iLotteryID = $_GET['l'];
$iDraw = $_GET['d'];
$iCheckerID = $_GET['c'];


// Helper function to send the customer an email when they've had a win
// This is used in two places
function sendLotteryWonCustomerEmail($aTicket, $iBookingItemID, $aLotteryInformation, $aNextDrawDetails, $aWinscanWinner, $iTransactionID)
{
    // Return the balance value to 2 decimal places, rounded down
    // This will then be further formatted to 2display as 2 decimal places
    $fBalance = floor($aWinscanWinner['balance'] * 100) / 100;

    // Now using Silverpop to send out email
    // $bValid = EmailAdmin::sendLotteryWonEmail($aTicket['firstname'], $aTicket['lastname'], $aTicket['email'], $aTicket['numbers'], $aTicket['currency'] . " " . number_format($aTicket['winning_amount'],2), $aTicket['draw_date'], $aTicket['lottery_title'], $aTicket['win_title'], $aTicket['ticket_price'], $aTicket['bookingreference']);
    $aPersonalisationData = array('username'         => $aWinscanWinner['email'],
                                  'balance'          => number_format($fBalance, 2, '.', ','),
                                  'current balance'  => number_format($fBalance, 2, '.', ','),
                                  'firstname'        => $aWinscanWinner['firstname'],
                                  'member_id'        => $aWinscanWinner['member_id'],
                                  'email_ref'        => 'order_win');

//    $aTicket['firstname'], $aTicket['lastname'], $aTicket['numbers'],
//                                    $aTicket['currency'] . " " . number_format($aTicket['winning_amount'],2),
//                                    $aTicket['draw_date'], $aTicket['lottery_title'], $aTicket['win_title'],
//                                    $aTicket['ticket_price'], $aTicket['bookingreference']


//    EmailAdmin::sendOrderWinning($aTicket['email'], $aPersonalisationData);

    //if ( $bValid )
    //{
        // Email sent OK, so set flag accordingly
        if ( LotteryResultsAdmin::setWinnerEmailSent($iBookingItemID) )
        {
            // No problem setting flag
            // add audit log entry
            $sAuditLogMsg = "Email for Winning ticket for {$aLotteryInformation['comment']} draw " .
                            "on {$aNextDrawDetails['drawdate']} sent to customer " .
                            "and update successful." .
                            "Booking ID {$aWinscanWinner['bookingitem_id']}, " .
                            "Transaction ID {$iTransactionID}";

            AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_EMAIL_SENT_OK_UPDATE_OK', 'lottery_results_checking', 0);
        }
        else
        {
            // Problem setting flag
            $sAuditLogMsg = "Email for Winning ticket for {$aLotteryInformation['comment']} draw " .
                            "on {$aNextDrawDetails['drawdate']} sent to customer " .
                            "but update failed.  " .
                            "Booking ID {$aWinscanWinner['bookingitem_id']}, " .
                            "Transaction ID {$iTransactionID}";

            AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_EMAIL_SENT_OK_UPDATE_FAILED', 'lottery_results_checking', 0);
        }
//    }
//    else
//    {
//        // Problem sending email
//        // add audit log entry
//        $sAuditLogMsg = "Failed to send Email for Winning ticket for {$aLotteryInformation['comment']} draw " .
//                        "on {$aNextDrawDetails['drawdate']}to customer. " .
//                        "Booking ID {$aWinscanWinner['bookingitem_id']}, " .
//                        "Transaction ID {$iTransactionID}";
//
//        AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_EMAIL_SENT_FAILURE', 'lottery_results_checking', 0);
//    }
}

// Check that the supplied security hash matches the supplied other details.
if (!LotteryResultsAdmin::checkSecurityHash($sSecurityHash, $iDraw, $iLotteryID, $iCheckerID))
{
    die("Sorry, you are not authorised to view this page");
}
else
{
    //echo ("START<BR>");
    // Get details of who was supposed to be doing this check
    $aCurrentChecker = LotteryResultsAdmin::getCheckerByHash($sSecurityHash);


    // Get further lottery information
    $aLotteryInformation = LotteryAdmin::getLotteryDetails($iLotteryID);
    $aNextDrawDetails = LotteryResultsAdmin::getLotteryDrawDateFromDrawRef($iDraw, $iLotteryID);

    // Write audit log row to record results stored
    $sAuditLogMsg = "{$aCurrentChecker['first_name']} entered results for {$aLotteryInformation['comment']} draw " .
                    "on {$aNextDrawDetails["drawdate"]}";

    AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_RESULTS_STORED', 'lottery_results_checking', $iCheckerID);

    // Send email to Administrators at this point informing them of results entered
    $aEmailData = array('lottery_name'  =>$aLotteryInformation['comment'],
                        'draw_date'     =>$aNextDrawDetails['drawdate']);

    EmailAdmin::sendEmailToAdministrator($aEmailData, 'checker_results_entered');

    //echo("<pre>");

    // Set flags which record the final checking status of numbers, winnings, next jackpot and rollover
    // and set all as true - they will only be changed if there's a mimatch
    $bCheckNumbersOK = true;
    $bCheckWinningsOK = true;
    $bCheckNJAmountOK = true;
    $bCheckNJRolloverOK = true;

    // Keep a record of whether we have winners infomration
    // as we'll need this to check on whether we run the winscan
    $bWinnersExist = true;

    // Set up redirect URL here
    $redirect = "lottery-results-confirm.php?sh=" . $sSecurityHash . "&l=" . $iLotteryID ."&c=" . $iCheckerID ."&d=" . $iDraw;
    //echo ("Before all checks<br>");
    // Count how many checkers have checked this lottery
    $iNumChecks = LotteryResultsAdmin::checkAllChecksMadeForLottery($iLotteryID, $iDraw);
    //echo ("After all checks<br>");

    // Have the required number of checks been made to do the equality check?
    // (using class constant)
    if ($iNumChecks == LotteryResultsAdmin::CHECKER_THRESHOLD)
    {
//       echo ("All checks made<br>");
        // Write audit log row to record start of checking
        $sAuditLogMsg = "Started results check for {$aLotteryInformation['comment']} draw " .
                        "on {$aNextDrawDetails['drawdate']}";

        AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_CHECKING_START', 'lottery_results_checking', 0);

        // Yes, yes they have, so get them out of the database
        $aRowsToBeChecked = LotteryResultsAdmin::getLotteryRowsToBeCheckedFromCheckingTable($iDraw, $iLotteryID, array(0,1,3));

        //print_r($aRowsToBeChecked);

        // Array to store the results of each data check
        $aDataCheck = array();

        // Loop through the rows to be checked
        foreach ($aRowsToBeChecked as $iCount=>$aData)
        {

            // Get the checker ID
            $iChecker = $aData['checker_id'];

            // Unserialize the winning data for this entry
            $aWinnings = unserialize($aData['winnings']);

            // Variabnle used to store the current prize level
            $sPrizeLevel = "";

            // Copy the retrieved numbers onto the data check array
            $aDataCheck['numbers'][$iChecker] = $aData['numbers'];

            // Do the same for the next jackpot information
            $aDataCheck['nj_amount'][$iChecker] = $aData['next_jackpot_amount'];
            $aDataCheck['nj_rollover'][$iChecker] = $aData['next_jackpot_rollover'];

            // Loop through each set of winnings for this checker
            foreach ($aWinnings as $iCount2=>$aData2)
            {
                // Create a prize level key based on match and bonus
                $sPrizeLevel = $aData2['match'] . "+" . $aData2['bonus'];

                //  Copy retrieved winners and prize for this level onto data check array
                $aDataCheck['winnings'][$sPrizeLevel][$iChecker]['winners'] = $aData2['winners'];
                $aDataCheck['winnings'][$sPrizeLevel][$iChecker]['prize'] = $aData2['prize'];
            }

        }

        // Pass the retrieved numbers (now in data check array) to equality checking function
        // and return the status of the check
        $bNumbersOK = LotteryResultsAdmin::checkNumbersEquality($aDataCheck['numbers']);

        if (!$bNumbersOK)
        {
            // Mismatch flagged, so set the overall number chweck flag to false
            $bCheckNumbersOK = false;
        }

        // Loop through the data check winnings
        foreach ( $aDataCheck['winnings'] as $sPrizeLevel => $aWinData )
        {
           // Pass the win data for this prize level to the equality checking function
           $bWinningsOK = LotteryResultsAdmin::checkPrizesEquality($aWinData);
           if (!$bWinningsOK)
           {
               // Mismatch flagged, so set overall winnings check flag to false
               $bCheckWinningsOK = false;
           }
        }

        // Pass the retrieved next jackpot amount and rollover (now in data check array) to equality checking function
        // and return the status of the check
        // (can use the numbers equality function for this)
        $bNJAmtOK = LotteryResultsAdmin::checkNumbersEquality($aDataCheck['nj_amount']);
        $bNJRoOK = LotteryResultsAdmin::checkNumbersEquality($aDataCheck['nj_rollover']);

        if (!$bNJAmtOK)
        {
            // Mismatch flagged, so set the overall number chweck flag to false
            $bCheckNJAmountOK = false;
        }

        if (!$bNJRoOK)
        {
            // Mismatch flagged, so set the overall number chweck flag to false
            $bCheckNJRolloverOK = false;
        }

        // At this point, do a check to see if we have any winners information
        // by seeing if the key exists in the aDataCheck array
        // If we don't have any winners information, we DO NOT WANT to run the Winscan
        // so set flag here
        if (!array_key_exists('winnings', $aDataCheck))
        {
            $bWinnersExist = false;
        }

        /* Uncomment the following for debug */
 //      var_dump($aDataCheck);
 //      print_d($aDataCheck);
 //      echo ("Numbers: " . ($bCheckNumbersOK?"Yes":"No") . "<br>");
 //      echo ("Winnings: " . ($bCheckWinningsOK?"Yes":"No")  ."<br>");
 //      echo ("Next Jackpot Amount: " . ($bCheckNJAmountOK?"Yes":"No")  ."<br>");
 //      echo ("Next Jackpot Rollover: " . ($bCheckNJRolloverOK?"Yes":"No")  ."<br><br>");
 //      echo ("Overall: " . ($bCheckNumbersOK&&$bCheckWinningsOK&&$bCheckNJAmountOK&&$bCheckNJRolloverOK?"Yes":"No") . "<br>");
 //      print_d($aRowsToBeChecked);
//       die();


        // Finally, perform Boolean AND on the four overall flags
        if ($bCheckNumbersOK&&$bCheckWinningsOK&&$bCheckNJAmountOK&&$bCheckNJRolloverOK)
        {
            // Enter into main results table
            // Send confirmation email to administrators


            // Update results as status '2'
//           echo ("<br/>No data mismatch, updating with status 2");
            LotteryResultsAdmin::updateAllCheckingStatusForLottery($iLotteryID, $iDraw, 2);

            // Write audit log row to record checking complete and no mismatch found
            $sAuditLogMsg = "Results check for {$aLotteryInformation['comment']} draw " .
                            "on {$aNextDrawDetails['drawdate']} completed with NO mismatches";

            AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_CHECKING_FINISHED_OK', 'lottery_results_checking', 0);
            /**
             *  We can now add the results into the main results table
             *
             *  Process as follows:
             *  1) Get prev. stored jackpot value for this lottery from r_lottery_dates table
             *  2) Get one of the 'checked' results rows
             *  3) Unserialize the winnings array and explode the numbers on | character
             *  4) Check the value of the 'top' (jackpot) prize level for number of winners
             *    -- IF WINNERS:
             *          jackpot amount to store =  top prize winners * top prize amount
             *    -- IF NO WINNERS:
             *          jackpot amount = amount previously stored
             *
             *  5) Store the numbers and winners data in the main results table
             *
             *  NB: WE NEED TO NOTIFY SOMEONE THAT THE NEXT JACKPOT AMOUNTS (AND DRAW DATES)
             *  FOR EACH DRAW MUST -ALWAYS- BE KEPT AS UP TO DATE AS POSSIBLE!!!!!!
             */

            $aDrawInfoToStore = array_shift($aRowsToBeChecked);

            $aNumbers = explode("|", $aDrawInfoToStore['numbers']);
            $aWinnings = unserialize($aDrawInfoToStore['winnings']);

            $iJackpot = 0;

            $iNextJackpot = $aDrawInfoToStore['next_jackpot_amount'];
            $bRollover = $aDrawInfoToStore['next_jackpot_rollover'];

            if ($aLotteryInformation['jackpot']) {
                $iJackpot = $aLotteryInformation['jackpot'];
            }
            else
            {
                $iJackpot = $aWinnings[0]['prize'] * $aWinnings[0]['winners'];
            }

            // Check whether a row has been entered for these results already
            // by getting the draw id from the reference and lottery ID
            $iDrawId = LotteryResultsAdmin::getLotteryDrawIdFromLotteryIdAndRef($iLotteryID, $iDraw);

            // If so, then edit the results, otherwise add a new row
            if (is_numeric($iDrawId))
            {
                LotteryResultsAdmin::editResult($iLotteryID, $aDrawInfoToStore['fk_lottery_date'], $iDraw, $aNumbers, $iJackpot, $aWinnings);
            }
            else
            {
                $iDrawId = LotteryResultsAdmin::addResults($iLotteryID, $aDrawInfoToStore['fk_lottery_date'], $iDraw, $aNumbers, $iJackpot, $aWinnings);
            }


            // Need to add the next jackpot information to the relevant table now
            // Firstly, we need to get the correct cutoff and price information
            // These are class variables, so get them from the class
            $sLottoClass = Lottery::getLotteryClassName($iLotteryID);



            // Create the new class (yay for variable variables!)
            // Defaulting language to english here
            $oLottery = new $sLottoClass(1);


            // Update the next jackpot information
            LotteryResultsAdmin::updateNextJackpotInformation($iDraw, $iLotteryID, $oLottery->getCutoff(), $oLottery->getPrice(), $iNextJackpot, $bRollover);

            // Write audit log row to record results copied to main results table
            $sAuditLogMsg = "Checked results for {$aLotteryInformation['comment']} draw " .
                            "on {$aNextDrawDetails['drawdate']} copied to main results table " .
                            "and next jackpot information for this draw updated";

            AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_RESULTS_COPIED_TO_MAIN', 'lottery_results_checking', 0);

            // Send email to administrators
            $aEmailData = array('lottery_name'=>$aLotteryInformation['comment'],
                                'draw_date'=>$aNextDrawDetails['drawdate'],
                                'check_status'=>'ok',
                                'lottery_id' =>$iLotteryID,
                                'draw_ref'=>$iDraw,
                                'data_entered'=>array('numbers'=>$aNumbers,
                                                      'winnings'=>$aWinnings,
                                                      'next_jackpot_amount'=>$iNextJackpot,
                                                      'next_jackpot_rollover'=>$bRollover));

            EmailAdmin::sendEmailToAdministrator($aEmailData, 'all_results_checked');

            # Flush the many caches
            LLCache::delete('GE-primaryGameList');
            LLCache::delete('GE-allGameList');
            LLCache::delete('GE-lotteryDraws');
            LLCache::delete('GE-lotteryResults');
            LLCache::delete('LR-LotteryResults-getWinners');

            /**
             * We now have 3 confirmed sets of results for this draw.
             *
             * The next stage is to start the automated winscan prodecdure
             * to extract any winners from the tickets that have been bought,
             *
             * BUT ONLY IF WE ACTUALLY HAVE WINNERS DATA!!
             *
             */
             if ($bWinnersExist === true)
             {

               // Need to add a flag onto the redirect URL to indicate winscan done
              $redirect .= "&ws=1";

//                 echo("Starting winscan<br/>");

//                 echo("Draw Id is $iDrawId, lottery id is $iLotteryID");

                // Before we start the winscan, we can add the breakdown of these results
                // into the relevant breakdown tables

                // Create a new lottery result object, passing in the draw ID and lottery object
                $oResult=new \LL\Results\LotteryResult($iDrawId,Lottery::getById($iLotteryID));

                // Insert the numbers
                foreach ($aNumbers as $iNumber) {
                    //echo ("Adding number $iNumber <br/>");
                    $oResult->oDrawBalls->addBall($iNumber);
                }

                // Insert the prizes
                $iPrizeTierNumber=1;
                foreach($aWinnings as $aWinner){
                   //print_r($aWinner);
                   $iNumberOfWinners=intval($aWinner['winners']);
                   $dPrizePerWinner=floatval($aWinner['prize']);

                   //echo ("Adding prize tier $iPrizeTierNumber, num winners $iNumberOfWinners, prize per winner $dPrizePerWinner");

                   $oResult->oDrawPrizes->addPrize($iPrizeTierNumber,$iNumberOfWinners,$dPrizePerWinner);

                   $iPrizeTierNumber++;
                }

                // Commit the rows
                $oResult->save();

                //echo("We have winners, so perform winscan!<br/>");

                $sAuditLogMsg = "Winscan started for {$aLotteryInformation['comment']} draw " .
                                "on {$aNextDrawDetails['drawdate']} ";
                AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_START', 'lottery_results_checking', 0);

                $aEmailData = array('lottery_name'=>$aLotteryInformation['comment'],
                                    'draw_date'=>$aNextDrawDetails['drawdate']);
                EmailAdmin::sendEmailToAdministrator($aEmailData, 'winscan_started');

                // Get winners for this lottery
                $aWinscanWinners = LotteryResultsAdmin::getWinners($iLotteryID, $iDraw, true);

                // We'll neeed to keep a record of any winning transactions
                $aWinscanTransactions = array();

                // Do we have winners?
                if ($aWinscanWinners)
                {
                    // Get the two thresholds for this lottery
                    $iThresholdInd = $aLotteryInformation['ind_paythreshold']; // individual payout threshold
                    $iThresholdDraw = $aLotteryInformation['draw_paythreshold']; // total payout threshold

                    // Running total of payouts for this draw
                    $fPayoutsRunningTotal = 0;

                    // Go through winners
                    foreach ($aWinscanWinners as $aWinscanWinner)
                    {
                        // Ticket information for this win
                        $aTicket = TicketsAdmin::getTicket($aWinscanWinner['bookingitem_id']);

                        $fTransactionAmount = $aWinscanWinner['transaction_amount'];

                        // Check we have information to be going on with
                        if ( $aTicket['win_title'] && is_numeric($fTransactionAmount) &&  $fTransactionAmount > 0)
                        {
                            // Transaction reference is the matching prize level
                            $sTransactionRef =  $aTicket['lottery'] . " - " . $aTicket['win_title'];

                            // If the transaction amount is BELOW the individual threshold,
                            // add the winning transaction to the transactions table
                            // but set it as pending, not completed
                            // we'll set as completed later on after a further check...
                            if ($fTransactionAmount <= $iThresholdInd)
                            {

                                $sAuditLogMsg = "Winning ticket found for {$aLotteryInformation['comment']} draw " .
                                                "on {$aNextDrawDetails['drawdate']} - " .
                                                "Booking ID {$aWinscanWinner['bookingitem_id']}, ".
                                                "Matching prize {$sTransactionRef}, " .
                                                "Win amount {$fTransactionAmount}.  " .
                                                "Ticket is BELOW payout threshold.";

                                AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_FOUND_BELOW_THRESHOLD', 'lottery_results_checking', 0);

                                // Should return a transaction id if successful
                                $iTransactionID = LotteryResultsAdmin::addWinTransactionPending($aTicket['member_id'], $fTransactionAmount, $sTransactionRef);

                                if (!$iTransactionID)
                                {
                                    // Problem writing transaction
                                    // add audit log entry
                                    $sAuditLogMsg = "Problem storing payout transaction for winning ticket " .
                                                    "for {$aLotteryInformation['comment']} draw on {$aNextDrawDetails['drawdate']}  - " .
                                                    "Booking ID {$aWinscanWinner['bookingitem_id']}";

                                    AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_STORE_TRANSACTION_FAILURE', 'lottery_results_checking', 0);
                                }
                                else
                                {
                                    // No problem writing trandaction
                                    // so we continue
                                    $sAuditLogMsg = "Transaction successfully stored for winning ticket payout " .
                                                    "for {$aLotteryInformation['comment']} draw on {$aNextDrawDetails['drawdate']}  - " .
                                                    "Booking ID {$aWinscanWinner['bookingitem_id']}, " .
                                                    "Transaction ID {$iTransactionID}";

                                    AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_STORE_TRANSACTION_SUCCESS', 'lottery_results_checking', 0);

                                    // Send the customer the email saying they've won
                                    sendLotteryWonCustomerEmail($aTicket, $iBookingItemID, $aLotteryInformation, $aNextDrawDetails, $aWinscanWinner, $iTransactionID);

                                    // Add the win amount to the running total of all win amounts
                                    // for this winscan
                                    // and keep a record of this transaction for later
                                    $fPayoutsRunningTotal += $aWinscanWinner['transaction_amount'];
                                    $aWinscanTransactions[] = array('transaction_id' => $iTransactionID,
                                                                    'bookingitem_id' => $aWinscanWinner['bookingitem_id'],
                                                                    'transaction_amount' => $fTransactionAmount,
                                                                    'transaction_ref' => $sTransactionRef,
                                                                    'member_id' => $aTicket['member_id']);
                                }
                            }
                            else
                            {
                                // Transaction amount is ABOVE the individual threshold,
                                // so add audit log entry
                                // and send administration and customer emails
                                $sAuditLogMsg = "Winning ticket found for {$aLotteryInformation['comment']} draw " .
                                                "on {$aNextDrawDetails['drawdate']} - " .
                                                "Booking ID {$aWinscanWinner['bookingitem_id']}, ".
                                                "Matching prize {$sTransactionRef}, " .
                                                "Win amount {$fTransactionAmount}.  " .
                                                "Ticket is ABOVE payout threshold.";

                                AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_WIN_FOUND_ABOVE_THRESHOLD', 'lottery_results_checking', 0);
/*
                                // Register a ticket in Zendesk
                                $zd = new Zendesk();
                                $sDrawDateString = date("D d M Y \a\\t H:i T", strtotime($aNextDrawDetails['drawdate']));

                                $sBody = <<<EOT
This is to inform you that the Winscan for the {$aLotteryInformation['comment']} draw on {$sDrawDateString} has found
a payout on a winning ticket which exceeds the individual payout limit for this lottery.

Customer ID: {$aTicket['member_id']}
Booking Item ID: {$aWinscanWinner['bookingitem_id']}
Winning combination: {$sTransactionRef}
Win amount: {$fTransactionAmount}
Threshold: {$iThresholdInd}

The payout transaction has been recorded in the database but has not yet been confirmed.
EOT;

                                $aNewTicket = array ('subject'  => "Winning ticket for {$aLotteryInformation['comment']} above payout threshold",
                                                     'comment'  => array ('body' => $sBody ),
                                                     'priority' => "normal",
                                                     //'group_id' => 123456,
                                                     'type'     => "task"
                                                      );

                                $zd->createTicket($aNewTicket);
 */

                                $aEmailData = array('lottery_name'=> $aLotteryInformation['comment'],
                                                    'draw_date'   => $aNextDrawDetails['drawdate'],
                                                    'customer_id' => $aTicket['member_id'],
                                                    'bookingitem_id'  => $aWinscanWinner['bookingitem_id'],
                                                    'win_combination' => $sTransactionRef,
                                                    'win_amount'  => $fTransactionAmount,
                                                    'threshold'   => $iThresholdInd);

                                EmailAdmin::sendEmailToAdministrator($aEmailData, 'winscan_win_above_ind_threshold');

                                // Send customer email saying they've won here
                                sendLotteryWonCustomerEmail($aTicket, $iBookingItemID, $aLotteryInformation, $aNextDrawDetails, $aWinscanWinner, $iTransactionID);

                            }
                        }
                    }

                    // Winscan has been completed, audit log accordingly.
                    $sAuditLogMsg = "Winscan completed for {$aLotteryInformation['comment']} draw " .
                                    "on {$aNextDrawDetails['drawdate']} - now checking all payouts";

                    AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_CHECKING_PAYOUTS_START', 'lottery_results_checking', 0);

                    // We should now have an array containing all the information about each payout transaction
                    // The next stage is to ensure that the total amount of each payout transaction
                    // is below the overall draw threshold
                    // If this is the case, then set all transactions as confirmed
                    // otherwise, send admin email
                    if ($fPayoutsRunningTotal <= $iThresholdDraw)
                    {
                        // Running total amount is below threshold

                        $sAuditLogMsg = "Payouts for {$aLotteryInformation['comment']} draw " .
                                        "on {$aNextDrawDetails['drawdate']} below draw threshold of {$iThresholdDraw} - " .
                                        " starting confirmation of payouts";

                        AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_CHECKING_PAYOUTS_BELOW_THRESHOLD', 'lottery_results_checking', 0);

                        // Go over the transaction information
                        foreach ($aWinscanTransactions as $aTransactionInfo) {

                            $sAuditLogIdentifier = "";

                            // Attempt to set transaction as confirmed
                            if (LotteryResultsAdmin::addWinTransactionConfirmed($aTransactionInfo))
                            {
                               // Success
                               $sAuditLogIdentifier = "WINSCAN_CHECKING_PAYOUTS_CONFIRM_OK";
                               $sAuditLogMsg = "Confirmed payout for {$aLotteryInformation['comment']} draw " .
                                                "on {$aNextDrawDetails['drawdate']} - transaction id {$aTransactionInfo['transaction_id']}";

                            }
                            else
                            {
                                // Failed
                                $sAuditLogIdentifier = "WINSCAN_CHECKING_PAYOUTS_CONFIRM_FAILED";
                                $sAuditLogMsg = "Failed to confirm payout for {$aLotteryInformation['comment']} draw " .
                                                "on {$aNextDrawDetails['drawdate']} - transaction id {$aTransactionInfo['transaction_id']}";
                            }

                            // Write audit log entry accordingly
                            AuditLog::LogItem($sAuditLogMsg, $sAuditLogIdentifier, 'lottery_results_checking', 0);
                        }

                        // At this stage, the winscan is complete
                        // Audit log and send admin emails
                        $sAuditLogMsg = "Confirmation of payouts for {$aLotteryInformation['comment']} draw " .
                                        "on {$aNextDrawDetails['drawdate']} completed";

                        AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_COMPLETE', 'lottery_results_checking', 0);

                        $aEmailData = array('lottery_name'=> $aLotteryInformation['comment'],
                                            'draw_date'   => $aNextDrawDetails['drawdate'],
                                            'win_amount_total'  => $fPayoutsRunningTotal,
                                            'threshold'   => $iThresholdDraw);

                        EmailAdmin::sendEmailToAdministrator($aEmailData, 'winscan_completed_transactions_confirmed');

                    }
                    else
                    {
                        // Running total is ABOVE threshold
                        // so audit log and send emails accordingly
                        $sAuditLogMsg = "Payouts for {$aLotteryInformation['comment']} draw " .
                                        "on {$aNextDrawDetails['drawdate']} ABOVE draw threshold of {$iThresholdDraw} - " .
                                        " sending email";

                        AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_CHECKING_PAYOUTS_ABOVE_THRESHOLD', 'lottery_results_checking', 0);


                        $aEmailData = array('lottery_name'=> $aLotteryInformation['comment'],
                                            'draw_date'   => $aNextDrawDetails['drawdate'],
                                            'win_amount_total'  => $fPayoutsRunningTotal,
                                            'threshold'   => $iThresholdDraw);

                        EmailAdmin::sendEmailToAdministrator($aEmailData, 'winscan_win_above_draw_threshold');

                        $sAuditLogMsg = "Winscan ended for {$aLotteryInformation['comment']} draw " .
                                         "on {$aNextDrawDetails['drawdate']} ";
                        AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_COMPLETE_ABOVE_THRESHOLD', 'lottery_results_checking', 0);
                    }
                }
                else
                {
  //                  echo ("No winners have been found in the Winscan!");

                   // No winners found in the winscan, log accordingly and send emails
                    $sAuditLogMsg = "Winscan completed for {$aLotteryInformation['comment']} draw " .
                                    "on {$aNextDrawDetails['drawdate']} " .
                                    "and no winners found";

                    AuditLog::LogItem($sAuditLogMsg, 'WINSCAN_COMPLETE_NO_WINNERS', 'lottery_results_checking', 0);

                    $aEmailData = array('lottery_name'=> $aLotteryInformation['comment'],
                                        'draw_date'   => $aNextDrawDetails['drawdate']);

                    EmailAdmin::sendEmailToAdministrator($aEmailData, 'winscan_completed_no_winners');
                }

                /*
                 * COMMENTED OUT UNTIL WE WORK OUT EXACLTLY WHAT'S HAPPENING HERE

                // Send email alerts out for each language
                foreach (array('en', 'de') as $sLang)
                {
                   Email::sendResultsAlerts($iLotteryID, $sLang);
                   Email::sendNextJackpotAlerts($iLotteryID, $sLang);
                }
                */
             }
             else
             {
                $sAuditLogMsg = "No winners information supplied, so winscan for {$aLotteryInformation['comment']} draw " .
                                "on {$aNextDrawDetails['drawdate']} not started ";
                AuditLog::LogItem($sAuditLogMsg, 'RESULTS_CHECKER_NO_WINSCAN', 'lottery_results_checking', 0);

                $aEmailData = array('lottery_name'=>$aLotteryInformation['comment'],
                                    'draw_date'=>$aNextDrawDetails['drawdate']);
                EmailAdmin::sendEmailToAdministrator($aEmailData, 'no_winscan');

 //               echo("No winners, so no winscan!<br/>");
             }
        }
        else
        {
            // Send email to administrators
            // Resend checking emails
            // Update results as status '3'

//            echo ("<br/>Data mismatch, updating with status 3");
            LotteryResultsAdmin::updateAllCheckingStatusForLottery($iLotteryID, $iDraw, 3);

            // Write audit log row to record checking complete and mismatches found
            $sAuditLogMsg = "Results check for {$aLotteryInformation['comment']} draw " .
                            "on {$aNextDrawDetails['drawdate']} completed - mismatches found";

            AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_CHECKING_FINISHED_MISMATCH', 'lottery_results_checking', 0);

            foreach ($aRowsToBeChecked as $aData) {
                $iCheckerID = $aData['checker_id'];
                $aCheckerInfo = LotteryResultsAdmin::getCheckerInfoFromID($iCheckerID, $iLotteryID, $iDraw);

                EmailAdmin::sendEmailToCheckerAgain($iCheckerID, $aCheckerInfo, $iLotteryID, $iDraw,
                                                    $aLotteryInformation['comment'], $aNextDrawDetails['drawdate']);
            }

            // Send email to administrators
            $aEmailData = array('lottery_name'=>$aLotteryInformation['comment'],
                                'draw_date'=>$aNextDrawDetails['drawdate'],
                                'check_status'=>'mismatch',
                                'lottery_id' =>$iLotteryID,
                                'draw_ref'=>$iDraw);

            EmailAdmin::sendEmailToAdministrator($aEmailData, 'all_results_checked');
        }

    }
    else
    {
        $sAuditLogMsg = "Waiting on sll checks for {$aLotteryInformation['comment']} draw " .
                        "on {$aNextDrawDetails['drawdate']}";

        AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_NOT_ALL_CHECKS_MADE', 'lottery_results_checking', 0);
//        echo("Not all entries made<br/>");
    }
//    echo($redirect);
    // Redirect to redirect location

    $sAuditLogMsg = "Redirecting to $redirect";
    AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_REDIRECT_START', 'lottery_results_checking', 0);

    // Kill any output
    $contents = ob_get_clean();

    header("Location: $redirect");
    echo "<script>window.location = '{$redirect}';</script>";
    die();

    $sAuditLogMsg = "Redirecting done";
    AuditLog::LogItem($sAuditLogMsg, 'RESULT_CHECKER_REDIRECT_FINISH', 'lottery_results_checking', 0);
    exit;

}
