<?php include("includes/top.php");

$iId = $_GET["id"];
if($_GET["fk_product_id"] && !$_SESSION[$oSecurityObject->getsAdminType() . "formdata"]["fk_product_id"]){
	$_SESSION[$oSecurityObject->getsAdminType() . "formdata"]["fk_product_id"] = $_GET["fk_product_id"];
}
$iLangId = $_GET["lid"];
if(!$iLangId){
	$iLangId = 0;
}
else{
	$currentLanguage = Language::getLanguageTitle($iLangId);
}


$bValidTable = false;

$sTablename = $_GET["tablename"];
$sLanguageTablename = $_GET["languagetable"];
$sPreviousPage = $_GET["pp"] . "?tablename=" . $sTablename;

$iReturnID = NULL;
if($_GET["returnid"]){
	$sPreviousPage .= "&id=" . $_GET["returnid"];
	$iReturnID = $_GET["returnid"];
}


if($sOldQueryString){
	if($_GET["pp"] && strstr($sOldQueryString, "&pp=")){
		$iPPStartPos = strpos($sOldQueryString, "&pp=");
		$sPPqs = substr($sOldQueryString, strpos(sOldQueryString, "&pp="));
		$sPPqsTrimmed = substr($sPPqs, 1);
		$iPPEndPos = strpos($sPPqsTrimmed, "&");
		$sPPqs = substr($sPPqs, 0, $iPPEndPos + 1);
		$sOldQueryString = str_replace($sPPqs, "", $sOldQueryString);
	}
	if($sOldQueryString){
		$sPreviousPage .= "&qs=" . $sOldQueryString;
	}
}
$bValidTable = GenericTableAdmin::checkTableExists($sTablename);
if($bValidTable && $iLangId && $sLanguageTablename){
	if(!GenericTableAdmin::checkTableExists($sLanguageTablename)){
		$bValidTable = false;
		echo "<span class='error'>Languages not applicable. Please set language filter to \'All\'.</span>";
	}
}

?>
<script type="text/javascript" language="javascript">
function change(id) {
	if (id == '1') {
		$("#newfilename_div").css("display", "none");
		$("#newfile_div").css("display", "none");
	}
	else if (id == '2') {
		$("#newfilename_div").css("display", "");
		$("#newfile_div").css("display", "none");
	}
	else if (id == '3') {
		$("#newfilename_div").css("display", "none");
		$("#newfile_div").css("display", "");
	}
}
</script>

<?php
    /**
     * Show a h1 for generic items
     * For handled classes we'll let the generic details handler do that
     */
     if(!class_exists($sTablename)):
?>
	<h1><?=ucwords($oPage->sTablename)?></h1>
	<?php
     endif;
	?>

		<?php if ($oErrors->getErrorCount() > 0) { ?>
        <table width="100%" class="detailsform"  border="0" cellspacing="0" cellpadding="5">
        <tr>
        <td bgcolor="#FF0000" class="white"><?=$oErrors->getErrorString()?></td>
        </tr>
        </table><?php
       	$oErrors->clearErrors();
        }
		?>

        <?php
                AuditLog::LogItem($oSecurityObject->getsFullName().' viewed '.$sTablename.' id '.$iId.' using generic-details', 'RECORD_VIEW', $sTablename, $iId);

		if($iLangId && $sLanguageTablename){
			echo GenericTableAdmin::createGenericLanguageDetailsForm($iId, $sTablename, $sLanguageTablename, $iLangId, $_SESSION[$oSecurityObject->getsAdminType() . "formdata"], urlencode($sPreviousPage), $iReturnID);
		}
		else{
			$bDeleteButton = false;
			if($sTablename == "multimedia_cmn"){
				$bDeleteButton = true;
			}

                        if(class_exists($sTablename)){
                            $oHandler=new $sTablename;
                            if(method_exists($oHandler,'GenericDetails')){
                                echo $oHandler->GenericDetails($sTablename, $iId, $_SESSION[$oSecurityObject->getsAdminType() . "formdata"], urlencode($sPreviousPage), $iWebsiteID, $bDeleteButton, $iReturnID);
                            }else{
                                echo GenericTableAdmin::createGenericDetailsForm($sTablename, $iId, $_SESSION[$oSecurityObject->getsAdminType() . "formdata"], urlencode($sPreviousPage), $iWebsiteID, $bDeleteButton, $iReturnID);
                            }
                        }else{
                            echo GenericTableAdmin::createGenericDetailsForm($sTablename, $iId, $_SESSION[$oSecurityObject->getsAdminType() . "formdata"], urlencode($sPreviousPage), $iWebsiteID, $bDeleteButton, $iReturnID);
                        }
		}

unset($_SESSION[$oSecurityObject->getsAdminType() . "formdata"]);


include("includes/bottom.php");

?>
