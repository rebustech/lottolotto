<?php

// Status page called by payment gateways
// used only on admin page

include("../includes/autoload.php");

switch ( $_GET['g'] )
{
    
    case 'payfrex' :
        $oPaymentGateway = new PayFrex();
        
        // Read the incoming input stream
        $sXML = file_get_contents("php://input");
		
        // Store input stream for debugging if need be
        file_put_contents(dirname(__FILE__).'/test.xml', $sXML);
           
        $oPaymentGateway->ConfirmPayment($sXML);				
    break;

    case 'ncptest' :
        echo "Directory of this file is " . dirname(__FILE__);
    break;

    default:
        header("location: /403.html" );
        exit;
    break;
}
?>