<?php

if (!is_object($server)) {
	header("Location:../wsdl.php?wsdl");
	exit;
}

/*-----------------------------------------------------------*/
/* getRoomIDs Structures                 
/*-----------------------------------------------------------*/

$server->wsdl->addComplexType(
    'getNextPage_1_Input',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' 	=> array('name' => 'username', 	'type' => 'xsd:string'),
        'password' 	=> array('name' => 'password', 	'type' => 'xsd:string'),
		'pin' 		=> array('name' => 'pin', 		'type' => 'xsd:string'),
		'websiteid' => array('name' => 'websiteid', 'type' => 'xsd:int'),
		'visitorid' => array('name' => 'visitorid', 'type' => 'xsd:int'),
		'whitelist' => array('name' => 'whitelist', 'type' => 'xsd:string'),
		'blacklist' => array('name' => 'blacklist', 'type' => 'xsd:string'),
		'precision'	=> array('name' => 'precision',	'type' => 'xsd:int', 'default' => 4),
		'limit'		=> array('name' => 'limit',		'type' => 'xsd:int', 'default' => 3)
		
    )
);

$server->wsdl->addComplexType(
    'getNextPage_1_ArrayOutput',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:getNextPage_1_Output[]')
    ),
    'tns:getNextPage_1_Output'
);
$server->wsdl->addComplexType(
    'getNextPage_1_Output',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'next_page' => array('name' => 'next_page', 'type' => 'xsd:string'),
        'weight' 	=> array('name' => 'weight', 'type' => 'xsd:string')
    )
);

/*-----------------------------------------------------------*/
/* getNextPage_1 Register              
/*-----------------------------------------------------------*/

$server->register( 
	'getNextPage_1',                           					 	// method name
    array('getNextPage_1_Input' => 'tns:getNextPage_1_Input'),     		// input parameters
    array('return' => 'tns:getNextPage_1_ArrayOutput'),    		  	// output parameters
    'uri:wsdl',                   							 	 	// namespace
    'uri:wsdl/getNextPage_1',             				   			// SOAPAction
    'rpc',                              							// style
    'encoded',                           							// use
	'getNextPage_1 Function : Returns back an array of Page IDs and Product IDs according to the visitor browsing history'			// documentation
	);
	
/*-----------------------------------------------------------*/
/* getNextPage_1 Function                   
/*-----------------------------------------------------------*/

function getNextPage_1($getNextPage_1_Input) {

	extract($getNextPage_1_Input, EXTR_OVERWRITE);
	$iUserID = Security::Authenticate($username, $password, $pin);
	if ( $iUserID ) {
		if ( Security::ValidateAccessToWebsite($iUserID, $websiteid) )
		{
			return ProfileType1::getNextPage($websiteid, $visitorid,  $whitelist, $blacklist, $precision, $limit);
		}
		else
		{
			return new soap_fault('Client', '', 'No access to the given Website ID');
		}
	}
	else 
	{
		return new soap_fault('Client', '', 'Incorrect Authentication Details');
	}
	
}

$aDescription['getNextPage_1'] = "This function return a list of pages suggested to the particular user's browsing history";

$aInput['getNextPage_1'] =  array(
        'username' 	=> array('type' => 'string','doc' => 'Username of Profiler Manager.'),
        'password' 	=> array('type' => 'string','doc' => 'Password of Profiler Manager.'),
		'pin' 		=> array('type' => 'string','doc' => 'Pin of Profiler Manager.'),
		'websiteid' => array('type' => 'int', 	'doc' => 'Website ID'),
		'visitorid' => array('type' => 'int',	'doc' => 'Visitor ID'),
		'whitelist'	=> array('type' 	=> 'string','doc' => 'Optional: Whitelist of Page IDs to return, comma separated'),
		'blacklist'	=> array('type' 	=> 'string','doc' => 'Optional: Blacklist of Page IDs to exclude from return, comma separated'),
		'precision'	=> array('type' => 'int',	'doc' => "Optional: Number of browsing history pages to match"),
		'limit'		=> array('type' => 'int',	'doc' => "Optional: Number of items to return back")

	);

$aOutput['getNextPage_1'] =  array(
		'next_page' => array('type' => 'string', 'doc' => 'An Array of Page IDs and Product IDs of the form PageID:ProductID'),
		'weight' => array('type' => 'int', 'doc' => 'Weight of Page')
	);

?>