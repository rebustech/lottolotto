<?php

if (!is_object($server)) {
	header("Location:../wsdl.php?wsdl");
	exit;
}

/*-----------------------------------------------------------*/
/* getRoomIDs Structures                 
/*-----------------------------------------------------------*/

$server->wsdl->addComplexType(
    'authenticateUser_Input',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'username' 	=> array('name' => 'username', 'type' => 'xsd:string'),
        'password' 	=> array('name' => 'password', 'type' => 'xsd:string'),
		'pin' 		=> array('name' => 'pin', 'type' => 'xsd:string')		
    )
);

/*-----------------------------------------------------------*/
/* getRoomIDs Register              
/*-----------------------------------------------------------*/

$server->register( 
	'authenticateUser',                           					 	// method name
    array('authenticateUser_Input' => 'tns:authenticateUser_Input'),     		// input parameters
    array('return' => 'xsd:int'),    		  	// output parameters
    'uri:wsdl',                   							 	 	// namespace
    'uri:wsdl/authenticateUser',             				   			// SOAPAction
    'rpc',                              							// style
    'encoded',                           							// use
	'authenticateUser Function : Use this function to verify the given username, password and pin of an affiliate.'			// documentation
	);
	
/*-----------------------------------------------------------*/
/* getRoomIDs Function                   
/*-----------------------------------------------------------*/

function authenticateUser($authenticateUser_Input) {

	extract($authenticateUser_Input, EXTR_OVERWRITE);
	$iUserID = WebServices::Authenticate($username, $password, $pin);
	if ( $iUserID ) {
		return true;
	}
	else 
	{
		return false;
	}
	
}

$aDescription['authenticateUser'] = "Authenticate affiliate with given credentials.";

$aInput['authenticateUser'] =  array(
        'username' 	=> array('type' => 'string','doc' => 'Username of Affiliate.'),
        'password' 	=> array('type' => 'string','doc' => 'Password of Affiliate.'),
		'pin' 		=> array('type' => 'string','doc' => 'Pin of Affiliate.')
	);

$aOutput['authenticateUser'] =  array(
		'return' => array('type' => 'int', 'doc' => 'Flag: 1 on Successful, 0 on Fail' )
	);

?>