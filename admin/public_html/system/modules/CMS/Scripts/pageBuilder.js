var pageBuilder={};

/**
 * Adds a new compoment to the ul containing the button
 */
pageBuilder.addComponent=function(){
    oNewItem=$('<li class="draggable">New Item</li>');
    $(this).before(oNewItem);
}

/**
 * Creates the markup and functionality for the page builder from the page builder
 * JSON data embedded in the page
 */
pageBuilder.setup=function(){
    //Construct the markup
    //Start with the top level items all in a ul
    var eTopLevel=$('<ul class="pageTopLevels connectedSortable"/>');
    if(typeof pageBuilderData.childComponents != 'undefined'){
        for(var iComponentIndex in pageBuilderData.childComponents){
            var oComponent=pageBuilderData.childComponents[iComponentIndex];
            var eChildComponent=pageBuilder.createComponentMarkup(oComponent,pageBuilderData.childComponents.length);
            eTopLevel.append(eChildComponent);
        }
        if(pageBuilderData.childComponents.length>1){
            var eCommonComponentHolder=$('<li class="draggable commonElements"><h4>Common components on any of the above</h4></li>');
            var eCommonComponentHolderUL=$('<ul class="connectedSortable"/>');
            eCommonComponentHolder.append(eCommonComponentHolderUL);
            for(var iComponentIndex in pageBuilderData.commonModifiers){
                var oComponent=pageBuilderData.commonModifiers[iComponentIndex];
                var eChildComponent=pageBuilder.createComponentMarkup(oComponent);
                eCommonComponentHolderUL.append(eChildComponent);
            }
            eTopLevel.append(eCommonComponentHolder);

            var eCommonAllComponentHolder=$('<li class="draggable commonElements"><h4>Common components on ALL of the above</h4></li>');
            var eCommonAllComponentHolderUL=$('<ul class="connectedSortable"/>');
            eCommonAllComponentHolder.append(eCommonAllComponentHolderUL);
            eTopLevel.append(eCommonAllComponentHolder);
        }
    }

    /**
     * Put an add button on the end which will enable us to add a new component
     */
    var oPageButton=$('<div class="addComponentButton">+</div>');
    oPageButton.click(pageBuilder.addComponent);
    eTopLevel.append(oPageButton);

    $('.cms_page_holder').html(eTopLevel);

    $('.cms_page_holder ul').sortable({
      connectWith: ".connectedSortable",
      placeholder: "ui-sortable-placeholder"
    }).disableSelection();

    $('.cms_page_holder li > div').click(function(){
        var eActiveComponent=$('.cms_page_holder li > div.active');
        if(eActiveComponent.length>0){
            var eActiveForm=eActiveComponent[0].eForm;
            $('.cms_page_data').append(eActiveForm);
        }
        $('.cms_page_holder li > div.active').removeClass('active');
        $(this).addClass('active');
        $('.cms_page_config').html($(this)[0].eForm);
    });
}

pageBuilder.editCompoment=function(){
    //var url='/administration/generic-details.php?tablename=page_components&id='++'&pp=generic-listing.php';

}

/**
 * Creates a game engine component
 * @param object oComponent Object data containing the component to be created
 * @param int iTotalInSet
 * @returns jQuery Returns the jQuery object for the LI that can be appended to the list
 * of components in the caller
 */
pageBuilder.createComponentMarkup=function(oComponent,iTotalInSet){
    try{
        var eComponent=$('<li class="draggable"></li>');
        eComponent.attr('data-id',oComponent.id).attr('parent-id',oComponent.parent_id);
        eComponent.css('width',((100/iTotalInSet)-2)+'%');
        var eItem=$('<div/>');
        var eTitle=$('<h2/>').text(oComponent.name);
        var eClass=$('<h6/>').text('('+oComponent.handler+')');
        var eChildren=$('<ul class="connectedSortable"/>');
        //Add the fields

        //Build it up
        eComponent.append(eItem);
        eItem.append(eTitle);
        eItem.append(eClass);

        //Add any children
        if(oComponent.childComponents.length>0){
            eComponent.append(eChildren);
            for(var iComponentIndex in oComponent.childComponents){
                var oChildComponent=oComponent.childComponents[iComponentIndex];
                var eChildComponent=pageBuilder.createComponentMarkup(oChildComponent,1);
                eChildren.append(eChildComponent);
            }
        }

        //Add config values
        eForm=$('<fieldset><h3>'+oComponent.name+'</h3>');
        for(var iFieldIndex in oComponent.configurationFields){
            var oField=oComponent.configurationFields[iFieldIndex];
            var eField=$('<label><span>'+oField.sCaption+'</span><input value="'+oField.mValue+'" name="configurationField['+oComponent.id+']['+oField.sName+']"/></label>');
            eForm.append(eField);
        }
        var eField=$('<label><span>Parent</span><input value="'+oComponent.parent_id+'" name="component_data['+oComponent.id+'][parent_id]"/></label>');
        eForm.append(eField);
        eItem[0].eForm=eForm;
        $('.cms_page_data').append(eForm);
        return eComponent;
    }catch(e){
        alert(e);
    }
}
$(document).ready(function(){
    $.getScript('/system/modules/CMS/Scripts/froala/js/froala_editor.min.js',function(){
            setTimeout(function(){
                //$('#cms_page_builder textarea').editable({inlineMode: false})
            },2000);
    })
});

/**
 * URGH - Matt didn't use this so commenting out
 */
//pageBuilder.setup();