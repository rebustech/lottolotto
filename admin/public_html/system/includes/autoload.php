<?php

	$sIncludePath = findDocumentRoot(realpath('.'));
        $_SERVER['DOCUMENT_ROOT']=$sIncludePath;
        /**
         * Function to find public_html. Because our server setup
         * has dynamic virtual hosts we can't rely on DOCUMENT_ROOT
         * @param type $path
         */
        function findDocumentRoot($path){
            //Correct the path for windows sytems
            $path=  str_replace('\\', '/', $path);

            $aParts=explode('/',$path);
            $bFound=false;
            $sPath='';
            foreach($aParts as $part){
                if($bFound) $sPath.='../';
                if($part=='public_html') $bFound=true;
            }
            return substr($sPath,0,-1);
        }

        function includePath($path){
            $sNewIncludePath=PATH_SEPARATOR.$path;
            $d=opendir($path);
            while($f=readdir($d)){
                $sNewPath=$path.'/'.$f;
                if(is_dir($sNewPath) && $f!='.' && $f!='..'){
                    $sNewIncludePath.=includePath($sNewPath);
                }
            }
            closedir($d);
            return $sNewIncludePath;
        }
        $sNewIncludePath=includePath(realpath($sIncludePath).'/system');
        $sModulesIncludePath=includePath(realpath($sIncludePath).'/../../modules');
        set_include_path(get_include_path().PATH_SEPARATOR.$sNewIncludePath.PATH_SEPARATOR.$sModulesIncludePath);

        /**
         * This autoloader just tries to include the file that matches the class name
         * The code above here will have already set the include path to include all the
         * subfolders in the project so PHP will handle locating the file natively
         * @param type $class_name
         */
        spl_autoload_register(function($aClassName) {
            $sClassFile=str_replace('\\','',$aClassName).'.class.php';
            @include($sClassFile);
	});
/*
        function number_format_locale($number,$decimals=2) {
            $locale = localeconv();
            return number_format($number,$decimals,
                       $locale['decimal_point'],
                       $locale['thousands_sep']);
        }
*/
        // copied from front end autoload
        function number_format_locale($number,$decimals=2) {
            $number = reverseNumberFormat($number);

            #$number = preg_replace('/[^0-9\\.]*/','',$number);
            $locale = localeconv();
            return number_format($number, $decimals,
                       $locale['decimal_point'],
                       $locale['thousands_sep']);
        }


        /**
        * Number formatting reversal function
        * Takes a number, checks if there's a decimal part
        * (by checking the characters towards the end of the string)
        * removes all non-numeric characters except the decimal point
        * and returns to 2 decimal places
        * Also correctly handles negative numbers by a similar process
        *
        * @param float $fInput input to function
        * @return float
        */
		if(!function_exists('reverseNumberFormat')) {
	        function reverseNumberFormat($fInput) {
	            // Set a flag to record if the number is negative
	            // default to nfalse
	            $bNeg = false;

	            // Cast to a string for this process
	            $sInput = (string) $fInput;

	            // Starting position character - we'll check if this is a minus sign
	            $cPos0 = substr($sInput, 0, 1);

	            // Check if the first character is a minus sign
	            // Set as negative if so
	            // and remove the character before performing further checks
	            if ($cPos0 == '-')
	            {
	                $bNeg = true;
	                $sInput = substr($sInput, 1);
	            }

	             // Get the characters at the second and thiird positions
	            // from the end - these will be checked in a moment
	            $cPos2 = substr($sInput, -2, 1);
	            $cPos3 = substr($sInput, -3, 1);

	            // If either of the two characters are NOT numeric
	            // replace them with something else for the moment
	            // in this case, a caret ^
	            // This will represent our decimal point
	            if (!is_numeric($cPos2))
	            {
	                $sInput = str_replace($cPos2, '^',  $sInput);
	            }

	            if (!is_numeric($cPos3))
	            {
	                $sInput = str_replace($cPos3, '^',  $sInput);
	            }

	            // Now remove all non-numeric characters in the string
	            // EXCEPT the caret
	            $sInput = preg_replace('/[^a-zA-Z0-9\^]/', '', $sInput);

	            // Change the caret back to a decimal point
	            $sInput = str_replace("^", ".", $sInput);

	            // Convert to float
	            //$sInput = (float) $sInput;


	            // Check if negative and change sign if so
	            if ($bNeg)
	            {
	                $sInput = 0 - $sInput;
	            }

	            return $sInput;
	            // Return the input formatted to 2 decimal places
	            return  sprintf("%.2f", $sInput);
	        }
		}
        /**
         * Load the constants file, which includes database config and start
         * the session
         */
	require_once("includes/constants.php");

        // Load Guzzle REST Http Client
        require_once('Guzzle/vendor/autoload.php');


        /**
        * Number formatting reversal function
        * Takes a number, checks if there's a decimal part
        * (by checking the characters towards the end of the string)
        * removes all non-numeric characters except the decimal point
        * and returns to 2 decimal places
        * Also correctly handles negative numbers by a similar process
        *
        * @param float $fInput input to function
        * @return float
        */
        function reverseNumberFormat($fInput)
        {
            // Set a flag to record if the number is negative
            // default to nfalse
            $bNeg = false;

            // Cast to a string for this process
            $sInput = (string) $fInput;

            // Starting position character - we'll check if this is a minus sign
            $cPos0 = substr($sInput, 0, 1);

            // Check if the first character is a minus sign
            // Set as negative if so
            // and remove the character before performing further checks
            if ($cPos0 == '-')
            {
                $bNeg = true;
                $sInput = substr($sInput, 1);
            }

             // Get the characters at the second and thiird positions
            // from the end - these will be checked in a moment
            $cPos2 = substr($sInput, -2, 1);
            $cPos3 = substr($sInput, -3, 1);

            // If either of the two characters are NOT numeric
            // replace them with something else for the moment
            // in this case, a caret ^
            // This will represent our decimal point
            if (!is_numeric($cPos2))
            {
                $sInput = str_replace($cPos2, '^',  $sInput);
            }

            if (!is_numeric($cPos3))
            {
                $sInput = str_replace($cPos3, '^',  $sInput);
            }

            // Now remove all non-numeric characters in the string
            // EXCEPT the caret
            $sInput = preg_replace('/[^a-zA-Z0-9\^]/', '', $sInput);

            // Change the caret back to a decimal point
            $sInput = str_replace("^", ".", $sInput);

            // Convert to float
            //$sInput = (float) $sInput;


            // Check if negative and change sign if so
            if ($bNeg)
            {
                $sInput = 0 - $sInput;
            }

            return $sInput;
            // Return the input formatted to 2 decimal places
            return  sprintf("%.2f", $sInput);
        }



        @session_start();
//        session_regenerate_id();
