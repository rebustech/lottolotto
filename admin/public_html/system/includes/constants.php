<?php
try{

    /**
     * Prevent direct access
     */
    if(basename($_SERVER['SCRIPT_FILENAME'])=='constants.php'){
        header('Location: /');
    }

    /**
     * Useful function to print_r but nicely, also allows filtering by IP so you
     * can print_r but only to yourself
     * @todo This shouldn't be here as this file is a script, we should move it somewhere else
     * @param mixed $sInput The object or array to print_r
     * @param boolean $bExit if set to true will die after print_r ing
     * @param string $sFilterIP IP Address to show this message to
     */
    function print_d($sInput, $bExit = false, $sFilterIP = NULL)
    {
    	if ( $sFilterIP == NULL || $_SERVER['REMOTE_ADDR'] == $sFilterIP )
    	{
    		echo "<pre>";
    		print_r($sInput);
    		echo "</pre>";
    		if ( $bExit )
    		{
    			exit;
    		}
    	}
    }

    /**
     * Enable or disable error reporting
     */
    if(isset($_GET['error_reporting'])){
        error_reporting(E_ALL);
    }

    /**
     * Create a new config object and load in the host based config
     */
    $config=new Config();
    //$config->sServerPath='c:/sites/lovelotto/public_html';
    //$config->sServerPath='F:/Customers/Network649/MaxLottos/ll/public_html';
    Config::$config->sServerPath='C:\Users\Nick\Desktop\Work\Lotto\latest\public_html';

    $config->loadConfig();

    /**
     * Fill in any default values (i.e. not configured by the host based stuff)
     * Some config values depend on each other so this way is more appropriate
     */

    if(!isset($config->bSendEmails)) $config->bSendEmails=true;
    if(!isset($config->sDatabaseName)) $config->sDatabaseName='lovelotto';
    if(!isset($config->sDatabaseHost)) $config->sDatabaseHost='127.0.0.1';
    if(!isset($config->sDatabaseUser)) $config->sDatabaseUser='root';
    if(!isset($config->sDatabasePassword)) $config->sDatabasePassword='';

    if(!isset($config->iLanguageId)) $config->iLanguageId=1;


    if(!isset($config->sEncrptKey)) $config->sEncrptKey='baracuda';
    if(!isset($config->sEncryptedLogin)) $config->sEncryptedLogin='5a17d5ef0757623bd7871bd5d0f1bef9';
    if(!isset($config->sEncryptedCookieName)) $config->sEncryptedCookieName='5b277f7f529e61fab57de0d8bb00bf28';

    if(!isset($config->sWebsiteName)) $config->sWebsiteName='maxlotto';
    if(!isset($config->sWebsiteTitle)) $config->sWebsiteTitle='MaxLotto.com';
    if(!isset($config->sSmartyDir)) $config->sSmartyDir=$config->sServerPath.'/../Smarty-2.6.28/libs/';
    if(!isset($config->sSmartyClass)) $config->sSmartyClass=$config->sSmartyDir.'Smarty.class.php';
    if(!isset($config->sTemplatePath)) $config->sTemplatePath=$config->sServerPath.'/skins/'.$config->sWebsiteName.'/templates';
    if(!isset($config->bSmartyCache)) $config->bSmartyCache=true;
    if(!isset($config->sTransactionReferencePrefix)) $config->sTransactionReferencePrefix='ML';

    if(!isset($config->sSmartyCacheDir)) $config->sSmartyCacheDir=$config->sServerPath.'/../../caches/'.$config->sWebsiteName.'/cache';
    if(!isset($config->sSmartyCompileDir)) $config->sSmartyCompileDir=$config->sServerPath.'/../../caches/'.$config->sWebsiteName.'/compile';

    @mkdir($config->sSmartyCacheDir);
    @chmod($config->sSmartyCacheDir,0755);
    @mkdir($config->sSmartyCompileDir);
    @chmod($config->sSmartyCompileDir,0755);


    /**
     * -------------------------------------------------------------------------
     * Now we need to hook our nice new config into the original way of doing things
     */

    $sServerPath = $config->sServerPath;

    define('SEND_MAILS',$config->bSendEmails);
    define("DATABASENAME", $config->sDatabaseName);
    define("DBSERVERADDRESS", $config->sDatabaseHost);
    define("DBUSERNAME", $config->sDatabaseUser);
    define("DBPASSWORD", $config->sDatabasePassword);

    define("ENCRYPTKEY", $config->sEncrptKey);
    define("ENCRYPTEDLOGIN", $config->sEncryptedLogin);
    define("ENCRYPTEDCOOKIENAME", $config->sEncryptedCookieName);

    $sServerName = $_SERVER['SERVER_NAME'];
    define("WEBSITEPATH", $sServerPath . "/");
    define("CLASSPATH", $sServerPath . "/system/classes/");
    define("SYSTEMPATH", $sServerPath . "/system/");

    define("WEBSITENAME", $config->sWebsiteName);
    define("WEBSITETITLE", $config->sWebsiteTitle);
    define("WEBSITEURL", '/');
    define("WEBSITEHOST", $_SERVER['SERVER_NAME'].'/');
    define("SMARTY_DIR", $config->sSmartyDir);
    define("SMARTY_CLASS", $config->sSmartyClass);
    define("TEMPLATEPATH", $config->sTemplatePath);
    define("SMARTYCACHE", $config->bSmartyCache);
    define("TRANSACTIONREFERENCE", $config->sTransactionReferencePrefix); //Text to preceede timestamp in transaction

    //LoveLotto.com Keys
    define("CAPTCHAPRIVATE", "6LfTP74SAAAAAN0sOJmuX1r99r65bpG0fvSWddNp");
    define("CAPTCHAPUBLIC" , "6LfTP74SAAAAAHI4uRZkGUENYj9LCSADccwsKyoz");

    //LoveLotto.dev Keys
    //define("CAPTCHAPRIVATE", "6LdWP74SAAAAALDOj-JtsXhNCqzmsZ_8ABfgeBax");
    //define("CAPTCHAPUBLIC" , "6LdWP74SAAAAABYMTQRin0hSfh8Uh1ThIdJKg816");
    $iAdminTypeID = 1;
    $sSessionName = "adSecurityObject";

    
    

    /**
     * Check that serverpath is set and that it can be accessed
     */
    if(!isset(Config::$config->sServerPath)){
        throw new LottoException('Serverpath is not set', 'Config.SererPathNotSet');
    }
    if(!is_dir(Config::$config->sServerPath)){
        throw new LottoException('Serverpath does not exist', 'Config.ServerPathDoesNotExist');
    }
    
}catch(Exception $e){
    $e->getExceptionMessage();
}