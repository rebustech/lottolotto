<?php
/**
 * Config for melchett.lovelotto.com
 * JP's main computer
 */
Config::$config->bSendEmails=false;
Config::$config->sDatabaseUser='root';
Config::$config->sDatabasePassword='';
Config::$config->sServerPath='H:/sites/lovelotto/public_html';

Config::$config->sTemplatePath=Config::$config->sServerPath.'/skins/maxlotto/templates/';
Config::$config->sWebsiteName='maxlotto';
Config::$config->sWebsiteTitle='MaxLotto.com';