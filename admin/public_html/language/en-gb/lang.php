gateways-gateway_id:ID
gateways-comment:Name
gateways-classname:Internal handler
gateways-account:Our account name
gateways-url:Endpoint URL
gateways-test_account:Test account name
gateways-test_url:Endpoint test URL
gateways-use_test:Test mode
gateways-is_active:Active
gateways-enabled:Enabled

